package com.onextwo.api;

/**
 * Methods for 1x2
 * 
 * @author Alexandre
 *
 */
public enum OnexTwoMethod {
	GET_ACCOUNT_DETAILS("GetAccountDetails"),
	PLACE_VIRTUAL_BET("PlaceVirtualBet"),
	SETTLE_VIRTUAL_BET("SettleVirtualBet"),
	CANCEL_BET("CancelBet"),
	QUERY_BET("QueryBet"),
	GET_BALANCE("GetBalance");
	
	private final String value;

	OnexTwoMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }
}
