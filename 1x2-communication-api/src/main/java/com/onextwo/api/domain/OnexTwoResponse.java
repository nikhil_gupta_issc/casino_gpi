package com.onextwo.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.onextwo.api.OnexTwoCodes;

/**
 * Response for 1x2
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class OnexTwoResponse {
	@JsonProperty(value="error_code")
    protected Integer errorCode = OnexTwoCodes.SUCCESS;
	
	@JsonProperty(value="error_desc")
    protected String errorDesc = "success";
    
    @JsonProperty(value="error_code")
	public Integer getErrorCode() {
		return errorCode;
	}
    
	@JsonProperty(value="error_code")
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	@JsonProperty(value="error_desc")
	public String getErrorDesc() {
		return errorDesc;
	}
	
	@JsonProperty(value="error_desc")
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	@Override
	public String toString() {
		return "OnexTwoResponse [errorCode=" + errorCode + ", errorDesc=" + errorDesc + "]";
	}
}
