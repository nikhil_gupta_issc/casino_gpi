package com.onextwo.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Transaction response for 1x2
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class OnexTwoTransactionResponse extends OnexTwoResponse {
	
	@JsonProperty(value="bet_id")
    protected String betId;
    protected String balance;
    
	@JsonProperty(value="bet_id")
	public String getBetId() {
		return betId;
	}
	
	@JsonProperty(value="bet_id")
	public void setBetId(String betId) {
		this.betId = betId;
	}
	
	public String getBalance() {
		return balance;
	}
	
	public void setBalance(String balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "OnexTwoTransactionResponse [betId=" + betId + ", balance=" + balance + ", errorCode=" + errorCode
				+ ", errorDesc=" + errorDesc + "]";
	}
}
