package com.onextwo.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * GetAccountDetails response for 1x2
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class OnexTwoGetAccountDetailsResponse extends OnexTwoResponse {
	@JsonProperty(value="account_id")
    protected String accountId;
	
	@JsonProperty(value="iso_code")
    protected String isoCode;
	
    protected String balance;
    
	@JsonProperty(value="account_id")
	public String getAccountId() {
		return accountId;
	}
	
	@JsonProperty(value="account_id")
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	@JsonProperty(value="iso_code")
	public String getIsoCode() {
		return isoCode;
	}
	
	@JsonProperty(value="iso_code")
	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}
	
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "OnexTwoGetAccountDetailsResponse [accountId=" + accountId + ", isoCode=" + isoCode + ", balance="
				+ balance + ", errorCode=" + errorCode + ", errorDesc=" + errorDesc + "]";
	}
}
