package com.onextwo.api.domain;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * CancelBet response for 1x2
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class OnexTwoCancelBetResponse extends OnexTwoTransactionResponse {
    protected String f1x2ID;

	public String getF1x2ID() {
		return f1x2ID;
	}

	public void setF1x2ID(String f1x2id) {
		f1x2ID = f1x2id;
	}

	@Override
	public String toString() {
		return "OnexTwoCancelBetResponse [f1x2ID=" + f1x2ID + ", betId=" + betId + ", balance=" + balance
				+ ", errorCode=" + errorCode + ", errorDesc=" + errorDesc + "]";
	}
}
