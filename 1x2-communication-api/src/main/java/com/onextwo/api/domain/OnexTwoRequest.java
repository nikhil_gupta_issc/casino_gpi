package com.onextwo.api.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Request from 1x2
 * 
 * @author Alexandre
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class OnexTwoRequest {
	@JsonProperty(value="sess_id")
	protected String sessionId;
	
	@JsonProperty(value="site_id")
	protected String siteId;
	
	protected String key;
	
	@JsonProperty(value="game_id")
	protected String gameId;
	
	@JsonProperty(value="sess_id")
	public String getSessionId() {
		return sessionId;
	}
	
	@JsonProperty(value="sess_id")
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@JsonProperty(value="site_id")
	public String getSiteId() {
		return siteId;
	}
	
	@JsonProperty(value="site_id")
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	@JsonProperty(value="game_id")
	public String getGameId() {
		return gameId;
	}
	
	@JsonProperty(value="game_id")
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	@Override
	public String toString() {
		return "OnexTwoRequest [sessionId=" + sessionId + ", siteId=" + siteId + ", key=" + key + ", gameId=" + gameId
				+ "]";
	}
}
