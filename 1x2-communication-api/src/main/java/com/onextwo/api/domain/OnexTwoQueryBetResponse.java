package com.onextwo.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * QueryBet response for 1x2
 * @author amg
 *
 */
public class OnexTwoQueryBetResponse extends OnexTwoResponse {
	
	@JsonProperty(value="bet_id")
	private String betId;

	@JsonProperty(value="bet_id")
	public String getBetId() {
		return betId;
	}

	public void setBetId(String betId) {
		this.betId = betId;
	}

	@Override
	public String toString() {
		return "OnexTwoQueryBetResponse [betId=" + betId + ", errorCode=" + errorCode + ", errorDesc=" + errorDesc
				+ "]";
	}
}
