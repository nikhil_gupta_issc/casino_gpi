package com.onextwo.api.domain;

/**
 * QueryBet request from 1x2
 * 
 * @author Alexandre
 *
 */
public class OnexTwoQueryBetRequest extends OnexTwoRequest {
	protected String description;

	@Override
	public String toString() {
		return "OnexTwoQueryBetRequest [description=" + description + ", sessionId=" + sessionId + ", siteId=" + siteId
				+ ", key=" + key + ", gameId=" + gameId + "]";
	}
}
