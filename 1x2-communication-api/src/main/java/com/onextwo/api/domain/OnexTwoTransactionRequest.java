package com.onextwo.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Transaction request from 1x2
 * 
 * @author Alexandre
 *
 */
public class OnexTwoTransactionRequest extends OnexTwoRequest {
	protected String amount;
	
	@JsonProperty(value="account_id")
	protected String accountId;
	
	protected String f1x2ID;
	
	protected String betID;
	
	protected String description;

	public String getAmount() {
		return amount;
	}
	
	@JsonProperty(value="account_id")
	public String getAccountId() {
		return accountId;
	}
	
	public String getF1x2ID() {
		return f1x2ID;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public void setF1x2ID(String f1x2id) {
		f1x2ID = f1x2id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBetID() {
		return betID;
	}

	public void setBetID(String betID) {
		this.betID = betID;
	}

	@Override
	public String toString() {
		return "OnexTwoTransactionRequest [amount=" + amount + ", accountId=" + accountId + ", f1x2ID=" + f1x2ID
				+ ", betID=" + betID + ", description=" + description + ", sessionId=" + sessionId + ", siteId="
				+ siteId + ", key=" + key + ", gameId=" + gameId + "]";
	}
}
