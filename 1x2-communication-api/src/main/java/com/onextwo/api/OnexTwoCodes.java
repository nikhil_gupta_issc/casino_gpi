package com.onextwo.api;

/**
 * Error codes for 1x2
 * 
 * @author Alexandre
 *
 */
public interface OnexTwoCodes {
	public static final Integer NO_BET_RECORD = -3;
	public static final Integer BET_NOT_FOUND = 204;
	public static final Integer GENERIC_ERROR = 104;
	public static final Integer SUCCESS = 0;

}
