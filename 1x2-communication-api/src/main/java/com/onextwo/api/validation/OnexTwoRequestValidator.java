package com.onextwo.api.validation;

import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.HashUtils;
import com.onextwo.api.domain.OnexTwoRequest;

/**
 * Request validator for 1x2 requests
 * 
 * @author Alexandre
 *
 */
public class OnexTwoRequestValidator {
	
	public boolean validateRequest(OnexTwoRequest request) {
		String saltKey = ApplicationProperties.getProperty("1x2.api.key");
		String hashedKey = HashUtils.getMD5Hash(saltKey + request.getSessionId());
		
		return hashedKey.equalsIgnoreCase(request.getKey());
	}
}
