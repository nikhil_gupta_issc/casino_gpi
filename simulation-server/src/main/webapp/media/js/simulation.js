$(document).ready(function () {

    $('#getPlayerInfo').click(function () {
        $.ajax({
            url: 'getMessage.do',
            dataType: "json",
            type: "POST",
            data: {
                token: $("#token").val(),
                method: $("#messageType").val()
            },
            success: function (data) {
                $("#xmlResult").val(data.data);
            }
        })
    });

    $('#sendPlayerInfoMessage').click(function () {
        $.ajax({
            contentType: "text/xml",
            dataType: "xml",
            url: 'gpi/handle.do',
            type: "POST",
            data: $("#xmlResult").val(),
            success: function (data) {
                var xmlstr = (new XMLSerializer()).serializeToString(data);
                $("#xmlResult").val(xmlstr);
            }
        })
    });


});