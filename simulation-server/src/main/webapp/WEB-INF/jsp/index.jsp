<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<df:setUrlProperty var="gpiUrl" name="gpi.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <script src="${staticUrl}/media/js/jquery-2.1.3.min.js" type="application/javascript"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/simulation.js"></script>
    <script src="${staticUrl}/media/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${staticUrl}/media/css/bootstrap.min.css">
    <link rel="stylesheet" href="${staticUrl}/media/css/bootstrap-theme.min.css">
</head>
<body>
<br/>

<div>
    <a class="btn btn-primary" href="<c:url value="${gpiUrl}/game.do?lang=es&token=tito&pn=sim&game="/>">Reload game</a>
</div>

<div>
    <a class="btn btn-primary" href="<c:url value="/gpi/game.do?lang=es&token=tito&pn=test&game="/>">Start game</a>
</div>

<br/>
<form id="requestForm">

    <div class="col-xs-4 input-group">
        <span class="input-group-addon">Token</span>
        <input id="token" type="text" class="form-control" disabled value="${param.token}"/>
    </div>

    <br/>

    <div class="col-xs-4 input-group">
        <span class="input-group-addon">Publisher</span>
        <input id="publisher" type="text" class="form-control" disabled value="${param.pn}"/>
    </div>

    <br/>

    <div class="col-xs-4 input-group">
        <select class="form-control" id="messageType">
            <option value="GET_PLAYER_INFO">Get player info</option>
            <option value="GET_BALANCE">Get Balance</option>
            <option value="BET">Bet</option>
            <option value="WIN">Win</option>
            <option value="REFUND_TRANSACTION">Refund transaction</option>
        </select>
    </div>

    <br/>
    <div class="col-xs-4">
        <button type="button" id="getPlayerInfo" class="btn btn-default">Generate Message</button>
        <button type="button" id="sendPlayerInfoMessage" class="btn btn-default">Send Message</button>
    </div>

    <div>
        <textarea id="xmlResult" class="form-control" rows="15"></textarea>
    </div>
</form>

</body>
</html>