package com.gpi.communication.api;

import com.gpi.api.*;
import com.gpi.service.token.TokenService;
import com.gpi.utils.MessageHelper;

/**
 * Created by nacho on 24/04/15.
 */
public class CasinoCommunicationApi implements CommunicationApi {

    private String token = TokenService.generateToken();

    private Long balance = (long) 100000;
    private String currency = "USD";

    @Override
    public Message awardWin(Message message) {
        return null;
    }

    @Override
    public Message getAccountDetails(Message message) {
        Message message1 = MessageHelper.createBasicMessageResponse(MethodType.GET_PLAYER_INFO, token);
        message1.getResult().getReturnset().setBalance(new Balance(this.balance));

        message1.getResult().getReturnset().setCurrency(new Currency(this.currency));

        message1.getResult().getReturnset().setLoginName(new LoginName("userTestSimulation"));
        return message1;
    }

    @Override
    public Message getBalance(Message message) {
        Message balanceMessage = MessageHelper.createBasicMessageResponse(MethodType.GET_BALANCE, token);
        balanceMessage.getResult().getReturnset().setBalance(new Balance(this.balance));
        balanceMessage.getResult().getReturnset().setCurrency(new Currency(this.currency));
        return balanceMessage;
    }

    @Override
    public Message placeBet(Message message) {
        return null;
    }

    @Override
    public Message refundBet(Message message) {
        return null;
    }
}
