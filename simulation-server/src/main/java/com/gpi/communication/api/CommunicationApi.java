package com.gpi.communication.api;

import com.gpi.api.Message;

public interface CommunicationApi {


    Message awardWin(Message message);

    Message getAccountDetails(Message message);

    Message getBalance(Message message);

    Message placeBet(Message message);

    Message refundBet(Message message);

}
