package com.gpi.simulation.controller;

import com.gpi.api.*;
import com.gpi.exceptions.MarshalException;
import com.gpi.service.token.TokenService;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.MessageHelper;
import com.gpi.utils.ViewUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Nacho on 11/21/2014.
 */
@Controller
public class GameSimulationController {

    private static final Logger logger = LoggerFactory.getLogger(GameSimulationController.class);

    private Long balance = 1000000l;
    private int round = 1;
    private int numberOfBets = 0;
    private List<String> transactionRefundedId = new ArrayList<>();

    @RequestMapping(value = "/index.do")
    public ModelAndView index() {
        ModelAndView index = new ModelAndView("index");
        String property = ApplicationProperties.getProperty("gpi.url");
        index.addObject("gpiUrl", property);
        return index;
    }

    @ResponseBody
    @RequestMapping(value = "/getMessage.do")
    public ModelAndView handle(HttpServletRequest request,
                         @RequestParam(value = "token", required = true) String token,
                         @RequestParam(value = "method", required = true) String method) {
        ModelAndView mav = ViewUtils.createJsonView();
        logger.info("Message received={}", request);
        MessageHelper messageHelper = new MessageHelper("username", "password", MethodType.valueOf(method), token);
        try {
            Message message = messageHelper.getMessage();
            if(MethodType.valueOf(method).equals(MethodType.BET)){
                TransactionId transactionId = new TransactionId();
                transactionId.setValue(UUID.randomUUID().toString());
                if(numberOfBets<=10){
                    numberOfBets++;
                } else {
                    round++;
                    numberOfBets=0;
                }
                RoundId roundId = new RoundId();
                roundId.setValue((long) round);
                Amount amount = new Amount();
                amount.setValue(1000l);
                GameReference gameReference = new GameReference();
                gameReference.setValue("flex");
                message.getMethod().getParams().setTransactionId(transactionId);
                message.getMethod().getParams().setRoundId(roundId);
                message.getMethod().getParams().setAmount(amount);
                message.getMethod().getParams().setGameReference(gameReference);
                transactionRefundedId.add(transactionId.getValue());
            } else if (MethodType.valueOf(method).equals(MethodType.WIN)){
                TransactionId transactionId = new TransactionId();
                transactionId.setValue(UUID.randomUUID().toString());
                RoundId roundId = new RoundId();
                roundId.setValue((long) round);
                Amount amount = new Amount();
                amount.setValue(1000l);
                GameReference gameReference = new GameReference();
                gameReference.setValue("flex");
                message.getMethod().getParams().setTransactionId(transactionId);
                message.getMethod().getParams().setRoundId(roundId);
                message.getMethod().getParams().setAmount(amount);
                message.getMethod().getParams().setGameReference(gameReference);
                transactionRefundedId.add(transactionId.getValue());
            } else if (MethodType.valueOf(method).equals(MethodType.REFUND_TRANSACTION)) {
                TransactionId transactionId = new TransactionId();
                transactionId.setValue(UUID.randomUUID().toString());
                RefundedTransactionId transactionRefundedId1 = new RefundedTransactionId();
                transactionRefundedId1.setValue(transactionRefundedId.remove(transactionRefundedId.size()-1));
                RoundId roundId = new RoundId();
                roundId.setValue((long) round);
                Amount amount = new Amount();
                amount.setValue(1000l);
                GameReference gameReference = new GameReference();
                gameReference.setValue("flex");
                message.getMethod().getParams().setTransactionId(transactionId);
                message.getMethod().getParams().setRoundId(roundId);
                message.getMethod().getParams().setAmount(amount);
                message.getMethod().getParams().setGameReference(gameReference);
                message.getMethod().getParams().setRefundedTransactionId(transactionRefundedId1);
            }else if (MethodType.valueOf(method).equals(MethodType.GET_BALANCE)) {

            }
            String stringFromMessage = MessageHelper.createStringFromMessage(message);
            mav.addObject("data", stringFromMessage);
        } catch (MarshalException e) {
            logger.error("error", e);
        }

        return mav;
    }

    @ResponseBody
    @RequestMapping(value = "/getMessage2.do", method = {RequestMethod.POST},
            consumes = "text/xml; charset=utf-8")
    public String handle2(@RequestBody String xml) throws MarshalException {

        logger.info("Message received={}", xml);
        String message = handleMessage(xml);
        logger.info("Message created: {}", message);
        return message;

    }

    private String handleMessage(String xml) throws MarshalException {
        Message messageFromString = MessageHelper.createMessageFromString(xml);
        Message messageResponse = null;
        if (messageFromString.getMethod().getName().equals(MethodType.GET_PLAYER_INFO)) {
            messageResponse = MessageHelper.createResponse(MethodType.GET_PLAYER_INFO, TokenService.generateToken());
            MessageHelper.addLoginName(messageResponse, "Tito");
            MessageHelper.addCurrency(messageResponse, "USD");
            balance = 1000000l;
            MessageHelper.addBalance(messageResponse, balance);
        } else if(messageFromString.getMethod().getName().equals(MethodType.GET_BALANCE)){
            messageResponse = MessageHelper.createResponse(MethodType.GET_BALANCE, TokenService.generateToken());
            MessageHelper.addBalance(messageResponse, balance);
        }else if(messageFromString.getMethod().getName().equals(MethodType.BET)){
            balance = balance-messageFromString.getMethod().getParams().getAmount().getValue();
            messageResponse = MessageHelper.createResponse(MethodType.BET, TokenService.generateToken());
            MessageHelper.addBalance(messageResponse, balance);
            MessageHelper.setAlreadyProcess(messageResponse, false);
            TransactionId transactionId = new TransactionId();
            transactionId.setValue(UUID.randomUUID().toString());
            messageResponse.getResult().getReturnset().setTransactionId(transactionId);
        }else if(messageFromString.getMethod().getName().equals(MethodType.WIN)){
            balance = balance + messageFromString.getMethod().getParams().getAmount().getValue();
            messageResponse = MessageHelper.createResponse(MethodType.WIN, TokenService.generateToken());
            MessageHelper.addBalance(messageResponse, balance);
            MessageHelper.setAlreadyProcess(messageResponse, false);
            TransactionId transactionId = new TransactionId();
            transactionId.setValue(UUID.randomUUID().toString());
            messageResponse.getResult().getReturnset().setTransactionId(transactionId);
        } else if (messageFromString.getMethod().getName().equals(MethodType.REFUND_TRANSACTION)) {
            balance = balance + messageFromString.getMethod().getParams().getAmount().getValue();
            messageResponse = MessageHelper.createResponse(MethodType.REFUND_TRANSACTION, TokenService.generateToken());
            MessageHelper.addBalance(messageResponse, balance);
            MessageHelper.setAlreadyProcess(messageResponse, false);
            TransactionId transactionId = new TransactionId();
            transactionId.setValue(UUID.randomUUID().toString());
            messageResponse.getResult().getReturnset().setTransactionId(transactionId);
        }else if (messageFromString.getMethod().getName().equals(MethodType.GET_BALANCE)) {
            messageResponse = MessageHelper.createResponse(MethodType.GET_BALANCE, TokenService.generateToken());
            MessageHelper.addBalance(messageResponse, balance);
        }

        return MessageHelper.createStringFromMessage(messageResponse);
    }
}
