package com.gpi.simulation.controller;

import com.gpi.exceptions.MarshalException;
import com.gpi.simulation.service.CasinoSimulationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * This controller resolves the calls to the simulation casino
 */
@Controller
public class CasinoSimulationController {

    private static final Logger log = LoggerFactory.getLogger(CasinoSimulationController.class);

    @Autowired
    private CasinoSimulationService casinoSimulationService;
    @ResponseBody
    @RequestMapping(value = "/handle.do", method = {RequestMethod.POST},
            consumes = "text/xml; charset=utf-8")
    public String handle(@RequestBody String xml) throws MarshalException {

        log.info("Message received={}", xml);
        String response = casinoSimulationService.processMessage(xml);
        log.info("Sending xml to game: {}", response);
        return response;
    }

}
