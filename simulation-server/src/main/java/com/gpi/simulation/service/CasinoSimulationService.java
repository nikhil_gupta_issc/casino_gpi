package com.gpi.simulation.service;

import com.gpi.exceptions.MarshalException;

public interface CasinoSimulationService {

    String processMessage(String xml) throws MarshalException;

}
