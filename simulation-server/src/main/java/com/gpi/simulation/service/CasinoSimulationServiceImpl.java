package com.gpi.simulation.service;

import com.gpi.api.Message;
import com.gpi.api.Method;
import com.gpi.communication.api.CommunicationApi;
import com.gpi.exceptions.MarshalException;
import com.gpi.utils.MessageHelper;

public class CasinoSimulationServiceImpl implements CasinoSimulationService {

    private CommunicationApi communicationApi;

    @Override
    public String processMessage(String xml) throws MarshalException {
        Message messageReceivedFromGpi = MessageHelper.createMessageFromString(xml);
        Message responseMessage = getResponseMessage(messageReceivedFromGpi);
        return MessageHelper.createStringFromMessage(responseMessage);
    }

    private Message getResponseMessage(Message messageReceivedFromGpi) {
        Method methodType = messageReceivedFromGpi.getMethod();
        Message responseMessage = null;
        switch (methodType.getName()) {
            case GET_PLAYER_INFO:
                responseMessage = communicationApi.getAccountDetails(messageReceivedFromGpi);
                break;
            case BET:
                responseMessage = communicationApi.placeBet(messageReceivedFromGpi);
                break;
            case WIN:
                responseMessage = communicationApi.awardWin(messageReceivedFromGpi);
                break;
            case GET_BALANCE:
                responseMessage = communicationApi.getBalance(messageReceivedFromGpi);
                break;
            case REFUND_TRANSACTION:
                responseMessage = communicationApi.refundBet(messageReceivedFromGpi);
                break;
        }
        return responseMessage;
    }


    public void setCommunicationApi(CommunicationApi communicationApi) {
        this.communicationApi = communicationApi;
    }
}
