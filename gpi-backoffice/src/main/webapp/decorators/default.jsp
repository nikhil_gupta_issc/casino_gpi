<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>
<df:setProperty var="hostUrl" name="application.host.url"/>
<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="bingoUrl" name="video.bingo.back.url" secureName="video.bingo.back.url"/>
<df:setProperty var="gitVersion" name="git.commit.id.abbrev"/>
<df:setProperty var="gitBranch" name="git.branch"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Backoffice - <decorator:title/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css" title="currentStyle">
        @import "${staticUrl}/media/css/jquery-ui-1.10.1.custom.min.css";
        @import "${staticUrl}/media/css/bootstrap.css";
        @import "${staticUrl}/media/css/bootstrap-theme.min.css";
        @import "${staticUrl}/media/css/lm.css";
        @import "${staticUrl}/media/css/select2-bootstrap.css";
        @import "${staticUrl}/media/css/select2.css";
    </style>
    <%-- <script type="text/javascript" src="${staticUrl}/media/js/jquery.js"></script>
    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>--%>
    <script type="text/javascript">
        var Back = {
            hostUrl: '${staticUrl}',
            useFilterByGame: '${report.useFilterByGame}',
            bingoUrl: '${bingoUrl}',
            publisher: {
                name: '${publisher.name}',
                id: '${publisher.id}'
            }
        }
    </script>
    <script type="text/javascript" src="${staticUrl}/media/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/jtable.2.4.0/jquery.jtable.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/jtable.iwantdev.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/additional-methods.min.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/change-password.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/utils.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/login.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/publisher_utils.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/user.util.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/moment.min.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/provider.js"></script>
    <script type="text/javascript">
        moment().format();
    </script>
    <link rel="icon" type="image/png" href="${staticUrl}/media/images/briefcase_16.png"/>
    <decorator:head/>


</head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true"/><decorator:getProperty property="body.class"
                                                                                                  writeEntireProperty="true"/>>

<div class="container">
    <div id="header">

        <jsp:include page="/common/header.jsp"/>

    </div>

    <div id="content" class="panel panel-default">
        <div id="main" class="panel-body">
            <decorator:body/>
        </div>
    </div>

    <div id="footer">
        <span class="text-right">Back.publisher.name v:${gitVersion} b:${gitBranch}</span>
    </div>
</div>

</body>
</html>
