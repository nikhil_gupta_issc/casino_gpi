$(function () {

    $("#add_provider_form").validate({
        rules: {
            providerName: {
                required: true
            }
        },
        messages: {
            providerName: {
                required: "Enter your userName"
            }
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().parent()).addClass('col-xs-9 col-xs-offset-3');
        },
        submitHandler: function (e) {
            $.ajax({
                url: "createProvider.do",
                cache: false,
                type: "POST",
                data: {
                    name: $("#add_provider_form input[name=providerName]").val(),
                    loginName: $("#add_provider_form input[name=providerLoginName]").val(),
                    password: $("#add_provider_form input[name=providerPassword]").val(),
                    prefix: $("#add_provider_form input[name=providerPrefix]").val(),
                    api: $("#add_provider_form input[name=providerApi]").val(),
                    hasExtendedInfo: $("#add_provider_form input[name=hasExtendedInfo]").prop('checked')
                },
                dataType: "json"
            }).done(function (json) {
                if (json.success) {
                    $("#add_provider_form input[name=providerName]").val("");
                    $("#add_provider_form input[name=providerLoginName]").val("");
                    $("#add_provider_form input[name=providerPassword]").val("");
                    $("#add_provider_form input[name=providerPrefix]").val("");
                    $("#add_provider_container").modal("hide");
                    $("#add_provider_error").text("");
                    $('#provider_table_container').jtable('load');

                } else {
                    $("#add_provider_error").text("Request failed: " + json.Error);
                }
            }).fail(function (jqXHR, json) {
                $("#add_provider_error").text("Request failed: " + json)
            });
        }
    });

    $('#provider_table_container').ready(function () {
        $('#provider_table_container').jtable({
            title: 'Providers List',
            paging: true,
            pageSize: 10,
            sorting: true,
            multiSorting: true,
            selecting: true,
            multiselect: false,
            defaultSorting: 'id DESC',
            actions: {
                listAction: 'providers.do'
            },
            toolbar: {
                items: [
                    {
                        text: 'Edit',
                        click: function () {
                            var selectProvider = $("#provider_table_container").jtable('selectedRows').first();
                            var record = selectProvider.data('record');

                            if (!record) {
                                alert("You have to select a Provider");
                                return;
                            }
                            $("#edit_provider_container").modal('show');
                            $("#edit_provider_form input[name=providerName]").val(record.name);
                            $("#edit_provider_form input[name=newProviderName]").val(record.name);
                            $("#edit_provider_form input[name=newProviderLoginName]").val(record.loginName);
                            $("#edit_provider_form input[name=newProviderPassword]").val(record.password);
                            $("#edit_provider_form input[name=newProviderApi]").val(record.apiName);
                            $("#edit_provider_form input[name=newProviderPrefix]").val(record.prefix);
                            $("#edit_provider_form input[name=hasExtendedInfoEdit]").prop('checked', record.hasExtendedInfo);
                            $('#provider_table_container').jtable('load');

                        }
                    }, {
                        text: 'Add new provider',
                        click: function () {
                            $("#add_provider_container").modal('show');
                        }
                    }
                ]
            },
            fields: {
                id: {
                    key: true,
                    title: 'id'
                },
                name: {
                    title: 'Name'

                },
                loginName: {
                    title: 'Login Name'

                },
                password: {
                    title: 'Password'

                },
                prefix: {
                	title: 'Prefix'
                },
                apiName: {
                    title: 'Api Used'

                }, hasExtendedInfo: {
                    title: 'Provide extended info?',
                    type: 'checkbox',
                    values: {'false': 'No', 'true': 'Yes'}
                }
            }
        });

        $('#provider_table_container').jtable('load');

    });

    $("#edit_provider_form").validate({
        rules: {
            providerName: {
                required: true
            }
        },
        messages: {
            providerName: {
                required: "Enter the provider name"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: "editProvider.do",
                cache: false,
                type: "POST",
                data: {
                    name: $("#edit_provider_form input[name=providerName]").val(),
                    newName: $("#edit_provider_form input[name=newProviderName]").val(),
                    loginName: $("#edit_provider_form input[name=newProviderLoginName]").val(),
                    password: $("#edit_provider_form input[name=newProviderPassword]").val(),
                    prefix: $("#edit_provider_form input[name=newProviderPrefix]").val(),
                    api: $("#edit_provider_form input[name=newProviderApi]").val(),
                    hasExtendedInfo: $("#edit_provider_form input[name=hasExtendedInfoEdit]").prop('checked')
                },
                dataType: "json"
            }).done(function (json) {
                if (json.success) {
                    $("#edit_provider_form input[name=providerName]").val("");
                    $("#edit_provider_container").modal("hide");
                    $("#edit_provider_error").text("");
                    $('#provider_table_container').jtable('load');
                } else {
                    $("#edit_provider_error").text(json.success);
                }
            }).fail(function (jqXHR, json) {
                $("#edit_provider_error").text("Request failed: " + json)
            });
        }
    });

});