$(function () {
    completePublishers();

    $('#apply_pg_filters').click(function (e) {
        e.preventDefault();
        $('#game_mode_table_container').jtable('load', {
            publisher: $('#gamesSelectRound').val()
        });
    });

    $('#game_mode_table_container').ready(function () {
        $('#game_mode_table_container').jtable({
            title: 'Game modes List',
            paging: true,
            pageSize: 10,
            sorting: true,
            multiSorting: true,
            selecting: true,
            multiselect: false,
            defaultSorting: 'id DESC',
            actions: {
                listAction: 'getGamesModes.do'
            },
            toolbar: {
                items: [
                    {
                        text: 'Change Modes',
                        click: function () {
                            var selectedGame = $("#game_mode_table_container").jtable('selectedRows').first();
                            var record = selectedGame.data('record');

                            if (!record) {
                                alert("You have to select a Game");
                                return;
                            }
                            $("#change_games_modes_container").dialog({
                                height: 300,
                                width: 320,
                                modal: true
                            }).show();

                            $("#change_games_modes_container input[name=pubGameId]").val(record.id);
                            $("#change_games_modes_container input[name=gameName]").val(record.game);
                            $("#change_games_modes_container input[name=charged]").prop("checked",
                                (record.charged) == ('true') ? true : false);
                            $("#change_games_modes_container input[name=free]").prop("checked",
                                (record.free) == ('true') ? true : false);
                        }
                    }
                ]
            },
            fields: {
                id: {
                    key: true,
                    title: 'id'
                },
                game: {
                    title: 'Game'
                },
                publisher: {
                    title: 'Publisher Name'
                },
                free: {
                    title: 'FREE',
                    type: 'checkbox',
                    values: { 'false': 'Off', 'true': 'On' }
                },
                charged: {
                    title: 'CHARGED',
                    type: 'checkbox',
                    values: { 'false': 'Off', 'true': 'On' }
                }
            }});

        $('#game_mode_table_container').jtable('load');

    });

    $('#games_table_container').ready(function () {
        $('#games_table_container').jtable({
            title: 'Game List',
            paging: true,
            pageSize: 10,
            sorting: true,
            multiSorting: true,
            selecting: true,
            multiselect: false,
            defaultSorting: 'id DESC',
            actions: {
                listAction: 'getGames.do'
            },
            toolbar: {
                items: [
                    {
                        text: 'Edit',
                        click: function () {
                            var selectedGame = $("#games_table_container").jtable('selectedRows').first();
                            var record = selectedGame.data('record');

                            if (!record) {
                                alert("You have to select a Game");
                                return;
                            }
                            window.open(Back.hostUrl +  "/admin/games/edit.do?id=" + record.id);
                        }
                    },{
                        text: 'Add',
                        click: function () {
                            window.location.href = (Back.hostUrl +  "/admin/games/edit.do");
                        }
                    }
                ]
            },
            fields: {
                id: {
                    key: true,
                    title: 'id'
                },
                name: {
                    title: 'Name'
                },
                url: {
                    title: 'Game URL'
                },
                provider: {
                    title: 'Provider'
                }
            }});

        $('#games_table_container').jtable('load');

    });

    $("#change_games_modes_form").validate({
        rules: {

        },
        messages: {

        },
        submitHandler: function (e) {
            $.ajax({
                url: "changeGameModes.do",
                cache: false,
                type: "POST",
                data: {
                    free: $("#change_games_modes_container input[name=free]").is(':checked'),
                    charged: $("#change_games_modes_container input[name=charged]").is(':checked'),
                    gameName: $("#change_games_modes_container input[name=gameName]").val(),
                    pubGameId: $("#change_games_modes_container input[name=pubGameId]").val()
                }
            }).done(function (json) {
                if (json.success) {
                    $("#change_games_modes_container input[name=gameName]").val("");
                    $("#change_games_modes_container input[name=pubGameId]").val("");
                    $("#change_games_modes_container input[name=gameName]").val("");
                    $("#change_games_modes_error").text("");
                    $("#change_games_modes_container").dialog("close");
                    $('#game_mode_table_container').jtable('load');
                } else {
                    $("#change_games_modes_error").text(json.error);
                }
            }).fail(function (jqXHR, json) {
                $("#change_games_modes_error").text("Request failed: " + json)
            });
        }
    });

$("#edit_game_form").validate({
        rules: {

        },
        messages: {

        },
        submitHandler: function (e) {
            $.ajax({
                url: "editGame.do",
                cache: false,
                type: "POST",
                data: {
                    newName: $("#newName").val(),
                    newUrl: $("#editNewURL").val(),
                    newProvider: $("#providerSelect").val(),
                    game: $("#editGameName").val()
                }
            }).done(function (json) {
                if (json.success) {
                    $("#edit_game_container input[name=gameName]").val("");
                    $("#edit_game_container input[name=pubGameId]").val("");
                    $("#edit_game_container input[name=gameName]").val("");
                    $("#edit_game_error").text("");
                    $("#edit_game_container").modal("hide");
                    $('#games_table_container').jtable('load');
                } else {
                    $("#edit_game_error").text(json.error);
                }
            }).fail(function (jqXHR, json) {
                $("#edit_game_error").text("Request failed: " + json)
            });
        }
    });

    function completePublishers() {
        $.getJSON(Back.hostUrl + "/admin/getPublishers.do", function (j) {
            var options = '<option  value=""></option>';
            for (var i = 0; i < j.data.length; i++) {
                options += '<option  value="' + j.data[i].name + '">' + j.data[i].name + '</option>';
            }

            $("select#gamesSelectRound").html(options);
        })
    }

    function completeProviders(provider) {
        $.getJSON(Back.hostUrl + "/admin/getProviders.do", function (j) {
            var options = '<option  value=""></option>';
            for (var i = 0; i < j.data.length; i++) {
                options += '<option  ' + ((j.data[i].name == provider) ? "selected" : "") + ' value="' + j.data[i].name + '">' + j.data[i].name + '</option>';
            }

            $("select#providerSelect").html(options);
        })
    }

});
