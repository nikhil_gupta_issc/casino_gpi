$(document).ready(function() {
	$('#table_container').jtable({
		title : 'Report List',
		paging : true,
		pageSize : 10,
		sorting : true,
		multiSorting : true,
		defaultSorting : 'name ASC',
		selecting: true,
        multiselect: false,
        selectingCheckboxes: false,
		actions : {
			listAction : 'reports.do'
		},
		messages: {
			deleteText: 'Delete Report',
			deleteConfirmation: 'The Report will be deleted. Are you sure ?'
		},
		toolbar: {
		    items: [
		    {
		        text: 'Execute Report',
		        click: function () {
		        	var selectedReport = $('#table_container').jtable('selectedRows').first();
		        	var record = selectedReport.data('record');

		        	if(!record){
		        		Back.Ui.basicModal({
		        			title: 'Execute Report',
		        			content: 'You have to select a report'
		        		});
		        		return;
		        	}

		        	document.location.href = 'execution-setup.do?name=' + encodeURIComponent(record.name);
		        }
		    },{
		        text: 'Remove',
		        click: function () {
		        	var selectedReport = $('#table_container').jtable('selectedRows').first();
		        	var record = selectedReport.data('record');

		        	if(!record){
		        		Back.Ui.basicModal({
		        			title: 'Remove Report',
		        			content: 'You have to select a report'
		        		});
		        		return;
		        	}

		        	document.location.href = 'removeReport.do?id=' + encodeURIComponent(record.id);
		        }
		    }]
		},
		fields : {
			id: {
				key : true,
				list: false
			},
			name: {
				title : 'Name',
                width: '20%'
			},
			description: {
				title : 'Description',
                width: '80%'
			}
	}});

	$('#table_container').jtable('load');
});
