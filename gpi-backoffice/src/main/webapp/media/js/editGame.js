$(document).ready(function() {
	$('#game').validate({
		wrapper: 'div',
		rules: {
			payout: {
		      number: true,
		      range: [1, 100]
		    },
		    averageRating: {
		    	number: true,
			    range: [0, 5]
		    }
		},
		errorPlacement: function(error, element) {
			error.prependTo(element.parent().parent())
				.addClass('error col-lg-offset-2 col-lg-10');
		},
		highlight: function(element, errorClass, validClass) {
			$(element).parent().addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parent().removeClass('has-error');
		}
	});	

	$("#gameType, #gamePlatform, #gameProvider").select2({
		dropdownCssClass: 'df-select2-dropdown-no-search'
	});
	
	$('#averageRating').number(true, 2, '.', '');
});