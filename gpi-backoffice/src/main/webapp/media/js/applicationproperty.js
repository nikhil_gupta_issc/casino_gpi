$(function () {

    $("#applicationproperty_table_container").jtable({
    	title: 'Application Properties',
        paging: true, //Enable paging
        pageSize: 10, //Set page size (default: 10)
        sorting: true, //Enable sorting
        defaultSorting: 'Order name ASC', //Set default sorting
    	actions: {
            listAction: '/applicationproperty/list.do',
            createAction: '/applicationproperty/create.do',
            updateAction: '/applicationproperty/update.do',
            deleteAction: '/applicationproperty/delete.do'
        },
        fields: {
            'id':{
            	key: true,
            	list: false,
            },
        	'name': {
            	title: 'key',
                width: '50%',
                required: true
            },	
            'value': {
            	title: 'value',
            	width: '50%',
            	required: true
            }
        }	
        ,
        formCreated: function(event, data){
        	data.form.css('width','400px');
        	data.form.find('input').css('width','380px');
        }
    }).jtable('load');

});