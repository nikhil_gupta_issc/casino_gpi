/**
 * Created by igabba on 19/05/15.
 */

(function ($) {

    //extension members
    $.extend(true, $.hik.jtable.prototype, {

        allRows: function () {
            return this._getAllRows();
        }, _getAllRows: function () {
            return this._$tableBody
                .find('>tr.jtable-data-row');
        }

    });

})(jQuery);