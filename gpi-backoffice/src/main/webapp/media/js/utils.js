var Back = Back|| {};
Back.Utils = Back.Utils || {
    toCents: function(amount) {
        if(amount == undefined || amount == null || amount == '' ) {
            return null;
        }

        var fixed = parseFloat(amount).toFixed(2);
        return fixed.replace('.', '');
    },
    toFloat: function(amount) {
        if(amount == undefined || amount == null || amount == '' ) {
            return null;
        }
        var value = parseInt(amount, 10);
        return value / 100;
    },
    getParameterByName: function (name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}




