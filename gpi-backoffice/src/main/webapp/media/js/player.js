$(function () {

completePublishers();



$('#player_table_container').ready(function () {
    $('#player_table_container').jtable({
        title: 'Players List',
        paging: true,
        pageSize: 10,
        sorting: true,
        multiSorting: true,
        selecting: true,
        multiselect: false,
        defaultSorting: 'id DESC',
        actions: {
            listAction: 'players.do'
        },
        selectionChanged: function () {
            var selectPlayer = $("#player_table_container").jtable('selectedRows').first();
            var record = selectPlayer.data('record');

        },
        toolbar: {
        },
        fields: {
            id: {
                key: true,
                title: 'id'
            },
            name: {
                title: 'Player Name'
            },
            publisher: {
                title: 'Publisher',
                visibility: 'hidden'
            },
            createdOn: {
                title: 'Created On',
                display: function (data) {

                    var formattedDate = new Date(data.record.createdOn);
                    return moment(formattedDate).format('YYYY-M-D h:mm:ss a');
                }
            }
        }

    });

    $('#player_table_container').jtable('load');

    $('#apply_filters').click(function (e) {
        e.preventDefault();
        $('#player_table_container').jtable('load', {
            playerId: $('#playerId').val(),
            playerName: $('#playerName').val(),
            publisher: $('#publisherSelectPlayer').val(),
        });
    });


});

$("#playerName").autocomplete({
    source: "players-suggest.do",
    minLength: 2,
    selected: function (event, ui) {
        log(ui.item ?
            "Selected: " + ui.item.name :
            "Nothing selected, input was " + this.value);
    }
});

function completePublishers() {
    $.getJSON(Back.hostUrl + "/admin/getPublishers.do", function (j) {
        var options = '<option  value=""></option>';
        for (var i = 0; i < j.data.length; i++) {
            options += '<option  value="' + j.data[i].id + '">' + j.data[i].name + '</option>';
        }

        $("select#publisherSelectPlayer").html(options);
    })
}


});