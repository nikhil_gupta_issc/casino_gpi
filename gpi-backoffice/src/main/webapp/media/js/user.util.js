$(function () {
    $('#change_passwords_container').on('hidden.bs.modal', function (e) {
        $("#change_users_passwords_form input").val("").removeClass('error');
        $("#reset_passwords_error").text("");
        $('label.error').remove();                        
    });

    $('#add_permission_container').on('hidden.bs.modal', function (e) {
        $("#add_permission_form input").val("").removeClass('error');
        $("#add_permission_error").text("");                
        $('label.error').remove();                        
    });

    $('#change_publisher_for_user').on('hidden.bs.modal', function (e) {
        $("#change_publisher_for_user_form input").val("").removeClass('error');
        $("#change_publisher_for_user_error").text("");             
        $('label.error').remove();                                            
    });

    $('#add_new_games_container').on('show.bs.modal', function (e) {
        completeProviders();
    });

    $('#user_table_container').ready(function () {
        $('#user_table_container').jtable({
            title: 'User List',
            paging: true,
            pageSize: 10,
            sorting: true,
            multiSorting: true,
            selecting: true,
            multiselect: false,
            defaultSorting: 'id DESC',
            actions: {
                listAction: 'user.do'
            },
            toolbar: {
                items: [
                    {
                        text: 'Change Password',
                        click: function () {
                            var selectedUser = $("#user_table_container").jtable('selectedRows').first();
                            var record = selectedUser.data('record');

                            if (!record) {
                                alert("You have to select a User");
                                return;
                            }

                            $("#change_passwords_container").modal('show');

                            $("#change_users_passwords_form input[name=userId]").val(record.id);
                        }
                    },
                    {
                        text: 'Add permissions',
                        click: function () {
                            var selectedUser = $("#user_table_container").jtable('selectedRows').first();
                            var record = selectedUser.data('record');

                            if (!record) {
                                alert("You have to select a User");
                                return;
                            }

                            $("#add_permission_form input[name=userId]").val(record.id);
                            $("#add_permission_container").modal('show');
                            completePermissions();
                        }
                    },
                    {
                        text: 'Change publisher',
                        click: function () {
                            var selectedUser = $("#user_table_container").jtable('selectedRows').first();
                            var record = selectedUser.data('record');

                            if (!record) {
                                alert("You have to select a User");
                                return;
                            }

                            $("#change_publisher_for_user input[name=userId]").val(record.id);
                            $("#change_publisher_for_user").modal('show');
                            completePublishers();
                        }
                    },
                    {
                        text: 'Add New User',
                        click: function () {
                            $("#add_user_container").modal('show');
                            completePublishers();
                            completeAvailablePermissions();
                        }
                    }
                ]
            },
            fields: {
                id: {
                    key: true,
                    title: 'id'
                },
                name: {
                    title: 'Name'

                },
                publisher: {
                    title: 'Publisher'

                },
                provider: {
                    title: 'Provider'

                },
                permission: {
                    title: 'Permissions'

                },
                createdOn: {
                    title: 'Created On'

                },
                lastLogin: {
                    title: 'Last Login'
                }
            }});

        $('#user_table_container').jtable('load');

    });

    $("#add_new_games_form").validate({
        rules: {
            game: {
                required: true
            }
        },
        messages: {
            game: {
                required: "Game Name Required"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: "addNewGame.do",
                cache: false,
                type: "POST",
                data: {
                    game: $("#add_new_games_form input[name=game]").val(),
                    url: $("#add_new_games_form input[name=url]").val(),
                    provider: $("#add_new_games_form select[name=add_game_provider]").val()
                },
                dataType: "json"
            }).done(function (json) {
                if (json.success) {
                    $("#add_new_games_form input[name=game]").val("");
                    $("#add_new_games_form input[name=url]").val("");
                    $("#add_new_games_form select[name=add_game_provider]").val("");
                    $("#add_new_games_error").text("");
                    $('#add_new_games_container').modal('hide');
                } else {
                    $("#add_new_games_error").text(json.error);
                }
            }).fail(function (jqXHR, json) {
                $("#add_new_games_error").text("Request failed: " + json)
            });
        }
    });

    $("#add_user_form").validate({
        rules: {
            userName: {
                required: true,
                minlength: 6
            },
            password: {
                required: true,
                minlength: 6
            },
            passwordConfirmation: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            userName: {
                required: "Enter your userName",
                minlength: "Must be at least 6 characters long"
            },
            password: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            passwordConfirmation: {
                equalTo: "Passwords don't match"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: "createUser.do",
                cache: false,
                type: "POST",
                data: {
                    userName: $("#add_user_form input[name=userName]").val(),
                    password: $("#add_user_form input[name=password]").val(),
                    passwordConfirmation: $("#add_user_form input[name=passwordConfirmation]").val(),
                    publisher: $("#add_user_form select[name=publisher]").val(),
                    permission: $("#add_user_form select[name=add_user_form_permissions]").val()
                },
                dataType: "json"
            }).done(function (json) {
                    if (json.success) {
                        $("#add_user_form input[name=userName]").val("");
                        $("#add_user_form input[name=password]").val("");
                        $("#add_user_form input[name=passwordConfirmation]").val("");
                        $('#user_table_container').jtable('load');
                        $("#add_user_container").modal("hide");
                    } else {
                        $("#add_user_error").text(json.error);
                    }
                }).fail(function (jqXHR, json) {
                    $("#add_user_error").text("Request failed: " + json)
                });
        }
    });

    $("#change_passwords_form").validate({
        rules: {
            currentPassword: {
                required: true,
                minlength: 6
            },
            newPassword: {
                required: true,
                minlength: 6
            },
            newPasswordConfirmation: {
                required: true,
                equalTo: "#newPassword"
            }
        },
        messages: {
            currentPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            newPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            newPasswordConfirmation: {
                equalTo: "Passwords don't match"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: Back.hostUrl + "/changePassword.do",
                cache: false,
                type: "POST",
                data: 'currentPassword=' + $("#change_passwords_form input[name=currentPassword]").val()
                    + '&newPassword=' + $("#change_passwords_form input[name=newPassword]").val()
                    + '&newPasswordConfirmation=' + $("#change_passwords_form input[name=newPasswordConfirmation]").val()
                    + '&userId=' + $("#change_passwords_form input[name=userId]").val()
            }).done(function (json) {
                    if (json.success) {
                        $("#change_passwords_form input[name=currentPassword]").val("");
                        $("#change_passwords_form input[name=newPassword]").val("");
                        $("#change_passwords_form input[name=newPasswordConfirmation]").val("");
                        $("#reset_passwords_error").text("");
                        $("#change_passwords_container").dialog("close");
                    } else {
                        $("#reset_passwords_error").text(json.error);
                    }
                }).fail(function (jqXHR, json) {
                    $("#reset_password_error").text("Request failed: " + json)
                });
        }
    });

    $("#change_users_passwords_form").validate({
        rules: {
            currentUserPassword: {
                required: true,
                minlength: 6
            },
            newUserPassword: {
                required: true,
                minlength: 6
            },
            newUserPasswordConfirmation: {
                required: true,
                equalTo: "#newUserPassword"
            }
        },
        messages: {
            currentUserPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            newUserPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            newUserPasswordConfirmation: {
                equalTo: "Passwords don't match"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: "changePassword.do",
                cache: false,
                type: "POST",
                data: {
                    newPassword: $("#change_users_passwords_form input[name=newUserPassword]").val(),
                    userId: $("#change_users_passwords_form input[name=userId]").val()
                }
            }).done(function (json) {
                    if (json.success) {
                        $("#change_passwords_container").modal("hide");
                    } else {
                        $("#reset_passwords_error").text(json.error);
                    }
                }).fail(function (jqXHR, json) {
                    $("#reset_password_error").text("Request failed: " + json)
                });
        }
    });

    $("#add_permission_form").validate({
        rules: {
            permission: {
                required: true
            }
        },
        messages: {
            permission: {
                required: "Select at least one Permission"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: "addPermissions.do",
                cache: false,
                type: "POST",
                data: 'permissions=' + $("#add_permission_form select[name=permissions]").val()
                    + '&userId=' + $("#add_permission_form input[name=userId]").val()
            }).done(function (json) {
                    if (json.success) {
                       $("#add_permission_container").modal("hide");
                    } else {
                        $("#add_permission_error").text(json.error);
                    }
                }).fail(function (jqXHR, json) {
                    $("#add_permission_error").text("Request failed: " + json)
                });
        }
    });

    $("#change_publisher_for_user_form").validate({
        rules: {
            publisher: {
                required: true
            }
        },
        messages: {
            publisher: {
                required: "Select a Publisher"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: "changePublisher.do",
                cache: false,
                type: "POST",
                data: 'publisher=' + $("#change_publisher_for_user_form select[name=publisher]").val()
                    + '&userId=' + $("#change_publisher_for_user_form input[name=userId]").val()
            }).done(function (json) {
                    if (json.success) {
                        $("#change_publisher_for_user").modal("hide");
                    } else {
                        $("#change_publisher_for_user_error").text(json.error);
                    }
                }).fail(function (jqXHR, json) {
                    $("#change_publisher_for_user_error").text("Request failed: " + json)
                });
        }
    });

});

function completePermissions() {
    $.getJSON("getPermissions.do", {userId: $("#add_permission_form input[name=userId]").val()}, function (j) {
        var options = '';
        for (var i = 0; i < j.data.length; i++) {
            options += '<option ' + ((j.data[i].hasPermission == true) ? "selected" : "") + ' value="' + j.data[i].id + '">' + j.data[i].permission + '</option>';
        }
        $("select#permission").html(options);
    })
}

function completeAvailablePermissions() {
    $.getJSON("getAvailablePermissions.do", {userId: $("#add_permission_form input[name=userId]").val()}, function (j) {
        var options = '';
        for (var i = 0; i < j.data.length; i++) {
            options += '<option  value="' + j.data[i].permission + '">' + j.data[i].permission + '</option>';
        }
        $("select#add_user_form_permissions").html(options);
    })
}

function completePublishers() {
    $.getJSON(Back.hostUrl + "/admin/getPublishers.do", function (j) {
        var options = '<option  value=""></option>';
        for (var i = 0; i < j.data.length; i++) {
            options += '<option  value="' + j.data[i].name + '">' + j.data[i].name + '</option>';
        }

        $("select#publisherSelect").html(options);
    })
}

function completeProviders() {
    $.getJSON(Back.hostUrl + "/admin/getProviders.do", function (j) {
        var options = '<option  value=""></option>';
        for (var i = 0; i < j.data.length; i++) {
            options += '<option  value="' + j.data[i].name + '">' + j.data[i].name + '</option>';
        }

        $("select#add_game_provider").html(options);
    })
}
