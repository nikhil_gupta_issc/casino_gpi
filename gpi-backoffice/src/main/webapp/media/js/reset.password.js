$(function () { 	
	$("#resetPasswordForm").validate({
		  rules: {
		    password: {
			  required: true,
			  minlength: 4
			},
			password_again: {
		      equalTo: "#password"
		    }
		  },
		  messages: {
				password: {
				  required: "Enter new password",
				  minlength: "Must be at least 4 characters long"
				},
				password_again: {
					equalTo: "Passwords don't match"
				}
		  },
		  submitHandler: function (e) {
			  	$("#resetPasswordForm").submit();
		  }
		});
});