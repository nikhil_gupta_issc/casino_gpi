$(function () { 
	$("#loginForm").validate({
		  rules: {
		    j_username: {
		      required: true,
		      minlength: 2
		    },
		    j_password: {
			  required: true,
			  minlength: 2
			}
		  },
		  messages: {
		    j_username: {
		      required: "Enter your username",
		      minlength: "At least 2 characters are necessary"
		    },
		    j_password: {
			  required: "Enter your password",
			  minlength: "At least 2 characters are necessary"
			}
		  },
		  submitHandler: function (form,  e) {
			  	e.preventDefault();
				$.ajax({
				  url: "login.do",
				  cache: false,
				  type: "POST",
				  data: $("#loginForm").serialize(),
				  dataType: "json" 
				}).done(function( json ) {
					if (json.success) {
						$("#loginContainer").hide();
						$("#logoutContainer").show();
						$("#wellcomeName").html($("#loginForm #loginUserName").val());
						$(document).trigger("Login");
					} else {
						alert( "Invalid credentials" );
					}
				}).fail(function(jqXHR, json) {
					alert( "Request failed: " + json );
				});
		  }
		});
	$("#logout").click(function (e) {
		e.preventDefault();
		$.ajax({
			  url: "logout.do",
			  cache: false,
			  type: "POST",
			  dataType: "json" 
			}).done(function( json ) {
				if (json.success) {
					$("#loginContainer").show();
					$("#logoutContainer").hide();
					$(document).trigger("Logout");
				} else {
					alert("there was an error in the logout process");
				}
			}).fail(function(jqXHR, json) {
				alert( "Request failed: " + json );
			});
	});
	
	$("#resetPasswordAsk").click(function (e) {
		e.preventDefault();
		$( "#resetPasswordForm" ).dialog({
		      height: 150,
		      width: 400,
		      modal: true
		    });	
		
	});
	$("#resetPasswordForm").validate({
		 rules: {
			email: {
			      required: true,
			      minlength: 2,
			      email:true
			}
		 },
		 messages: {
		    email: {
		      required: "Enter your username",
		      minlength: "At least 2 characters are necessary"
		    }
		 },
		 submitHandler: function (form, e) {
		  	e.preventDefault();
			$( "#resetPasswordForm" ).dialog("close");
			$( "#resetMessageContainer" ).dialog({
			      height: 200,
			      width: 200,
			      modal: true
			    });	
			$.ajax({
				  url: "reset/email.do",
				  cache: false,
				  type: "POST",
				  data: $("#resetPasswordForm").serialize(),
				  dataType: "json" 
				}).done(function( json ) {
					if (json.success) {
						$("#resetMessageContainer").show();
					} else {
						alert("there was an error in the logout process");
					}
				}).fail(function(jqXHR, json) {
					alert( "Request failed: " + json );
				});
			}
	});
        $("#spinBet").click(function (e) {
            e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "/testing/spinBet.do",
                    cache: false,
                    data: 'token=' + $("#token").val() 
                            + "&amount=" + $("#amount").val()
                            + "&denomination=" + $("#denomination").val()
                            + "&stakes=" + $("#stakes").val()
                            + "&tickets=" + $("#tickets").val(),
                    success: function(response) {
                        $('#result').html("");
                        var obj = JSON.parse(response);
                        $('#result').html("Success:- " + obj.success + "</br>Balance:- " + obj.message + "</br>Token- " + obj.token) ;
                    },
                    error: function() {
                        alert('Error while request..');
                    }
                });
            });
            $("#requestGame").click(function(e) {
        e.preventDefault();
        $.ajax({
            type: "post",
            url: "/testing/requestGame.do",
            cache: false,
            data: 'game=' + $("#game").val() + "&token=" + $("#token").val()
            + "&pp=" + $("#pp").val() + "&pn=" + $("#pn").val()+ "&lang=" + $("#lang").val(),
            success: function(response) {
                $('#result').html("");
                var obj = JSON.parse(response);
                $('#result').html("Success:- " + obj.success + "</br>Message:- "
                        + obj.message + "</br>Token:- " + obj.token + "</br>Player: - " + obj.player.playerName
                        + "</br>Credits: - " + obj.credits
                        + "</br>Tickets: - " + obj.tickets
                        );
                $('#token').html("tito");
            },
            error: function() {
                alert('Error while request..');
            }
        });
    });
});
