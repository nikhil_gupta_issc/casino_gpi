$(function () {

    $('#table_container').jtable({
        title: 'User List',
        paging: true,
        pageSize: 10,
        sorting: true,
        multiSorting: true,
        selecting: true,
        multiselect: false,
        defaultSorting: 'id DESC',
        actions: {
            listAction: Back.hostUrl +'/free-spin/users.do'
        },
        toolbar: {
            items: [
                {
                    text: 'Assign',
                    click: function () {

                        $('#change_player_container').modal('show');

                        $("#change_player_form").validate({
                            rules: {},
                            messages: {},
                            submitHandler: function (e) {
                                $.ajax({
                                    url: Back.hostUrl +"/free-spin/users/assign.do",
                                    cache: false,
                                    type: "POST",
                                    data: {
                                        cents: $("#cents").val(),
                                        amount: $("#amount").val(),
                                        userName: $("#playersId").val(),
                                        game: $("#games").val()
                                    }
                                }).done(function (json) {
                                    $('#change_player_container').modal('hide');
                                    $('#table_container').jtable('load');
                                    if (json.error) {
                                        alert("Error Code: " + json.errorCode);
                                    } else {
                                        $("#packageAmount").html(json.freeSpin.spinPackage.totalSpin);
                                        $("#packageAmountLeft").html(json.freeSpin.spinPackage.spinLeft);
                                    }
                                }).error(function (json) {
                                    $('#change_player_container').modal('hide');
                                    $('#table_container').jtable('load');
                                    alert("unknown error");
                                })


                            }
                        });
                    }
                }
            ]
        },
        fields: {
            id: {
                key: true,
                title: 'id'
            },
            player: {
                title: 'player'
            },
            amount: {
                title: 'amount'
            },
            game: {
                title: 'Game'
            }
        }
    });

    $('#table_container').jtable('load');

    $("#package").change(function () {
        window.location.href = Back.hostUrl + '/free-spin/assign.do?id=' + $("#package").val();
    });

/*    $("#playerName").autocomplete({
        source: Back.hostUrl +"/admin/players-suggest.do",
        minLength: 2,
        selected: function (event, ui) {
            log(ui.item ?
            "Selected: " + ui.item.name :
            "Nothing selected, input was " + this.value);
        }
    });*/


    initializeBotPlayers();


    function initializeBotPlayers() {

        var playersIdSelect = $("#playersId").select2({
            allowClear: true,
            dropdownCssClass: 'df-select2-dropdown-no-search',
            width: '100%',
            formatResult: function(item){return item.name; },
            formatSelection: function(item){return item.name; },
            closeOnSelect : false,
            placeholder : "Player names",
            minimumInputLength: 1,
            escapeMarkup: function (m) { return m; },
            multiple: true,
            query: function(query) {
                $.ajax({
                    url : Back.hostUrl +"/admin/players-suggest.do",
                    dataType : "json",
                    type : "GET",
                    data: {
                        term: query.term
                    },
                    success: function(data) {
                        var results = [];
                        for (var i in data){
                            results.push({ id: data[i].id, name : data[i].value});
                        };
                        query.callback({results: results});
                    }
                });
            }
        });
    }

});