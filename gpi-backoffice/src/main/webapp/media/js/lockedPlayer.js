$(function () {

    $('#locked_player_table_container').ready(function () {
        $('#locked_player_table_container').jtable({
            title: 'Players List',
            sorting: true,
            multiSorting: true,
            selecting: true,
            multiselect: true,
            actions: {
                listAction: Back.hostUrl +'/admin/lockedPlayers.do'
            },
            toolbar: {
                items: [
                    {
                        text: 'Unlock Players',
                        click: function () {
                            var $selectedRows = $("#locked_player_table_container").jtable('selectedRows');
                            if ($selectedRows.length > 0) {
                                var ids = [];
                                $selectedRows.each(function () {
                                    var record = $(this).data('record');
                                    ids.push(record.id);
                                });
                                $.ajax({
                                    url: Back.hostUrl + "/admin/unlockPlayers.do",
                                    cache: false,
                                    type: "POST",
                                    data: {
                                        playerIds: ids
                                    }
                                }).done(function (json) {
                                    if (json.success) {
                                        $('#locked_player_table_container').jtable('load');
                                    } else {
                                        console.log("Error: " + json.error)
                                    }
                                }).fail(function (jqXHR, json) {
                                    console.log("Request failed: " + json)
                                });
                                console.log(ids);
                            } else {
                                console.log("Nothing selected");
                            }
                        }
                    }
                ]
            },
            fields: {
                id: {
                    key: true,
                    title: 'id'
                },
                name: {
                    title: 'Player Name'
                },
                locked: {
                    type: 'checkbox',
                    values: { 'false': 'Off', 'true': 'On' }
                }
            }

        });

        $('#locked_player_table_container').jtable('load');

    });

});