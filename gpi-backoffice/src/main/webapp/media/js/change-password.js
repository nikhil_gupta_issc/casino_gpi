$(function() {
    $("#change_password_form").validate({
        rules: {
            currentPassword: {
                required: true,
                minlength: 6
            },
            newPassword: {
                required: true,
                minlength: 6
            },
            newPasswordConfirmation: {
                required: true,
                equalTo: "#new_password"
            }
        },
        messages: {
            currentPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            newPassword: {
                required: "Enter your new password",
                minlength: "Must be at least 6 characters long"
            },
            newPasswordConfirmation: {
                equalTo: "Passwords don't match"
            }
        },
        submitHandler: function(e) {
            $.ajax({
                url: Back.hostUrl + "/admin/changePassword.do",
                cache: false,
                type: "POST",
                data: $("#change_password_form").serialize(),
                dataType: "json"
            }).done(function(json) {
                if (json.success) {
                    $("#change_password_form input[name=currentPassword]").val("");
                    $("#change_password_form input[name=newPassword]").val("");
                    $("#change_password_form input[name=newPasswordConfirmation]").val("");
                    $("#reset_password_error").text("");
                    $("#change_password_container").modal('hide');
                } else {
                    $("#reset_password_error").text(json.error);
                }
            }).fail(function(jqXHR, json) {
                $("#reset_password_error").text("Request failed: " + json.error)
            });
        }
    });
});