$(function () {
    $('#edit_publisher_container').on('hidden.bs.modal', function (e) {
        $("#edit_publisher_form input").val("").removeClass('error');
        $("#change_url_error").text("");
        $('label.error').remove();
    });

    $('#edit_currency_deno_container').on('hidden.bs.modal', function (e) {

    });

    $('#addSelectedGamesButton').click(function (e) {
        e.preventDefault();
        $("#add_facebook_games_form").submit();
    });


    $("#add_publisher_form").validate({
        rules: {
            publisherName: {
                required: true
            },
            publisherPassword: {
                required: true,
                minlength: 4
            },
            pPasswordConfirmation: {
                required: true,
                equalTo: "#publisherPassword"
            },
            apiUsername: {
                required: true
            },
            url: {
            },
            apiName: {
                required: true
            },
            ownerId: {
                
            },
            ortizCasinoNumber: {
                
            },
            publisher: {
                required: true
            },
            supportZeroBet: {
            	required: true
            },
            supportZeroWin: {
            	required: true
            }
        },
        messages: {
            userName: {
                required: "Enter your userName"
            },
            publisherPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            pPasswordConfirmation: {
                equalTo: "Passwords don't match"
            },
            apiName: {
                required: "ApiName value must be set"
            },
            ownerId: {
                required: "ApiName value must be set"
            },
            ortizCasinoNumber: {
                 required: "ApiName value must be set"
            },
            publisher: {
                required: "Publisher must be set"
            },
            partnerCode: {
            	
            },
            supportZeroBet: {
            	required: "Zero bet must be set"
            },
            supportZeroWin: {
            	required: "Zero win must be set"
            }
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().parent()).addClass('col-xs-9 col-xs-offset-3');
        },
        submitHandler: function (e) {
        	$(this.currentForm).find('button[type=submit]').prop('disabled', true);
        	$.ajax({
                url: "createPublisher.do",
                cache: false,
                type: "POST",
                data: $("#add_publisher_form").serialize(),
                dataType: "json"
            }).done(function (json) {
            	$(this.currentForm).find('button[type=submit]').removeProp('disabled');
            	if (json.success) {
                    $("#add_publisher_form input[name=publisherName]").val("");
                    $("#add_publisher_form input[name=publisherPassword]").val("");
                    $("#add_publisher_form input[name=pPasswordConfirmation]").val("");
                    //$("#add_publisher_form").text("");
                    $('#publisher_table_container').jtable('load');
                    $("#add_publisher_container").modal("hide");
                } else {
                    $("#add_publisher_error").text("Request failed: " + json.Error);
                }
            }).fail(function (jqXHR, json) {
                $("#add_publisher_error").text("Request failed: " + json)
            });
        }
    });

    $('#publisher_table_container').ready(function () {
        $('#publisher_table_container').jtable({
            title: 'Publisher List',
            paging: true,
            pageSize: 10,
            sorting: true,
            multiSorting: true,
            selecting: true,
            multiselect: false,
            defaultSorting: 'id DESC',
            actions: {
                listAction: 'publisher.do'
            },
            toolbar: {
                items: [
                    {
                        text: 'Edit',
                        click: function () {
                            var selectPublisher = $("#publisher_table_container").jtable('selectedRows').first();
                            var record = selectPublisher.data('record');

                            if (!record) {
                                alert("You have to select a Publisher");
                                return;
                            }
                            $("#edit_publisher_container").modal('show');
                            $("#edit_publisher_form input[name=url]").val(record.url);
                            $("#edit_publisher_form input[name=publisherName]").val(record.name);
                            $("#edit_publisher_form input[name=newPublisherName]").val(record.name);
                            $("#edit_publisher_form input[name=apiUsername]").val(record.apiUsername);
                            $("#edit_publisher_form input[name=apiName]").val(record.apiName);
                            $("#edit_publisher_form input[name=publisherId]").val(record.id);
                            $("#edit_publisher_form input[name=ownerId]").val(record.ownerId);
                            $("#edit_publisher_form input[name=ortizCasinoNumber]").val(record.ortizCasinoNumber);
                            $("#edit_publisher_form input[name=partnerCode]").val(record.partnerCode);
                            $("#edit_publisher_form select[name=supportZeroBet] option[value='" + record.supportZeroBet + "']").prop('selected', true);
                            $("#edit_publisher_form select[name=supportZeroWin] option[value='" + record.supportZeroWin + "']").prop('selected', true);
                            
                            $("#gate option[value='Gateway 2']").attr("selected", true);

                            $('#publisher_table_container').jtable('load');

                            $.getJSON(Back.hostUrl + "/admin/publisherExtras.do?publisherName=" + record.name, function (j) {
                                $("#edit_publisher_extras_form input[name=jackpotEnabled]").prop('checked', j.data.jackpotEnabled);
                                $("#edit_publisher_extras_form input[name=unifiedJackpot]").prop('checked', j.data.jackpotEnabled);
                                $("#edit_publisher_extras_form input[name=fitToScreen]").prop('checked', j.data.fitToScreen);
                            })
                            $("#add_publisher_extras_error").text("");

                        }
                    },{
                        text: 'Add Publisher',
                        click: function () {
                            $("#add_publisher_container").modal('show');
                            $('#publisher_table_container').jtable('load');
                        }
                    }
                ]
            },
            fields: {
                id: {
                    key: true,
                    title: 'id'
                },
                name: {
                    title: 'Name'

                },
                apiName: {
                    title: 'ApiName'

                },
                url: {
                    title: 'URL'

                },
                apiUsername: {
                    title: 'Api Username'

                },
              
                ownerId: {
                    title: 'Home URL'

                },
                
                ortizCasinoNumber: {
                    title: 'Ortiz casino Id'

                },
                partnerCode: {
                	title: 'Partner code'
                },
                createdOn: {
                    title: 'Created On'
                },
    			games: {					
    				sorting: false,
    				width: '2%',
    				display: gamesByProviderJT,
    			},
    			/*addAllGameMappings: {
                    title: '',
                    width: '1%',
                    sorting: false,
                    create: false,
                    edit: false,
                    list: true,
                    display: addAllGameMappingsBtn
                },
    			deleteAllGameMappings: {
                    title: '',
                    width: '1%',
                    sorting: false,
                    create: false,
                    edit: false,
                    list: true,
                    display: deleteAllGameMappingsBtn
                } */   			
            }});

        $('#publisher_table_container').jtable('load');

    });


    $("#change_pub_password_form").validate({
        rules: {
            currentPassword: {
                required: true,
                minlength: 6
            },
            newPassword: {
                required: true,
                minlength: 6
            },
            newPasswordConfirmation: {
                required: true,
                equalTo: "#newPassword"
            }
        },
        messages: {
            currentPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            newPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            newPasswordConfirmation: {
                equalTo: "Passwords don't match"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: "changePubPassword.do",
                cache: false,
                type: "POST",
                data: $("#change_pub_password_form").serialize(),
                dataType: "json"
            }).done(function (json) {
                if (json.success) {
                    $("#change_pub_password_form input[name=currentPassword]").val("");
                    $("#change_pub_password_form input[name=newPassword]").val("");
                    $("#change_pub_password_form input[name=newPasswordConfirmation]").val("");
                    $("#reset_password_error").text("");
                    $("#change_pub_password_container").modal("hide");
                } else {
                    $("#reset_pub_password_error").text(json.success);
                }
            }).fail(function (jqXHR, json) {
                $("#reset_password_error").text("Request failed: " + json)
            });
        }
    });

    $("#edit_publisher_form").validate({
        rules: {
        },
        messages: {
        },
        submitHandler: function (e) {
            $.ajax({
                url: "editPublisher.do",
                cache: false,
                type: "POST",
                data: {
                    url: $("#edit_publisher_form input[name=url]").val(),
                    publisherName: $("#edit_publisher_form input[name=publisherName]").val(),
                    apiName: $("#edit_publisher_form input[name=apiName]").val(),
                    apiUsername: $("#edit_publisher_form input[name=apiUsername]").val(),
                    publisherId: $("#edit_publisher_form input[name=publisherId]").val(),
                    ownerId: $("#edit_publisher_form input[name=ownerId]").val(),
                    ortizCasinoNumber: $("#edit_publisher_form input[name=ortizCasinoNumber]").val(),
                    partnerCode: $("#edit_publisher_form input[name=partnerCode]").val(),
                    supportZeroBet: $("#edit_publisher_form select[name=supportZeroBet]").val(),
                    supportZeroWin: $("#edit_publisher_form select[name=supportZeroWin]").val()
                }
            }).done(function (json) {
                if (json.success) {
                    $("#edit_publisher_form input[name=url]").val("");
                    $("#change_url_error").text("");
                    $("#edit_publisher_container").modal("hide");
                } else {
                    $("#change_url_error").text(json.error);
                }
            }).fail(function (jqXHR, json) {
                $("#change_url_error").text("Request failed: " + json)
            });
        }
    });

    $("#edit_publisher_extras_form").validate({
        rules: {
        },
        messages: {
        },
        submitHandler: function (e) {
            $.ajax({
                url: "editPublisherExtras.do",
                cache: false,
                type: "POST",
                data: {
                    jackpotEnabled: $("#edit_publisher_extras_form input[name=jackpotEnabled]").prop('checked'),
                    fitToScreen: $("#edit_publisher_extras_form input[name=fitToScreen]").prop('checked'),
                    unifiedJackpot: $("#edit_publisher_extras_form input[name=unifiedJackpot]").prop('checked'),
                    publisherName: $("#edit_publisher_form input[name=publisherName]").val()
                }
            }).done(function (json) {
                if (json.success) {

                    $("#add_publisher_extras_error").text("The info was saved successfully");
                } else {
                    $("#add_publisher_extras_error").text(json.error);
                }
            }).fail(function (jqXHR, json) {
                $("#add_publisher_extras_error").text("Request failed: " + json)
            });
        }
    });
    
    function gamesByProviderJT(publisherData){
	    //Create an image that will be used to open child table
	    var $img = $('<button title="List games"><img src="/media/images/icon/game_16.png" style="cursor: pointer;" /></button>');
	    //Open child table when user clicks the image
	    $img.click(function () {
	         $('#publisher_table_container').jtable('openChildTable',
	                $img.closest('tr'),
	                {
	                    title: publisherData.record.name + ' - Games',
	                    paging: true,
	                    pageSize : 100,
	                    sorting: true,
	                    defaultSorting: 'game.gameProvider.name ASC, game.name ASC',
	                    actions: {
	                    	listAction: function (postData, jtParams) {
	                    	    return $.Deferred(function ($dfd) {
	                    	        $.ajax({
	                    	            url: '/admin/getGameMappings.do?publisher=' + publisherData.record.name + "&jtStartIndex=" + jtParams.jtStartIndex + '&jtPageSize=' + jtParams.jtPageSize + '&jtSorting=' + jtParams.jtSorting,
	                    	            type: 'POST',
	                    	            dataType: 'json',
	                    	            data: postData,
	                    	            success: function (data) {
	                    	                $dfd.resolve(data);
	                    	            },
	                    	            error: function () {
	                    	                $dfd.reject();
	                    	            }
	                    	        });
	                    	    });
	                    	},
	                    	createAction: function (postData, jtParams) {
	                        	var formData = getVars(postData);
	                        	
	                            if($('#input-image').val() !== ""){
	                                formData.append("file", $('#input-image').get(0).files[0]);
	                            }
	
	                            return $.Deferred(function ($dfd) {
	                                $.ajax({
	                    	            url: '/admin/gameMappings/create.do?publisherId=' + publisherData.record.id,
	                                    type: 'POST',
	                                    dataType: 'json',
	                                    data: formData,
	                                    processData: false, // Don't process the files
	                                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	                                    success: function (data) {
	                                        $dfd.resolve(data);
	                                        $('#publisher_table_container').jtable('load');
	                                    },
	                                    error: function (data) {
	                                        alert(data.statusText);
	                                    	$dfd.reject();
	                                    }
	                                });
	                            });
	                        },
	                        updateAction: function (postData, jtParams) {
	                        	var formData = getVars(postData);
	                        	
	                            if($('#input-image').val() !== ""){
	                                formData.append("file", $('#input-image').get(0).files[0]);
	                            }
	
	                            return $.Deferred(function ($dfd) {
	                                $.ajax({
	                    	            url: '/admin/gameMappings/update.do?publisherId=' + publisherData.record.id,
	                                    type: 'POST',
	                                    dataType: 'json',
	                                    data: formData,
	                                    processData: false, // Don't process the files
	                                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	                                    success: function (data) {
	                                        $dfd.resolve(data);
	                                        $('#publisher_table_container').jtable('load');
	                                    },
	                                    error: function (data) {
	                                        alert(data.statusText);
	                                    	$dfd.reject();
	                                    }
	                                });
	                            });
	                        },
	                        deleteAction: '/admin/gameMappings/delete.do',
	                    },
	                    toolbar: {
	                        items: [
	                            {
	                                text: 'Add provider records',
	                                click: function () {
	                                	addAllGamesMappingsDialog({record : { id: publisherData.record.id }});
	                                }
	                           },
	                           {
	                                text: 'Delete provider records',
	                                click: function () {
	                                	deleteAllGamesMappingsDialog({record : { id: publisherData.record.id }});
	                                }
	                           },
	                            ]
	                    },
	                    /*recordsLoaded: function(event, data) {
	                        var rowCount = data.records.length;
	                        if (rowCount>0){
	                            if ($(this).find('[name="addProviderGames"]').length == 0){
	                            	$(this).find('.jtable-toolbar').append('<span class="jtable-toolbar-item jtable-toolbar-item-add-record" style="overflow: hidden;"><span class="jtable-toolbar-item-icon"></span><span name="addProviderGames" class="jtable-toolbar-item-text">Add provider records</span></span>');
	                            	$(this).find('[name="addProviderGames"]').click(function(){
	                                	addAllGamesMappingsDialog({record : { id: data.records[0]["publisher.id"] }});
	                                });                                    	
	
	                                $(this).find('.jtable-toolbar').append('<span class="jtable-toolbar-item jtable-toolbar-item-add-record" style="overflow: hidden;"><span class="jtable-toolbar-item-icon"></span><span name="deleteProviderGames" class="jtable-toolbar-item-text">Delete provider records</span></span>');
	                                $(this).find('[name="deleteProviderGames"').click(function(){
	                                	deleteAllGamesMappingsDialog({record : { id: data.records[0]["publisher.id"] }});
	                                });
	                            }
	                        }
	                    },*/
	                    fields: {
	                        "publisher.id": {
	                            type: 'hidden',
	                            defaultValue: publisherData.record.id
	                        },
	                        id: {
	                            key: true,
	                            create: false,
	                            edit: false,
	                            list: false,
	                            sorting: false,
	                        },
	                        "game.gameProvider.id": {
	                            create: false,
	                            edit: false,
	                            list: false,
	                            sorting: true,
	                        },
	                        "game.gameProvider.name": {
	                        	create: false,
	                        	edit: false,
	                        	title: 'Provider',
	                            width: '30%',
	                        },
	                        "game.id": {
	                            create: true,
	                            edit: true,
	                            list: false,
	                            sorting: true,
	                            options:  '/admin/getAllGamesJTOptions.do',
	                        },
	                        "game.name": {
	                        	create: false,
	                        	edit: false,
	                        	list: true,
	                        	title: 'Game',
	                            width: '30%',
	                        },
	                        free: {
	                            title: 'Free',
	                            width: '30%',
	                            options: { 'true': 'Yes', 'false': 'No'}
	                        },
	                        charged: {
	                            title: 'Charged',
	                            width: '30%',
	                            options: { 'true': 'Yes', 'false': 'No'}
	                        },
	                        thumb_url: {
	                            title: 'Image',
	                            type: 'file',
	                            create: false,
	                            edit: true,
	                            list: true,
	                            display: function(data){
	                                if (data.record.thumb_url){
	                                	return '<img src="' + data.record.thumb_url +  '" width="50" height="50" class="preview">';
	                                }
	                            	return "";
	                            },
	                            input: function(data){
	                            	 if (data.record.thumb_url){
	                            		 return '<img src="' + data.record.thumb_url +  '" width="150" height="150" class="preview">';
	                            	 }
	                            	 return "";
	                            },
	                            width: "150",
	                            listClass: "class-row-center"
	                        },
	                        image: {
	                            title: 'Select File',
	                            list: false,
	                            create: true,
	                            input: function(data) {
	                                html = '<input type ="file" id="input-image" name="userfile" accept="image/*" />';
	                                return html;
	                            }
	                        },                           
	                    }
	                }, function (data) { //opened handler
	                    data.childTable.jtable('load');
	                });
            });
        //Return image to show on the person row
        return $img;
    }
    
    // Read a page's GET URL variables and return them as an associative array.
    function getVars(url)
    {
        var formData = new FormData();
        var split;
        $.each(url.split("&"), function(key, value) {
            split = value.split("=");
            formData.append(split[0], decodeURIComponent(split[1].replace(/\+/g, " ")));
        });

        return formData;
    }
    
    function addAllGameMappingsBtn(publisherData){
    	
        var $btn = $('<button title="Add Provider Game Mappings"><img src="/media/images/icon/game_16.png" style="cursor: pointer;" /></button>');

        $btn.click(function () {
        	addAllGamesMappingsDialog(publisherData);
        });
        
        return $btn;
    }
    
    function deleteAllGameMappingsBtn(publisherData){
        
        var $btn = $('<button title="Delete Provider Game Mappings"><img src="/media/images/icon/game_delete.png" style="cursor: pointer;" /></button>');

        $btn.click(function () {
        	deleteAllGamesMappingsDialog(publisherData);
        });
        
        return $btn;
    }
    
    function addAllGamesMappingsDialog(publisherData){
    	confirmDialog('Add All Game Mappings', function(providerId){
	    	$.ajax({
	            url: "/admin/gameMappings/addAll.do",
	            cache: false,
	            type: "POST",
	            data: {publisherId : publisherData.record.id, providerId: providerId},
	            dataType: "json"
	        }).success(function (json) {
	        	alert("All game mappings had been added");
	        	$('#publisher_table_container').jtable('load');
	        }).fail(function (jqXHR, json) {
	            alert("Request failed: " + json)
	        });
    	});
    }
    
    function deleteAllGamesMappingsDialog(publisherData){
    	confirmDialog('Delete All Game Mappings', function(providerId){
	    	$.ajax({
	            url: "/admin/gameMappings/deleteAll.do",
	            cache: false,
	            type: "POST",
	            data: {publisherId : publisherData.record.id, providerId : providerId},
	            dataType: "json"
	        }).success(function (json) {
	        	alert("All game mappings had been deleted");
	        	$('#publisher_table_container').jtable('load');
	        }).fail(function (jqXHR, json) {
	            alert("Request failed: " + json)
	        });
    	});
    }
    
    function confirmDialog(title, execute) {
        $('<div id="add-delete-games"></div>')
	        .appendTo('body')
	        .html('<div><h5>Game Provider</h5></div>')
	        .append(providers)
	        .dialog({
	            modal: true, title: title, zIndex: 10000, autoOpen: true,
	            width: '300px', resizable: false,
	            buttons: {
	                Save: function () {
	                	$.when(execute($("#add-delete-games").find("#provider").val())).done(function(){
	                		$("#add-delete-games").dialog("close");
	                	});
	                },
	                Cancel: function () {                                                                 
	                    $('body').append('<h1>Confirm Dialog Result: <i>No</i></h1>');
	
	                    $("#add-delete-games").dialog("close");
	                }
	            },
	            close: function (event, ui) {
	                $(this).remove();
	            }
	        });
    };
    
    var providers = null;
    $.ajax({
        url: "/admin/getProviders.do",
        cache: false,
        type: "GET",
        dataType: "json"
    }).success(function (response) {
    	providers = document.createElement("select");
    	providers.id = "provider";
    	providers.name = "provider";
    	$.each(response.data, function(index, value){
    		var option = document.createElement('option');
    		option.value = value.id;
    		option.innerHTML = value.name;
    		providers.appendChild(option);
    	});
    }).fail(function (jqXHR, json) {
        alert("Request failed: " + json)
    });
    
});
