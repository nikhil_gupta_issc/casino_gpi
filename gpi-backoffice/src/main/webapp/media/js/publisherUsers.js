$(function () {

    $('#publisher_user_table_container').ready(function () {
        $('#publisher_user_table_container').jtable({
            title: 'User List',
            paging: true,
            pageSize: 10,
            sorting: true,
            multiSorting: true,
            selecting: true,
            multiselect: false,
            defaultSorting: 'id DESC',
            actions: {
                listAction: Back.hostUrl + '/admin/' + 'publisherUsers.do'
            },
            toolbar: {
                items: [
                    {
                        text: 'Reset Password',
                        click: function () {
                            var selectedUser = $("#publisher_user_table_container").jtable('selectedRows').first();
                            var record = selectedUser.data('record');

                            if (!record) {
                                alert("You have to select a User");
                                return;
                            }

                            $("#reset_passwords_container").modal('show');

                            $("#reset_backoffice_user_passwords_form").find("input[name=userId]").val(record.id);
                        }
                    },
                    {
                        text: 'Change permissions',
                        click: function () {
                            var selectedUser = $("#publisher_user_table_container").jtable('selectedRows').first();
                            var record = selectedUser.data('record');

                            if (!record) {
                                alert("You have to select a User");
                                return;
                            }

                            $("#add_back_user_permission_form input[name=userId]").val(record.id);
                            $("#add_back_user_permission_container").modal('show');
                        }
                    },
                    {
                        text: 'Add new User',
                        click: function () {
                            $("#add_publisher_user_container").modal('show');
                        }
                    }
                ]
            },
            fields: {
                id: {
                    key: true,
                    title: 'id'
                },
                name: {
                    title: 'Name'

                },
                permission: {
                    title: 'Permissions'

                },
                createdOn: {
                    title: 'Created On'

                },
                lastLogin: {
                    title: 'Last Login'
                }
            }});

        $('#publisher_user_table_container').jtable('load');

    });

    $("#add_publisher_user_form").validate({
        rules: {
            userName: {
                required: true,
                minlength: 6
            },
            password: {
                required: true,
                minlength: 6
            },
            passwordConfirmation: {
                required: true,
                minlength: 6,
                equalTo: "#add_publisher_user_form input[name=password]"
            }
        },
        messages: {
            userName: {
                required: "Enter your userName",
                minlength: "Must be at least 6 characters long"
            },
            password: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            passwordConfirmation: {
                equalTo: "Passwords don't match"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: Back.hostUrl + '/admin/' + "createPublisherUser.do",
                cache: false,
                type: "POST",
                data: {
                    userName: $("#add_publisher_user_form input[name=userName]").val(),
                    password: $("#add_publisher_user_form input[name=password]").val(),
                    passwordConfirmation: $("#add_publisher_user_form input[name=passwordConfirmation]").val(),
                    permission: $("#add_publisher_user_form select[name=add_user_form_permissions]").val()
                },
                dataType: "json"
            }).done(function (json) {
                    if (json.success) {
                        $("#add_publisher_user_form input[name=userName]").val("");
                        $("#add_publisher_user_form input[name=password]").val("");
                        $("#add_publisher_user_form input[name=passwordConfirmation]").val("");
                        $("#add_publisher_user_container").modal("hide");
                        $('#publisher_user_table_container').jtable('load');
                    } else {
                        $("#add_publisher_user_error").text(json.error);
                    }
                }).fail(function (jqXHR, json) {
                    $("#add_user_error").text("Request failed: " + json)
                });
        }
    });

    $("#reset_backoffice_user_passwords_form").validate({
        rules: {
            currentPassword: {
                required: true,
                minlength: 6
            },
            newPassword: {
                required: true,
                minlength: 6
            },
            newPasswordConfirmation: {
                required: true,
                equalTo: "#newPassword"
            }
        },
        messages: {
            currentPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            newPassword: {
                required: "Enter your password",
                minlength: "Must be at least 6 characters long"
            },
            newPasswordConfirmation: {
                equalTo: "Passwords don't match"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: "resetPublisherUserPassword.do",
                cache: false,
                type: "POST",
                data: {
                    newPassword: $("#reset_backoffice_user_passwords_form input[name=newPassword]").val(),
                    userId: $("#reset_backoffice_user_passwords_form input[name=userId]").val()
                }
            }).done(function (json) {
                    if (json.success) {
                        $("#reset_backoffice_user_passwords_form input[name=currentPassword]").val("");
                        $("#reset_backoffice_user_passwords_form input[name=newPassword]").val("");
                        $("#reset_backoffice_user_passwords_form input[name=newPasswordConfirmation]").val("");
                        $("#reset_passwords_error").text("");
                        $("#change_passwords_container").dialog("close");
                    } else {
                        $("#reset_passwords_error").text(json.error);
                    }
                }).fail(function (jqXHR, json) {
                    $("#reset_password_error").text("Request failed: " + json)
                });
        }
    });

    $("#add_back_user_permission_form").validate({
        rules: {
            permission: {
                required: true
            }
        },
        messages: {
            permission: {
                required: "Select at least one Permission"
            }
        },
        submitHandler: function (e) {
            $.ajax({
                url: Back.hostUrl + '/admin/' + "addPublisherUserPermissions.do",
                cache: false,
                type: "POST",
                data: {
                    permissions: $("#add_back_user_permission_form select[name=permissions]").val(),
                    userId: $("#add_back_user_permission_form input[name=userId]").val()
                }
            }).done(function (json) {
                    if (json.success) {
                       $("#add_back_user_permission_container").modal("hide");
                        $('#publisher_user_table_container').jtable('load');
                    } else {
                        $("#add_permission_error").text(json.error);
                    }
                }).fail(function (jqXHR, json) {
                    $("#add_permission_error").text("Request failed: " + json)
                });
        }
    });


});
