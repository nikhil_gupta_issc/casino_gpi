$('#round_table_container').ready(function () {
    completeGames();
    completePublishers();
    completeProviders();

    $('#round_table_container').jtable({
        title: 'Round List',
        paging: true,
        pageSize: 10,
        sorting: true,
        multiSorting: true,
        selecting: true,
        multiselect: false,
        defaultSorting: 'id DESC',
        actions: {
            listAction: 'round.do'
        },
        selectionChanged: function () {

        },
        fields: {
            id: {
                key: true,
                title: 'id'
            },
            game: {
                title: 'Game'
            },
            bet_amount: {
                title: 'Bet Amount'

            },
            win_amount: {
                title: 'Winning Amount'
            },
            playerName: {
                title: 'Player Name',
                sorting: false
            },
            gameRoundId: {
                title: 'Game Round Id',
                sorting: false
            },
            createdOn: {
                title: 'Created On',
                display: function (data) {

                    var formattedDate = new Date(data.record.createdOn);
                    return moment(formattedDate).format('YYYY-M-D h:mm:ss a');
                }
            },
            provider: {
                title: 'Provider'
            },
            publisher: {
                title: 'Publisher'
            },
            transactions: {
                title: 'Transactions',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: getTransactions
            }
        },
        recordsLoaded: function (event, data) {
            $('#totalWin').val(data.serverResponse.totalWin);
            $('#totalBet').val(data.serverResponse.totalBet);
        }
    });

    function getTransactions(data) {
        var $img = $("<button title='Transactions' class='jtable-command-button glyphicon glyphicon-search'><span>Transactions</span></button>");

        $img.click(function () {
            var $selectedRows = $('#round_table_container').jtable('allRows');
            $selectedRows.each(function () {
                $('#round_table_container').jtable('closeChildTable', $(this))
            });
            $('#round_table_container').jtable('openChildTable',
                $img.closest('tr'),
                {
                    title: "Transactions of round " + data.record.id,
                    paging: true,
                    pageSize: 10,
                    actions: {
                        listAction: data.record.id + "/transaction.do"
                    },

                    fields: {
                        id: {
                            key: true,
                            title: 'Id',
                            list: false
                        },
                        gameTransactionId: {
                            title: "Game Transaction Id",
                            edit: false
                        },
                        amount: {
                            title: "Amount",
                            edit: false
                        },
                        type: {
                            title: "Type",
                            edit: false
                        },
                        publisherId: {
                            title: "Publisher Transaction id",
                            edit: false
                        },
                        createdOn: {
                            title: "Created on",
                            edit: false
                        },
                        success: {
                            title: "Success",
                            edit: false,
                            type: 'checkbox',
                            values: {'false': 'Fail', 'true': 'Success'}
                        }, refunded: {
                            title: "Refunded",
                            edit: false,
                            type: 'checkbox',
                            values: {'false': 'No', 'true': 'Yes'}
                        }, freeSpin: {
                            title: "Free Spin",
                            edit: false,
                            type: 'checkbox',
                            values: {'false': 'No', 'true': 'Yes'}
                        }
                    }
                }, function (data2) {
                    data2.childTable.jtable('load');
                    if (data.record.hasExternalInfo) {
                        $('.jtable-child-table-container').append("<div class='text-center h4'><a href='#' id='gameTransactionDetail'>Show data provided by game developer</a></div>");
                        $('#gameTransactionDetail').click(function (e) {
                            e.preventDefault();
                            completeGameTransactionDetail(data.record);
                        });
                        $('.jtable-command-button .jtable-close-button').click(function (e) {
                            var $selectedRows = $('#round_table_container').jtable('allRows');
                            $selectedRows.each(function () {
                                $('#round_table_container').jtable('closeChildTable', $(this))
                            });
                        })
                    }
                });
        });
        return $img;

    }

    $('#round_table_container').jtable('load');

    $('#apply_round_filters').click(function (e) {
        e.preventDefault();
        $('#round_table_container').jtable('load', {
            roundId: $('#roundId').val(),
            dateFrom: $('#dateFrom').val(),
            dateUntil: $('#dateUntil').val(),
            playerName: $('#playerName').val(),
            publisher: $('#publisherSelectRound').val(),
            game: $('#gameNameFilterRound').val(),
            currency: $('#currencyFilter').val(),
            provider: $("#providerSelectRound").val()
        });
    });

    function completeGames() {
        $.getJSON(Back.hostUrl + "/admin/getAllGames.do", function (j) {
            var options = '<option  value=""></option>';
            for (var i = 0; i < j.data.length; i++) {
                options += '<option  value="' + j.data[i].id + '">' + j.data[i].game + '</option>';
            }

            $("select#gameNameFilterRound").html(options);
        })
    }

    function completeGameTransactionDetail(record) {
        $("#round_data_container").remove();
        var container = $(".jtable-child-table-container");
        container.after("<tr id='round_data_container'><td colspan='10'><div id='tx_container'></div></td></tr>");

        if (!record) {


            return;
        }
        var roundId = record.id;


        $.ajax({
            type: "post",
            url: roundId + "/gameTransactions.do",
            cache: false,
            success: function (response) {
                $('#tx_container').html("");
                var extraBall = 1,
                    wager = 0,
                    payout = 0,
                    bonusShoot = 1,
                    currency = ''
                hold = 0;


                $.each(response.Records, function (i, record) {
                    //$('#tx_container').append('<p>Type: ' + record.type + ' - extraInfo: ' + JSON.stringify(record.extraInfo) + '</p>');

                    if (record.extraInfo.c) {
                        currency = record.extraInfo.c
                    }


                    var mainTemplate = $("#" + ( ( record.type == 'FREE_SPIN' ) ? 'SPIN' : record.type )).tmpl({
                        type: record.type,
                        extra: record.extraInfo,
                        extraBallNumber: extraBall,
                        bonusShoot: bonusShoot,
                        currency: currency
                    });

                    $('#tx_container').append(mainTemplate);

                    if ((record.type == "SPIN" || record.type == "FREE_SPIN") && record.extraInfo.b != undefined) {
                        currency = record.extraInfo.c
                        $.each(record.extraInfo.b, function (key, ball) {
                            $("#cell_ball" + ball).css({"background-color": "black", "color": "white"})
                        });

                        wager += record.extraInfo.ba;

                    }

                    if (record.type == "EXTRA_BALL" && record.extraInfo.b != undefined) {
                        $("#cell_ball" + record.extraInfo.b).css({"background-color": "blue", "color": "white"});
                        $("#cell_ball" + record.extraInfo.b).append("<span class='extraBallPosition'>" + extraBall + "</span>");
                        wager += record.extraInfo.ebp;
                        extraBall++;
                        record.extraInfo.c = currency;

                    }

                    if (record.type == "WIN") {
                        payout += record.extraInfo.wa;
                        record.extraInfo.c = currency;
                    }
                    if (record.type == "BONUS") {
                        if (record.extraInfo.bc != -1) {
                            payout += record.extraInfo.wa,
                                bonusShoot++
                        }
                    }
                });


                $('#tx_container').append($("#RESULTS").tmpl({
                    wager: wager / 100,
                    payout: payout / 100,
                    hold: (wager - payout) / 100,
                    currency: currency
                }));

                $('#tx_container').append("<div style='clear:both'>&nbsp</div>")

            },
            error: function () {
                alert('Error while request..');
            }
        });
    }

    function completePublishers() {
        if ($("select#publisherSelectRound").length > 0) {

            $.getJSON(Back.hostUrl + "/admin/getPublishers.do", function (j) {
                var options = '<option  value=""></option>';
                for (var i = 0; i < j.data.length; i++) {
                    options += '<option  value="' + j.data[i].id + '">' + j.data[i].name + '</option>';
                }

                $("select#publisherSelectRound").html(options);
            })
        }
    }

    function completeProviders() {
        if ($("select#providerSelectRound").length > 0) {

            $.getJSON(Back.hostUrl + "/admin/getProviders.do", function (j) {
                var options = '<option  value=""></option>';
                for (var i = 0; i < j.data.length; i++) {
                    options += '<option value="' + j.data[i].name + '">' + j.data[i].name + '</option>';
                }

                $("select#providerSelectRound").html(options);
            })
        }
    }

    $("#playerName").autocomplete({
        source: "players-suggest.do",
        minLength: 2,
        selected: function (event, ui) {
            log(ui.item ?
            "Selected: " + ui.item.name :
            "Nothing selected, input was " + this.value);
        }
    });

});





