var Back = Back || {};


var dateFormat = "yy-mm-dd";

$(document).ready(function() {
    completeGames();
	datePickerSetup();

	$('#execute_button').click(function() {
		$('iframe').attr('src', generateReportUrl());
	});

	$('#export_button').click(function() {
		$('iframe').attr('src', generateReportUrl($('#format_select').val()));
	});

    completeCurrencies();
    completeProviders();
    completePublishers();
});

function datePickerSetup() {
	$('.time-filter-group').change(function() {
		var radioButton = $(this);
		$('.time-filter-container').hide('fast');
		$('#' + radioButton.attr('filter-container')).show('slow');
	});
	
	var radioButtons = $("input[name='time_filter']:first");
	
	if(radioButtons.length > 0) {
		radioButtons.click();
		setupHourPicker();
		setupDayPicker();
		setupWeekPicker();
		setupMonthPicker();
		setupYear();
	}
	
	if($('#day_cmp_filter_container').length > 0) {
		setupDayCmpPicker();
	}
}

function setupHourPicker() {
	$('#from_hour_input').datetimepicker({
		dateFormat : dateFormat
	});
	
	$('#to_hour_input').datetimepicker({
		dateFormat : dateFormat
	});
	
	var now = $.datepicker.formatDate(dateFormat, new Date());
	
	$('#from_hour_input').val(now + " 00:00");
	$('#to_hour_input').val(now + " 23:59");
}

function setupDayPicker() {
	$('#from_day_input').datepicker({
		dateFormat : dateFormat
	});
	
	$('#to_day_input').datepicker({
		dateFormat : dateFormat
	});
	
	var now = $.datepicker.formatDate(dateFormat, new Date());
	
	$('#from_day_input').val(now);
	$('#to_day_input').val(now);
}

function setupDayCmpPicker() {
	$('#from_day_cmp_input').datepicker({
		dateFormat : dateFormat
	});
	
	$('#to_day_cmp_input').datepicker({
		dateFormat : dateFormat
	});
	
	var now = $.datepicker.formatDate(dateFormat, new Date());
	
	$('#from_day_cmp_input').val(now);
	$('#to_day_cmp_input').val(now);
}

function setupWeekPicker() {
	var startDate;
	var endDate;

	var selectCurrentWeek = function() {
		window.setTimeout(function() {
			$('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
		}, 1);
	}

	$('.week-picker').datepicker({
		showOtherMonths : true,
		selectOtherMonths : true,
		onSelect : function(dateText, inst) {
			var date = $(this).datepicker('getDate');
			startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
			endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
			$('#from_week_input').val($.datepicker.formatDate(dateFormat, startDate));
			$('#to_week_input').val($.datepicker.formatDate(dateFormat, endDate));

			selectCurrentWeek();
		},
		beforeShowDay : function(date) {
			var cssClass = '';
			if (date >= startDate && date <= endDate)
				cssClass = 'ui-datepicker-current-day';
			return [ true, cssClass ];
		},
		onChangeMonthYear : function(year, month, inst) {
			selectCurrentWeek();
		}
	});

	$('.week-picker .ui-datepicker-calendar tr').live('mousemove', function() {
		$(this).find('td a').addClass('ui-state-hover');
	});

	$('.week-picker .ui-datepicker-calendar tr').live('mouseleave', function() {
		$(this).find('td a').removeClass('ui-state-hover');
	});
}

function setupMonthPicker() {
	$('#month_picker').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
        onClose: function(dateText, inst) { 
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        },
        onChangeMonthYear : function(year, month, inst) {
			var actualMonth = month - 1;
        	var startDate = new Date(year, actualMonth, 1);
        	var endDate = new Date(year, actualMonth, daysInMonth(year, actualMonth));
			$('#from_month_p_input').val($.datepicker.formatDate(dateFormat, startDate));
			$('#to_month_p_input').val($.datepicker.formatDate(dateFormat, endDate));
        }
    });
	
	$('#from_month_picker').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
        onClose: function(dateText, inst) { 
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        },
        onChangeMonthYear : function(year, month, inst) {
			var actualMonth = month - 1;
        	var startDate = new Date(year, actualMonth, 1);
			$('#from_month_input').val($.datepicker.formatDate(dateFormat, startDate));
        }
    });
	
	$('#to_month_picker').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
        onClose: function(dateText, inst) { 
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        },
        onChangeMonthYear : function(year, month, inst) {
			var actualMonth = month - 1;
        	var endDate = new Date(year, actualMonth, daysInMonth(year, actualMonth));
			$('#to_month_input').val($.datepicker.formatDate(dateFormat, endDate));
		}
    });
	
	var now = new Date();
	var startDate = new Date(now.getFullYear(), now.getMonth(), 1);
	var endDate = new Date(now.getFullYear(), now.getMonth(), daysInMonth(now.getFullYear(), now.getMonth()));
	$('#from_month_input').val($.datepicker.formatDate(dateFormat, startDate));
	$('#to_month_input').val($.datepicker.formatDate(dateFormat, endDate));
	$('#from_month_p_input').val($.datepicker.formatDate(dateFormat, startDate));
	$('#to_month_p_input').val($.datepicker.formatDate(dateFormat, endDate));
}

function setupYear() {
	for (var i = new Date().getFullYear(); i > 1990; i--) {
	    $('#year_picker').append($('<option />').val(i).html(i));
	}
}

function daysInMonth(year, month) {
	return 32 - new Date(year, month, 32).getDate();
}

function generateReportUrl(format) {
	var url = 'execute.do?name='+
		Back.Utils.getParameterByName("name");
		

	if (format) {
		url += '&format=' + format;
	}
	
	var timeParams = getTimeParameters();
	if(timeParams) {
		url += '&from=' + encodeURIComponent(timeParams.from);
		url += '&to=' + encodeURIComponent(timeParams.to);
	}

    var playerName = $('#playerName').val();
    if(playerName) {
        url += '&playerName=' + playerName;
    }

    var publisher = $('#publisher_filterSelect').val();
    if(publisher) {
        url += '&publisher=' + publisher;
    }

    var currency = $('#round_currency_filter_select').val();
    if(currency) {
        url += '&currency=' + currency;
    }

    var game = $('#round_game_filter_select').val();
    if(game) {
        url += '&game=' + game;
    }
    var freeChoice= $("#freeModeChoice").is(':checked');
    if(freeChoice) {
        url += '&includeFree=' + freeChoice;
    }
    var provider= $("#round_provider_filter_select").val();
    if(provider) {
        url += '&provider=' + provider;
    }
    
    
    return url;
}

function getTimeParameters() {
	var params = {};

	switch($("input[name='time_filter']:checked").val()) {
		case 'hour':
			params.from = $('#from_hour_input').val();
			params.to = $('#to_hour_input').val();
			break;
		case 'day':
			params.from = $('#from_day_input').val() + " 00:00";
			params.to = $('#to_day_input').val() + " 23:59";
			break;
		case 'week':
			params.from = $('#from_week_input').val() + " 00:00";
			params.to = $('#to_week_input').val() + " 23:59";
			break;
		case 'month':
			params.from = $('#from_month_input').val() + " 00:00";
			params.to = $('#to_month_input').val() + " 23:59";
			break;
		case 'month_p':
			params.from = $('#from_month_p_input').val() + " 00:00";
			params.to = $('#to_month_p_input').val() + " 23:59";
			break;
		case 'year':
			params.from = $('#year_picker').val() + "-01-01 00:00";
			params.to = $('#year_picker').val() + "-12-31 23:59";
			break;
		default:
			params = null;
			break;
	}
	
	return params;
}

function getTimeCmpParameters() {
	var params = {};
	
	params.from = $('#from_day_cmp_input').val();
	params.to = $('#to_day_cmp_input').val();
	
	if(params.from == undefined || params.to == undefined) {
		params = null;
	}
	else {
		params.from += " 00:00";
		params.to += " 23:59";
	}
	return params;
}

function completeGames() {
    if (Back.useFilterByGame) {
        $.getJSON(Back.hostUrl + "/admin/getAllGames.do", function (j) {
            var options = '<option  value=""></option>';
            for (var i = 0; i < j.data.length; i++) {
                options += '<option  value="' + j.data[i].id + '">' + j.data[i].game + '</option>';
            }

            $("select#round_game_filter_select").html(options);
        })
    }
}

function completeCurrencies() {
    $.getJSON(Back.hostUrl + "/admin/getCurrencies.do", function (j) {
        var options = '<option  value=""></option>';
        for (var i = 0; i < j.data.length; i++) {
            options += '<option  value="' + j.data[i].name + '">' + j.data[i].name + '</option>';
        }

        $("select#round_currency_filter_select").html(options);
    })
}

function completeProviders() {
    $.getJSON(Back.hostUrl + "/admin/getAllGameProviders.do", function (j) {
        var options = '<option  value=""></option>';
        for (var i = 0; i < j.data.length; i++) {
            options += '<option  value="' + j.data[i].id + '">' + j.data[i].name + '</option>';
        }

        $("select#round_provider_filter_select").html(options);
    })
}

function completePublishers() {
    $.getJSON(Back.hostUrl + "/admin/getPublishers.do", function (j) {
        var options = '<option  value=""></option>';
        for (var i = 0; i < j.data.length; i++) {
            options += '<option  value="' + j.data[i].id + '">' + j.data[i].name + '</option>';
        }

        $("select#publisher_filterSelect").html(options);
    })
}
