<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <style type="text/css" title="currentStyle">
        @import "${staticUrl}/media/css/jtable.css";
    </style>
    <script type="text/javascript" src="${staticUrl}/media/js/free-spin-assign.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/select2.js"></script>
    <script type="text/javascript">
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
        };

        $.ui.dialog.prototype._allowInteraction = function (e) {
            return !!$(e.target).closest('.ui-dialog, .ui-datepicker, .select2-drop, .select2-input').length;
        };
    </script>
</head>
<body>


<div class="row clearfix">
    <div class="col-md-12 column">
        <div id="table_container"></div>
    </div>
</div>

<div id="change_player_container" class="modal fade" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Choose Player</h4>
            </div>
            <form action="#" id="change_player_form" class="form-horizontal" role="form" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="playersId" class="col-xs-4 control-label">Players</label>

                        <div class="col-sm-8">
                            <input type="hidden" id="playersId" value="" title=""/>
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="col-xs-4 control-label">Amount</label>

                        <div class="col-xs-8">
                            <input class="form-control" name="amount" id="amount" type="number"/>
                        </div>
                    </div>
                    
                    <div class="form-group">

                        <label class="col-xs-4 control-label">Cents</label>

                        <div class="col-xs-8">
                           <select class="form-control" id="cents" name="cents">
		                            
		                            <option value="25">25</option>
		                            
		                    </select>
                        </div>
                    </div>
                    
                    <div class="form-group">

                        <label class="col-xs-4 control-label">Game</label>

                        <div class="col-xs-8">
                            <select class="form-control" id="games" name="games">
		                        <c:forEach items="${games}" var="game">
		                            <option value="${game.name}">${game.name}</option>
		                        </c:forEach>
		                    </select>
                        </div>
                    </div>
                    
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>