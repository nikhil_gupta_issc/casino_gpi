<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Players List</title>
    <style type="text/css" title="currentStyle">
        @import "${staticUrl}/media/css/jtable.css";
    </style>
    <script type="text/javascript" src="${staticUrl}/media/js/player.js"></script>
</head>
<body>
<div id="change_player_container" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Player</h4>
            </div>
            <form action="#" id="change_player_form" class="form-horizontal" role="form" method="post">
                <div class="modal-body">
                    <div class="form-group col-xs-12">
                        <label class="col-xs-4 control-label">Player</label>
                        <div class="col-xs-8">
                            <input disabled="true" class="form-control" name="playerName" id="pn" type="text"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="playerId" value=""/>
                    </div>

                    <div class="form-group col-xs-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="locked"> Locked
                            </label>
                        </div>
                    </div>
                    <div>
                        <label id="change_player_error"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="playerFilters" class="row">
    <form class="form-horizontal" role="form">
        <div class="col-xs-4">
            <div class="form-group">
                <label for="playerName" class="col-xs-4 control-label">Player Name: </label>

                <div class="col-sm-8">
                    <input type="text" id="playerName" class="form-control"/>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <sec:authorize access="hasRole('ADMIN')">
                <div class="form-group">
                    <label for="publisherSelectPlayer" class="col-xs-4 control-label">Choose Publisher</label>

                    <div class="col-sm-8">
                        <select id="publisherSelectPlayer" class="form-control">
                        </select>
                    </div>
                </div>
            </sec:authorize>
        </div>
        <div class="col-xs-2">
            <input type="submit" id="apply_filters" value="Apply Filters" class="btn btn-primary btn-block"/>
            <input type="reset" id="reset" value="Reset" class="btn btn-default btn-block"/>
        </div>
    </form>
</div>
<div id="player_table_container"></div>
</body>
</html>