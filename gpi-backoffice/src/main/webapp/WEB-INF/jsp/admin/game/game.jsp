<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Game List</title>
    <style type="text/css" title="currentStyle">
        @import "${staticUrl}/media/css/jtable.css";
    </style>
    <script type="text/javascript" src="${staticUrl}/media/js/games.js"></script>
</head>
<body>
<div class="modal fade" id="edit_game_container" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Game</h4>
            </div>
            <form action="#" id="edit_game_form" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Name</label>
                        <div class="input-group col-xs-9">
                            <input name="newName" id="newName" class="form-control" type="text"/>
                        </div>
                    </div><div class="form-group">
                        <label class="col-xs-3 control-label">URL</label>
                        <div class="input-group col-xs-9">
                            <input name="newURL" id="editNewURL" class="form-control" type="text"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Provider</label>
                        <div class="input-group col-xs-9">
                            <select name="providerSelect" id="providerSelect" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="editGameName" id="editGameName" value=""/>
                    </div>
                    <div class="form-group">
                        <label id="edit_game_error" class="col-xs-11 col-xs-offset-1"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="games_table_container"></div>
</body>
</html>
