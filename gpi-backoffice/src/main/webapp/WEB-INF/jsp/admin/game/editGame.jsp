<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>

<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.static.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>
<df:setProperty var="staticVersion" name="application.static.version"/>

<df:setUrlProperty var="imageStaticUrl" name="application.image.url" secureName="application.secure.image.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title><spring:message code='edit.game.page.title'/></title>
    <script type="text/javascript" src="${staticUrl}/media/js/libs/jquery.number.min.js?v=${staticVersion}"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/pages/games/editGame.js?v=${staticVersion}"></script>
    <c:if test="${success}">
        <script type="text/javascript">
            $(function () {
                $("#saved-modal").dialog({
                    height: 140,
                    modal: true
                });
            });
        </script>
    </c:if>
</head>
<body>
<ol class="breadcrumb">
    <li><a href="${hostUrl}"><spring:message code='breadcrumb.home'/></a></li>
    <li><a href="${hostUrl}/admin/getGames.do"><spring:message code='breadcrumb.games'/></a></li>
    <c:choose>
        <c:when test="${empty game.id}">
            <li class="active"><spring:message code='breadcrumb.create'/></li>
        </c:when>
        <c:otherwise>
            <li class="active"><spring:message code='breadcrumb.edit'/></li>
        </c:otherwise>
    </c:choose>
</ol>
<form:form modelAttribute="game" action="${hostUrl}/admin/games/edit.do" enctype="multipart/form-data"
           class="form-horizontal" role="form">
    <form:hidden path="id"/>
    <div class="form-group col-lg-12">
        <form:errors path="*" element="div" cssClass="alert alert-danger"/>
    </div>
    <div class="form-group">
        <form:errors path="name" element="div" cssClass="error col-lg-offset-2 col-lg-10"/>
        <form:label path="name" class="col-lg-2 control-label"><spring:message
                code='edit.game.form.label.game.name'/></form:label>
        <div class="col-lg-3">
            <form:input path="name" class="form-control" required="required" maxlength="100"/>
        </div>
    </div>
    <div class="form-group">
        <form:errors path="gameProvider" element="div" cssClass="error col-lg-offset-2 col-lg-10"/>
        <form:label path="gameProvider" class="col-lg-2 control-label"><spring:message
                code='edit.game.form.game.provider'/></form:label>
        <div class="col-lg-3">
            <select id="provider" name="provider" class="df-select2" required="required">

                <c:forEach items="${providers}" var="gameProvider">
                    <c:choose>
                        <c:when test="${actualGameProvider == gameProvider.name}">
                            <option value="${gameProvider.name}" selected="true">${gameProvider.name}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${gameProvider.name}">${gameProvider.name}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </div>
    </div>
    <div class="form-group">
            <label class="col-lg-2 control-label">Free Spins:</label>

            <div class="col-lg-6">
                <form:checkbox path="freespins" name="freespins" id="freespins" />
            </div>
        </div>
    <div class="form-group">
        <form:errors path="url" element="div" cssClass="error col-lg-offset-2 col-lg-10"/>
        <form:label path="url" class="col-lg-2 control-label"><spring:message
                code='edit.game.form.game.url'/></form:label>
        <div class="col-lg-6">
            <form:textarea path="url" class="form-control" maxlength="1000" url="url" placeholder="http://example.com/"
                           rows="4"/>
        </div>
    </div>
    <div class="form-group">
        <form:errors path="description" element="div" cssClass="error col-lg-offset-2 col-lg-10"/>
        <form:label path="description" class="col-lg-2 control-label"><spring:message
                code='edit.game.form.game.description'/></form:label>
        <div class="col-lg-6">
            <form:textarea path="description" class="form-control" maxlength="1000" url="description" placeholder="Add a brief description of the game"
                           rows="4"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label"><spring:message code='edit.game.form.jpg.image'/></label>

        <div>
            <div class="thumbnail col-lg-1">
                <img src="${imageUrl}"  height="70" width="70"/>
            </div>
            <div class="col-lg-3">
                <input name="file" type="file" accept="image/jpeg" class="form-control"/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-3">
            <input type="submit" class="btn btn-primary" value="<spring:message code='edit.game.form.save.button' />"/>
        </div>
    </div>
</form:form>
<c:if test="${success}">
    <div id="saved-modal" title="Saved">
        <p><spring:message code='edit.game.dialog.save.modal'/></p>
    </div>
</c:if>
</body>
</html>