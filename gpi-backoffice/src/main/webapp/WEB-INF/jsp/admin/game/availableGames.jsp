<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>

<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.static.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>
<df:setUrlProperty var="gpiUrl" name="gpi.host.url" secureName="gpi.host.url"/>
<df:setProperty var="staticVersion" name="application.static.version"/>

<df:setUrlProperty var="imageStaticUrl" name="application.image.url" secureName="application.secure.image.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title><spring:message code='edit.game.page.title'/></title>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#urlCreatorCharged").on("click", function () {
                $('.config-url').each(function (i, obj) {
                    var url = obj.textContent;
                    url = url.replace(($("#urlCreatorCharged").prop('checked') ? "type=FREE" : "type=CHARGED"), ($("#urlCreatorCharged").prop('checked') ? "type=CHARGED" : "type=FREE"))
                    obj.textContent = url;
                });
            });
            $("#urlCreatorLang").on("change", function () {
                $('.config-url').each(function (i, obj) {
                    var url = obj.textContent;
                    var select = $("#selectedLanguage");
                    var selectedLang = select.val();
                    var original = "lang=" + select.val();
                    var final = "lang=" + $("#urlCreatorLang").val();
                    url = url.replace(original, final);
                    obj.textContent = url;
                    select.val($("#urlCreatorLang").val());
                });
            });
        });
    </script>
</head>

<a class="btn btn-info" data-toggle="collapse" href="#collapseUrlCreator" aria-expanded="false"
   aria-controls="collapseExample">
    Url Creator
</a>
<input type="text" class="hidden" value="en" id="selectedLanguage">

<div class="row collapse" id="collapseUrlCreator">
    <div class="col-xs-12">
        <div class="form-group">
            <label class="col-xs-1 control-label">Charged:</label>

            <div class="col-xs-2">
                <input name="charged" id="urlCreatorCharged" type="checkbox" checked/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8">
                <label class="col-xs-1 control-label">Language:</label>

                <div class="col-xs-2">

                    <select id="urlCreatorLang" class="df-select2" required="required">
                        <option value="en">EN</option>
                        <option value="es">ES</option>
                        <option value="pt">PT</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<c:forEach items="${games}" var="game">
    <div class="row">

        <div>
            <div class="img-rounded col-lg-2">
                <a href="${imageStaticUrl}/games/gpi-${game.id}.jpg" download="${game.name}.jpg" title="${game.name}">
                    <img src="${imageStaticUrl}/games/gpi-${game.id}.jpg?"
                         onerror='this.src="${imageStaticUrl}/games/default.jpg"' height="100" width="100"/>
                </a>
            </div>
        </div>
        <div class="col-lg-10">
            <div class="col-xs-12"><h3>${game.name} - (${game.gameProvider.name})</h3></div>
            <div class="text-info col-xs-12">${game.description}</div>
            <c:if test="${game.freespins}">
                <div class="text-warning col-xs-12">Free spins available</div>
            </c:if>
                <%--https://pi-test.igamingsolution.com/game.do?token=lm_token&pn=casinoTest&lang=lm_lang&game=shamrockBingo&type=CHARGED&device=desktop--%>
            <div class="text-info col-xs-12">Sample URL: <span
                    class="config-url">${gpiUrl}?token={token}&pn=${publisher.name}&lang=en&game=${game.name}&type=CHARGED</span>
            </div>
        </div>
    </div>
    <hr>
</c:forEach>
</div>
</body>
</html>