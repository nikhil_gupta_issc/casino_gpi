<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/tags/functions.tld" prefix="df"%>

<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.host.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Report List</title>
<link rel="stylesheet" href="${staticUrl}/media/css/jtable.css"/>
<script type="text/javascript" src="${staticUrl}/media/js/jquery.jtable.js"></script>
<script type="text/javascript" src="${staticUrl}/media/js/reports.js"></script>
</head>
<body>
	<div id="table_container"></div>
</body>
</html>
