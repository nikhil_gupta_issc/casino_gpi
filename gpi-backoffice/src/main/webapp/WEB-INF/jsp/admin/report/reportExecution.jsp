<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>

<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Report Execution</title>
    <script type="text/javascript" src="${staticUrl}/media/js/utils.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/reportExecution.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/jquery-ui-timepicker-addon.js"></script>

    <style type="text/css" title="currentStyle">
        @import "${staticUrl}/media/css/jtable.css";
        @import "${staticUrl}/media/css/jquery-ui-timepicker-addon.css";
        @import "${staticUrl}/media/css/reportExecution.css";
    </style>
</head>
<body>
<div id="report_parameters">
    <c:if test="${report.useFilterByHour}">
        <div>
            <label>Filter by Hour</label>
            <input type="radio" name="time_filter" value="hour" class="time-filter-group"
                   filter-container="hour_filter_container"/>
        </div>
    </c:if>
    <c:if test="${report.useFilterByDay}">
        <div>
            <label>Filter by Day</label>
            <input type="radio" name="time_filter" value="day" class="time-filter-group"
                   filter-container="day_filter_container"/>
        </div>
    </c:if>
    <c:if test="${report.useFilterByWeek}">
        <div>
            <label>Filter by Week</label>
            <input type="radio" name="time_filter" value="week" class="time-filter-group"
                   filter-container="week_filter_container"/>
        </div>
    </c:if>
    <c:if test="${report.useMonthPicker}">
        <div>
            <label>Select a Month</label>
            <input type="radio" name="time_filter" value="month_p" class="time-filter-group"
                   filter-container="month_picker_container"/>
        </div>
    </c:if>
    <c:if test="${report.useFilterByMonth}">
        <div>
            <label>Filter by Month</label>
            <input type="radio" name="time_filter" value="month" class="time-filter-group"
                   filter-container="month_filter_container"/>
        </div>
    </c:if>
    <c:if test="${report.useFilterByYear}">
        <div>
            <label>Filter by Year</label>
            <input type="radio" name="time_filter" value="year" class="time-filter-group"
                   filter-container="year_filter_container"/>
        </div>
    </c:if>
    <c:if test="${report.useFilterByHour}">
        <div id="hour_filter_container" class="time-filter-container">
            <label>From</label>
            <input type="text" id="from_hour_input"/>
            <label>To</label>
            <input type="text" id="to_hour_input"/>
        </div>
    </c:if>
    <c:if test="${report.useFilterByDay}">
        <div id="day_filter_container" class="time-filter-container">
            <label>From</label>
            <input type="text" id="from_day_input"/>
            <label>To</label>
            <input type="text" id="to_day_input"/>
        </div>
    </c:if>
    <c:if test="${report.useFilterByWeek}">
        <div id="week_filter_container" class="time-filter-container">
            <div class="week-picker"></div>
            <input type="hidden" id="from_week_input"/>
            <input type="hidden" id="to_week_input"/>
        </div>
    </c:if>
    <c:if test="${report.useFilterByMonth}">
        <div id="month_filter_container" class="time-filter-container">
            <div id="from_month_picker"></div>
            <div id="to_month_picker"></div>
            <input type="hidden" id="from_month_input"/>
            <input type="hidden" id="to_month_input"/>
        </div>
    </c:if>
    <c:if test="${report.useMonthPicker}">
        <div id="month_picker_container" class="time-filter-container">
            <div id="month_picker"></div>
            <input type="hidden" id="from_month_p_input"/>
            <input type="hidden" id="to_month_p_input"/>
        </div>
    </c:if>
    <c:if test="${report.useFilterByYear}">
        <div id="year_filter_container" class="time-filter-container">
            <label>Year</label>
            <select id="year_picker"></select>
        </div>
    </c:if>
    <c:if test="${report.useFilterByGame}">
        <div>
            <label>Game</label>
            <select id="round_game_filter_select">
            </select>
        </div>
    </c:if>
    <c:if test="${report.useFilterByUserName}">
        <div id="playerNameFilter">
            <label>User</label> <input id="playerName" type="text"/>

        </div>
    </c:if>
    <c:if test="${report.useFilterByPublisher}">
        <sec:authorize access="hasRole('AUDIT_ADMIN')">
            <label>Publisher</label>
            <select name="publisher" id="publisher_filterSelect">
            </select>
        </sec:authorize>
    </c:if>
    <c:if test="${report.useFilterByCurrency}">
        <div>
            <div>
                <label>Currency</label>
                <select id="round_currency_filter_select">
                </select>
            </div>
        </div>
    </c:if>
    <c:if test="${report.showFreeGames}">
        <div>
            <div>
                <label>Show FREE mode rounds?</label>
                <input type="checkbox" id="freeModeChoice"/>
            </div>
        </div>
    </c:if>
    <c:if test="${report.useFilterByProvider}">
        <div>
            <div>
                <label>Provider</label>
                <select id="round_provider_filter_select">
                </select>
            </div>
        </div>
    </c:if>
</div>
<div>
    <button id="execute_button" type="button">Execute Report</button>
    <button id="export_button" type="button">Export Report to</button>
    <select id="format_select">
        <option>xls</option>
        <option>pdf</option>
    </select>
</div>
<div id="report_container">
    <iframe style="width: 1030px; height: 800px;">
    </iframe>
</div>
</body>
</html>
