<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib  prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/tags/functions.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Report Creation</title>
</head>
<body>
	<form:form modelAttribute="report" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
		<div class="form-group col-lg-12">
			<form:errors path="*" element="div" cssClass="alert alert-danger"/>
		</div>
		<div class="form-group">
			<form:errors path="name" element="div" cssClass="error col-lg-offset-2 col-lg-10"/>
			<form:label path="name" class="col-lg-2 control-label">Name</form:label>
			<div class="col-lg-3">
				<form:input path="name" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<form:errors path="description" element="div" cssClass="error col-lg-offset-2 col-lg-10"/>
			<form:label path="description" class="col-lg-2 control-label">Description</form:label>
			<div class="col-lg-3">
				<form:input path="description" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-3">
				<div class="checkbox">
					<form:label path="useFilterByHour">Use filter by hour</form:label>
					<form:checkbox path="useFilterByHour" />
				</div>
				<div class="checkbox">
					<form:label path="useFilterByDay">Use filter by day</form:label>
					<form:checkbox path="useFilterByDay" />
				</div>
				<div class="checkbox">
					<form:label path="useFilterByWeek">Use filter by week</form:label>
					<form:checkbox path="useFilterByWeek" />
				</div>
				<div class="checkbox">
					<form:label path="useFilterByMonth">Use filter by month</form:label>
					<form:checkbox path="useFilterByMonth" />
				</div>
				<div class="checkbox">
					<form:label path="useMonthPicker">Use month picker</form:label>
					<form:checkbox path="useMonthPicker" />
				</div>
			</div>
			<div class="col-lg-3">
				<div class="checkbox">
					<form:label path="useFilterByYear">Use filter by year</form:label>
					<form:checkbox path="useFilterByYear" />
				</div>

				<div class="checkbox">
					<form:label path="useFilterByGame">Use filter by Game</form:label>
					<form:checkbox path="useFilterByGame" />
				</div>

			</div>
            <div class="col-lg-3">
                <div class="checkbox">
                    <form:label path="useFilterByUserName">Use filter by Username</form:label>
                    <form:checkbox path="useFilterByUserName" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="checkbox">
                    <form:label path="useFilterByCurrency">Use filter by Currency</form:label>
                    <form:checkbox path="useFilterByCurrency" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="checkbox">
                    <form:label path="showFreeGames">Show Free Games choice</form:label>
                    <form:checkbox path="showFreeGames" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="checkbox">
                    <form:label path="useFilterByProvider">Use filter by Provider</form:label>
                    <form:checkbox path="useFilterByProvider" />
                </div>
            </div>
            <sec:authorize access="hasRole('ADMIN')">
	            <div class="col-lg-3">
	                <div class="checkbox">
	                    <form:label path="useFilterByPublisher">Use filter by Publisher</form:label>
	                    <form:checkbox path="useFilterByPublisher" />
	                </div>
	            </div>
            </sec:authorize>
            <div>

            <form:select path="authorities">
                <option value="PUBLISHER_ADMIN">PUBLISHER_ADMIN</option>
                <option value="REPORT">REPORT</option>
                <option value="ADMIN">ADMIN</option>
            </form:select>
            </div>
		</div>
		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-3">
				<input name="report_file" type="file" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-3">
				<input class="btn btn-primary" type="submit" value="Create Report" />
			</div>
		</div>
	</form:form>
</body>
</html>
