<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Round List</title>
    <style type="text/css" title="currentStyle">
        @import "${staticUrl}/media/css/jtable.css";
        @import "${staticUrl}/media/css/rounds.css";
    </style>
    <script type="text/javascript" src="${staticUrl}/media/js/round.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="${staticUrl}/media/js/jquery.tmpl.min.js"></script>
    <script>
        $(function () {
            var dateFormat = "yy-mm-dd";
            var now = $.datepicker.formatDate(dateFormat, new Date());
            $('#dateFrom').datetimepicker({
                dateFormat: dateFormat
            });

            $('#dateFrom').val(now + " 00:00");

            $('#dateUntil').datetimepicker({
                dateFormat: dateFormat
            });
            $('#dateUntil').val(now + " 23:59");
        });
    </script>
    <script type="text/javascript" src="${staticUrl}/media/js/round.js"></script>
</head>
<body>
<a class="btn btn-info" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    Filters
</a>
<div class="collapse" id="collapseExample">
    <div id="roundFilters">
        <form class="form-horizontal" role="form">
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="roundId" id="roundIdLabel" class="col-xs-4 control-label">Round Id:</label>

                    <div class="col-sm-8">
                        <input type="text" name="roundId" id="roundId" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="dateFrom" class="col-xs-4 control-label">Date from: </label>

                    <div class="col-sm-8">
                        <input type="text" id="dateFrom" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="dateUntil" class="col-xs-4 control-label">Date until: </label>

                    <div class="col-sm-8">
                        <input type="text" id="dateUntil" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="playerName" class="col-xs-4 control-label">Player Name: </label>

                    <div class="col-sm-8">
                        <input type="text" id="playerName" class="form-control"/>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <sec:authorize access="hasAnyRole('ADMIN','TESTER', 'PROVIDER')">
                    <div class="form-group">
                        <label for="publisherSelectRound" class="col-xs-4 control-label">Choose
                            Publisher</label>

                        <div class="col-sm-8">
                            <select id="publisherSelectRound" class="form-control">
                            </select>
                        </div>
                    </div>
                </sec:authorize>
                <div class="form-group">
                    <label for="gameNameFilterRound" class="col-xs-4 control-label">Choose Game</label>

                    <div class="col-sm-8">
                        <select name="game" id="gameNameFilterRound" class="form-control">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="currencyFilter" class="col-xs-4 control-label">Currency</label>

                    <div class="col-sm-8">
                        <select name="currency" id="currencyFilter" class="form-control">
                            <option value=""></option>
                            <option value="USD">USD</option>
                            <option value="BRL">BRL</option>
                            <option value="EUR">EUR</option>
                        </select>
                    </div>
                </div>
                <sec:authorize access="hasAnyRole('ADMIN','TESTER', 'PUBLISHER_ADMIN', 'SUPPORT')">
                    <div class="form-group">
                        <label for="currencyFilter" class="col-xs-4 control-label">Provider: </label>

                        <div class="col-sm-8">
                            <select id="providerSelectRound" class="form-control">
                            </select>
                        </div>
                    </div>
                </sec:authorize>
            </div>
            <div class="col-xs-2">
                <input type="submit" id="apply_round_filters" value="Apply Filters"
                       class="btn btn-primary btn-block"/>
                <input type="reset" id="reset" value="Reset" class="btn btn-default btn-block"/>
            </div>
        </form>
    </div>
</div>

<div id="summary" class="row">
    <hr/>
    <form class="form-horizontal" role="form">
        <div class="row">

            <div class="col-xs-6">
                <div class="form-group">
                    <label for="totalWin" class="col-xs-4 control-label">Total cents won on table:</label>

                    <div class="col-sm-6">
                        <input type="text" id="totalWin" class="form-control" disabled/>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="totalBet" class="col-xs-4 control-label">Total cents bet on table:</label>

                    <div class="col-sm-6">
                        <input type="text" id="totalBet" class="form-control" disabled/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-6">
                <div class="form-group">
                    <label for="absoluteWin" class="col-xs-4 control-label">Total cents won:</label>

                    <div class="col-sm-6">
                        <input type="text" id="absoluteWin" class="form-control" disabled/>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="absoluteBet" class="col-xs-4 control-label">Total cents bet:</label>

                    <div class="col-sm-6">
                        <input type="text" id="absoluteBet" class="form-control" disabled/>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="round_table_container"></div>

<script id="GAME_DATA" type="text/x-jquery-tmpl">
	<div class="data-container">
    	<div class="tickets">

			{{each(i, ticket) extra.t}}
        		 <table>
        		 		<tr>
					{{each(j, val) ticket}}
							{{if j == '5' || j == '10'|| j == '15'|| j == '20'}}
								</tr>
								<tr>
							{{/if}}
							<td id = 'cell_ball\${val}'>\${val}</td>
							{{if j == '25'}}
								</tr>
							{{/if}}

    				{{/each}}
						</tr>
				</table>
    		{{/each}}

		</div>
    	<div></div>
	</div>



</script>

<script id="SPIN" type="text/x-jquery-tmpl">
	<div class="data-container spin">

		<div>
			<label>Type: </label>
			<span>\${type}</span>
		</div>
		<div>
			<label>Jackpot: </label>
			<span>\${extra.j}</span>
		</div>
		<div>
			<label>Jackpot position: </label>
			<span>\${extra.jp}</span>
		</div>
		<div>
			<label>Currency: </label>
			<span>\${extra.c}</span>
		</div>
		<div>
			<label>Community Jackpot: </label>
			<span>\${extra.cp}</span>
		</div>
		<div>
			<label>Denomination: </label>
			<span>\${extra.d / 100}</span>
		</div>
		<div>
			<label>Stakes: </label>
			<span>\${extra.s}</span>
		</div>
		<div>
			<label>Free balls: </label>
			<span>\${extra.fb}</span>
		</div>
		<div>
			<label>Tickets activos: </label>
			<span>\${extra.tp}</span>
		</div>
		<div>
			<label>Extraball disponible: </label>
			<span>\${extra.eb}</span>
		</div>
		<div>
			<label>Jackpot Winning: </label>
			<span>\${extra.jpw}</span>
		</div>
		<div>
			<label>Bet Amount: </label>
			<span>\${extra.ba}</span>
		</div>
	</div>



</script>

<script id="EXTRA_BALL" type="text/x-jquery-tmpl">
	<div class="data-container-small">

			<label>\${extraBallNumber} Extra ball</label>
			<span> Number \${extra.b}  -  Price:
			\${extra.ebp / 100} \${currency}</span>
	</div>



</script>

<script id="BONUS" type="text/x-jquery-tmpl">
        <div class="data-container-small blue">

            {{if extra.bc != -1}}
            <label> Bonus shoot \${bonusShoot}</label>
			<span> Credits \${extra.bc}  -  Price: \${extra.wa}</span>
            {{/if}}
        </div>



</script>
<script id="WIN" type="text/x-jquery-tmpl">
	<div class="data-container win">
		<div>
			<label>Winning: </label>
			<span><b>\${extra.wa / 100 } \${currency}</b> - </span>

			{{each(i, win) extra.price}}
					<span> <i>Pattern:</i> \${win.Pattern}  -  <i>Ticket:</i> \${win.Ticket + 1}  .</span>
			{{/each}}

		</div>
	</div>



</script>

<script id="RESULTS" type="text/x-jquery-tmpl">
	<div class="data-container-small">
		&nbsp;
	</div>
	<div class="data-container-small">
		<label>Wager: </label> <span> \${wager} \${currency}</span>
	</div>
	<div class="data-container-small">
		<label>Payout: </label> <span> \${payout} \${currency}</span>
	</div>
	<div class="data-container-small">
		<label>Hold: </label> <span> \${hold} \${currency}</span>
	</div>



</script>
</body>
</html>