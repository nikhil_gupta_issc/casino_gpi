<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/tags/functions.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>Users List</title>
        <style type="text/css" title="currentStyle">
            @import "${staticUrl}/media/css/jtable.css";
        </style>
        <script type="text/javascript" src="${staticUrl}/media/js/user.util.js"></script>
    </head>
    <body>
        <div id="user_table_container"></div>
        <div id="change_passwords_container" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                    </div>
                    <form action="#" id="change_users_passwords_form" class="form-horizontal" role="form" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">New Password: </label>
                                <div class="col-xs-8">
                                    <input name="newUserPassword" id="newUserPassword" class="form-control" type="text" />
                                </div>
                            </div>
                            <div>
                                <label id="reset_passwords_error"></label>
                            </div>
                            <div>
                                <input type="hidden" name="userId" value=""/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Change</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="add_permission_container" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Permission</h4>
                    </div>
                    <form action="#" id="add_permission_form" class="form-horizontal" role="form" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Permissions</label>
                                <div class="col-xs-8">
                                    <select multiple="true" name="permissions" id="permission" class="form-control">
                                    </select>
                                </div>
                            </div>
                            
                            <div>
                                <label id="add_permission_error"></label>
                            </div>
                            <div>
                                <input type="hidden" name="userId" value=""/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="change_publisher_for_user" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Change Publisher</h4>
                    </div>
                    <form action="#" id="change_publisher_for_user_form" class="form-horizontal" role="form" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Publisher</label>
                                <div class="col-xs-8">
                                    <select name="publisher" id="publisherSelect" class="form-control">
                                    </select>
                                </div>
                            </div>
                            
                            <div>
                                <label id="change_publisher_for_user_error"></label>
                            </div>
                            <div>
                                <input type="hidden" name="userId" value=""/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="add_user_container" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add User</h4>
                    </div>
                    <form action="#" id="add_user_form" method="post" class="form-horizontal" role="form">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Username</label>

                                <div class="col-xs-9">
                                    <input name="userName" id="userName" type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Password</label>

                                <div class="col-xs-9">
                                    <input name="password" id="password" type="password" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Confirm Password</label>

                                <div class="col-xs-9">
                                    <input name="passwordConfirmation" id="password_confirmation" type="password"
                                           class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Permissions</label>

                                <div class="col-xs-9">
                                    <select multiple="true" name="add_user_form_permissions" id="add_user_form_permissions"
                                            class="form-control">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Publisher</label>

                                <div class="col-xs-9">
                                    <select name="publisher" id="publisherSelect" class="form-control"></select>
                                </div>
                            </div>
                            <div>
                                <label id="add_user_error"></label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add User</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </body>
</html>