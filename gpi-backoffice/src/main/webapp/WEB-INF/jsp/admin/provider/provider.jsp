<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Providers List</title>
    <style type="text/css" title="currentStyle">
        @import "${staticUrl}/media/css/jtable.css";
    </style>
    <script type="text/javascript" src="${staticUrl}/media/js/provider.js"></script>
</head>
<body>
<sec:authorize access="hasAnyRole('ADMIN')">
    <div id="add_provider_container" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add New Provider</h4>
                </div>
                <form class="form-horizontal" role="form" action="#" id="add_provider_form" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Provider Name:</label>

                            <div class="col-sm-9">
                                <input name="providerName" id="providerName" class="form-control" type="text"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">loginName:</label>

                            <div class="col-sm-9">
                                <input name="providerLoginName" id="providerLoginName" class="form-control"
                                       type="text"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">password:</label>

                            <div class="col-sm-9">
                                <input name="providerPassword" id="providerPassword" class="form-control" type="text"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Api Name:</label>

                            <div class="col-sm-9">
                                <input name="providerApi" id="providerApi" class="form-control" type="text"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Has extended info:</label>
                            <div>
                                <input name="hasExtendedInfo" id="hasExtendedInfo" type="checkbox"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_provider_container" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Change Provider Name</h4>
                </div>
                <form action="#" id="edit_provider_form" class="form-horizontal" role="form" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Provider name</label>

                            <div class="input-group col-xs-9">
                                <input name="newProviderName" id="newProviderName" class="form-control" type="text"/>

                                <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                   data-trigger="hover" title="Game provider name.">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Provider login name</label>

                            <div class="input-group col-xs-9">
                                <input name="newProviderLoginName" id="newProviderLoginName" class="form-control"
                                       type="text"/>

                                <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                   data-trigger="hover" title="Game provider loginName, used in every gpi message.">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Provider password</label>

                            <div class="input-group col-xs-9">
                                <input name="newProviderPassword" id="newProviderPassword" class="form-control"
                                       type="text"/>

                                <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                   data-trigger="hover" title="Game provider password. Used in every gpi message.">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Provider prefix</label>

                            <div class="input-group col-xs-9">
                                <input name="newProviderPrefix" id="newProviderPrefix" class="form-control"
                                       type="text"/>

                                <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                   data-trigger="hover" title="Game provider prefix. Used in providers that implement gpi api.">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Api Name</label>

                            <div class="input-group col-xs-9">
                                <input name="newProviderApi" id="newProviderApi" class="form-control"
                                       type="text"/>

                                <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                   data-trigger="hover" title="The API used to communicate the game request.">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Has extended info:</label>
                            <div>
                                <input name="hasExtendedInfoEdit" id="hasExtendedInfoEdit" type="checkbox"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label id="edit_provider_error" class="col-xs-11 col-xs-offset-1"></label>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="providerName" value=""/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Confirm</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</sec:authorize>
<!-- /.modal -->

<div id="provider_table_container"></div>
</body>
</html>
