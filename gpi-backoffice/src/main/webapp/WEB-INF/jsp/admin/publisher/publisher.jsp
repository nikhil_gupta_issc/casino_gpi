<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>
<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Publishers List</title>
    <style type="text/css" title="currentStyle">
        @import "${staticUrl}/media/css/jtable.css";
    </style>
    <script type="text/javascript" src="${staticUrl}/media/js/publisher_utils.js"></script>
</head>
<body>

<div class="modal fade" id="edit_publisher_container" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Change Publisher Password</h4>
            </div>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab">Basic</a></li>
                <li><a href="#extras" data-toggle="tab">Extras</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">

                    <form action="#" id="edit_publisher_form" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Publisher name</label>

                                <div class="input-group col-xs-9">
                                    <input name="newPublisherName" id="newPublisherName" class="form-control"
                                           type="text"/>
                                    <input name="publisherId" id="publisherId" type="hidden"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="Es el nombre que se le va a dar al publisher. Se corresponde con el attributo 'pn' que debe enviar en el requestGame.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Api Username</label>

                                <div class="input-group col-xs-9">
                                    <input name="apiUsername" id="apiUsername" class="form-control" type="text"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="Es el valor correspondiente a 'loginName'. Este valor se utiliza al establecer la comunicacion con el publisher.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">URL</label>

                                <div class="input-group col-xs-9">
                                    <input name="url" id="url" class="form-control" type="text"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="URL que va a utilizar la API. En caso de haber elegido las API 'test' no es necesario especificar una url valida.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">API Name</label>

                                <div class="input-group col-xs-9">
                                    <input name="apiName" id="apiName" class="form-control" type="text"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="Se refiere a la API de comunicacion que se va a utilizar. Puede ser GPI, test.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <label class="col-xs-3 control-label">Home URL</label>

                                <div class="input-group col-xs-9">
                                    <input name="ownerId" id="ownerId" class="form-control" type="text"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="El owner Id en wearecasino.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div> 
                            
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Ortiz Casino Id</label>

                                <div class="input-group col-xs-9">
                                    <input name="ortizCasinoNumber" id="ortizCasinoNumber" class="form-control" type="text"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="El casino id de Ortiz.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Partner code</label>

                                <div class="input-group col-xs-9">
                                    <input name="partnerCode" id="partnerCode" class="form-control" type="text"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="The partner code.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-xs-3 control-label" style="width: 27% !important;margin-right: 0px;padding-right: 0px;">Support zero bet</label>

                                <div class="input-group col-xs-9" style="width: 73%; margin-left: 0px;padding-left: 6px;">
                                    <select name="supportZeroBet" id="supportZeroBet" class="form-control">
                                    	<option value="true">Yes</option>
                                    	<option value="false">No</option>
                                    </select>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="If the publisher supports zero amount bet calls">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-xs-3 control-label" style="width: 27% !important;margin-right: 0px;padding-right: 0px;">Support zero win</label>

                                <div class="input-group col-xs-9" style="width: 73%; margin-left: 0px;padding-left: 6px;">
                                    <select name="supportZeroWin" id="supportZeroWin" class="form-control">
                                    	<option value="true">Yes</option>
                                    	<option value="false">No</option>
                                    </select>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="If the publisher supports zero amount win calls">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label id="add_publisher_error" class="col-xs-11 col-xs-offset-1"></label>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="publisherName" value=""/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Confirm</button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="extras">
                    <form action="#" id="edit_publisher_extras_form" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="jackpotEnabled"> Jackpot Enabled
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="unifiedJackpot"> Unified Jackpot
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="fitToScreen"> Fit to Screen
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label id="add_publisher_extras_error" class="col-xs-11 col-xs-offset-1"></label>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="publisherName" value=""/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="add_publisher_container" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Publisher</h4>
            </div>
            <form action="#" id="add_publisher_form" method="post" class="form-horizontal" role="form">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Publisher name</label>

                        <div class="input-group col-xs-9">
                            <input name="publisherName" id="publisherName" class="form-control" type="text"/>

                            <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                               data-trigger="hover"
                               title="Es el nombre que se le va a dar al publisher. Se corresponde con el attributo 'pn' que debe enviar en el requestGame.">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Api Username</label>

                        <div class="input-group col-xs-9">
                            <input name="apiUsername" id="apiUsername" class="form-control" type="text"/>

                            <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                               data-trigger="hover"
                               title="Es el valor correspondiente a 'loginName'. Este valor se utiliza al establecer la comunicacion con el publisher.">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Password</label>

                        <div class="input-group col-xs-9">
                            <input name="publisherPassword" id="publisherPassword" class="form-control"
                                   type="password"/>

                            <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                               data-trigger="hover"
                               title="Password que se le asigna para entrar al backoffice">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Password Confirmation</label>

                        <div class="input-group col-xs-9">
                            <input name="pPasswordConfirmation" id="pPasswordConfirmation" class="form-control"
                                   type="password"/>

                            <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                               data-trigger="hover" title="Confirmacion de password.">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">URL</label>

                        <div class="input-group col-xs-9">
                            <input name="url" id="url" class="form-control" type="text"/>

                            <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                               data-trigger="hover"
                               title="URL que va a utilizar la API. En caso de haber elegido las API 'test' no es necesario especificar una url valida.">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">API Name</label>

                        <div class="input-group col-xs-9">
                            <input name="apiName" id="apiName" class="form-control" type="text"/>

                            <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                               data-trigger="hover"
                               title="Se refiere a la API de comunicacion que se va a utilizar. Puede ser GPI, test.">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                                <label class="col-xs-3 control-label">Home URL</label>

                                <div class="input-group col-xs-9">
                                    <input name="ownerId" id="ownerId" class="form-control" type="text"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="El owner Id en wearecasino.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Ortiz Casino Id</label>

                                <div class="input-group col-xs-9">
                                    <input name="ortizCasinoNumber" id="ortizCasinoNumber" class="form-control" type="text"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="El casino id de Ortiz.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Partner code</label>

                                <div class="input-group col-xs-9">
                                    <input name="partnerCode" id="partnerCode" class="form-control" type="text"/>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="The partner code.">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Support zero bet</label>

                                <div class="input-group col-xs-9">
                                    <select name="supportZeroBet" id="supportZeroBet" class="form-control">
                                    	<option value="true">Yes</option>
                                    	<option value="false">No</option>
                                    </select>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="If the publisher supports zero amount bet calls">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Support zero win</label>

                                <div class="input-group col-xs-9">
                                    <select name="supportZeroWin" id="supportZeroWin" class="form-control">
                                    	<option value="true">Yes</option>
                                    	<option value="false">No</option>
                                    </select>

                                    <p class="input-group-addon" data-toggle="tooltip" data-placement="right"
                                       data-trigger="hover"
                                       title="If the publisher supports zero amount win calls">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </p>
                                </div>
                            </div>
                    <div class="form-group">
                        <label id="add_publisher_error" class="col-xs-11 col-xs-offset-1"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Publisher</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="publisher_table_container"></div>
</body>
</html>
