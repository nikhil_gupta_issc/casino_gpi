<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>


<df:setProperty var="hostUrl" name="application.host.url"/>
<df:setUrlProperty var="staticUrl" name="application.static.url" secureName="application.secure.url"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>Login Page</title>
<script type="text/javascript" src="${staticUrl}/media/js/jquery.js"></script>
<link href="${staticUrl}/media/css/login-box.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

$(function () {
	$("#login_button").click(function(event){
		event.preventDefault();
		$("#login_form").submit();
	});
	$("#j_username").focus();
	$("input").keypress(function(event) {
	    if (event.which == 13) {
	        event.preventDefault();
	        $("#login_form").submit();
	    }
	});
})
</script>
</head>
<body style="background-color: rgb(53, 58, 60);">
	<div style="padding: 100px 0 0 350px;">
		<div id="login_box">
			<h2>Login</h2>
			Welcome to GPI Backoffice. <br/> <br/>
			<form id='login_form' method='post' action="<c:url value='j_spring_security_check' />">
				<div id="login_box_name" style="margin-top: 20px;">Username:</div>
				<div id="login_box_field" style="margin-top: 20px;">
					<input id="j_username" name='j_username' class="form_login" title="Username"
						value="" size="30" maxlength="2048" />
				</div>
				<div id="login_box_name">Password:</div>
				<div id="login_box_field">
					<input id="j_password" name='j_password' type="password" class="form_login"
						title="Password" value="" size="30" maxlength="2048" />
				</div>
				<c:if test="${not empty error}">
				<br />
				<div id="login_fail">Login failed please try again</div>
				</c:if>
				<br /> <br /> <a id="login_button" href="#"><img src="${staticUrl}/media/images/login-btn.png" width="103" height="42" style="margin-left: 90px;" /></a>
			</form>
		</div>
	</div>
</body>
</html>