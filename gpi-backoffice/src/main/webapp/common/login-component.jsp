<%@ page session="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/tags/functions.tld" prefix="df"%>

<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>

<div id="loginComponent">
	<div id="loginContainer" class="hiddenloginComponent"
		<sec:authorize access="!isAuthenticated()">style="display:block"</sec:authorize>>
		<form action="#" id="loginForm">
			<input name="j_username" id="j_username" class="txt_field" type="text" placeholder="<spring:message code='header.login.form.label.user.name' />" /> 
			<input name="j_password" id="j_password" class="txt_field" type="password" placeholder="<spring:message code='header.login.form.label.password' />" /> 
			<input type="submit" class="green_button" value="<spring:message code='header.login.form.input.submit' />">
		</form>
		
		<!-- Errors from server -->
		<div id="loginServerErrors"></div>
		
		
		<form action="#" id="resetPasswordForm" style="display: none">
			<label><spring:message code='header.login.reset.form.label.user.mail' /></label>
			<input name="email" id="imail" type="text" /> 
			<input type="submit" class="green_button" value="<spring:message code='header.login.rest.form.submit' />">
		</form>
		<c:import url="/registerForm.do" />
	</div>

	<div id="resetMessageContainer" style="display: none">
		<spring:message code='header.login.reset.password.inform.mail' />
	</div>
</div>

