<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/tags/functions.tld" prefix="df" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<df:setUrlProperty var="hostUrl" name="application.host.url" secureName="application.secure.url"/>
<nav class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
    </div>
    
    <div class="collapse navbar-collapse" id="navbar">
        <ul class="nav navbar-nav">
            <sec:authorize access="hasAnyRole('ADMIN','PUBLISHER_ADMIN', 'SUPPORT')">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Games <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <sec:authorize access="hasRole('ADMIN')">
                            <li><a href="<c:url value="${hostUrl}/admin/roundList.do" />">Game rounds</a></li>
                            <li><a href="<c:url value="${hostUrl}/admin/games/availableGames.do" />">Available Games</a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('PUBLISHER_ADMIN', 'SUPPORT')">
                            <li><a href="<c:url value="${hostUrl}/admin/round.do" />">Game rounds</a></li>
                            <li><a href="<c:url value="${hostUrl}/admin/games/availableGames.do" />">Available Games</a>
                            </li>
                        </sec:authorize>

                        <sec:authorize access="hasRole('ADMIN')">
                            <li><a href="<c:url value="${hostUrl}/admin/getGames.do" />">Games</a></li>
                        </sec:authorize>
                    </ul>
                </li>
            </sec:authorize>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">User <b
                    class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#" id="change_password_ask" data-toggle="modal"
                           data-target="#change_password_container">Change Password</a></li>
                    <sec:authorize access="hasRole('ADMIN')">
                        <li><a href="<c:url value="${hostUrl}/admin/user.do" />" id="show_users">Show Users</a></li>
                    </sec:authorize>
                    <sec:authorize access="hasRole('PUBLISHER_ADMIN')">
                        <li><a href="<c:url value="${hostUrl}/admin/publisherUsers.do" />">Show Users</a></li>
                    </sec:authorize>
                </ul>
            </li>
            <sec:authorize access="hasAnyRole('ADMIN')">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Publisher <b
                        class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <sec:authorize access="hasAnyRole('ADMIN')">
                            <li><a href="<c:url value="${hostUrl}/admin/publisher.do" />">Show publishers</a></li>
                        </sec:authorize>
                    </ul>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAnyRole('ADMIN')">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Provider <b
                        class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <sec:authorize access="hasAnyRole('ADMIN')">
                            <li><a href="<c:url value="${hostUrl}/admin/providers.do" />">Show providers</a></li>
                        </sec:authorize>
                    </ul>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAnyRole('ADMIN','PUBLISHER_ADMIN', 'REPORT')">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <b
                        class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<c:url value="${hostUrl}/admin/report/reports.do" />">Show Reports</a></li>
                        <sec:authorize access="hasAnyRole('ADMIN')">
                            <li><a href="<c:url value="${hostUrl}/admin/report/new.do" />" id="new_report">New
                                Report</a></li>
                        </sec:authorize>
                    </ul>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAnyRole('ADMIN')">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Extras <b
                        class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<c:url value="${hostUrl}/admin/players.do" />">Manage Players</a></li>
                        <li><a href="<c:url value="${hostUrl}/applicationproperty/list.do" />">Application Properties</a></li>
                    </ul>                    
                </li>
            </sec:authorize>
            <sec:authorize access="hasAnyRole('PUBLISHER_ADMIN','FREE_SPIN')">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Free Spins <b
                        class="caret"></b></a>
                    <ul class="dropdown-menu">
                            <li><a href="<c:url value="${hostUrl}/free-spin/assign.do" />">Assign Free Spins</a></li>
                    </ul>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAnyRole('PUBLISHER_ADMIN')">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Players<b
                        class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<c:url value="${hostUrl}/admin/players.do"/>">Players</a></li>
                        <li><a href="<c:url value="${hostUrl}/admin/lockedPlayers.do"/>">Unlock Players</a></li>
                    </ul>
                </li>
            </sec:authorize>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="pull-right"><a href="<c:url value="${hostUrl}/j_spring_security_logout" />">Logout</a></li>
        </ul>
    </div>
</nav>
<div id="change_password_container" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Change Password</h4>
            </div>
            <form action="#" id="change_password_form" class="form-horizontal" role="form" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-xs-4 control-label">Current Password</label>

                        <div class="col-xs-8">
                            <input name="currentPassword" id="current_password" class="form-control" type="password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 control-label">New Password</label>

                        <div class="col-xs-8">
                            <input name="newPassword" id="new_password" class="form-control" type="password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 control-label">New Password Confirmation</label>

                        <div class="col-xs-8">
                            <input name="newPasswordConfirmation" id="new_password_confirmation" class="form-control"
                                   type="password"/>
                            <input type="hidden" name="userId" value=""/>
                        </div>
                    </div>
                    <div>
                        <label id="reset_password_error"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Change Password</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>





