package com.gpi.backoffice.birt;

import com.gpi.backoffice.domain.report.Report;
import com.gpi.backoffice.domain.user.BackofficePermission;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.exceptions.BackofficeError;
import com.gpi.backoffice.exceptions.ReportError;
import com.gpi.backoffice.exceptions.ReportNotFoundException;
import com.gpi.backoffice.service.report.ReportService;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.utils.ApplicationProperties;

import org.eclipse.birt.report.engine.api.*;
import org.eclipse.birt.report.engine.api.impl.ParameterValidationException;
import org.springframework.util.Assert;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.logging.Level;

public class BirtView extends AbstractView {

	public static final String PARAM_ISNULL = "__isnull";
	public static final String UTF_8_ENCODE = "UTF-8";

	private IReportEngine birtEngine;
	private ReportService reportService;

	private String nameParameter = "name";
	private String formatParameter = "format";

	public void setReportFormatRequestParameter(String formatParameter) {
		Assert.hasText(formatParameter, "the report format parameter must not be null");
		this.formatParameter = formatParameter;
	}

	public void setReportNameRequestParameter(String nameParameter) {
		Assert.hasText(nameParameter, "the report name parameter must not be null");
		this.nameParameter = nameParameter;
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	protected void renderMergedOutputModel(Map map, HttpServletRequest request, HttpServletResponse response) {
		boolean success = false;
		BackofficeError error = null;
		
		try {
			String reportName = request.getParameter(this.nameParameter);
			Report report = reportService.findByName(reportName);
			if(report == null) {
				throw new ReportNotFoundException();
			}
			
			runAndRenderReport(report, map, request, response);
			success = true;
			
		} catch (ParameterValidationException e) {
			logger.error("The validation of the report parameters has failed", e);
			error = ReportError.PARAMETER_VALIDATION_FAILED;
			
		} catch (IOException e) {
			logger.error("An I/O exception has ocurred when processing the report", e);
			error = ReportError.UNEXPECTED_ERROR;
			
		} catch (ReportNotFoundException e) {
			logger.error("The requested report couldn't be processed", e);
			error = e.getError();
			
		} catch (Exception e) {
			logger.error("The requested report couldn't be processed", e);
			error = ReportError.UNEXPECTED_ERROR;
		}

		if (!success) {
			try {
				response.sendRedirect(ApplicationProperties.getProperty("application.secure.url") +
						"/admin/report/execution-error.do?errorCode=" + error.getCode());
			} catch (IOException e) {
				logger.error("Failed to send redirect", e);
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void runAndRenderReport(Report report, Map map, HttpServletRequest request, HttpServletResponse response)
			throws EngineException, IOException {
		String format = request.getParameter(this.formatParameter);
		ServletContext sc = request.getSession().getServletContext();
		
		if (format == null) {
			format = "html";
		}

		String designPath = ApplicationProperties.getProperty("application.report.repository.path") + "/"
				+ report.getId() + ".rptdesign";

		IReportRunnable runnable = birtEngine.openReportDesign(designPath);
		IRunAndRenderTask runAndRenderTask = birtEngine.createRunAndRenderTask(runnable);
		
		runAndRenderTask.setParameterValues(discoverAndSetParameters(runnable, request));
		response.setContentType(birtEngine.getMIMEType(format));

		if (format.equalsIgnoreCase("pdf")) {
			response.setHeader("Content-Disposition", "attachment; filename=\"" + report.getName() + ".pdf\"");
			PDFRenderOption pdfOptions = new PDFRenderOption();
			pdfOptions.setOutputFormat("pdf");
			pdfOptions.setOption(IPDFRenderOption.PAGE_OVERFLOW, IPDFRenderOption.FIT_TO_PAGE_SIZE);
			pdfOptions.setOutputStream(response.getOutputStream());
			runAndRenderTask.setRenderOption(pdfOptions);

		} else if (format.equalsIgnoreCase("xls")) {
			response.setHeader("Content-Disposition", "attachment; filename=\"" + report.getName() + ".xlsx\"");
			EXCELRenderOption xlsOptions = new EXCELRenderOption();
			xlsOptions.setOutputFormat("xlsx");
			xlsOptions.setOutputStream(response.getOutputStream());
			runAndRenderTask.setRenderOption(xlsOptions);

		} else {
			HTMLRenderOption htmlOptions = new HTMLRenderOption();
			htmlOptions.setOutputFormat("html");
			htmlOptions.setOutputStream(response.getOutputStream());
			htmlOptions.setImageHandler(new HTMLServerImageHandler());
			htmlOptions.setBaseImageURL(request.getContextPath() + "/reports/images_output");
			htmlOptions.setImageDirectory(sc.getRealPath("/reports/images_output"));
			runAndRenderTask.setRenderOption(htmlOptions);
		}

		runAndRenderTask.getAppContext().put(EngineConstants.APPCONTEXT_BIRT_VIEWER_HTTPSERVET_REQUEST, request);
		runAndRenderTask.getAppContext().put("MAX_PAGE_BREAK_INTERVAL", 1000000);
		runAndRenderTask.run();
		runAndRenderTask.close();
	}

	@SuppressWarnings("rawtypes")
	protected HashMap discoverAndSetParameters(IReportRunnable report, HttpServletRequest request)
			throws UnsupportedEncodingException {
		HashMap<String, Object> parms = new HashMap<String, Object>();
		IGetParameterDefinitionTask task = birtEngine.createGetParameterDefinitionTask(report);
        addPublisherFilter(parms);
		@SuppressWarnings("unchecked")
		Collection<IParameterDefnBase> params = task.getParameterDefns(true);
		Iterator<IParameterDefnBase> iter = params.iterator();
		while (iter.hasNext()) {
			IParameterDefnBase param = (IParameterDefnBase) iter.next();

			IScalarParameterDefn scalar = (IScalarParameterDefn) param;
			if (request.getParameter(param.getName()) != null) {
				parms.put(param.getName(), getParamValueObject(request, scalar));
			}
		}
		task.close();
		return parms;
	}

    /**
     * This method adds a publisher parameter to the params. This is necessary because every publisher should see
     * it own data. In case of the current user is the admin then this filter won't work because the admin can
     * filter by every publisher.
     * @param parms
     */
    private void addPublisherFilter(HashMap<String,Object> parms) {
        BackofficeUser currentUser = UserUtils.getCurrentUser();
        if(currentUser.hasPermission(BackofficePermission.ADMIN)){
            return;
        }
        logger.info("Adding publisher parameter: " + currentUser.getPublisher());
        if (currentUser.getPublisher() != null) {
        	parms.put("publisher", currentUser.getPublisher().getId());
        }
        
    }

    protected Object getParamValueObject(HttpServletRequest request, IScalarParameterDefn parameterObj)
			throws UnsupportedEncodingException {
		String paramName = parameterObj.getName();
		String format = parameterObj.getDisplayFormat();
		if (doesReportParameterExist(request, paramName)) {
			ReportParameterConverter converter = new ReportParameterConverter(format, request.getLocale());
			// Get value from http request
			String paramValue = getReportParameter(request, paramName, null);
			return converter.parse(paramValue, parameterObj.getDataType());
		}
		return null;
	}

	public static String getReportParameter(HttpServletRequest request, String name, String defaultValue)
			throws UnsupportedEncodingException {
		assert request != null && name != null;

		String value = getParameter(request, name);
		// Treat it as blank value.
		if (value == null || value.length() <= 0) {
			value = ""; //$NON-NLS-1$
		}

		@SuppressWarnings("rawtypes")
		Map paramMap = request.getParameterMap();
		if (paramMap == null || !paramMap.containsKey(name)) {
			value = defaultValue;
		}

		@SuppressWarnings("rawtypes")
		Set nullParams = getParameterValues(request, PARAM_ISNULL);

		if (nullParams != null && nullParams.contains(name)) {
			value = null;
		}

		return value;
	}

	public static boolean doesReportParameterExist(HttpServletRequest request, String name) {
		assert request != null && name != null;

		boolean isExist = false;

		@SuppressWarnings("rawtypes")
		Map paramMap = request.getParameterMap();
		if (paramMap != null) {
			isExist = (paramMap.containsKey(name));
		}

		@SuppressWarnings("rawtypes")
		Set nullParams = getParameterValues(request, PARAM_ISNULL);
		if (nullParams != null && nullParams.contains(name)) {
			isExist = true;
		}

		return isExist;
	}

	public static String getParameter(HttpServletRequest request, String parameterName)
			throws UnsupportedEncodingException {
		if (request.getCharacterEncoding() == null) {
			request.setCharacterEncoding(UTF_8_ENCODE);
		}

		return request.getParameter(parameterName);
	}

	// allows setting parameter values to null using __isnull
	@SuppressWarnings("rawtypes")
	public static Set getParameterValues(HttpServletRequest request, String parameterName) {
		Set<String> parameterValues = null;
		String[] parameterValuesArray = request.getParameterValues(parameterName);

		if (parameterValuesArray != null) {
			parameterValues = new LinkedHashSet<String>();

			for (int i = 0; i < parameterValuesArray.length; i++) {
				parameterValues.add(parameterValuesArray[i]);
			}
		}

		return parameterValues;
	}

	public void setBirtEngine(IReportEngine birtEngine) {
		this.birtEngine = birtEngine;
        this.birtEngine.changeLogLevel(Level.FINEST);
        this.birtEngine.getLogger().setUseParentHandlers(false);
	}

	/**
	 * @param reportService
	 *            the reportService to set
	 */
	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}
}
