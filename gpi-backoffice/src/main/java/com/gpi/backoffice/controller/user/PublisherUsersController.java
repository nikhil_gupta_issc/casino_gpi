package com.gpi.backoffice.controller.user;

import com.gpi.backoffice.domain.user.BackofficePermission;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.domain.user.BackofficeUserDTO;
import com.gpi.backoffice.exceptions.*;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.FilterOperator;
import com.gpi.backoffice.query.filter.FirstLevelFieldFilter;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.backoffice.service.user.UserService;
import com.gpi.backoffice.service.user.publisher.PublisherUserService;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.domain.publisher.Publisher;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.ViewUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * This controller resolves all request related to the publisher users
 */
@Controller
public class PublisherUsersController {

    private static final Logger log = LoggerFactory.getLogger(PublisherUsersController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private PublisherService publisherService;

    @Autowired
    @Qualifier(value = "userQueryObject")
    private QueryObject<BackofficeUser> queryObject;

    @Autowired
    private PublisherUserService publisherUserService;

    @RequestMapping(value = "/admin/publisherUsers.do", method = RequestMethod.GET)
    @Secured({"PUBLISHER_ADMIN"})
    public ModelAndView publisherUsers() {
        log.info("Looking for Users info.");
        return new ModelAndView("admin/user/publisherUser");
    }

    @RequestMapping(value = "/admin/publisherUsers.do", method = RequestMethod.POST)
    @Secured({"PUBLISHER_ADMIN"})
    public ModelAndView publisherUsers(@RequestParam("jtStartIndex") Integer startIndex,
                                       @RequestParam("jtPageSize") Integer pageSize,
                                       @RequestParam("jtSorting") String sortExpression) {
        log.info("Looking for Users.");
        BackofficeUser backofficeUser = UserUtils.getCurrentUser();

        ModelAndView mav = ViewUtils.createJsonView();
        List<HibernateFilter> filters = new ArrayList<>();
        FirstLevelFieldFilter filter = new FirstLevelFieldFilter("publisher", FilterOperator.EQ, backofficeUser.getPublisher());
        filters.add(filter);

        log.info("Retrieving entries from database...");
        List<BackofficeUser> entries = queryObject.get(startIndex, pageSize, sortExpression, filters);

        JSONArray records = new JSONArray();
        log.info("Creating response");
        for (BackofficeUser entry : entries) {
            if (entry.getId().equals(backofficeUser.getId())) {
                log.info("Excluding current user from the list");
                continue;
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", entry.getId());
            jsonObject.element("name", entry.getUsername());
            jsonObject.element("provider", entry.getGameProvider() != null ? entry.getGameProvider().getName() : "");
            jsonObject.element("permission", entry.getPermissions().toString());
            jsonObject.element("createdOn", entry.getCreatedOn().toString());
            jsonObject.element("lastLogin", entry.getLastLogin() != null ? entry.getLastLogin().toString() : "");
            records.add(jsonObject);
        }
        log.info("Obtained {} records", records.size());
        mav.addObject("Records", records);
        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", queryObject.getCount(filters));

        return mav;
    }

    @RequestMapping(value = "/admin/createPublisherUser.do", method = RequestMethod.POST)
    @Secured({"PUBLISHER_ADMIN"})
    public ModelAndView createUser(
            @RequestParam("userName") String userName,
            @RequestParam("password") String password,
            @RequestParam("passwordConfirmation") String passwordConfirmation,
            @RequestParam("permission[]") List<String> permissions
    ) {
        log.info("Creating new Backoffice with values {}, {}, {}", userName, permissions);
        ModelAndView mav = ViewUtils.createJsonView();
        Publisher publisher = UserUtils.getCurrentUser().getPublisher();
        try {

            BackofficeUserDTO dto = new BackofficeUserDTO(userName, password, passwordConfirmation, publisher, permissions);
            userService.createUser(dto);
            mav.addObject("success", true);
        } catch (InvalidUsernameException e) {
            mav.addObject("success", false);
            mav.addObject("error", "Invalid Username");
        } catch (InvalidPasswordException e) {
            mav.addObject("success", false);
            mav.addObject("error", "Invalid Password");
        } catch (UserNameAlreadyExistsException e) {
            mav.addObject("success", false);
            mav.addObject("error", "User already exists");
        } catch (OnePermissionRequiredException e) {
            mav.addObject("success", false);
            mav.addObject("error", "One permission is required.");
        } catch (InvalidPasswordConfirmationException e) {
            mav.addObject("success", false);
            mav.addObject("error", "Password and confirmation must match.");
        } catch (Exception e) {
            mav.addObject("success", false);
            mav.addObject("error", e.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "/admin/addPublisherUserPermissions.do", method = RequestMethod.POST)
    @Secured({"PUBLISHER_ADMIN"})
    public ModelAndView addPermissions(
            @RequestParam("permissions[]") String permissions,
            @RequestParam(value = "userId") Integer userId
    ) {
        ModelAndView mav = ViewUtils.createJsonView();
        BackofficeUser user = userService.findUserById(userId);

        try {

            //first remove all permissions
            for (BackofficePermission backofficePermission : BackofficePermission.values()) {
                user.removeAuthority(backofficePermission.getValue());
            }

            for (String id : permissions.split(",")) {
                BackofficePermission backofficePermission = BackofficePermission.valueOf(id);
                if (!backofficePermission.isAvailableOnPublisher()) {
                    throw new InvalidPermissionIdException();
                }
                user.addAuthority(backofficePermission.getValue());
            }
            userService.updateUser(user);
            mav.addObject("success", true);
        } catch (UserNotFoundException | InvalidUsernameException | InvalidPasswordException | OnePermissionRequiredException | InvalidPermissionIdException ex) {
            log.error("Error while trying to add permissions", ex);
        }
        return mav;
    }

    @RequestMapping(value = "/admin/resetPublisherUserPassword.do", method = RequestMethod.POST)
    @Secured({"PUBLISHER_ADMIN"})
    public ModelAndView changePassword(@RequestParam("newPassword") String newPassword,
                                       @RequestParam(value = "userId", required = false) Integer userId) {
        ModelAndView mav = ViewUtils.createJsonView();
        boolean success = true;
        String error = null;

        try {
            publisherUserService.resetUserPassword(newPassword, userId, UserUtils.getCurrentUser());
        } catch (InvalidPasswordConfirmationException e) {
            log.error("Password confirmation failed", e);
            success = false;
            error = "Password is not valid. Must be same in both fields";
        } catch (UserNotFoundException e) {
            log.error("Current user couldn't be found", e);
            success = false;
            error = "User couldn't be found";
        } catch (AuthenticationException e) {
            log.error("Current pasword doesn't match", e);
            success = false;
            error = "Current password doesn't match";
        } catch (InvalidPermissionIdException e) {
            log.error("User does not have permission to reset this password", e);
            success = false;
            error = "User does not have permission to reset this password";
        }

        mav.addObject("success", success);
        if (error != null) {
            mav.addObject("error", error);
        }
        return mav;
    }
}
