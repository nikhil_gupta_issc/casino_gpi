package com.gpi.backoffice.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.dto.GameImage;
import com.gpi.dto.GameImageKey;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.game.GameProviderService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * User: igabbarini
 */
@Controller
public class GameController {

    private static final Logger log = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private GameDomainService gameService;
    @Autowired
    private GameProviderService gameProviderService;

    @RequestMapping(value = "/admin/games/edit.do")
    @Secured({"ADMIN"})
    public ModelAndView edit(@RequestParam(value = "id", required = false) Long id) throws NonExistentGameException {
        ModelAndView mav = new ModelAndView("admin/game/editGame");
        Game game = new Game();

        if (id != null) {
            game = gameService.getGameById(id);

        }
        List<GameProvider> providers = gameProviderService.getProviders();
        mav.addObject("game", game);
        mav.addObject("providers", providers);
        mav.addObject("actualGameProvider", (game.getGameProvider() != null)?game.getGameProvider().getName():null);
        if (id != null) {
        	GameImage gameImage = gameService.getGameImage(new GameImageKey(game.getId()));
        	if (gameImage != null) {
        		 mav.addObject("imageUrl", gameImage.getUrl());
        	}
        }
       
        return mav;
    }

    @RequestMapping(value = "/admin/games/edit.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView saveGame(@RequestParam(value = "file", required = false) CommonsMultipartFile file,
                                 @RequestParam(value = "provider", required = false) String gameProvider,
                                 @ModelAttribute Game game,
                                 BindingResult result) {
        try {
            GameProvider provider = gameProviderService.getGameProvider(gameProvider);
            game.setGameProvider(provider);
            gameService.saveOrUpdateGame(game);
            gameService.saveImage(new GameImageKey(game.getId()), file);

        } catch (MalformedURLException e) {
            log.error("Failed to create/update a game. Invalid Game URL", e);
            result.reject("game.error.invalid.game.url");
        } catch (IOException e) {
            log.error("Failed to create/update a game. Unable to save image", e);
            result.reject("game.error.invalid.image");
        } catch (Exception e) {
            log.error("Failed to create/update a game. Unexpected error", e);
            result.reject("game.error");
        }

        ModelAndView mav = new ModelAndView("admin/game/editGame");
        mav.addObject("game", game);
        List<GameProvider> providers = gameProviderService.getProviders();
        mav.addObject("providers", providers);
        mav.addObject("gameProvider", game.getGameProvider().getName());
        if (!result.hasErrors()) {
            mav.addObject("success", true);
        }
        return mav;
    }

    @RequestMapping(value = "/admin/games/availableGames.do", method = RequestMethod.GET)
    @Secured({"ADMIN","PUBLISHER_ADMIN", "SUPPORT"})
    public ModelAndView availableGames(){
        ModelAndView mav = new ModelAndView("admin/game/availableGames");
        List<Game> games = gameService.getAvailableGames();
       
        Collections.sort(games, new Comparator<Game>() {
            @Override
            public int compare(Game g1, Game g2) {
                return g1.getGameProvider().getName().compareTo(g2.getGameProvider().getName());
            }
        });
        
        BackofficeUser currentUser = UserUtils.getCurrentUser();
        mav.addObject("games", games);
        mav.addObject("publisher", (currentUser.getPublisher()!=null)?currentUser.getPublisher():null);
        return mav;
    }

    @ResponseBody
    @RequestMapping(value = "/admin/getAllGames.do", method = RequestMethod.GET)
    public String getGames() {
        log.info("Getting all games.");
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        List<Game> availableGames = gameService.getAvailableGames();
        for (Game game : availableGames) {
            JSONObject o = new JSONObject();
            o.put("game", game.getName());
            o.put("id", game.getId());
            array.add(o);
        }
        object.put("data", array);
        return object.toString();
    }
    
    @ResponseBody
    @RequestMapping(value = "/admin/getAllGameProviders.do", method = RequestMethod.GET)
    public String getProviders() {
        log.info("Looking for game providers.");
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        List<GameProvider> gameProviders = gameProviderService.getProviders();
        for (GameProvider gameProvider : gameProviders) {
            JSONObject o = new JSONObject();
            o.put("name", gameProvider.getName());
            o.put("id", gameProvider.getId());
            array.add(o);
        }
        object.put("data", array);
        return object.toString();
    }
    
    @ResponseBody
    @RequestMapping(value = "/admin/getAllGamesJTOptions.do", method = RequestMethod.POST)
    public String getGamesJTOptions() {
        log.info("Getting all games.");
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        List<Game> availableGames = gameService.getAvailableGames();
        Collections.sort(availableGames,  new Comparator<Game>() {
	            public int compare(Game g1, Game g2) {
	                int gameProviderCompare = g1.getGameProvider().getName().compareTo(g2.getGameProvider().getName());
	                return (gameProviderCompare != 0) ? gameProviderCompare : g1.getName().compareTo(g2.getName());
	            }
	    });
        for (Game game : availableGames) {
            JSONObject o = new JSONObject();
            o.put("DisplayText", game.getGameProvider().getName() + "-" + game.getName());
            o.put("Value", game.getId());
            array.add(o);
        }
        object.put("Result", "OK");
        object.put("Options", array);
        return object.toString();
    }
}
