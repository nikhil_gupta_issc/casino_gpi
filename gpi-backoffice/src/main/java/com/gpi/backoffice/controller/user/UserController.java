package com.gpi.backoffice.controller.user;

import com.gpi.backoffice.domain.user.BackofficePermission;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.domain.user.BackofficeUserDTO;
import com.gpi.backoffice.exceptions.*;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.backoffice.service.user.UserService;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.ViewUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * User: igabbarini
 */
@Controller
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private PublisherService publisherService;

    @Autowired
    @Qualifier(value = "userQueryObject")
    private QueryObject<BackofficeUser> queryObject;

    @RequestMapping(value = "/admin/user.do", method = RequestMethod.GET)
    @Secured({"ADMIN"})
    public ModelAndView listByPublisher() {
        log.info("Looking for Users info.");
        return new ModelAndView("admin/user/user");
    }

    @RequestMapping(value = "/admin/user.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView publishers(@RequestParam("jtStartIndex") Integer startIndex,
                                   @RequestParam("jtPageSize") Integer pageSize,
                                   @RequestParam("jtSorting") String sortExpression) {
        log.info("Looking for Users.");
        ModelAndView mav = ViewUtils.createJsonView();
        List<HibernateFilter> filters = new ArrayList<>();

        log.info("Retrieving entries from database...");
        List<BackofficeUser> entries = queryObject.get(startIndex, pageSize, sortExpression, filters);

        JSONArray records = new JSONArray();
        log.info("Creating response");
        for (BackofficeUser entry : entries) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", entry.getId());
            jsonObject.element("name", entry.getUsername());
            jsonObject.element("publisher", entry.getPublisher() != null ? entry.getPublisher().getName() : "");
            jsonObject.element("provider", entry.getGameProvider() != null ? entry.getGameProvider().getName() : "");
            jsonObject.element("permission", entry.getPermissions().toString());
            jsonObject.element("createdOn", entry.getCreatedOn().toString());
            jsonObject.element("lastLogin", entry.getLastLogin() != null ? entry.getLastLogin().toString() : "");
            records.add(jsonObject);
        }
        log.info("Obtained {} records", records.size());
        mav.addObject("Records", records);
        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", queryObject.getCount(filters));

        return mav;
    }

    @RequestMapping(value = "/admin/createUser.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView createUser(
            @RequestParam("userName") String userName,
            @RequestParam("password") String password,
            @RequestParam("passwordConfirmation") String passwordConfirmation,
            @RequestParam("publisher") String publisherName,
            @RequestParam("permission[]") List<String> permissions
    ) {
        log.info("Creating new Backoffice with values {}, {}, {}", userName, publisherName, permissions);
        ModelAndView mav = ViewUtils.createJsonView();
        Publisher publisher = null;

        try {
            if (StringUtils.isNotEmpty(publisherName)) {
                publisher = publisherService.findByName(publisherName);
            }

            BackofficeUserDTO dto = new BackofficeUserDTO(userName, password, passwordConfirmation, publisher, permissions);
            userService.createUser(dto);
            mav.addObject("success", true);
        } catch (InvalidUsernameException e) {
            mav.addObject("success", false);
            mav.addObject("error", "Invalid Username");
        } catch (InvalidPasswordException e) {
            mav.addObject("success", false);
            mav.addObject("error", "Invalid Password");
        } catch (UserNameAlreadyExistsException e) {
            mav.addObject("success", false);
            mav.addObject("error", "User already exists");
        } catch (OnePermissionRequiredException e) {
            mav.addObject("success", false);
            mav.addObject("error", "One permission is required.");
        } catch (InvalidPasswordConfirmationException e) {
            mav.addObject("success", false);
            mav.addObject("error", "Password and confirmation must match.");
        } catch (Exception e) {
            mav.addObject("success", false);
            mav.addObject("error", e.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "/admin/addPermissions.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView addPermissions(
            @RequestParam("permissions") String permissions,
            @RequestParam(value = "userId") Integer userId
    ) throws InvalidPermissionIdException {
        ModelAndView mav = ViewUtils.createJsonView();
        BackofficeUser user = userService.findUserById(userId);


        //first remove all permissions
        for (BackofficePermission backofficePermission : BackofficePermission.values()) {
            user.removeAuthority(backofficePermission.getValue());
        }

        for (String id : permissions.split(",")) {
            user.addAuthority(Integer.parseInt(id));
        }
        try {
            userService.updateUser(user);
            mav.addObject("success", true);
        } catch (UserNotFoundException | InvalidUsernameException | InvalidPasswordException
                | OnePermissionRequiredException ex) {
            log.error("Error while trying to add permissions", ex);
        }
        return mav;
    }

    @RequestMapping(value = "/admin/changePublisher.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView changePublisher(
            @RequestParam("publisher") String publisher,
            @RequestParam(value = "userId") Integer userId
    ) throws InvalidPermissionIdException {
        ModelAndView mav = ViewUtils.createJsonView();
        BackofficeUser user = userService.findUserById(userId);

        Publisher publisher1 = null;
        try {
            if (StringUtils.isNotEmpty(publisher)) {
                publisher1 = publisherService.findByName(publisher);
            }
            user.setPublisher(publisher1);
            userService.updateUser(user);
            mav.addObject("success", true);
        } catch (UserNotFoundException | InvalidUsernameException | InvalidPasswordException
                | OnePermissionRequiredException | NonExistentPublisherException ex) {
            log.error("Error while trying to add permissions", ex);
        }
        return mav;
    }

    @ResponseBody
    @RequestMapping(value = "/admin/getPermissions.do", method = RequestMethod.GET)
    @Secured({"ADMIN"})
    public String getPermissions(@RequestParam(value = "userId") Integer userId) {
        log.info("Getting all permissions.");
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        BackofficeUser findUserById = userService.findUserById(userId);
        BackofficePermission[] values = BackofficePermission.values();
        for (BackofficePermission permission : values) {
            JSONObject o = new JSONObject();
            o.put("hasPermission", findUserById.hasPermission(permission));
            o.put("permission", permission.toString());
            o.put("id", permission.getValue());
            array.add(o);
        }
        object.put("data", array);
        return object.toString();
    }

    @ResponseBody
    @RequestMapping(value = "/admin/getAvailablePermissions.do", method = RequestMethod.GET)
    @Secured({"ADMIN"})
    public String getAvailablePermissions() {
        log.info("Getting all permissions.");
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        BackofficePermission[] values = BackofficePermission.values();
        for (BackofficePermission permission : values) {
            JSONObject o = new JSONObject();
            o.put("permission", permission.toString());
            o.put("id", permission.getValue());
            array.add(o);
        }
        object.put("data", array);
        return object.toString();
    }
}
