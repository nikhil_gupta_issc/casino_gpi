package com.gpi.backoffice.controller.applicationproperty;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.domain.applicationproperty.ApplicationProperty;
import com.gpi.service.applicationproperty.ApplicationPropertyService;
import com.gpi.utils.ViewUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * User: f.fernandez
 */
@Controller
public class ApplicationPropertyController {

    private static final Logger log = LoggerFactory.getLogger(ApplicationPropertyController.class);
    @Autowired
    @Qualifier(value = "applicationpropertyQueryObject")
    private QueryObject<ApplicationProperty> queryObject;

    @Autowired
    private ApplicationPropertyService applicationPropertyService;

    @RequestMapping(value = "/applicationproperty/list.do", method = RequestMethod.GET)
    @Secured("ADMIN")
    public ModelAndView listGet() {
        log.info("Looking for Applicarion Properties info.");
        return new ModelAndView("admin/applicationproperty/applicationproperty");
    }

    @RequestMapping(value = "/applicationproperty/list.do", method = RequestMethod.POST)
    @Secured("ADMIN")
    public ModelAndView list(@RequestParam("jtStartIndex") Integer startIndex,
                                  @RequestParam("jtPageSize") Integer pageSize,
                                  @RequestParam("jtSorting") String sortExpression) {
        log.info("Looking for Providers.");
        ModelAndView mav = ViewUtils.createJsonView();
        List<HibernateFilter> filters = new ArrayList<>();

        log.info("Retrieving entries from database...");
        List<ApplicationProperty> entries = queryObject.get(startIndex, pageSize, sortExpression, filters);

        JSONArray records = new JSONArray();
        log.info("Creating response");
        for (ApplicationProperty entry : entries) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", entry.getId());
            jsonObject.element("name", entry.getName());
            jsonObject.element("value", entry.getValue());
            records.add(jsonObject);
        }
        log.info("Obtained {} records", records.size());
        mav.addObject("Records", records);
        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", queryObject.getCount(filters));

        return mav;
    }

    @RequestMapping(value = "/applicationproperty/create.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView create(@RequestParam("name") String name, @RequestParam("value") String value)
    {
        ModelAndView mav = ViewUtils.createJsonView();
        try {
            Integer id = applicationPropertyService.saveOrUpdate(null, name, value);
            JSONObject record = new JSONObject();
            record.element("id", id);
            record.element("name", name);
            record.element("value", value);
            mav.addObject("Result", "OK");
            mav.addObject("Record", record);
        } catch (Exception ex) {
            mav.addObject("Result", "ERROR");
            mav.addObject("Message", ex.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "/applicationproperty/update.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView editProvider(@RequestParam("id") Integer id, @RequestParam("name")String name, @RequestParam("value") String value) {

        ModelAndView mav = ViewUtils.createJsonView();
        try {
        	applicationPropertyService.saveOrUpdate(id, name, value);
        	mav.addObject("Result", "OK");
        } catch (Exception ex) {
            log.error("Error while editing property", ex);
            mav.addObject("Result", "ERROR");
            mav.addObject("Message", ex.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "/applicationproperty/delete.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView delete(@RequestParam("id") Integer id) {
        ModelAndView mav = ViewUtils.createJsonView();
        try {
        	applicationPropertyService.delete(id);
        	mav.addObject("Result", "OK");
        } catch (Exception ex) {
            log.error("Error while editing property", ex);
            mav.addObject("Result", "ERROR");
            mav.addObject("Message", ex.getMessage());
        }
        return mav;
    }

}
