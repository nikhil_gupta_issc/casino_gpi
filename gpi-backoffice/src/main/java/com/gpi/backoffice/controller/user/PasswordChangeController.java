package com.gpi.backoffice.controller.user;

import com.gpi.backoffice.exceptions.AuthenticationException;
import com.gpi.backoffice.exceptions.InvalidPasswordConfirmationException;
import com.gpi.backoffice.exceptions.UserNotFoundException;
import com.gpi.backoffice.service.user.PasswordService;
import com.gpi.backoffice.service.user.UserService;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.utils.ViewUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PasswordChangeController {

    private final Logger logger = LoggerFactory.getLogger(PasswordChangeController.class);
    @Autowired
    private PasswordService passwordService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/admin/changePassword.do", method = RequestMethod.POST)
    public ModelAndView changePassword(@RequestParam(value = "currentPassword", required = false) String currentPassword,
            @RequestParam("newPassword") String newPassword,
            @RequestParam(value = "userId", required = false) Integer userId,
            @RequestParam(value = "newPasswordConfirmation", required = false) String newPasswordConfirmation) {
        ModelAndView mav = ViewUtils.createJsonView();
        boolean success = true;
        String error = null;

        try {
            if (userId != null) {
                passwordService.changePassword(newPassword, userId);
            } else {
                passwordService.changePassword(currentPassword, newPassword, newPasswordConfirmation,
                        UserUtils.getCurrentUser());
            }
        } catch (InvalidPasswordConfirmationException e) {
            logger.error("Password confirmation failed", e);
            success = false;
            error = "Password is not valid. Must be same in both fields";
        } catch (UserNotFoundException e) {
            logger.error("Current user couldn't be found", e);
            success = false;
            error = "User couldn't be found";
        } catch (AuthenticationException e) {
            logger.error("Current pasword doesn't match", e);
            success = false;
            error = "Current password doesn't match";
        }

        mav.addObject("success", success);
        if (error != null) {
            mav.addObject("error", error);
        }
        return mav;
    }

}
