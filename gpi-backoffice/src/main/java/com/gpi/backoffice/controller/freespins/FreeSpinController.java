package com.gpi.backoffice.controller.freespins;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gpi.backoffice.domain.filters.FreeSpinFilters;
import com.gpi.backoffice.domain.user.BackofficePermission;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.dto.ReportDto;
import com.gpi.backoffice.exceptions.ReportsException;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.FilterOperator;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.backoffice.query.filter.SecondLevelFieldFilter;
import com.gpi.backoffice.service.report.ReportsService;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPlayerException;
import com.gpi.service.freespin.FreeSpinService;
import com.gpi.service.freespin.FreeSpinTransactionService;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.player.PlayerService;
import com.gpi.service.publisher.PublisherService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.spring.web.servlet.view.JsonView;

/**
 * Created by nacho on 29/12/14.
 */
@Controller
public class FreeSpinController {

    private static final Logger logger = LoggerFactory.getLogger(FreeSpinController.class);

    @Autowired
    private PublisherService publisherService;

    @Autowired
    private FreeSpinService freeSpinService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private GameDomainService gameDomainService;

    @Autowired
    private FreeSpinTransactionService freeSpinTransactionService;
    
	@Autowired
	@Qualifier(value = "freeSpinReportService")
	private ReportsService<FreeSpin> reportsService;

    @Autowired
    @Qualifier(value = "gameQueryObject")
    private QueryObject<Game> queryObject;

  
    private List<HibernateFilter> filterProvider(BackofficeUser currentUser, List<HibernateFilter> filters) {
        SecondLevelFieldFilter providerFilter = new SecondLevelFieldFilter("gameProvider", "id", FilterOperator.EQ, currentUser.getGameProvider().getId());
        filters.add(providerFilter);
        return filters;
    }

    @RequestMapping(value = "/free-spin/assign.do", method = RequestMethod.GET)
    @Secured({"FREE_SPIN","PUBLISHER_ADMIN"})
    public ModelAndView assing(@RequestParam(value = "id", required = false) Long id) {
        ModelAndView mav = new ModelAndView("admin/freeSpins/assign");
        BackofficeUser user = UserUtils.getCurrentUser();
        Publisher publisher = user.getPublisher();

        if (user.getPublisher() == null && user.hasPermission(BackofficePermission.ADMIN)) {
            mav.addObject("publishers", publisherService.getPublisherList());
        }
        List<Game> games = gameDomainService.getAvailableGamesWithFreeSpins();
        

        mav.addObject("games", games);

        return mav;
    }

  
    @RequestMapping(value = "/free-spin/users.do", method = RequestMethod.POST)
    @Secured({"FREE_SPIN","PUBLISHER_ADMIN","ADMIN"})
    public ModelAndView getUsers(@ModelAttribute("playerFilters") FreeSpinFilters freeSpinFilters, @RequestParam("jtStartIndex") Integer startIndex,
			@RequestParam("jtPageSize") Integer pageSize, @RequestParam("jtSorting") String sortExpression) throws ReportsException {

        ModelAndView mav = new ModelAndView(new JsonView());
        JSONArray array = new JSONArray();
        ReportDto<FreeSpin> reportDto = reportsService.getReportDto(freeSpinFilters, startIndex, pageSize, sortExpression);
            if (reportDto != null) {
                for (FreeSpin spin : reportDto.getObjects()) {
                    JSONObject o = new JSONObject();
                    o.element("id", spin.getId());
                    o.element("player", spin.getPlayer().getName());
                    o.element("amount", spin.getAmountLeft());
                    o.element("game", spin.getGame().getName());
                    array.add(o);
                }
            }
        mav.addObject("Records", array);
        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", reportDto != null ? reportDto.getCount() : 0);

        return mav;
    }

    
    @RequestMapping(value = "/free-spin/users/assign.do", method = RequestMethod.POST)
    @Secured({"FREE_SPIN","PUBLISHER_ADMIN","ADMIN"})
    public ModelAndView assingUser(@RequestParam(value = "amount", required = true) Integer amount,
                                   @RequestParam(value = "userName", required = true) String playerIds,
                                   @RequestParam(value = "game", required = true) String gameName,
                                   @RequestParam(value = "cents", required = true) Integer cents) {

        ModelAndView mav = new ModelAndView(new JsonView());

        try {
            if (playerIds != null) {
                String[] split = playerIds.split(",");
                
                Game game = gameDomainService.getGameByName(gameName);
               
                for (String id : split) {

                    Player player = playerService.findUserById(Long.valueOf(id));
                    if (player != null) {
                        FreeSpin freeSpin = freeSpinService.assignFreeSpin(player, amount, game, cents, null);
                        mav.addObject("freeSpin", freeSpin);
                    } else {
                        mav.addObject("error", true);
                        mav.addObject("errorCode", "003");
                    }
                }
            }
        } catch (NonExistentGameException e) {
            mav.addObject("error", true);
            mav.addObject("errorCode", "002");
        } catch (NonExistentPlayerException e) {
            mav.addObject("error", true);
            mav.addObject("errorCode", "003");
        }

        return mav;
    }

}
