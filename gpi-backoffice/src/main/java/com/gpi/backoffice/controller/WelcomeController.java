package com.gpi.backoffice.controller;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.utils.UserUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Ignacio on 3/26/2014.
 */
@Controller
public class WelcomeController {
    @RequestMapping(value = "/admin/welcome.do", method = RequestMethod.GET)
    public ModelAndView welcome() {
        ModelAndView view = new ModelAndView("admin/welcome/welcome");
        BackofficeUser currentUser = UserUtils.getCurrentUser();
        if(currentUser.getPublisher()!=null){
            view.addObject("publisher", currentUser.getPublisher());
        }
        return view;
    }

}
