package com.gpi.backoffice.controller.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gpi.backoffice.dto.ProviderRequest;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.domain.game.GameProvider;
import com.gpi.service.game.GameProviderService;
import com.gpi.utils.ViewUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * User: igabbarini
 */
@Controller
public class ProviderController {

    private static final Logger log = LoggerFactory.getLogger(ProviderController.class);
    @Autowired
    @Qualifier(value = "providerQueryObject")
    private QueryObject<GameProvider> queryObject;

    @Autowired
    private GameProviderService gameProviderService;

    @RequestMapping(value = "/admin/providers.do", method = RequestMethod.GET)
    @Secured("ADMIN")
    public ModelAndView providers() {
        log.info("Looking for Providers info.");
        return new ModelAndView("admin/provider/provider");
    }

    @RequestMapping(value = "/admin/providers.do", method = RequestMethod.POST)
    @Secured("ADMIN")
    public ModelAndView providers(@RequestParam("jtStartIndex") Integer startIndex,
                                  @RequestParam("jtPageSize") Integer pageSize,
                                  @RequestParam("jtSorting") String sortExpression) {
        log.info("Looking for Providers.");
        ModelAndView mav = ViewUtils.createJsonView();
        List<HibernateFilter> filters = new ArrayList<>();

        log.info("Retrieving entries from database...");
        List<GameProvider> entries = queryObject.get(startIndex, pageSize, sortExpression, filters);

        JSONArray records = new JSONArray();
        log.info("Creating response");
        for (GameProvider entry : entries) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", entry.getId());
            jsonObject.element("name", entry.getName());
            jsonObject.element("loginName", entry.getLoginName());
            jsonObject.element("password", entry.getPassword());
            jsonObject.element("prefix", entry.getPrefix());
            jsonObject.element("apiName", entry.getApiName());
            jsonObject.element("hasExtendedInfo", entry.isExtendedInfo());
            records.add(jsonObject);
        }
        log.info("Obtained {} records", records.size());
        mav.addObject("Records", records);
        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", queryObject.getCount(filters));

        return mav;
    }

    @RequestMapping(value = "/admin/createProvider.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView createProvider(@ModelAttribute("providerRequest") ProviderRequest providerRequest
    ) {
        log.info("Creating new GameProvider with name={}, providerApi={}, loginName={} and password={}", providerRequest.getName()
                , providerRequest.getApi(), providerRequest.getLoginName(), providerRequest.getPassword(), providerRequest.isHasExtendedInfo());
        ModelAndView mav = ViewUtils.createJsonView();
        try {
            gameProviderService.createGameProvider(providerRequest.getName(), providerRequest.getLoginName(), providerRequest.getPassword()
                    , providerRequest.getPrefix(), providerRequest.getApi(), providerRequest.isHasExtendedInfo());
            mav.addObject("success", true);
        } catch (Exception ex) {
            mav.addObject("Error", ex.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "/admin/editProvider.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView editProvider(@ModelAttribute("providerRequest") ProviderRequest providerRequest) {
        log.info("Editing provider={} with new name={}, new loginName={}, newApi={},new password={}, extendedInfo={}",
                providerRequest.getName(), providerRequest.getNewName(), providerRequest.getLoginName(),
                providerRequest.getApi(), providerRequest.getPassword(), providerRequest.isHasExtendedInfo());
        ModelAndView mav = ViewUtils.createJsonView();
        try {
            GameProvider gameProvider = gameProviderService.getGameProvider(providerRequest.getName());
            gameProviderService.updateProvider(gameProvider, providerRequest.getNewName(), providerRequest.getLoginName(),
                    providerRequest.getPassword(), providerRequest.getPrefix(), providerRequest.getApi(), providerRequest.isHasExtendedInfo());
            mav.addObject("success", true);
        } catch (Exception ex) {
            log.error("Error while editing provider", ex);
            mav.addObject("Error", ex.getMessage());
        }
        return mav;
    }

    @ResponseBody
    @RequestMapping(value = "/admin/getProviders.do", method = RequestMethod.GET)
    @Secured({"ADMIN","PUBLISHER_ADMIN","SUPPORT"})
    public String getProviders() {
        log.info("Getting providers...");
        List<GameProvider> providers = gameProviderService.getProviders();
        Collections.sort(providers, new Comparator<GameProvider>() {
			@Override
			public int compare(GameProvider o1, GameProvider o2) {
				return o1.getName().compareTo(o2.getName());
			}
        });
        JSONObject object = new JSONObject();
        JSONArray records = new JSONArray();
        log.info("Creating response");
        for (GameProvider entry : providers) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", entry.getId());
            jsonObject.element("name", entry.getName());
            jsonObject.element("apiName", entry.getApiName());
            records.add(jsonObject);
        }
        object.put("data", records);
        return object.toString();
    }
    
    @ResponseBody
    @RequestMapping(value = "/admin/getProvidersJTOptions.do", method = RequestMethod.POST)
    @Secured({"ADMIN","PUBLISHER_ADMIN","SUPPORT"})
    public String getProvidersJTOptions() {
        log.info("Getting providers...");
        List<GameProvider> providers = gameProviderService.getProviders();

        JSONObject object = new JSONObject();
        JSONArray records = new JSONArray();
        log.info("Creating response");
        for (GameProvider entry : providers) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("Value", entry.getId());
            jsonObject.element("DisplayText", entry.getName());
            records.add(jsonObject);
        }
        object.put("Result", "OK");
        object.put("Options", records);
        return object.toString();
    }

}
