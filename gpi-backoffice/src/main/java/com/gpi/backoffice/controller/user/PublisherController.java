package com.gpi.backoffice.controller.user;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.FilterOperator;
import com.gpi.backoffice.query.filter.FirstLevelFieldFilter;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.domain.publisher.Publisher;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.ViewUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * User: igabbarini
 */
@Controller
public class PublisherController {

    private static final Logger log = LoggerFactory.getLogger(PublisherController.class);
    @Autowired
    private PublisherService publisherService;

    @Autowired
    @Qualifier(value = "publisherQueryObject")
    private QueryObject<Publisher> queryObject;

    @RequestMapping(value = "/admin/publisher.do", method = RequestMethod.GET)
    @Secured({"ADMIN"})
    public ModelAndView listByPublisher() {
        log.info("Looking for Publisher info.");
        
        return new ModelAndView("admin/publisher/publisher");
    }

    @RequestMapping(value = "/admin/publisher.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView publishers(@RequestParam("jtStartIndex") Integer startIndex,
                                   @RequestParam("jtPageSize") Integer pageSize,
                                   @RequestParam("jtSorting") String sortExpression) {
        log.info("Looking for Publishers.");
        ModelAndView mav = ViewUtils.createJsonView();
        List<HibernateFilter> filters = new ArrayList<>();

        log.info("Retrieving entries from database...");
        List<Publisher> entries = queryObject.get(startIndex, pageSize, sortExpression, filters);

        JSONArray records = new JSONArray();
        log.info("Creating response");
        for (Publisher entry : entries) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", entry.getId());
            jsonObject.element("name", entry.getName());
            jsonObject.element("apiUsername", entry.getApiUsername());
            jsonObject.element("apiName", entry.getApiName());
            jsonObject.element("url", entry.getUrl());
            jsonObject.element("createdOn", entry.getCreatedOn().toString());
            jsonObject.element("ownerId", entry.getOwnerId());
            jsonObject.element("ortizCasinoNumber", entry.getOrtizCasinoNumber());
            jsonObject.element("partnerCode", entry.getPartnerCode());
            jsonObject.element("supportZeroBet", entry.getSupportZeroBet());
            jsonObject.element("supportZeroWin", entry.getSupportZeroWin());
            records.add(jsonObject);
        };
        log.info("Obtained {} records", records.size());
        mav.addObject("Records", records);
        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", queryObject.getCount(filters));

        return mav;
    }

    @RequestMapping(value = "/admin/createPublisher.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView createUser(
            @RequestParam("publisherName") String name,
            @RequestParam("apiName") String apiName,
            @RequestParam("apiUsername") String apiUsername,
            @RequestParam("publisherPassword") String password,
            @RequestParam("pPasswordConfirmation") String passwordConfirmation,
            @RequestParam("ownerId") String ownerId,
            @RequestParam("ortizCasinoNumber") String ortizCasinoNumber,
            @RequestParam("url") String url,
            @RequestParam("partnerCode") String partnerCode,
            @RequestParam("supportZeroBet") Boolean supportZeroBet,
            @RequestParam("supportZeroWin") Boolean supportZeroWin
    ) {
        log.info("Creating new Publisher with name={}, api={}, username={} and url={}", name, apiName, apiUsername, url);
        ModelAndView mav = ViewUtils.createJsonView();
        try {
            publisherService.createPublisher(name, apiName, apiUsername, password, passwordConfirmation, url, ownerId, ortizCasinoNumber, partnerCode, supportZeroBet, supportZeroWin);
            mav.addObject("success", true);
        } catch (Exception ex) {
            mav.addObject("Error", ex.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "/admin/changePubPassword.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView changePassword(@RequestParam("currentPassword") String currentPassword,
                                       @RequestParam("newPassword") String newPassword,
                                       @RequestParam("newPasswordConfirmation") String newPasswordConfirmation) {
        log.info("Changing Publisher password...");
        ModelAndView mav = ViewUtils.createJsonView();
        boolean success = true;
        String error = null;

        try {
            publisherService.changePassword(currentPassword, newPassword, newPasswordConfirmation,
                    UserUtils.getCurrentUser().getPublisher().getName());
        } catch (Exception ex) {
            success = false;
            error = ex.getMessage();
        }

        mav.addObject("success", success);
        if (error != null) {
            mav.addObject("error", error);
        }
        return mav;
    }

    @RequestMapping(value = "/admin/changeURL.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView changeURL(@RequestParam("url") String url,
                                  @RequestParam("publisherName") String publisherName
    ) {
        log.info("Changing Publisher URL...");
        ModelAndView mav = ViewUtils.createJsonView();
        boolean success = true;
        String error = null;

        try {
            publisherService.changeURL(publisherName, url);
        } catch (Exception ex) {
            success = false;
            error = ex.getMessage();
        }
        
        mav.addObject("success", success);
        if (error != null) {
            mav.addObject("error", error);
        }
        return mav;
    }

    @RequestMapping(value = "/admin/editPublisher.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView editPublisher(
            @RequestParam("publisherName") String name,
            @RequestParam("apiName") String apiName,
            @RequestParam("apiUsername") String apiUsername,
            @RequestParam("publisherId") Long publisherId,
            @RequestParam("url") String url,
            @RequestParam("ownerId") String ownerId,
            @RequestParam("ortizCasinoNumber") String ortizCasinoNumber,
            @RequestParam("partnerCode") String partnerCode,
            @RequestParam("supportZeroBet") Boolean supportZeroBet,
            @RequestParam("supportZeroWin") Boolean supportZeroWin
    ) {
        log.info("setting new values for publisher with id={}. New values: apiUsername={}, apiName={},name={}, url={}",
                publisherId, apiUsername, apiName, name, url);
        ModelAndView mav = ViewUtils.createJsonView();
        try {
            Publisher publisher = publisherService.findById(publisherId);
            publisher.setApiName(apiName);
            publisher.setApiUsername(apiUsername);
            publisher.setName(name);
            publisher.setUrl(url);
            publisher.setOwnerId(ownerId);
            publisher.setOrtizCasinoNumber(ortizCasinoNumber);
            publisher.setPartnerCode(partnerCode);
            publisher.setSupportZeroBet(supportZeroBet);
            publisher.setSupportZeroWin(supportZeroWin);
            publisherService.updatePublisher(publisher);
            mav.addObject("success", true);
        } catch (Exception ex) {
            mav.addObject("Error", ex.getMessage());
        }
        return mav;
    }

    private List<HibernateFilter> processPublisherFilter(List<HibernateFilter> filters, BackofficeUser currentUser) {
        log.debug("Adding a filter by Publisher");
        if (currentUser.getPublisher() != null) {
            log.debug("Filtering by Publisher name {}", currentUser.getPublisher());
            FirstLevelFieldFilter filter = new FirstLevelFieldFilter("name", FilterOperator.EQ, currentUser.getPublisher().getName());
            filters.add(filter);
        }

        return filters;
    }

    @ResponseBody
    @RequestMapping(value = "/admin/getPublishers.do", method = RequestMethod.GET)
    @Secured({"ADMIN", "PROVIDER"})
    public String getPublishers() {
        log.info("Looking for Publishers.");
        ModelAndView mav = ViewUtils.createJsonView();
        log.info("Retrieving entries from database...");
        List<Publisher> publisherList = publisherService.getPublisherList();
        JSONObject object = new JSONObject();
        JSONArray records = new JSONArray();
        log.info("Creating response");
        for (Publisher entry : publisherList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", entry.getId());
            jsonObject.element("name", entry.getName());
            records.add(jsonObject);
        }
        object.put("data", records);
        return object.toString();
    }


}
