package com.gpi.backoffice.controller.utils;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Nacho on 2/27/14.
 */
public class TimeZoneUtils {

	public static TimeZone getTimeZone (HttpServletRequest request){
		Locale clientLocale = request.getLocale();
		Calendar calendar = Calendar.getInstance(clientLocale);
		TimeZone clientTimeZone = calendar.getTimeZone();
		return clientTimeZone;
	}
}
