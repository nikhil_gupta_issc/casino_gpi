package com.gpi.backoffice.controller.player;

import com.gpi.backoffice.domain.filters.PlayerFilters;
import com.gpi.backoffice.domain.user.BackofficePermission;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.dto.PlayerDto;
import com.gpi.backoffice.dto.ReportDto;
import com.gpi.backoffice.exceptions.ReportsException;
import com.gpi.backoffice.service.api.BingoApi;
import com.gpi.backoffice.service.report.ReportsService;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.domain.player.Player;
import com.gpi.dto.PlayerLockedDto;
import com.gpi.service.player.PlayerService;
import com.gpi.utils.ViewUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * User: igabbarini
 */
@Controller
public class PlayerController {
	private static final Logger log = LoggerFactory.getLogger(PlayerController.class);

	@Autowired
	@Qualifier(value = "playerReportService")
	private ReportsService<PlayerDto> reportsService;

	@Autowired
	private PlayerService playerService;

    @Autowired
    private BingoApi bingoApi;

	@ModelAttribute("playerFilters")
	public PlayerFilters getPlayerFilters() {
		return new PlayerFilters();
	}

	@RequestMapping(value = "/admin/players.do", method = RequestMethod.GET)
	@Secured({ "PUBLISHER_ADMIN", "ADMIN" })
	public ModelAndView players() {
		return new ModelAndView("admin/player/player");
	}

	@RequestMapping(value = "/admin/players.do", method = RequestMethod.POST)
	@Secured({ "PUBLISHER_ADMIN", "ADMIN" })
	public ModelAndView getPlayers(@ModelAttribute("playerFilters") PlayerFilters playerFilters, @RequestParam("jtStartIndex") Integer startIndex,
			@RequestParam("jtPageSize") Integer pageSize, @RequestParam("jtSorting") String sortExpression) {

		log.info("Getting all players with this filters: {}", playerFilters);
		ModelAndView mav = ViewUtils.createJsonView();
		JSONArray array = new JSONArray();
		ReportDto<PlayerDto> reportDto;
		BackofficeUser currentUser = UserUtils.getCurrentUser();
		if(currentUser.getPublisher()!=null){
			playerFilters.setPublisher(currentUser.getPublisher().getId());
		}
		try {
			reportDto = reportsService.getReportDto(playerFilters, startIndex, pageSize, sortExpression);
			for (PlayerDto player : reportDto.getObjects()) {
				JSONObject o = new JSONObject();
				o.put("name", player.getName());
				o.put("publisher", player.getPublisher());
				o.put("createdOn", player.getCreatedOn().toString());
				o.put("id", player.getId());
				array.add(o);
			}

			mav.addObject("Result", "OK");
			mav.addObject("TotalRecordCount", reportDto.getCount());
			mav.addObject("Records", array);
		} catch (ReportsException e) {
			mav.addObject("Result", "ERROR");
			mav.addObject("Message", e.getMessage());

		}

		return mav;
	}

	@RequestMapping(value = "/admin/lockedPlayers.do", method = RequestMethod.GET)
	@Secured({ "PUBLISHER_ADMIN", "ADMIN" })
	public ModelAndView lockedPlayersView() {
		ModelAndView view = new ModelAndView("admin/player/lockedPlayer");
		return view;
	}

    @RequestMapping(value = "/admin/lockedPlayers.do", method = RequestMethod.POST)
    @Secured({ "PUBLISHER_ADMIN", "ADMIN" })
    public ModelAndView lockedPlayers() {

        ModelAndView mav = ViewUtils.createJsonView();
        JSONArray array = new JSONArray();
        ReportDto<PlayerDto> reportDto;
        BackofficeUser currentUser = UserUtils.getCurrentUser();
        try {
            List<PlayerLockedDto> lockedPlayers = null;
            if (currentUser.getPublisher() != null) {
                lockedPlayers = bingoApi.getLockedPlayers(currentUser.getPublisher().getName());
            }
            for (PlayerLockedDto playerLockedDto : lockedPlayers) {
                JSONObject o = new JSONObject();
                o.put("name", playerLockedDto.getPlayerName());
                o.put("locked", playerLockedDto.isLocked());
                o.put("id", playerLockedDto.getId());
                array.add(o);
            }

            mav.addObject("Result", "OK");
            mav.addObject("TotalRecordCount", lockedPlayers.size());
            mav.addObject("Records", array);
        } catch (Exception e) {
            mav.addObject("Result", "ERROR");
            mav.addObject("Message", e.getMessage());

        }

        return mav;
    }

	@RequestMapping(value = "/admin/unlockPlayers.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" , "PUBLISHER_ADMIN"})
	public ModelAndView unlockPlayers(@RequestParam("playerIds[]") List<Long> playerIds) {
		log.info("Calling request to unlock players: " + playerIds);
		ModelAndView mav = ViewUtils.createJsonView();
		try {
			BackofficeUser currentUser = UserUtils.getCurrentUser();
			bingoApi.unlockPlayers(playerIds, currentUser.getPublisher().getName());
			mav.addObject("success", true);
		} catch (Exception ex) {
			mav.addObject("Error", ex.getMessage());
		}
		return mav;
	}

	@RequestMapping(value = "/admin/changePlayerStatus.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView changePlayerStatus(@RequestParam("test") boolean test, @RequestParam("locked") boolean locked, @RequestParam("playerId") Long playerId) {
		log.info("Changin player status. test={}, locked={}, playerId={}", test, locked, playerId);
		ModelAndView mav = ViewUtils.createJsonView();
		try {
			Player player = playerService.findUserById(playerId);
			mav.addObject("success", true);
		} catch (Exception ex) {
			mav.addObject("Error", ex.getMessage());
		}
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "/admin/players-suggest.do", method = RequestMethod.GET)
	@Secured({ "PUBLISHER_ADMIN", "ADMIN" , "FREE_SPIN"})
	public String players(@RequestParam(value = "term", required = false) String name,
			@RequestParam(value = "publisherId", required = false) Integer publisherId) {
		BackofficeUser currentUser = UserUtils.getCurrentUser();
		if(publisherId == null && currentUser.hasPermission(BackofficePermission.ADMIN)){
			List<Player> players = playerService.findPlayerLike(name);
			return processPlayersNames(players).toString();
		} else if(publisherId == null) {
			publisherId = currentUser.getPublisher().getId();
		}

		List<Player> players = playerService.findPlayerLike(name, publisherId);
		return processPlayersNames(players).toString();

	}

	JSONArray processPlayersNames(List<Player> players){
		JSONArray names= new JSONArray();
		for(Player p : players){
			JSONObject object = new JSONObject();
			object.put("value", p.getName());
			object.put("id", p.getId());
			names.add(object);
		}
		return names;
	}
}
