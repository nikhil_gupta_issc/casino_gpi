package com.gpi.backoffice.controller.report;

import com.gpi.backoffice.birt.BirtView;
import com.gpi.backoffice.domain.report.Report;
import com.gpi.backoffice.exceptions.ReportNotFoundException;
import com.gpi.backoffice.service.report.ReportListService;
import com.gpi.backoffice.service.report.ReportService;
import com.gpi.utils.ViewUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.AbstractView;

import java.util.List;

@Controller
public class ReportController {

    @Autowired
    private BirtView birtView;

    @Autowired
    private ReportListService reportListService;

    @Autowired
    private ReportService reportService;

    private static final Logger logger = LoggerFactory.getLogger(ReportController.class);

    @RequestMapping(value = "/admin/report/reports.do", method = RequestMethod.GET)
    @Secured({"ADMIN", "PUBLISHER_ADMIN", "REPORT"})
    public String showReports() {
        return "admin/report/reports";
    }

    @RequestMapping(value = "/admin/report/reports.do", method = RequestMethod.POST)
    @Secured({"ADMIN", "PUBLISHER_ADMIN", "REPORT"})
    public ModelAndView getReports(@RequestParam("jtStartIndex") Integer startIndex,
                                   @RequestParam("jtPageSize") Integer pageSize,
                                   @RequestParam(value = "jtSorting", required = false) String sortExpression) {
        logger.info("Requesting list of reports. start: {}  pageSize: {} sortExpression: {}"
                , startIndex, pageSize, sortExpression);

        ModelAndView mav = ViewUtils.createJsonView();
        List<Report> reports = reportListService.list(startIndex, pageSize, sortExpression);

        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", reportListService.getCount());

        JSONArray records = new JSONArray();
        for (Report report : reports) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", report.getId());
            jsonObject.element("name", report.getName());
            jsonObject.element("description", report.getDescription());
            records.add(jsonObject);
        }

        mav.addObject("Records", records);
        return mav;
    }

    @RequestMapping(value = "/admin/report/execution-setup.do", method = RequestMethod.GET)
    @Secured({"ADMIN", "PUBLISHER_ADMIN", "REPORT"})
    public ModelAndView prepareExecution(@RequestParam("name") String name) {
        ModelAndView mav = new ModelAndView("/admin/report/reportExecution");
        Report report = reportService.findByName(name);
        mav.addObject("report", report);
        return mav;
    }

    @RequestMapping(value = "/admin/report/execute.do", method = RequestMethod.GET)
    @Secured({"ADMIN", "PUBLISHER_ADMIN", "REPORT"})
    public AbstractView executeReport(Model model) {
        return birtView;
    }

    @RequestMapping(value = "/admin/report/removeReport.do", method = RequestMethod.GET)
    @Secured({"ADMIN"})
    public String removeReport(@RequestParam("id") Integer id) {
        logger.info("Called removeReport.do with id={}", id);
        try {
            reportService.removeReport(id);
            logger.info("removeReport.do executed successfully.");
        } catch (ReportNotFoundException e) {
            logger.warn("A problem occurred while trying to remove a report.", e);
        }
        return "admin/report/reports";
    }

    @RequestMapping(value = "/admin/report/execution-error.do", method = RequestMethod.GET)
    @Secured({"ADMIN", "PUBLISHER_ADMIN", "REPORT"})
    public ModelAndView showExecutionError(@RequestParam(value = "errorCode", required = false) Integer errorCode) {
        ModelAndView mav = new ModelAndView("/admin/report/reportExecutionError");
        mav.addObject("errorCode", errorCode);
        return mav;
    }
}
