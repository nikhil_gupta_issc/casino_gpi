package com.gpi.backoffice.controller.freespins;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gpi.domain.currency.Currency;
import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.freespin.FreeSpinService;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.player.PlayerService;
import com.gpi.service.publisher.PublisherService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.spring.web.servlet.view.JsonView;

@Controller
public class FreeSpinApiController {
	
	@Autowired
	private PlayerService playerService;
	
    @Autowired
    private PublisherService publisherService;
    
    @Autowired
    private FreeSpinService freeSpinService;
    
    @Autowired
    private GameDomainService gameDomainService;

    @RequestMapping(value = "/api/free-spin.do", method = RequestMethod.POST)
	public ModelAndView addFreeSpins (@RequestParam("playerName") String playerName, @RequestParam("currency") Currency currency, @RequestParam("gameName") String gameName, @RequestParam("amount") int amount, @RequestParam("ref") String ref, @RequestParam("publisher") String publisherName, @RequestParam("publisherPassword") String publisherPassword) throws Exception {
		    	
    	ModelAndView mav = new ModelAndView(new JsonView());
    	
		Publisher publisher = publisherService.findByName(publisherName);
		if (!publisher.getPassword().equals(publisherPassword)) {
			throw new Exception("Invalid credentials");
		}
		Player player = playerService.findUserByUserNameAndPublisher(playerName, publisher.getId());
		if (player == null) {
			player = playerService.createNewUser(playerName, currency, publisher);
		}
		Game game = gameDomainService.getGameByName(gameName);
		FreeSpin freeSpin = freeSpinService.assignFreeSpin(player, amount, game, 25, ref);
		mav.addObject("ref", freeSpin.getExternalRef());
		mav.addObject("spins", freeSpin.getAmountLeft());
		return mav;
	}
	
    @RequestMapping(value = "/api/free-spin.do", method = RequestMethod.GET)
	public ModelAndView getFreeSpins (@RequestParam("playerName") String username, @RequestParam("publisher") String publisherName) throws NonExistentPublisherException {
    	ModelAndView mav = new ModelAndView(new JsonView());
    	Publisher publisher = publisherService.findByName(publisherName);
		Player player = playerService.findUserByUserNameAndPublisher(username, publisher.getId());
		List<FreeSpin> freeSpins = freeSpinService.findFreeSpinByPlayer(player);
		
		JSONArray array = new JSONArray();
		if (freeSpins != null) {
			for (FreeSpin f : freeSpins) {
				JSONObject object = new JSONObject();
				object.put("game", f.getGame().getName());
				object.put("amount", f.getAmountLeft());
				object.put("ref", f.getExternalRef());
				array.add(object);
			}
		}
		mav.addObject("freespins", array);
		
		return mav;
	}
    
    @RequestMapping(value = "/api/free-spin/games.do", method = RequestMethod.GET)
   	public ModelAndView getFreeSpinsGames ()  {
    	ModelAndView mav = new ModelAndView(new JsonView());
    	
    	List<Game>games = gameDomainService.getAvailableGamesWithFreeSpins();
    	
    	JSONArray array = new JSONArray();
    	if (games != null) {
    		for (Game g : games) {
    			JSONObject object = new JSONObject();
    			object.put("name", g.getName());
    			array.add(object);
    		}
    	}
    	mav.addObject("games", array);
    	return mav;
    }
}
