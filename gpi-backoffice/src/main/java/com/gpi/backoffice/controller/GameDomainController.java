package com.gpi.backoffice.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.gpi.backoffice.domain.user.BackofficePermission;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.exceptions.InvalidPermissionIdException;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.FilterOperator;
import com.gpi.backoffice.query.filter.FirstLevelFieldFilter;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.PublisherGameMapping;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.GameImage;
import com.gpi.dto.GameImageKey;
import com.gpi.dto.GamesImages;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.game.PublisherGameService;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.ViewUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Created by Nacho on 2/27/14.
 */
@Controller
public class GameDomainController {

	private static final Logger logger = LoggerFactory.getLogger(GameDomainController.class);

	@Autowired
	private GameDomainService gameDomainService;
	@Autowired
	private PublisherService publisherService;
	@Autowired
	private PublisherGameService publisherGameService;

	@Autowired
	@Qualifier(value = "publisherGameQueryObject")
	private QueryObject<PublisherGameMapping> publisherGameQueryObject;

	@Autowired
	@Qualifier(value = "gameQueryObject")
	private QueryObject<Game> queryObject;

	@RequestMapping(value = "/admin/changeGameModes.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView changeGameModes(@RequestParam("free") boolean free, 
			@RequestParam("charged") boolean charged,
			@RequestParam("gameName") String gameName, 
			@RequestParam("pubGameId") Long pubGameId) {
		
		logger.info("Changing PublisherGame modes. Setting free={}, charged={} for pubGameId={}", free, charged, pubGameId);
		ModelAndView mav = ViewUtils.createJsonView();
		boolean success = true;
		String error = null;
		try {
			Game game = gameDomainService.getGameByName(gameName);
			BackofficeUser currentUser = UserUtils.getCurrentUser();
			if (currentUser.hasPermission(BackofficePermission.ADMIN)) {
				publisherGameService.changePublisherGameModes(pubGameId, free, charged);
			} else {
				Publisher publisher = publisherService.findByName(currentUser.getPublisher().getName());
				publisherGameService.changePublisherGameModes(publisher, game, free, charged);
			}
		} catch (Exception ex) {
			success = false;
			error = ex.getMessage();
		}

		mav.addObject("success", success);
		if (error != null) {
			mav.addObject("error", error);
		}
		return mav;
	}

	@RequestMapping(value = "/admin/getGamesModes.do", method = RequestMethod.GET)
	@Secured({ "ADMIN" })
	public ModelAndView getGamesModes() {
		logger.info("Looking for Game modes");
		return new ModelAndView("admin/publisher/gameModes");
	}

	@RequestMapping(value = "/admin/getGamesModes.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView gamesModes(@RequestParam(value = "jtStartIndex") Integer startIndex,
			@RequestParam(value = "jtPageSize") Integer pageSize,
			@RequestParam(value = "jtSorting") String sortExpression,
			@RequestParam(value = "publisher", required = false) String publisher) {
		
		logger.info("Getting Game modes...");
		ModelAndView mav = ViewUtils.createJsonView();
		BackofficeUser currentUser = UserUtils.getCurrentUser();
		List<HibernateFilter> filters = new ArrayList<>();
		try {
			filters = filterPublishers(currentUser, filters, publisher);

			logger.info("Retrieving entries from database...");
			List<PublisherGameMapping> entries = publisherGameQueryObject.get(startIndex, pageSize, sortExpression,
					filters);

			JSONArray records = new JSONArray();
			logger.info("Creating response");
			for (PublisherGameMapping entry : entries) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.element("id", entry.getId());
				jsonObject.element("game", entry.getGame().getName());
				jsonObject.element("free", String.valueOf(entry.isFree()));
				jsonObject.element("charged", String.valueOf(entry.isCharged()));
				jsonObject.element("publisher", entry.getPublisher().getName());
				jsonObject.element("provider", entry.getGame().getGameProvider().getName());
				records.add(jsonObject);
			}
			logger.info("Obtained {} records", records.size());
			mav.addObject("Records", records);
			mav.addObject("Result", "OK");
			mav.addObject("TotalRecordCount", publisherGameQueryObject.getCount(filters));

		} catch (NonExistentPublisherException e) {
			mav.addObject("success", false);
			mav.addObject("error", e.getMessage());
		}
		return mav;
	}

	@RequestMapping(value = "/admin/addNewGame.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView addNewGame(@RequestParam("game") String game, 
			@RequestParam("url") String url,
			@RequestParam("provider") String provider) throws InvalidPermissionIdException {
		
		ModelAndView mav = ViewUtils.createJsonView();

		try {
			gameDomainService.addGame(game, url, provider);
			mav.addObject("success", true);
		} catch (Exception ex) {
			logger.error("Error while trying to add permissions", ex);
		}
		return mav;
	}

	private List<HibernateFilter> filterPublishers(BackofficeUser currentUser, List<HibernateFilter> filters,
			String publisherName) throws NonExistentPublisherException {
		
		if (currentUser.hasPermission(BackofficePermission.ADMIN) && StringUtils.isNotEmpty(publisherName)) {
			Publisher publisher = publisherService.findByName(publisherName);
			FirstLevelFieldFilter filter = new FirstLevelFieldFilter("publisher", FilterOperator.EQ, publisher);
			filters.add(filter);
		} else if (currentUser.hasPermission(BackofficePermission.ADMIN) && StringUtils.isEmpty(publisherName)) {
			logger.info("Showing all publishers because is an admin user.");
		} else {
			Publisher publisher = publisherService.findByName(currentUser.getPublisher().getName());
			FirstLevelFieldFilter filter = new FirstLevelFieldFilter("publisher", FilterOperator.EQ, publisher);
			filters.add(filter);
		}
		return filters;
	}

	@RequestMapping(value = "/admin/getGames.do", method = RequestMethod.GET)
	@Secured({ "ADMIN" })
	public ModelAndView listByPublisher() {
		logger.info("Looking for Games info.");
		return new ModelAndView("admin/game/game");
	}

	@RequestMapping(value = "/admin/getGames.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView publishers(@RequestParam("jtStartIndex") Integer startIndex,
			@RequestParam("jtPageSize") Integer pageSize, 
			@RequestParam("jtSorting") String sortExpression) {
		
		logger.info("Looking for Games.");
		ModelAndView mav = ViewUtils.createJsonView();

		List<HibernateFilter> filters = new ArrayList<>();
		logger.info("Retrieving entries from database...");
		List<Game> entries = queryObject.get(startIndex, pageSize, sortExpression, filters);

		JSONArray records = new JSONArray();
		logger.info("Creating response");
		for (Game entry : entries) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.element("id", entry.getId());
			jsonObject.element("name", entry.getName());
			jsonObject.element("url", entry.getUrl());
			jsonObject.element("provider", (entry.getGameProvider() != null) ? entry.getGameProvider().getName() : "");
			records.add(jsonObject);
		}
		logger.info("Obtained {} records", records.size());
		mav.addObject("Records", records);
		mav.addObject("Result", "OK");
		mav.addObject("TotalRecordCount", queryObject.getCount(filters));

		return mav;
	}

	@RequestMapping(value = "/admin/editGame.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView editPublisher(@RequestParam("newName") String newName, 
			@RequestParam("newUrl") String newUrl,
			@RequestParam("newProvider") String newProvider, 
			@RequestParam("game") String game) {
		
		logger.info("Editing Game. newUrl={}, newProvider={}, newName={} for game={}", newUrl, newProvider, newName, game);
		ModelAndView mav = ViewUtils.createJsonView();
		try {
			gameDomainService.changeGame(game, newName, newUrl, newProvider);
			mav.addObject("success", true);
		} catch (Exception ex) {
			mav.addObject("Error", ex.getMessage());
		}
		return mav;
	}

	@RequestMapping(value = "/admin/getGameMappings.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView gamesMappings(@RequestParam(value = "jtStartIndex") Integer startIndex,
			@RequestParam(value = "jtPageSize") Integer pageSize,
			@RequestParam(value = "jtSorting") String sortExpression,
			@RequestParam(value = "publisher", required = false) String publisher,
			@RequestParam(value = "provider", required = false) String provider) {
		
		logger.info("Getting Game modes...");
		ModelAndView mav = ViewUtils.createJsonView();
		BackofficeUser currentUser = UserUtils.getCurrentUser();
		List<HibernateFilter> filters = new ArrayList<>();
		try {
			filters = filterPublishers(currentUser, filters, publisher);
			
			logger.info("Retrieving entries from database...");
			List<PublisherGameMapping> entries = publisherGameQueryObject.get(startIndex, pageSize, sortExpression, filters);

			JSONArray records = new JSONArray();
			logger.info("Creating response");
			GamesImages gamesImageUrls = gameDomainService.getGamesImageUrls();
			for (PublisherGameMapping domain : entries) {
				GameImage gameImage = gamesImageUrls.get(new GameImageKey(domain.getPublisher().getId(), domain.getGame().getId()));
				records.add(DomainToDto(domain, gameImage));
			}
			logger.info("Obtained {} records", records.size());
			mav.addObject("Records", records);
			mav.addObject("Result", "OK");
			mav.addObject("TotalRecordCount", publisherGameQueryObject.getCount(filters));

		} catch (NonExistentPublisherException e) {
			mav.addObject("success", false);
			mav.addObject("error", e.getMessage());
		}
		return mav;
	}

	@RequestMapping(value = "/admin/gameMappings/create.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView createGameMapping(@RequestParam("publisherId") Integer publisherId,
			@RequestParam("game.id") Long gameId, 
			@RequestParam("free") Boolean free,
			@RequestParam("charged") Boolean charged,
			@RequestParam(value = "file", required = false) CommonsMultipartFile file)
	{
		logger.info("Create GameMapping. publisherId={}, gameId={}, free={} for changed={}", publisherId, gameId, free,charged);
		ModelAndView mav = ViewUtils.createJsonView();
		try {

			PublisherGameMapping domain = buildPublisherGameMapping(null, publisherId, gameId, free, charged);
			Long id = publisherGameService.addPublisherGame(domain);
			gameDomainService.saveImage(new GameImageKey(publisherId, gameId), file);
			
			GameImage gameImage = gameDomainService.getGameImage(new GameImageKey(publisherId, gameId));
			mav.addObject("Record", DomainToDto(publisherGameService.getById(id), gameImage));
			mav.addObject("Result", "OK");
		} catch (Exception ex) {
			mav.addObject("Result", "ERROR");
			mav.addObject("Message", ex.getMessage());
		}
		return mav;
	}

	@RequestMapping(value = "/admin/gameMappings/update.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView updateGameMapping(@RequestParam(value = "id") Long publisherGameMappingId,
			@RequestParam("publisher.id") Integer publisherId, 
			@RequestParam("game.id") Long gameId,
			@RequestParam("free") Boolean free, 
			@RequestParam("charged") Boolean charged,
			@RequestParam(value = "file", required = false) CommonsMultipartFile file) {
		
		logger.info("Update GameMapping. publisherId={}, gameId={}, free={} for changed={}", publisherId, gameId, free, charged);
		ModelAndView mav = ViewUtils.createJsonView();
		try {
			publisherGameService.changePublisherGameModes(publisherGameMappingId, free, charged);
			gameDomainService.saveImage(new GameImageKey(publisherId, gameId), file);
			
			mav.addObject("Result", "OK");
		} catch (Exception ex) {
			mav.addObject("Result", "ERROR");
			mav.addObject("Message", ex.getMessage());
		}
		return mav;
	}

	@RequestMapping(value = "/admin/gameMappings/delete.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView deleteGameMapping(@RequestParam(value = "id") Long id) {
		logger.info("deleting GameMapping. id={}", id);
		ModelAndView mav = ViewUtils.createJsonView();
		try {
			publisherGameService.deleteGameMapping(id);

			mav.addObject("Result", "OK");
		} catch (Exception ex) {
			mav.addObject("Result", "ERROR");
			mav.addObject("Message", ex.getMessage());
		}
		return mav;
	}

	@RequestMapping(value = "/admin/gameMappings/addAll.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView addAllGameMapping(
			@RequestParam(value = "publisherId") Integer publisherId,
			@RequestParam(value = "providerId")Integer providerId
			) {
		logger.info("adding all GameMapping. publisherId={}, providerId={}", publisherId, providerId);
		ModelAndView mav = ViewUtils.createJsonView();
		try {
			publisherGameService.saveByPublisherAndProvider(publisherId, providerId);

			mav.addObject("Result", "OK");
		} catch (Exception ex) {
			mav.addObject("Result", "ERROR");
			mav.addObject("Message", ex.getMessage());
		}
		return mav;
	}
	
	@RequestMapping(value = "/admin/gameMappings/deleteAll.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public ModelAndView deleteAllGameMapping(
			@RequestParam(value = "publisherId") Integer publisherId,
			@RequestParam(value = "providerId") Integer providerId) {
		logger.info("deleting all GameMapping. publisherId={}, providerId={}", publisherId, providerId);
		ModelAndView mav = ViewUtils.createJsonView();
		try {
			publisherGameService.deleteByPublisherAndProvider(publisherId, providerId);
			
			mav.addObject("Result", "OK");
		} catch (Exception ex) {
			mav.addObject("Result", "ERROR");
			mav.addObject("Message", ex.getMessage());
		}
		return mav;
	}
	
	
	private JSONObject DomainToDto(PublisherGameMapping domain, GameImage gameImage){
		JSONObject jsonObject = new JSONObject();
		jsonObject.element("id", domain.getId());
		jsonObject.element("game.id", domain.getGame().getId());
		jsonObject.element("game.name", domain.getGame().getName());
		jsonObject.element("publisher.id", domain.getPublisher().getId());
		jsonObject.element("publisher.name", domain.getPublisher().getName());
		jsonObject.element("game.gameProvider.id", domain.getGame().getGameProvider().getId());
		jsonObject.element("game.gameProvider.name", domain.getGame().getGameProvider().getName());
		jsonObject.element("free", String.valueOf(domain.isFree()));
		jsonObject.element("charged", String.valueOf(domain.isCharged()));
		if (gameImage != null) {
			jsonObject.element("thumb_url", gameImage.getUrl());		
		}

		return jsonObject;
	}

	private PublisherGameMapping buildPublisherGameMapping(Long id, Integer publisherId, Long gameId, Boolean free, Boolean charged) {
		Game game = new Game();
		game.setId(gameId);
		Publisher publisher = new Publisher();
		publisher.setId(publisherId);
		PublisherGameMapping gameMapping = new PublisherGameMapping();
		gameMapping.setId(id);
		gameMapping.setGame(game);
		gameMapping.setPublisher(publisher);
		gameMapping.setFree(free);
		gameMapping.setCharged(charged);
		return gameMapping;
	}
	
}
