package com.gpi.backoffice.controller.report;

import com.gpi.backoffice.domain.report.Report;
import com.gpi.backoffice.domain.user.BackofficePermission;
import com.gpi.backoffice.exceptions.DuplicateReportException;
import com.gpi.backoffice.exceptions.ReportFileRequiredException;
import com.gpi.backoffice.service.report.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.naming.InvalidNameException;
import java.io.IOException;
import java.util.List;

@Controller
public class ReportCreationController {

	@Autowired
	private ReportService reportService;
	
	private static final Logger logger = LoggerFactory.getLogger(ReportCreationController.class);

	@RequestMapping(value = "/admin/report/new.do", method = RequestMethod.GET)
	@Secured({ "ADMIN" })
	public ModelAndView showReportCreation() {
		ModelAndView mav = new ModelAndView("admin/report/reportCreation");
		mav.addObject("report", new Report());
		return mav;
	}

	@RequestMapping(value = "/admin/report/new.do", method = RequestMethod.POST)
	@Secured({ "ADMIN" })
	public String saveReport(@RequestParam(value = "report_file", required = false) MultipartFile reportFile,
                             @RequestParam(value = "authorities", required = true) List<String> authorities,
			ModelMap model, @ModelAttribute("report") Report report, BindingResult result) {
		logger.info("Creating a new Report");
        try {
            for (String authority:authorities){
                report.addAuthority(BackofficePermission.valueOf(authority));
            }
			reportService.saveReport(report, reportFile);
		} catch (ReportFileRequiredException e) {
			logger.error("Report file not specified exception", e);

		} catch (IOException e) {
			logger.error("Unable to save report", e);

		} catch (InvalidNameException e) {
			logger.error("Invalid Name", e);

		} catch (DuplicateReportException e) {
			logger.error("Duplicate report", e);

		} catch (Exception e) {
			logger.error("Unexpected error", e);
		}

		return "redirect:/admin/report/reports.do";
	}

}
