package com.gpi.backoffice.controller;

import com.gpi.backoffice.controller.controller.TimeZoneAwareController;
import com.gpi.backoffice.domain.filters.RoundFilters;
import com.gpi.backoffice.dto.RoundDto;
import com.gpi.backoffice.dto.RoundReportDto;
import com.gpi.backoffice.dto.RoundSummaryDto;
import com.gpi.backoffice.service.report.RoundReportService;
import com.gpi.utils.ViewUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author igabba
 */
@Controller
public class RoundController extends TimeZoneAwareController {

    private static final Logger logger = LoggerFactory.getLogger(RoundController.class);

    @Autowired
    private RoundReportService roundReportService;

    @ModelAttribute("roundFilters")
    public RoundFilters getRoundFilters() {
        return new RoundFilters();
    }

    @RequestMapping(value = "/admin/round.do", method = RequestMethod.GET)
    @Secured({"PUBLISHER_ADMIN", "ADMIN", "TESTER", "SUPPORT"})
    public ModelAndView listByPublisher() {
        return new ModelAndView("admin/round/round");
    }

    @RequestMapping(value = "/admin/round.do", method = RequestMethod.POST)
    @Secured({"PUBLISHER_ADMIN", "ADMIN", "TESTER", "SUPPORT"})
    public ModelAndView roundByPublisher(@ModelAttribute("roundFilters") RoundFilters roundFilters,
                                         @RequestParam("jtStartIndex") Integer startIndex,
                                         @RequestParam("jtPageSize") Integer pageSize,
                                         @RequestParam("jtSorting") String sortExpression) throws Exception {
        ModelAndView mav = ViewUtils.createJsonView();

        RoundReportDto rounds = null;
        try {
            rounds = roundReportService.getRounds(roundFilters, startIndex, pageSize, sortExpression);
        } catch (Exception e) {
            logger.error("Error", e);
        }
        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", rounds != null ? rounds.getCount() : null);

        RoundSummaryDto records = getRecords(rounds != null ? rounds.getRounds() : null);

        mav.addObject("Records", records.getRecords());
        mav.addObject("totalBet", records.getBetAmount());
        mav.addObject("totalWin", records.getWinAmount());
        return mav;
    }

    private RoundSummaryDto getRecords(List<RoundDto> entries) {
        JSONArray records = new JSONArray();
        int winAmount = 0;
        int betAmount = 0;
        if (entries != null) {
            for (RoundDto entry : entries) {
                JSONObject jsonObject = new JSONObject();
                // conversion to string is to avoid getting javascript overflow in long value parsing
                jsonObject.element("id", String.valueOf(entry.getId()));
                jsonObject.element("bet_amount", entry.getBetAmount().toString());
                jsonObject.element("createdOn", entry.getCreatedOn().toString());
                jsonObject.element("win_amount", entry.getWinAmount());
                jsonObject.element("playerName", entry.getPlayerName());
                jsonObject.element("game", entry.getGame());
                jsonObject.element("provider", entry.getProvider());
                // conversion to string is to avoid getting javascript overflow in long value parsing
                jsonObject.element("gameRoundId", String.valueOf(entry.getGameRoundId()));
                jsonObject.element("publisher", entry.getPublisher());
                jsonObject.element("hasExternalInfo", entry.isHasExtendedInfo());
                records.add(jsonObject);
                winAmount += entry.getWinAmount();
                betAmount += entry.getBetAmount();
            }
        }

        return new RoundSummaryDto(records, winAmount, betAmount);
    }

    @RequestMapping(value = "/admin/roundList.do", method = RequestMethod.GET)
    @Secured({"ADMIN"})
    public ModelAndView list() {
        return new ModelAndView("admin/round/round");
    }

    @RequestMapping(value = "/admin/roundList.do", method = RequestMethod.POST)
    @Secured({"ADMIN"})
    public ModelAndView round(@ModelAttribute("roundFilters") RoundFilters roundFilters,
                              @RequestParam("jtStartIndex") Integer startIndex,
                              @RequestParam("jtPageSize") Integer pageSize,
                              @RequestParam("jtSorting") String sortExpression) throws Exception {
        ModelAndView mav = ViewUtils.createJsonView();
        RoundReportDto rounds = null;
        try {
            rounds = roundReportService.getRounds(roundFilters, startIndex, pageSize, sortExpression);
        } catch (Exception e) {
            logger.error("Error", e);
        }
        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", rounds != null ? rounds.getCount() : 0);

        RoundSummaryDto records = getRecords(rounds != null ? rounds.getRounds() : null);

        mav.addObject("Records", records.getRecords());
        mav.addObject("totalBet", records.getBetAmount());
        mav.addObject("totalWin", records.getWinAmount());
        return mav;
    }

}
