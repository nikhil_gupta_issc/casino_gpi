package com.gpi.backoffice.controller.controller;

import com.gpi.domain.currency.Currency;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Nacho on 4/28/2014.
 */
@Controller
public class UtilsController {

    private static final Logger log = LoggerFactory.getLogger(UtilsController.class);


    @ResponseBody
    @RequestMapping(value = "/admin/getCurrencies.do", method = RequestMethod.GET)
    @Secured({ "ADMIN" , "REPORT" })
    public String getCurrencies() {
        log.info("Looking for currencies.");
        Currency[] values = Currency.values();
        JSONObject object = new JSONObject();
        JSONArray records = new JSONArray();
        log.info("Creating response");
        for(Currency currency : values){
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", currency.ordinal());
            jsonObject.element("name", currency.getDescription());
            records.add(jsonObject);
        }
        object.put("data", records);
        return object.toString();
    }
}
