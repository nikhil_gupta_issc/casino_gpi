package com.gpi.backoffice.controller.user;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @RequestMapping(value = "/index.do", method = RequestMethod.GET)
    public String login() {
        return "index";
    }

}
