package com.gpi.backoffice.controller.controller;

import com.gpi.backoffice.controller.utils.TimeZoneUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Nacho on 2/27/14.
 */
public abstract class TimeZoneAwareController {

	private static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";

	private String dateTimeFormat = DEFAULT_DATE_TIME_FORMAT;

	@InitBinder
	public void initBinderAll(HttpServletRequest request, WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(dateTimeFormat);
		dateFormat.setTimeZone(TimeZoneUtils.getTimeZone(request));
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * Changes the format of {@link Date} that will be used for object binding
	 * @param dateTimeFormat the dateTimeFormat to set
	 */
	public void setDateTimeFormat(String dateTimeFormat) {
		this.dateTimeFormat = dateTimeFormat;
	}
}
