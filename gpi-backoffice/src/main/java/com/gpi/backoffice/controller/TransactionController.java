package com.gpi.backoffice.controller;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.service.api.BingoApi;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.domain.audit.FreeSpinTransaction;
import com.gpi.domain.audit.RefundedTransaction;
import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.RoundTransactionDto;
import com.gpi.service.freespin.FreeSpinTransactionService;
import com.gpi.service.refund.transaction.RefundedTransactionService;
import com.gpi.service.round.RoundService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.ViewUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author igabba
 */
@Controller
public class TransactionController {

    private static final Logger logger = LoggerFactory.getLogger(RoundController.class);

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private RefundedTransactionService refundedTransactionService;

    @Autowired
    private FreeSpinTransactionService freeSpinTransactionService;

    @Autowired
    private BingoApi bingoApi;

    @Autowired
    private RoundService roundService;

    @RequestMapping(value = "/admin/{roundId}/transaction.do", method = RequestMethod.GET)
    public ModelAndView list() {
        return new ModelAndView("admin/transaction/transaction");
    }

    @RequestMapping(value = "/admin/{roundId}/transaction.do", method = RequestMethod.POST)
    public ModelAndView round(@RequestParam("jtStartIndex") Integer startIndex,
                              @RequestParam("jtPageSize") Integer pageSize,
                              @RequestParam(value = "jtSorting", required = false) String sortExpression,
                              @PathVariable(value = "roundId") Long roundId) throws IOException, URISyntaxException {
        ModelAndView mav = ViewUtils.createJsonView();
        List<Transaction> transactions = transactionService.getTransactionsByRoundId(startIndex, pageSize, sortExpression, roundId);
        List<RefundedTransaction> refundedTransactions = refundedTransactionService.getRefundedTransactions(roundId);
        List<FreeSpinTransaction> freeSpinTransactions = freeSpinTransactionService.getFreeSpinTransactions(roundId);
        Map<Long, RefundedTransaction> refundedTransactionMap = getRefundedTransactionsMap(refundedTransactions);
        Map<Long, FreeSpinTransaction> freeSpinTransactionsMap = getFreeSpinTransactionsMap(freeSpinTransactions);
        Long count = transactionService.count(roundId, startIndex, pageSize, sortExpression);

        mav.addObject("Result", "OK");
        mav.addObject("TotalRecordCount", count);

        JSONArray records = new JSONArray();

        for (Transaction entry : transactions) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("id", entry.getId());
            jsonObject.element("createdOn", entry.getCreatedOn().toString());
            jsonObject.element("type", entry.getType().getDescription());
            jsonObject.element("amount", entry.getAmount());
            jsonObject.element("publisherId", entry.getExtTransactionId());
            jsonObject.element("gameTransactionId", entry.getGameTransactionId());
            jsonObject.element("success", entry.isSuccess());
            jsonObject.element("refunded", refundedTransactionMap.containsKey(entry.getId()));
            jsonObject.element("freeSpin", freeSpinTransactionsMap.containsKey(entry.getId()));
            records.add(jsonObject);
        }

        mav.addObject("Records", records);
        return mav;
    }

    @RequestMapping(value = "/admin/{roundId}/gameTransactions.do", method = RequestMethod.POST)
    public ModelAndView round(@PathVariable("roundId") Long roundId) throws IOException, URISyntaxException {
        ModelAndView mav = ViewUtils.createJsonView();


        JSONArray records = new JSONArray();

        BackofficeUser currentUser = UserUtils.getCurrentUser();
        Publisher publisher;
        Round round = roundService.getRoundById(roundId);
        if (round.getGame().getGameProvider().isExtendedInfo()) {

            if (currentUser.getPublisher() == null) {
                publisher = round.getPublisher();
            } else {
                publisher = currentUser.getPublisher();
            }
            List<RoundTransactionDto> roundDetails = bingoApi.getRoundDetails(round.getGameRoundId(), publisher);
            if (roundDetails != null && !roundDetails.isEmpty()) {
                mav.addObject("Records", getGameRecords(roundDetails));
            }
            mav.addObject("Result", "OK");
            mav.addObject("TotalRecordCount", roundDetails.size());
        }
        return mav;
    }

    private JSONArray getGameRecords(List<RoundTransactionDto> roundDetails) {
        JSONArray records = new JSONArray();
        for (RoundTransactionDto dto : roundDetails) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.element("createdOn", dto.getCreatedOn());
            jsonObject.element("type", dto.getType());
            jsonObject.element("extraInfo", dto.getExtraInfo());
            records.add(jsonObject);
        }
        return records;
    }

    private Map<Long, FreeSpinTransaction> getFreeSpinTransactionsMap(List<FreeSpinTransaction> freeSpinTransactions) {
        Map<Long, FreeSpinTransaction> freeSpinTransactionMap = new HashMap<>();
        for (FreeSpinTransaction freeSpinTransaction : freeSpinTransactions) {
            freeSpinTransactionMap.put(freeSpinTransaction.getTransaction().getId(), freeSpinTransaction);
        }
        return freeSpinTransactionMap;
    }

    private Map<Long, RefundedTransaction> getRefundedTransactionsMap(List<RefundedTransaction> refundedTransactions) {
        Map<Long, RefundedTransaction> refundedTransactionMap = new HashMap<>();
        for (RefundedTransaction refundedTransaction : refundedTransactions) {
            refundedTransactionMap.put(refundedTransaction.getTransaction().getId(), refundedTransaction);
        }
        return refundedTransactionMap;
    }
}
