package com.gpi.backoffice.exceptions;


public enum ReportError implements BackofficeError {
    REPORT_NOT_FOUND(10000),
    PARAMETER_VALIDATION_FAILED(10001),
    UNEXPECTED_ERROR(10002);
    private final int code;

    private ReportError(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return super.toString() + "[" + code + "]";
    }
}
