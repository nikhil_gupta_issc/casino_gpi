package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public class InvalidPermissionIdException extends Exception {
    private static final long serialVersionUID = -5673515873984583159L;

    public InvalidPermissionIdException() {
    }

    public InvalidPermissionIdException(String message) {
        super(message);
    }

    public InvalidPermissionIdException(String message, Throwable cause) {
        super(message, cause);
    }
}
