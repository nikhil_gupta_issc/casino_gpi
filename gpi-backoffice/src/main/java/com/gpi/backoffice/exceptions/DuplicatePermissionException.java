package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public class DuplicatePermissionException extends Exception {
    private static final long serialVersionUID = 51651073555440988L;

    public DuplicatePermissionException() {
    }

    public DuplicatePermissionException(String message) {
        super(message);
    }

    public DuplicatePermissionException(String message, Throwable cause) {
        super(message, cause);
    }
}
