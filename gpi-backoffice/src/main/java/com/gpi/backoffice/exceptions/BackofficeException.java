package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public class BackofficeException extends Exception {

    private static final long serialVersionUID = 186608076390251412L;
    private BackofficeError error;

    public BackofficeException(BackofficeError error) {
        super("Error Code " + error.getCode());
        this.error = error;
    }

    public BackofficeException(String message, BackofficeError error) {
        super("Error Code " + error.getCode() + ". " + message);
        this.error = error;
    }

    /**
     * @return the errorCode
     */
    public BackofficeError getError() {
        return error;
    }
}
