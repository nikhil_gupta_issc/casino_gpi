package com.gpi.backoffice.exceptions;

/**
 * Thrown when trying to create a report without the required file
 * 
 * @author csanchez
 * 
 */
public class ReportFileRequiredException extends Exception {

	private static final long serialVersionUID = 2246639197442817574L;
	
}
