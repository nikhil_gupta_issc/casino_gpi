package com.gpi.backoffice.exceptions;


public class ReportNotFoundException extends BackofficeException {

	private static final long serialVersionUID = 8979194005979789616L;
	
	public ReportNotFoundException() {
		super(ReportError.REPORT_NOT_FOUND);
	}
}
