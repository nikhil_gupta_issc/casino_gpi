package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public class InvalidPasswordConfirmationException extends Exception {
    private static final long serialVersionUID = 2044667005161634106L;

    public InvalidPasswordConfirmationException() {
    }

    public InvalidPasswordConfirmationException(String message) {
        super(message);
    }

    public InvalidPasswordConfirmationException(String message, Throwable cause) {
        super(message, cause);
    }
}
