package com.gpi.backoffice.exceptions;

/**
 * Thrown when trying to create a report with a name that is already in use
 * 
 * @author csanchez
 * 
 */
public class DuplicateReportException extends Exception {
	
	private static final long serialVersionUID = 1770689836170788044L;
	
}
