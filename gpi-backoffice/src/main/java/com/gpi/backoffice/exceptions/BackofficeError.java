package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public interface BackofficeError {
    public int getCode();
}
