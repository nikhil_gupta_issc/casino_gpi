package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public class UserNameAlreadyExistsException extends Exception {
    private static final long serialVersionUID = 7279356903151542649L;
}
