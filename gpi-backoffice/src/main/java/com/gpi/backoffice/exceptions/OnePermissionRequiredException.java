package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public class OnePermissionRequiredException extends Exception {
    private static final long serialVersionUID = -7140115324904670556L;

    public OnePermissionRequiredException() {
    }

    public OnePermissionRequiredException(String message) {
        super(message);
    }

    public OnePermissionRequiredException(String message, Throwable cause) {
        super(message, cause);
    }
}
