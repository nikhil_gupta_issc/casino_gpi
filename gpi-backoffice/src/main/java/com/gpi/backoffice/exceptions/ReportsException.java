package com.gpi.backoffice.exceptions;

/**
 * Created by Nacho on 12/19/13.
 */
public class ReportsException extends Exception {
    private static final long serialVersionUID = -4644561719274942491L;

    public ReportsException(String message) {
        super(message);
    }
}
