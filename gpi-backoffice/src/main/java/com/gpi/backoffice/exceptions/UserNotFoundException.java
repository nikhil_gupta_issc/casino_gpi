package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public class UserNotFoundException extends Exception {
    private static final long serialVersionUID = 7171699034333698556L;

    public UserNotFoundException() {
    }

    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
