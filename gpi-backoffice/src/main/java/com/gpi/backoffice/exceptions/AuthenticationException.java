package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public class AuthenticationException extends Exception {
    private static final long serialVersionUID = -4700525668587617870L;


	public AuthenticationException() {
	}

	public AuthenticationException(String message) {
		super(message);
	}
}
