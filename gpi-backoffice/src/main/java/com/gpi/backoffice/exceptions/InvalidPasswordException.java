package com.gpi.backoffice.exceptions;

/**
 * User: igabbarini
 */
public class InvalidPasswordException extends Exception {
    private static final long serialVersionUID = 5177518332307280067L;


    public InvalidPasswordException() {
    }

    public InvalidPasswordException(String message) {
        super(message);
    }

    public InvalidPasswordException(String message, Throwable cause) {
        super(message, cause);
    }
}
