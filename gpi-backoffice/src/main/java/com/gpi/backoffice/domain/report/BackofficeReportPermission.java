package com.gpi.backoffice.domain.report;

import com.gpi.backoffice.domain.user.BackofficePermission;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Table(name = "backoffice_report_permissions")
public class BackofficeReportPermission implements GrantedAuthority {
	
	private static final long serialVersionUID = -1262322964918472186L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

    @Column(name = "permission_id", nullable = false)
    private int permissionId;
        
    
    public BackofficeReportPermission(){
    	super();
    }
    
    public BackofficeReportPermission(BackofficePermission permission){
    	this.permissionId = permission.getValue();
    }
    
	@Override
	public String getAuthority() {
		return getPermission().toString();
	}
	
	BackofficePermission getPermission(){
		return BackofficePermission.parse(permissionId);
	}
	
	public void setPermission(BackofficePermission permission) {
		this.permissionId = permission.getValue();
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public int getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + permissionId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
        BackofficeReportPermission other = (BackofficeReportPermission) obj;
        return permissionId == other.permissionId;
    }
	
	@Override
	public String toString(){
		return "BackofficeUserPermission Perm=" + getPermission().toString();
	}
}
