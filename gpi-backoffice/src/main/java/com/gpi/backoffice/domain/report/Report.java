package com.gpi.backoffice.domain.report;

import com.gpi.backoffice.domain.user.BackofficePermission;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "report")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "name", unique = true, nullable = false, length = 100)
    private String name;
    @Column(name = "description", nullable = true, length = 255)
    private String description;
    @Column(name = "use_filter_by_hour", nullable = true)
    private Boolean useFilterByHour = false;
    @Column(name = "use_filter_by_day", nullable = true)
    private Boolean useFilterByDay = false;
    @Column(name = "use_filter_by_week", nullable = true)
    private Boolean useFilterByWeek = false;
    @Column(name = "use_month_picker", nullable = true)
    private Boolean useMonthPicker = false;
    @Column(name = "use_filter_by_month", nullable = true)
    private Boolean useFilterByMonth = false;
    @Column(name = "use_filter_by_year", nullable = true)
    private Boolean useFilterByYear = false;
    @Column(name = "use_filter_by_game", nullable = true)
    private Boolean useFilterByGame = false;
    @Column(name = "use_filter_by_currency", nullable = true)
    private Boolean useFilterByCurrency = false;
    @Column(name = "use_filter_by_user_name", nullable = true)
    private Boolean useFilterByUserName = false;
    @Column(name = "showFreeGames", nullable = true)
    private Boolean showFreeGames = false;
    @Column(name = "use_filter_by_publisher", nullable = true)
    private Boolean useFilterByPublisher = false;
    @Column(name = "use_filter_by_provider", nullable = true)
    private Boolean useFilterByProvider = false;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "report_id", referencedColumnName = "id", nullable = false)
    private Set<BackofficeReportPermission> authorities = new HashSet<>();

    public Report() {
    }

    public Report(String name) {
        this.name = name;
    }

    public Report(String name, String description, List<String> permissions, boolean useFilterByHour,
                  boolean useFilterByDay, boolean useFilterByWeek, boolean useFilterByMonth,
                  boolean useMonthPicker, boolean useFilterByYear, boolean useFilterByGame,
                  boolean useFilterByUserName, boolean useFilterByCurrency, boolean useFilterByPublisher, boolean useFilterByProvider) {
        this.name = name;
        this.description = description;
        for (String string : permissions) {
            addAuthority(BackofficePermission.valueOf(string.toUpperCase()));
        }
        this.useFilterByHour = useFilterByHour;
        this.useFilterByDay = useFilterByDay;
        this.useFilterByWeek = useFilterByWeek;
        this.useFilterByMonth = useFilterByMonth;
        this.useMonthPicker = useMonthPicker;
        this.useFilterByYear = useFilterByYear;
        this.useFilterByGame = useFilterByGame;
        this.useFilterByUserName = useFilterByUserName;
        this.useFilterByCurrency = useFilterByCurrency;
        this.useFilterByPublisher = useFilterByPublisher;
        this.useFilterByProvider = useFilterByProvider;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the useFilterByDay
     */
    public Boolean getUseFilterByDay() {
        return useFilterByDay;
    }

    /**
     * @param useFilterByDay the useFilterByDay to set
     */
    public void setUseFilterByDay(Boolean useFilterByDay) {
        this.useFilterByDay = useFilterByDay;
    }

    /**
     * @return the useFilterByMonth
     */
    public Boolean getUseFilterByMonth() {
        return useFilterByMonth;
    }

    /**
     * @param useFilterByMonth the useFilterByMonth to set
     */
    public void setUseFilterByMonth(Boolean useFilterByMonth) {
        this.useFilterByMonth = useFilterByMonth;
    }

    /**
     * @return the useFilterByWeek
     */
    public Boolean getUseFilterByWeek() {
        return useFilterByWeek;
    }

    /**
     * @param useFilterByWeek the useFilterByWeek to set
     */
    public void setUseFilterByWeek(Boolean useFilterByWeek) {
        this.useFilterByWeek = useFilterByWeek;
    }

    /**
     * @return the useFilterByYear
     */
    public Boolean getUseFilterByYear() {
        return useFilterByYear;
    }

    /**
     * @param useFilterByYear the useFilterByYear to set
     */
    public void setUseFilterByYear(Boolean useFilterByYear) {
        this.useFilterByYear = useFilterByYear;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the useFilterByGame
     */
    public Boolean getUseFilterByGame() {
        return useFilterByGame;
    }

    /**
     * @param useFilterByGame the useFilterByGame to set
     */
    public void setUseFilterByGame(Boolean useFilterByGame) {
        this.useFilterByGame = useFilterByGame;
    }

    /**
     * @return the useFilterByCurrency
     */
    public Boolean getUseFilterByCurrency() {
        return useFilterByCurrency;
    }

    /**
     * @param useFilterByCurrency the useFilterByCurrency to set
     */
    public void setUseFilterByCurrency(Boolean useFilterByCurrency) {
        this.useFilterByCurrency = useFilterByCurrency;
    }

    /**
     * @return the useFilterByHour
     */
    public Boolean getUseFilterByHour() {
        return useFilterByHour;
    }

    /**
     * @param useFilterByHour the useFilterByHour to set
     */
    public void setUseFilterByHour(Boolean useFilterByHour) {
        this.useFilterByHour = useFilterByHour;
    }

    /**
     * @return the useFilterByUserName
     */
    public Boolean getUseFilterByUserName() {
        return useFilterByUserName;
    }

    /**
     * @param useFilterByUserName the useFilterByUserName to set
     */
    public void setUseFilterByUserName(Boolean useFilterByUserName) {
        this.useFilterByUserName = useFilterByUserName;
    }

    /**
     * @return the useMonthPicker
     */
    public Boolean getUseMonthPicker() {
        return useMonthPicker;
    }

    /**
     * @param useMonthPicker the useMonthPicker to set
     */
    public void setUseMonthPicker(Boolean useMonthPicker) {
        this.useMonthPicker = useMonthPicker;
    }

    public Boolean getShowFreeGames() {
        return showFreeGames;
    }

    public void setShowFreeGames(Boolean showFreeGames) {
        this.showFreeGames = showFreeGames;
    }

    public Boolean getUseFilterByProvider() {
		return useFilterByProvider;
	}

	public void setUseFilterByProvider(Boolean useFilterByProvider) {
		this.useFilterByProvider = useFilterByProvider;
	}

	public Boolean getUseFilterByPublisher() {
        return useFilterByPublisher;
    }

    public void setUseFilterByPublisher(Boolean useFilterByPublisher) {
        this.useFilterByPublisher = useFilterByPublisher;
    }

    public Set<BackofficeReportPermission> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<BackofficeReportPermission> authorities) {
        this.authorities = authorities;
    }

    public void addAuthority(BackofficePermission permission) {
        BackofficeReportPermission newPermission = new BackofficeReportPermission(permission);
        this.authorities.add(newPermission);
    }

    public boolean hasPermission(BackofficePermission permission) {
        for (BackofficeReportPermission userPermission : getAuthorities()) {
            if (BackofficePermission.parse(userPermission.getPermissionId()).equals(permission)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasAtLeastOnePermission(List<BackofficePermission> permissions) {
        for (BackofficePermission permission : permissions) {
            for (BackofficeReportPermission reportPermission : getAuthorities()) {
                if (BackofficePermission.parse(reportPermission.getPermissionId()).equals(permission)) {
                    return true;
                }
            }
        }
        return false;
    }
}
