package com.gpi.backoffice.domain.filters;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by Ignacio on 12/11/13.
 */
public class PlayerFilters  implements ReportFilters{

    private Integer publisher;
    private String playerName;
    private boolean test;
    private boolean locked;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Integer getPublisher() {
        return publisher;
    }

    public void setPublisher(Integer publisher) {
        this.publisher = publisher;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("playerName", playerName)
                .append("publisher", publisher)
                .append("test", test)
                .append("locked", locked)
                .toString();
    }
}
