package com.gpi.backoffice.domain.user;

import com.gpi.backoffice.exceptions.InvalidPermissionIdException;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.publisher.Publisher;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "backoffice_users")
public class BackofficeUser implements Serializable, UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "user_name", unique = true, nullable = false, length = 100)
	private String username;

	@Column(name = "password", nullable = false, length = 128)
	private String password;

	@Column(name = "status", nullable = false)
	private Integer status;

	@Column(name = "created_on", nullable = false)
	private Date createdOn;

	@Column(name = "last_login", nullable = true)
	private Date lastLogin;

	@Column(name = "enabled", nullable = false)
	private boolean enabled;

	@Column(name = "acc_non_expired", nullable = false)
	private boolean accountNonExpired;

	@Column(name = "acc_non_locked", nullable = false)
	private boolean accountNonLocked;

	@Column(name = "cred_non_expired", nullable = false)
	private boolean credentialsNonExpired;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "publisher_id", referencedColumnName = "id")
    private Publisher publisher;

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id", referencedColumnName = "id")
    private GameProvider gameProvider;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private Set<BackofficeUserPermission> authorities = new HashSet<>();

	public BackofficeUser() {
		super();
	}

	/**
	 * Assigns a new permission to the user
	 * 
	 * @return true if the permission was assigned, false otherwise
	 */
    void addAuthority(BackofficePermission permission) {
		BackofficeUserPermission newPermission = new BackofficeUserPermission(permission);
		this.authorities.add(newPermission);
	}

    /**
     * Assigns a new permission to the user
     *
     * @param permissionId
     * @return true if the permission was assigned, false otherwise
     * @throws com.gpi.backoffice.exceptions.InvalidPermissionIdException
     */
    public BackofficePermission addAuthority(Integer permissionId) throws InvalidPermissionIdException {
        BackofficePermission permission = BackofficePermission.parse(permissionId);
        if (permission == null) {
            throw new InvalidPermissionIdException();
        }
        addAuthority(permission);
        return permission;
    }

	/**
	 * Removes a permission from a user
	 * 
	 * @return true if the permission was removed, false otherwise
	 * @throws com.gpi.backoffice.exceptions.OnePermissionRequiredException
	 *             when trying to remove the only remaining permission
	 * @throws InvalidPermissionIdException
	 */
	public BackofficePermission removeAuthority(Integer permissionId) throws InvalidPermissionIdException {
		BackofficePermission permission = BackofficePermission.parse(permissionId);
		if (permission == null) {
			throw new InvalidPermissionIdException();
		}
		removeAuthority(permission);
		return permission;
	}

	/**
	 * Removes a permission from a user
	 * 
	 * @return true if the permission was removed, false otherwise
	 */
    void removeAuthority(BackofficePermission permission) {
		BackofficeUserPermission toRemove = new BackofficeUserPermission(permission);
		this.authorities.remove(toRemove);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BackofficeUser other = (BackofficeUser) obj;
		if (getUsername() == null) {
			if (other.getUsername() != null)
				return false;
		} else if (!getUsername().equals(other.getUsername()))
			return false;
		return true;
	}

	public Integer getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public Integer getStatus() {
		return status;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setStatus(BackofficeUserStatus status) {
		this.status = status.getId();
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNotExpired) {
		this.accountNonExpired = accountNotExpired;
	}

	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public Set<BackofficeUserPermission> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<BackofficeUserPermission> authorities) {
		this.authorities = authorities;
	}

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public boolean hasPermission(BackofficePermission permission){
        for(BackofficeUserPermission userPermission : getAuthorities()){
            if(BackofficePermission.parse(userPermission.getPermissionId()).equals(permission)){
                return true;
            }
        }
        return false;
    }
    
    public List<BackofficePermission> getPermissions(){
        List<BackofficePermission> permissions = new ArrayList<>();
        for (BackofficeUserPermission backofficeUserPermission : authorities) {
            permissions.add(backofficeUserPermission.getPermission());
        }
        return permissions;
    }


	public GameProvider getGameProvider() {
		return gameProvider;
	}

	public void setGameProvider(GameProvider gameProvider) {
		this.gameProvider = gameProvider;
	}

	@Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BackofficeUser{");
        sb.append("id=").append(id);
        sb.append(", username='").append(username).append('\'');
        sb.append(", enabled=").append(enabled);
        if(publisher!=null){
            sb.append(", publisher='").append(publisher.getName()).append('\'');
        }
        sb.append(", status=").append(status);
        sb.append(", authorities=").append(authorities);
        sb.append('}');
        return sb.toString();
    }
}
