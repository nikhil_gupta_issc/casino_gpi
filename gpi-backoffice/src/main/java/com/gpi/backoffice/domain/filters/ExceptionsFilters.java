package com.gpi.backoffice.domain.filters;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by Nacho on 12/19/13.
 */
public class ExceptionsFilters implements ReportFilters{

    private String name;
    private String dateFrom;
    private String dateUntil;
    private Integer game;
    private Integer publisher;
    private String player;

    public String getDateUntil() {
        return dateUntil;
    }

    public void setDateUntil(String dateUntil) {
        this.dateUntil = dateUntil;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Integer getGame() {
        return game;
    }

    public void setGame(Integer game) {
        this.game = game;
    }

    public Integer getPublisher() {
        return publisher;
    }

    public void setPublisher(Integer publisher) {
        this.publisher = publisher;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("dateFrom", dateFrom)
                .append("dateUntil", dateUntil)
                .append("game", game)
                .append("publisher", publisher)
                .append("player", player)
                .toString();
    }
}
