package com.gpi.backoffice.domain.filters;

public class FreeSpinFilters implements ReportFilters{

	private String playerName;
	private Long gameId;
	
	/**
	 * @return the playerName
	 */
	public String getPlayerName() {
		return playerName;
	}
	/**
	 * @param playerName the playerName to set
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	/**
	 * @return the gameId
	 */
	public Long getGameId() {
		return gameId;
	}
	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}
	
	
}
