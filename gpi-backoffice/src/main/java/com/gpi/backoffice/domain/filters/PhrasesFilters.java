package com.gpi.backoffice.domain.filters;

/**
 * Created by Nacho on 12/12/13.
 */
public class PhrasesFilters implements ReportFilters{

    private String language;
    private String text;


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
