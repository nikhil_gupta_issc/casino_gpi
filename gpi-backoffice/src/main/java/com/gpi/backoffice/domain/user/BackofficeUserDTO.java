package com.gpi.backoffice.domain.user;


import com.gpi.domain.publisher.Publisher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Data transfer object used for the creation of users
 *
 * @author Carlos
 */
public class BackofficeUserDTO {

    private final Publisher publisher;

    private String username;

    private String password;

    private String confirmPassword;

    private List<BackofficePermission> authorities;

    public BackofficeUserDTO(String username, String password, String confirmPassword, Publisher publisher,
                             List<String> permissions) {

        this.username = username;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.publisher = publisher;
        authorities = new ArrayList<>(permissions.size());
        for (String permission : permissions) {
            authorities.add(BackofficePermission.valueOf(permission));
        }
    }

    public BackofficeUser GenerateUser() {
        BackofficeUser user = new BackofficeUser();
        user.setUsername(this.username);
        user.setPassword(this.password);
        user.setEnabled(true);
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setStatus(BackofficeUserStatus.ACTIVE);
        user.setPublisher(publisher);

        for (BackofficePermission permission : this.authorities) {
            user.getAuthorities().add(new BackofficeUserPermission(permission));
        }

        user.setCreatedOn(new Date());
        return user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public List<BackofficePermission> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<BackofficePermission> authorities) {
        if (authorities == null) {
            return;
        }
        this.authorities = authorities;
    }

}
