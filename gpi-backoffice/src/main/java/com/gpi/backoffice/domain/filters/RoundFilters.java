package com.gpi.backoffice.domain.filters;

import com.gpi.backoffice.domain.user.BackofficeUser;

import java.util.Date;

/**
 * Created by Ignacio on 12/4/13.
 */
public class RoundFilters {

    private Long roundId;
    private Integer publisher;
    private Long game;
    private Date dateFrom;
    private Date dateUntil;
    private String playerName;
    private String currency;
    private boolean bonus;
    private boolean bingo;
    private boolean jackpot;
    private boolean error;
	private BackofficeUser backofficeUser;
	private boolean includeTest = true;
	private String provider;

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public boolean isIncludeTest() {
		return includeTest;
	}

	public void setIncludeTest(boolean includeTest) {
		this.includeTest = includeTest;
	}

	public BackofficeUser getBackofficeUser() {
		return backofficeUser;
	}

	public void setBackofficeUser(BackofficeUser backofficeUser) {
		this.backofficeUser = backofficeUser;
	}

	public boolean isBonus() {
        return bonus;
    }

    public void setBonus(boolean bonus) {
        this.bonus = bonus;
    }

    public boolean isBingo() {
        return bingo;
    }

    public void setBingo(boolean bingo) {
        this.bingo = bingo;
    }

    public boolean isJackpot() {
        return jackpot;
    }

    public void setJackpot(boolean jackpot) {
        this.jackpot = jackpot;
    }


    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

	public Date getDateUntil() {
		return dateUntil;
	}

	public void setDateUntil(Date dateUntil) {
		this.dateUntil = dateUntil;
	}

	public Long getRoundId() {
        return roundId;
    }

    public void setRoundId(Long roundId) {
        this.roundId = roundId;
    }

    public Integer getPublisher() {
        return publisher;
    }

    public void setPublisher(Integer publisher) {
        this.publisher = publisher;
    }

    public Long getGame() {
        return game;
    }

    public void setGame(Long game) {
        this.game = game;
    }
}
