package com.gpi.backoffice.domain.user;

public enum BackofficeUserStatus {
	ACTIVE(1);

	private final Integer id;
	
	private BackofficeUserStatus(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

}
