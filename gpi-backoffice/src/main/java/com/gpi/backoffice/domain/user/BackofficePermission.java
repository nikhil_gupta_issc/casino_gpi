package com.gpi.backoffice.domain.user;

public enum BackofficePermission {

    PUBLISHER_ADMIN(1,true),
    ADMIN(2, false),
    AUDIT_ADMIN(3, false),
    TESTER(6, false),
    FREE_SPIN(7, true),
    PROVIDER(8, false),
    REPORT(9, true),
    SUPPORT(10, true);

    private final Integer id;
    /**
     * this property is set to true if the Permission can be used on a publisherUser
     */
    private final boolean availableOnPublisher;

    BackofficePermission(int value, boolean availableOnPublisher) {
        this.id = value;
        this.availableOnPublisher = availableOnPublisher;
    }

    public static BackofficePermission parse(int id) {
        BackofficePermission auth = null;
        for (BackofficePermission item : BackofficePermission.values()) {
            if (item.getValue() == id) {
                auth = item;
                break;
            }
        }
        return auth;
    }

    public int getValue() {
        return id;
    }

    public boolean isAvailableOnPublisher() {
        return availableOnPublisher;
    }
}
