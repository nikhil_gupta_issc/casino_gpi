package com.gpi.backoffice.service.user;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.domain.user.BackofficeUserDTO;
import com.gpi.backoffice.exceptions.*;

/**
 * This service provides the required methods to manipulate the users of the
 * backoffice
 *
 * @author Carlos
 */
public interface UserService {
    /**
     * Creates a new Backoffice user using a DTO as prototype
     *
     * @return The user created
     */
    public BackofficeUser createUser(BackofficeUserDTO dto) throws InvalidUsernameException,
            InvalidPasswordException, UserNameAlreadyExistsException,
            OnePermissionRequiredException, InvalidPasswordConfirmationException;

    /**
     * Retrieves a user by his username
     */
    public BackofficeUser findUserByUsername(String username);

    /**
     * Saves the changes made to a previously created backoffice user
     *
     * @throws OnePermissionRequiredException
     */
    public void updateUser(BackofficeUser user) throws UserNotFoundException, InvalidUsernameException,
            InvalidPasswordException, OnePermissionRequiredException;

    /**
     * Deletes a previously created user
     */
    public void delete(BackofficeUser user) throws UserNotFoundException;

    public BackofficeUser findUserById(Integer userId);

    BackofficeUser findUserByUsernameAndPassword(String user, String password) throws UserNotFoundException, InvalidPasswordConfirmationException;
}
