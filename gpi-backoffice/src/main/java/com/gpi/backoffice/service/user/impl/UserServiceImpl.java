package com.gpi.backoffice.service.user.impl;

import com.gpi.backoffice.dao.user.UserDao;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.domain.user.BackofficeUserDTO;
import com.gpi.backoffice.exceptions.*;
import com.gpi.backoffice.service.user.UserService;
import com.gpi.backoffice.utils.UserUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

public class UserServiceImpl implements UserService {

    private UserDao userDao;

    private PasswordEncoder passwordEncoder;

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    @Transactional
    public BackofficeUser createUser(BackofficeUserDTO dto) throws InvalidUsernameException,
            InvalidPasswordException, UserNameAlreadyExistsException,
            OnePermissionRequiredException, InvalidPasswordConfirmationException {
        log.debug("Creating new BackOffice");
        BackofficeUser user = dto.GenerateUser();

        log.debug("BackOffice User created = {}", user);

        UserUtils.validateUser(user);

        if (!dto.getConfirmPassword().equals(user.getPassword())) {
            throw new InvalidPasswordConfirmationException();
        }

        BackofficeUser foundUser = findUserByUsername(user.getUsername());
        if (foundUser != null) {
            throw new UserNameAlreadyExistsException();
        }
        log.debug("Encoding password...");
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        log.debug("Saving BackOffice User.");
        return userDao.saveUser(user);
    }

    @Override
    public BackofficeUser findUserByUsername(String username) {
        return userDao.findByUserName(username);
    }

    @Override
    public BackofficeUser findUserById(Integer userId) {
        return userDao.findById(userId);
    }

    @Override
    public BackofficeUser findUserByUsernameAndPassword(String user, String password) throws UserNotFoundException, InvalidPasswordConfirmationException {
        log.info("Finding user by name and password. User: {}", user);
        BackofficeUser backofficeUser = findUserByUsername(user);
        if(backofficeUser==null){
            throw new UserNotFoundException(String.format("Username %s doesn't exists", user));
        }

        if(!passwordEncoder.matches(password, backofficeUser.getPassword())){
            throw new InvalidPasswordConfirmationException("Password doesn't match.");
        }

        return backofficeUser;
    }

    @Override
    public void updateUser(BackofficeUser user) throws UserNotFoundException, InvalidUsernameException,
            InvalidPasswordException, OnePermissionRequiredException {
        if (user == null) {
            throw new UserNotFoundException();
        }

        UserUtils.validateUser(user);

        userDao.updateUser(user);
    }

    @Override
    public void delete(BackofficeUser user) throws UserNotFoundException {
        if (user == null) {
            throw new UserNotFoundException();
        }
        userDao.deleteUser(user);
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
}
