package com.gpi.backoffice.service.user.publisher;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.exceptions.AuthenticationException;
import com.gpi.backoffice.exceptions.InvalidPasswordConfirmationException;
import com.gpi.backoffice.exceptions.InvalidPermissionIdException;
import com.gpi.backoffice.exceptions.UserNotFoundException;
import com.gpi.backoffice.service.user.PasswordService;
import com.gpi.backoffice.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PublisherUserServiceImpl implements PublisherUserService {


    private static final Logger log = LoggerFactory.getLogger(PublisherUserServiceImpl.class);
    private PasswordService passwordService;
    private UserService userService;

    @Override
    public void resetUserPassword(String newPassword, Integer userId, BackofficeUser currentUser) throws InvalidPermissionIdException,
            UserNotFoundException, InvalidPasswordConfirmationException, AuthenticationException {
        log.info("resetting password for user={}");
        BackofficeUser backofficeUser = userService.findUserById(userId);

        if (!backofficeUser.getPublisher().equals(currentUser.getPublisher())) {
            throw new InvalidPermissionIdException();
        }

        passwordService.changePassword(newPassword, userId);

    }

    public void setPasswordService(PasswordService passwordService) {
        this.passwordService = passwordService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
