package com.gpi.backoffice.service.report;


import java.util.List;

import com.gpi.backoffice.domain.report.Report;

/**
 * This service allows to retrieve a list of reports divided in pages
 * 
 * @author csanchez
 * 
 */
public interface ReportListService {
	/**
	 * Retrieves a list of report
	 * 
	 * @param sortExpression
	 *            the expression that will be used to sort the data
	 */
	public List<Report> list(int startIndex, int pageSize, String sortExpression);

	/**
	 * Retrieves the total number of reports
	 */
	public Long getCount();
}
