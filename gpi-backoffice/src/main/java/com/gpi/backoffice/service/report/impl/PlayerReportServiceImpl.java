package com.gpi.backoffice.service.report.impl;

import com.gpi.backoffice.domain.filters.PlayerFilters;
import com.gpi.backoffice.domain.filters.ReportFilters;
import com.gpi.backoffice.dto.PlayerDto;
import com.gpi.backoffice.dto.ReportDto;
import com.gpi.backoffice.service.report.ReportsService;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.FilterOperator;
import com.gpi.backoffice.query.filter.FirstLevelFieldFilter;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.backoffice.query.filter.SecondLevelFieldFilter;
import com.gpi.domain.player.Player;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ignacio on 12/11/13.
 */
public class PlayerReportServiceImpl implements ReportsService<PlayerDto> {

    private static final Logger log = LoggerFactory.getLogger(PlayerReportServiceImpl.class);

    private QueryObject<Player> queryObject;

    @Override
    public ReportDto<PlayerDto> getReportDto(ReportFilters reportFilters, Integer startIndex, Integer pageSize, String sortExpression) {
        log.info("Looking for players...");
        PlayerFilters playerFilters = (PlayerFilters) reportFilters;
        List<HibernateFilter> filters = generateFilters(playerFilters);

        List<Player> players = queryObject.get(startIndex, pageSize, sortExpression, filters);
        Long count = queryObject.getCount(filters);
        List<PlayerDto> playerDtos = generatePlayerDtos(players);
        return new ReportDto<>(playerDtos, count);
    }

    private List<PlayerDto> generatePlayerDtos(List<Player> players) {
        List<PlayerDto> playerDtos = new ArrayList<>(players.size());
        for (Player player : players) {
            PlayerDto playerDto = new PlayerDto();
            playerDto.setName(player.getName());
            playerDto.setCreatedOn(new DateTime(player.getCreatedOn()));
            playerDto.setId(player.getId());
            playerDto.setPublisher(player.getPublisher().getName());
            playerDtos.add(playerDto);
        }
        return playerDtos;
    }

    private List<HibernateFilter> generateFilters(PlayerFilters playerFilters) {
        List<HibernateFilter> filters = new ArrayList<>();
        filters = processPlayerFilter(filters, playerFilters.getPlayerName());
        filters = processPublisherFilter(filters, playerFilters.getPublisher());
        return filters;
    }

    private List<HibernateFilter> processPublisherFilter(List<HibernateFilter> filters, Integer publisher) {
        if (publisher != null) {
            log.info("Looking for publisher=[{}]", publisher);
            SecondLevelFieldFilter filter = new SecondLevelFieldFilter("publisher", "id", FilterOperator.EQ, publisher);
            filters.add(filter);
        }
        return filters;
    }

    private List<HibernateFilter> processPlayerFilter(List<HibernateFilter> filters, String playerName) {
        if (StringUtils.isNotBlank(playerName)) {
            log.info("Applying filter player=[{}]", playerName);
            FirstLevelFieldFilter fieldFilter = new FirstLevelFieldFilter("name", FilterOperator.EQ, playerName);
            filters.add(fieldFilter);
        }
        return filters;
    }

    public void setQueryObject(QueryObject<Player> queryObject) {
        this.queryObject = queryObject;
    }
}
