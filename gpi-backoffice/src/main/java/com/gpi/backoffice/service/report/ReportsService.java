package com.gpi.backoffice.service.report;

import com.gpi.backoffice.domain.filters.ReportFilters;
import com.gpi.backoffice.dto.ReportDto;
import com.gpi.backoffice.exceptions.ReportsException;

/**
 * Created by Nacho on 12/19/13.
 */
public interface ReportsService<T> {

    public ReportDto<T> getReportDto(ReportFilters filters, Integer startIndex, Integer pageSize, String sortExpression) throws ReportsException;
}
