package com.gpi.backoffice.service.report;

import com.gpi.backoffice.domain.report.Report;
import com.gpi.backoffice.exceptions.*;
import com.gpi.utils.ValidationUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.InvalidNameException;
import java.io.IOException;

public interface ReportService {

    public Report findByName(String name);

    public Report saveReport(Report report, MultipartFile reportFile) throws IOException, InvalidNameException,
            DuplicateReportException, ReportFileRequiredException, UserNotFoundException, InvalidUsernameException,
            ValidationUtils.InvalidEmailException, InvalidPasswordException, OnePermissionRequiredException;

    void removeReport(Integer id) throws ReportNotFoundException;
}
