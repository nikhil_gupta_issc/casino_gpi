package com.gpi.backoffice.service.api;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Created by nacho on 29/04/15.
 */
public class HttpClientServiceImpl implements HttpClientService {

    private static final Logger log = LoggerFactory.getLogger(HttpClientServiceImpl.class);

    @Override
    public Object sendPostRequest(String url, Map<String, String> params) throws IOException, URISyntaxException {
        return this.sendHttpRequest(RequestMethod.POST, url, params, null);
    }

    @Override
    public Object sendGetRequest(String s, Map<String, String> params) throws IOException, URISyntaxException {
        return this.sendHttpRequest(RequestMethod.GET, s, params, null);
    }

    @Override
    public Object sendGetRequest(String url, Map<String, String> params, TypeReference typeReference) throws IOException, URISyntaxException {
        return this.sendHttpRequest(RequestMethod.GET, url, params, typeReference );
    }

    private Object sendHttpRequest(RequestMethod method, String url, Map<String, String> params, TypeReference typeReference) throws IOException, URISyntaxException {
        Object responseObject = null;
        try {
            HttpRequestBase httpRequest;
            DefaultHttpClient httpClient = new DefaultHttpClient();
            if (RequestMethod.POST.equals(method)) {
                httpRequest = new HttpPost(url);
            } else {
                httpRequest = new HttpGet(url);
            }
            httpRequest.addHeader("accept", "application/json");
            URIBuilder uriBuilder = new URIBuilder(httpRequest.getURI());
            log.debug("Contructing url params...");
            for (String key : params.keySet()) {
                uriBuilder.addParameter(key, params.get(key));
            }

            URI uri = uriBuilder.build();
            log.debug("uri build: " + uri.toString());
            httpRequest.setURI(uri);
            HttpResponse response = httpClient.execute(httpRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
            }

            if (typeReference != null) {
                ObjectMapper mapper = new ObjectMapper();
                responseObject = mapper.readValue(response.getEntity().getContent(), typeReference);
            }
            httpClient.getConnectionManager().shutdown();

        } catch (IOException | URISyntaxException e) {
            log.error("Error while doing a request.", e);
            throw e;
        }
        return responseObject;
    }

}
