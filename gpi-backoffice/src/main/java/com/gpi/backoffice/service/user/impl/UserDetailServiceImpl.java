package com.gpi.backoffice.service.user.impl;

import com.gpi.backoffice.dao.user.UserDao;
import com.gpi.backoffice.domain.user.BackofficeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailServiceImpl implements UserDetailsService {
	
	private UserDao userDao;
    private static final Logger log = LoggerFactory.getLogger(UserDetailServiceImpl.class);

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
        log.debug("Loading user by username = [{}]", username);
		BackofficeUser user = userDao.findByUserName(username);
        if (user==null){
            throw new UsernameNotFoundException("Username not found" + username);
        }
		return user;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
