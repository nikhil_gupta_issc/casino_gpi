package com.gpi.backoffice.service.report.impl;

import com.gpi.backoffice.domain.filters.RoundFilters;
import com.gpi.backoffice.domain.user.BackofficePermission;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.dto.RoundDto;
import com.gpi.backoffice.dto.RoundReportDto;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.*;
import com.gpi.backoffice.service.report.RoundReportService;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.domain.audit.Round;
import com.gpi.domain.currency.Currency;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.player.PlayerService;
import com.gpi.service.publisher.PublisherService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * User: igabbarini
 */
public class RoundReportServiceImpl implements RoundReportService {

    private static final Logger log = LoggerFactory.getLogger(RoundReportServiceImpl.class);
    private PublisherService publisherService;
    private PlayerService playerService;
    private QueryObject<Round> queryObject;

    @Override
    public RoundReportDto getRounds(RoundFilters roundFilters, Integer startIndex, Integer pageSize, String sortExpression) throws ParseException, NonExistentPublisherException {
        if (roundFilters.getBackofficeUser() == null) {
            roundFilters.setBackofficeUser(UserUtils.getCurrentUser());
        }
        List<HibernateFilter> filters = getFilters(roundFilters);
        List<Round> rounds = queryObject.get(startIndex, pageSize, sortExpression, filters);
        Long count = queryObject.getCount(filters);
        List<RoundDto> roundDtos = getRoundDtos(rounds);
        return new RoundReportDto(roundDtos, count);
    }

    private List<RoundDto> getRoundDtos(List<Round> rounds) {
        List<RoundDto> roundDtos = new ArrayList<>(rounds.size());
        for (Round round : rounds) {
            RoundDto roundDto = new RoundDto();
            roundDto.setGameRoundId(round.getGameRoundId());
            roundDto.setGame(round.getGame().getName());
            roundDto.setPublisher(round.getPublisher().getName());
            roundDto.setCreatedOn(new DateTime(round.getCreatedOn()));
            roundDto.setBetAmount(round.getBetAmount());
            roundDto.setWinAmount(round.getWinAmount());
            roundDto.setCurrency(round.getCurrency().getDescription());
            roundDto.setError(round.isError());
            roundDto.setPlayerName(round.getPlayer().getName());
            roundDto.setProvider(round.getGame().getGameProvider().getName());
            roundDto.setId(round.getId());
            roundDto.setHasExtendedInfo(round.getGame().getGameProvider().isExtendedInfo());
            roundDtos.add(roundDto);
        }

        return roundDtos;
    }

    private List<HibernateFilter> getFilters(RoundFilters roundFilters) throws NonExistentPublisherException, ParseException {
        List<HibernateFilter> filters = new ArrayList<>();

        filters = processPublisherFilter(filters, roundFilters.getBackofficeUser(), roundFilters.getPublisher());

        filters = processProviderFilter(filters, roundFilters.getBackofficeUser(), roundFilters.getProvider());

        filters = processRoundFilter(filters, roundFilters.getRoundId());

        filters = processDateFromFilter(filters, roundFilters.getDateFrom());

        filters = processDateUntilFilter(filters, roundFilters.getDateFrom(), roundFilters.getDateUntil());
        filters = processPlayerNameFilter(filters, roundFilters.getPlayerName(), roundFilters.getPublisher());
        filters = processGameFilter(filters, roundFilters.getGame());
        filters = processCurrencyFilter(filters, roundFilters.getCurrency());
        filters = processErrorFilter(filters, roundFilters.isError());

        return filters;
    }

    private List<HibernateFilter> processProviderFilter(List<HibernateFilter> filters, BackofficeUser backofficeUser, String provider) {
        if (backofficeUser.getGameProvider() != null) {
            log.debug("The user is a provider so we must only show his games.");
            ThirdLevelFieldFilter filter = new ThirdLevelFieldFilter("game", "gameProvider", "name",
                    FilterOperator.EQ, backofficeUser.getGameProvider().getName());
            filters.add(filter);
            return filters;
        } else if (StringUtils.isNotBlank(provider)) {
            ThirdLevelFieldFilter filter = new ThirdLevelFieldFilter("game", "gameProvider", "name",
                    FilterOperator.EQ, provider);
            filters.add(filter);

        }
        return filters;

    }

    private List<HibernateFilter> processErrorFilter(List<HibernateFilter> filters, boolean error) {
        log.debug("Show only rounds with error={}", error);
        if (error) {
            FirstLevelFieldFilter filter = new FirstLevelFieldFilter("error", FilterOperator.EQ, "1");
            filters.add(filter);
        }
        return filters;
    }

    private List<HibernateFilter> processCurrencyFilter(List<HibernateFilter> filters, String currency) {
        log.info("Applying filter currency={}", currency);
        if (StringUtils.isNotEmpty(currency)) {
            FirstLevelFieldFilter filter = new FirstLevelFieldFilter("currency", FilterOperator.EQ,
                    Currency.valueOf(currency));
            filters.add(filter);
        }
        return filters;
    }

    private List<HibernateFilter> processGameFilter(List<HibernateFilter> filters, Long game) {
        log.info("Applying filter game={}", game);
        if (game != null) {
            SecondLevelFieldFilter filter = new SecondLevelFieldFilter("game", "id", FilterOperator.EQ, game);
            filters.add(filter);
        }
        return filters;
    }

    private List<HibernateFilter> processPlayerNameFilter(List<HibernateFilter> filters, String playerName,
                                                          Integer publisher) {
        if(publisher == null){
        	BackofficeUser user = UserUtils.getCurrentUser();        
        	if(user.getPublisher() != null){
        		publisher =user.getPublisher().getId();
        	}
        }
    	if (StringUtils.isNotBlank(playerName)) {
            log.info("Applying filter player={} and publisher={}", playerName, publisher);
            
            List<Player> players;
            if( publisher != null ){
            	players = playerService.findPlayersByPlayerNameAndPublisher(playerName, publisher);
            }else{
            	players = playerService.findPlayersByPlayerName(playerName);
            }
            
            
            if(players == null){
            	filters.add(new FirstLevelFieldFilter("1",FilterOperator.EQ,"0"));
            	return filters;
            }
            
            List<Long> playerIds = new ArrayList<Long>();

            
            for(Player p : players){
            	playerIds.add(p.getId());
            }
                        
            SecondLevelFieldFilter fieldFilter = new SecondLevelFieldFilter("player", "id", FilterOperator.IN, playerIds.toArray() );
            filters.add(fieldFilter);
        }
        return filters;
    }

    private List<HibernateFilter> processDateUntilFilter(List<HibernateFilter> filters, Date dateFrom, Date dateUntil) {
        if (dateUntil != null && !dateUntil.equals(dateFrom)) {
            log.info("Applying filter dataFrom={} and dateUntil={}", dateFrom, dateUntil);
            FirstLevelFieldFilter dateUntilFilter = new FirstLevelFieldFilter("createdOn", FilterOperator.LE, dateUntil);
            filters.add(dateUntilFilter);
        }

        return filters;
    }

    private List<HibernateFilter> processDateFromFilter(List<HibernateFilter> filters, Date dateFrom) {

        if (dateFrom == null) {
            dateFrom = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        }
        log.info("Applying filter dataFrom={}", dateFrom);
        FirstLevelFieldFilter dateFromFilter = new FirstLevelFieldFilter("createdOn", FilterOperator.GE, dateFrom);
        filters.add(dateFromFilter);
        return filters;
    }

    private List<HibernateFilter> processRoundFilter(List<HibernateFilter> filters, Long roundId) {
        if (roundId != null) {
            log.info("Applying filter for round with id={}", roundId);
            FirstLevelFieldFilter roundIdFilter = new FirstLevelFieldFilter("id", FilterOperator.EQ,
                    roundId);
            filters = new ArrayList<>();
            filters.add(roundIdFilter);
        }
        return filters;
    }

    private List<HibernateFilter> processPublisherFilter(List<HibernateFilter> filters, BackofficeUser currentUser,
                                                         Integer publisherId) throws NonExistentPublisherException {
        if (publisherId != null) {
            if (currentUser.hasPermission(BackofficePermission.TESTER) || currentUser.hasPermission(BackofficePermission.ADMIN)) {

                log.info("User has {} permission. Looking for publisher={}", currentUser.getPermissions(), publisherId);
                SecondLevelFieldFilter filter = new SecondLevelFieldFilter("publisher", "id", FilterOperator.EQ,
                        publisherId);
                filters.add(filter);
            }
        } else if (currentUser.getGameProvider() != null) {
            return filters;
        } else if (!currentUser.hasPermission(BackofficePermission.ADMIN) && !currentUser.hasPermission(BackofficePermission.TESTER)) {
            log.info("User [{}] isn't an Admin user so the filter applied is for publisher=[{}]",
                    currentUser, currentUser.getPublisher());
            Publisher publisher = publisherService.findByName(currentUser.getPublisher().getName());
            SecondLevelFieldFilter filter = new SecondLevelFieldFilter("publisher", "id", FilterOperator.EQ, publisher
                    .getId());
            filters.add(filter);
        }

        return filters;
    }

    public void setPublisherService(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public void setQueryObject(QueryObject<Round> queryObject) {
        this.queryObject = queryObject;
    }
}
