package com.gpi.backoffice.service.api;

import com.google.common.base.Joiner;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.PlayerLockedDto;
import com.gpi.dto.RoundTransactionDto;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BingoApiImpl implements BingoApi {

    private static final Logger log = LoggerFactory.getLogger(BingoApiImpl.class);

    private HttpClientService httpClientService;

    private String bingoUrl;

    @Override
    public List<PlayerLockedDto> getLockedPlayers(String publisherName) throws URISyntaxException, IOException {
        List<PlayerLockedDto> players = null;
        Map<String, String> params = new HashMap<>();
        params.put("publisherName", publisherName);
        players = (List<PlayerLockedDto>) httpClientService.sendGetRequest(bingoUrl + "/player/locked", params, new TypeReference<List<PlayerLockedDto>>() {
        });
        return players;
    }

    @Override
    public void unlockPlayers(List<Long> playersId, String publisherName) throws URISyntaxException, IOException {
        Map<String, String> params = new HashMap<>();
        params.put("publisherName", publisherName);
        String ids = Joiner.on(",").join(playersId);
        params.put("playerIds", ids);
        httpClientService.sendPostRequest(bingoUrl + "/player/unlock", params);
    }

    @Override
    public List<RoundTransactionDto> getRoundDetails(Long roundId, Publisher publisher) throws IOException, URISyntaxException {
        Map<String, String> params = new HashMap<>();
        params.put("publisherName", publisher.getName());
        params.put("roundId", roundId.toString());
        return (List<RoundTransactionDto>) httpClientService.sendGetRequest(bingoUrl + "/transactions", params, new TypeReference<List< RoundTransactionDto>>() {
           });
    }

    public void setHttpClientService(HttpClientService httpClientService) {
        this.httpClientService = httpClientService;
    }

    public void setBingoUrl(String bingoUrl) {
        this.bingoUrl = bingoUrl;
    }
}
