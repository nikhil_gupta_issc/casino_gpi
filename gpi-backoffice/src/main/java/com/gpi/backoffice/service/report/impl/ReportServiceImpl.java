package com.gpi.backoffice.service.report.impl;

import com.gpi.backoffice.dao.report.ReportDao;
import com.gpi.backoffice.domain.report.Report;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.service.report.ReportService;
import com.gpi.backoffice.service.user.UserService;
import com.gpi.backoffice.utils.ReportUtils;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.backoffice.exceptions.*;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.ValidationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.InvalidNameException;
import java.io.File;
import java.io.IOException;

public class ReportServiceImpl implements ReportService {

	private ReportDao reportDao;
	
	private UserService userService;

	private static final Logger logger = LoggerFactory.getLogger(ReportServiceImpl.class);

	@Override
	@Transactional
	public Report findByName(String name) {
		return reportDao.findByName(name);
	}

    @Override
    @Transactional
    public void removeReport(Integer id) throws ReportNotFoundException {
        logger.info("removeReport={}", id);
        Report r = findById(id);
        r.setId(id);
        reportDao.removeReport(r);
        logger.info("Report with id={} deleted successfully.", id);
    }

    private Report findById(Integer id) throws ReportNotFoundException {
        logger.info("Looking for Report with id={}", id);
        Report r = reportDao.findById(id);
        if(r==null){
            throw new ReportNotFoundException();
        }
        return r;
    }

    @Override
	@Transactional(rollbackFor = Exception.class)
	public Report saveReport(Report report, MultipartFile reportFile) throws IOException, InvalidNameException,
			DuplicateReportException, ReportFileRequiredException, UserNotFoundException, InvalidUsernameException,
            ValidationUtils.InvalidEmailException, InvalidPasswordException, OnePermissionRequiredException {
		logger.info("Saving a new report: {}", report);
		ReportUtils.validateReport(report);

		if (reportDao.findByName(report.getName()) != null) {
			throw new DuplicateReportException();
		}
		
		
		report = reportDao.saveReport(report);
		logger.debug("Report saved");

		if (reportFile.isEmpty()) {
			throw new ReportFileRequiredException();
		}

		logger.debug("Moving report file");
		String reportRepositoryPath = ApplicationProperties.getProperty("application.report.repository.path");
		File outputFile = new File(reportRepositoryPath + "/" + report.getId() + ".rptdesign");
		reportFile.transferTo(outputFile);
		logger.debug("Report moved to {}", outputFile.getAbsolutePath());
		

		BackofficeUser user = UserUtils.getCurrentUser();
		

		userService.updateUser(user);
	
		reportDao.updateReport(report);
		
		return report;
	}

	/**
	 * @param reportDao
	 *            the reportDao to set
	 */
	public void setReportDao(ReportDao reportDao) {
		this.reportDao = reportDao;
	}

	/**
	 * @param userService the userService to set
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
