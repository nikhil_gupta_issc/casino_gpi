package com.gpi.backoffice.service.user;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.exceptions.AuthenticationException;
import com.gpi.backoffice.exceptions.InvalidPasswordConfirmationException;
import com.gpi.backoffice.exceptions.UserNotFoundException;

public interface PasswordService {

    /**
     * Changes the password of one of the BackOffice users
     *
     * @param newPassword
     * @param user
     * @throws com.gpi.backoffice.exceptions.UserNotFoundException when the userName provides doesn't belong
     * to a valid BackOffice user
     * @throws com.gpi.backoffice.exceptions.InvalidPasswordConfirmationException when new password doesn't
     * match with its confirmation
     * @throws com.gpi.backoffice.exceptions.AuthenticationException when currentPassword doesn't match the
     * current user's password
     */
    public void changePassword(String currentPassword, String newPassword, String newPasswordConfirmation,
            BackofficeUser user) throws UserNotFoundException,
            InvalidPasswordConfirmationException, AuthenticationException;

    /**
     * Changes the password of one of the BackOffice users
     *
     * @param currentPassword
     * @param userId
     * @throws UserNotFoundException when the userName provides doesn't belong
     * to a valid BackOffice user
     * @throws InvalidPasswordConfirmationException when new password doesn't
     * match with its confirmation
     * @throws AuthenticationException when currentPassword doesn't match the
     * current user's password
     */
    public void changePassword(String currentPassword, Integer userId) throws UserNotFoundException,
            InvalidPasswordConfirmationException, AuthenticationException;

}
