package com.gpi.backoffice.service.user.impl;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.exceptions.AuthenticationException;
import com.gpi.backoffice.exceptions.InvalidPasswordConfirmationException;
import com.gpi.backoffice.exceptions.UserNotFoundException;
import com.gpi.backoffice.service.user.PasswordService;
import com.gpi.backoffice.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordServiceImpl implements PasswordService {

    private static final Logger log = LoggerFactory.getLogger(PasswordServiceImpl.class);
    private PasswordEncoder passwordEncoder;
    private UserService userService;

    @Override
    public void changePassword(String currentPassword, String newPassword, String newPasswordConfirmation,
            BackofficeUser user) throws UserNotFoundException,
            InvalidPasswordConfirmationException, AuthenticationException {

        log.debug("Changing password.");
        if (!newPassword.equals(newPasswordConfirmation)) {
            throw new InvalidPasswordConfirmationException();
        }

        if (user == null) {
            throw new UserNotFoundException();
        }

        if (!passwordEncoder.matches(currentPassword, user.getPassword())) {
            throw new AuthenticationException();
        }

        user.setPassword(passwordEncoder.encode(newPassword));
        try {
            userService.updateUser(user);
        } catch (Exception e) {
            throw new RuntimeException("Failed to update user", e);
        }
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void changePassword(String newPassword, Integer userId) throws UserNotFoundException, InvalidPasswordConfirmationException, AuthenticationException {
        BackofficeUser user = userService.findUserById(userId);
        user.setPassword(passwordEncoder.encode(newPassword));
        try {
            userService.updateUser(user);
        } catch (Exception e) {
            throw new RuntimeException("Failed to update user", e);
        }
    }
}
