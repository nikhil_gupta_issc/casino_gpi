package com.gpi.backoffice.service.api;

import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Created by nacho on 29/04/15.
 */
public interface HttpClientService {

    Object sendPostRequest(String s, Map<String, String> params) throws IOException, URISyntaxException;

    Object sendGetRequest(String s, Map<String, String> params) throws IOException, URISyntaxException;

    Object sendGetRequest(String url, Map<String, String> params, TypeReference typeReference) throws IOException, URISyntaxException;
}
