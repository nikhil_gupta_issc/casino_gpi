package com.gpi.backoffice.service.report.impl;

import com.gpi.backoffice.domain.filters.FreeSpinFilters;
import com.gpi.backoffice.domain.filters.PlayerFilters;
import com.gpi.backoffice.domain.filters.ReportFilters;
import com.gpi.backoffice.dto.PlayerDto;
import com.gpi.backoffice.dto.ReportDto;
import com.gpi.backoffice.service.report.ReportsService;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.FilterOperator;
import com.gpi.backoffice.query.filter.FirstLevelFieldFilter;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.backoffice.query.filter.SecondLevelFieldFilter;
import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.player.Player;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ignacio on 12/11/13.
 */
public class FreeSpinReportServiceImpl implements ReportsService<FreeSpin> {

    private static final Logger log = LoggerFactory.getLogger(FreeSpinReportServiceImpl.class);

    private QueryObject<FreeSpin> queryObject;

    @Override
    public ReportDto<FreeSpin> getReportDto(ReportFilters reportFilters, Integer startIndex, Integer pageSize, String sortExpression) {
        log.info("Looking for free spins...");
        FreeSpinFilters playerFilters = (FreeSpinFilters) reportFilters;
        List<HibernateFilter> filters = generateFilters(playerFilters);

        List<FreeSpin> freeSprins = queryObject.get(startIndex, pageSize, sortExpression, filters);
        Long count = queryObject.getCount(filters);
       
        return new ReportDto<>(freeSprins, count);
    }

    private List<HibernateFilter> generateFilters(FreeSpinFilters playerFilters) {
        List<HibernateFilter> filters = new ArrayList<>();
        filters = processPlayerFilter(filters, playerFilters.getPlayerName());
        filters = processGameFilter(filters, playerFilters.getGameId());
        return filters;
    }

    private List<HibernateFilter> processGameFilter(List<HibernateFilter> filters, Long game) {
        if (game != null) {
            log.info("Looking for game=[{}]", game);
            SecondLevelFieldFilter filter = new SecondLevelFieldFilter("game", "id", FilterOperator.EQ, game);
            filters.add(filter);
        }
        return filters;
    }

    private List<HibernateFilter> processPlayerFilter(List<HibernateFilter> filters, String playerName) {
        if (StringUtils.isNotBlank(playerName)) {
            log.info("Applying filter player=[{}]", playerName);
            FirstLevelFieldFilter fieldFilter = new FirstLevelFieldFilter("name", FilterOperator.EQ, playerName);
            filters.add(fieldFilter);
        }
        return filters;
    }

    public void setQueryObject(QueryObject<FreeSpin> queryObject) {
        this.queryObject = queryObject;
    }
}
