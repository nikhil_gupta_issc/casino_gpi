package com.gpi.backoffice.service.user.publisher;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.exceptions.AuthenticationException;
import com.gpi.backoffice.exceptions.InvalidPasswordConfirmationException;
import com.gpi.backoffice.exceptions.InvalidPermissionIdException;
import com.gpi.backoffice.exceptions.UserNotFoundException;

public interface PublisherUserService {


    void resetUserPassword(String newPassword, Integer userId, BackofficeUser currentUser) throws InvalidPermissionIdException, UserNotFoundException, InvalidPasswordConfirmationException, AuthenticationException;
}
