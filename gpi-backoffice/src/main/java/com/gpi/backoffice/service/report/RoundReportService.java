package com.gpi.backoffice.service.report;

import com.gpi.backoffice.domain.filters.RoundFilters;
import com.gpi.backoffice.dto.RoundReportDto;
import com.gpi.exceptions.NonExistentPublisherException;

import java.text.ParseException;

/**
 * User: igabbarini
 */
public interface RoundReportService {

    RoundReportDto getRounds(RoundFilters roundFilters, Integer startIndex, Integer pageSize, String sortExpression) throws ParseException, NonExistentPublisherException;

}
