package com.gpi.backoffice.service.api;

import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.PlayerLockedDto;
import com.gpi.dto.RoundTransactionDto;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface BingoApi {

    List<PlayerLockedDto> getLockedPlayers(String publisher) throws URISyntaxException, IOException;

    void unlockPlayers(List<Long> playersId, String publisherName) throws URISyntaxException, IOException;

    List<RoundTransactionDto> getRoundDetails(Long roundId, Publisher publisher) throws IOException, URISyntaxException;
}
