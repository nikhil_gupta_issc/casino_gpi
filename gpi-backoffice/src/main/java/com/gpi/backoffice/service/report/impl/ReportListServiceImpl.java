package com.gpi.backoffice.service.report.impl;


import com.gpi.backoffice.domain.report.Report;
import com.gpi.backoffice.domain.user.BackofficePermission;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.service.report.ReportListService;
import com.gpi.backoffice.utils.UserUtils;
import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.HibernateFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ReportListServiceImpl implements ReportListService {

	private QueryObject<Report> queryObject;

	@Override
	public List<Report> list(int startIndex, int pageSize, String sortExpression) {
        List<Report> reports = queryObject.get(startIndex, pageSize, sortExpression, generateFilters(null));
        List<Report> filteredReports = new ArrayList<>();
        BackofficeUser currentUser = UserUtils.getCurrentUser();
        for (Report report : reports){
            if (report.hasAtLeastOnePermission(currentUser.getPermissions())){
                filteredReports.add(report);
            }
        }
        return filteredReports;
    }

	@Override
	public Long getCount() {
		return queryObject.getCount(generateFilters(null));
	}

	public void setQueryObject(QueryObject<Report> queryObject) {
		this.queryObject = queryObject;
	}

	private List<HibernateFilter> generateFilters(Collection<BackofficePermission> userPerm) {
		List<HibernateFilter> filters = new ArrayList<HibernateFilter>();

		return filters;
	}
}
