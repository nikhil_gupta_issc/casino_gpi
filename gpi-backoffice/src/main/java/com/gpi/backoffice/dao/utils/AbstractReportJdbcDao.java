package com.gpi.backoffice.dao.utils;

import com.gpi.backoffice.query.filter.SqlFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * Created by Ignacio on 12/11/13.
 */
public abstract class AbstractReportJdbcDao<T> extends PersistenceContextJdbcDao{
    private static final Logger log = LoggerFactory.getLogger(AbstractReportJdbcDao.class);

    public List<T> getObjects(Integer startIndex, Integer pageSize, String sortExpression, List<SqlFilter>
            filters) {
        String filters1 = getFilters(filters);
        String format = String.format(getSelectQuery(), filters1, sortExpression);
        log.info("Query: {}",format);
        return getJdbcTemplate().query(format, new Object[]{startIndex,pageSize}, getRowMapper());
    }

    protected abstract RowMapper getRowMapper();

    protected String getFilters(List<SqlFilter> filters) {
        if(filters==null || filters.isEmpty()){
            return " ";
        }
        StringBuffer sb = new StringBuffer();
        for (SqlFilter filter : filters){
            sb.append(filter.getFilter());
        }
        return sb.toString();
    }

    public Long getCountFiltered(List<SqlFilter> filters) {
        String filters1 = getFilters(filters);
        String format = String.format(getSelectCount(), filters1);
        return getJdbcTemplate().queryForLong(format);
    }

    public abstract String getSelectCount();
    public abstract String getSelectQuery();
}
