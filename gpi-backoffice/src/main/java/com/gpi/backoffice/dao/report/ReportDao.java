package com.gpi.backoffice.dao.report;


import com.gpi.backoffice.domain.report.Report;

public interface ReportDao {
	/**
	 * Saves the report to the DB
	 * */
	Report saveReport(Report report);
	
	/**
	 * Updates the report on the DB
	 * */
	public void updateReport(Report report);
	
	/**
	 * Gets a Report by name
	 * */
	Report findByName(String name);

    void removeReport(Report report);

    Report findById(Integer id);
}
