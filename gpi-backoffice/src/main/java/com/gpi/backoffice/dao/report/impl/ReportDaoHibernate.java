package com.gpi.backoffice.dao.report.impl;

import com.gpi.backoffice.dao.report.ReportDao;
import com.gpi.backoffice.domain.report.Report;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportDaoHibernate extends ExtendedHibernateDaoSupport implements ReportDao {

    private static final Logger logger = LoggerFactory.getLogger(ReportDaoHibernate.class);

    @Override
    public Report saveReport(Report report) {
        getHibernateSession().save(report);
        return report;
    }

    @Override
    public void updateReport(Report report) {
        getHibernateSession().update(report);
    }

    @Override
    public Report findByName(String name) {
        return (Report) getHibernateSession().createQuery("from Report where name = :name")
                .setString("name", name)
                .uniqueResult();
    }

    @Override
    public Report findById(Integer id) {
        logger.info("Searching for report with id={} into database.", id);
        return (Report) getHibernateSession().createQuery("from Report where id = :id").setInteger("id", id).uniqueResult();
    }

    @Override
    public void removeReport(Report report) {
        logger.info("Attempting to remove report with id={} from database.", report.getId());
        getHibernateSession().delete(report);
        logger.info("Report removed from databse successfully.");
    }
}
