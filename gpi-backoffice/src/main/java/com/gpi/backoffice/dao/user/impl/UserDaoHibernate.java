package com.gpi.backoffice.dao.user.impl;

import com.gpi.backoffice.dao.user.UserDao;
import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class UserDaoHibernate extends ExtendedHibernateDaoSupport implements UserDao {

    private static final Logger log = LoggerFactory.getLogger(UserDaoHibernate.class);
	@Override
	public BackofficeUser saveUser(BackofficeUser user) {
        log.debug("Saving BackofficeUser into database...");
		Integer userId = (Integer) getHibernateSession().save(user);
        log.debug("BackofficeUser saved with id = {}", userId);
		user.setId(userId);
		return user;
	}

	@Override
	public void updateUser(BackofficeUser user) {
        log.debug("Updating BackofficeUser...");
		getHibernateSession().update(user);
	}
	
	@Override
	public BackofficeUser findById(Integer id) {
        log.debug("Finding BackofficeUser with id = {}", id);
		return (BackofficeUser)getHibernateSession().createQuery("from BackofficeUser where id = :id")
				.setInteger("id", id)
				.uniqueResult();
	}

	@Override
	public BackofficeUser findByUserName(String userName) {
        log.debug("Finding BackofficeUser by name = {} ", userName);
		return (BackofficeUser)getHibernateSession().createQuery("from BackofficeUser where username = :userName")
				.setString("userName", userName)
				.uniqueResult();
	}
	
	@Override
	public void deleteUser(BackofficeUser user) {
        log.debug("Removing BackofficeUser with id = [{}] and name [{}] from database",
                user.getId(),user.getUsername());
		getHibernateSession().delete(user);
	}
}
