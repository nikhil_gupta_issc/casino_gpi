package com.gpi.backoffice.dao.user;

import com.gpi.backoffice.domain.user.BackofficeUser;

/**
 * backoffice user data access object.
 * */
public interface UserDao {
	
	/**
	 * Saves the user to the DB
	 * @return the user with its proper id
	 * */
	BackofficeUser saveUser(BackofficeUser user);
	
	/**
	 * updates the user on the DB
	 * */
	public void updateUser(BackofficeUser user);
	
	/**
	 * Gets a user by id
	 * */
	BackofficeUser findById(Integer id);
	
	/**
	 * Gets a user by UserName
	 * */
	BackofficeUser findByUserName(String userName);
	
	/**
	 * Deletes a user
	 */
	void deleteUser(BackofficeUser user);
}
