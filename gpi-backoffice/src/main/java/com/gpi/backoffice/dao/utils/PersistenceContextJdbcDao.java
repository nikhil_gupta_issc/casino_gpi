package com.gpi.backoffice.dao.utils;

import org.springframework.jdbc.core.JdbcTemplate;

public abstract class PersistenceContextJdbcDao {

    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
