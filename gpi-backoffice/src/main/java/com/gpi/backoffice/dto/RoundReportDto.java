package com.gpi.backoffice.dto;

import java.util.List;

/**
 * User: igabbarini
 */
public class RoundReportDto {

    private List<RoundDto> rounds;
    private Long count;

    public RoundReportDto(List<RoundDto> rounds, Long count) {
        this.rounds = rounds;
        this.count = count;
    }

    public List<RoundDto> getRounds() {
        return rounds;
    }

    public Long getCount() {
        return count;
    }


}
