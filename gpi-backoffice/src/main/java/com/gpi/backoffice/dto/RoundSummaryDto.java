package com.gpi.backoffice.dto;

import net.sf.json.JSONArray;

/**
 * User: igabbarini
 */
public class RoundSummaryDto {

    private final JSONArray records;

    private final int winAmount;

    private final int betAmount;

    public RoundSummaryDto(JSONArray records, int winAmount, int betAmount) {
        this.records = records;
        this.winAmount = winAmount;
        this.betAmount = betAmount;
    }

    public JSONArray getRecords() {
        return records;
    }

    public int getWinAmount() {
        return winAmount;
    }

    public int getBetAmount() {
        return betAmount;
    }
}
