package com.gpi.backoffice.dto;

public class ProviderRequest {

    private String name;
    private String newName;
    private String loginName;
    private String password;
    private String prefix;
    private String api;
    private boolean hasExtendedInfo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public boolean isHasExtendedInfo() {
        return hasExtendedInfo;
    }

    public void setHasExtendedInfo(boolean hasExtendedInfo) {
        this.hasExtendedInfo = hasExtendedInfo;
    }
}
