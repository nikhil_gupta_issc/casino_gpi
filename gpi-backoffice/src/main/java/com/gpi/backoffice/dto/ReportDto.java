package com.gpi.backoffice.dto;

import java.util.List;

/**
 * Created by Ignacio on 12/11/13.
 */
public class ReportDto<T> {


    private List<T> objects;
    private Long count;

    public ReportDto(List<T> objects, Long count) {
        this.objects = objects;
        this.count = count;

    }

    public List<T> getObjects() {
        return objects;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

}
