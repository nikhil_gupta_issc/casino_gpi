package com.gpi.backoffice.dto;

import org.joda.time.DateTime;

/**
 * Created by Ignacio on 12/11/13.
 */
public class PlayerDto {

    private long id;
    private String publisher;
    private String name;

    private DateTime createdOn;

    public DateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

}
