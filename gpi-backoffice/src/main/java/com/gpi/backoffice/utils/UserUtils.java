package com.gpi.backoffice.utils;

import com.gpi.backoffice.domain.user.BackofficeUser;
import com.gpi.backoffice.exceptions.InvalidPasswordException;
import com.gpi.backoffice.exceptions.InvalidUsernameException;
import com.gpi.backoffice.exceptions.OnePermissionRequiredException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserUtils {
    private static final Logger log = LoggerFactory.getLogger(UserUtils.class);

    public static BackofficeUser getCurrentUser() {
        return (BackofficeUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static void validateUser(BackofficeUser user) throws InvalidUsernameException, InvalidPasswordException, OnePermissionRequiredException {

        String userName = user.getUsername();
        if (StringUtils.isEmpty(userName) || userName.length() > 100 || userName.length() < 3) {
            throw new InvalidUsernameException();
        }

        String password = user.getPassword();
        if (StringUtils.isEmpty(password) || password.length() > 100 || password.length() < 3) {
            throw new InvalidPasswordException();
        }

        if (user.getAuthorities().size() == 0) {
            throw new OnePermissionRequiredException();
        }
    }
}
