package com.gpi.backoffice.utils;


import com.gpi.backoffice.domain.report.Report;
import org.apache.commons.lang.StringUtils;

import javax.naming.InvalidNameException;

public class ReportUtils {

	public static void validateReport(Report report) throws InvalidNameException {

		String name = report.getName();
		if (StringUtils.isEmpty(name) || name.length() > 100) {
			throw new InvalidNameException();
		}
	}
}
