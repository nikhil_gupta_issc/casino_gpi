use gpi;

ALTER TABLE publisher_game add UNIQUE INDEX `unique_publisher_game` (`publisher`, `game_id`);

update publisher_game set free=1, charged=1;

INSERT INTO publisher_game (charged, free, game_id, publisher) 
SELECT 1, 1, g.id, p.id FROM publisher p, game g where NOT EXISTS (select id from publisher_game where publisher = p.id and game_id = g.id);