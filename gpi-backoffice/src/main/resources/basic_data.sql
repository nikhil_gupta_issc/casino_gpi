START TRANSACTION ;
INSERT INTO publisher (id,name,api_name,api_username,password,created_on,url) VALUES (2,'testPublisher','test','testPublisher','test',now(),'http://localhost:8081/testing/betMotion.do');
INSERT INTO publisher (id,name,api_name,api_username,password,created_on,url) VALUES (4,'free','free','free','free',now(),'nourl');

INSERT INTO game_provider (id, login_name, name, password, api_name, extended_info) VALUES (1, 'username', 'Njoy Gaming', 'password', 'gpi', true);

INSERT INTO backoffice_users (id,created_on,acc_non_expired,acc_non_locked,cred_non_expired,last_login,user_name,password,status,enabled) VALUES (2,now(),true,true,true,now(),'asdasd','04981c7dc65a012c02a3b8170da8a5c1dc697c700fe03a461bac59005fca851bf893f5f2ca58991a',1,true);
INSERT INTO backoffice_users (id, created_on, acc_non_expired, acc_non_locked, cred_non_expired, last_login, user_name, password, status, enabled, provider_id)
VALUES (3, now(), TRUE, TRUE, TRUE, now(), 'provider',
        '04981c7dc65a012c02a3b8170da8a5c1dc697c700fe03a461bac59005fca851bf893f5f2ca58991a', 1, TRUE, 1);
INSERT INTO backoffice_users (id, created_on, acc_non_expired, acc_non_locked, cred_non_expired, last_login, user_name, password, status, enabled, publisher_id)
VALUES (1, now(), TRUE, TRUE, TRUE, now(), 'publisher',
        '04981c7dc65a012c02a3b8170da8a5c1dc697c700fe03a461bac59005fca851bf893f5f2ca58991a', 1, TRUE, 2);

INSERT INTO backoffice_permissions (permission_id,user_id) VALUES (2,2);
INSERT INTO backoffice_permissions (permission_id, user_id) VALUES (8, 3);
INSERT INTO backoffice_permissions (permission_id, user_id) VALUES (7, 3);
INSERT INTO backoffice_permissions (permission_id, user_id) VALUES (1, 3);
INSERT INTO backoffice_permissions (permission_id, user_id) VALUES (1, 1);
INSERT INTO backoffice_permissions (permission_id, user_id) VALUES (7, 1);

INSERT INTO game (id, name, url, provider_id, freespins)
VALUES (1, 'pachinko-world', 'https://pw.test.njoybingo.com/game.do', 1, 1);
INSERT INTO game (id, name, url, provider_id, freespins)
VALUES (2, 'Sample-game', 'https://pw.test.njoybingo.com/game.do', 1, 0);

INSERT INTO publisher_game (free, game_id, publisher, charged) VALUES (true, 1, 2, true);
INSERT INTO publisher_game (free, game_id, publisher, charged) VALUES (true, 1, 4, true);

INSERT INTO gpi.player (id, created_on, currency, name, publisher_id) VALUES (1, now(), 'USD', 'Nacho', 2);

INSERT INTO gpi.round (id, bet_amount, created_on, currency, error, game_round_id, win_amount, game_id, player_id, publisher_id) VALUES (1, 100, now(), 'USD', 0, 1, 200, 1, 1, 2);
INSERT INTO gpi.round (id, bet_amount, created_on, currency, error, game_round_id, win_amount, game_id, player_id, publisher_id) VALUES (2, 100, now(), 'USD', 0, 1, 200, 1, 1, 2);

INSERT INTO gpi.transaction (id, amount, created_on, ext_transaction_id, game_transaction_id, success, type, round_id) VALUES (1, 100, now(), 1000, 1, 1, 'BET', 1);
INSERT INTO gpi.transaction (id, amount, created_on, ext_transaction_id, game_transaction_id, success, type, round_id) VALUES (2, 100, now(), 1001, 2, 1, 'BET', 1);
INSERT INTO gpi.transaction (id, amount, created_on, ext_transaction_id, game_transaction_id, success, type, round_id) VALUES (3, 100, now(), 1002, 3, 1, 'BET', 1);
INSERT INTO gpi.transaction (id, amount, created_on, ext_transaction_id, game_transaction_id, success, type, round_id) VALUES (4, 450, now(), 1004, 4, 1, 'WIN', 1);
COMMIT ;