package com.bet.games.api;

public interface BetGamesErrorCodes {
	public static final int INVALIDSIGNATURE_ERROR = 1;
	public static final int EXPIREDMESSAGE_ERROR = 2;
	public static final int INVALIDTOKEN_ERROR = 3;

	public static final int NOPAYIN_ERROR = 700;
	public static final int INTERNAL_ERROR = 500;
}