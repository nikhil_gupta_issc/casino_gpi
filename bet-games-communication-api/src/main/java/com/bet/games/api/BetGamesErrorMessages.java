package com.bet.games.api;

public interface BetGamesErrorMessages {
	public static final String INVALIDSIGNATURE_MESSAGE = "wrong signature";
	public static final String EXPIREDMESSAGE_MESSAGE = "request is expired";
	public static final String INVALIDTOKEN_MESSAGE = "invalid token";
	public static final String NOPAYIN_MESSAGE = "there is no PAYIN with provided bet_id";
	public static final String INTERNAL_MESSAGE = "internal error";
}