package com.bet.games.api.validation;

import java.math.BigInteger;

import com.bet.games.api.BetGamesMethodType;
import com.bet.games.api.Root;
import com.gpi.utils.ApplicationProperties;

/**
 * Builder for signatures to be used in Bet Games integration
 *
 * @author Alexandre
 *
 */
public class BetGamesSignatureBuilder {

	private Root message;
	private BetGamesMethodType methodType;
	
	/**
	 * @param message message to construct signature from
	 * @param methodType type of the method call. One of {@link BetGamesMethodType}
	 */
	public BetGamesSignatureBuilder(Root message, BetGamesMethodType methodType) {
		this.message = message;
		this.methodType = methodType;
	}
	
	/**
	 * @param message new message to set
	 * @return SignatureBuilder
	 */
	public BetGamesSignatureBuilder setMessage(Root message) {
		this.message = message;
		
		return this;
	}

	/**
	 * @param methodType new method type to set
	 * @return SignatureBuilder
	 */
	public BetGamesSignatureBuilder setMethodType(BetGamesMethodType methodType) {
		this.methodType = methodType;
		
		return this;
	}

	/**
	 * Builds a signature according to the message and methodType defined
	 * @return constructed signature or null if the message or methodType are not defined
	 */
	public String build() {
		if (message == null || methodType == null)
			return null;
		
		StringBuilder sb = new StringBuilder();
		sb.append("method").append(message.getMethod());
		sb.append("token").append(message.getToken());
		
		if (BetGamesMethodType.REQUEST.equals(methodType)) {
			sb.append("time").append(message.getTime());

			if ("transaction_bet_payin".equals(message.getMethod())) {
				sb.append("amount").append(message.getParams().getAmount());
				sb.append("currency").append(message.getParams().getCurrency());
				sb.append("bet_id").append(message.getParams().getBetId());
				sb.append("transaction_id").append(message.getParams().getTransactionId());
				sb.append("retrying").append(message.getParams().getRetrying());
			}
			else if ("transaction_bet_payout".equals(message.getMethod())) {
				sb.append("player_id").append(message.getParams().getPlayerId());
				sb.append("amount").append(message.getParams().getAmount());
				sb.append("currency").append(message.getParams().getCurrency());
				sb.append("bet_id").append(message.getParams().getBetId());
				sb.append("transaction_id").append(message.getParams().getTransactionId());
				sb.append("retrying").append(message.getParams().getRetrying());
			}
			// all other methods dont need additional params
		}
		else if (BetGamesMethodType.RESPONSE.equals(methodType)) {
			sb.append("success").append(message.getSuccess());
			sb.append("error_code").append(message.getErrorCode());
			sb.append("error_text").append(message.getErrorText());
		
			sb.append("time").append(message.getTime());
			
			if ("get_account_details".equals(message.getMethod())) {
				if (message.getParams() != null) {
					sb.append("user_id").append(message.getParams().getUserId());
					sb.append("username").append(message.getParams().getUsername());
					sb.append("currency").append(message.getParams().getCurrency());
					sb.append("info").append(message.getParams().getInfo());
				}
			}
			else if ("request_new_token".equals(message.getMethod())) {
				if (message.getParams() != null) {
					sb.append("new_token").append(message.getParams().getNewToken());
				}
			}
			else if ("get_balance".equals(message.getMethod())) {
				if (message.getParams() != null) {
					sb.append("balance").append(message.getParams().getBalance());
				}
			}
			else if ("transaction_bet_payin".equals(message.getMethod()) || "transaction_bet_payout".equals(message.getMethod())) {
				if (message.getParams() != null) {
					if (BigInteger.valueOf(1).equals(message.getSuccess())) {
						sb.append("balance_after").append(message.getParams().getBalanceAfter());
						sb.append("already_processed").append(message.getParams().getAlreadyProcessed());
					}
				}
			}
			
			// all other methods dont need additional params
		}
		
		sb.append(ApplicationProperties.getProperty("betgames.api.key"));
		
		return sb.toString();
	}
}
