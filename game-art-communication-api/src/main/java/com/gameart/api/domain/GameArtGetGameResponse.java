package com.gameart.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class GameArtGetGameResponse extends GameArtResponse {
	private GameArtGameConfigResponse response;

	@JsonProperty(value="response")
	public GameArtGameConfigResponse getResponse() {
		return response;
	}

	public void setResponse(GameArtGameConfigResponse response) {
		this.response = response;
	}
}
