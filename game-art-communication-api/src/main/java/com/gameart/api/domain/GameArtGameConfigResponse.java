package com.gameart.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class GameArtGameConfigResponse {
	@JsonProperty("game_url")
	private String gameUrl;
	
	public String getGameUrl() {
		return gameUrl;
	}
	public void setGameUrl(String gameUrl) {
		this.gameUrl = gameUrl;
	}
}
