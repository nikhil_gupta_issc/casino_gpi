package com.gameart.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class GameArtRemoteData {
	@JsonProperty(value="current_token")
	private String currentToken;
	
	@JsonProperty(value="currency")
	private String currency;

	public String getCurrentToken() {
		return currentToken;
	}

	public void setCurrentToken(String currentToken) {
		this.currentToken = currentToken;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
