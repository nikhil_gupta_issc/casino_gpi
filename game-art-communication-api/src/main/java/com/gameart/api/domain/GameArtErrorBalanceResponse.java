package com.gameart.api.domain;

public class GameArtErrorBalanceResponse extends GameArtBalanceResponse{
	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
