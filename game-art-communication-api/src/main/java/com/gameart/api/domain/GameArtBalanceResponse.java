package com.gameart.api.domain;

public class GameArtBalanceResponse extends GameArtResponse {
	private double balance;
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
}
