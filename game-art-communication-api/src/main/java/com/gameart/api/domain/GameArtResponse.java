package com.gameart.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public abstract class GameArtResponse {
	private int status;
	
	private GameArtRemoteData remoteData;
	
	@JsonProperty(value="remote_data")
	public GameArtRemoteData getRemoteData() {
		return remoteData;
	}

	public void setRemoteData(GameArtRemoteData remoteData) {
		this.remoteData = remoteData;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
