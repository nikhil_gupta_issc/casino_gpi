package com.gameart.api.domain;

public class GameArtErrorResponse extends GameArtResponse{
	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
