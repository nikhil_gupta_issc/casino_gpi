package com.gameart.api;

public interface GameArtResponseMessages {
	public static final String INSUFFICIENT_FUNDS_MSG = "Insufficient funds";
	public static final String INVALID_REMOTE_DATA = "Invalid remote_data field";
	public static final String INVALID_KEY = "Invalid key";
	public static final String GAME_REQUEST_NOT_EXIST = "Game request does not exist";
}
