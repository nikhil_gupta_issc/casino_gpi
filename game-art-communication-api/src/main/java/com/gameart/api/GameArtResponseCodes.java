package com.gameart.api;

public interface GameArtResponseCodes {
	public static final int INSUFFICIENT_BALANCE_ERROR_CODE = 203;
	public static final int OK_STATUS_CODE = 200;
	public static final int INSUFFICIENT_FUNDS_STATUS_CODE = 403;
	public static final int INTERNAL_ERROR_STATUS_CODE = 500;
}
