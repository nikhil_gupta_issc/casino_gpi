package com.gameart.api.validation;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.gameart.api.domain.GameArtResponse;
import com.gpi.utils.ApplicationProperties;

/**
 * Validator for the requests received in GameArt
 * 
 * @author Alexandre
 *
 */
public class GameArtRequestValidator {
	
	/**
	 * Create SHA1 hash of a message
	 * @param message message to create SHA1 hash
	 * @return SHA1 hash of a message
	 */
	private static String sha1Hash(String message) {
		MessageDigest mDigest;
		try {
			mDigest = MessageDigest.getInstance("SHA1");
			
			byte[] result = mDigest.digest(message.getBytes());
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < result.length; i++) {
	            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
	        }
	         
	        return sb.toString();
		} catch (NoSuchAlgorithmException e) {
		}
        return null;
	}
	
	/**
	 * @param queryString received query string
	 * @param key received key in request
	 * @param salyKey salt key from game art
	 * @return if the received query string is valid according to the provided key in the query string key param
	 */
	public boolean validate(String queryString, String key, String saltKey) {
		// remove key=... from query string
		queryString = queryString.replaceAll("key=[a-z0-9]*&?", "").replaceAll("&$", "");
		
		return sha1Hash(saltKey+queryString).equals(key);
	}
}
