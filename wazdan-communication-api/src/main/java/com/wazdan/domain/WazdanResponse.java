package com.wazdan.domain;

import java.io.IOException;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class WazdanResponse {
	
	@JsonProperty("status")
	private Integer status;
	
	@JsonProperty("msg_type")
	private String msg_type;

	@JsonProperty("msg_time")
	private String msg_time;

	@JsonProperty("msg_text")
	private String msg_text;
	
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	
	/**
	 * @return the msg_type
	 */
	public String getMsg_type() {
		return msg_type;
	}

	/**
	 * @param msg_type the msg_type to set
	 */
	public void setMsg_type(String msg_type) {
		this.msg_type = msg_type;
	}

	/**
	 * @return the msg_time
	 */
	public String getMsg_time() {
		return msg_time;
	}

	/**
	 * @param msg_time the msg_time to set
	 */
	public void setMsg_time(String msg_time) {
		this.msg_time = msg_time;
	}

	/**
	 * @return the msg_text
	 */
	public String getMsg_text() {
		return msg_text;
	}

	/**
	 * @param msg_text the msg_text to set
	 */
	public void setMsg_text(String msg_text) {
		this.msg_text = msg_text;
	}

	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
}
