package com.wazdan.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class WazdanAuthenticateRequest extends WazdanRequest {

	@JsonProperty("token")
	private String token;
	
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	
	
	
}
