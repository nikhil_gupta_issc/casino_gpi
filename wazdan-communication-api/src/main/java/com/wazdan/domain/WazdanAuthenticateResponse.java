package com.wazdan.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class WazdanAuthenticateResponse extends WazdanResponse {

	@JsonProperty("remoteUserId")
	private String remoteUserId;

	@JsonProperty("currency")
	private String currency;
	
	@JsonProperty("birthDate")
	private String birthDate;
	
	@JsonProperty("gender")
	private String gender;
	
	@JsonProperty("country")
	private String country;
	
	@JsonProperty("rc_interval")
	private Integer rc_interval;
	
	@JsonProperty("rc_transactionurl")
	private String rc_transactionurl;
	
	@JsonProperty("rc_startinterval")
	private Integer rc_startinterval;
	
	@JsonProperty("rc_closegameurl")
	private String rc_closegameurl;
	
	

	public WazdanAuthenticateResponse() {
		super();
	}

	/**
	 * @return the remoteUserId
	 */
	public String getRemoteUserId() {
		return remoteUserId;
	}

	/**
	 * @param remoteUserId the remoteUserId to set
	 */
	public void setRemoteUserId(String remoteUserId) {
		this.remoteUserId = remoteUserId;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the birthDate
	 */
	public String getBirthDate() {
		return birthDate;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the rc_interval
	 */
	public Integer getRc_interval() {
		return rc_interval;
	}

	/**
	 * @param rc_interval the rc_interval to set
	 */
	public void setRc_interval(Integer rc_interval) {
		this.rc_interval = rc_interval;
	}

	/**
	 * @return the rc_transactionurl
	 */
	public String getRc_transactionurl() {
		return rc_transactionurl;
	}

	/**
	 * @param rc_transactionurl the rc_transactionurl to set
	 */
	public void setRc_transactionurl(String rc_transactionurl) {
		this.rc_transactionurl = rc_transactionurl;
	}

	/**
	 * @return the rc_startinterval
	 */
	public Integer getRc_startinterval() {
		return rc_startinterval;
	}

	/**
	 * @param rc_startinterval the rc_startinterval to set
	 */
	public void setRc_startinterval(Integer rc_startinterval) {
		this.rc_startinterval = rc_startinterval;
	}

	/**
	 * @return the rc_closegameurl
	 */
	public String getRc_closegameurl() {
		return rc_closegameurl;
	}

	/**
	 * @param rc_closegameurl the rc_closegameurl to set
	 */
	public void setRc_closegameurl(String rc_closegameurl) {
		this.rc_closegameurl = rc_closegameurl;
	}
	
}
