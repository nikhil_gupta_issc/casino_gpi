package com.wazdan.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class WazdanFrResultRequest extends WazdanRequest {

	@JsonProperty("token")
	private String token;
	
	@JsonProperty("remoteUserId")
	private String remoteUserId;

	@JsonProperty("stake")
	private Double stake;
	
	@JsonProperty("win")
	private Double win;
	
	@JsonProperty("gameId")
	private Integer gameId;
	
	@JsonProperty("gameCount")
	private Integer gameCount;

	@JsonProperty("freeroundsId")
	private Integer freeroundsId;

	@JsonProperty("transactionId")
	private Long transactionId;
	
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the remoteUserId
	 */
	public String getRemoteUserId() {
		return remoteUserId;
	}

	/**
	 * @param remoteUserId the remoteUserId to set
	 */
	public void setRemoteUserId(String remoteUserId) {
		this.remoteUserId = remoteUserId;
	}

	/**
	 * @return the gameId
	 */
	public Integer getGameId() {
		return gameId;
	}

	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the stake
	 */
	public Double getStake() {
		return stake;
	}

	/**
	 * @param stake the stake to set
	 */
	public void setStake(Double stake) {
		this.stake = stake;
	}

	/**
	 * @return the win
	 */
	public Double getWin() {
		return win;
	}

	/**
	 * @param win the win to set
	 */
	public void setWin(Double win) {
		this.win = win;
	}

	/**
	 * @return the gameCount
	 */
	public Integer getGameCount() {
		return gameCount;
	}

	/**
	 * @param gameCount the gameCount to set
	 */
	public void setGameCount(Integer gameCount) {
		this.gameCount = gameCount;
	}

	/**
	 * @return the freeroundsId
	 */
	public Integer getFreeroundsId() {
		return freeroundsId;
	}

	/**
	 * @param freeroundsId the freeroundsId to set
	 */
	public void setFreeroundsId(Integer freeroundsId) {
		this.freeroundsId = freeroundsId;
	}

	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}	

}
