package com.wazdan.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class WazdanErrorResponse extends WazdanResponse{
	
	public WazdanErrorResponse(Integer statusCode, String message) {
		super();
		this.setStatus(statusCode);
		this.message = message;
	}

	public WazdanErrorResponse() {
		super();
	}
	
	@JsonProperty("message")
	private String message;

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
}
