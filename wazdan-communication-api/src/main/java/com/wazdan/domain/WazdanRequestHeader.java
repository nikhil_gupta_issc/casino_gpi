package com.wazdan.domain;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

public class WazdanRequestHeader {

	private String authorization;
	
	public WazdanRequestHeader(String authorization) {
		super();
		this.authorization = authorization;
	}

	public WazdanRequestHeader() {
		super();
	}
	
	/**
	 * @return the authorization
	 */
	public String getAuthorization() {
		return authorization;
	}

	/**
	 * @param authorization the authorization to set
	 */
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
}

