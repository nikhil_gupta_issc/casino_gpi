package com.wazdan.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class WazdanFrResultResponse extends WazdanResponse {
	
	@JsonProperty("currentFunds")
	private Double currentFunds;

	@JsonProperty("bonusFunds")
	private Double bonusFunds;
	
	@JsonProperty("bonusWin")
	private Double bonusWin;

	/**
	 * @return the currentFunds
	 */
	public Double getCurrentFunds() {
		return currentFunds;
	}

	/**
	 * @param currentFunds the currentFunds to set
	 */
	public void setCurrentFunds(Double currentFunds) {
		this.currentFunds = currentFunds;
	}

	/**
	 * @return the bonusFunds
	 */
	public Double getBonusFunds() {
		return bonusFunds;
	}

	/**
	 * @param bonusFunds the bonusFunds to set
	 */
	public void setBonusFunds(Double bonusFunds) {
		this.bonusFunds = bonusFunds;
	}

	/**
	 * @return the bonusWin
	 */
	public Double getBonusWin() {
		return bonusWin;
	}

	/**
	 * @param bonusWin the bonusWin to set
	 */
	public void setBonusWin(Double bonusWin) {
		this.bonusWin = bonusWin;
	}
	
}
