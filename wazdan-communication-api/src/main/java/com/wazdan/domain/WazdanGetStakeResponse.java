package com.wazdan.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class WazdanGetStakeResponse extends WazdanResponse {

	@JsonProperty("currentFunds")
	private Double currentFunds;
	
	@JsonProperty("bonusFunds")
	private Double bonusFunds;
	
	@JsonProperty("bonusStake")
	private Double bonusStake;

	/**
	 * @return the currentFunds
	 */
	public Double getCurrentFunds() {
		return currentFunds;
	}

	/**
	 * @param currentFunds the currentFunds to set
	 */
	public void setCurrentFunds(Double currentFunds) {
		this.currentFunds = currentFunds;
	}

	/**
	 * @return the bonusFunds
	 */
	public Double getBonusFunds() {
		return bonusFunds;
	}

	/**
	 * @param bonusFunds the bonusFunds to set
	 */
	public void setBonusFunds(Double bonusFunds) {
		this.bonusFunds = bonusFunds;
	}

	/**
	 * @return the bonusStake
	 */
	public Double getBonusStake() {
		return bonusStake;
	}

	/**
	 * @param bonusStake the bonusStake to set
	 */
	public void setBonusStake(Double bonusStake) {
		this.bonusStake = bonusStake;
	}
	
}
