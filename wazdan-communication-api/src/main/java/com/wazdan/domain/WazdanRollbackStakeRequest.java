package com.wazdan.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class WazdanRollbackStakeRequest extends WazdanRequest {

	@JsonProperty("token")
	private String token;
	
	@JsonProperty("remoteUserId")
	private String remoteUserId;
	
	@JsonProperty("stake")
	private Double stake;
	
	@JsonProperty("bonusAmount")
	private Double bonusAmount;
	
	@JsonProperty("gameId")
	private Integer gameId;
	
	@JsonProperty("gameNo")
	private Integer gameNo;
	
	@JsonProperty("sessionId")
	private Integer sessionId;
	
	@JsonProperty("step")
	private Integer step;
	
	@JsonProperty("originalTransactionId")
	private Long originalTransactionId;
	
	@JsonProperty("transactionId")
	private Long transactionId;
	
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the remoteUserId
	 */
	public String getRemoteUserId() {
		return remoteUserId;
	}

	/**
	 * @param remoteUserId the remoteUserId to set
	 */
	public void setRemoteUserId(String remoteUserId) {
		this.remoteUserId = remoteUserId;
	}

	/**
	 * @return the stake
	 */
	public Double getStake() {
		return stake;
	}

	/**
	 * @param stake the stake to set
	 */
	public void setStake(Double stake) {
		this.stake = stake;
	}

	/**
	 * @return the gameId
	 */
	public Integer getGameId() {
		return gameId;
	}

	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the gameNo
	 */
	public Integer getGameNo() {
		return gameNo;
	}

	/**
	 * @param gameNo the gameNo to set
	 */
	public void setGameNo(Integer gameNo) {
		this.gameNo = gameNo;
	}

	/**
	 * @return the sessionId
	 */
	public Integer getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the step
	 */
	public Integer getStep() {
		return step;
	}

	/**
	 * @param step the step to set
	 */
	public void setStep(Integer step) {
		this.step = step;
	}
	
	/**
	 * @return the bonusAmount
	 */
	public Double getBonusAmount() {
		return bonusAmount;
	}

	/**
	 * @param bonusAmount the bonusAmount to set
	 */
	public void setBonusAmount(Double bonusAmount) {
		this.bonusAmount = bonusAmount;
	}

	/**
	 * @return the originalTransactionId
	 */
	public Long getOriginalTransactionId() {
		return originalTransactionId;
	}

	/**
	 * @param originalTransactionId the originalTransactionId to set
	 */
	public void setOriginalTransactionId(Long originalTransactionId) {
		this.originalTransactionId = originalTransactionId;
	}

	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	
}
