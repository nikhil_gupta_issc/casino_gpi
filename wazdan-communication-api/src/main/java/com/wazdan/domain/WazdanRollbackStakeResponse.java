package com.wazdan.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class WazdanRollbackStakeResponse extends WazdanResponse {

	@JsonProperty("currentFunds")
	private Double currentFunds;
	
	@JsonProperty("bonusFunds")
	private Double bonusFunds;
	
	/**
	 * @return the currentFunds
	 */
	public Double getCurrentFunds() {
		return currentFunds;
	}

	/**
	 * @param currentFunds the currentFunds to set
	 */
	public void setCurrentFunds(Double currentFunds) {
		this.currentFunds = currentFunds;
	}

	/**
	 * @return the bonusFunds
	 */
	public Double getBonusFunds() {
		return bonusFunds;
	}

	/**
	 * @param bonusFunds the bonusFunds to set
	 */
	public void setBonusFunds(Double bonusFunds) {
		this.bonusFunds = bonusFunds;
	}
	
	
}
