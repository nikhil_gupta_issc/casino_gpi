package com.wazdan.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class WazdanGameCloseRequest extends WazdanRequest {

	@JsonProperty("token")
	private String token;
	
	@JsonProperty("remoteUserId")
	private String remoteUserId;
	
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the remoteUserId
	 */
	public String getRemoteUserId() {
		return remoteUserId;
	}

	/**
	 * @param remoteUserId the remoteUserId to set
	 */
	public void setRemoteUserId(String remoteUserId) {
		this.remoteUserId = remoteUserId;
	}

}
