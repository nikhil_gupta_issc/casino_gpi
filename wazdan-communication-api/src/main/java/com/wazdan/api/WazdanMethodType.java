package com.wazdan.api;

public enum WazdanMethodType {
	AUTHENTICATE("authenticate"), GETSTAKE("getstake"), ROLLBACKSTAKE("rollbackstake"), RETURNWIN("returnwin"), GAMECLOSE("gameclose"), 
	FREERESULT("frresult"), GETFUNDS("getfunds"), RCCLOSE("rcclose"), RCCONTINUE("rccontinue "), PING("ping");
	
	private WazdanMethodType(String methodName) {
		this.methodName = methodName;
	}
	
	private String methodName;
	
	public String getMethodName() {
		return methodName;
	}
}
