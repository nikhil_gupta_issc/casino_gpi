package com.wazdan.api;

public interface WazdanErrorCodes {

	public static final Integer STATUS_CODE_INTERNAL_ERROR = 9999;
	
	public static final String STATUS_MSG_INTERNAL_ERROR = "internal error";
}