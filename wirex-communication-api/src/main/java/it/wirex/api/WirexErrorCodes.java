package it.wirex.api;

/**
 * Error codes for the Wirex API
 * 
 * @author Alexandre
 *
 */
public interface WirexErrorCodes {
	public static final int INVALID_TOKEN = 1;
	public static final int INVALID_SESSION = 2;
	public static final int AMOUNT_NOT_AVAILABLE = 4;
	public static final int INTERNAL_SERVER_ERROR = 7;
}