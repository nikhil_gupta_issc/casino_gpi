package it.wirex.api;

/**
 * Methods for the Wirex API
 * 
 * @author Alexandre
 *
 */
public enum WirexMethod {

    AUTHENTICATE("authenticate"), SESSION("session"), WALLET("wallet"), WITHDRAW("withdraw"), DEPOSIT("deposit"),
    CANCEL("cancel"), ROLLBACK("rollback");

    private final String name;

    private WirexMethod(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
