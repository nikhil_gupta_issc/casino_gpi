package it.wirex.api.domain;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Base response to Wirex API
 * 
 * @author Alexandre
 *
 */

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class WirexResponse {
	protected int statusCode;
	
	protected WirexError error;

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public WirexError getError() {
		return error;
	}

	public void setError(WirexError error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "WirexResponse [statusCode=" + statusCode + ", error=" + error + "]";
	}
}
