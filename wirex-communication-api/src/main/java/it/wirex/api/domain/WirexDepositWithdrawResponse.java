package it.wirex.api.domain;

/**
 * Deposit and withdraw response for Wirex API
 * 
 * @author Alexandre
 *
 */
public class WirexDepositWithdrawResponse extends WirexResponse {
	private WirexTransfer result;

	public WirexTransfer getResult() {
		return result;
	}

	public void setResult(WirexTransfer result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "WirexDepositWithdrawResponse [result=" + result + "]";
	}
	
}
