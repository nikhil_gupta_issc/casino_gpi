package it.wirex.api.domain;

/**
 * Transfer response for Wirex API
 * 
 * @author Alexandre
 *
 */
public class WirexTransfer {
	private double amount;
	private String currency;
	private WirexWallet wallet;
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public WirexWallet getWallet() {
		return wallet;
	}
	public void setWallet(WirexWallet wallet) {
		this.wallet = wallet;
	}
	
	@Override
	public String toString() {
		return "WirexTransfer [amount=" + amount + ", currency=" + currency + ", wallet=" + wallet + "]";
	}
	
	
}
