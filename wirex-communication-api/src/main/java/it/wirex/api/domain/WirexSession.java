package it.wirex.api.domain;

/**
 * Session data container for Wirex API
 * 
 * @author Alexandre
 *
 */
public class WirexSession {
	private String sessionId;
	private int userId;
	private String username;
	private String currency;
	private String country;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Override
	public String toString() {
		return "WirexSession [sessionId=" + sessionId + ", userId=" + userId + ", username=" + username + ", currency="
				+ currency + "]";
	}
}
