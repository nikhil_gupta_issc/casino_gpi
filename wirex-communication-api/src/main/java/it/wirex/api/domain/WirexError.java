package it.wirex.api.domain;

/**
 * Error data container to Wirex API
 * 
 * @author Alexandre
 *
 */
public class WirexError {
	protected int typeCode;

	public int getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(int typeCode) {
		this.typeCode = typeCode;
	}

	@Override
	public String toString() {
		return "WirexError [typeCode=" + typeCode + "]";
	}
}
