package it.wirex.api.domain;

/**
 * Refund response for Wirex API
 * 
 * @author Alexandre
 *
 */
public class WirexCancelResponse extends WirexResponse {
	private WirexTransaction result;

	public WirexTransaction getResult() {
		return result;
	}

	public void setResult(WirexTransaction result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "WirexCancelResponse [result=" + result + "]";
	}
	
}
