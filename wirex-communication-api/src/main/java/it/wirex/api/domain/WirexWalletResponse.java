package it.wirex.api.domain;

/**
 * Wallet response for Wirex API
 * 
 * @author Alexandre
 *
 */
public class WirexWalletResponse extends WirexResponse {
	private WirexWallet result;

	public WirexWallet getResult() {
		return result;
	}

	public void setResult(WirexWallet result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "WirexWalletResponse [result=" + result + "]";
	}
	
	
}
