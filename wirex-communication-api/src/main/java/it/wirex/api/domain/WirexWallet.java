package it.wirex.api.domain;

/**
 * Wallet data container for Wirex API
 * 
 * @author Alexandre
 *
 */
public class WirexWallet {
	private double balance;
	private String currency;
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Override
	public String toString() {
		return "WirexWallet [balance=" + balance + ", currency=" + currency + "]";
	}
	
	
}
