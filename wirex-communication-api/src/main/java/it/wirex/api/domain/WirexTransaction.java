package it.wirex.api.domain;

/**
 * Transaction data container for Wirex API
 * 
 * @author Alexandre
 *
 */
public class WirexTransaction {
	private WirexWallet wallet;

	public WirexWallet getWallet() {
		return wallet;
	}

	public void setWallet(WirexWallet wallet) {
		this.wallet = wallet;
	}

	@Override
	public String toString() {
		return "WirexTransaction [wallet=" + wallet + "]";
	}
	
	
}
