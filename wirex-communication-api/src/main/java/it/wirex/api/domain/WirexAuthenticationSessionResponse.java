package it.wirex.api.domain;

/**
 * Authentication and Session response for Wirex API
 * 
 * @author Alexandre
 *
 */
public class WirexAuthenticationSessionResponse extends WirexResponse {
	private WirexSession result;

	public WirexSession getResult() {
		return result;
	}

	public void setResult(WirexSession result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "WirexAuthenticationSessionResponse [result=" + result + "]";
	}
	
	
}
