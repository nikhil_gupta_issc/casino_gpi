package com.spinmatic.api.domain;

/**
 * CheckPlayer request for spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticCheckPlayerRequest {
	private final String action = "checkPlayer";
	private Integer siteId;
	private String privateKey;
	private String publicKey;
	private String accessKey;
	private String username;
	private String password;
	private Integer ownerId;
	private Integer externalId;
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public String getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public Integer getExternalId() {
		return externalId;
	}
	public void setExternalId(Integer externalId) {
		this.externalId = externalId;
	}
	public String getAction() {
		return action;
	}
	@Override
	public String toString() {
		return "SpinmaticCheckPlayerRequest [action=" + action + ", siteId=" + siteId + ", privateKey=" + privateKey
				+ ", publicKey=" + publicKey + ", accessKey=" + accessKey + ", username=" + username + ", password="
				+ password + ", ownerId=" + ownerId + ", externalId=" + externalId + "]";
	}
	
	
}
