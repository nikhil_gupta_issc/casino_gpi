package com.spinmatic.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Success response for spinmatic
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SpinmaticOkResponse extends SpinmaticResponse {
	private SpinmaticResponseData response;
	
    @JsonProperty("Response")
	public SpinmaticResponseData getResponse() {
		return response;
	}

	public void setResponse(SpinmaticResponseData response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "SpinmaticOkResponse [response=" + response + "]";
	}
}
