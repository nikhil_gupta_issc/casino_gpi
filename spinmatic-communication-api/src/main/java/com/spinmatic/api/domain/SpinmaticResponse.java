package com.spinmatic.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Response for spinmatic
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public abstract class SpinmaticResponse {
    @JsonProperty("Status")
	protected String status;
    
    @JsonProperty("Message")
	protected String message;

    @JsonProperty("Status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

    @JsonProperty("Message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
