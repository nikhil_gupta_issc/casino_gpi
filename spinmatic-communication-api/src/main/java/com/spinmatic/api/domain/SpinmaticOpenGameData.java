package com.spinmatic.api.domain;

/**
 * OpenGame response data for spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticOpenGameData {
	private String lobbyURL;
	private String sessionID;
	public String getLobbyURL() {
		return lobbyURL;
	}
	public void setLobbyURL(String lobbyURL) {
		this.lobbyURL = lobbyURL;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	
	
}
