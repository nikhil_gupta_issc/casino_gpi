package com.spinmatic.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * OpenGame response data for spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticOpenGameResponseData {
    @JsonProperty("lobbyUrl")
	private String lobbyUrl;
    
    @JsonProperty("mobileLobbyUrl")
	private String mobileLobbyUrl;
    
    @JsonProperty("con_id")
	private String con_id;
    
	public String getLobbyUrl() {
		return lobbyUrl;
	}
	public void setLobbyUrl(String lobbyUrl) {
		this.lobbyUrl = lobbyUrl;
	}
	public String getMobileLobbyUrl() {
		return mobileLobbyUrl;
	}
	public void setMobileLobbyUrl(String mobileLobbyUrl) {
		this.mobileLobbyUrl = mobileLobbyUrl;
	}
	public String getCon_id() {
		return con_id;
	}
	public void setCon_id(String con_id) {
		this.con_id = con_id;
	}
	@Override
	public String toString() {
		return "SpinmaticOpenGameResponseData [lobbyUrl=" + lobbyUrl + ", mobileLobbyUrl=" + mobileLobbyUrl
				+ ", con_id=" + con_id + "]";
	}
}
