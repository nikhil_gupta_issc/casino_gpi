package com.spinmatic.api.domain;

/**
 * OpenGame request for spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticOpenGameRequest {
	private final String action = "openGame";
	private Integer siteId;
	private String privateKey;
	private String publicKey;
	private String accessKey;
	private String username;
	private Integer ownerId;
	private Long externalId;
	private Integer gameId;
	private String currency;
	
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public String getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public Long getExternalId() {
		return externalId;
	}
	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}
	public Integer getGameId() {
		return gameId;
	}
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	public String getAction() {
		return action;
	}
	
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Override
	public String toString() {
		return "SpinmaticOpenGameRequest [action=" + action + ", siteId=" + siteId + ", privateKey=" + privateKey
				+ ", publicKey=" + publicKey + ", accessKey=" + accessKey + ", username=" + username + ", ownerId="
				+ ownerId + ", externalId=" + externalId + ", gameId=" + gameId + ", currency=" + currency + "]";
	}
}
