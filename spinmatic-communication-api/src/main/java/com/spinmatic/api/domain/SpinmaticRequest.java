package com.spinmatic.api.domain;

/**
 * Request for spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticRequest {
	private String action;
	private Integer siteId;
	private Integer ownerId;
	private String ownerExternalId;
	private Integer playerId;
	private Integer playerExternalId;
	private String playerUsername;
	private String sessionId;
	private Integer sessionType;
	private Integer gameId;
	private String uniqueId;
	private String transactionId;
	private String timestamp;
	private String publicKey;
	private String accessKey;
	private String privateKey;
	private Long roundId;
	private Double debitAmount;
	private String debitCurrency;
	private Double creditAmount;
	private String creditCurrency;
	private Double rollbackAmount;
	private String rollbackCurrency;
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public String getOwnerExternalId() {
		return ownerExternalId;
	}
	public void setOwnerExternalId(String ownerExternalId) {
		this.ownerExternalId = ownerExternalId;
	}
	public Integer getPlayerId() {
		return playerId;
	}
	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}
	public Integer getPlayerExternalId() {
		return playerExternalId;
	}
	public void setPlayerExternalId(Integer playerExternalId) {
		this.playerExternalId = playerExternalId;
	}
	public String getPlayerUsername() {
		return playerUsername;
	}
	public void setPlayerUsername(String playerUsername) {
		this.playerUsername = playerUsername;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Integer getSessionType() {
		return sessionType;
	}
	public void setSessionType(Integer sessionType) {
		this.sessionType = sessionType;
	}
	public Integer getGameId() {
		return gameId;
	}
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	public Long getRoundId() {
		return roundId;
	}
	public void setRoundId(Long roundId) {
		this.roundId = roundId;
	}
	public Double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	public String getDebitCurrency() {
		return debitCurrency;
	}
	public void setDebitCurrency(String debitCurrency) {
		this.debitCurrency = debitCurrency;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public String getCreditCurrency() {
		return creditCurrency;
	}
	public void setCreditCurrency(String creditCurrency) {
		this.creditCurrency = creditCurrency;
	}
	public Double getRollbackAmount() {
		return rollbackAmount;
	}
	public void setRollbackAmount(Double rollbackAmount) {
		this.rollbackAmount = rollbackAmount;
	}
	public String getRollbackCurrency() {
		return rollbackCurrency;
	}
	public void setRollbackCurrency(String rollbackCurrency) {
		this.rollbackCurrency = rollbackCurrency;
	}
	@Override
	public String toString() {
		return "SpinmaticRequest [action=" + action + ", siteId=" + siteId + ", ownerId=" + ownerId
				+ ", ownerExternalId=" + ownerExternalId + ", playerId=" + playerId + ", playerExternalId="
				+ playerExternalId + ", playerUsername=" + playerUsername + ", sessionId=" + sessionId
				+ ", sessionType=" + sessionType + ", gameId=" + gameId + ", uniqueId=" + uniqueId + ", transactionId="
				+ transactionId + ", timestamp=" + timestamp + ", publicKey=" + publicKey + ", accessKey=" + accessKey
				+ ", privateKey=" + privateKey + ", roundId=" + roundId + ", debitAmount=" + debitAmount
				+ ", debitCurrency=" + debitCurrency + ", creditAmount=" + creditAmount + ", creditCurrency="
				+ creditCurrency + ", rollbackAmount=" + rollbackAmount + ", rollbackCurrency=" + rollbackCurrency
				+ "]";
	}
}
