package com.spinmatic.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * CheckPlayer response for spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticCheckPlayerResponse extends SpinmaticResponse {
	// type is object because we dont care of the response. its just to not fail in deserializing
    @JsonProperty("Response")
	private Object response;

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}
	
	
}
