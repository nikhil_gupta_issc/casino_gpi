package com.spinmatic.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * OpenGame response for spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticOpenGameResponse extends SpinmaticResponse {
    @JsonProperty("Response")
	private SpinmaticOpenGameResponseData response;

	public SpinmaticOpenGameResponseData getResponse() {
		return response;
	}

	public void setResponse(SpinmaticOpenGameResponseData response) {
		this.response = response;
	}
	
	
}
