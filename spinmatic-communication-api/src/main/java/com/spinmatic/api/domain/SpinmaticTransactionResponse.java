package com.spinmatic.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Success transaction response for spinmatic
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SpinmaticTransactionResponse extends SpinmaticResponse {
	private SpinmaticTransactionResponseData response;
	
    @JsonProperty("Response")
	public SpinmaticTransactionResponseData getResponse() {
		return response;
	}

	public void setResponse(SpinmaticTransactionResponseData response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "SpinmaticTransactionResponse [response=" + response + "]";
	}
}
