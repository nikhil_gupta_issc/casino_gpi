package com.spinmatic.api.domain;

/**
 * Response data for spinmatic transaction
 * 
 * @author Alexandre
 *
 */
public class SpinmaticTransactionResponseData extends SpinmaticResponseData {
	protected Double oldBalance;
	protected Double oldBonusAmount;
	protected Long roundId;
	
	/**
	 * @param responseData responseData object to be used to copy fields to a new transaction response
	 */
	public SpinmaticTransactionResponseData(SpinmaticResponseData responseData) {
		this.action = responseData.getAction();
		this.siteId = responseData.getSiteId();
		this.ownerExternalId = responseData.getOwnerExternalId();
		this.playerExternalId = responseData.getPlayerExternalId();
		this.balance = responseData.getBalance();
		this.bonusAmount = responseData.getBonusAmount();
		this.currency = responseData.getCurrency();
		this.gameId = responseData.getGameId();
		this.ownerId = responseData.getOwnerId();
		this.playerId = responseData.getPlayerId();
		this.playerUsername = responseData.getPlayerUsername();
		this.gameId = responseData.getGameId();
		this.sessionId = responseData.getSessionId();
		this.sessionType = responseData.getSessionType();
		this.siteId = responseData.getSiteId();
		this.timestamp = responseData.getTimestamp();
		this.uniqueId = responseData.getUniqueId();
		this.publicKey = responseData.getPublicKey();
		this.accessKey = responseData.getPublicKey();
		this.privateKey = responseData.getPrivateKey();
	}
	
	public Double getOldBalance() {
		return oldBalance;
	}
	
	public void setOldBalance(Double oldBalance) {
		this.oldBalance = oldBalance;
	}
	
	public Double getOldBonusAmount() {
		return oldBonusAmount;
	}
	
	public void setOldBonusAmount(Double oldBonusAmount) {
		this.oldBonusAmount = oldBonusAmount;
	}
	
	public Long getRoundId() {
		return roundId;
	}
	
	public void setRoundId(Long roundId) {
		this.roundId = roundId;
	}

	@Override
	public String toString() {
		return "SpinmaticTransactionResponseData [oldBalance=" + oldBalance + ", oldBonusAmount=" + oldBonusAmount
				+ ", roundId=" + roundId + ", action=" + action + ", playerId=" + playerId + ", playerUsername="
				+ playerUsername + ", sessionId=" + sessionId + ", gameId=" + gameId + ", transactionId="
				+ transactionId + ", balance=" + balance + ", currency=" + currency + "]";
	}
}
