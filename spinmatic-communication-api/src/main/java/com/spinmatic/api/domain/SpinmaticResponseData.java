package com.spinmatic.api.domain;

/**
 * Response data for spinmatic response
 * @author Alexandre
 *
 */
public class SpinmaticResponseData {
	protected String action;
	protected Integer siteId;
	protected Integer ownerId;
	protected String ownerExternalId;
	protected Integer playerId;
	protected Integer playerExternalId;
	protected String playerUsername;
	protected String sessionId;
	protected Integer sessionType;
	protected Integer gameId;
	protected String uniqueId;
	protected String transactionId;
	protected String timestamp;
	protected String publicKey;
	protected String accessKey;
	protected String privateKey;
	protected Double balance;
	protected Double bonusAmount;
	protected String currency;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public String getOwnerExternalId() {
		return ownerExternalId;
	}
	public void setOwnerExternalId(String ownerExternalId) {
		this.ownerExternalId = ownerExternalId;
	}
	public Integer getPlayerId() {
		return playerId;
	}
	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}
	public Integer getPlayerExternalId() {
		return playerExternalId;
	}
	public void setPlayerExternalId(Integer playerExternalId) {
		this.playerExternalId = playerExternalId;
	}
	public String getPlayerUsername() {
		return playerUsername;
	}
	public void setPlayerUsername(String playerUsername) {
		this.playerUsername = playerUsername;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Integer getSessionType() {
		return sessionType;
	}
	public void setSessionType(Integer sessionType) {
		this.sessionType = sessionType;
	}
	public Integer getGameId() {
		return gameId;
	}
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Double getBonusAmount() {
		return bonusAmount;
	}
	public void setBonusAmount(Double bonusAmount) {
		this.bonusAmount = bonusAmount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Override
	public String toString() {
		return "SpinmaticResponseData [action=" + action + ", siteId=" + siteId + ", ownerId=" + ownerId
				+ ", ownerExternalId=" + ownerExternalId + ", playerId=" + playerId + ", playerExternalId="
				+ playerExternalId + ", playerUsername=" + playerUsername + ", sessionId=" + sessionId
				+ ", sessionType=" + sessionType + ", gameId=" + gameId + ", uniqueId=" + uniqueId + ", transactionId="
				+ transactionId + ", timestamp=" + timestamp + ", publicKey=" + publicKey + ", accessKey=" + accessKey
				+ ", privateKey=" + privateKey + ", balance=" + balance + ", bonusAmount=" + bonusAmount + ", currency="
				+ currency + "]";
	}
	
	
}