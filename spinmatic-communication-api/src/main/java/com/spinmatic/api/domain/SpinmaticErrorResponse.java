package com.spinmatic.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Error response for spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticErrorResponse extends SpinmaticResponse{
	private boolean response;

    @JsonProperty("Response")
	public boolean getResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "SpinmaticErrorResponse [response=" + response + "]";
	}
}
