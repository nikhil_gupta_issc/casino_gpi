package com.spinmatic.api;

/**
 * Status messages for spinmatic
 * 
 * @author Alexandre
 *
 */
public interface SpinmaticStatus {
	public static final String SUCCESS_MESSAGE = "Success";
	public static final String ERROR_MESSAGE = "Error";

}
