package com.habanero.api;

public enum HabaneroMethodType {
	TRANSACTION("transaction"), AUTH("auth"), QUERY("query");
	
	private HabaneroMethodType(String methodName) {
		this.methodName = methodName;
	}
	
	private String methodName;
	
	public String getMethodName() {
		return methodName;
	}
}
