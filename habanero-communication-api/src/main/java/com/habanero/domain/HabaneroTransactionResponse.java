package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroTransactionResponse extends HabaneroResponse {

	@JsonProperty("fundtransferresponse")
	private HabaneroFundTransferResponse fundtransferresponse;
	
	@JsonProperty("dialogmessageresponse")
	private HabaneroDialogMessageResponse dialogmessageresponse;
	
	public HabaneroTransactionResponse() {
		super();
	}
	
	public HabaneroTransactionResponse(HabaneroFundTransferResponse fundtransferresponse) {
		super();
		this.fundtransferresponse = fundtransferresponse;
	}
	
	public HabaneroTransactionResponse(HabaneroFundTransferResponse fundtransferresponse,
			HabaneroDialogMessageResponse dialogmessageresponse) {
		super();
		this.fundtransferresponse = fundtransferresponse;
		this.dialogmessageresponse = dialogmessageresponse;
	}

	public static HabaneroTransactionResponse createSuccessCompleted() {
		HabaneroTransactionResponse response = new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus()));
		response.getFundtransferresponse().getStatus().setSuccess(true);
		return response;
	}
	
	public static HabaneroTransactionResponse createSuccessExpired() {
		HabaneroTransactionResponse response = new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus()));
		response.getFundtransferresponse().getStatus().setSuccess(true);
		return response;
	}
	
	public HabaneroFundTransferResponse getFundtransferresponse() {
		return fundtransferresponse;
	}

	public void setFundtransferresponse(HabaneroFundTransferResponse fundtransferresponse) {
		this.fundtransferresponse = fundtransferresponse;
	}
	
}
