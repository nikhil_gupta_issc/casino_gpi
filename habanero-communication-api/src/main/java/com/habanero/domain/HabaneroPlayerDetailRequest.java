package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class HabaneroPlayerDetailRequest {
	
	@JsonProperty("token")
	private String token;
	
	@JsonProperty("gamelaunch")
	private Boolean gamelaunch;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getGamelaunch() {
		return gamelaunch;
	}

	public void setGamelaunch(Boolean gamelaunch) {
		this.gamelaunch = gamelaunch;
	}


}