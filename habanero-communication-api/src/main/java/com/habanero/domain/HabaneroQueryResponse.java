package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroQueryResponse extends HabaneroResponse {

	@JsonProperty("fundtransferresponse")
	private HabaneroFundTransferResponse fundtransferresponse;
	
	public HabaneroQueryResponse() {
		super();
	}

	public HabaneroQueryResponse(HabaneroFundTransferResponse fundtransferresponse) {
		super();
		this.fundtransferresponse = fundtransferresponse;
	}

	public HabaneroFundTransferResponse getFundtransferresponse() {
		return fundtransferresponse;
	}

	public void setFundtransferresponse(HabaneroFundTransferResponse fundtransferresponse) {
		this.fundtransferresponse = fundtransferresponse;
	}
	
}
