package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroDialogMessageResponse {

	public static Integer TYPE_OK_AND_CONTINUE = 0;
	public static Integer TYPE_OK_AND_RELOAD = 1;
	public static Integer TYPE_OK_AND_LOBBY = 2;
	public static Integer TYPE_KO = 3;
	
	@JsonProperty("type")
	private Integer type;

	@JsonProperty("message")
	private String message;
	
	public HabaneroDialogMessageResponse() {
		super();
	}
	
	public HabaneroDialogMessageResponse(Integer type, String message) {
		super();
		this.type = type;
		this.message = message;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
}