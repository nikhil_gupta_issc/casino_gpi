package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroStatus {
	
	public HabaneroStatus() {
		super();
	}

	public HabaneroStatus(Boolean success, Boolean autherror) {
		super();
		this.success = success;
		this.autherror = autherror;
	}

	public HabaneroStatus(Boolean success, Boolean autherror, String message) {
		super();
		this.success = success;
		this.autherror = autherror;
		this.message = message;
	}
	
	public HabaneroStatus(Boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	}
	
	public static HabaneroStatus createSuccess() {
		return new HabaneroStatus(true, false);
	}
	
	public static HabaneroStatus createSuccessTransfer(boolean successdebit, boolean sucecsscredit) {
		HabaneroStatus status = new HabaneroStatus();
		status.setSuccess(true);
		status.setSuccessdebit(successdebit);
		status.setSuccesscredit(sucecsscredit);
		return status;
	}
	
	@JsonProperty("success")
	private Boolean success;
	
	@JsonProperty("autherror")
	private Boolean autherror;
	
	@JsonProperty("nofunds")
	private Boolean nofunds;;
	
	@JsonProperty("successdebit")
	private Boolean successdebit;
	
	@JsonProperty("successcredit")
	private Boolean successcredit;
	
	@JsonProperty("refundstatus")
	private Integer refundStatus;
	
	@JsonProperty("Message")
	private String message;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Boolean getAutherror() {
		return autherror;
	}

	public void setAutherror(Boolean autherror) {
		this.autherror = autherror;
	}

	public Boolean getNofunds() {
		return nofunds;
	}

	public void setNofunds(Boolean nofunds) {
		this.nofunds = nofunds;
	}

	public Boolean getSuccessdebit() {
		return successdebit;
	}

	public void setSuccessdebit(Boolean successdebit) {
		this.successdebit = successdebit;
	}

	public Boolean getSuccesscredit() {
		return successcredit;
	}

	public void setSuccesscredit(Boolean successcredit) {
		this.successcredit = successcredit;
	}

	public Integer getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(Integer refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
