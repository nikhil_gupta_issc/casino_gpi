package com.habanero.domain;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroFunds {

	@JsonProperty("debitandcredit")
	private Boolean debitandcredit;
	
	@JsonProperty("fundinfo")
	private List<HabaneroFundInfo> fundinfo;
	
	@JsonProperty("refund")
	private HabaneroRefund refund;

	public Boolean getDebitandcredit() {
		return debitandcredit;
	}

	public void setDebitandcredit(Boolean debitandcredit) {
		this.debitandcredit = debitandcredit;
	}

	public List<HabaneroFundInfo> getFundinfo() {
		return fundinfo;
	}

	public void setFundinfo(List<HabaneroFundInfo> fundinfo) {
		this.fundinfo = fundinfo;
	}

	public HabaneroRefund getRefund() {
		return refund;
	}

	public void setRefund(HabaneroRefund refund) {
		this.refund = refund;
	}
	
}
