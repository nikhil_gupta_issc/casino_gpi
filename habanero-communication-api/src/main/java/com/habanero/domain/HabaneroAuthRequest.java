package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroAuthRequest extends HabaneroRequest {
	
	@JsonProperty("basegame")
	private HabaneroBasegame basegame;
	
	@JsonProperty("playerdetailrequest")
	private HabaneroPlayerDetailRequest playerdetailrequest;


	public HabaneroBasegame getBasegame() {
		return basegame;
	}

	public void setBasegame(HabaneroBasegame basegame) {
		this.basegame = basegame;
	}

	public HabaneroPlayerDetailRequest getPlayerdetailrequest() {
		return playerdetailrequest;
	}

	public void setPlayerdetailrequest(HabaneroPlayerDetailRequest playerdetailrequest) {
		this.playerdetailrequest = playerdetailrequest;
	}
	
}
