package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroAuthResponse extends HabaneroResponse {

	@JsonProperty("playerdetailresponse")
	private HabaneroPlayerDetailResponse playerdetailresponse;
	
	public HabaneroAuthResponse(HabaneroPlayerDetailResponse playerdetailresponse) {
		super();
		this.playerdetailresponse = playerdetailresponse;
	}

	public HabaneroAuthResponse() {
		super();
	}

	public HabaneroPlayerDetailResponse getPlayerdetailresponse() {
		return playerdetailresponse;
	}

	public void setPlayerdetailresponse(HabaneroPlayerDetailResponse playerdetailresponse) {
		this.playerdetailresponse = playerdetailresponse;
	}
	
}
