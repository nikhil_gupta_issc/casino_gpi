package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroFundTransferRequest {

	@JsonProperty("token")
	private String token;
	
	@JsonProperty("accountid")
	private String accountid;
	
	@JsonProperty("gameinstanceid")
	private String gameinstanceid;
	
	@JsonProperty("friendlygameinstanceid")
	private String friendlygameinstanceid;
	
	@JsonProperty("customplayertype")
	private Integer customplayertype;
	
	@JsonProperty("isretry")
	private Boolean isretry;
	
	@JsonProperty("isrefund")
	private Boolean isrefund;
	
	@JsonProperty("isrecredit")
	private Boolean isrecredit;
	
	@JsonProperty("funds")
	private HabaneroFunds funds;
	
	@JsonProperty("retrycount")
	private Integer retrycount;
	
	@JsonProperty("gamedetails")
	private HabaneroGameDetails gamedetails;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAccountid() {
		return accountid;
	}

	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}

	public String getGameinstanceid() {
		return gameinstanceid;
	}

	public void setGameinstanceid(String gameinstanceid) {
		this.gameinstanceid = gameinstanceid;
	}

	public String getFriendlygameinstanceid() {
		return friendlygameinstanceid;
	}

	public void setFriendlygameinstanceid(String friendlygameinstanceid) {
		this.friendlygameinstanceid = friendlygameinstanceid;
	}

	public Integer getCustomplayertype() {
		return customplayertype;
	}

	public void setCustomplayertype(Integer customplayertype) {
		this.customplayertype = customplayertype;
	}

	public Boolean getIsretry() {
		return isretry;
	}

	public void setIsretry(Boolean isretry) {
		this.isretry = isretry;
	}

	public Boolean getIsrefund() {
		return isrefund;
	}

	public void setIsrefund(Boolean isrefund) {
		this.isrefund = isrefund;
	}

	public Boolean getIsrecredit() {
		return isrecredit;
	}

	public void setIsrecredit(Boolean isrecredit) {
		this.isrecredit = isrecredit;
	}

	public HabaneroFunds getFunds() {
		return funds;
	}

	public void setFunds(HabaneroFunds funds) {
		this.funds = funds;
	}

	public Integer getRetrycount() {
		return retrycount;
	}

	public void setRetrycount(Integer retrycount) {
		this.retrycount = retrycount;
	}

	public HabaneroGameDetails getGamedetails() {
		return gamedetails;
	}

	public void setGamedetails(HabaneroGameDetails gamedetails) {
		this.gamedetails = gamedetails;
	}
	
}
