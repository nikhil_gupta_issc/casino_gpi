package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroQuery {

		@JsonProperty("transferid")
		private String transferid;
		
		@JsonProperty("accountid")
		private String accountid;
		
		@JsonProperty("token")
		private String token;
		
		@JsonProperty("queryamount")
		private String queryamount;

		public String getTransferid() {
			return transferid;
		}

		public void setTransferid(String transferid) {
			this.transferid = transferid;
		}

		public String getAccountid() {
			return accountid;
		}

		public void setAccountid(String accountid) {
			this.accountid = accountid;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public String getQueryamount() {
			return queryamount;
		}

		public void setQueryamount(String queryamount) {
			this.queryamount = queryamount;
		}
		
		
		
}
