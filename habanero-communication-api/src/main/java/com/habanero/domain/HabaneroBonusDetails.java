package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroBonusDetails {

	@JsonProperty("couponid")
	private String couponid;
	
	@JsonProperty("couponcode")
	private String couponcode;
	
	@JsonProperty("promobalanceid")
	private String promobalanceid;
	
	@JsonProperty("coupontypeid")
	private String coupontypeid;

	public String getCouponid() {
		return couponid;
	}

	public void setCouponid(String couponid) {
		this.couponid = couponid;
	}

	public String getCouponcode() {
		return couponcode;
	}

	public void setCouponcode(String couponcode) {
		this.couponcode = couponcode;
	}

	public String getPromobalanceid() {
		return promobalanceid;
	}

	public void setPromobalanceid(String promobalanceid) {
		this.promobalanceid = promobalanceid;
	}

	public String getCoupontypeid() {
		return coupontypeid;
	}

	public void setCoupontypeid(String coupontypeid) {
		this.coupontypeid = coupontypeid;
	}
	
}
