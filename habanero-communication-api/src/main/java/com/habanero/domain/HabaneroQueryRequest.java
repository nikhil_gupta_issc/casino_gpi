package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroQueryRequest extends HabaneroRequest {
	
	@JsonProperty("queryrequest")
	private HabaneroQuery queryrequest;

	public HabaneroQuery getQueryrequest() {
		return queryrequest;
	}

	public void setQueryrequest(HabaneroQuery queryrequest) {
		this.queryrequest = queryrequest;
	}
		
		
}
