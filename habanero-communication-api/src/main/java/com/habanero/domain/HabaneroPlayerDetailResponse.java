package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroPlayerDetailResponse {
	
	@JsonProperty("status")
	private HabaneroStatus status;
	
	@JsonProperty("accountid")
	private String accountid; //Player_id
	
	@JsonProperty("accountname")
	private String accountname; //player screen name
	
	@JsonProperty("balance")
	private String balance;
	
	@JsonProperty("currencycode")
	private String currencycode;
	
	@JsonProperty("segmentkey")
	private String segmentkey;
	
	public HabaneroPlayerDetailResponse(HabaneroStatus status) {
		super();
		this.status = status;
	}

	public HabaneroPlayerDetailResponse(String accountid, String accountname, String balance, String currencycode,
			String segmentkey, HabaneroStatus status) {
		super();
		this.accountid = accountid;
		this.accountname = accountname;
		this.balance = balance;
		this.currencycode = currencycode;
		this.segmentkey = segmentkey;
		this.status = status;
	}

	public HabaneroPlayerDetailResponse() {
		super();
	}
	
	public HabaneroStatus getStatus() {
		return status;
	}
	
	public void setStatus(HabaneroStatus status) {
		this.status = status;
	}

	public String getAccountid() {
		return accountid;
	}

	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}

	public String getAccountname() {
		return accountname;
	}

	public void setAccountname(String accountname) {
		this.accountname = accountname;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getCurrencycode() {
		return currencycode;
	}

	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}

	public String getSegmentkey() {
		return segmentkey;
	}

	public void setSegmentkey(String segmentkey) {
		this.segmentkey = segmentkey;
	}
	
	
}
