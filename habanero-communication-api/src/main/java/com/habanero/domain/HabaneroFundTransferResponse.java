package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroFundTransferResponse {

	@JsonProperty("status")
	private HabaneroStatus status;

	@JsonProperty("balance")
	private String balance;
	
	@JsonProperty("currencycode")
	private String currencycode;
	
	public HabaneroFundTransferResponse() {
		super();
	}

	public HabaneroFundTransferResponse(HabaneroStatus status) {
		super();
		this.status = status;
	}

	public HabaneroStatus getStatus() {
		return status;
	}

	public void setStatus(HabaneroStatus status) {
		this.status = status;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getCurrencycode() {
		return currencycode;
	}

	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}
	
	
}