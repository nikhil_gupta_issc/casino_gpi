package com.habanero.domain;

import java.io.IOException;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroFundInfo {

	@JsonProperty("transferid")
	private String transferid;
	
	@JsonProperty("amount")
	private String amount;
	
	@JsonProperty("dtevent")
	private String dtevent;
	
	@JsonProperty("currencycode")
	private String currencycode;
	
	@JsonProperty("gamestatemode")
	private Integer gamestatemode; 
	
	@JsonProperty("jpwin")
	private Boolean jpwin;
	
	@JsonProperty("jpid")
	private Long jpid;
	
	@JsonProperty("jpcont")
	private String jpcont;
	
	@JsonProperty("isbonus")
	private Boolean isbonus;
	
	@JsonProperty("initialdebittransferid")
	private String initialdebittransferid;

	public String getTransferid() {
		return transferid;
	}

	public void setTransferid(String transferid) {
		this.transferid = transferid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDtevent() {
		return dtevent;
	}

	public void setDtevent(String dtevent) {
		this.dtevent = dtevent;
	}

	public String getCurrencycode() {
		return currencycode;
	}

	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}

	public Integer getGamestatemode() {
		return gamestatemode;
	}

	public void setGamestatemode(Integer gamestatemode) {
		this.gamestatemode = gamestatemode;
	}

	public Boolean getJpwin() {
		return jpwin;
	}

	public void setJpwin(Boolean jpwin) {
		this.jpwin = jpwin;
	}

	public Long getJpid() {
		return jpid;
	}

	public void setJpid(Long jpid) {
		this.jpid = jpid;
	}

	public String getJpcont() {
		return jpcont;
	}

	public void setJpcont(String jpcont) {
		this.jpcont = jpcont;
	}

	public Boolean getIsbonus() {
		return isbonus;
	}

	public void setIsbonus(Boolean isbonus) {
		this.isbonus = isbonus;
	}

	public String getInitialdebittransferid() {
		return initialdebittransferid;
	}

	public void setInitialdebittransferid(String initialdebittransferid) {
		this.initialdebittransferid = initialdebittransferid;
	}
	
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
}
