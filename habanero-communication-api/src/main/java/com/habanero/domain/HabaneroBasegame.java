package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroBasegame {

	@JsonProperty("brandgameid")
	private String brandgameid;
	
	@JsonProperty("keyname")
	private String keyname;
	
	public String getBrandgameid() {
		return brandgameid;
	}
	public void setBrandgameid(String brandgameid) {
		this.brandgameid = brandgameid;
	}
	public String getKeyname() {
		return keyname;
	}
	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}

	

}