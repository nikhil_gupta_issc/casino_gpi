package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroTransactionRequest extends HabaneroRequest {
	
	@JsonProperty("basegame")
	private HabaneroBasegame basegame;
	
	@JsonProperty("auth")
	private HabaneroAuth auth;
	
	@JsonProperty("fundtransferrequest")
	private HabaneroFundTransferRequest fundtransferrequest;
	
	@JsonProperty("gamedetails")
	private HabaneroGameDetails gamedetails;
	
	@JsonProperty("bonusdetails")
	private HabaneroBonusDetails bonusdetails;

	public HabaneroBasegame getBasegame() {
		return basegame;
	}

	public void setBasegame(HabaneroBasegame basegame) {
		this.basegame = basegame;
	}

	public HabaneroAuth getAuth() {
		return auth;
	}

	public void setAuth(HabaneroAuth auth) {
		this.auth = auth;
	}

	public HabaneroFundTransferRequest getFundtransferrequest() {
		return fundtransferrequest;
	}

	public void setFundtransferrequest(HabaneroFundTransferRequest fundtransferrequest) {
		this.fundtransferrequest = fundtransferrequest;
	}

	public HabaneroGameDetails getGamedetails() {
		return gamedetails;
	}

	public void setGamedetails(HabaneroGameDetails gamedetails) {
		this.gamedetails = gamedetails;
	}

	public HabaneroBonusDetails getBonusdetails() {
		return bonusdetails;
	}

	public void setBonusdetails(HabaneroBonusDetails bonusdetails) {
		this.bonusdetails = bonusdetails;
	}


	
}
