package com.habanero.domain;

import java.io.IOException;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public abstract class HabaneroRequest {

	@JsonProperty("tt")
	private String tt;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("dtsent")
	private String dtsent;
	
	@JsonProperty("auth")
	private HabaneroAuth auth;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDtsent() {
		return dtsent;
	}

	public void setDtsent(String dtsent) {
		this.dtsent = dtsent;
	}
	
	public HabaneroAuth getAuth() {
		return auth;
	}

	public void setAuth(HabaneroAuth auth) {
		this.auth = auth;
	}
	
	public String getTt() {
		return tt;
	}

	public void setTt(String tt) {
		this.tt = tt;
	}
	
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
	
	
}

