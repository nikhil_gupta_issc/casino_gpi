package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class HabaneroGameDetails {

	@JsonProperty("brandgameid")
	private String brandgameid;
	
	@JsonProperty("keyname")
	private String keyname;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("gameinstanceid")
	private String gameinstanceid;
	
	@JsonProperty("friendlygameinstanceid")
	private String friendlygameinstanceid;
	
	@JsonProperty("gametypeid")
	private String gametypeid;
	
	@JsonProperty("gamesessionid")
	private String gamesessionid;
	
	@JsonProperty("gametypename")
	private String gametypename;

	public String getBrandgameid() {
		return brandgameid;
	}

	public void setBrandgameid(String brandgameid) {
		this.brandgameid = brandgameid;
	}

	public String getKeyname() {
		return keyname;
	}

	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGameinstanceid() {
		return gameinstanceid;
	}

	public void setGameinstanceid(String gameinstanceid) {
		this.gameinstanceid = gameinstanceid;
	}

	public String getFriendlygameinstanceid() {
		return friendlygameinstanceid;
	}

	public void setFriendlygameinstanceid(String friendlygameinstanceid) {
		this.friendlygameinstanceid = friendlygameinstanceid;
	}

	public String getGametypeid() {
		return gametypeid;
	}

	public void setGametypeid(String gametypeid) {
		this.gametypeid = gametypeid;
	}

	public String getGamesessionid() {
		return gamesessionid;
	}

	public void setGamesessionid(String gamesessionid) {
		this.gamesessionid = gamesessionid;
	}

	public String getGametypename() {
		return gametypename;
	}

	public void setGametypename(String gametypename) {
		this.gametypename = gametypename;
	}
	
}
