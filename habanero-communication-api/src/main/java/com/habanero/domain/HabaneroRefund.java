package com.habanero.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public class HabaneroRefund {

	@JsonProperty("gamemode")
	private String gamemode;
	
	@JsonProperty("originaltransferid")
	private String originaltransferid;
	
	@JsonProperty("transferid")
	private String transferid;
	
	@JsonProperty("currencycode")
	private String currencycode;
	
	@JsonProperty("amount")
	private String amount;
	
	@JsonProperty("jpcont")
	private String jpcont;
	
	@JsonProperty("dtevent")
	private String dtevent;
	
	@JsonProperty("initialdebittransferid")
	private String initialdebittransferid;

	public String getGamemode() {
		return gamemode;
	}

	public void setGamemode(String gamemode) {
		this.gamemode = gamemode;
	}

	public String getOriginaltransferid() {
		return originaltransferid;
	}

	public void setOriginaltransferid(String originaltransferid) {
		this.originaltransferid = originaltransferid;
	}

	public String getTransferid() {
		return transferid;
	}

	public void setTransferid(String transferid) {
		this.transferid = transferid;
	}

	public String getCurrencycode() {
		return currencycode;
	}

	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getJpcont() {
		return jpcont;
	}

	public void setJpcont(String jpcont) {
		this.jpcont = jpcont;
	}

	public String getDtevent() {
		return dtevent;
	}

	public void setDtevent(String dtevent) {
		this.dtevent = dtevent;
	}

	public String getInitialdebittransferid() {
		return initialdebittransferid;
	}

	public void setInitialdebittransferid(String initialdebittransferid) {
		this.initialdebittransferid = initialdebittransferid;
	}
	
}
