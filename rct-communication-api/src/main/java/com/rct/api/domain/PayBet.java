package com.rct.api.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by nacho on 21/01/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "payBet")
public class PayBet {

    @XmlElement(name = "processed", required = true)
    private String processed;

    @XmlElement(name = "key", required = true)
    private String key;

    @XmlElement(name = "newBalance", required = true)
    private Long newBalance;

    @XmlElement(name = "timestamp", required = true)
    private long timestamp;

    @XmlElement(name = "transactionId", required = true)
    private long transactionId;

    @XmlElement(name = "error", required = true)
    private String error;

    public String getProcessed() {
        return processed;
    }

    public void setProcessed(String processed) {
        this.processed = processed;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(Long newBalance) {
        this.newBalance = newBalance;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
