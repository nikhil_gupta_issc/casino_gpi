package com.rct.api.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by igabba on 20/01/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "openSession")
public class OpenSession {

    @XmlElement(name = "processed", required = true)
    private String processed;
    @XmlElement(name = "user", required = true)
    private String user;
    @XmlElement(name = "key", required = true)
    private String key;
    @XmlElement(name = "error", required = true)
    private String error;
    @XmlElement(name = "currency", required = true)
    private String currency;
    @XmlElement(name = "balance", required = true)
    private String balance;

    @XmlElement(name = "token", required = true)
    private String token;

    public String isProcessed() {
        return processed;
    }

    public void setProcessed(String processed) {
        this.processed = processed;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
