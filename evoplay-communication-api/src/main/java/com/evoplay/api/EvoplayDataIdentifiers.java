package com.evoplay.api;

public interface EvoplayDataIdentifiers {
	public static final String ROUND_ID = "round_id";
	public static final String ACTION_ID = "action_id";
	public static final String FINAL_ACTION = "final_action";
	public static final String AMOUNT = "amount";
	public static final String DETAILS = "details";
	public static final String CURRENCY = "currency";
	public static final String REFUND_ACTION_ID = "refund_action_id";
	public static final String REFUND_ROUND_ID = "refund_round_id";
	public static final Object REFUND_CALLBACK_ID = "refund_callback_id";
}
