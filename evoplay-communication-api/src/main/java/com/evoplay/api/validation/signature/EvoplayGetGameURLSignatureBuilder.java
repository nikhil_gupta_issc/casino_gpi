package com.evoplay.api.validation.signature;

/**
 * Signature builder for the get game url
 * 
 * @author Alexandre
 *
 */
public class EvoplayGetGameURLSignatureBuilder extends EvoplayAbstractSignatureBuilder {
	private Integer game;
	private String exitURL;
	private String language;
	private Integer https;
	private String denomination;
	private String currency;
	private Integer returnURLInfo;
	private Integer callbackVersion;
	
	public String buildSignature() {
		String baseSignature = this.buildBaseSignature();
		StringBuilder sb = new StringBuilder(baseSignature);
		
		sb.append(VALUE_SEPARATOR).append(this.game);
		
		String settingsString = null;
		
		if (this.exitURL != null || this.language != null || this.https != null)  {
			StringBuilder settingsStringBuilder = new StringBuilder();
			
			settingsStringBuilder.append(VALUE_SEPARATOR);
			
			if (this.exitURL != null) {
				settingsStringBuilder.append(this.exitURL).append(ARRAY_SEPARATOR);
			}
			
			if (this.language != null) {
				settingsStringBuilder.append(this.language).append(ARRAY_SEPARATOR);
			}
			
			if (this.https != null) {
				settingsStringBuilder.append(this.https);
			}
			
			settingsString = settingsStringBuilder.toString();
			settingsString = settingsString.replaceAll(ARRAY_SEPARATOR+"$", "");
			
			sb.append(settingsString);
			
		}
		
		sb.append(VALUE_SEPARATOR).append(this.denomination);
		sb.append(VALUE_SEPARATOR).append(this.currency);
		sb.append(VALUE_SEPARATOR).append(this.returnURLInfo);
		sb.append(VALUE_SEPARATOR).append(this.callbackVersion);
		
		String signature = sb.toString();
		signature = this.appendKeyToSignature(signature);
		
		return signature;
	}

	public void setGame(Integer game) {
		this.game = game;
	}

	public void setExitURL(String exitURL) {
		this.exitURL = exitURL;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setHttps(Integer https) {
		this.https = https;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setReturnURLInfo(Integer returnURLInfo) {
		this.returnURLInfo = returnURLInfo;
	}

	public void setCallbackVersion(Integer callbackVersion) {
		this.callbackVersion = callbackVersion;
	}
}
