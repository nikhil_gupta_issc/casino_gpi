package com.evoplay.api.validation.signature;

/**
 * Signature builder for the init callback
 * 
 * @author Alexandre
 *
 */
public class EvoplayInitCallbackSignatureBuilder extends EvoplayAbstractCallbackSignatureBuilder {
	
	public String buildSignature() {
		String baseSignature = this.buildBaseSignature();
		String signature = this.appendKeyToSignature(baseSignature);
		
		return signature;
	}
}
