package com.evoplay.api.validation.signature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.utils.ApplicationProperties;

public abstract class EvoplayAbstractSignatureBuilder {
	protected static final String VALUE_SEPARATOR = "*";
	protected static final String ARRAY_SEPARATOR = ":";
	
	private Integer projectNumber;
	private Integer version;
	private String token;
	
	/**
	 * Builds the base of every request signature
	 */
	protected String buildBaseSignature() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.projectNumber).append(VALUE_SEPARATOR);
		sb.append(this.version).append(VALUE_SEPARATOR);
		sb.append(this.token);
		
		return sb.toString();
	}
	
	protected String appendKeyToSignature(String signature) {
		StringBuilder sb = new StringBuilder(signature);
		String apiKey = ApplicationProperties.getProperty("evoplay.api.key");

		sb.append(VALUE_SEPARATOR).append(apiKey);
		
		return sb.toString();
	}
	
	public abstract String buildSignature();
	
	public void setToken(String token) {
		this.token = token;
	}

	public void setProjectNumber(Integer projectNumber) {
		this.projectNumber = projectNumber;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
}
