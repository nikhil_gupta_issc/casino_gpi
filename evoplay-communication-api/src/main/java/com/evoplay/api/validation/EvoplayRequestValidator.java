package com.evoplay.api.validation;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.evoplay.api.validation.signature.EvoplayAbstractCallbackSignatureBuilder;
import com.gpi.utils.ApplicationProperties;

/**
 * Validator for the requests received from evoplay
 * 
 * @author Alexandre
 *
 */
public class EvoplayRequestValidator {

	private static final Logger logger = LoggerFactory.getLogger(EvoplayRequestValidator.class);

	private Map<String, EvoplayAbstractCallbackSignatureBuilder> signatureBuilders;
	
	/**
	 * @param signature signature to hash
	 * @return md5 hashed signature
	 */
	public static String getHashedSignature(String signature) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(signature.getBytes());
			byte[] digest = md.digest();
			String myHash = DatatypeConverter.printHexBinary(digest).toLowerCase();

			return myHash;

		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}

		return null;
	}

	/**
	 * @param request request received from evoplay
	 * @return if the request received is valid based on its signature
	 */
	public boolean validate(EvoplayCallbackRequest request) {
		EvoplayAbstractCallbackSignatureBuilder signatureBuilder = this.signatureBuilders.get(request.getName());;
		Integer projectNumber = Integer.valueOf(ApplicationProperties.getProperty("evoplay.project.number"));
		Integer callbackVersion = Integer.valueOf(ApplicationProperties.getProperty("evoplay.api.callbackversion"));

		signatureBuilder.setProjectNumber(projectNumber);
		signatureBuilder.setVersion(callbackVersion);
		signatureBuilder.setToken(request.getToken());
		signatureBuilder.setCallbackID(request.getCallbackId());
		signatureBuilder.setName(request.getName());
		signatureBuilder.setData(request.getData());
		
		String buildSignature = signatureBuilder.buildSignature();
		
		String hashedSignature = getHashedSignature(buildSignature);
		
		return hashedSignature.equalsIgnoreCase(request.getSignature());
	}

	public void setSignatureBuilders(Map<String, EvoplayAbstractCallbackSignatureBuilder> signatureBuilders) {
		this.signatureBuilders = signatureBuilders;
	}
	
	
}
