package com.evoplay.api.validation.signature;

import com.evoplay.api.EvoplayDataIdentifiers;

/**
 * Signature builder for the refund callback
 * 
 * @author Alexandre
 *
 */
public class EvoplayRefundCallbackSignatureBuilder extends EvoplayAbstractCallbackSignatureBuilder {
	public String buildSignature() {
		String baseSignature = this.buildBaseSignature();
		
		StringBuilder settingsStringBuilder = new StringBuilder(baseSignature);
		settingsStringBuilder.append(VALUE_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.REFUND_ROUND_ID));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.REFUND_ACTION_ID));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.REFUND_CALLBACK_ID));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.AMOUNT));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.CURRENCY));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.DETAILS));
		
		String signature = settingsStringBuilder.toString();
		signature = this.appendKeyToSignature(signature);
		
		return signature;
	}
}
