package com.evoplay.api.validation.signature;

import com.evoplay.api.EvoplayDataIdentifiers;

/**
 * Signature builder for the win callback
 * 
 * @author Alexandre
 *
 */
public class EvoplayWinCallbackSignatureBuilder extends EvoplayAbstractCallbackSignatureBuilder {
	public String buildSignature() {
		String baseSignature = this.buildBaseSignature();
		
		StringBuilder settingsStringBuilder = new StringBuilder(baseSignature);
		settingsStringBuilder.append(VALUE_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.ROUND_ID));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.ACTION_ID));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.FINAL_ACTION));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.AMOUNT));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.CURRENCY));
		settingsStringBuilder.append(ARRAY_SEPARATOR).append(this.data.get(EvoplayDataIdentifiers.DETAILS));
		
		String signature = settingsStringBuilder.toString();
		signature = this.appendKeyToSignature(signature);
		
		return signature;
	}
}
