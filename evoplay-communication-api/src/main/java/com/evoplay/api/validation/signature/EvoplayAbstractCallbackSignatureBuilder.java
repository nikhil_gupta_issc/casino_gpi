package com.evoplay.api.validation.signature;

import java.util.Map;

/**
 * Abstract signature builder for evoplay requests
 * 
 * @author Alexandre
 *
 */
public abstract class EvoplayAbstractCallbackSignatureBuilder extends EvoplayAbstractSignatureBuilder {
	protected String callbackID;
	protected String name;
	protected Map<String, String> data;
	
	/**
	 * Builds the base of every callback signature
	 */
	@Override
	protected String buildBaseSignature() {
		String baseSignature = super.buildBaseSignature();
		StringBuilder sb = new StringBuilder(baseSignature);
		sb.append(VALUE_SEPARATOR).append(callbackID);
		sb.append(VALUE_SEPARATOR).append(name);
		
		return sb.toString();
	}
	
	public void setCallbackID(String callbackID) {
		this.callbackID = callbackID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}
	
	
	
}
