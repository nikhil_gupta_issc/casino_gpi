package com.evoplay.api.domain;

/**
 * Data field included in get game url response from evoplay
 * 
 * @author Alexandre
 *
 */
public class EvoplayGetGameURLResponseData {
	private String link;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "EvoplayGetGameURLResponseData [link=" + link + "]";
	}
	
}
