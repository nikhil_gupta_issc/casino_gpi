package com.evoplay.api.domain;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Success response from evoplay
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class EvoplayOkResponse extends EvoplayResponse {
	private EvoplayResponseData data;

	public EvoplayResponseData getData() {
		return data;
	}

	public void setData(EvoplayResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "EvoplayOkResponse [data=" + data + ", status=" + status + "]";
	}
}
