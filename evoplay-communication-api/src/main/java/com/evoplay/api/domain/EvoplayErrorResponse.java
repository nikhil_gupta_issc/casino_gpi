package com.evoplay.api.domain;

/**
 * Error response to evoplay
 * 
 * @author Alexandre
 *
 */
public class EvoplayErrorResponse extends EvoplayResponse{
	private EvoplayErrorData error;

	public EvoplayErrorData getError() {
		return error;
	}

	public void setError(EvoplayErrorData error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "EvoplayErrorResponse [error=" + error + "]";
	}
}
