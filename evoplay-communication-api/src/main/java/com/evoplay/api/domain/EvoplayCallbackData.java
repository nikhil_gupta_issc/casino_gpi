package com.evoplay.api.domain;

/**
 * Data field included in evoplay callback requests
 * 
 * @author Alexandre
 *
 */
public class EvoplayCallbackData {
	private long actionID;
	private double amount;
	private String currency;
	private String details;
	private String signature;
	
	public long getActionID() {
		return actionID;
	}
	
	public void setActionID(long actionID) {
		this.actionID = actionID;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	
	public String getSignature() {
		return signature;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return "EvoplayCallbackData [actionID=" + actionID + ", amount=" + amount + ", currency=" + currency
				+ ", details=" + details + ", signature=" + signature + "]";
	}
	
}
