package com.evoplay.api.domain;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Data field included in error response to evoplay requests
 * 
 * @author Alexandre
 *
 */
public class EvoplayErrorData {
	private String scope;
	private Integer noRefund;
	private String message;
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	
	@JsonProperty(value="no_refund")
	public Integer getNoRefund() {
		return noRefund;
	}
	
	public void setNoRefund(Integer noRefund) {
		this.noRefund = noRefund;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "EvoplayErrorData [scope=" + scope + ", noRefund=" + noRefund + ", message=" + message + "]";
	}
	
	
}
