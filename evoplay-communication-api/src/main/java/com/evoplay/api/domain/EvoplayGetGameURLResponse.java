package com.evoplay.api.domain;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Get game url response returned by evoplay
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class EvoplayGetGameURLResponse extends EvoplayResponse {
	private EvoplayGetGameURLResponseData data;

	public EvoplayGetGameURLResponseData getData() {
		return data;
	}

	public void setData(EvoplayGetGameURLResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "EvoplayGetGameURLResponse [data=" + data + "]";
	}
	
}
