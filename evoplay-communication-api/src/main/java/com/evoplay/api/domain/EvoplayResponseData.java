package com.evoplay.api.domain;

/**
 * Data field in response from evoplay
 * 
 * @author Alexandre
 *
 */
public class EvoplayResponseData {
	private double balance;
	private String currency;
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Override
	public String toString() {
		return "EvoplayResponseData [balance=" + balance + ", currency=" + currency + "]";
	}
	
	
}
