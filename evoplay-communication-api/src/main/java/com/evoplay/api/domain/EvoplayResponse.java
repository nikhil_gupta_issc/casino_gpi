package com.evoplay.api.domain;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Response from evoplay
 * 
 * @author Alexandre
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public abstract class EvoplayResponse {
	protected String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
