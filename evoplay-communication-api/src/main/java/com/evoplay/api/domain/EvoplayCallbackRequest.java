package com.evoplay.api.domain;

import java.util.Map;

/**
 * Callback request sent by Evoplay
 * 
 * @author Alexandre
 *
 */
public class EvoplayCallbackRequest {
	private String token;
	
	private String callbackID;
	private String name;
	private String signature;
	private Map<String, String> data;
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getCallbackId() {
		return callbackID;
	}
	
	public void setCallback_id(String callbackID) {
		this.callbackID = callbackID;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSignature() {
		return signature;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	public Map<String, String> getData() {
		return data;
	}
	
	public void setData(Map<String, String> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "EvoplayCallbackRequest [token=" + token + ", callbackID=" + callbackID + ", name=" + name
				+ ", signature=" + signature + ", data=" + data + "]";
	}
	
	
	
	
}
