package com.gpi.service.xml.impl;

import com.gpi.exceptions.MarshalException;
import com.gpi.service.xml.MessageMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;


public abstract class AbstractMessageMarshallerImpl implements MessageMarshaller {

    private static final Logger log = LoggerFactory.getLogger(AbstractMessageMarshallerImpl.class);

    @Override
    public Object unmarshall(String xml) throws MarshalException {
        Object unmarshalledObject;
        try {
            log.debug("Unmarshalling message");
            JAXBContext context = getContext();
            Unmarshaller unmarshall = context.createUnmarshaller();

            StringReader reader = new StringReader(xml);
            unmarshalledObject = unmarshall.unmarshal(reader);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new MarshalException(String.format("Error occurred while trying to unmarshal [%s] ", e.getMessage()));
        }
        return unmarshalledObject;
    }



    @Override
    public String marshall(Object object) throws MarshalException {
        log.debug("Marshalling object={}", object);

        Marshaller marshall;
        String xml;
        try {
            marshall = getContext().createMarshaller();
            marshall.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStream output = new OutputStream() {
                private final StringBuilder string = new StringBuilder();

                @Override
                public void write(int b) throws IOException {
                    this.string.append((char) b);
                }

                @Override
                public String toString() {
                    return this.string.toString();
                }
            };

            marshall.marshal(object, output);
            xml = output.toString();
        } catch (JAXBException e) {
            log.error("Error marshalling object: ", e);
            throw new MarshalException("Error occurred while trying to marshal ");
        }
        return xml;
    }

}
