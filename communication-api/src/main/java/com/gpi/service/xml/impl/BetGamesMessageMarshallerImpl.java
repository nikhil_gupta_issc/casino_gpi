package com.gpi.service.xml.impl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public class BetGamesMessageMarshallerImpl extends AbstractMessageMarshallerImpl {


    @Override
    public JAXBContext getContext() throws JAXBException {
        return JAXBContext.newInstance("com.bet.games.api");
    }
}
