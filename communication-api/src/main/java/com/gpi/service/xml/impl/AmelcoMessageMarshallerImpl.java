package com.gpi.service.xml.impl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public class AmelcoMessageMarshallerImpl extends AbstractMessageMarshallerImpl {


    @Override
    public JAXBContext getContext() throws JAXBException {
        return JAXBContext.newInstance("com.amelco.api");
    }
}
