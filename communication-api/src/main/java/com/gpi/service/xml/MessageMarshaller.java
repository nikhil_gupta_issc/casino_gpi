package com.gpi.service.xml;

import com.gpi.exceptions.MarshalException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public interface MessageMarshaller {


    /**
     * This method receives a XML and transform it to a object. See xsd's for a valid xml.
     *
     * @param xml The String containing the XML
     * @return The object unmarshalled
     * @throws com.gpi.exceptions.MarshalException if the xml parameter don't contain a valid XML
     */
    public Object unmarshall(String xml) throws MarshalException;

    /**
     * This method receives an object and transform this object in a String.
     *
     * @param object to be marshalled.
     * @return A String containing the XML representation of the object.
     * @throws com.gpi.exceptions.MarshalException
     */
    public String marshall(Object object) throws MarshalException;

    public JAXBContext getContext() throws JAXBException;
}
