package com.gpi.service.xml.impl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 * Created by igabba on 20/01/15.
 */
public class RCTMessageMarshallerImpl extends AbstractMessageMarshallerImpl {


    @Override
    public JAXBContext getContext() throws JAXBException {
        return JAXBContext.newInstance("com.rct.api.domain");
    }
}
