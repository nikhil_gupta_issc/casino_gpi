package com.gpi.exceptions;

public class CommunicationException extends Exception {

    private static final long serialVersionUID = 1498711273414701413L;

    public CommunicationException(Exception e) {
        super(e);
    }

    public CommunicationException(String s) {
        super(s);
    }
}
