package com.gpi.exceptions;

public class ApiException extends Exception {

    private static final long serialVersionUID = -8996887255137394650L;

    public ApiException(String message) {
        super(message);
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
