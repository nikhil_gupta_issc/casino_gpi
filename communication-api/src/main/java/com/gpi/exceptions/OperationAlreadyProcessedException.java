package com.gpi.exceptions;

import com.gpi.dto.ApiResponseDto;

/**
 * User: igabbarini
 */
public class OperationAlreadyProcessedException extends ApiException {
    private static final long serialVersionUID = -1427624851393305076L;

    private final ApiResponseDto apiResponseDto;

    public OperationAlreadyProcessedException(String s, ApiResponseDto apiResponseDto) {
        super(s);
        this.apiResponseDto = apiResponseDto;
    }

    public ApiResponseDto getApiResponseDto() {
        return apiResponseDto;
    }
}
