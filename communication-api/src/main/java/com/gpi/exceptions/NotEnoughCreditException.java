package com.gpi.exceptions;

import com.gpi.utils.ApplicationProperties;

public class NotEnoughCreditException extends ApiException {

    private static final long serialVersionUID = 4383905773412070776L;

    private static final String ERROR_CODE = "gpi.not.enough.credit.exception";
    private Long balance;
    private String token;

    public NotEnoughCreditException() {
        super(ApplicationProperties.getProperty(ERROR_CODE));
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
