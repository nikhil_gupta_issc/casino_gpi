package com.gpi.dto;


import com.gpi.api.MethodType;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.currency.Currency;

import java.util.List;

/**
 * Created by Nacho on 2/20/14.
 */
public class MessageDto {

    private String user;
    private Currency currency;
    private Long transactionID;
    private Long betReferenceNum;
    private Long winReferenceNum;
    private Long clearBetsReferenceNum;
    private String gameReference;
    private List<Transaction> refundedTransactions;

    private boolean repeatedTransaction;
    private MethodType methodType;
    private Long amount;

    public String getGameReference() {
        return gameReference;
    }

    public Long getTransactionID() {
        return transactionID;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }


    public boolean isRepeatedTransaction() {
        return repeatedTransaction;
    }


    public MethodType getMethodType() {
        return methodType;
    }


}
