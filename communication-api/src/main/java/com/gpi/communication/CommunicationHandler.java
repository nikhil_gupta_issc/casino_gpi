package com.gpi.communication;

import com.gpi.api.Message;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.CommunicationException;
import com.gpi.exceptions.MarshalException;

/**
 *
 */
public interface CommunicationHandler {

    /**
     * This method is responsible for execute a POST to the specified url with a specific message.
     *
     * @param publisher     where the httpclient should do the POST
     * @param message the text for the body message
     * @return A String containing the response from the request.
     */
    public Message communicate(Publisher publisher, Message message) throws CommunicationException;

    Object transformMessageToProperObject(Message message) throws MarshalException, CommunicationException;

    Message transformObjectToMessage(Object o) throws CommunicationException;

}
