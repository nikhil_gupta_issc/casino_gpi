package com.gpi.communication;

import com.gpi.api.Message;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.CommunicationException;

/**
 * Created by igabba on 20/01/15.
 */
public interface CommunicationHandlerSelector {

    public Message communicate(Publisher publisher, Message message) throws CommunicationException;
}
