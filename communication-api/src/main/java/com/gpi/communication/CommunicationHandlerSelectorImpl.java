package com.gpi.communication;

import com.gpi.api.Message;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.CommunicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by nacho on 26/11/14.
 */
public class CommunicationHandlerSelectorImpl implements CommunicationHandlerSelector {

    private static final Logger log = LoggerFactory.getLogger(CommunicationHandlerSelectorImpl.class);
    private Map<String, CommunicationHandler> communicationHandlerMap;


    public Message communicate(Publisher publisher, Message message) throws CommunicationException {
        log.debug("Executing post to the publisher={}", publisher.getName());
        CommunicationHandler handler = communicationHandlerMap.get(publisher.getApiName());
        
        if (handler == null) {
            throw new CommunicationException("No handler defined to manage the communication: " + publisher.getApiName());
        }
        
        return handler.communicate(publisher, message);
    }

    public void setCommunicationHandlerMap(Map<String, CommunicationHandler> communicationHandlerMap) {
        this.communicationHandlerMap = communicationHandlerMap;
    }
}
