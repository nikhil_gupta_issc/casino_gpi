package com.gpi.utils;

import com.gpi.exceptions.MarshalException;
import com.gpi.service.xml.MessageMarshaller;
import com.gpi.utils.ApplicationContextProvider;

/**
 * Created by igabba on 20/01/15.
 */
public class RCTMessageHelper {

    private static MessageMarshaller messageMarshaller;

    private static MessageMarshaller getMessageMarshaller() {
        if (messageMarshaller == null) {
            messageMarshaller = (MessageMarshaller) ApplicationContextProvider.getInstance().getBean("rctMessageMarshaller");
        }
        return messageMarshaller;
    }

    public static String createStringFromObject(Object object) throws MarshalException {
        return getMessageMarshaller().marshall(object);
    }
}
