package com.gpi.utils;

import com.bet.games.api.Params;
import com.bet.games.api.Root;
import com.gpi.api.*;
import com.gpi.api.Error;
import com.gpi.domain.audit.Transaction;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.MarshalException;
import com.gpi.exceptions.TransactionAlreadyProcessedException;
import com.gpi.service.token.TokenService;
import com.gpi.service.xml.MessageMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BetGamesMessageHelper {

    private static final Logger logger = LoggerFactory.getLogger(BetGamesMessageHelper.class);
    private static MessageMarshaller messageMarshaller;

    private static MessageMarshaller getMessageMarshaller() {
        if (messageMarshaller == null) {
            messageMarshaller = (MessageMarshaller) ApplicationContextProvider.getInstance().getBean("betGamesMessageMarshaller");
        }
        return messageMarshaller;
    }

    public static String createStringFromMessage(Root message) throws MarshalException {
        return getMessageMarshaller().marshall(message);
    }

    public static Root createMessageFromString(String xml) throws MarshalException {
        return (Root) getMessageMarshaller().unmarshall(xml);
    }
}