package com.gpi.utils;

import com.gpi.api.*;
import com.gpi.api.Error;
import com.gpi.domain.audit.Transaction;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.MarshalException;
import com.gpi.exceptions.TransactionAlreadyProcessedException;
import com.gpi.service.token.TokenService;
import com.gpi.service.xml.MessageMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class MessageHelper {

    private static final Logger logger = LoggerFactory.getLogger(MessageHelper.class);
    private static MessageMarshaller messageMarshaller;
    private final Credentials credentials = new Credentials();
    private final Token token;
    private final Params params = new Params();
    private final Message message = new Message();
    private final Method method = new Method();
    
    public static final int TRANSACTION_NOT_FOUND_ERROR_CODE = 202;
    public static final int PREVIOUS_ROUND_NOT_EXISTS_ERROR_CODE = 9999;

    /**
     * This constructor creates a standard MessageHelper object with the specific MethodType
     *
     * @param loginName
     * @param password
     * @param methodType
     * @param token
     */
    public MessageHelper(String loginName, String password, MethodType methodType, String token) {
        this.credentials.setLogin(loginName);
        this.credentials.setPassword(password);
        this.method.setCredentials(credentials);
        if (methodType != null) {
            this.method.setName(methodType);
            method.setParams(params);
            message.setMethod(method);
        }
        this.token = new Token();
        this.token.setValue(token);
        this.params.setToken(this.token);
    }

    public static Message changeTransactionId(Message message, String id) {
        TransactionId transactionId = message.getMethod().getParams().getTransactionId();
        logger.debug("Setting new TransactionId={}, the original was={}", id, transactionId.getValue());
        transactionId.setValue(id);
        return message;
    }

    public static void changeRoundId(Message message, Long id) {
        RoundId roundId = message.getMethod().getParams().getRoundId();
        logger.debug("Setting new TransactionId={}, the original was={}", id, roundId.getValue());
        roundId.setValue(id);
    }

    public static void setAlreadyProcess(Message message, boolean b) {
        AlreadyProcessed alreadyProcessed = new AlreadyProcessed();
        alreadyProcessed.setValue(b);
        message.getResult().getReturnset().setAlreadyProcessed(alreadyProcessed);
    }

    public static void changeToken(Message gameMessage, String publisherToken) {
        logger.info("Changing token from={} to {}", gameMessage.getMethod().getParams().getToken().getValue(), publisherToken);
        gameMessage.getMethod().getParams().getToken().setValue(publisherToken);
    }
    
    public static void changeCredentials(Message gameMessage, String newLoginName, String newPassword) {
    	gameMessage.getMethod().getCredentials().setLogin(newLoginName);
    	gameMessage.getMethod().getCredentials().setPassword(newPassword);
    }

    /*
    Create a xml response to communicate the exception to the game provider
     */
    public static Message createErrorResponse(GamePlatformIntegrationException e, String resultName) {
       
            Error error = new Error();
            ErrorCode errorCode = new ErrorCode();
            logger.info("creating error for: "+e);
            if (e instanceof TransactionAlreadyProcessedException) {
                Message message = getTransactionAlreadyProcessedException((TransactionAlreadyProcessedException) e, resultName);
                return message;
            } else {
                error.setValue(((e.getErrorMessage()!=null)?e.getMessage():"Internal error"));
                errorCode.setValue(e.getErrorCode());
            }
            Result result = new Result();
            result.setSuccess(0);
            if (resultName != null) {
                MethodType methodType = MethodType.fromValue(resultName);
                result.setName(methodType);
            } else {
                MethodType methodType = MethodType.GET_PLAYER_INFO;
                result.setName(methodType);
            }
            Returnset returnset = new Returnset();
            returnset.setErrorCode(errorCode);
            returnset.setError(error);
            result.setReturnset(returnset);
            Message message1 = new Message();
            message1.setResult(result);
            return message1;
      
    }
    
    public static Message createErrorResponse(int code, MethodType method, String errorMessage) {
    	Error error = new Error();
        ErrorCode errorCode = new ErrorCode();
        logger.info("creating error with errorCode=" + code + " and message " + errorMessage);
        Result result = new Result();
        result.setSuccess(0);
        if (method != null) {
            result.setName(method);
        } else {
            MethodType methodType = MethodType.GET_PLAYER_INFO;
            result.setName(methodType);
        }
        Returnset returnset = new Returnset();
        returnset.setErrorCode(errorCode);
        returnset.setError(error);
        result.setReturnset(returnset);
        Message message1 = new Message();
        message1.setResult(result);
        return message1;
    }

    private static Message getTransactionAlreadyProcessedException(TransactionAlreadyProcessedException e, String resultName) {
        Message message = new Message();
        Result result = new Result();
        result.setName(MethodType.fromValue(resultName));
        result.setSuccess(0);
        Returnset returnset = new Returnset();
        AlreadyProcessed alreadyProcessed = new AlreadyProcessed();
        alreadyProcessed.setValue(true);
        returnset.setAlreadyProcessed(alreadyProcessed);
        TransactionId transactionId = new TransactionId();
        transactionId.setValue(String.valueOf(e.getT().getId()));
        Token token1 = new Token();
        token1.setValue(TokenService.generateToken());
        returnset.setToken(token1);
        returnset.setTransactionId(transactionId);
        result.setReturnset(returnset);
        message.setResult(result);
        return message;
    }

    private static MessageMarshaller getMessageMarshaller() {
        if (messageMarshaller == null) {
            messageMarshaller = (MessageMarshaller) ApplicationContextProvider.getInstance().getBean("messageMarshaller");
        }
        return messageMarshaller;
    }

    public static String createStringFromMessage(Message message) throws MarshalException {
        return getMessageMarshaller().marshall(message);
    }

    public static Message createMessageFromString(String xml) throws MarshalException {
        return (Message) getMessageMarshaller().unmarshall(xml);
    }

    private static List<Long> getTransactionsIds(List<Transaction> transactions) {
        List<Long> ids = new ArrayList<>();
        if (transactions != null) {

            for (Transaction t : transactions) {
                ids.add(t.getId());
            }
        }
        return ids;
    }

    public static Message createResponse(MethodType name, String newToken) {
        Message m = new Message();
        Result result = new Result();
        result.setName(name);
        result.setSuccess(1);
        Returnset returnset = new Returnset();
        m.setResult(result);
        result.setReturnset(returnset);
        Token token1 = new Token();
        token1.setValue(newToken);
        returnset.setToken(token1);
        return m;
    }

    public static void addLoginName(Message message, String playerName) {
        LoginName loginName = new LoginName();
        loginName.setValue(playerName);
        message.getResult().getReturnset().setLoginName(loginName);
    }
    
    public static void addCurrency(Message message, String currency) {
        Currency currency1 = new Currency();
        currency1.setValue(currency);
        message.getResult().getReturnset().setCurrency(currency1);
    }

    public static void addBalance(Message messageResponse, Long balance) {
        Balance b = new Balance();
        b.setValue(balance);
        messageResponse.getResult().getReturnset().setBalance(b);
    }

    public static Message copyMessage(Message message) {
        Message newMessage = new Message();
        Method originalMethod = message.getMethod();
        if (originalMethod != null) {
            Method newMethod = new Method();
            newMethod.setName(originalMethod.getName());
            newMessage.setMethod(newMethod);
            Params originalMethodParams = originalMethod.getParams();
            if (originalMethod.getCredentials() != null) {
                Credentials c = new Credentials();
                c.setPassword(originalMethod.getCredentials().getPassword());
                c.setLogin(originalMethod.getCredentials().getLogin());
                newMethod.setCredentials(c);
            }
            if (originalMethodParams != null) {
                Params newParams = new Params();
                newMethod.setParams(newParams);
                if (originalMethodParams.getTransactionId() != null) {
                    TransactionId t = new TransactionId();
                    t.setValue(originalMethodParams.getTransactionId().getValue());
                    newParams.setTransactionId(t);
                }
                if (originalMethodParams.getAmount() != null) {
                    Amount a = new Amount();
                    a.setValue(originalMethodParams.getAmount().getValue());
                    newParams.setAmount(a);
                }
                if (originalMethodParams.getCurrency() != null) {
                    Currency currency = new Currency();
                    currency.setValue(originalMethodParams.getCurrency().getValue());
                    newParams.setCurrency(currency);
                }
                if (originalMethodParams.getGameReference() != null) {
                    GameReference gameReference = new GameReference();
                    gameReference.setValue(originalMethodParams.getGameReference().getValue());
                    newParams.setGameReference(gameReference);
                }
                if (originalMethodParams.getRefundedTransactionId() != null) {
                    RefundedTransactionId transactionId = new RefundedTransactionId();
                    transactionId.setValue(originalMethodParams.getRefundedTransactionId().getValue());
                    newParams.setRefundedTransactionId(transactionId);
                }
                if (originalMethodParams.getRoundId() != null) {
                    RoundId roundId = new RoundId();
                    roundId.setValue(originalMethodParams.getRoundId().getValue());
                    newParams.setRoundId(roundId);
                }
                if (originalMethodParams.getToken() != null) {
                    Token t = new Token();
                    t.setValue(originalMethodParams.getToken().getValue());
                    newParams.setToken(t);
                }
                if (originalMethodParams.getFreeSpin() != null) {
                    FreeSpin freeSpin = new FreeSpin();
                    freeSpin.setValue(originalMethodParams.getFreeSpin().isValue());
                    newParams.setFreeSpin(freeSpin);
                }
                if (originalMethodParams.getJackpotAmount() != null) {
                    JackpotAmount jackpotAmount = new JackpotAmount();
                    jackpotAmount.setValue(originalMethodParams.getJackpotAmount().getValue());
                    newParams.setJackpotAmount(jackpotAmount);
                }
            }

        }
        if (message.getResult() != null) {
            Result newResult = new Result();
            newResult.setName(message.getResult().getName());
            newResult.setSuccess(message.getResult().getSuccess());
            newMessage.setResult(newResult);
            if (message.getResult().getReturnset() != null) {
                Returnset newReturnset = new Returnset();
                newResult.setReturnset(newReturnset);
                if (message.getResult().getReturnset().getTransactionId() != null) {
                    TransactionId t = new TransactionId();
                    t.setValue(message.getResult().getReturnset().getTransactionId().getValue());
                    newReturnset.setTransactionId(t);
                }
                if (message.getResult().getReturnset().getCurrency() != null) {
                    Currency currency = new Currency();
                    currency.setValue(message.getResult().getReturnset().getCurrency().getValue());
                    newReturnset.setCurrency(currency);
                }
                if (message.getResult().getReturnset().getBalance() != null) {
                    Balance balance = new Balance();
                    balance.setValue(message.getResult().getReturnset().getBalance().getValue());
                    newReturnset.setBalance(balance);
                }
                if (message.getResult().getReturnset().getError() != null) {
                    Error error = new Error();
                    error.setValue(message.getResult().getReturnset().getError().getValue());
                    newReturnset.setError(error);
                }
                if (message.getResult().getReturnset().getErrorCode() != null) {
                    ErrorCode e = new ErrorCode();
                    e.setValue(message.getResult().getReturnset().getErrorCode().getValue());
                    newReturnset.setErrorCode(e);
                }
                if (message.getResult().getReturnset().getToken() != null) {
                    Token t = new Token();
                    t.setValue(message.getResult().getReturnset().getToken().getValue());
                    newReturnset.setToken(t);
                }
                if (message.getResult().getReturnset().getLoginName() != null) {
                    LoginName l = new LoginName();
                    l.setValue(message.getResult().getReturnset().getLoginName().getValue());
                    newReturnset.setLoginName(l);
                }
                if ((message.getResult().getReturnset().getAlreadyProcessed() != null)) {
                    AlreadyProcessed alreadyProcessed = new AlreadyProcessed();
                    alreadyProcessed.setValue(message.getResult().getReturnset().getAlreadyProcessed().isValue());
                    newReturnset.setAlreadyProcessed(alreadyProcessed);
                }
            }
        }
        return newMessage;
    }

    public Message getMessage() {
        return message;
    }

    public MessageHelper setGameReference(String game) {
        GameReference gameReference1 = new GameReference();
        gameReference1.setValue(game);
        this.params.setGameReference(gameReference1);
        return this;
    }

    public MessageHelper setAmount(Long refundAmount) {
        Amount amount = new Amount();
        amount.setValue(refundAmount);
        this.params.setAmount(amount);
        return this;
    }
    
    public static void addRefundedTransactionIdToMethod(Message message, String transactionId) {
        RefundedTransactionId refundedTransaction = new RefundedTransactionId();
        refundedTransaction.setValue(transactionId);
        message.getMethod().getParams().setRefundedTransactionId(refundedTransaction);
    }

    public static void addAmountToMethod(Message betMessage, Long amount) {
        Amount amount1 = new Amount();
        amount1.setValue(amount);
        betMessage.getMethod().getParams().setAmount(amount1);
    }
    
    public static void addTokenToMethod(Message message, String token) {
    	Token token1 = new Token();
    	token1.setValue(token);
    	message.getResult().getReturnset().setToken(token1);
    }

    public static void addTransactionIdToMethod(Message betMessage, String transactionId) {
        TransactionId transaction = new TransactionId();
        transaction.setValue(transactionId);
        betMessage.getMethod().getParams().setTransactionId(transaction);
    }
    
    public static void addTransactionIdToReturnset(Message betMessage, String transactionId) {
        TransactionId transaction = new TransactionId();
        transaction.setValue(transactionId);
        betMessage.getResult().getReturnset().setTransactionId(transaction);
    }

    public static void addGameReferenceToMethod(Message betMessage, String name) {
        GameReference gameReference = new GameReference();
        gameReference.setValue(name);
        betMessage.getMethod().getParams().setGameReference(gameReference);
    }

    public static void addRoundIdToMethod(Message betMessage, Long roundId) {
        RoundId roundId1 = new RoundId();
        roundId1.setValue(roundId);
        betMessage.getMethod().getParams().setRoundId(roundId1);
    }

    public static Message createBasicMessageResponse(MethodType getPlayerInfo, String token) {
        Message message = new Message();

        Result result = new Result();
        result.setName(getPlayerInfo);
        result.setSuccess(1);

        Returnset returnset = new Returnset();
        returnset.setToken(new Token(token));

        returnset.setAlreadyProcessed(new AlreadyProcessed(false));

        result.setReturnset(returnset);

        message.setResult(result);
        return message;
    }

}