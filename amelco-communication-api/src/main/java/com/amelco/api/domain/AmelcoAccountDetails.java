package com.amelco.api.domain;

import java.io.Serializable;

public class AmelcoAccountDetails implements Serializable {
	private String username;
	private String currencyCode;
	private long accountId;
	
	public AmelcoAccountDetails(String username, String currencyCode, long accountId) {
		this.username = username;
		this.currencyCode = currencyCode;
		this.accountId = accountId;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public long getAccountId() {
		return accountId;
	}
	
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
}
