package com.heliogaming.api;

import java.io.IOException;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeliogamingWinData {

	private Long transactionID;
	
	private Long couponID;
	
	private String currencyCode;
	
	private Double amount;
	
	private String sessionKey;
	
	private String timestamp;
	
	private HeliogamingWinGroupData group;
	
	public Long getTransactionID() {
		return transactionID;
	}

	@JsonProperty("TransactionID")
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}
	
	@JsonProperty("CurrencyCode")
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Long getCouponID() {
		return couponID;
	}
	
	@JsonProperty("CouponID")
	public void setCouponID(Long couponID) {
		this.couponID = couponID;
	}

	public Double getAmount() {
		return amount;
	}
	
	@JsonProperty("Amount")
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getSessionKey() {
		return sessionKey;
	}

	@JsonProperty("SessionKey")
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getTimestamp() {
		return timestamp;
	}
	
	@JsonProperty("Timestamp")
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public HeliogamingWinGroupData getGroup() {
		return group;
	}

	@JsonProperty("Group")
	public void setGroups(HeliogamingWinGroupData group) {
		this.group = group;
	}
	
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
}