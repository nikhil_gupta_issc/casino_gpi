package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class HeliogamingCancelWinRequest extends HeliogamingCoreRequest {

	private HeliogamingWinData[] cancelledWins;

	public HeliogamingWinData[] getCancelledWins() {
		return cancelledWins;
	}

	@JsonProperty("CancelledWins")
	public void setCancelledWins(HeliogamingWinData[] cancelledWins) {
		this.cancelledWins = cancelledWins;
	}
}
