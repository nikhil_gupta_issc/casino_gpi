package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class HeliogamingGameData{
	
	@Deprecated
	private Integer gameID;
	
	private String gameGroupCode;
	
	private Boolean isPrimary;

	@Deprecated
	public Integer getGameID() {
		return gameID;
	}

	@JsonProperty("GameID")
	@Deprecated
	public void setGameID(Integer gameID) {
		this.gameID = gameID;
	}

	public String getGameGroupCode() {
		return this.gameGroupCode;
	}
	
	@JsonProperty("GameGroupCode")
	public void setGameGroupCode(String gameGroupCode) {
		this.gameGroupCode = gameGroupCode;
	}
	
	public Boolean getIsPrimary() {
		return isPrimary;
	}

	@JsonProperty("IsPrimary")
	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}
	
}
