package com.heliogaming.api;

import java.io.IOException;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public abstract class HeliogamingCoreRequest {
	
	protected String username;
	
	protected String password;
	
	protected String methodType;

	protected String rawJson;
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@JsonProperty("UserName")
	public void setUsername(String username) {
		this.username = username;
	}

	@JsonProperty("Password")
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getMethodType() {
		return methodType;
	}
	
	@JsonProperty("MethodType")
	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}
	
	public String getRawJson() {
		return rawJson;
	}

	public void setRawJson(String rawJson) {
		this.rawJson = rawJson;
	}

	public static<T> T create(String json, Class<T> klass) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			T request = objectMapper.readValue(json, klass);
			((HeliogamingCoreRequest)request).setRawJson(json);
			return request;
		} catch (IOException e) {
			return null;
		}
	}
	
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
	
}
