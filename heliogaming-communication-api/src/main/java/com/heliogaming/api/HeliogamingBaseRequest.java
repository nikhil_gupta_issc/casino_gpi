package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public abstract class HeliogamingBaseRequest extends HeliogamingCoreRequest{

	@Deprecated
	protected Integer gameID;

	protected String authenticationType;
	
	protected HeliogamingSessionKeyAuthenticationData sessionKeyAuthentication;

	protected String sessionKey;

	protected String gameGroupCode;
	
	/**
	 * @return the gameID
	 */
	@Deprecated
	public Integer getGameID() {
		return gameID;
	}

	@JsonProperty("GameID")
	@Deprecated
	public void setGameID(Integer gameID) {
		this.gameID = gameID;
	}
	
	/**
	 * @return the authenticationType
	 */
	public String getAuthenticationType() {
		return authenticationType;
	}

	@JsonProperty("AuthenticationType")
	public void setAuthenticationType(String authenticationType) {
		this.authenticationType = authenticationType;
	}
	
	/**
	 * @return the sessionKeyAuthentication
	 */
	public HeliogamingSessionKeyAuthenticationData getSessionKeyAuthentication() {
		return sessionKeyAuthentication;
	}

	@JsonProperty("SessionKeyAuthentication")
	public void setSessionKeyAuthentication(HeliogamingSessionKeyAuthenticationData sessionKeyAuthentication) {
		this.sessionKeyAuthentication = sessionKeyAuthentication;
	}
	
	/**
	 * @return the sessionKey
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	@JsonProperty("SessionKey")
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getGameGroupCode() {
		return this.gameGroupCode;
	}
	
	@JsonProperty("GameGroupCode")
	public void setGameGroupCode(String gameGroupCode) {
		this.gameGroupCode = gameGroupCode;
	}
	
}
