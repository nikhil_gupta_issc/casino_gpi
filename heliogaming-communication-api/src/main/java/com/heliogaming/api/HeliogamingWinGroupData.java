package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class HeliogamingWinGroupData{
	
	private Long participationID;
	
	private Double amount;
	
	private HeliogamingTransactionData transaction;	

	public Long getParticipationID() {
		return participationID;
	}

	@JsonProperty("ParticipationID")
	public void setParticipationID(Long participationID) {
		this.participationID = participationID;
	}

	public Double getAmount() {
		return amount;
	}

	@JsonProperty("Amount")
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public HeliogamingTransactionData getTransaction() {
		return transaction;
	}

	@JsonProperty("Transaction")
	public void setTransactions(HeliogamingTransactionData transaction) {
		this.transaction = transaction;
	}
	
}
