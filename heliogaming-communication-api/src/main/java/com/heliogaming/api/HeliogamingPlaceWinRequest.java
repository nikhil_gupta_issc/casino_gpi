package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class HeliogamingPlaceWinRequest extends HeliogamingCoreRequest {
	
	private HeliogamingWinData[] wins;

	public HeliogamingWinData[] getWins() {
		return wins;
	}

	@JsonProperty("Wins")
	public void setWins(HeliogamingWinData[] wins) {
		this.wins = wins;
	}
	
	
}
