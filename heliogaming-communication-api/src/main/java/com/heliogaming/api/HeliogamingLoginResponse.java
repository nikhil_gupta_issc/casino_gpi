package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonProperty;

public class HeliogamingLoginResponse extends HeliogamingCoreResponse {
	
	private HeliogamingBalanceData balance;
	
	private HeliogamingPlayerData player;

	@JsonProperty("Balance")
	public HeliogamingBalanceData getBalance() {
		return balance;
	}
	
	public void setBalance(HeliogamingBalanceData balance) {
		this.balance = balance;
	}
	
	@JsonProperty("Player")
	public HeliogamingPlayerData getPlayer() {
		return player;
	}
	
	public void setPlayer(HeliogamingPlayerData player) {
		this.player = player;
	}
	
	
}
