package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeliogamingBetGroupData{
	
	private Long participationID;
	
	private Double amount;
	
	private HeliogamingTransactionData[] transactions;

	public Long getParticipationID() {
		return participationID;
	}

	@JsonProperty("ParticipationID")
	public void setParticipationID(Long participationID) {
		this.participationID = participationID;
	}

	public Double getAmount() {
		return amount;
	}

	@JsonProperty("Amount")
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public HeliogamingTransactionData[] getTransactions() {
		return transactions;
	}

	@JsonProperty("Transactions")
	public void setTransactions(HeliogamingTransactionData[] transactions) {
		this.transactions = transactions;
	}
	
}
