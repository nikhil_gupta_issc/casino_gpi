package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class HeliogamingStaggeredPaymentRequest extends HeliogamingCoreRequest {
	
	private HeliogamingWinData staggeredPayment;

	public HeliogamingWinData getStaggeredPayment() {
		return staggeredPayment;
	}

	@JsonProperty("StaggeredPayment")
	public void setStaggeredPayment(HeliogamingWinData staggeredPayment) {
		this.staggeredPayment = staggeredPayment;
	}
	
	
}
