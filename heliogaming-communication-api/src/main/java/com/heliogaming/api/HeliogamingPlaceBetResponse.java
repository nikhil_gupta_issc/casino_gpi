package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonProperty;

public class HeliogamingPlaceBetResponse extends HeliogamingCoreResponse {
	
	private HeliogamingBalanceData balance;
	
	@JsonProperty("Balance")
	public HeliogamingBalanceData getBalance() {
		return balance;
	}
	
	public void setBalance(HeliogamingBalanceData balance) {
		this.balance = balance;
	}
}
