package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeliogamingPlaceBetRequest extends HeliogamingBaseRequest {
	
	private HeliogamingBetData bet;
	
	public HeliogamingBetData getBet() {
		return bet;
	}
	
	@JsonProperty("Bet")
	public void setBet(HeliogamingBetData bet){
		this.bet = bet;
	}
	
}
