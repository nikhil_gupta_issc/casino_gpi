package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeliogamingBetData {

	private String currencyCode;
	
	private Long couponID;
	
	private Double amount;
	
	private String timestamp;

	private HeliogamingGameData[] games;
	
	private HeliogamingBetGroupData[] groups;
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	
	@JsonProperty("CurrencyCode")
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Long getCouponID() {
		return couponID;
	}
	
	@JsonProperty("CouponID")
	public void setCouponID(Long couponID) {
		this.couponID = couponID;
	}

	public Double getAmount() {
		return amount;
	}
	
	@JsonProperty("Amount")
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	@JsonProperty("Timestamp")
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public HeliogamingGameData[] getGames() {
		return games;
	}

	@JsonProperty("Games")
	public void setGames(HeliogamingGameData[] games) {
		this.games = games;
	}

	public HeliogamingBetGroupData[] getGroups() {
		return groups;
	}

	@JsonProperty("Groups")
	public void setGroups(HeliogamingBetGroupData[] groups) {
		this.groups = groups;
	}
	
}