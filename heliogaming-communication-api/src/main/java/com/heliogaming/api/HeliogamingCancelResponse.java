package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonProperty;

public class HeliogamingCancelResponse extends HeliogamingCoreResponse {
	
	private Long[] failedTransactionIDs;

	@JsonProperty("FailedTransactionIDs")
	public Long[] getFailedTransactionIDs() {
		return failedTransactionIDs;
	}

	public void setFailedTransactionIDs(Long[] failedTransactionIDs) {
		this.failedTransactionIDs = failedTransactionIDs;
	}
}
