package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class HeliogamingTransactionData{
	
	private Long participationID;
	
	private Long drawID;
	
	private String drawDate;
	
	private Double Amount;
	
	private Boolean isPrimary;
	
	@Deprecated
	private Integer gameID;

	private String gameGroupCode;
	
	public Long getParticipationID() {
		return participationID;
	}

	@JsonProperty("ParticipationID")
	public void setParticipationID(Long participationID) {
		this.participationID = participationID;
	}

	public Long getDrawID() {
		return drawID;
	}

	@JsonProperty("DrawID")
	public void setDrawID(Long drawID) {
		this.drawID = drawID;
	}

	public String getDrawDate() {
		return drawDate;
	}

	@JsonProperty("DrawDate")
	public void setDrawData(String drawDate) {
		this.drawDate = drawDate;
	}

	public Double getAmount() {
		return Amount;
	}

	@JsonProperty("Amount")
	public void setAmount(Double amount) {
		Amount = amount;
	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	@JsonProperty("IsPrimary")
	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	@Deprecated
	public Integer getGameID() {
		return gameID;
	}

	@JsonProperty("GameID")
	@Deprecated
	public void setGameID(Integer gameID) {
		this.gameID = gameID;
	}
	
	public String getGameGroupCode() {
		return this.gameGroupCode;
	}
	
	@JsonProperty("GameGroupCode")
	public void setGameGroupCode(String gameGroupCode) {
		this.gameGroupCode = gameGroupCode;
	}
	
}
