package com.heliogaming.api;

public interface ErrorCodes {

	public static final int INSUFFICIENT_FUNDS_CODE = 13000;
	public static final String INSUFFICIENT_FUNDS_DESC = "Insufficient Player Balance";
	
	public static final int PLAYER_EXCEEDED_PLAY_LIMIT_CODE = 13001;
	public static final String PLAYER_EXCEEDED_PLAY_LIMIT_DESC = "Player Exceeded Play Limit";
	
	public static final int INVALID_SESSION_CODE = 13002;
	public static final String INVALID_SESSION_DESC = "Invalid Session";
	
	public static final int PLAYER_RESTRICTED_CODE = 13003;
	public static final String PLAUER_RESTRICTED_DESC = "Player has an active restriction set";
	
	public static final int GENERIC_ERROR_CODE = 13004;
	public static final String GENERIC_ERROR_DESC = "Generic Error";
	
}
