package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class HeliogamingPlayerData {
	
	private String playerExtID;
	
	private String currencyCode;
	
	@JsonProperty("PlayerExtID")
	public String getPlayerExtID() {
		return playerExtID;
	}
	
	public void setPlayerExtID(String playerExtID) {
		this.playerExtID = playerExtID;
	}
	
	@JsonProperty("CurrencyCode")
	public String getCurrencyCode() {
		return currencyCode;
	}
	
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
}
