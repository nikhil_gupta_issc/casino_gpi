package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class HeliogamingCancelBetRequest extends HeliogamingCoreRequest {
	
	private HeliogamingWinData[] cancelledBets;

	public HeliogamingWinData[] getCancelledBets() {
		return cancelledBets;
	}

	@JsonProperty("CancelledBets")
	public void setCancelledBets(HeliogamingWinData[] cancelledBets) {
		this.cancelledBets = cancelledBets;
	}
}
