package com.heliogaming.api;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class HeliogamingSessionKeyAuthenticationData {
	
	private String sessionKey;

	public String getSessionKey() {
		return sessionKey;
	}

	@JsonProperty("SessionKey")
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	
	
}
