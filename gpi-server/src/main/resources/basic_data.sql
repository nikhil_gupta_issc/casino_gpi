INSERT INTO publisher (id, api_name, api_username, created_on, name, password, url, ortiz_casino, owner_id) VALUE (1, 'gpi', 'username', now(), 'test', 'password', 'http://localhost:8085/sim/handle.do', '', 1);
INSERT INTO publisher (id, api_name, api_username, created_on, name, password, url, ortiz_casino, owner_id) VALUE (2, 'casinoSim', 'loginName', now(), 'loginName', 'password', 'http://dev.gpi.com:8085/sim/getMessage2.do', '', 1);

INSERT INTO game_provider (id, name, login_name, password, api_name, extended_info) VALUE (1, 'gameProvider', 'username', 'password', 'gpi', 0);
INSERT INTO game_provider (id, name, login_name, password, api_name, extended_info) VALUE (2, 'rct', 'username', 'password', 'rct', 0);

INSERT INTO game (name, url, provider_id) VALUE ('flex', 'https://dev.gpi.com:8450/index.do', 1);
INSERT INTO game (name, url, provider_id) VALUE ('rctGame', 'https://dev.gpi.com:8450/index.do', 2);
INSERT INTO game (name, url, provider_id) VALUE ('alPachinko', 'https://dev.games.com:8443/pw/game.do', 1);
INSERT INTO game (name, url, provider_id) VALUE ('aztecBingo', 'https://dev.games.com:8443/ab/game.do', 1);