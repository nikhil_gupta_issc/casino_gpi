<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
	<style>
		body{
			background-color: black;
			width:100%;
			height: 100%;
			
		}
		#mainContainer{
			color: white;
		    height: 200px;
		    width: 400px;
		    position: fixed;
		    top: 50%;
		    left: 25%;
		}
		#mainContainer font{
			font-size: 3em;
		}
	</style>
</head>
<body>
	<div id="mainContainer"><font>${message}</font></div>
</body>
</html>