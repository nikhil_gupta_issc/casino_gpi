<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core'%>
<html>
	<body>
		<div id="wac-heliogaming-div"></div>
		<script type="text/javascript" src="${gameLaunchUrl}/launch-game.js"></script>
	
		<script>
		var config = {
				gameGroupCode: '${gameGroupCode}',
				apiKey: '${apiKey}',
				sessionKey: '${sessionKey}',
				languageCode: '${languageCode}',
				targetElement: 'wac-heliogaming-div',
				width: '100%',
				height: '100%',
				currencyCode: '${currencyCode}',
			};
			helioGaming.launchGame(config);
		</script>
	</body>
</html>