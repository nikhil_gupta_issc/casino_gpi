<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<html>
	<body>
		<script type="text/javascript"> 
  var _bt = _bt || [];    
  _bt.push(['server', '${server}']);
  _bt.push(['partner', '${partnerCode}']);
  _bt.push(['token', '${token}']);
  _bt.push(['language', '${lang}']);
  _bt.push(['timezone', '0']);
  _bt.push(['is_mobile', '${isMobile}']);
  
  <c:choose>
  	<c:when test="${currentGame != null}">
  		_bt.push(['current_game', '${currentGame}']);
  	</c:when>
  </c:choose>
  //_bt.push(['odds_format', '<odds_format>']);
  _bt.push(['home_url', '${homeURL}']);
  (function(){
  document.write('<'+'script type="text/javascript" src="${server}/design/client/js/betgames.js"><'+'/script>');
  })();
 </script>
 <script type="text/javascript">BetGames.frame(_bt);</script>
		
	</body>
</html>