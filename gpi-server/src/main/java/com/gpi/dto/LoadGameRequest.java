package com.gpi.dto;

public class LoadGameRequest {

    private String token;
    private String pn;
    private String serviceUrl;
    private String lang;
    private String type;
    private String serviceMessage;
    private boolean lobby;
    private String device;
    private String game;
    private Integer gid;
    private String homeUrl;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getServiceMessage() {
        return serviceMessage;
    }

    public void setServiceMessage(String serviceMessage) {
        this.serviceMessage = serviceMessage;
    }

    public boolean isLobby() {
        return lobby;
    }

    public void setLobby(boolean lobby) {
        this.lobby = lobby;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }
    
    public String getHome() {
		return homeUrl;
	}

	public void setHome(String homeUrl) {
		this.homeUrl = homeUrl;
	}

	@Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("LoadGameRequest{");
        sb.append("token='").append(token).append('\'');
        sb.append(", pn='").append(pn).append('\'');
        sb.append(", serviceUrl='").append(serviceUrl).append('\'');
        sb.append(", lang='").append(lang).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", serviceMessage='").append(serviceMessage).append('\'');
        sb.append(", lobby=").append(lobby);
        sb.append(", device='").append(device).append('\'');
        sb.append(", game='").append(game).append('\'');
        sb.append(", gid=").append(gid);
        sb.append(", homeUrl='").append(homeUrl).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
