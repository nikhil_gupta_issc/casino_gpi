package com.gpi.dto;

/**
 * Created by igabba on 15/01/15.
 */
public class RCTLoadGameDto extends LoadGameDto {

    private String loginName;
    private String key;
    private String currency;
    private Long balance;

    public RCTLoadGameDto(LoadGameDto loadGameDto) {
        this.setToken(loadGameDto.getToken());
        this.setGameUrl(loadGameDto.getGameUrl());
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getBalance() {
        return balance;
    }
}
