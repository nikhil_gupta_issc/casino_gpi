package com.gpi.dto.transaction;

import java.io.Serializable;

public class GpiTransactionIdRequest implements Serializable {

	private static final long serialVersionUID = 8134498975665637993L;
	
	private String gameTransactionId;
	
	private String game;

	public String getGameTransactionId() {
		return gameTransactionId;
	}

	public void setGameTransactionId(String gameTransactionId) {
		this.gameTransactionId = gameTransactionId;
	}

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransactionGetRequest [gameTransactionId=");
		builder.append(gameTransactionId);
		builder.append(", game=");
		builder.append(game);
		builder.append("]");
		return builder.toString();
	}
	
}
