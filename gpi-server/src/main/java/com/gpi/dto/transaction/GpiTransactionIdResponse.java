package com.gpi.dto.transaction;

import java.io.Serializable;

public class GpiTransactionIdResponse implements Serializable {

	private static final long serialVersionUID = 6397665289530111057L;
	
	private boolean success = false;
	
	private String error;
	
	private Integer errorCode;
	
	private Long gpiTransactionId;
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public GpiTransactionIdResponse(){
		super();
	}

	public Long getGpiTransactionId() {
		return gpiTransactionId;
	}

	public void setGpiTransactionId(Long gpiTransactionId) {
		this.gpiTransactionId = gpiTransactionId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GpiTransactionIdResponse [success=");
		builder.append(success);
		builder.append(", error=");
		builder.append(error);
		builder.append(", errorCode=");
		builder.append(errorCode);
		builder.append(", gpiTransactionId=");
		builder.append(gpiTransactionId);
		builder.append("]");
		return builder.toString();
	}
	
}
