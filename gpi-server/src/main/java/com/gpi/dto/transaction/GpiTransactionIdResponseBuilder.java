package com.gpi.dto.transaction;

import com.gpi.domain.audit.Transaction;
import com.gpi.exceptions.NoExistentTransactionException;

public class GpiTransactionIdResponseBuilder {

	private Transaction transaction;
	
	private NoExistentTransactionException error = new NoExistentTransactionException("transaction not found.");
	
	public GpiTransactionIdResponseBuilder setTransaction(Transaction transaction){
		this.transaction = transaction;
		return this;
	}
	
	public GpiTransactionIdResponse build(){
		GpiTransactionIdResponse response = new GpiTransactionIdResponse();
		response.setSuccess(transaction != null && transaction.isSuccess());
		response.setGpiTransactionId(transaction != null ? transaction.getId() : null);
		response.setError(transaction == null ? error.getErrorMessage() : null);
		response.setErrorCode(transaction == null ? error.getErrorCode() : null);
		return response;
	}
	
}
