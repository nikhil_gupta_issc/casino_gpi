package com.gpi.dto;

import java.io.Serializable;

/**
 * Created by Ignacio on 2/19/14.
 */
public class LoadGameDto implements Serializable {

    private String token;
    private String gameUrl;

    public LoadGameDto(String token, String gameUrl) {
        this.token = token;
        this.gameUrl = gameUrl;
    }

    public LoadGameDto() {

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getGameUrl() {
        return gameUrl;
    }

    public void setGameUrl(String gameUrl) {
        this.gameUrl = gameUrl;
    }
}
