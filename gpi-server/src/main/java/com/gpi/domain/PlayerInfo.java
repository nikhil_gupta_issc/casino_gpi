package com.gpi.domain;

import java.io.Serializable;

public class PlayerInfo implements Serializable {
	private String token;
	private String username;
	private String currency;
	private String language;
	private String gameName;
	private String usernameInPublisher;
	
	public PlayerInfo(String token, String username, String currency, String language, String gameName, String usernameInPublisher) {
		this.token = token;
		this.username = username;
		this.currency = currency;
		this.language = language;
		this.gameName = gameName;
		this.usernameInPublisher = usernameInPublisher;
	}
	
	public PlayerInfo(String token, String username, String currency, String language) {
		this(token, username, currency, language, null, null);
	}
	
	public PlayerInfo(String token, String username, String currency) {
		this(token, username, currency, null, null, null);
	}

	public PlayerInfo() {}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getUsernameInPublisher() {
		return usernameInPublisher;
	}

	public void setUsernameInPublisher(String usernameInPublisher) {
		this.usernameInPublisher = usernameInPublisher;
	}
	
	
}
