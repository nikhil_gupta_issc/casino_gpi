package com.gpi.domain;


import com.gpi.domain.game.Game;
import com.gpi.domain.game.PlayingType;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ignacio on 2/19/14.
 */


public class GameRequest implements Serializable {

    private String token;

    private String publisherToken;

    private Publisher publisher;

    private Date createdOn;

    private PlayingType playingType;

    private Player player;

    private Game game;

    private String device;

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPublisherToken() {
        return publisherToken;
    }

    public void setPublisherToken(String publisherToken) {
        this.publisherToken = publisherToken;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public PlayingType getPlayingType() {
        return playingType;
    }

    public void setPlayingType(PlayingType playingType) {
        this.playingType = playingType;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "GameRequest{" + ", token='" + token + '\''
                + ", publisherToken='" + publisherToken + '\''
                + ", publisher=" + publisher.getName()
                + ", createdOn=" + createdOn
                + ", playingType=" + playingType.getType()
                + ", player=" + player + '}';
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDevice() {
        return device;
    }
}
