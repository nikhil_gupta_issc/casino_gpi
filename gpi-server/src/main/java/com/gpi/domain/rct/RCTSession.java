package com.gpi.domain.rct;

import com.gpi.domain.game.Game;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.RCTLoadGameDto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by nacho on 21/01/15.
 */
public class RCTSession implements Serializable{

    private String loginName;
    private String key;
    private String currency;
    private Long balance;
    private Game game;
    private Publisher publisher;
    private long transactionId = 0;
    private String token;
    private Long roundId;

    public RCTSession(RCTLoadGameDto rctLoadGameDto, Game gameByName, Publisher publisher) {
        this.game = gameByName;
        this.publisher = publisher;
        this.loginName = rctLoadGameDto.getLoginName();
        this.currency = rctLoadGameDto.getCurrency();
        //TODO transform password to md5 key
        this.key = gameByName.getGameProvider().getPassword();
        this.token = rctLoadGameDto.getToken();
        this.balance = rctLoadGameDto.getBalance();
        this.roundId = new Date().getTime();
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public long getTransactionId() {
        transactionId++;
        return transactionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getRoundId() {
        return roundId;
    }
}
