package com.gpi.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.annotate.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.evoplay.api.domain.EvoplayResponse;
import com.gameart.api.domain.GameArtResponse;
import com.google.common.net.MediaType;
import com.gpi.service.gpi.EvoplayService;
import com.gpi.service.gpi.GameArtService;

@Controller
public class EvoplayIntegrationController {


	private static final Logger logger = LoggerFactory.getLogger(EvoplayIntegrationController.class);

	@Autowired
	private EvoplayService evoplayService;
	
	@ResponseBody
	@RequestMapping(value="/evoplay/handle.do", method=RequestMethod.POST, consumes= {"application/x-www-form-urlencoded"}, produces = {"application/json"})
	@JsonView
	public EvoplayResponse handle(EvoplayCallbackRequest req) {

		EvoplayResponse resp = evoplayService.processRequest(req);
		
		return resp;
	}
}
