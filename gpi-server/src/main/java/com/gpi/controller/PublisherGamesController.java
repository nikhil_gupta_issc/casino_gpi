package com.gpi.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gpi.domain.game.Game;
import com.gpi.domain.game.PlayingType;
import com.gpi.domain.game.PublisherGameMapping;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.GameImage;
import com.gpi.dto.GameImageKey;
import com.gpi.dto.GamesImages;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.publishergames.api.domain.v1.PublisherGameImageV1Response;
import com.gpi.publishergames.api.domain.v1.PublisherGameUrlParameterV1Response;
import com.gpi.publishergames.api.domain.v1.PublisherGameUrlV1Response;
import com.gpi.publishergames.api.domain.v1.PublisherGameV1Response;
import com.gpi.publishergames.api.domain.v1.PublisherGamesListV1Response;
import com.gpi.publishergames.api.domain.v1.PublisherProviderV1Response;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.game.PublisherGameService;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.ApplicationProperties;

@Controller
public class PublisherGamesController {

	private static final Logger logger = LoggerFactory.getLogger(PublisherGamesController.class);

	@Autowired
	private PublisherService publisherService;
	
	@Autowired
	private PublisherGameService publisherGameService;
	
	@Autowired 
	private GameDomainService gameDomainService;
	
	
	@ResponseBody
	@RequestMapping(value = "/v1/publisher/games/{pn}", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json")
	public ResponseEntity<PublisherGamesListV1Response> gameList(@PathVariable(value="pn") String pn) {

		logger.info("Retreving game list for: pn={}", pn);
		PublisherGamesListV1Response response = new PublisherGamesListV1Response();
		
		try {
			
			Publisher publisher = publisherService.findByName(pn);

			List<PublisherGameMapping> gameMappings = this.getPublisherMappings(publisher);

			if (gameMappings.size() > 0) {
				
				//retrieve some data from database before start looping
				String hostUrl = ApplicationProperties.getProperty("application.host.url");
				Map<Long, Game> gamesMap = this.getGamesMap();
				GamesImages gamesImages = gameDomainService.getGamesImageUrls();
				
				//break control loop - first element
				PublisherGameMapping prevGameMapping = gameMappings.get(0);
				PublisherProviderV1Response publisherProvider = this.buildPublisherProvider(prevGameMapping);
				
				for (PublisherGameMapping gameMapping : gameMappings){
					if (!prevGameMapping.getGame().getGameProvider().getName().equals(gameMapping.getGame().getGameProvider().getName())){
						response.getPublisherProviders().add(publisherProvider);
						publisherProvider = this.buildPublisherProvider(gameMapping);
					}
					
					Game game = this.getGame(gameMapping.getGame().getId(), gamesMap);
					GameImage gameImage = gamesImages.get(new GameImageKey(publisher.getId(), game.getId()));
					
					publisherProvider.getGames().add(this.buildPublisherGame(publisher, game, gameMapping, gameImage, hostUrl));
					
					prevGameMapping = gameMapping;
				}
				response.getPublisherProviders().add(publisherProvider);
			}
			
			logger.info(gameMappings.size()+ " games retrieved for: pn={}", pn);
			return new ResponseEntity<PublisherGamesListV1Response>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<PublisherGamesListV1Response>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private PublisherGameV1Response buildPublisherGame(Publisher publisher, Game game, PublisherGameMapping gameMapping,GameImage gameImage, String hostUrl) {
		//Build publisher game
		PublisherGameV1Response publisherGameResponse = new PublisherGameV1Response();
		publisherGameResponse.setName(game.getName());
		publisherGameResponse.setDescription(game.getDescription());
		publisherGameResponse.setFree(gameMapping.isFree());
		publisherGameResponse.setCharged(gameMapping.isCharged());
		publisherGameResponse.setProvider(gameMapping.getGame().getGameProvider().getName());
		if (gameImage != null) {
			publisherGameResponse.setImage(new PublisherGameImageV1Response(gameImage.getUrl()));						
		}
		publisherGameResponse.setUrl(buildGameUrl(publisher, game, hostUrl));
		return publisherGameResponse;
	}

	//Prevent caching problems
	private Game getGame(Long gameId, Map<Long, Game> gamesMap) throws NonExistentGameException {
		if (!gamesMap.containsKey(gameId))
			throw new NonExistentGameException("game " + gameId + " do not exist in db but it does in cache.");
		return gamesMap.get(gameId);
	}

	//Prevent game caching problems inside publisher games cache
	private Map<Long, Game> getGamesMap() {
		Map<Long, Game> gamesMap = new HashMap<Long, Game>();
		List<Game> gamesList = gameDomainService.getAvailableGames();
		for (int i=0;i<=gamesList.size()-1;i++) gamesMap.put(gamesList.get(i).getId(), gamesList.get(i));
		return gamesMap;
	}
	
	private List<PublisherGameMapping> getPublisherMappings(Publisher publisher){
		List<PublisherGameMapping> list = publisherGameService.getPublisherGameMappings(publisher);
		Collections.sort(list, new Comparator<PublisherGameMapping>(){
			@Override
			public int compare(PublisherGameMapping o1, PublisherGameMapping o2) {
				int compare = o1.getGame().getGameProvider().getName().compareToIgnoreCase(o2.getGame().getGameProvider().getName());
				return compare != 0 ? compare : o1.getGame().getName().compareToIgnoreCase(o2.getGame().getName());
			}
		});
		return list;
	}
	
	private PublisherProviderV1Response buildPublisherProvider(PublisherGameMapping gameMapping) {
		PublisherProviderV1Response publisherProvider = new PublisherProviderV1Response();
		publisherProvider.setName(gameMapping.getGame().getGameProvider().getName());
		return publisherProvider;
	}

	private PublisherGameUrlV1Response buildGameUrl(Publisher publisher, Game game, String hostUrl) {
		
		PublisherGameUrlV1Response publisherGameUrl = new PublisherGameUrlV1Response();
		publisherGameUrl.setHost(hostUrl);
		publisherGameUrl.setPath("game.do");
		publisherGameUrl.getParameters().add(new PublisherGameUrlParameterV1Response("token", "{token}", true));
		publisherGameUrl.getParameters().add(new PublisherGameUrlParameterV1Response("pn", publisher.getName(), true));
		publisherGameUrl.getParameters().add(new PublisherGameUrlParameterV1Response("game", game.getName(), true));
		publisherGameUrl.getParameters().add(new PublisherGameUrlParameterV1Response("type", "{type}", true, Arrays.asList(PlayingType.FREE.getType(), PlayingType.CHARGED.getType())));
		publisherGameUrl.getParameters().add(new PublisherGameUrlParameterV1Response("lang", "{lang}", true));
		publisherGameUrl.getParameters().add(new PublisherGameUrlParameterV1Response("device", "{device}", false, Arrays.asList("desktop", "mobile")));
		publisherGameUrl.getParameters().add(new PublisherGameUrlParameterV1Response("lobby", "{lobby}", false, Arrays.asList("true", "false")));
		publisherGameUrl.getParameters().add(new PublisherGameUrlParameterV1Response("homeUrl", "{homeUrl}", false));
		
		return publisherGameUrl;
	}
	
}
