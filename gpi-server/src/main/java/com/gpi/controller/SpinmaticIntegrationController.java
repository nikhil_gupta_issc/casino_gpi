package com.gpi.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.annotate.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.evoplay.api.domain.EvoplayResponse;
import com.gameart.api.domain.GameArtResponse;
import com.google.common.net.MediaType;
import com.gpi.service.gpi.EvoplayService;
import com.gpi.service.gpi.GameArtService;
import com.gpi.service.gpi.SpinmaticService;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponse;

@Controller
public class SpinmaticIntegrationController {


	private static final Logger logger = LoggerFactory.getLogger(SpinmaticIntegrationController.class);

	@Autowired
	private SpinmaticService spinmaticService;
	
	@ResponseBody
	@RequestMapping(value="/spinmatic/handle.do", method=RequestMethod.POST, consumes= {"application/json"}, produces = {"application/json"})
	@JsonView
	public SpinmaticResponse handle(@RequestBody SpinmaticRequest req) {

		SpinmaticResponse resp = spinmaticService.processRequest(req);
		
		return resp;
	}
}
