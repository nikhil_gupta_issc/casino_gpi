package com.gpi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bet.games.api.Root;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.MarshalException;
import com.gpi.service.gpi.BetGamesService;
import com.gpi.utils.BetGamesMessageHelper;
import com.gpi.utils.MessageHelper;

@Controller
public class BetGamesIntegrationController {

	private static final Logger logger = LoggerFactory.getLogger(BetGamesIntegrationController.class);

	@Autowired
	private BetGamesService betGamesService;
	
	@ResponseBody
	@RequestMapping(value = "/betgames/handle.do", method = {RequestMethod.POST}, consumes = "application/xml; charset=utf-8")
	public String handle(@RequestBody String xml) {

		logger.info("BetGames: Message received={}", xml);

		String response = null;

		try {
			Root messageFromGame = BetGamesMessageHelper.createMessageFromString(xml);

			if (messageFromGame.getMethod() == null) {
				throw new MarshalException("Invalid PKT. Method element expected");
			}
			
			Root responseMsg = betGamesService.processMessage(messageFromGame);
			
			response = BetGamesMessageHelper.createStringFromMessage(responseMsg);

		} catch (MarshalException e) {
			logger.error("Error marshalling object messageFromGame=" + xml, e);
			try {
				response = MessageHelper.createStringFromMessage(MessageHelper.createErrorResponse(new GamePlatformIntegrationException(e), null));
			} catch (Exception e2){
				response = "Error 0";
			}
		} catch (GameRequestNotExistentException e) {
			logger.error("Game doesnt exists", e);
		} catch (Exception e) {
			logger.error("Exception", e);
		}

		logger.info("BetGames: Message response={}", response);
		return response;
	}

}
