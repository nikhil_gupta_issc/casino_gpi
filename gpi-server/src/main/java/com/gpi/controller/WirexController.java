package com.gpi.controller;

import java.util.Map;

import org.codehaus.jackson.map.annotate.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gpi.service.gpi.PragmaticPlayService;
import com.gpi.service.gpi.WirexService;
import com.pragmaticplay.api.PragmaticPlayMethod;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

import it.wirex.api.WirexMethod;
import it.wirex.api.domain.WirexResponse;

@Controller
public class WirexController {
	@Autowired
	private WirexService wirexService;

	@JsonView
	@ResponseBody
	@RequestMapping(value = "/wirex/sessions/authenticate", method = { RequestMethod.POST })
	public WirexResponse authenticate(@RequestParam Map<String, String> params) {
		return wirexService.processRequest(null, params, WirexMethod.AUTHENTICATE);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/wirex/sessions/{sessionId}", method = { RequestMethod.GET })
	public WirexResponse session(@PathVariable(value="sessionId") String sessionId, @RequestParam Map<String, String> params) {
		return wirexService.processRequest(sessionId, params, WirexMethod.SESSION);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/wirex/sessions/{sessionId}/wallet", method = { RequestMethod.GET })
	public WirexResponse wallet(@PathVariable(value="sessionId") String sessionId, @RequestParam Map<String, String> params) {
		return wirexService.processRequest(sessionId, params, WirexMethod.WALLET);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/wirex/sessions/{sessionId}/wallet/withdraw", method = { RequestMethod.POST })
	public WirexResponse withdraw(@PathVariable(value="sessionId") String sessionId, @RequestParam Map<String, String> params) {
		return wirexService.processRequest(sessionId, params, WirexMethod.WITHDRAW);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/wirex/sessions/{sessionId}/wallet/deposit", method = { RequestMethod.POST })
	public WirexResponse deposit(@PathVariable(value="sessionId") String sessionId, @RequestParam Map<String, String> params) {
		return wirexService.processRequest(sessionId, params, WirexMethod.DEPOSIT);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/wirex/sessions/{sessionId}/wallet/cancel", method = { RequestMethod.POST })
	public WirexResponse cancel(@PathVariable(value="sessionId") String sessionId, @RequestParam Map<String, String> params) {
		return wirexService.processRequest(sessionId, params, WirexMethod.CANCEL);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/wirex/sessions/{sessionId}/wallet/rollback", method = { RequestMethod.POST })
	public WirexResponse rollback(@PathVariable(value="sessionId") String sessionId, @RequestParam Map<String, String> params) {
		return wirexService.processRequest(sessionId, params, WirexMethod.ROLLBACK);
	}
}
