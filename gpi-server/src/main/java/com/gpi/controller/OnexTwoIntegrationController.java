package com.gpi.controller;


import org.codehaus.jackson.map.annotate.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gpi.service.gpi.OnexTwoService;
import com.onextwo.api.OnexTwoMethod;
import com.onextwo.api.domain.OnexTwoQueryBetRequest;
import com.onextwo.api.domain.OnexTwoRequest;
import com.onextwo.api.domain.OnexTwoResponse;
import com.onextwo.api.domain.OnexTwoTransactionRequest;

@Controller
public class OnexTwoIntegrationController {
	@Autowired
	private OnexTwoService onexTwoService;
	
	private static final Logger logger = LoggerFactory.getLogger(OnexTwoIntegrationController.class);

	@JsonView
	@ResponseBody
	@RequestMapping(value = "/1x2/GetAccountDetails", method = { RequestMethod.POST })
	public OnexTwoResponse getAccountDetails(@RequestBody OnexTwoRequest request) {
		logger.info("GetAccountDetails request " + request);
		return onexTwoService.processRequest(request, OnexTwoMethod.GET_ACCOUNT_DETAILS);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/1x2/PlaceVirtualBet", method = { RequestMethod.POST })
	public OnexTwoResponse placeVirtualBet(@RequestBody OnexTwoTransactionRequest request) {
		logger.info("PlaceVirtualBet request " + request);
		return onexTwoService.processRequest(request, OnexTwoMethod.PLACE_VIRTUAL_BET);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/1x2/SettleVirtualBet", method = { RequestMethod.POST })
	public OnexTwoResponse settleVirtualBet(@RequestBody OnexTwoTransactionRequest request) {
		logger.debug("SettleVirtualBet request " + request);
		return onexTwoService.processRequest(request, OnexTwoMethod.SETTLE_VIRTUAL_BET);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/1x2/CancelBet", method = { RequestMethod.POST })
	public OnexTwoResponse cancelBet(@RequestBody OnexTwoTransactionRequest request) {
		logger.debug("CancelBet request " + request);
		return onexTwoService.processRequest(request, OnexTwoMethod.CANCEL_BET);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/1x2/QueryBet", method = { RequestMethod.POST })
	public OnexTwoResponse queryBet(@RequestBody OnexTwoQueryBetRequest request) {
		logger.info("QueryBet request " + request);
		return onexTwoService.processRequest(request, OnexTwoMethod.QUERY_BET);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/1x2/GetBalance", method = { RequestMethod.POST })
	public OnexTwoResponse getBalance(@RequestBody OnexTwoRequest request) {
		logger.info("GetBalance request " + request);
		return onexTwoService.processRequest(request, OnexTwoMethod.GET_BALANCE);
	}
}
