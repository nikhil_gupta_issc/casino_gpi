package com.gpi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.booming.api.BoomingErrorCodes;
import com.booming.api.BoomingMethodType;
import com.booming.domain.AbstractBoomingResponse;
import com.booming.domain.BoomingErrorResponse;
import com.booming.domain.BoomingRequest;
import com.booming.domain.BoomingRequestHeader;
import com.gpi.service.gpi.BoomingService;
import com.gpi.utils.ApplicationProperties;

@Controller

public class BoomingIntegrationController {

	private static final Logger logger = LoggerFactory.getLogger(BoomingIntegrationController.class);

	@Autowired
	private BoomingService boomingService;
	
	@ResponseBody
	@RequestMapping(value = "/booming/betwin.do", method = {RequestMethod.POST})
	public AbstractBoomingResponse betwin(@RequestHeader("Bg-Signature")String signature, @RequestHeader("Bg-Nonce")String nonce, @RequestBody String requestBody) {

		BoomingRequestHeader header = new BoomingRequestHeader(signature, nonce, ApplicationProperties.getProperty("booming.api.callback"));
		BoomingRequest request = BoomingRequest.create(requestBody, BoomingRequest.class);
		
		logger.info("Booming: Message (Spin) signature={}, nonce={}, request={}", header.getSignature(), header.getNonce(), request.getRawJson());

		AbstractBoomingResponse response = null;
		try {
			
			response = boomingService.processRequest(header, request, BoomingMethodType.BETWIN);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new BoomingErrorResponse(BoomingErrorCodes.GENERIC_ERROR_CODE, e.getMessage());
		}

		logger.info("Booming: Message (Spin) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/booming/rollback.do", method = {RequestMethod.POST})
	public AbstractBoomingResponse rollback(@RequestHeader("Bg-Signature")String signature, @RequestHeader("Bg-Nonce")String nonce, @RequestBody String requestBody) {

		BoomingRequestHeader header = new BoomingRequestHeader(signature, nonce, ApplicationProperties.getProperty("booming.api.rollback_callback"));
		BoomingRequest request = BoomingRequest.create(requestBody, BoomingRequest.class);
		
		logger.info("Booming: Message (Rollback) signature={}, nonce={}, request={}", header.getSignature(), header.getNonce(), request.getRawJson());

		AbstractBoomingResponse response = null;
		try {
			
			response = boomingService.processRequest(header, request, BoomingMethodType.ROLLBACK);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new BoomingErrorResponse(BoomingErrorCodes.GENERIC_ERROR_CODE, e.getMessage());
		}

		logger.info("Booming: Message (Rollback) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/booming/notenoughcredit.do", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView notenoughcredit() {
		ModelAndView mav = new ModelAndView("/message");
		mav.addObject("message", "Not enough credits");
		return mav;
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public void handleBadRequest(HttpMessageNotReadableException e) {
	    logger.error("Returning HTTP 400 Bad Request", e);
	    throw e;
	}

}
