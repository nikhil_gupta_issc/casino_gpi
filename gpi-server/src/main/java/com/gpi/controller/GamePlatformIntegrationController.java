package com.gpi.controller;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.api.Message;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.PlayingType;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.GameUrlNotDefinedException;
import com.gpi.exceptions.MarshalException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.gameprovider.OpenGameUrl;
import com.gpi.service.gameprovider.OpenGameUrlService;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.provider.ProviderService;
import com.gpi.utils.DeviceUtils;
import com.gpi.utils.MessageHelper;

/**
 * Created by Nacho on 2/18/14.
 */
@Controller
public class GamePlatformIntegrationController {

    private static final Logger logger = LoggerFactory.getLogger(GamePlatformIntegrationController.class);

    @Autowired
    private ProviderService providerService;

    @Qualifier("gamePlatformIntegrationService")
    @Autowired
    private GamePlatformIntegrationService gamePlatformIntegrationService;

    @Autowired
    private GameDomainService gameDomainService;
    
    @Autowired
    private OpenGameUrlService openGameUrlService;
    
    
    @ResponseBody
    @RequestMapping(value = "/handle.do", method = {RequestMethod.POST}, consumes = "text/xml; charset=utf-8")
    public String handle(@RequestBody String xml) {
    	long startTime = System.currentTimeMillis();
        logger.info("Message received={}", xml);
        
        String response;
		try {
			Message messageFromGame = MessageHelper.createMessageFromString(xml);
			if (messageFromGame.getMethod() == null) {
			    throw new MarshalException("Invalid PKT. Method element expected");
			}
			
			logger.info("Looking for gameRequest for messageFromGame={}", messageFromGame);
			response =  MessageHelper.createStringFromMessage(gamePlatformIntegrationService.processMessage(messageFromGame));
		} catch (MarshalException e) {
            logger.error("Error marshalling object messageFromGame=" + xml, e);
            try {
            	response = MessageHelper.createStringFromMessage(MessageHelper.createErrorResponse(new GamePlatformIntegrationException(e), null));
            } catch (Exception e2){
            	response = "Error 0";
            }
        } finally {
            long stopTime = System.currentTimeMillis();
            logger.info("/handle.do - Communication took " + (stopTime - startTime) + " ms");
        }
		
        logger.info("Sending xml to game: {}", response);
        
        return response;
    }

    @RequestMapping(value = "/game.do")
    public ModelAndView index(
            HttpServletRequest request, @ModelAttribute("providerRequest") LoadGameRequest gameRequest) throws URISyntaxException, MalformedURLException, InvalidKeyException, NoSuchAlgorithmException {
    	long startTime = System.currentTimeMillis();
        logger.info("Requesting for a new game with values={}", gameRequest);
        ModelAndView mav;
        try {
            if (gameRequest.getDevice() == null) {
                gameRequest.setDevice(DeviceUtils.resolveDevice(request));
            }
            Game game = getGame(gameRequest);
            
            int gameRequestTTL = "betgames".equals(game.getGameProvider().getName()) ? CacheConstants.BETGAMES_TOKEN_TTL : CacheConstants.GPI_GAME_REQUEST_TTL;
            
            LoadGameDto loadGameDto = providerService.loadGame(gameRequest.getToken(), gameRequest.getPn(), game,
                    (gameRequest.getType()== null) ? PlayingType.CHARGED.getType() : gameRequest.getType(), gameRequest.isLobby(),
                    gameRequest.getDevice(), gameRequestTTL);
            String url = replaceUrlParameters(loadGameDto.getGameUrl(), gameRequest.getLang());
            UriComponentsBuilder builder = null;
            if (StringUtils.isNotEmpty(url)) {
	            builder = fromHttpUrl(url);
	            providerService.setProviderSpecificParams(game, request.getParameterMap(), loadGameDto, builder);
	            //add all particular parameters but first remove ours
	            Map parameterMap = new HashMap(request.getParameterMap());
	            addOtherParams(builder, parameterMap);
            }
            logger.info("GameProvider={}", game.getGameProvider().getName());
            
            OpenGameUrl customUrl = openGameUrlService.getImplementation(game.getGameProvider().getName());
            
            if (customUrl != null) {
            	ModelAndView customMav = customUrl.processURL(builder, request, loadGameDto, gameRequest, game, url);
            	if (customMav != null) {
            		return customMav;
            	}
            }
            
            String urlBuild = builder.build().toString();
            logger.info("Url created: {}", urlBuild);
            mav = new ModelAndView("redirect:" + urlBuild);
        } catch (NonExistentPublisherException | GameUrlNotDefinedException | NonExistentGameException | GameRequestNotExistentException e) {
            logger.error("Error: ", e);
            mav = new ModelAndView("error");
            mav.addObject("error", e.getMessage());
        } finally {
            long stopTime = System.currentTimeMillis();
            logger.info("/game.do - Communication took " + (stopTime - startTime) + " ms");
        }
        return mav;
    }
    
    private Game getGame(LoadGameRequest gameRequest) throws NonExistentGameException {
        if(gameRequest.getGid()!= null){
            return gameDomainService.getGameById(Long.valueOf(gameRequest.getGid()));
        }else {
            return gameDomainService.getGameByName(gameRequest.getGame());
        }
    }

    private void setProviderSpecificParams(String serviceUrl, String lang, String type, String serviceMessage, LoadGameDto loadGameDto, UriComponentsBuilder builder) {

    }

    private void addOtherParams(UriComponentsBuilder builder, Map<String, String[]> parameterMap) {
        removeParameters(parameterMap);
        for (String key : parameterMap.keySet()) {
            builder.queryParam(key, parameterMap.get(key)[0]);
        }
    }

    private void removeParameters(Map parameterMap) {
        parameterMap.remove("token");
        parameterMap.remove("type");
        parameterMap.remove("pn");
        parameterMap.remove("lang");
        parameterMap.remove("game");
        parameterMap.remove("serviceUrl");
        parameterMap.remove("serviceMessage");

    }

    private String replaceUrlParameters(String gameUrl, String lang) {
        Locale locale;
        if (lang == null) {
            locale = new Locale("pt", "BR");
        } else {
            locale = new Locale(lang);
        }
        return StringUtils.replace(gameUrl, "${lang}", locale.toString());
    }
    
    
	
	
}
