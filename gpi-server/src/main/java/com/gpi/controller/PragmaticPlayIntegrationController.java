package com.gpi.controller;

import java.util.Map;

import org.codehaus.jackson.map.annotate.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gpi.service.gpi.PragmaticPlayService;
import com.pragmaticplay.api.PragmaticPlayMethod;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

@Controller
public class PragmaticPlayIntegrationController {
	@Autowired
	private PragmaticPlayService pragmaticPlayService;

	@JsonView
	@ResponseBody
	@RequestMapping(value = "/pragmaticplay/authenticate.do", method = { RequestMethod.POST })
	public PragmaticPlayResponse authenticate(@RequestParam Map<String, String> params) {
		return pragmaticPlayService.processRequest(params, PragmaticPlayMethod.AUTHENTICATE);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/pragmaticplay/balance.do", method = { RequestMethod.POST })
	public PragmaticPlayResponse balance(@RequestParam Map<String, String> params) {
		return pragmaticPlayService.processRequest(params, PragmaticPlayMethod.BALANCE);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/pragmaticplay/bet.do", method = { RequestMethod.POST })
	public PragmaticPlayResponse bet(@RequestParam Map<String, String> params) {
		return pragmaticPlayService.processRequest(params, PragmaticPlayMethod.BET);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/pragmaticplay/result.do", method = { RequestMethod.POST })
	public PragmaticPlayResponse result(@RequestParam Map<String, String> params) {
		return pragmaticPlayService.processRequest(params, PragmaticPlayMethod.RESULT);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/pragmaticplay/bonusWin.do", method = { RequestMethod.POST })
	public PragmaticPlayResponse bonusWin(@RequestParam Map<String, String> params) {
		return pragmaticPlayService.processRequest(params, PragmaticPlayMethod.BONUS_WIN);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/pragmaticplay/jackpotWin.do", method = { RequestMethod.POST })
	public PragmaticPlayResponse jackpotWin(@RequestParam Map<String, String> params) {
		return pragmaticPlayService.processRequest(params, PragmaticPlayMethod.JACKPOT_WIN);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/pragmaticplay/refund.do", method = { RequestMethod.POST })
	public PragmaticPlayResponse refund(@RequestParam Map<String, String> params) {
		return pragmaticPlayService.processRequest(params, PragmaticPlayMethod.REFUND);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/pragmaticplay/endround.do", method = { RequestMethod.POST })
	public PragmaticPlayResponse endRound(@RequestParam Map<String, String> params) {
		return pragmaticPlayService.processRequest(params, PragmaticPlayMethod.END_ROUND);
	}
}
