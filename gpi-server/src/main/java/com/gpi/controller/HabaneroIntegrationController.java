package com.gpi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.HabaneroService;
import com.habanero.api.HabaneroMethodType;
import com.habanero.domain.HabaneroAuthRequest;
import com.habanero.domain.HabaneroAuthResponse;
import com.habanero.domain.HabaneroFundTransferResponse;
import com.habanero.domain.HabaneroPlayerDetailResponse;
import com.habanero.domain.HabaneroQueryRequest;
import com.habanero.domain.HabaneroQueryResponse;
import com.habanero.domain.HabaneroResponse;
import com.habanero.domain.HabaneroStatus;
import com.habanero.domain.HabaneroTransactionRequest;
import com.habanero.domain.HabaneroTransactionResponse;

@Controller

public class HabaneroIntegrationController {

	private static final Logger logger = LoggerFactory.getLogger(HabaneroIntegrationController.class);

	@Autowired
	private HabaneroService habaneroService;
	
	@ResponseBody
	@RequestMapping(value = "/habanero/api/auth", method = {RequestMethod.POST})
	public HabaneroResponse auth(@RequestBody HabaneroAuthRequest request) {

		logger.info("Habanero: Message (Auth) request={}", request.toJson());

		HabaneroResponse response = null;
		try {
			
			response = habaneroService.processRequest(request, HabaneroMethodType.AUTH);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new HabaneroAuthResponse(new HabaneroPlayerDetailResponse(new HabaneroStatus(false, true)));
		}

		logger.info("Habanero: Message (Auth) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/habanero/api/tx", method = {RequestMethod.POST})
	public HabaneroResponse tx(@RequestBody HabaneroTransactionRequest request) {

		logger.info("Habanero: Message (Tx) request={}", request.toJson());

		HabaneroResponse response = null;
		try {
			
			response = habaneroService.processRequest(request, HabaneroMethodType.TRANSACTION);
		} catch(GameRequestNotExistentException e) {
			logger.error(e.getMessage(), e);
			return new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus(false, true)));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus(false, e.getMessage())));
		}

		logger.info("Habanero: Message (Tx) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/habanero/api/query", method = {RequestMethod.POST})
	public HabaneroResponse query(@RequestBody HabaneroQueryRequest request) {

		logger.info("Habanero: Message (Query) request={}", request.toJson());

		HabaneroResponse response = null;
		try {
			
			response = habaneroService.processRequest(request, HabaneroMethodType.QUERY);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new HabaneroQueryResponse(); // Don't send {success: false} because habanero handles on debit check as it was never done 
		}

		logger.info("Habanero: Message (Query) response={}", response.toJson());
		return response;
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public void handleBadRequest(HttpMessageNotReadableException e) {
	    logger.error("Returning HTTP 400 Bad Request", e);
	    throw e;
	}

}
