package com.gpi.controller;

import com.gpi.exceptions.MarshalException;
import com.gpi.service.gpi.impl.RCTServiceImpl;
import com.gpi.utils.RCTMessageHelper;
import com.rct.api.domain.GetBalance;
import com.rct.api.domain.OpenSession;
import com.rct.api.domain.PayBet;
import com.rct.api.domain.PayWin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by igabba on 16/01/15.
 */
@Controller
public class RCTIntegrationController {

    private static final Logger logger = LoggerFactory.getLogger(RCTIntegrationController.class);

    @Autowired
    private RCTServiceImpl rctService;

    @ResponseBody
    @RequestMapping(value = "/rct/confirmOpenSession.do", method = {RequestMethod.GET})
    public String confirmOpenSession(@RequestParam(value = "session", required = true) String token,
                         @RequestParam(value = "game", required = true) Long gameId,
                         @RequestParam(value = "key", required = true) String key) throws MarshalException {

        logger.info("confirm open session for game={} and key={}", gameId, key);
        String stringFromMessage = null;
        OpenSession openSession;
        try {
            openSession = rctService.confirmOpenSession(token, gameId, key);
        } catch (Exception e) {
            logger.warn("Error while confirmSession", e);
            openSession = new OpenSession();
            openSession.setError("0");
        }
        stringFromMessage = RCTMessageHelper.createStringFromObject(openSession);
        logger.info("Sending xml to game: {}", stringFromMessage);
        return stringFromMessage;
    }

    @ResponseBody
    @RequestMapping(value = "/rct/closeSession.do", method = {RequestMethod.GET})
    public String closeSession(@RequestParam(value = "session", required = true) String token,
                         @RequestParam(value = "key", required = true) String key) {

        return "";
    }

    @ResponseBody
    @RequestMapping(value = "/rct/getBalance.do", method = {RequestMethod.GET})
    public String getBalance(@RequestParam(value = "session", required = true) String token,
                             @RequestParam(value = "key", required = true) String key,
                             @RequestParam(value = "userName", required = true) String userName) {

        logger.info("getBalance called for token={} and key={}", token, key);
        GetBalance getBalance = rctService.getBalance(token);
        String response = null;
        try {
            response = RCTMessageHelper.createStringFromObject(getBalance);
        } catch (MarshalException e) {
            logger.error("error:", e);
        }
        logger.info("Sending xml to game: {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/rct/payBet.do", method = {RequestMethod.GET})
    public String payBet(
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "amount", required = true) Long amount,
            @RequestParam(value = "transactionId", required = true) Long transactionId,
            @RequestParam(value = "key", required = true) String key) throws MarshalException {

        logger.info("payBet called for token={}, amount={}, transactionId={} and key={}", token,amount, transactionId, key);
        String response = null;
        PayBet payBet = null;
        try {
            payBet = rctService.payBet(token, amount, transactionId, key);
        } catch (Exception e) {
            logger.error("Error", e);
            payBet = new PayBet();
            payBet.setError("0");
        }
        response = RCTMessageHelper.createStringFromObject(payBet);
        logger.info("Sending xml to game: {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/payBetConfirmation.do", method = {RequestMethod.GET})
    public String payBetConfirmation(@RequestParam(value = "session", required = true) String token,
                               @RequestParam(value = "key", required = true) String key) {

        return "";
    }

    @ResponseBody
    @RequestMapping(value = "/rct/payWin.do", method = {RequestMethod.GET})
    public String payWin(@RequestParam(value = "token", required = true) String token,
                         @RequestParam(value = "amount", required = true) Long amount,
                         @RequestParam(value = "transactionId", required = true) Long transactionId,
                         @RequestParam(value = "key", required = true) String key) throws MarshalException {

        logger.info("payWin called for token={}, key={}, amount={} and transactionId={}", token, key, amount, transactionId);
        String stringFromObject = null;
        PayWin response = new PayWin();
        try {
            response = rctService.payWin(token, amount, transactionId, key);
        } catch (Exception e) {
            logger.error("Error", e);
            response.setError("0");
        }
        stringFromObject = RCTMessageHelper.createStringFromObject(response);
        logger.info("Sending xml to game: {}", stringFromObject);
        return stringFromObject;
    }

    @ResponseBody
    @RequestMapping(value = "/payBetRecovery.do", method = {RequestMethod.GET})
    public String payBetRecovery(@RequestParam(value = "session", required = true) String token,
                               @RequestParam(value = "key", required = true) String key) {


        return "";
    }


}
