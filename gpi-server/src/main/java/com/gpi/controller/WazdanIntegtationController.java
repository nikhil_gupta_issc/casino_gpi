package com.gpi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.gpi.service.gpi.WazdanService;
import com.wazdan.api.WazdanErrorCodes;
import com.wazdan.api.WazdanMethodType;
import com.wazdan.domain.WazdanAuthenticateRequest;
import com.wazdan.domain.WazdanErrorResponse;
import com.wazdan.domain.WazdanFrResultRequest;
import com.wazdan.domain.WazdanGameCloseRequest;
import com.wazdan.domain.WazdanGetFundsRequest;
import com.wazdan.domain.WazdanGetStakeRequest;
import com.wazdan.domain.WazdanRcCloseRequest;
import com.wazdan.domain.WazdanRcContinueRequest;
import com.wazdan.domain.WazdanRequestHeader;
import com.wazdan.domain.WazdanResponse;
import com.wazdan.domain.WazdanReturnWinRequest;
import com.wazdan.domain.WazdanRollbackStakeRequest;

@Controller
public class WazdanIntegtationController {

	private static final Logger logger = LoggerFactory.getLogger(WazdanIntegtationController.class);

	
	@Autowired
	private WazdanService wazdanService;
	
	@ResponseBody
	@RequestMapping(value = "/wazdan/authenticate", method = {RequestMethod.POST})
	public WazdanResponse auth(@RequestHeader("Authorization")String authorization,  @RequestBody WazdanAuthenticateRequest request) {
		
		WazdanRequestHeader requestHeader = new WazdanRequestHeader(authorization);
		
		logger.info("Wazdan: Message (authenticate) request={}", request.toJson());
		
		WazdanResponse response = null;
		try {
			
			response = wazdanService.processRequest(requestHeader, request, WazdanMethodType.AUTHENTICATE);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new WazdanErrorResponse(WazdanErrorCodes.STATUS_CODE_INTERNAL_ERROR, WazdanErrorCodes.STATUS_MSG_INTERNAL_ERROR);
		}

		logger.info("Wazdan: Message (authenticate) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/wazdan/getStake", method = {RequestMethod.POST})
	public WazdanResponse getstake(@RequestHeader("Authorization")String authorization,  @RequestBody WazdanGetStakeRequest request) {
		
		WazdanRequestHeader requestHeader = new WazdanRequestHeader(authorization);
		
		logger.info("Wazdan: Message (getStake) request={}", request.toJson());
		
		WazdanResponse response = null;
		try {
			
			response = wazdanService.processRequest(requestHeader, request, WazdanMethodType.GETSTAKE);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new WazdanErrorResponse(WazdanErrorCodes.STATUS_CODE_INTERNAL_ERROR, WazdanErrorCodes.STATUS_MSG_INTERNAL_ERROR);
		}

		logger.info("Wazdan: Message (getStake) response={}", response.toJson());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/wazdan/rollbackStake", method = {RequestMethod.POST})
	public WazdanResponse rollback(@RequestHeader("Authorization")String authorization,  @RequestBody WazdanRollbackStakeRequest request) {
		
		WazdanRequestHeader requestHeader = new WazdanRequestHeader(authorization);
		
		logger.info("Wazdan: Message (rollbackStake) request={}", request.toJson());
		
		WazdanResponse response = null;
		try {
			
			response = wazdanService.processRequest(requestHeader, request, WazdanMethodType.ROLLBACKSTAKE);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new WazdanErrorResponse(WazdanErrorCodes.STATUS_CODE_INTERNAL_ERROR, WazdanErrorCodes.STATUS_MSG_INTERNAL_ERROR);
		}

		logger.info("Wazdan: Message (rollbackStake) response={}", response.toJson());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/wazdan/returnWin", method = {RequestMethod.POST})
	public WazdanResponse returnwin(@RequestHeader("Authorization")String authorization,  @RequestBody WazdanReturnWinRequest request) {
		
		WazdanRequestHeader requestHeader = new WazdanRequestHeader(authorization);
		
		logger.info("Wazdan: Message (returnWin) request={}", request.toJson());
		
		WazdanResponse response = null;
		try {
			
			response = wazdanService.processRequest(requestHeader, request, WazdanMethodType.RETURNWIN);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new WazdanErrorResponse(WazdanErrorCodes.STATUS_CODE_INTERNAL_ERROR, WazdanErrorCodes.STATUS_MSG_INTERNAL_ERROR);
		}

		logger.info("Wazdan: Message (returnWin) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/wazdan/gameClose", method = {RequestMethod.POST})
	public WazdanResponse gameClose(@RequestHeader("Authorization")String authorization,  @RequestBody WazdanGameCloseRequest request) {
		
		WazdanRequestHeader requestHeader = new WazdanRequestHeader(authorization);
		
		logger.info("Wazdan: Message (gameClose) request={}", request.toJson());
		
		WazdanResponse response = null;
		try {
			
			response = wazdanService.processRequest(requestHeader, request, WazdanMethodType.GAMECLOSE);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new WazdanErrorResponse(WazdanErrorCodes.STATUS_CODE_INTERNAL_ERROR, WazdanErrorCodes.STATUS_MSG_INTERNAL_ERROR);
		}

		logger.info("Wazdan: Message (gameClose) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/wazdan/frResult", method = {RequestMethod.POST})
	public WazdanResponse frResult(@RequestHeader("Authorization")String authorization,  @RequestBody WazdanFrResultRequest request) {
		
		WazdanRequestHeader requestHeader = new WazdanRequestHeader(authorization);
		
		logger.info("Wazdan: Message (frResult) request={}", request.toJson());
		
		WazdanResponse response = null;
		try {
			
			response = wazdanService.processRequest(requestHeader, request, WazdanMethodType.FREERESULT);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new WazdanErrorResponse(WazdanErrorCodes.STATUS_CODE_INTERNAL_ERROR, WazdanErrorCodes.STATUS_MSG_INTERNAL_ERROR);
		}

		logger.info("Wazdan: Message (frResult) response={}", response.toJson());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/wazdan/getFunds", method = {RequestMethod.POST})
	public WazdanResponse getFunds(@RequestHeader("Authorization")String authorization,  @RequestBody WazdanGetFundsRequest request) {
		
		WazdanRequestHeader requestHeader = new WazdanRequestHeader(authorization);
		
		logger.info("Wazdan: Message (getFunds) request={}", request.toJson());
		
		WazdanResponse response = null;
		try {
			
			response = wazdanService.processRequest(requestHeader, request, WazdanMethodType.GETFUNDS);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new WazdanErrorResponse(WazdanErrorCodes.STATUS_CODE_INTERNAL_ERROR, WazdanErrorCodes.STATUS_MSG_INTERNAL_ERROR);
		}

		logger.info("Wazdan: Message (getFunds) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/wazdan/rcClose", method = {RequestMethod.POST})
	public WazdanResponse rcClose(@RequestHeader("Authorization")String authorization,  @RequestBody WazdanRcCloseRequest request) {
		
		WazdanRequestHeader requestHeader = new WazdanRequestHeader(authorization);
		
		logger.info("Wazdan: Message (rcClose) request={}", request.toJson());
		
		WazdanResponse response = null;
		try {
			
			response = wazdanService.processRequest(requestHeader, request, WazdanMethodType.RCCLOSE);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new WazdanErrorResponse(WazdanErrorCodes.STATUS_CODE_INTERNAL_ERROR, WazdanErrorCodes.STATUS_MSG_INTERNAL_ERROR);
		}

		logger.info("Wazdan: Message (rcClose) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/wazdan/rcContinue", method = {RequestMethod.POST})
	public WazdanResponse rcContinue(@RequestHeader("Authorization")String authorization,  @RequestBody WazdanRcContinueRequest request) {
		
		WazdanRequestHeader requestHeader = new WazdanRequestHeader(authorization);
		
		logger.info("Wazdan: Message (rcContinue) request={}", request.toJson());
		
		WazdanResponse response = null;
		try {
			
			response = wazdanService.processRequest(requestHeader, request, WazdanMethodType.RCCONTINUE);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new WazdanErrorResponse(WazdanErrorCodes.STATUS_CODE_INTERNAL_ERROR, WazdanErrorCodes.STATUS_MSG_INTERNAL_ERROR);
		}

		logger.info("Wazdan: Message (rcContinue) response={}", response.toJson());
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/wazdan/ping", method = {RequestMethod.POST})
	public WazdanResponse ping() {
		return null;
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public void handleBadRequest(HttpMessageNotReadableException e) {
	    logger.error("Returning HTTP 400 Bad Request", e);
	    throw e;
	}

	
}
