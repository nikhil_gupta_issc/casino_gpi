package com.gpi.controller;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.gpi.service.gpi.HeliogamingService;
import com.heliogaming.api.HeliogamingCancelBetRequest;
import com.heliogaming.api.HeliogamingCancelWinRequest;
import com.heliogaming.api.HeliogamingCoreResponse;
import com.heliogaming.api.HeliogamingGetBalanceRequest;
import com.heliogaming.api.HeliogamingLoginRequest;
import com.heliogaming.api.HeliogamingPlaceBetRequest;
import com.heliogaming.api.HeliogamingPlaceWinRequest;
import com.heliogaming.api.HeliogamingStaggeredPaymentRequest;

@Controller
public class HeliogamingIntegrationController {
	
	private static final Logger logger = LoggerFactory.getLogger(HeliogamingIntegrationController.class);
	
	@Autowired
	private HeliogamingService heliogamingService;

	@ResponseBody
	@RequestMapping(value = "/heliogaming/Login", method = { RequestMethod.POST })
	public HeliogamingCoreResponse login(@RequestBody HeliogamingLoginRequest request) {
		return heliogamingService.processRequest(request);
	}

	@ResponseBody
	@RequestMapping(value = "/heliogaming/GetBalance", method = { RequestMethod.POST })
	public HeliogamingCoreResponse getBalance(@RequestBody HeliogamingGetBalanceRequest request) {
		return heliogamingService.processRequest(request);
	}
	
	@ResponseBody
	@RequestMapping(value = "/heliogaming/PlaceBet", method = { RequestMethod.POST })
	public HeliogamingCoreResponse placeBet(@RequestBody HeliogamingPlaceBetRequest request) {
		return heliogamingService.processRequest(request);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/heliogaming/PlaceWin", method = { RequestMethod.POST })
	public HeliogamingCoreResponse placeWin(@RequestBody HeliogamingPlaceWinRequest request) {
		return heliogamingService.processRequest(request);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/heliogaming/StaggeredPayment", method = { RequestMethod.POST })
	public HeliogamingCoreResponse staggeredPayment(@RequestBody HeliogamingStaggeredPaymentRequest request) {
		return heliogamingService.processRequest(request);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/heliogaming/CancelBet", method = { RequestMethod.POST })
	public HeliogamingCoreResponse cancelBet(@RequestBody HeliogamingCancelBetRequest request ){
		return heliogamingService.processRequest(request);
	}
	
	@JsonView
	@ResponseBody
	@RequestMapping(value = "/heliogaming/CancelWin", method = { RequestMethod.POST })
	public HeliogamingCoreResponse cancelWin(@RequestBody HeliogamingCancelWinRequest request) {
		return heliogamingService.processRequest(request);
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public void handleBadRequest(HttpMessageNotReadableException e) {
	    logger.error("Returning HTTP 400 Bad Request", e);
	    throw e;
	}
}
