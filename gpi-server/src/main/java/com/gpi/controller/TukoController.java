package com.gpi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.MarshalException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.utils.MessageHelper;
import com.gpi.utils.cache.CacheManager;

@Controller
public class TukoController {
	
	
    private static final Logger logger = LoggerFactory.getLogger(TukoController.class);
    
    @Qualifier("gamePlatformIntegrationService")
    @Autowired
    private GamePlatformIntegrationService gamePlatformIntegrationService;
       
    
    @Qualifier("tukoCacheManager")
    @Autowired
    private CacheManager tukoCacheManager;
	
	 	@ResponseBody
	    @RequestMapping(value = "/handle-tuko.do", method = {RequestMethod.POST}, consumes = "text/xml; charset=utf-8")
	    public String handle(@RequestBody String xml) {

	        logger.info("Tuko: Message received={}", xml);
	        
	        String response;
			try {
				Message messageFromGame = MessageHelper.createMessageFromString(xml);
				
				if (messageFromGame.getMethod() == null) {
				    throw new MarshalException("Invalid PKT. Method element expected");
				}
					logger.info("Looking for gameRequest for messageFromGame={}", messageFromGame);
					
					
					
				 
				 
					if (shouldUpdateRound(messageFromGame)) {
						String token = messageFromGame.getMethod().getParams().getToken().getValue();
						Long roundId = (Long)tukoCacheManager.getAndTouch("tuko-" +token , 2592000);
						logger.info("Tuko: Getting roundId: "+roundId + " for token: "+token);
						messageFromGame.getMethod().getParams().getRoundId().setValue(roundId);
					
					}

					response =  MessageHelper.createStringFromMessage(gamePlatformIntegrationService.processMessage(messageFromGame));
			} catch (MarshalException e) {
	            logger.error("Error marshalling object messageFromGame=" + xml, e);
	            try {
	            	response = MessageHelper.createStringFromMessage(MessageHelper.createErrorResponse(new GamePlatformIntegrationException(e), null));
	            } catch (Exception e2){
	            	response = "Error 0";
	            }
	        } 
	       
	        logger.info("Sending xml to game: {}", response);
	        return response;
	    }
	 	
	 	
	 	private boolean shouldUpdateRound(Message message) {

	      
	    
	           return  message.getMethod().getName().equals(MethodType.BET)
	                    || message.getMethod().getName().equals(MethodType.WIN)
	                    || message.getMethod().getName().equals(MethodType.REFUND_TRANSACTION);

	       

	    }
}
