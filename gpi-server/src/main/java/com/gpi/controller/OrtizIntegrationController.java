package com.gpi.controller;

import java.math.BigDecimal;

import org.codehaus.jackson.map.annotate.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gpi.domain.PlayerInfo;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.utils.MessageHelper;
import com.gpi.utils.OrtizUtils;
import com.gpi.utils.cache.CacheManager;
import com.ortiz.api.domain.AccountStatRequest;
import com.ortiz.api.domain.AccountStatResponse;
import com.ortiz.api.domain.BetRequest;
import com.ortiz.api.domain.BetResponse;
import com.ortiz.api.domain.GameInfoRequest;
import com.ortiz.api.domain.GameInfoResponse;

@Controller
public class OrtizIntegrationController {

	@Autowired
	private GameRequestDao gameRequestDao;

	@Qualifier("gamePlatformIntegrationService")
	@Autowired
	private GamePlatformIntegrationService gamePlatformIntegrationService;

	@Qualifier("gamePlatformIntegrationCacheManager")
	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private PlayerInfoDao playerInfoDaoCache;

	private static final Logger logger = LoggerFactory.getLogger(OrtizIntegrationController.class);

	@JsonView
	@ResponseBody
	@RequestMapping(value = "/ortiz/accountstat", method = { RequestMethod.POST })
	public AccountStatResponse accountstat(@RequestBody AccountStatRequest request)
			throws GameRequestNotExistentException {

		Message messageFromGame = getMessage(request.getAuthToken(), null, null, null, MethodType.GET_PLAYER_INFO);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
		logger.info("Accountstat request: " + request);

		PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(OrtizUtils.getPlayerInfoKey(request.getAuthToken()));

		String currency;
		double balance;

		if (returnMessage.getResult().getSuccess() == 1) {
			currency = returnMessage.getResult().getReturnset().getCurrency().getValue();
			balance = returnMessage.getResult().getReturnset().getBalance().getValue();
		} else {
			currency = playerInfo.getCurrency();
			messageFromGame = getMessage(request.getAuthToken(), null, null, null, MethodType.GET_BALANCE);
			returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
			balance = returnMessage.getResult().getReturnset().getBalance().getValue();
		}

		String language;

		if (playerInfo != null) {
			language = playerInfo.getLanguage();
		} else {
			language = "UNK";
		}

		AccountStatResponse response = new AccountStatResponse();
		response.setUserCurrency(currency);
		response.setUserLanguage(language);
		response.setBalanceCredits(new BigDecimal(balance).divide(new BigDecimal(100)));
		
		logger.info("Acountstat response: " + response);
		
		return response;
	}

	@JsonView
	@ResponseBody
	@RequestMapping(value = "/ortiz/startgame", method = { RequestMethod.POST })
	public GameInfoResponse startgame(@RequestBody GameInfoRequest request) throws GameRequestNotExistentException {
		logger.info("Startgame request: " + request);

		Message messageFromGame = getMessage(request.getAuthToken(), null, null, null, MethodType.GET_BALANCE);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
		GameInfoResponse response = new GameInfoResponse();
		response.setBalanceCredits(new BigDecimal(returnMessage.getResult().getReturnset().getBalance().getValue())
				.divide(new BigDecimal(100)));
		
		logger.info("Startgame response: " + response);
		return response;
	}

	@JsonView
	@ResponseBody
	@RequestMapping(value = "/ortiz/endgame", method = { RequestMethod.POST })
	public GameInfoResponse endgame(@RequestBody GameInfoRequest request) throws GameRequestNotExistentException {
		logger.info("Endgame request: " + request);

		Message messageFromGame = getMessage(request.getAuthToken(), null, null, null, MethodType.GET_BALANCE);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
		GameInfoResponse response = new GameInfoResponse();
		response.setBalanceCredits(new BigDecimal(returnMessage.getResult().getReturnset().getBalance().getValue())
				.divide(new BigDecimal(100)));
		
		logger.info("Endgame response: " + response);
		return response;
	}

	@JsonView
	@ResponseBody
	@RequestMapping(value = "/ortiz/deposit", method = { RequestMethod.POST })
	public BetResponse deposit(@RequestBody BetRequest request) throws GameRequestNotExistentException, HttpRequestMethodNotSupportedException {
		logger.info("Deposit request: " + request);

		Message messageFromGame = getMessage(request.getAuthToken(), new Float(request.getAmount() * 100F).longValue(),
				request.getMessageId(), request.getGameRoundId(), MethodType.WIN);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
	
		BetResponse response = new BetResponse();
		response.setBalanceCredits(new BigDecimal(returnMessage.getResult().getReturnset().getBalance().getValue())
				.divide(new BigDecimal(100)));
		
		
		
		logger.info("Deposit response: " + response);
		return response;
	}

	@JsonView
	@ResponseBody
	@RequestMapping(value = "/ortiz/withdraw", method = { RequestMethod.POST })
	public BetResponse withdraw(@RequestBody BetRequest request) throws GameRequestNotExistentException, HttpRequestMethodNotSupportedException {
		logger.info("Withdraw request: " + request);

		Message messageFromGame = getMessage(request.getAuthToken(), new Float(request.getAmount() * 100F).longValue(),
				request.getMessageId(), request.getGameRoundId(), MethodType.BET);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
		BetResponse response = new BetResponse();
		if (returnMessage.getResult().getSuccess() == 0L) {
			throw new HttpRequestMethodNotSupportedException("Insuficient Founds");
		} else {
			response.setBalanceCredits(new BigDecimal(returnMessage.getResult().getReturnset().getBalance().getValue())
				.divide(new BigDecimal(100)));
		}
		logger.info("Withdraw response: " + response);
		return response;
	}

	private Message getMessage(String token, Long amount, Long transactionId, Long roundId, MethodType methodType)
			throws GameRequestNotExistentException {

		GameRequest gameRequest = gameRequestDao.getGameRequest(token);

		MessageHelper messageHelper = new MessageHelper(gameRequest.getGame().getGameProvider().getName(),
				gameRequest.getGame().getGameProvider().getPassword(), methodType, token);

		Message betMessage = messageHelper.getMessage();
		if (amount != null) {
			MessageHelper.addAmountToMethod(betMessage, amount);
		}

		if (transactionId != null) {
			MessageHelper.addTransactionIdToMethod(betMessage, String.valueOf(transactionId));
		}
		MessageHelper.addGameReferenceToMethod(betMessage, gameRequest.getGame().getName());
		MessageHelper.addRoundIdToMethod(betMessage, roundId);
		return betMessage;
	}

}
