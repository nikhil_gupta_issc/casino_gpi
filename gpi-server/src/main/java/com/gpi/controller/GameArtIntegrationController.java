package com.gpi.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.annotate.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gameart.api.domain.GameArtResponse;
import com.gpi.service.gpi.GameArtService;

@Controller
public class GameArtIntegrationController {


	private static final Logger logger = LoggerFactory.getLogger(GameArtIntegrationController.class);

	@Autowired
	private GameArtService gameArtService;
	
	@ResponseBody
	@RequestMapping(value="/gameart/handle.do", method=RequestMethod.GET)
	@JsonView
	public GameArtResponse handle(@RequestParam Map<String, String> params, HttpServletRequest request) {

		logger.info("GameArt: Message received={}", params);
		
		String queryString = request.getQueryString();
		
		GameArtResponse resp = gameArtService.processRequest(params, queryString);
		
		return resp;
	}
}
