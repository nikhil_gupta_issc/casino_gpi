package com.gpi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.evenbet.api.EvenbetErrorCodes;
import com.evenbet.api.EvenbetMethodType;
import com.evenbet.domain.EvenbetErrorResponse;
import com.evenbet.domain.EvenbetRequest;
import com.evenbet.domain.EvenbetRequestHeader;
import com.evenbet.domain.EvenbetResponse;
import com.gpi.service.gpi.EvenbetService;

@Controller

public class EvenbetIntegrationController {

	private static final Logger logger = LoggerFactory.getLogger(EvenbetIntegrationController.class);

	@Autowired
	private EvenbetService evenbetService;
	
	@ResponseBody
	@RequestMapping(value = "/evenbet/handle.do", method = {RequestMethod.POST})
	public EvenbetResponse handle(@RequestHeader("sign")String sign, @RequestBody String jsonBody) {

		EvenbetRequestHeader header = new EvenbetRequestHeader(sign);
		EvenbetRequest request = EvenbetRequest.create(jsonBody);
		
		logger.info("Evenbet: Message ({}) sign={}, request={}", request.getMethod(), header.getSign(), jsonBody);

		EvenbetResponse response = null;
		try {
			
			response = evenbetService.processRequest(header, request, EvenbetMethodType.getByMethodName(request.getMethod()));
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response = new EvenbetErrorResponse(EvenbetErrorCodes.GENERIC_ERROR_CODE, e.getMessage());
		}

		logger.info("Evenbet: Message ({}) response={}", request.getMethod(), response.toJson());
		return response;
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public void handleBadRequest(HttpMessageNotReadableException e) {
	    logger.error("Returning HTTP 400 Bad Request", e);
	    throw e;
	}

}
