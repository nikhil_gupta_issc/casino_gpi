package com.gpi.controller;

import org.codehaus.jackson.map.annotate.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gpi.domain.audit.Transaction;
import com.gpi.dto.transaction.GpiTransactionIdRequest;
import com.gpi.dto.transaction.GpiTransactionIdResponse;
import com.gpi.dto.transaction.GpiTransactionIdResponseBuilder;
import com.gpi.service.transaction.TransactionService;

@Controller
public class TransactionController {

	@Autowired
	private TransactionService transactionService;
	
	@JsonView
 	@ResponseBody
    @RequestMapping(value = "/transaction/{gameTransactionId}", method = {RequestMethod.GET})
	public GpiTransactionIdResponse GpiTransactionId(GpiTransactionIdRequest request){
 		Transaction transaction = transactionService.getTransaction(request.getGameTransactionId(), request.getGame());
 		return new GpiTransactionIdResponseBuilder().setTransaction(transaction).build();
 	}
	
}
