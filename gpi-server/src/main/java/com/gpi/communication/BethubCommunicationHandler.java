package com.gpi.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.Provider;
import com.gpi.dao.game.GameDao;
import com.gpi.domain.game.Game;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.CommunicationException;
import com.gpi.exceptions.NonExistentGameException;

public class BethubCommunicationHandler extends DefaultCommunicationHandler implements CommunicationHandler {
	
	private GameDao gameDao;
	
	private static final Logger log = LoggerFactory.getLogger(BethubCommunicationHandler.class);
	
	@Override
	public Message communicate(Publisher publisher, Message message) throws CommunicationException {
		try {
			if (messageHasGameReference(message)) {
				String gameReference = message.getMethod().getParams().getGameReference().getValue();
				
				Game game = gameDao.getGame(gameReference);
				String gameProvider = game.getGameProvider().getName();
				
				Provider provider = new Provider();
				provider.setValue(gameProvider);
				message.getMethod().getParams().setProvider(provider);
			}
		} catch (NonExistentGameException e) {
			log.error(e.getMessage());
		}
		
		Message publisherMessage = super.communicate(publisher, message);

		return publisherMessage;
		
	}
	
	private boolean messageHasGameReference(Message message) {
		return message.getMethod().getParams().getGameReference() != null;
	}

	public void setGameDao(GameDao gameDao) {
		this.gameDao = gameDao;
	}
}
