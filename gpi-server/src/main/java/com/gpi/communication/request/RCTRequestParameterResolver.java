package com.gpi.communication.request;

import com.gpi.domain.game.PlayingType;
import com.gpi.dto.LoadGameDto;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by igabba on 15/01/15.
 */
public class RCTRequestParameterResolver implements RequestParameterResolver {
    @Override
    public void setProviderSpecificParams(String serviceUrl, String lang, String type, String serviceMessage, LoadGameDto loadGameDto, UriComponentsBuilder builder) {
        builder.queryParam("session", loadGameDto.getToken());
    }
}
