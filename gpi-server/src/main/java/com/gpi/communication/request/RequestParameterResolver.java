package com.gpi.communication.request;

import com.gpi.dto.LoadGameDto;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by igabba on 15/01/15.
 */
public interface RequestParameterResolver {

    void setProviderSpecificParams(String serviceUrl, String lang, String type, String serviceMessage, LoadGameDto loadGameDto, UriComponentsBuilder builder);
}
