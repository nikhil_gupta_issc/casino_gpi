package com.gpi.communication.request;

import com.gpi.domain.game.PlayingType;
import com.gpi.dto.LoadGameDto;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by igabba on 15/01/15.
 */
public class GpiRequestParameterResolver implements RequestParameterResolver {

    @Override
    public void setProviderSpecificParams(String serviceUrl, String lang, String type, String serviceMessage, LoadGameDto loadGameDto, UriComponentsBuilder builder) {
        builder.queryParam("token", loadGameDto.getToken());
        builder.queryParam("pn", "gpi");
        if (serviceUrl != null) {
            builder.queryParam("serviceUrl", serviceUrl);
            builder.queryParam("serviceMessage", serviceMessage);
        }
        builder.queryParam("lang", lang.toLowerCase());
        builder.queryParam("type", (type == null) ? PlayingType.CHARGED.getType() : type);
    }
}
