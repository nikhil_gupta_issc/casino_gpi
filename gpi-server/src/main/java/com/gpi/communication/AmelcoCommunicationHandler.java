package com.gpi.communication;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.CommunicationException;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.amelco.AmelcoHandler;

public class AmelcoCommunicationHandler extends DefaultCommunicationHandler implements CommunicationHandler {
	
	private static final Logger log = LoggerFactory.getLogger(AmelcoCommunicationHandler.class);
	private Map<String, AmelcoHandler> handlers;
	
	@Override
	public Message communicate(Publisher publisher, Message message) throws CommunicationException {
		log.debug("Sending message to publisher {}: {}", publisher.getName(), message);
		String method = message.getMethod().getName().toString();
		
		AmelcoHandler handler = handlers.get(method);
		
		handler.setBaseURL(publisher.getUrl());
		
		try {
			return handler.execute(publisher, message);
		} catch (GameRequestNotExistentException e) {
			log.error(e.getMessage(), e);
			throw new CommunicationException(e);
		}
	}

	public void setHandlers(Map<String, AmelcoHandler> handlers) {
		this.handlers = handlers;
	}
}
