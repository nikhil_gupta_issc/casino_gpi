package com.gpi.communication.test;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.AlreadyProcessed;
import com.gpi.api.Balance;
import com.gpi.api.Currency;
import com.gpi.api.LoginName;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.api.Result;
import com.gpi.api.Returnset;
import com.gpi.api.Token;
import com.gpi.api.TransactionId;
import com.gpi.communication.CommunicationHandler;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.CommunicationException;
import com.gpi.exceptions.MarshalException;
import com.gpi.service.xml.MessageMarshaller;
import com.gpi.utils.MessageHelper;

/**
 * Created by Nacho on 2/21/14.
 */
public class TestCommunicationHandler implements CommunicationHandler {

    private MessageMarshaller messageMarshaller;

    private static final Logger log = LoggerFactory.getLogger(TestCommunicationHandler.class);
    
    @Override
    public Message communicate(Publisher publisher, Message message) throws CommunicationException {
    	log.debug("Sending message: {}", message);
        String response = null;
        try {

            MethodType methodType = message.getMethod().getName();
            if (methodType.equals(MethodType.GET_PLAYER_INFO)) {
                message = createGetAccountDetailsResponse();
            } else if (methodType.equals(MethodType.GET_BALANCE)) {
                message = createGetBalanceResponse();
            } else if (methodType.equals(MethodType.BET)) {
                message = createPlaceBetResponse();
            } else if (methodType.equals(MethodType.WIN)) {
                message = createAwardWinningsResponse();
            } else if (methodType.equals(MethodType.REFUND_TRANSACTION)) {
                message = createRefundBetResponse();
            }

            response = messageMarshaller.marshall(message);
        } catch (MarshalException e) {
            e.printStackTrace();
        }
        
        log.debug("Response: {}", response);
        return transformObjectToMessage(response);
    }

    @Override
    public Object transformMessageToProperObject(Message message) throws MarshalException, CommunicationException {
        return null;
    }

    @Override
    public Message transformObjectToMessage(Object o) throws CommunicationException {
        String response = (String) o;
        try {
            return MessageHelper.createMessageFromString(response);
        } catch (MarshalException e) {
            throw new CommunicationException(e.getMessage());
        }
    }

    private Message createRefundBetResponse() {
        Message message = new Message();
        Result result = new Result();
        result.setSuccess(1);
        result.setName(MethodType.REFUND_TRANSACTION);
        Returnset returnset = new Returnset();
        Token token = new Token();
        token.setValue("" + new Date().getTime());
        returnset.setToken(token);
        Balance balance = new Balance();
        balance.setValue(100000l);
        returnset.setBalance(balance);
        TransactionId transactionId = new TransactionId();
        transactionId.setValue("1111111");
        returnset.setTransactionId(transactionId);
        AlreadyProcessed alreadyProcessed = new AlreadyProcessed();
        alreadyProcessed.setValue(false);
        returnset.setAlreadyProcessed(alreadyProcessed);
        result.setReturnset(returnset);
        message.setResult(result);
        return message;
    }

    private Message createAwardWinningsResponse() {
        Message message = new Message();
        Result result = new Result();
        result.setSuccess(1);
        result.setName(MethodType.WIN);
        Returnset returnset = new Returnset();
        Token token = new Token();
        token.setValue("" + new Date().getTime());
        returnset.setToken(token);
        Balance balance = new Balance();
        balance.setValue(100000l);
        returnset.setBalance(balance);
        TransactionId transactionId = new TransactionId();
        transactionId.setValue("1111111");
        returnset.setTransactionId(transactionId);
        AlreadyProcessed alreadyProcessed = new AlreadyProcessed();
        alreadyProcessed.setValue(false);
        returnset.setAlreadyProcessed(alreadyProcessed);
        result.setReturnset(returnset);
        message.setResult(result);
        return message;
    }

    private Message createPlaceBetResponse() {
        Message message = new Message();
        Result result = new Result();
        result.setSuccess(1);
        result.setName(MethodType.BET);
        Returnset returnset = new Returnset();
        Token token = new Token();
        token.setValue("" + new Date().getTime());
        returnset.setToken(token);
        Balance balance = new Balance();
        balance.setValue(100000l);
        returnset.setBalance(balance);
        TransactionId transactionId = new TransactionId();
        transactionId.setValue("1111111");
        returnset.setTransactionId(transactionId);
        AlreadyProcessed alreadyProcessed = new AlreadyProcessed();
        alreadyProcessed.setValue(false);
        returnset.setAlreadyProcessed(alreadyProcessed);
        result.setReturnset(returnset);
        message.setResult(result);
        return message;
    }

    private Message createGetBalanceResponse() {
        Message message = new Message();
        Result result = new Result();
        result.setSuccess(1);
        result.setName(MethodType.GET_BALANCE);
        Returnset returnset = new Returnset();
        Token token = new Token();
        token.setValue("" + new Date().getTime());
        returnset.setToken(token);
        Balance balance = new Balance();
        balance.setValue(100000l);
        returnset.setBalance(balance);
        result.setReturnset(returnset);
        message.setResult(result);
        return message;
    }

    private Message createGetAccountDetailsResponse() {
        Message message = new Message();
        Result result = new Result();
        result.setSuccess(1);
        result.setName(MethodType.GET_PLAYER_INFO);
        Returnset returnset = new Returnset();
        Token token = new Token();
        token.setValue("" + new Date().getTime());
        returnset.setToken(token);
        LoginName loginName = new LoginName();
        loginName.setValue("UserGPI");
        returnset.setLoginName(loginName);
        Currency currency = new Currency();
        currency.setValue("BRL");
        returnset.setCurrency(currency);
        result.setReturnset(returnset);
        message.setResult(result);
        return message;
    }

    public void setMessageMarshaller(MessageMarshaller messageMarshaller) {
        this.messageMarshaller = messageMarshaller;
    }
}
