package com.gpi.communication;

import com.gpi.api.Message;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.CommunicationException;
import com.gpi.exceptions.MarshalException;
import com.gpi.utils.MessageHelper;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DefaultCommunicationHandler implements CommunicationHandler {

    private static final Logger log = LoggerFactory.getLogger(DefaultCommunicationHandler.class);
    private MultiThreadedHttpConnectionManager connectionManager;
    protected long timeout;
    protected int connectionTimeout;

    public Message communicate(Publisher publisher, Message message) throws CommunicationException {
    	log.debug("Sending message to publisher {}: {}", publisher.getName(), message);
        String response = null;
        PostMethod httpPost = null;
        long startTime = System.currentTimeMillis();
        try {
            String xml = (String) transformMessageToProperObject(message);
            HttpClient client = new HttpClient(connectionManager);
            HttpClientParams params = client.getParams();
            params.setConnectionManagerTimeout(timeout);
            params.setSoTimeout(connectionTimeout);
            
            log.debug("Connection Pool Size: "+ connectionManager.getConnectionsInPool());
            
            client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, true));

            httpPost = new PostMethod(publisher.getUrl());
            StringRequestEntity stringRequestEntity = new StringRequestEntity(xml, "text/xml", "utf-8");
            httpPost.setRequestEntity(stringRequestEntity);
            log.debug("Executing request " + httpPost.getURI());
            log.info("Sending: " + xml);

            client.executeMethod(httpPost);
            response = httpPost.getResponseBodyAsString();

            log.info("Received response: " + response);
        } catch (IOException e) {
            throw new CommunicationException(e);
        } finally {
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
        }
        Message messageResponse =  transformObjectToMessage(response);
        long stopTime = System.currentTimeMillis();
        log.info(publisher == null ? "empty" : publisher.getUrl() + " - Communication took " + (stopTime - startTime) + " ms");
        return messageResponse;
    }

    @Override
    public Object transformMessageToProperObject(Message message) throws CommunicationException {
        String stringFromMessage;
        try {
            stringFromMessage = MessageHelper.createStringFromMessage(message);
        } catch (MarshalException e) {
            throw new CommunicationException(e.getMessage());
        }
        return stringFromMessage;
    }

    @Override
    public Message transformObjectToMessage(Object o) throws CommunicationException {
        String response = (String) o;
        try {
            return MessageHelper.createMessageFromString(response);
        } catch (MarshalException e) {
            throw new CommunicationException(e.getMessage());
        }
    }

    public void setConnectionManager(MultiThreadedHttpConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }
}
