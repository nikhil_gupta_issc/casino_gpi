package com.gpi.utils;

/**
 * Builder for a player unique identifier
 * 
 * @author Alexandre
 *
 */
public class PlayerIdentifierBuilder {
	private static final String SEPARATOR = "-";

	private String username;
	private String publisherName;
	
	/**
	 * Build an unique identifier of a player
	 * @return unique identifier of a player
	 */
	public String build() {
		return this.publisherName + SEPARATOR + this.username;
	}
	
	/**
	 * @param username the username
	 * @return {@link PlayerIdentifierBuilder}
	 */
	public PlayerIdentifierBuilder setUsername(String username) {
		this.username = username;
		return this;
	}
	
	/**
	 * @param publisherName the publisherName
	 * @return {@link PlayerIdentifierBuilder}
	 */
	public PlayerIdentifierBuilder setPublisherName(String publisherName) {
		this.publisherName = publisherName;
		return this;
	}
	
	
}
