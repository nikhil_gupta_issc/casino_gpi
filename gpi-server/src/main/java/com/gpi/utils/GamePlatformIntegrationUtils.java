package com.gpi.utils;

import com.gpi.domain.PlayerInfo;

import java.math.BigDecimal;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.player.PlayerService;
import com.gpi.utils.MessageHelper;
import com.gpi.utils.cache.CacheManager;

/**
 * Utils to be used by GPI
 * 
 * @author Alexandre
 *
 */
public class GamePlatformIntegrationUtils {
	private GameRequestDao gameRequestDao;
	private GamePlatformIntegrationService gamePlatformIntegrationService;

	/**
	 * @param token the token
	 * @param amount the amount
	 * @param transactionId id of the transaction
	 * @param roundId round id
	 * @param methodType type of the method
	 * @return message ready to be sent to gpi
	 * @throws GameRequestNotExistentException
	 */
	public Message constructGPIMessage(String token, Long amount, String transactionId, Long roundId, MethodType methodType) throws GameRequestNotExistentException {

		GameRequest gameRequest = gameRequestDao.getGameRequest(token);

		MessageHelper messageHelper = new MessageHelper(gameRequest.getGame().getGameProvider().getLoginName(), gameRequest.getGame().getGameProvider().getPassword(),
				methodType, token);

		Message betMessage = messageHelper.getMessage();
		if (amount != null) {
			MessageHelper.addAmountToMethod(betMessage, amount);
		}

		if (transactionId != null) {
			MessageHelper.addTransactionIdToMethod(betMessage, transactionId);
		}
		MessageHelper.addGameReferenceToMethod(betMessage, gameRequest.getGame().getName());
		MessageHelper.addRoundIdToMethod(betMessage, roundId);
		return betMessage;
	}
	
	/**
	 * @param publisher name of the publisher
	 * @param token token of the publisher
	 * @param language the language of the player
	 * @return player info returned from gpi including token, username, currency and language
	 * @throws GameRequestNotExistentException
	 */
	public PlayerInfo loadPlayerInfo(String publisher, String token, String language) throws GameRequestNotExistentException {
		Message gpiFormattedMessageFromGame = constructGPIMessage(token, null, null, null,
				MethodType.GET_PLAYER_INFO);
		Message responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);

		String gpiToken = responseMessage.getResult().getReturnset().getToken().getValue();
		String usernamePublisher = responseMessage.getResult().getReturnset().getLoginName().getValue();

		PlayerIdentifierBuilder playerIdentifierBuilder = new PlayerIdentifierBuilder();
		playerIdentifierBuilder.setPublisherName(publisher);
		playerIdentifierBuilder.setUsername(usernamePublisher);
		String gpiUsername = playerIdentifierBuilder.build();

		String gpiCurrency = responseMessage.getResult().getReturnset().getCurrency().getValue();

		PlayerInfo playerInfo = new PlayerInfo(gpiToken, gpiUsername, gpiCurrency, language, null, usernamePublisher);

		return playerInfo;
	}
	
	/**
	 * @param publisher name of the publisher
	 * @param token token of the publisher
	 * @return player info returned from gpi including token, username and currency
	 * @throws GameRequestNotExistentException
	 */
	public PlayerInfo loadPlayerInfo(String publisher, String token) throws GameRequestNotExistentException {
		return loadPlayerInfo(publisher, token, null);
	}
	
	/**
	 * @param amount balance amount returned from GPI
	 * @param coefficient coefficient to be used in conversion
	 * @return balance amount returned from GPI ready to be used by the provider
	 */
	public double getAmountFromGPI(long amount, int coefficient) {
		return amount / (double)coefficient;
	}
	
	/**
	 * @param amount balance amount returned from Provider
	 * @param coefficient coefficient to be used in conversion
	 * @return balance amount returned from provider ready to be used by GPI
	 */
	public long getAmountFromProvider(double amount, int coefficient) {
		return (long) (amount * coefficient);
	}
	
	/**
	 * @param amount balance amount returned from GPI
	 * @param coefficient coefficient to be used in conversion
	 * @return balance amount returned from GPI ready to be used by the provider (uses BigDecimal)
	 */
	public BigDecimal getPreciseAmountFromGPI(long value, int coefficient) {
		return BigDecimal.valueOf(value).divide(BigDecimal.valueOf(coefficient));
	}
	
	/**
	 * @param amount balance amount returned from Provider 
	 * @param coefficient coefficient to be used in conversion
	 * @return precise amount returned from provider ready to be used by GPI (uses BigDecimal)
	 */
	public long getPreciseAmountFromProvider(BigDecimal amount, int coefficient) {
		BigDecimal convertedValue = amount.multiply(BigDecimal.valueOf(coefficient));
		return convertedValue.longValue();
	}
	
	/**
	 * @param message message from gpi
	 * @return if the message is successfully processed from gpi
	 */
	public boolean messageIsSuccess(Message message) {
		return (message.getResult().getSuccess() == 1) || messageIsAlreadyProcessed(message);
	}
	
	/**
	 * @param message message from gpi
	 * @return if the message is already processed
	 */
	public boolean messageIsAlreadyProcessed(Message message) {
		return message.getResult().getSuccess() == 0 && (message.getResult().getReturnset().getAlreadyProcessed() != null && message.getResult().getReturnset().getAlreadyProcessed().isValue());
	}
	
	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}

	/**
	 * @param gamePlatformIntegrationService the gamePlatformIntegrationService to set
	 */
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}
}
