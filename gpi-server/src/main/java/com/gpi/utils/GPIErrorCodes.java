package com.gpi.utils;

/**
 * Error codes returned from GPI
 * 
 * @author Alexandre
 *
 */
public interface GPIErrorCodes {
	public static final int INVALID_TOKEN = 2000;
	public static final int NON_EXISTING_TRANSACTION = 5000;
	public static final int UNKNOWN_TOKEN = 4000;
	public static final int GENERAL_EXCEPTION = -1;
	public static final int NOT_ENOUGH_CREDITS = 200;
}
