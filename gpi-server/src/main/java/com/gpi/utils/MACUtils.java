package com.gpi.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Formatter;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class MACUtils {

	public static final String HMAC_SHA256 = "HmacSHA256";
	
	public static final String HMAC_SHA512 = "HmacSHA512";
	
	public static String toHexString(byte[] bytes) {
	    Formatter formatter = new Formatter();
	    for (byte b : bytes) {
	        formatter.format("%02x", b);
	    }
	    return formatter.toString();
	}
	
	public static String calculateHMAC(String data, String secret, String algorithm)
	    throws SignatureException, NoSuchAlgorithmException, InvalidKeyException
	{
	    SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes(), algorithm);
	    Mac mac = Mac.getInstance(algorithm);
	    mac.init(secretKeySpec);
	    return toHexString(mac.doFinal(data.getBytes()));
	}
	

}
