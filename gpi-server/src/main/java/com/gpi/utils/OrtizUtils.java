package com.gpi.utils;

import com.gpi.constants.CacheConstants;

public class OrtizUtils {
	/**
	 * @param key identifier of the player
	 * @return player info identifier in the ortiz playerinfo cache
	 */
	public static String getPlayerInfoKey(String key) {
		return CacheConstants.ORTIZ_PLAYERINFO_KEY + key;
	}
}
