package com.gpi.utils;

import org.springframework.mobile.device.Device;

import javax.servlet.http.HttpServletRequest;

public class DeviceUtils {

    public static String resolveDevice(HttpServletRequest request) {
        Device device = (Device) request.getAttribute("currentDevice");

        if (device.isMobile()) {
            return "mobile";
        } else if (device.isNormal()) {
            return "desktop";
        } else {
            return "tablet";
        }
    }
}
