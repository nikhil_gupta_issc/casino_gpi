package com.gpi.exceptions;

/**
 * Created by Ignacio on 2/19/14.
 */
public class GameUrlNotDefinedException extends Exception {

    public GameUrlNotDefinedException(String message) {
        super(message);
    }
}
