package com.gpi.exceptions;

/**
 * Created by Nacho on 12/12/2014.
 */
public class PlayerNotConfiguredException extends GamePlatformIntegrationException {
    private static final long serialVersionUID = 1257136307686008246L;

    public PlayerNotConfiguredException(String s) {
        super(s);
    }
}
