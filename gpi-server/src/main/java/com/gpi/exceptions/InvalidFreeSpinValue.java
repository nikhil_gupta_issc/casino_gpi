package com.gpi.exceptions;

/**
 * Created by igabba on 05/01/15.
 */
public class InvalidFreeSpinValue extends Exception{
    private static final long serialVersionUID = -4700008830991853515L;

    public InvalidFreeSpinValue(String s) {
        super(s);
    }
}
