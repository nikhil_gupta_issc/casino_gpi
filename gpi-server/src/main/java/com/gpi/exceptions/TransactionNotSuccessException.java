package com.gpi.exceptions;

public class TransactionNotSuccessException extends GamePlatformIntegrationException {


    private static final long serialVersionUID = -3111508299647415901L;

    public TransactionNotSuccessException(String message, int errorCode) {
        super(message, errorCode);
    }

}
