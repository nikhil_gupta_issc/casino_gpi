package com.gpi.service.spinmatic;

import java.sql.Timestamp;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.spinmatic.api.SpinmaticStatus;
import com.spinmatic.api.domain.SpinmaticErrorResponse;
import com.spinmatic.api.domain.SpinmaticOkResponse;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponse;
import com.spinmatic.api.domain.SpinmaticResponseData;
import com.spinmatic.api.domain.SpinmaticTransactionResponse;
import com.spinmatic.api.domain.SpinmaticTransactionResponseData;

/**
 * Abstract handler to Spinmatic integration
 * 
 * @author Alexandre
 *
 */
public abstract class SpinmaticHandler {
	protected GamePlatformIntegrationService gamePlatformIntegrationService;
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	protected PlayerInfoDao playerInfoDao;
	
	protected static final int BALANCE_COEFFICIENT = 100;
	
	/**
	 * Handles a request from spinmatic
	 * @param request request sent by spinmatic
	 * @return response ready to be returned to spinmatic
	 * @throws GameRequestNotExistentException
	 */
	public abstract SpinmaticResponse handle(SpinmaticRequest request) throws GameRequestNotExistentException;
	
	/**
	 * @param request request from spinmatic
	 * @return ok response to be returned to spinmatic
	 */
	protected SpinmaticOkResponse createOkResponse(SpinmaticRequest request) {
		SpinmaticResponseData responseData = this.createResponseData(request);
		SpinmaticOkResponse response = new SpinmaticOkResponse();
		response.setResponse(responseData);
		response.setMessage(SpinmaticStatus.SUCCESS_MESSAGE);
		response.setStatus(SpinmaticStatus.SUCCESS_MESSAGE);
		
		return response;
	}
	
	/**
	 * @param request request from spinmatic
	 * @return transaction response to be returned to spinmatic
	 */
	protected SpinmaticTransactionResponse createTransactionResponse(SpinmaticRequest request) {
		SpinmaticTransactionResponseData responseData = this.createTransactionResponseData(request);
		SpinmaticTransactionResponse response = new SpinmaticTransactionResponse();
		response.setResponse(responseData);
		response.setMessage(SpinmaticStatus.SUCCESS_MESSAGE);
		response.setStatus(SpinmaticStatus.SUCCESS_MESSAGE);
		
		return response;
	}
	
	/**
	 * @param request request from spinmatic
	 * @return error response to be returned to spinmatic
	 */
	protected SpinmaticErrorResponse createErrorResponse(SpinmaticRequest request, String errorMessage, int errorCode) {
		SpinmaticErrorResponse errorResponse = new SpinmaticErrorResponse();
		errorResponse.setMessage(errorMessage);
		errorResponse.setResponse(false);
		errorResponse.setStatus(String.valueOf(errorCode));
		
		return errorResponse;
	}
	
	/**
	 * @param transactionRequest transaction request from spinmatic
	 * @return transaction response data to be added to transaction response
	 */
	private SpinmaticTransactionResponseData createTransactionResponseData(SpinmaticRequest transactionRequest) {
		SpinmaticResponseData responseData = this.createResponseData(transactionRequest);
		SpinmaticTransactionResponseData transactionResponseData = new SpinmaticTransactionResponseData(responseData);
		transactionResponseData.setTransactionId(transactionRequest.getTransactionId());
		transactionResponseData.setRoundId(transactionRequest.getRoundId());
		
		return transactionResponseData;
	}
	
	/**
	 * @param request request from spinmatic
	 * @return response data to be added to response
	 */
	private SpinmaticResponseData createResponseData(SpinmaticRequest request) {
		SpinmaticResponseData responseData = new SpinmaticResponseData();
		responseData.setBonusAmount(0.0);
		responseData.setAction(request.getAction());
		responseData.setGameId(request.getGameId());
		responseData.setOwnerId(request.getOwnerId());
		responseData.setOwnerExternalId(request.getOwnerExternalId());
		responseData.setPlayerId(request.getPlayerId());
		responseData.setPlayerExternalId(request.getPlayerExternalId());
		responseData.setPlayerUsername(request.getPlayerUsername());
		responseData.setSessionId(request.getSessionId());
		responseData.setSessionType(request.getSessionType());
		responseData.setGameId(request.getGameId());
		responseData.setSiteId(request.getSiteId());
		responseData.setTimestamp((new Timestamp(System.currentTimeMillis())).toString());
		responseData.setUniqueId(request.getUniqueId());
		responseData.setPublicKey(request.getPublicKey());
		responseData.setAccessKey(request.getAccessKey());
		responseData.setPrivateKey(request.getPrivateKey());
		responseData.setTransactionId(request.getTransactionId());
		
		return responseData;
	}
	
	/**
	 * @param request request form gpi
	 * @return player info cache key constructed using the sessionid from spinmatic
	 */
	protected String getPlayerInfoKeyWithSessionId(SpinmaticRequest request)  {
		String sessionId = request.getSessionId();
		String playerInfoKey = getPlayerInfoKey(sessionId);
		
		return playerInfoKey;
	}
	
	/**
	 * @param key identifier of the player
	 * @return player info identifier in the spinmatic playerinfo cache
	 */
	public static String getPlayerInfoKey(String key) {
		return CacheConstants.SPINMATIC_PLAYERINFO_KEY + key;
	}
	
	protected void setPlayerInfoTokenAndStore(String key, String token, PlayerInfo playerInfo) {
		if (!token.trim().isEmpty()) {
			playerInfo.setToken(token);
			this.playerInfoDao.savePlayerInfo(key, playerInfo);
		}
	}

	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	public void setPlayerInfoDao(PlayerInfoDao playerInfoDao) {
		this.playerInfoDao = playerInfoDao;
	} 
	
	
}
