package com.gpi.service.spinmatic;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.spinmatic.api.domain.SpinmaticErrorResponse;
import com.spinmatic.api.domain.SpinmaticOkResponse;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponse;

/**
 * Handler for GetPlayerInfo/GetBalance requests from spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticGetPlayerInfoHandler extends SpinmaticHandler {

	@Override
	public SpinmaticResponse handle(SpinmaticRequest request) throws GameRequestNotExistentException {
		String playerInfoKey = this.getPlayerInfoKeyWithSessionId(request);
		PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
		String token = playerInfo.getToken();
		
		Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
		Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
		
		if (this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
			SpinmaticOkResponse response = this.createOkResponse(request);
			
			long gpiBalance = gpiMessage.getResult().getReturnset().getBalance().getValue();
			Double balance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT).doubleValue();
			String currency = playerInfo.getCurrency();
			String gpiToken = gpiMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(playerInfoKey, gpiToken, playerInfo);
			
			response.getResponse().setBalance(balance);
			response.getResponse().setCurrency(currency);

			return response;
		} else {
			String errorMessage = gpiMessage.getResult().getReturnset().getError().getValue();
			int errorCode = gpiMessage.getResult().getReturnset().getErrorCode().getValue();
			SpinmaticErrorResponse errorResponse = this.createErrorResponse(request, errorMessage, errorCode);
			return errorResponse;
		}
	}

}
