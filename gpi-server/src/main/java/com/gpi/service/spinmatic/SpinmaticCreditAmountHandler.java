package com.gpi.service.spinmatic;

import java.math.BigDecimal;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.spinmatic.api.domain.SpinmaticErrorResponse;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponse;
import com.spinmatic.api.domain.SpinmaticTransactionResponse;

/**
 * Handler for CreditAmount requests from spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticCreditAmountHandler extends SpinmaticHandler {

	@Override
	public SpinmaticResponse handle(SpinmaticRequest request) throws GameRequestNotExistentException {
		String playerInfoKey = this.getPlayerInfoKeyWithSessionId(request);
		PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
		String token = playerInfo.getToken();
		
		long gpiAmount = this.gamePlatformIntegrationUtils.getPreciseAmountFromProvider(BigDecimal.valueOf(request.getCreditAmount()), BALANCE_COEFFICIENT);
		
		Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, request.getTransactionId(), request.getRoundId(), MethodType.WIN);
		Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
		
		if (this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
			SpinmaticTransactionResponse response = this.createTransactionResponse(request);
			
			if (this.gamePlatformIntegrationUtils.messageIsAlreadyProcessed(gpiMessage)) {
				message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
				gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
			}
			
			long gpiBalance = gpiMessage.getResult().getReturnset().getBalance().getValue();
			long oldValue = gpiBalance+gpiAmount;
			
			Double balance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT).doubleValue();
			Double oldBalance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(oldValue, BALANCE_COEFFICIENT).doubleValue();
			
			String currency = playerInfo.getCurrency();
			String gpiToken = gpiMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(playerInfoKey, gpiToken, playerInfo);
			
			response.getResponse().setBalance(balance);
			response.getResponse().setCurrency(currency);
			response.getResponse().setOldBalance(oldBalance);

			return response;
		} else {
			String errorMessage = gpiMessage.getResult().getReturnset().getError().getValue();
			int errorCode = gpiMessage.getResult().getReturnset().getErrorCode().getValue();
			SpinmaticErrorResponse errorResponse = this.createErrorResponse(request, errorMessage, errorCode);
			return errorResponse;
		}
	}

}
