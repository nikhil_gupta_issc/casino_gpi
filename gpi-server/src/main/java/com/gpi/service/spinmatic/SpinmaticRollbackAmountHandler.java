package com.gpi.service.spinmatic;

import java.math.BigDecimal;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.transaction.TransactionType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.MessageHelper;
import com.spinmatic.api.domain.SpinmaticErrorResponse;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponse;
import com.spinmatic.api.domain.SpinmaticTransactionResponse;

/**
 * Handler for RollbackAmount requests from spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticRollbackAmountHandler extends SpinmaticHandler {
	
	private TransactionService transactionService;

	@Override
	public SpinmaticResponse handle(SpinmaticRequest request) throws GameRequestNotExistentException {
		String playerInfoKey = this.getPlayerInfoKeyWithSessionId(request);
		PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
		String token = playerInfo.getToken();
		
		long gpiAmount = this.gamePlatformIntegrationUtils.getPreciseAmountFromProvider(BigDecimal.valueOf(request.getRollbackAmount()), BALANCE_COEFFICIENT);
		
		Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, "REF-"+request.getTransactionId(), request.getRoundId(), MethodType.REFUND_TRANSACTION);
		MessageHelper.addRefundedTransactionIdToMethod(message, request.getTransactionId());
		Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
		
		if (this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
			SpinmaticTransactionResponse response = this.createTransactionResponse(request);
			
			if (this.gamePlatformIntegrationUtils.messageIsAlreadyProcessed(gpiMessage)) {
				message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
				gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
			}
			
			String gameName = playerInfo.getGameName();
			
			TransactionType transactionType = this.getTransactionType(request.getTransactionId(), gameName);
			long gpiBalance = gpiMessage.getResult().getReturnset().getBalance().getValue();
			long oldValue = gpiBalance;
			
			if (TransactionType.BET.equals(transactionType)) {
				oldValue = oldValue - gpiAmount;
			} else if (TransactionType.WIN.equals(transactionType)) {
				oldValue = oldValue + gpiAmount;
			} else {
				return null;
			}
			
			Double balance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT).doubleValue();
			Double oldBalance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(oldValue, BALANCE_COEFFICIENT).doubleValue();
			
			String currency = playerInfo.getCurrency();
			String gpiToken = gpiMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(playerInfoKey, gpiToken, playerInfo);
			
			response.getResponse().setBalance(balance);
			response.getResponse().setCurrency(currency);
			response.getResponse().setOldBalance(oldBalance);

			return response;
		} else {
			String errorMessage = gpiMessage.getResult().getReturnset().getError().getValue();
			int errorCode = gpiMessage.getResult().getReturnset().getErrorCode().getValue();
			SpinmaticErrorResponse errorResponse = this.createErrorResponse(request, errorMessage, errorCode);
			return errorResponse;
		}
	}
	
	/**
	 * @param providerTransaction transaction id in spinmatic side
	 * @param gameName game name in gpi side
	 * @return type of the transaction (bet or win)
	 */
	private TransactionType getTransactionType(String providerTransaction, String gameName) {
		Transaction transaction = this.transactionService.getTransaction(providerTransaction, gameName);
		if (transaction == null) {
			return null;
		}
		
		return transaction.getType();
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}
	
	

}
