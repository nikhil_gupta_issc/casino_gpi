package com.gpi.service.pragmaticplay;

import java.math.BigInteger;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayBetResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResultResponse;

/**
 * Win message handler for Pragmatic Play integration
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayMessageWin extends PragmaticPlayMessageCommand {
	
	protected static final Logger logger = LoggerFactory.getLogger(PragmaticPlayMessageWin.class);

	@Override
	public PragmaticPlayResponse execute(Map<String, String> params) throws GameRequestNotExistentException {
		String userId = params.get("userId");
		long roundId = Long.valueOf(params.get("roundId"));
		double amount = Double.valueOf(params.get("amount"));
		String transactionId = params.get("reference");
		
		PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(getPlayerInfoKey(userId));
		String playerInfoToken = playerInfo.getToken();
		
		long gpiAmount = gamePlatformIntegrationUtils.getAmountFromProvider(amount, BALANCE_COEFFICIENT);
		
		Message messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, gpiAmount, transactionId, roundId, MethodType.WIN);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
		
		PragmaticPlayResponse response;
		
		if ((returnMessage.getResult().getSuccess() == 1) || returnMessage.getResult().getSuccess() == 0 && returnMessage.getResult().getReturnset().getTransactionId() != null) {
			// success or transaction already processed
			double winBalance;
			
			// save the transaction id returned as it can be removed due to calling GET_BALANCE in the next if else
			String gpiTransactionId = returnMessage.getResult().getReturnset().getTransactionId().getValue();
			
			if (returnMessage.getResult().getReturnset().getBalance() != null) {
				winBalance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			} else {
				// there is no balance in the previous response. do a new call to get the updated balance
				messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, null, null, null, MethodType.GET_BALANCE);
				returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
				winBalance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			}
			
			String gpiToken = returnMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(gpiToken, playerInfo);
			
			response = new PragmaticPlayResultResponse();
			((PragmaticPlayResultResponse) response).setCash(winBalance);
			((PragmaticPlayResultResponse) response).setCurrency(playerInfo.getCurrency());
			((PragmaticPlayResultResponse) response).setTransactionId(gpiTransactionId);
		} else {
			response = new PragmaticPlayResponse();
			logger.error(returnMessage.getResult().toString());
			response.setError(PragmaticPlayErrorCodes.INTERNAL_ERROR_RECONCILIATION);
		}
		
		logger.info("Win response: " + response);
		
		
		return response;
	}

}
