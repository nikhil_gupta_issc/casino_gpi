package com.gpi.service.pragmaticplay;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.PlayerIdentifierBuilder;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayAuthenticateResponse;
import com.pragmaticplay.api.domain.PragmaticPlayBalanceResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

/**
 * Authentication message handler for Pragmatic Play integration
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayMessageAuthenticate extends PragmaticPlayMessageCommand {

	protected static final Logger logger = LoggerFactory.getLogger(PragmaticPlayMessageAuthenticate.class);
	
	@Override
	public PragmaticPlayResponse execute(Map<String, String> params) throws GameRequestNotExistentException {
		String token = params.get("token");
		
		GameRequest gameRequest = gameRequestDao.getGameRequest(token);
		
		Message messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_PLAYER_INFO);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);

		String currency;
		double balance;
		String userId;

		PragmaticPlayResponse response;
		
		if (returnMessage.getResult().getSuccess() == 1) {
			currency = returnMessage.getResult().getReturnset().getCurrency().getValue();
			balance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			userId = returnMessage.getResult().getReturnset().getLoginName().getValue();
			
			PlayerInfo playerInfo = new PlayerInfo();
			
			PlayerIdentifierBuilder playerIdentifierBuilder = new PlayerIdentifierBuilder();
			playerIdentifierBuilder.setPublisherName(gameRequest.getPublisher().getName());
			playerIdentifierBuilder.setUsername(userId);
			userId = playerIdentifierBuilder.build();
			
			String responseToken = returnMessage.getResult().getReturnset().getToken().getValue();
			
			playerInfo.setToken(responseToken);
			playerInfo.setUsername(userId);
			playerInfo.setCurrency(currency);
			setPlayerInfoTokenAndStore(responseToken, playerInfo);
			
			response = new PragmaticPlayAuthenticateResponse();
			((PragmaticPlayAuthenticateResponse) response).setUserId(userId);
			((PragmaticPlayAuthenticateResponse) response).setCash(balance);
			((PragmaticPlayAuthenticateResponse) response).setCurrency(currency);
			((PragmaticPlayAuthenticateResponse) response).setToken(responseToken);
			
			logger.info("Authenticate response: " + response);
		} else {
			response = new PragmaticPlayResponse();
			response.setError(PragmaticPlayErrorCodes.EXPIRED_TOKEN_ERROR);
		}
		
		return response;
	}

}
