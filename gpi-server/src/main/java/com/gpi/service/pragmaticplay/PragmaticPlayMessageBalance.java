package com.gpi.service.pragmaticplay;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayBalanceResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

/**
 * Balance message handler for Pragmatic Play integration
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayMessageBalance extends PragmaticPlayMessageCommand {
	
	protected static final Logger logger = LoggerFactory.getLogger(PragmaticPlayMessageBalance.class);

	@Override
	public PragmaticPlayResponse execute(Map<String, String> params) throws GameRequestNotExistentException {
		String userId = params.get("userId");
		
		PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(getPlayerInfoKey(userId));

		Message messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfo.getToken(), null, null, null, MethodType.GET_BALANCE);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);

		String currency;
		double balance;
		
		PragmaticPlayResponse response;

		if (returnMessage.getResult().getSuccess() == 1) {
			currency = playerInfo.getCurrency();
			balance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			
			String gpiToken = returnMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(gpiToken, playerInfo);
			
			response = new PragmaticPlayBalanceResponse();
			((PragmaticPlayBalanceResponse) response).setCash(balance);
			((PragmaticPlayBalanceResponse) response).setCurrency(currency);
			
		} else {
			response = new PragmaticPlayResponse();
			logger.error(returnMessage.getResult().toString());
			response.setError(PragmaticPlayErrorCodes.INTERNAL_ERROR_NO_RECONCILIATION);
		}
		
		logger.info("Balance response: " + response);
		
		return response;
	}

}
