package com.gpi.service.pragmaticplay;

import java.math.BigInteger;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.MessageHelper;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayBetResponse;
import com.pragmaticplay.api.domain.PragmaticPlayRefundResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

/**
 * Refund message handler for Pragmatic Play integration
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayMessageRefund extends PragmaticPlayMessageCommand {
	
	protected static final Logger logger = LoggerFactory.getLogger(PragmaticPlayMessageRefund.class);

	@Override
	public PragmaticPlayResponse execute(Map<String, String> params) throws GameRequestNotExistentException {
		String userId = params.get("userId");
		long roundId = Long.valueOf(params.get("roundId"));
		double amount = Double.valueOf(params.get("amount"));
		String transactionId = params.get("reference");
		
		PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(getPlayerInfoKey(userId));
		String playerInfoToken = playerInfo.getToken();
		
		long gpiAmount = gamePlatformIntegrationUtils.getAmountFromProvider(amount, BALANCE_COEFFICIENT);
		
		Message messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, gpiAmount, "REF-" + transactionId, roundId, MethodType.REFUND_TRANSACTION);
		MessageHelper.addRefundedTransactionIdToMethod(messageFromGame, transactionId);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
		
		PragmaticPlayResponse response;
		
		if ((returnMessage.getResult().getSuccess() == 1) || returnMessage.getResult().getSuccess() == 0 && returnMessage.getResult().getReturnset().getTransactionId() != null) {
			// success or transaction already processed
			
			// if it was an error there will not be a valid token. just change it in successful responses
			if (returnMessage.getResult().getSuccess() == 1) {
				String gpiToken = returnMessage.getResult().getReturnset().getToken().getValue();
				setPlayerInfoTokenAndStore(gpiToken, playerInfo);
			}
		
			response = new PragmaticPlayRefundResponse();
			((PragmaticPlayRefundResponse) response).setTransactionId(returnMessage.getResult().getReturnset().getTransactionId().getValue());
		} else {
			response = new PragmaticPlayResponse();
			logger.error(returnMessage.getResult().toString());
			response.setError(PragmaticPlayErrorCodes.INTERNAL_ERROR_NO_RECONCILIATION);
		}
		
		logger.info("Refund response: " + response);
		
		return response;
	}

}
