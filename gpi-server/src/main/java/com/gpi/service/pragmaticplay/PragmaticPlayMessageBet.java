package com.gpi.service.pragmaticplay;

import java.math.BigInteger;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayBetResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

/**
 * Bet message handler for Pragmatic Play integration
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayMessageBet extends PragmaticPlayMessageCommand {
	
	protected static final Logger logger = LoggerFactory.getLogger(PragmaticPlayMessageBet.class);
	private static final int GPI_NOT_ENOUGH_CREDIT_ERROR = 200;
	
	@Override
	public PragmaticPlayResponse execute(Map<String, String> params) throws GameRequestNotExistentException {
		String userId = params.get("userId");
		long roundId = Long.valueOf(params.get("roundId"));
		double amount = Double.valueOf(params.get("amount"));
		String transactionId = params.get("reference");
		
		PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(getPlayerInfoKey(userId));
		String playerInfoToken = playerInfo.getToken();
		
		long gpiAmount = gamePlatformIntegrationUtils.getAmountFromProvider(amount, BALANCE_COEFFICIENT);
		
		Message messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, gpiAmount, transactionId, roundId, MethodType.BET);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
		
		PragmaticPlayResponse response;
		
		if ((returnMessage.getResult().getSuccess() == 1) || returnMessage.getResult().getSuccess() == 0 && returnMessage.getResult().getReturnset().getTransactionId() != null) {
			// success or transaction already processed
			
			double betBalance;
			
			// save the transaction id returned as it can be removed due to calling GET_BALANCE in the next if else
			String gpiTransactionId = returnMessage.getResult().getReturnset().getTransactionId().getValue();
			
			if (returnMessage.getResult().getReturnset().getBalance() != null) {
				betBalance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			} else {
				// there is no balance in the previous response. do a new call to get the updated balance
				messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, null, null, null, MethodType.GET_BALANCE);
				returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
				betBalance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			}
			
			String gpiToken = returnMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(gpiToken, playerInfo);
			
			response = new PragmaticPlayBetResponse();
			((PragmaticPlayBetResponse) response).setCash(betBalance);
			((PragmaticPlayBetResponse) response).setCurrency(playerInfo.getCurrency());
			((PragmaticPlayBetResponse) response).setTransactionId(gpiTransactionId);
		} else {
			response = new PragmaticPlayResponse();
			
			if (returnMessage.getResult().getSuccess() == 0 && returnMessage.getResult().getReturnset().getErrorCode().getValue() == GPI_NOT_ENOUGH_CREDIT_ERROR) {
				// insufficient balance error
				response.setError(PragmaticPlayErrorCodes.INSUFFICIENT_BALANCE_ERROR);
			} else {
				logger.error(returnMessage.getResult().toString());
				response.setError(PragmaticPlayErrorCodes.INTERNAL_ERROR_NO_RECONCILIATION);
			}
		}
		
		logger.info("Bet response: " + response);
		
		return response;
	}

}
