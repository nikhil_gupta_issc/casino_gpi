package com.gpi.service.pragmaticplay;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.Game;
import com.gpi.domain.transaction.TransactionType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayBetResponse;
import com.pragmaticplay.api.domain.PragmaticPlayEndRoundResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResultResponse;

/**
 * EndRound message handler for Pragmatic Play integration
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayMessageEndRound extends PragmaticPlayMessageCommand {
	
	protected static final Logger logger = LoggerFactory.getLogger(PragmaticPlayMessageEndRound.class);
	
	@Override
	public PragmaticPlayResponse execute(Map<String, String> params) throws GameRequestNotExistentException {
		String userId = params.get("userId");
		long roundId = Long.valueOf(params.get("roundId"));
		
		PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(getPlayerInfoKey(userId));
		String playerInfoToken = playerInfo.getToken();
		
		Message messageFromGame;
		
		GameRequest gameRequest = gameRequestDao.getGameRequest(playerInfoToken);
		
		if (shouldAddZeroWinTransaction(roundId, gameRequest.getGame())) {
			// create new transaction id using timestamp to save as a 0 win amount
			String transactionID = "GPI-0-" + (new Date()).getTime();
			messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, Long.valueOf(0), transactionID, roundId, MethodType.WIN);
		} else {
			messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, null, null, null, MethodType.GET_BALANCE);
		}
		
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
		
		PragmaticPlayResponse response;
		
		if ((returnMessage.getResult().getSuccess() == 1) || returnMessage.getResult().getSuccess() == 0 && returnMessage.getResult().getReturnset().getTransactionId() != null) {
			// success or transaction already processed
			double winBalance;
			
			if (returnMessage.getResult().getReturnset().getBalance() != null) {
				winBalance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			} else {
				// there is no balance in the previous response. do a new call to get the updated balance
				messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, null, null, null, MethodType.GET_BALANCE);
				returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
				winBalance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			}
			
			String gpiToken = returnMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(gpiToken, playerInfo);
			
			response = new PragmaticPlayEndRoundResponse();
			((PragmaticPlayEndRoundResponse) response).setCash(winBalance);
		} else {
			response = new PragmaticPlayResponse();
			logger.error(returnMessage.getResult().toString());
			response.setError(PragmaticPlayErrorCodes.INTERNAL_ERROR_RECONCILIATION);
		}
		
		return response;
	}
	
	private boolean shouldAddZeroWinTransaction(long roundID, Game game) {
		List<Transaction> transactions = transactionService.getTransactionsByGameRoundId(0, 10, roundID, game);
		
		for (Transaction t : transactions) {
			if (TransactionType.WIN.equals(t.getType())) {
				return false;
			}
		}
		
		return true;
	}

}
