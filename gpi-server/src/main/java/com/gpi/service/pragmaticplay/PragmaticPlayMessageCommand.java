package com.gpi.service.pragmaticplay;

import java.util.Map;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.gpi.impl.PragmaticPlayServiceImpl;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.MessageHelper;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

/**
 * Base message handler for Pragmatic Play integration
 * 
 * @author Alexandre
 *
 */
public abstract class PragmaticPlayMessageCommand {
	
	protected GamePlatformIntegrationService gamePlatformIntegrationService;

	protected GameRequestDao gameRequestDao;
	
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	
	protected PlayerInfoDao playerInfoDaoCache;
	
	protected TransactionService transactionService;
	
	protected static final int BALANCE_COEFFICIENT = 100;
	
	/**
	 * Handles the requested message
	 * @param params map of key value parameter received
	 * @return response to be returned
	 * @throws GameRequestNotExistentException thrown when the game request does not exist
	 */
	public abstract PragmaticPlayResponse execute(Map<String, String> params) throws GameRequestNotExistentException;
	
	/**
	 * @param key identifier of the player
	 * @return player info identifier in the pragmatic play playerinfo cache
	 */
	public static String getPlayerInfoKey(String key) {
		return CacheConstants.PRAGMATICPLAY_PLAYERINFO_KEY + key;
	}
	
	protected void setPlayerInfoTokenAndStore(String token, PlayerInfo playerInfo) {
		if (!token.trim().isEmpty()) {
			playerInfo.setToken(token);
			this.playerInfoDaoCache.savePlayerInfo(getPlayerInfoKey(playerInfo.getUsername()), playerInfo);
		}
	}
	
	/**
	 * @param playerInfoDaoCache the playerInfoDaoCache to set
	 */
	public void setPlayerInfoDaoCache(PlayerInfoDao playerInfoDaoCache) {
		this.playerInfoDaoCache = playerInfoDaoCache;
	}
	
	/**
	 * @param gamePlatformIntegrationUtils the gamePlatformIntegrationUtils to set
	 */
	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	/**
	 * @param gamePlatformIntegrationService the gamePlatformIntegrationService to set
	 */
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}
}
