package com.gpi.service.gameart;

import java.util.Map;

import com.gameart.api.GameArtResponseCodes;
import com.gameart.api.GameArtResponseMessages;
import com.gameart.api.domain.GameArtBalanceResponse;
import com.gameart.api.domain.GameArtErrorBalanceResponse;
import com.gameart.api.domain.GameArtRemoteData;
import com.gameart.api.domain.GameArtResponse;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.impl.GameArtServiceImpl;

public class GameArtMessageBalance extends GameArtMessageCommand {
	
	@Override
	public GameArtResponse execute(Map<String, String> params, String queryString) throws GameRequestNotExistentException {
		
		GameArtBalanceResponse response;
		
		String token = getRemoteDataToken(params);
		
		if (token == null) {
			response = new GameArtErrorBalanceResponse();
			response.setStatus(GameArtResponseCodes.INTERNAL_ERROR_STATUS_CODE);
			((GameArtErrorBalanceResponse)response).setMsg(GameArtResponseMessages.INVALID_REMOTE_DATA);
			return response;
		}
		else
			response = new GameArtBalanceResponse();
		
		Message message = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
		Message messageFromGPI = gamePlatformIntegrationService.processMessage(message);

		if (messageFromGPI.getResult().getSuccess() == 1) {
			double balance = gamePlatformIntegrationUtils.getAmountFromGPI(messageFromGPI.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			response.setBalance(balance);
			String currentToken = messageFromGPI.getResult().getReturnset().getToken().getValue();
			GameArtRemoteData remoteData = new GameArtRemoteData();
			remoteData.setCurrentToken(currentToken);
			response.setRemoteData(remoteData);
			response.setStatus(GameArtResponseCodes.OK_STATUS_CODE);
		}
		else
			response.setStatus(GameArtResponseCodes.INTERNAL_ERROR_STATUS_CODE);
		
		return response;
	}

}
