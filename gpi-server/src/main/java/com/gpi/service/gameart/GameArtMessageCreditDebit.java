package com.gpi.service.gameart;

import java.util.Map;

import com.gameart.api.GameArtResponseCodes;
import com.gameart.api.GameArtResponseMessages;
import com.gameart.api.domain.GameArtBalanceResponse;
import com.gameart.api.domain.GameArtErrorBalanceResponse;
import com.gameart.api.domain.GameArtRemoteData;
import com.gameart.api.domain.GameArtResponse;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.MessageHelper;

public class GameArtMessageCreditDebit extends GameArtMessageCommand {

	@Override
	public GameArtResponse execute(Map<String, String> params, String queryString) throws GameRequestNotExistentException {
		
		GameArtBalanceResponse response;
		
		String token = getRemoteDataToken(params);
		
		if (token == null) {
			response = new GameArtErrorBalanceResponse();
			response.setStatus(GameArtResponseCodes.INTERNAL_ERROR_STATUS_CODE);
			((GameArtErrorBalanceResponse)response).setMsg(GameArtResponseMessages.INVALID_REMOTE_DATA);
			return response;
		}
		else
			response = new GameArtBalanceResponse();
		
		double amount = Double.valueOf(params.get("amount"));
		long gpiAmount = gamePlatformIntegrationUtils.getAmountFromProvider(amount, BALANCE_COEFFICIENT);

		Message message;

		if (CREDIT_METHOD.equals(params.get("action")) && "REF".equals(params.get("action_type"))) {
			// refund call
			message = gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, "REF-" + params.get("transaction_id"), null, MethodType.REFUND_TRANSACTION);
			MessageHelper.addRefundedTransactionIdToMethod(message, params.get("transaction_id"));
		} else {
			// freespin. bet 0 amount
			if (DEBIT_METHOD.equals(params.get("action")) && BET_FREE_ACTION.equals(params.get("action_type"))) {
				gpiAmount = 0;
			}
			
			message = gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, params.get("transaction_id"), 
					Long.valueOf(params.get("round_id")), DEBIT_METHOD.equals(params.get("action")) ? MethodType.BET : MethodType.WIN);
		}

		Message messageFromGPI = gamePlatformIntegrationService.processMessage(message);
		
		if ((messageFromGPI.getResult().getSuccess() == 1) || messageFromGPI.getResult().getSuccess() == 0 && messageFromGPI.getResult().getReturnset().getTransactionId() != null) {
			// success or transaction already processed
			double balance;
			if (messageFromGPI.getResult().getReturnset().getBalance() != null) {
				balance = gamePlatformIntegrationUtils.getAmountFromGPI(messageFromGPI.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			}
			else {
				// there is no balance in the previous response. do a new call to get the updated balance
				message = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
				messageFromGPI = gamePlatformIntegrationService.processMessage(message);
				balance = gamePlatformIntegrationUtils.getAmountFromGPI(messageFromGPI.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			}
			
			response.setBalance(balance);
			String currentToken = messageFromGPI.getResult().getReturnset().getToken().getValue();
			GameArtRemoteData remoteData = new GameArtRemoteData();
			remoteData.setCurrentToken(currentToken);
			response.setRemoteData(remoteData);
			response.setStatus(GameArtResponseCodes.OK_STATUS_CODE);
		}
		else if (messageFromGPI.getResult().getSuccess() == 0 && DEBIT_METHOD.equals(params.get("action")) 
				&& messageFromGPI.getResult().getReturnset().getErrorCode().getValue() == GameArtResponseCodes.INSUFFICIENT_BALANCE_ERROR_CODE) {
			// insufficient balance error
			response = new GameArtErrorBalanceResponse();
			response.setStatus(GameArtResponseCodes.INSUFFICIENT_FUNDS_STATUS_CODE);
			((GameArtErrorBalanceResponse)response).setMsg(GameArtResponseMessages.INSUFFICIENT_FUNDS_MSG);
			return response;
		}
		else {
			response = new GameArtErrorBalanceResponse();
			response.setStatus(GameArtResponseCodes.INTERNAL_ERROR_STATUS_CODE);
			((GameArtErrorBalanceResponse)response).setMsg(messageFromGPI.getResult().getReturnset().getError().getValue());
			logger.error("Error in response from GPI: " + messageFromGPI.getResult().getReturnset().getError().getValue());
		}
		
		return response;
	}

}
