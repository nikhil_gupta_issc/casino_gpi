package com.gpi.service.gameart;

import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gameart.api.domain.GameArtErrorBalanceResponse;
import com.gameart.api.domain.GameArtRemoteData;
import com.gameart.api.domain.GameArtResponse;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.gpi.impl.GameArtServiceImpl;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.MessageHelper;

/**
 * Message command received from GameArt
 * 
 * @author Alexandre
 *
 */
public abstract class GameArtMessageCommand {
	protected GamePlatformIntegrationService gamePlatformIntegrationService;

	protected GameRequestDao gameRequestDao;
	
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	
	protected static final Logger logger = LoggerFactory.getLogger(GameArtServiceImpl.class);

	protected static final int BALANCE_COEFFICIENT = 100;
	
	protected static final String DEBIT_METHOD = "debit";
	protected static final String CREDIT_METHOD = "credit";
	protected static final String BET_FREE_ACTION = "bet_free";
	
	/**
	 * Handle the message received from Game Art
	 * @param params params sent by Game Art in request
	 * @param queryString original query string sent by Game Art
	 * @return response to be returned to Game Art
	 * @throws GameRequestNotExistentException if there's no associated previous game request
	 */
	public abstract GameArtResponse execute(Map<String, String> params, String queryString) throws GameRequestNotExistentException;
	
	/**
	 * @param params params sent by Game Art in request
	 * @return token extracted from remote_data request field
	 */
	protected String getRemoteDataToken(Map<String, String> params) {
		if (params.containsKey("remote_data")) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				GameArtRemoteData remoteData = mapper.readValue(params.get("remote_data"), GameArtRemoteData.class);
				String token = remoteData.getCurrentToken();
				return token;
			} catch (IOException e) {
				logger.error("Error deserializing remoteData",e);
			}
		}
		
		return null;
	}
	
	/**
	 * @param gamePlatformIntegrationService the gamePlatformIntegrationService to set
	 */
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}

	/**
	 * @param gamePlatformIntegrationUtils the gamePlatformIntegrationUtils to set
	 */
	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}
}
