package com.gpi.service.heliogaming;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.heliogaming.api.ErrorCodes;
import com.heliogaming.api.HeliogamingCoreRequest;
import com.heliogaming.api.HeliogamingCoreResponse;
import com.heliogaming.api.HeliogamingStaggeredPaymentRequest;
import com.heliogaming.api.HeliogamingWinData;

/**
 * Handler for DebitAmount requests from spinmatic
 * 
 * @author Alexandre
 *
 */
public class HeliogamingStaggeredPaymentHandler extends HeliogamingHandler {

	private static final Logger logger = LoggerFactory.getLogger(HeliogamingStaggeredPaymentHandler.class);
	
	@Override
	public HeliogamingCoreResponse handle(HeliogamingCoreRequest request) throws GameRequestNotExistentException {
		
		logger.info("Heliogaming: staggeredPaymentRequest=" + request.toJson());

		HeliogamingStaggeredPaymentRequest staggeredPaymentRequest = (HeliogamingStaggeredPaymentRequest) request;		
		HeliogamingCoreResponse response = null;
		
		HeliogamingWinData staggeredPaymentData = staggeredPaymentRequest.getStaggeredPayment();
		
		String playerInfoKey = this.getPlayerInfoKeyWithSessionKey(staggeredPaymentRequest.getStaggeredPayment().getSessionKey());
		PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
		if (playerInfo != null) {
			String token = playerInfo.getToken();
			
			long gpiAmount = this.gamePlatformIntegrationUtils.getPreciseAmountFromProvider(BigDecimal.valueOf(staggeredPaymentData.getGroup().getTransaction().getAmount()), BALANCE_COEFFICIENT);
			
			Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, MethodType.WIN + "-" + staggeredPaymentData.getGroup().getTransaction().getParticipationID(), staggeredPaymentData.getCouponID(), MethodType.WIN);
			Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
			
			if (this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {				
				response = new HeliogamingCoreResponse();
				response.setSuccess(true);
			} else {
				logger.error("Heliogaming: ErrorCode: " + gpiMessage.getResult().getReturnset().getErrorCode().getValue() + ", ErrorMessage: " + gpiMessage.getResult().getReturnset().getError().getValue());

				response = this.createErrorResponse(gpiMessage, HeliogamingCoreResponse.class);
			}
		}else {
			logger.error("Heliogaming: Session Expired for key " +  playerInfoKey);
			response = this.createErrorResponse(ErrorCodes.INVALID_SESSION_DESC, ErrorCodes.INVALID_SESSION_CODE);
		}
		
		logger.info("Heliogaming: staggeredPaymentResponse=" + response.toJson());
		
		return response;
	}
}
