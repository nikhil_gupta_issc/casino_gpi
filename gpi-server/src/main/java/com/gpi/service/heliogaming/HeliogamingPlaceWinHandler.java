package com.gpi.service.heliogaming;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.heliogaming.api.ErrorCodes;
import com.heliogaming.api.HeliogamingCoreRequest;
import com.heliogaming.api.HeliogamingCoreResponse;
import com.heliogaming.api.HeliogamingPlaceWinRequest;
import com.heliogaming.api.HeliogamingPlaceWinResponse;
import com.heliogaming.api.HeliogamingWinData;

/**
 * Handler for DebitAmount requests from spinmatic
 * 
 * @author Alexandre
 *
 */
public class HeliogamingPlaceWinHandler extends HeliogamingHandler {

	private static final Logger logger = LoggerFactory.getLogger(HeliogamingPlaceWinHandler.class);
	
	@Override
	public HeliogamingCoreResponse handle(HeliogamingCoreRequest request) throws GameRequestNotExistentException {
		
		logger.info("Heliogaming: PlaceWinRequest=" + request.toJson());
		
		HeliogamingPlaceWinRequest placeWinRequest = (HeliogamingPlaceWinRequest) request;
		
		List<Long> failedTransactions = new LinkedList<>();
		int transactionsCount = 0;
		
		for (HeliogamingWinData win : placeWinRequest.getWins()) {
			String playerInfoKey = this.getPlayerInfoKeyWithSessionKey(win.getSessionKey());
			PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
			if (playerInfo != null) {
				String token = playerInfo.getToken();
				
				long gpiAmount = this.gamePlatformIntegrationUtils.getPreciseAmountFromProvider(BigDecimal.valueOf(win.getGroup().getTransaction().getAmount().doubleValue()), BALANCE_COEFFICIENT);
				Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, MethodType.WIN + "-" + win.getGroup().getTransaction().getParticipationID(), win.getCouponID(), MethodType.WIN);
				Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
				if (!this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
					logger.error("Heliogaming: ErrorCode: " + gpiMessage.getResult().getReturnset().getErrorCode().getValue() + ", ErrorMessage: " + gpiMessage.getResult().getReturnset().getError().getValue());
	
					failedTransactions.add(win.getTransactionID());
				}
				
				transactionsCount++;
			}else {
				logger.error("Heliogaming: Session Expired for key " + playerInfoKey);
				failedTransactions.add(win.getTransactionID());
			}
		}
		
		HeliogamingPlaceWinResponse response = new HeliogamingPlaceWinResponse();
		if (failedTransactions.isEmpty()) {
			response.setSuccess(true);
		} else {
			if (failedTransactions.size() == transactionsCount) {
				response = (HeliogamingPlaceWinResponse)this.createErrorResponse(ErrorCodes.GENERIC_ERROR_DESC, ErrorCodes.GENERIC_ERROR_CODE, HeliogamingPlaceWinResponse.class);
			}else {
				response.setSuccess(true);
				response.setFailedTransactionIDs((Long[])failedTransactions.toArray(new Long[failedTransactions.size()]));
			}
		}
		
		logger.info("Heliogaming: PlaceWinResponse=" + response.toJson());
		
		return response;
	}
}
