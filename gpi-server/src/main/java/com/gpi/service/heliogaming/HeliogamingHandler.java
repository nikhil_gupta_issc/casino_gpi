package com.gpi.service.heliogaming;

import java.sql.Timestamp;

import com.gpi.api.Message;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.refund.transaction.RefundedTransactionService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.GPIErrorCodes;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.cache.CacheManager;
import com.heliogaming.api.ErrorCodes;
import com.heliogaming.api.HeliogamingBaseRequest;
import com.heliogaming.api.HeliogamingCoreRequest;
import com.heliogaming.api.HeliogamingCoreResponse;
import com.spinmatic.api.SpinmaticStatus;
import com.spinmatic.api.domain.SpinmaticOkResponse;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponseData;
import com.spinmatic.api.domain.SpinmaticTransactionResponse;
import com.spinmatic.api.domain.SpinmaticTransactionResponseData;

/**
 * Abstract handler to Heliogaming integration
 * 
 * @author Alexandre
 *
 */
public abstract class HeliogamingHandler {
	protected GamePlatformIntegrationService gamePlatformIntegrationService;
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	protected PlayerInfoDao playerInfoDao;
	protected TransactionService transactionService;
	protected RefundedTransactionService refundedTransactionService;
	protected CacheManager cacheManager;
	protected GameRequestDao gameRequestDao;
	protected static final int BALANCE_COEFFICIENT = 100;
	
	protected static final int NOT_ENOUGH_CREDIT_ERROR_CODE = 200;
	
	
	/**
	 * Handles a request from heliogaming
	 * @param request request sent by heliogaming
	 * @return response ready to be returned to heliogaming
	 * @throws GameRequestNotExistentException
	 */
	public abstract HeliogamingCoreResponse handle(HeliogamingCoreRequest request) throws GameRequestNotExistentException;
	
	/**
	 * @param request request from heliogaming
	 * @return error response to be returned to heliogaming
	 */
	protected HeliogamingCoreResponse createErrorResponse(String errorMessage, int errorCode) {
		HeliogamingCoreResponse errorResponse = new HeliogamingCoreResponse();
		errorResponse.setSuccess(false);
		errorResponse.setErrorMsg(errorMessage);
		errorResponse.setErrorCode(errorCode);
		
		return errorResponse;
	}
	
	protected HeliogamingCoreResponse createErrorResponse(String errorMessage, int errorCode, Class<? extends HeliogamingCoreResponse> klass) {
		HeliogamingCoreResponse errorResponse;
		try {
			errorResponse = klass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			errorResponse = new HeliogamingCoreResponse();
		}
		errorResponse.setSuccess(false);
		errorResponse.setErrorMsg(errorMessage);
		errorResponse.setErrorCode(errorCode);
		
		return errorResponse;
	}
	
	protected HeliogamingCoreResponse createErrorResponse(Message gpiResponseMessage, Class<? extends HeliogamingCoreResponse> klass)  {
		HeliogamingCoreResponse errorResponse;
		try {
			errorResponse = klass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			errorResponse = new HeliogamingCoreResponse();
		}
		errorResponse.setSuccess(false);
		if (gpiResponseMessage.getResult().getReturnset().getErrorCode().getValue() == GPIErrorCodes.NOT_ENOUGH_CREDITS) {					
			errorResponse.setErrorMsg(ErrorCodes.INSUFFICIENT_FUNDS_DESC);
			errorResponse.setErrorCode(ErrorCodes.INSUFFICIENT_FUNDS_CODE);
		}else if (gpiResponseMessage.getResult().getReturnset().getErrorCode().getValue() == GPIErrorCodes.INVALID_TOKEN || gpiResponseMessage.getResult().getReturnset().getErrorCode().getValue() == GPIErrorCodes.UNKNOWN_TOKEN) {
			errorResponse.setErrorMsg(ErrorCodes.INVALID_SESSION_DESC);
			errorResponse.setErrorCode(ErrorCodes.INVALID_SESSION_CODE);
		}else {
			errorResponse.setErrorMsg(gpiResponseMessage.getResult().getReturnset().getError().getValue());
			errorResponse.setErrorCode(ErrorCodes.GENERIC_ERROR_CODE);			
		}
		return errorResponse;
	}
	
	/**
	 * @param request request form gpi
	 * @return player info cache key constructed using the sessionid from spinmatic
	 */
	protected String getPlayerInfoKeyWithSessionKey(HeliogamingBaseRequest request)  {
		String sessionKey = !org.apache.commons.lang.StringUtils.isEmpty(request.getSessionKey()) ? 
				request.getSessionKey() : request.getSessionKeyAuthentication().getSessionKey();
		return getPlayerInfoKeyWithSessionKey(sessionKey);
	}

	protected String getPlayerInfoKeyWithSessionKey(String sessionKey)  {
		String username = (String)cacheManager.getAndTouch(getSessionKey(sessionKey), CacheConstants.HELIOGAMING_TOKEN_TTL_KEY);
		return getPlayerInfoKey(username);
	}
	
	protected void storeUsernameBySessionId(String sessionId, String username) {
		this.cacheManager.store(getSessionKey(sessionId), username, CacheConstants.HELIOGAMING_TOKEN_TTL_KEY);
	}
	
	protected void savePlayerInfo(PlayerInfo playerInfo, String token) {
		String username = playerInfo.getUsername();
		if (!token.trim().isEmpty()) {
			playerInfo.setToken(token);
		}
		playerInfoDao.savePlayerInfo(getPlayerInfoKey(username), playerInfo);
	}

	/**
	 * @param key identifier of the player
	 * @return player info identifier in the heliogaming playerinfo cache
	 */
	public static String getPlayerInfoKey(String key) {
		return CacheConstants.HELIOGAMING_PLAYERINFO_KEY  + key;
	}
	
	public static String getSessionKey(String key) {
		return CacheConstants.HELIOGAMING_SESSION_KEY + key;
	}

	public static String getCouponKey(String key) {
		return CacheConstants.HELIOGAMING_COUPON_KEY + key;
	}
	
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	public void setPlayerInfoDao(PlayerInfoDao playerInfoDao) {
		this.playerInfoDao = playerInfoDao;
	}

	/**
	 * @param transactionService the transactionService to set
	 */
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	/**
	 * @param refundedTransactionService the refundedTransactionService to set
	 */
	public void setRefundedTransactionService(RefundedTransactionService refundedTransactionService) {
		this.refundedTransactionService = refundedTransactionService;
	}

	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	} 
	
	
	
}
