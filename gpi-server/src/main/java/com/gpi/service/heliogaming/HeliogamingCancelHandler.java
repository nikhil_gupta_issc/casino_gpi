package com.gpi.service.heliogaming;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.TransformerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.audit.RefundedTransaction;
import com.gpi.domain.audit.Transaction;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.MessageHelper;
import com.heliogaming.api.ErrorCodes;
import com.heliogaming.api.HeliogamingCancelBetRequest;
import com.heliogaming.api.HeliogamingCancelResponse;
import com.heliogaming.api.HeliogamingCancelWinRequest;
import com.heliogaming.api.HeliogamingCoreRequest;
import com.heliogaming.api.HeliogamingCoreResponse;
import com.heliogaming.api.HeliogamingPlaceWinResponse;
import com.heliogaming.api.HeliogamingWinData;

/**
 * Handler for DebitAmount requests from spinmatic
 * 
 * @author Alexandre
 *
 */
public class HeliogamingCancelHandler extends HeliogamingHandler {

	private static final Logger logger = LoggerFactory.getLogger(HeliogamingCancelHandler.class);

	@Override
	public HeliogamingCoreResponse handle(HeliogamingCoreRequest request) throws GameRequestNotExistentException {

		logger.info("Heliogaming: CancelRequest=" + request.toJson());
		
		HeliogamingCoreResponse response = null;
		
		response = cancelByGroupParticipation(request);
		
		logger.info("Heliogaming: CancelResponse=" + response.toJson());
		return response;
	}

	private HeliogamingCoreResponse cancelByGroupParticipation(HeliogamingCoreRequest request) throws GameRequestNotExistentException{

		List<Long> failedTransactions = new LinkedList<>();
		
		int transactionsCount = 0;
		MethodType methodType = null;
		HeliogamingWinData[] cancelledTransactions;
		if (request instanceof HeliogamingCancelBetRequest) {
			cancelledTransactions = ((HeliogamingCancelBetRequest) request).getCancelledBets();
			methodType = MethodType.BET;
		} else {
			cancelledTransactions = ((HeliogamingCancelWinRequest) request).getCancelledWins();
			methodType = MethodType.WIN;
		}
		
		Map<Long, List<HeliogamingWinData>> txByGroupPart = new HashMap<Long, List<HeliogamingWinData>>();
		for (HeliogamingWinData cancelledTransaction : cancelledTransactions){
			if (!txByGroupPart.containsKey(cancelledTransaction.getGroup().getParticipationID())){
				txByGroupPart.put(cancelledTransaction.getGroup().getParticipationID(), new ArrayList<HeliogamingWinData>());
			}
			txByGroupPart.get(cancelledTransaction.getGroup().getParticipationID()).add(cancelledTransaction);
			transactionsCount++;
		}
		
		for (Long groupParticipationid : txByGroupPart.keySet()){
			Double amount = 0D;
			for (HeliogamingWinData cancelTransaction : txByGroupPart.get(groupParticipationid)){
				amount = amount + cancelTransaction.getAmount();
			}
			
			String playerInfoKey = this.getPlayerInfoKeyWithSessionKey(txByGroupPart.get(groupParticipationid).get(0).getSessionKey()); //this.getPlayerInfoKeyWithCouponId(txByGroupPart.get(groupParticipationid).get(0).getCouponID());
			PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
			
			if (playerInfo != null) {
				
				String token = playerInfo.getToken();
				GameRequest gameRequest = null;
				
				try {
					gameRequest = gameRequestDao.getGameRequest(token);
					
					Transaction transactionToRefund = transactionService.getTransaction(methodType + "-" + txByGroupPart.get(groupParticipationid).get(0).getGroup().getParticipationID(), gameRequest.getGame().getName());
					
					if (transactionToRefund != null) {
	
						RefundedTransaction refundedTransaction = refundedTransactionService.getRefundedTransaction("REF-" + methodType + "-" + txByGroupPart.get(groupParticipationid).get(0).getGroup().getParticipationID(), transactionToRefund);
						
						if (refundedTransaction == null) {
							
								long gpiAmount = this.gamePlatformIntegrationUtils.getPreciseAmountFromProvider(BigDecimal.valueOf(amount), BALANCE_COEFFICIENT);
								Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, "REF-" + methodType + "-" + txByGroupPart.get(groupParticipationid).get(0).getGroup().getParticipationID(), txByGroupPart.get(groupParticipationid).get(0).getCouponID(), MethodType.REFUND_TRANSACTION);
								MessageHelper.addRefundedTransactionIdToMethod(message, methodType + "-" + txByGroupPart.get(groupParticipationid).get(0).getGroup().getParticipationID());
								Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
								if (!this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
									logger.error("ErrorCode: " + gpiMessage.getResult().getReturnset().getErrorCode().getValue() + ", ErrorMessage: " + gpiMessage.getResult().getReturnset().getError().getValue());
									failedTransactions.addAll((List<Long>)CollectionUtils.collect(txByGroupPart.get(groupParticipationid), TransformerUtils.invokerTransformer("getTransactionID")));
								}
						}else{
							if (!refundedTransaction.getTransaction().isSuccess()){
								failedTransactions.addAll((List<Long>)CollectionUtils.collect(txByGroupPart.get(groupParticipationid), TransformerUtils.invokerTransformer("getTransactionID")));
							}
						}
					}else {
						logger.error("Heliogaming: No transaction found for ParticipationId=" + groupParticipationid);
						failedTransactions.addAll((List<Long>)CollectionUtils.collect(txByGroupPart.get(groupParticipationid), TransformerUtils.invokerTransformer("getTransactionID")));
					}

				} catch (GameRequestNotExistentException e1) {
					logger.error("Heliogaming: Session Expired for token " + token);
					failedTransactions.addAll((List<Long>)CollectionUtils.collect(txByGroupPart.get(groupParticipationid), TransformerUtils.invokerTransformer("getTransactionID")));
				}
			}else {
				logger.error("Heliogaming: Session Expired for playerInfoKey " + playerInfoKey);
				failedTransactions.addAll((List<Long>)CollectionUtils.collect(txByGroupPart.get(groupParticipationid), TransformerUtils.invokerTransformer("getTransactionID")));
			}			
			
		}
		
		HeliogamingCoreResponse response = new HeliogamingCancelResponse();
		if (failedTransactions.isEmpty()) {
			response.setSuccess(true);
		}else {
			if (failedTransactions.size() == transactionsCount) {
				response = (HeliogamingPlaceWinResponse)this.createErrorResponse(ErrorCodes.GENERIC_ERROR_DESC, ErrorCodes.GENERIC_ERROR_CODE, HeliogamingPlaceWinResponse.class);
			}else {
				response.setSuccess(true);
				((HeliogamingPlaceWinResponse)response).setFailedTransactionIDs((Long[])failedTransactions.toArray(new Long[failedTransactions.size()]));				
			}
		}
		
		return response;
		
	}
	
}
