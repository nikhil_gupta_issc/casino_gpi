package com.gpi.service.heliogaming;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.heliogaming.api.ErrorCodes;
import com.heliogaming.api.HeliogamingBalanceData;
import com.heliogaming.api.HeliogamingBaseRequest;
import com.heliogaming.api.HeliogamingCoreRequest;
import com.heliogaming.api.HeliogamingCoreResponse;
import com.heliogaming.api.HeliogamingLoginResponse;
import com.heliogaming.api.HeliogamingPlayerData;

/**
 * Handler for GetPlayerInfo/GetBalance requests from spinmatic
 * 
 * @author Alexandre
 *
 */
public class HeliogamingLoginGetBalanceHandler extends HeliogamingHandler {

	private static final Logger logger = LoggerFactory.getLogger(HeliogamingLoginGetBalanceHandler.class);
	
	@Override
	public HeliogamingCoreResponse handle(HeliogamingCoreRequest request) throws GameRequestNotExistentException {
		
		logger.info("Heliogaming: LoginRequest=" + request.toJson());

		HeliogamingCoreResponse response = null;
		
		String playerInfoKey = this.getPlayerInfoKeyWithSessionKey((HeliogamingBaseRequest)request);
		PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
		if (playerInfo != null) {
			String token = playerInfo.getToken();
			
			Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
			Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
			
			if (this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
				long gpiBalance = gpiMessage.getResult().getReturnset().getBalance().getValue();
				Double balance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT).doubleValue();
				String currency = playerInfo.getCurrency();
				
				response = new HeliogamingLoginResponse();
				response.setSuccess(true);
				
				HeliogamingPlayerData playerData = new HeliogamingPlayerData();
				playerData.setCurrencyCode(currency);
				playerData.setPlayerExtID(playerInfo.getUsername());
				((HeliogamingLoginResponse)response).setPlayer(playerData);
				
				HeliogamingBalanceData balanceData = new HeliogamingBalanceData();
				balanceData.setBalance(balance);
				((HeliogamingLoginResponse)response).setBalance(balanceData);
	
				return response;
			} else {
				logger.error("ErrorCode: " + gpiMessage.getResult().getReturnset().getErrorCode().getValue() + ", ErrorMessage: " + gpiMessage.getResult().getReturnset().getError().getValue());
				response = this.createErrorResponse(gpiMessage.getResult().getReturnset().getError().getValue(), ErrorCodes.GENERIC_ERROR_CODE);
			}
		}else {
			logger.error(ErrorCodes.INVALID_SESSION_CODE + " " + ErrorCodes.INVALID_SESSION_DESC);
			response = this.createErrorResponse(ErrorCodes.INVALID_SESSION_DESC,  ErrorCodes.INVALID_SESSION_CODE);
		}
		
		logger.info("Heliogaming: LoginResponse=" + response.toJson());
		return response;
		
	}

}
