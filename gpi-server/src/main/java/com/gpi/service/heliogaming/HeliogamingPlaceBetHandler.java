package com.gpi.service.heliogaming;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.heliogaming.api.ErrorCodes;
import com.heliogaming.api.HeliogamingBalanceData;
import com.heliogaming.api.HeliogamingBaseRequest;
import com.heliogaming.api.HeliogamingBetGroupData;
import com.heliogaming.api.HeliogamingCoreRequest;
import com.heliogaming.api.HeliogamingCoreResponse;
import com.heliogaming.api.HeliogamingPlaceBetRequest;
import com.heliogaming.api.HeliogamingPlaceBetResponse;

/**
 * Handler for DebitAmount requests from spinmatic
 * 
 * @author Alexandre
 *
 */
public class HeliogamingPlaceBetHandler extends HeliogamingHandler {

	private static final Logger logger = LoggerFactory.getLogger(HeliogamingPlaceBetHandler.class);
	
	@Override
	public HeliogamingCoreResponse handle(HeliogamingCoreRequest request) throws GameRequestNotExistentException {
		
		logger.info("Heliogaming: PlaceBetRequest=" + request.toJson());
		
		HeliogamingPlaceBetRequest placeBetRequest = (HeliogamingPlaceBetRequest) request;
		HeliogamingCoreResponse response = null;
		
		String playerInfoKey = this.getPlayerInfoKeyWithSessionKey((HeliogamingBaseRequest)request);
		PlayerInfo playerInfo = playerInfoDao.findPlayerInfo(playerInfoKey);
		if (playerInfo != null) {
			
			String token = playerInfo.getToken();
			Double balance = null;
			
			logger.info("Heliogaming: playerInfo: {token: '" + token + "'}");
			
			for (HeliogamingBetGroupData groupData : placeBetRequest.getBet().getGroups()) {
				
				long gpiAmount = this.gamePlatformIntegrationUtils.getPreciseAmountFromProvider(BigDecimal.valueOf(groupData.getAmount()), BALANCE_COEFFICIENT);
				
				Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, MethodType.BET + "-" + groupData.getParticipationID(), placeBetRequest.getBet().getCouponID(), MethodType.BET);
				Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
				
				if (this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
					if (this.gamePlatformIntegrationUtils.messageIsAlreadyProcessed(gpiMessage)) {
						message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
						gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
					}
					
					long gpiBalance = gpiMessage.getResult().getReturnset().getBalance().getValue();
					
					balance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT).doubleValue();
					
				}else {
					logger.error("Heliogaming: ErrorCode: " + gpiMessage.getResult().getReturnset().getErrorCode().getValue() + ", ErrorMessage: " + gpiMessage.getResult().getReturnset().getError().getValue());

					response = createErrorResponse(gpiMessage, HeliogamingCoreResponse.class);
					logger.info("Heliogaming: PlaceBetResponse=" + response.toJson());
					return response;
				}
				
			}

			response = new HeliogamingPlaceBetResponse();
			HeliogamingBalanceData balanceData = new HeliogamingBalanceData();
			balanceData.setBalance(balance);
			((HeliogamingPlaceBetResponse)response).setBalance(balanceData);
			response.setSuccess(true);
			
		}else {
			logger.error("Heliogaming: Session Expired for key " + playerInfoKey);
			response = this.createErrorResponse(ErrorCodes.INVALID_SESSION_DESC, ErrorCodes.INVALID_SESSION_CODE);
		}		
		
		logger.info("Heliogaming: PlaceBetResponse=" + response.toJson());
		return response;
	}

}
