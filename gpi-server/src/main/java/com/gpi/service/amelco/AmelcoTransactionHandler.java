package com.gpi.service.amelco;

import java.math.BigDecimal;

import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.URIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amelco.api.ApiError;
import com.amelco.api.Funds;
import com.amelco.api.TransactionType;
import com.amelco.api.TxnReq;
import com.amelco.api.TxnResp;
import com.amelco.api.domain.AmelcoAccountDetails;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.MessageHelper;

public class AmelcoTransactionHandler extends AmelcoAbstractHandler  {

	private static String RELATIVE_URL = "/game/txn";
	
	private static final Logger logger = LoggerFactory.getLogger(AmelcoTransactionHandler.class);

	@Override
	public Message execute(Publisher publisher, Message message) throws GameRequestNotExistentException {

		Message resultMessage = null;

		try {
			this.postMethod.setURI(new URI(constructEndpointURL(this.baseURL, RELATIVE_URL), false));

			String token = message.getMethod().getParams().getToken().getValue();

			TxnReq txnBetReq = new TxnReq();
			AmelcoAccountDetails accountDetails = (AmelcoAccountDetails)amelcoCacheManager.get(getTokenCacheKey(token));

			txnBetReq.setAmount(convertToAmelcoAmount(message.getMethod().getParams().getAmount().getValue()).setScale(2));
			txnBetReq.setAccountId(accountDetails.getAccountId());
			txnBetReq.setExtGameId("WAC");
			txnBetReq.setExtTxnId(String.valueOf(message.getMethod().getParams().getTransactionId().getValue()));
			txnBetReq.setExtRoundId(String.valueOf(message.getMethod().getParams().getRoundId().getValue()));
			txnBetReq.setCurrencyCode(accountDetails.getCurrencyCode());
			txnBetReq.setAccountToken(token);
			txnBetReq.setExtPlayId(txnBetReq.getExtTxnId());
			txnBetReq.setReconciliation(false);
			txnBetReq.setDescription(message.getMethod().getName() + " " + txnBetReq.getExtTxnId());

			if (MethodType.BET.equals(message.getMethod().getName())) {
				txnBetReq.setType(TransactionType.STAKE);
			} else if (MethodType.WIN.equals(message.getMethod().getName())) {
				txnBetReq.setType(TransactionType.WIN);
			} else if (MethodType.REFUND_TRANSACTION.equals(message.getMethod().getName())) {
				txnBetReq.setType(TransactionType.REFUND);
				txnBetReq.setReconciliation(true);
				txnBetReq.setExtTxnId(String.valueOf(message.getMethod().getParams().getRefundedTransactionId().getValue()));
			}

			Object response = amelcoRequest(publisher.getName(), txnBetReq, TxnResp.class);
			
			if (response instanceof TxnResp) {
				TxnResp txnResp = (TxnResp)response;

				long totalFunds = 0;
				for (Funds fund : txnResp.getWallet().getFunds()) {
					totalFunds += convertToGPIAmount(new BigDecimal(fund.getBalance()));
				}
				
				String amelcoTransactionId = txnResp.getTxnId();
				
				resultMessage = MessageHelper.createResponse(message.getMethod().getName(), token);

				MessageHelper.addBalance(resultMessage, totalFunds);
				MessageHelper.addTransactionIdToReturnset(resultMessage, amelcoTransactionId);
				MessageHelper.setAlreadyProcess(resultMessage, false);
			} else if (response instanceof ApiError) {
				ApiError error = (ApiError)response;
				resultMessage = constructGPIErrorMessage(this.postMethod.getStatusCode(), error, message.getMethod().getName());
			}
		} catch (URIException e) {
			logger.error(e.getMessage());
		} catch (NullPointerException e) {
			logger.error(e.getMessage());
		}

		return resultMessage;
	}
}
