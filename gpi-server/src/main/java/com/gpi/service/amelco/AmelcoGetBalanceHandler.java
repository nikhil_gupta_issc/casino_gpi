package com.gpi.service.amelco;

import java.math.BigDecimal;

import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amelco.api.ApiError;
import com.amelco.api.AuthReq;
import com.amelco.api.AuthResp;
import com.amelco.api.Funds;
import com.amelco.api.domain.AmelcoAccountDetails;
import com.gpi.api.Message;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.MessageHelper;

public class AmelcoGetBalanceHandler extends AmelcoAbstractHandler  {

	private static String RELATIVE_URL = "/account/auth";
	
	private static final Logger logger = LoggerFactory.getLogger(AmelcoGetPlayerInfoHandler.class);

	@Override
	public Message execute(Publisher publisher, Message message) throws GameRequestNotExistentException {
		Message resultMessage = null;

		try {
			this.postMethod.setURI(new URI(constructEndpointURL(this.baseURL, RELATIVE_URL), false));
			String token = message.getMethod().getParams().getToken().getValue();
			
			AuthReq authReq = new AuthReq();
			authReq.setAccountToken(token);
			authReq.setExtGameId("WAC");

			Object response = amelcoRequest(publisher.getName(), authReq, AuthResp.class);
			
			if (response instanceof AuthResp) {
				AuthResp authResp = (AuthResp)response;
				String responseLoginName = authResp.getAccountName();
				String accountToken = authResp.getAccountToken();
				long accountId = authResp.getAccountId();

				long totalFunds = 0;
				for (Funds fund : authResp.getWallet().getFunds()) {
					totalFunds += convertToGPIAmount(new BigDecimal(fund.getBalance()));
				}

				String responseCurrency = StringUtils.upperCase(authResp.getWallet().getFunds().get(0).getCurrencyCode());

				AmelcoAccountDetails accountDetails = new AmelcoAccountDetails(responseLoginName, responseCurrency, accountId);
				amelcoCacheManager.store(getTokenCacheKey(accountToken), accountDetails, CacheConstants.AMELCO_TTL);

				resultMessage = MessageHelper.createResponse(message.getMethod().getName(), accountToken);

				MessageHelper.addBalance(resultMessage, totalFunds);
			} else if (response instanceof ApiError) {
				ApiError error = (ApiError)response;
				resultMessage = constructGPIErrorMessage(this.postMethod.getStatusCode(), error, message.getMethod().getName());
			}
		} catch (URIException e) {
			logger.error(e.getMessage());
		} catch (NullPointerException e) {
			logger.error(e.getMessage());
		}

		return resultMessage;
	}
}
