package com.gpi.service.amelco;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import com.amelco.api.ApiError;
import com.amelco.api.ErrorType;
import com.google.common.net.HttpHeaders;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.MessageHelper;
import com.gpi.utils.cache.CacheManager;

public abstract class AmelcoAbstractHandler implements AmelcoHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(AmelcoAbstractHandler.class);
	
	private static final String TOKEN_HTTP_HEADER = "amelco-games-api-token";
	private static final int BALANCE_COEFFICIENT = 100;
	private HttpConnectionManager connectionManager;
	private long timeout;
	private int connectionTimeout;
	
	protected String baseURL;
	
	protected PostMethod postMethod;
	
	protected CacheManager amelcoCacheManager;
	
	protected GameRequestDao gameRequestDao;
	
	protected Object amelcoRequest(String pn, Object request, Class typeClass) {
		
		String apiTokenKey = ApplicationProperties.getProperty("amelco.key."+pn);
		this.postMethod.setRequestHeader(TOKEN_HTTP_HEADER, apiTokenKey);
		this.postMethod.setRequestHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		
		HttpClient client = new HttpClient(this.connectionManager);
		
		HttpClientParams params = client.getParams();
		params.setConnectionManagerTimeout(timeout);
		params.setSoTimeout(connectionTimeout);

		client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, true));
		
		try {
			ObjectMapper om = new ObjectMapper();
			om.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			String requestMessage = om.writeValueAsString(request);
			
			logger.debug("Sending to endpoint " + this.postMethod.getURI().toString() + " message " + requestMessage);

			StringRequestEntity requestEntity = new StringRequestEntity(
					requestMessage,
					MediaType.APPLICATION_JSON_VALUE,
				    "UTF-8");
			
			this.postMethod.setRequestEntity(requestEntity);
			
			this.postMethod.setRequestHeader("Content-Length", String.valueOf(requestMessage.getBytes("UTF-8").length));
			
			client.executeMethod(this.postMethod);
			
			String resp = this.postMethod.getResponseBodyAsString();
			
			logger.debug("Received response " + resp);
			
			Object amelcoResponse;
			
			if (this.postMethod.getStatusCode() == HttpStatus.SC_OK) {
				amelcoResponse = om.readValue(resp, typeClass);
			} else {
				amelcoResponse = om.readValue(resp, ApiError.class);
			}
			
			return amelcoResponse;
			
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	protected Message constructGPIErrorMessage(int statusCode, ApiError error, MethodType method) {
		int errorCode;
		
		if (statusCode == HttpStatus.SC_FORBIDDEN || 
				ErrorType.PERMISSIONS_ERROR.name().equals(error.getErrType().name())) {
			errorCode = 101;
		} else if (ErrorType.INSUFFICIENT_FUNDS.name().equals(error.getErrType().name())) {
			errorCode = 200;
		}
		else {
			errorCode = 100;
		}
		
		return MessageHelper.createErrorResponse(errorCode, method, error.getDetails());
	}
	
	protected boolean checkSuccessResponse(int statusCode) {
		return HttpStatus.SC_OK == statusCode;
	}
	
	protected long convertToGPIAmount(BigDecimal value) {
		BigDecimal convertedValue = value.multiply(BigDecimal.valueOf(BALANCE_COEFFICIENT));
		return convertedValue.longValue();
	}
	
	protected BigDecimal convertToAmelcoAmount(long value) {
		return BigDecimal.valueOf(value).divide(BigDecimal.valueOf(BALANCE_COEFFICIENT));
	}
	
	protected String getTokenCacheKey(String token) {
		return CacheConstants.AMELCO_TOKEN_KEY + token;
	}
	
	/**
	 * @param baseUrl baseUrl with http://
	 * @param relativeUrl relative url with / before. e.g. /auth
	 * @return constructed endpoint url to be used in amelco requests taking into account the defined version in this class
	 */
	protected String constructEndpointURL(String baseUrl, String relativeUrl) {
		return baseUrl + relativeUrl;
	}
	
	public void setPostMethod(PostMethod postMethod) {
		this.postMethod = postMethod;
	}
	
	public void setBaseURL(String url) {
		this.baseURL = url;
	}
	
	public void setConnectionManager(HttpConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public void setAmelcoCacheManager(CacheManager amelcoCacheManager) {
		this.amelcoCacheManager = amelcoCacheManager;
	}

	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}
	
}
