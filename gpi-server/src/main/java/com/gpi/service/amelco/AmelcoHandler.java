package com.gpi.service.amelco;

import com.gpi.api.Message;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.GameRequestNotExistentException;

public interface AmelcoHandler {
	Message execute(Publisher publisher, Message message) throws GameRequestNotExistentException;
	
	void setBaseURL(String url);
}
