package com.gpi.service.evoplay;

import java.math.BigDecimal;

import com.evoplay.api.EvoplayDataIdentifiers;
import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.MessageHelper;

/**
 * Handler to create messages for the refund callback in evoplay
 * 
 * @author Alexandre
 *
 */
public class EvoplayMessageRefundCallbackHandler extends EvoplayMessageCallbackHandler {

	@Override
	public Message constructGPIMessage(EvoplayCallbackRequest request) throws GameRequestNotExistentException {
		String token = request.getToken();
		String transactionID = request.getCallbackId();
		double evoplayAmount = new BigDecimal(request.getData().get(EvoplayDataIdentifiers.AMOUNT)).doubleValue();
		long gpiAmount = this.gamePlatformIntegrationUtils.getAmountFromProvider(evoplayAmount, EvoplayCallbackProcessor.BALANCE_COEFFICIENT);
		long refundRoundId = Long.valueOf(request.getData().get(EvoplayDataIdentifiers.REFUND_ROUND_ID));
		
		Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, transactionID, refundRoundId, MethodType.REFUND_TRANSACTION);
		
		String transactionIdToRefund = request.getData().get(EvoplayDataIdentifiers.REFUND_CALLBACK_ID);
		MessageHelper.addRefundedTransactionIdToMethod(message, transactionIdToRefund);
		
		return message;
	}

}
