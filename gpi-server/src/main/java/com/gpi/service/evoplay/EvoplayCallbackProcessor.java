package com.gpi.service.evoplay;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.evoplay.api.domain.EvoplayErrorData;
import com.evoplay.api.domain.EvoplayErrorResponse;
import com.evoplay.api.domain.EvoplayOkResponse;
import com.evoplay.api.domain.EvoplayResponse;
import com.evoplay.api.domain.EvoplayResponseData;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.gpi.impl.EvoplayServiceImpl;
import com.gpi.utils.GamePlatformIntegrationUtils;

/**
 * Processor for the evoplay callback requests
 * 
 * @author Alexandre
 *
 */
public class EvoplayCallbackProcessor {
	protected static final int BALANCE_COEFFICIENT = 100;
	
	private GamePlatformIntegrationService gamePlatformIntegrationService;

	private GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	
	private PlayerInfoDao playerInfoDaoCache;
	
	private static final Logger logger = LoggerFactory.getLogger(EvoplayCallbackProcessor.class);
	
	private Map<String, EvoplayMessageCallbackHandler> handlers;
	
	/**
	 * Process a request received from evoplay
	 * @param request request to process
	 * @return response of the processed request
	 * @throws GameRequestNotExistentException
	 */
	public EvoplayResponse processRequest(EvoplayCallbackRequest request) throws GameRequestNotExistentException {
		logger.debug("Received request is " + request);
		String token = request.getToken();
		
		Message message = this.handlers.get(request.getName()).constructGPIMessage(request);
		
		Message messageFromGPI = this.gamePlatformIntegrationService.processMessage(message);
		
		if ((messageFromGPI.getResult().getSuccess() == 1) || messageFromGPI.getResult().getSuccess() == 0 && messageFromGPI.getResult().getReturnset().getTransactionId() != null) {
			// success or transaction already processed
			
			EvoplayOkResponse response = new EvoplayOkResponse();
			response.setStatus("ok");
			
			EvoplayResponseData data = new EvoplayResponseData();
			
			long gpiBalance;
			
			if (messageFromGPI.getResult().getReturnset().getBalance() != null) {
				gpiBalance = messageFromGPI.getResult().getReturnset().getBalance().getValue();
			} else {
				// there is no balance in the previous response. do a new call to get the updated balance
				message = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
				messageFromGPI = gamePlatformIntegrationService.processMessage(message);
				gpiBalance = messageFromGPI.getResult().getReturnset().getBalance().getValue();
			}
			
			double balance = this.gamePlatformIntegrationUtils.getAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT);
			
			data.setBalance(balance);
			
			PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(getPlayerInfoKey(token));
			data.setCurrency(playerInfo.getCurrency());
			response.setData(data);
			
			String gpiToken = messageFromGPI.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(gpiToken, playerInfo);
			
			logger.debug("Response is " + response);
			
			return response;
		} else {
			EvoplayErrorResponse response = new EvoplayErrorResponse();
			EvoplayErrorData errorData = new EvoplayErrorData();
			errorData.setMessage(messageFromGPI.getResult().getReturnset().getError().getValue());
			errorData.setNoRefund(1);
			errorData.setScope("internal");
			
			response.setError(errorData);
			response.setStatus("error");
			
			logger.debug("Response is " + response);
			
			return response;
		}
	}
	
	/**
	 * @param key identifier of the player
	 * @return player info identifier in the evoplay playerinfo cache
	 */
	public static String getPlayerInfoKey(String key) {
		return CacheConstants.EVOPLAY_PLAYERINFO_KEY + key;
	}
	
	/**
	 * Set the player info token and store it in playerinfo cache using its token as key
	 * @param token token to use
	 * @param playerInfo playerinfo object
	 */
	protected void setPlayerInfoTokenAndStore(String token, PlayerInfo playerInfo) {
		if (!token.trim().isEmpty()) {
			playerInfo.setToken(token);
			this.playerInfoDaoCache.savePlayerInfo(getPlayerInfoKey(playerInfo.getUsername()), playerInfo);
		}
	}

	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	public void setPlayerInfoDaoCache(PlayerInfoDao playerInfoDaoCache) {
		this.playerInfoDaoCache = playerInfoDaoCache;
	}

	public void setHandlers(Map<String, EvoplayMessageCallbackHandler> handlers) {
		this.handlers = handlers;
	}
	
}
