package com.gpi.service.evoplay;

import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.exceptions.GameRequestNotExistentException;

/**
 * Handler to create messages for the init callback in evoplay
 * 
 * @author Alexandre
 *
 */
public class EvoplayMessageInitCallbackHandler extends EvoplayMessageCallbackHandler {

	@Override
	public Message constructGPIMessage(EvoplayCallbackRequest request) throws GameRequestNotExistentException {
		String token = request.getToken();
		
		Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
		
		return message;
	}

}
