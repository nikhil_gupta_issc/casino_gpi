package com.gpi.service.evoplay;

import java.math.BigDecimal;

import com.evoplay.api.EvoplayDataIdentifiers;
import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.exceptions.GameRequestNotExistentException;

/**
 * Handler to create messages for the win callback in evoplay
 * 
 * @author Alexandre
 *
 */
public class EvoplayMessageWinCallbackHandler extends EvoplayMessageCallbackHandler {

	@Override
	public Message constructGPIMessage(EvoplayCallbackRequest request) throws GameRequestNotExistentException {
		String token = request.getToken();
		String transactionID = request.getCallbackId();
		Long roundID = Long.valueOf(request.getData().get(EvoplayDataIdentifiers.ROUND_ID));
		double evoplayAmount = new BigDecimal(request.getData().get(EvoplayDataIdentifiers.AMOUNT)).doubleValue();
		long gpiAmount = this.gamePlatformIntegrationUtils.getAmountFromProvider(evoplayAmount, EvoplayCallbackProcessor.BALANCE_COEFFICIENT);
		
		Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, transactionID, roundID, MethodType.WIN);
		
		return message;
	}

}
