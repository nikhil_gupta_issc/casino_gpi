package com.gpi.service.evoplay;

import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.gpi.api.Message;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.GamePlatformIntegrationUtils;

/**
 * Abstract handler to create messages for the callbacks in evoplay
 * 
 * @author Alexandre
 *
 */
public abstract class EvoplayMessageCallbackHandler {
	
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;

	/**
	 * @param request request received from evoplay
	 * @return processed message ready to be sent to gpi
	 * @throws GameRequestNotExistentException
	 */
	public abstract Message constructGPIMessage(EvoplayCallbackRequest request) throws GameRequestNotExistentException;

	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}
}
