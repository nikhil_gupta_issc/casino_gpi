package com.gpi.service.gamerequest;

import com.gpi.api.Message;
import com.gpi.domain.GameRequest;
import com.gpi.domain.game.Game;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.UnknownTokenException;

/**
 * Created by Nacho on 2/21/14.
 */
public interface GameRequestService {

    void registerGameRequest(String token, String publisherToken, Publisher publisher, Game game, String type, String device, int ttl);

    GameRequest getGameRequest(Message message) throws GamePlatformIntegrationException, UnknownTokenException;

    void removeGameRequest(GameRequest gameRequest);
}
