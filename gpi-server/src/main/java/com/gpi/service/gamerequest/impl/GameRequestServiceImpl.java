package com.gpi.service.gamerequest.impl;

import com.gpi.api.GameReference;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.api.Token;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.domain.GameRequest;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.PlayingType;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.UnknownTokenException;
import com.gpi.service.gamerequest.GameRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Nacho on 2/21/14.
 */
public class GameRequestServiceImpl implements GameRequestService {

    private static final Logger logger = LoggerFactory.getLogger(GameRequestServiceImpl.class);
    private GameRequestDao gameRequestDao;

    @Override
    @Transactional
    public void registerGameRequest(String token, String publisherToken, Publisher publisher, Game game, String
            type, String device, int ttl) {
        logger.debug("Creating a new GameRequest with token={}, publisherToken={}, publisher={} and game={}", token, publisherToken,
                publisher.getName(), game.getName());
        GameRequest gameRequest = new GameRequest();
        gameRequest.setToken(token);
        gameRequest.setPublisherToken(publisherToken);
        gameRequest.setPublisher(publisher);
        gameRequest.setGame(game);
        gameRequest.setCreatedOn(new Date());
        gameRequest.setPlayingType(PlayingType.valueOf(type.toUpperCase()));
        gameRequest.setDevice(device);
        gameRequestDao.saveGameRequest(gameRequest, ttl);
    }

    @Override
    public GameRequest getGameRequest(Message message) throws GamePlatformIntegrationException, UnknownTokenException {
        logger.debug("Getting the token from the Message object in order to get the GameRequest");
        Token token = message.getMethod().getParams().getToken();
        GameReference gameReference = message.getMethod().getParams().getGameReference();

        MethodType methodType = message.getMethod().getName();
        if (token == null) {
            throw new UnknownTokenException("Message doesn't have a valid token.");
        } else if (gameReference == null && (!methodType.equals(MethodType.GET_PLAYER_INFO) && !methodType.equals(MethodType.GET_BALANCE))) {
            throw new GamePlatformIntegrationException("Message doesn't have a valid GameReference.");
        }

        GameRequest gameRequest = gameRequestDao.getGameRequest(token.getValue());


        return gameRequest;
    }

    @Override
    public void removeGameRequest(GameRequest gameRequest) {
        logger.debug("Removing GameRequest={}", gameRequest);
        gameRequestDao.removeGameRequest(gameRequest);
    }

    public void setGameRequestDao(GameRequestDao gameRequestDao) {
        this.gameRequestDao = gameRequestDao;
    }
}
