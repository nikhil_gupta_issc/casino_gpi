package com.gpi.service.gpi;

import com.heliogaming.api.HeliogamingCoreRequest;
import com.heliogaming.api.HeliogamingCoreResponse;
import com.spinmatic.api.domain.SpinmaticOpenGameData;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponse;

/**
 * Service for the Heliogaming integration
 * 
 * @author Alexandre
 *
 */
public interface HeliogamingService {

	/**
	 * @param request request received from heliogaming to be processed
	 * @return response to return to heliogaming
	 */
	HeliogamingCoreResponse processRequest(HeliogamingCoreRequest request);
	
	/**
	 * @param idGame id of the game to get url
	 * @param username player username
	 * @param currency player currency
	 * @param idPlayer player id in gpi
	 * @return url to open a game
	 */
	//SpinmaticOpenGameData getOpenGameData(int idGame, String username, String currency, long idPlayer);
	
}
