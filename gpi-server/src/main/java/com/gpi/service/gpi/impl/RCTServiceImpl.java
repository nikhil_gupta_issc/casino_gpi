package com.gpi.service.gpi.impl;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.dao.rct.RCTDao;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.game.PlayingType;
import com.gpi.domain.publisher.Publisher;
import com.gpi.domain.rct.*;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.RCTLoadGameDto;
import com.gpi.exceptions.GameUrlNotDefinedException;
import com.gpi.exceptions.MarshalException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.MessageHelper;
import com.rct.api.domain.GetBalance;
import com.rct.api.domain.OpenSession;
import com.rct.api.domain.PayBet;
import com.rct.api.domain.PayWin;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class RCTServiceImpl extends GamePlatformIntegrationServiceImpl {

    private static final Logger logger = LoggerFactory.getLogger(RCTServiceImpl.class);
    private final String allwayspayingPass = ApplicationProperties.getProperty("rct.allwaysplayingPass");
    private RCTDao rctDao;

    @Override
    public LoadGameDto loadGame(String publisherToken, String pn, Game game, String type, boolean lobby, String device, int ttl) throws NonExistentPublisherException, NonExistentGameException, GameUrlNotDefinedException {
        logger.debug("Calling loadGame publisherToken={}, pn={}, game={}, type={} and lobby={}", publisherToken, pn, game, type, lobby);
        LoadGameDto loadGameDto = super.loadGame(publisherToken, pn, game, type, lobby, device, ttl);

        RCTLoadGameDto rctLoadGameDto = new RCTLoadGameDto(loadGameDto);
        Publisher publisher = publisherService.findByName(pn);
        logger.info("type" + type);
        logger.info("equals:" + PlayingType.FREE.getType().equals(type));
        if (PlayingType.FREE.getType().equalsIgnoreCase(type)) {
        	try {
				String token = "free"+UUID.randomUUID().toString();
				rctLoadGameDto.setLoginName(token);
				rctLoadGameDto.setBalance(400000L);
				rctLoadGameDto.setCurrency("BRL");
				String key = getKey(game.getGameProvider().getPassword(), token);
				rctLoadGameDto.setKey(key);
				rctLoadGameDto.setToken(token);
				RCTSession rctSession = new RCTSession(rctLoadGameDto, game, publisher);
				rctDao.saveRCTSession(rctSession);
			} catch (NoSuchAlgorithmException e) {
				logger.error("error en free", e);
			}
        } else {
        	MessageHelper messageHelper = new MessageHelper(game.getGameProvider().getLoginName(), game.getGameProvider().getPassword()
                    , MethodType.GET_PLAYER_INFO, loadGameDto.getToken());
            Message message = messageHelper.getMessage();
            try {
                logger.debug("LoadGame successfully called. Now sending getPlayerInfo message to publisher.");
          
                Message messageFromPublisher = processMessage(message);
               
                rctLoadGameDto.setLoginName(messageFromPublisher.getResult().getReturnset().getLoginName().getValue());
                rctLoadGameDto.setToken(messageFromPublisher.getResult().getReturnset().getToken().getValue());
                String key = getKey(game.getGameProvider().getPassword(), messageFromPublisher.getResult().getReturnset().getToken().getValue());
                rctLoadGameDto.setKey(key);
                rctLoadGameDto.setCurrency(messageFromPublisher.getResult().getReturnset().getCurrency().getValue());
                rctLoadGameDto.setBalance(messageFromPublisher.getResult().getReturnset().getBalance().getValue());
                RCTSession rctSession = new RCTSession(rctLoadGameDto, game, publisher);
                rctDao.saveRCTSession(rctSession);
            } catch (NoSuchAlgorithmException e) {
                logger.error("Error", e);
            }
        }
        
        return rctLoadGameDto;
    }

    private String getKey(Object... args) throws NoSuchAlgorithmException {
        return getKey('.', args);
    }

    private String getKey(Character separator, Object... args) throws NoSuchAlgorithmException {
        logger.debug("getKey with separator={} and args={}", separator, args);
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");

        String key = StringUtils.join(args, (separator == null) ? "" : separator.toString());
        byte[] digest = messageDigest.digest(key.getBytes());
        return new String(Hex.encodeHex(digest));
    }

    @Override
    public void setProviderSpecificParams(Map parameterMap, LoadGameDto loadGameDto, UriComponentsBuilder builder) throws NonExistentPublisherException {
        logger.debug("Setting setProviderSpecificParams...");
        RCTLoadGameDto rctLoadGameDto = (RCTLoadGameDto) loadGameDto;
        builder.queryParam("login", rctLoadGameDto.getLoginName());
        String[] lang = (String[]) parameterMap.get("lang");
        builder.queryParam("language", String.format("%s-%s", lang[0], lang[0]));
        builder.queryParam("session", rctLoadGameDto.getToken());
        String[] type = (String[]) parameterMap.get("type");
        builder.queryParam("type", type != null ? type[0] : PlayingType.CHARGED.getType());
    }


    public OpenSession confirmOpenSession(String token, Long gameId, String key) throws NoSuchAlgorithmException {
        logger.debug("confirmOpenSession with token={}, key={} and gameId={}", token, key, gameId);
        OpenSession openSession = new OpenSession();
        if (!getKey(null, allwayspayingPass, token).equals(key)) {
            logger.warn("Key does not match. Received={} - expected={}", key, getKey(allwayspayingPass, token));
            openSession.setError("1");
        } else {
            RCTSession rctSession = rctDao.findRCTSession(token);
           
            	openSession.setProcessed(Boolean.TRUE.toString());
            	openSession.setError("");
            	openSession.setUser(rctSession.getLoginName());
            	openSession.setKey(getKey(null, rctSession.getGame().getGameProvider().getPassword(), token));
            	openSession.setCurrency(rctSession.getCurrency());
            	openSession.setBalance(rctSession.getBalance().toString());
            	openSession.setToken(token);
           
        }

        return openSession;
    }

    public GetBalance getBalance(String token) {
        //TODO define key
        RCTSession rctSession = rctDao.findRCTSession(token);
        Message getBalanceMessage = getMessage(token, null, null, rctSession, MethodType.GET_BALANCE);
        String getBalanceResponse;
        GetBalance getBalance = new GetBalance();

        Message getBalanceResponseMessage = processMessage(getBalanceMessage);
        getBalance.setCurrentBalance(getBalanceResponseMessage.getResult().getReturnset().getBalance().getValue());
        getBalance.setError("");
        getBalance.setKey(rctSession.getKey());
        getBalance.setProcessed(Boolean.TRUE.toString());
        getBalance.setTimestamp(new Date().getTime());
        getBalance.setTransactionId(rctSession.getTransactionId());
        rctDao.saveRCTSession(rctSession);
       
        return getBalance;
    }

    public PayBet payBet(String token, Long amount, Long transactionId, String key) throws NoSuchAlgorithmException {

        RCTSession rctSession = rctDao.findRCTSession(token);
        PayBet payBet = new PayBet();
        if (rctSession == null) {
            payBet.setError("5");
            return payBet;
        }
        if (!getKey(allwayspayingPass, rctSession.getLoginName(), amount).equals(key)) {
            logger.warn("Key does not match. Received={} - expected={}", key, getKey(allwayspayingPass, rctSession.getLoginName(), amount));
            payBet.setError("1");
        } else {
            Message betMessage = getMessage(token, amount, transactionId, rctSession, MethodType.BET);
    
            Message betMessageResponse = processMessage(betMessage);
            if (betMessageResponse.getResult().getSuccess() == 0) {
            	payBet = new PayBet();
                payBet.setError(betMessageResponse.getResult().getReturnset().getError().getValue());
            } else {
            	 payBet.setNewBalance(betMessageResponse.getResult().getReturnset().getBalance().getValue());
                 payBet.setError("");
                 String key1 = getKey(rctSession.getGame().getGameProvider().getPassword(), transactionId);
                 payBet.setKey(key1);
                 payBet.setProcessed(Boolean.TRUE.toString());
                 payBet.setTimestamp(new Date().getTime());
                 payBet.setTransactionId(Long.valueOf(betMessageResponse.getResult().getReturnset().getTransactionId().getValue()));
                 rctSession.setKey(key1);
                 rctDao.saveRCTSession(rctSession);
            }
          
        }
        return payBet;
    }

    public PayWin payWin(String token, Long amount, Long transactionId, String key) throws NoSuchAlgorithmException {
        PayWin payWin = new PayWin();

        RCTSession rctSession = rctDao.findRCTSession(token);
        if (rctSession == null) {
            payWin.setError("5");
            return payWin;
        }
        if (!getKey(allwayspayingPass, rctSession.getLoginName(), amount).equals(key)) {
            logger.warn("Key does not match. Received={} - expected={}", key, getKey(allwayspayingPass, rctSession.getLoginName(), amount));
            payWin.setError("1");
        } else {
            Message winMessage = getMessage(token, amount, transactionId, rctSession, MethodType.WIN);
            String winMessageResponseString;

            
        	Message winMessageResponse  = processMessage(winMessage);
            payWin.setNewBalance(winMessageResponse.getResult().getReturnset().getBalance().getValue());
            payWin.setError("");
            String key1 = getKey(rctSession.getGame().getGameProvider().getPassword(), transactionId);
            payWin.setKey(key1);
            payWin.setProcessed(Boolean.TRUE.toString());
            payWin.setTimestamp(new Date().getTime());
            payWin.setTransactionId(Long.valueOf(winMessageResponse.getResult().getReturnset().getTransactionId().getValue()));
            rctSession.setKey(key1);
            rctDao.saveRCTSession(rctSession);
            
        }
        return payWin;
    }

    private Message getMessage(String token, Long amount, Long transactionId, RCTSession rctSession, MethodType methodType) {
        GameProvider provider = rctSession.getGame().getGameProvider();
        MessageHelper messageHelper = new MessageHelper(provider.getLoginName(), provider.getPassword(),
                methodType, token);

        Message betMessage = messageHelper.getMessage();
        if (amount != null) {
            MessageHelper.addAmountToMethod(betMessage, amount);
        }

        if (transactionId != null) {
            MessageHelper.addTransactionIdToMethod(betMessage, String.valueOf(transactionId));
        }
        MessageHelper.addGameReferenceToMethod(betMessage, rctSession.getGame().getName());
        MessageHelper.addRoundIdToMethod(betMessage, rctSession.getRoundId());
        return betMessage;
    }

    public void setRctDao(RCTDao rctDao) {
        this.rctDao = rctDao;
    }

}
