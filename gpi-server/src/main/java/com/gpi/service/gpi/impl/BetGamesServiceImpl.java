package com.gpi.service.gpi.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bet.games.api.BetGamesErrorCodes;
import com.bet.games.api.BetGamesErrorMessages;
import com.bet.games.api.BetGamesMethodType;
import com.bet.games.api.Root;
import com.gpi.api.Message;
import com.gpi.api.Params;
import com.gpi.api.Token;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.betgames.BetGamesMessageCommand;
import com.gpi.service.gpi.BetGamesService;
import com.gpi.service.gpi.validation.BetGamesMessageValidator;
import com.gpi.utils.cache.CacheManager;

/**
 * Default implementation for the BetGames service
 * 
 * @author Alexandre
 *
 */
public class BetGamesServiceImpl implements BetGamesService {

	private static final Logger logger = LoggerFactory.getLogger(BetGamesServiceImpl.class);

	private BetGamesMessageValidator messageValidator;

	private Map<String, BetGamesMessageCommand> commands;

	@Override
	public Root processMessage(Root messageFromGame) throws GameRequestNotExistentException {

		Root returnMessage = new Root();

		returnMessage.setMethod(messageFromGame.getMethod());
		returnMessage.setToken(messageFromGame.getToken());
		returnMessage.setSuccess(BigInteger.valueOf(0));
		returnMessage.setErrorCode(BigInteger.valueOf(0));
		returnMessage.setErrorText("");

		if (messageValidator.validateRequestMessage(messageFromGame, returnMessage)) {
			String method = messageFromGame.getMethod();

			try {
				commands.get(method).execute(messageFromGame, returnMessage, method);
			} catch (Exception ex) {
				logger.error("General Exception", ex);
				returnMessage.setErrorCode(BigInteger.valueOf(BetGamesErrorCodes.INTERNAL_ERROR));
				returnMessage.setErrorText(BetGamesErrorMessages.INTERNAL_MESSAGE);
			}
		}

		returnMessage.setTime(new Date().getTime() / 1000);
		returnMessage.setSignature(messageValidator.getSignatureHash(returnMessage, BetGamesMethodType.RESPONSE));

		return returnMessage;
	}

	/**
	 * @param messageValidator
	 *            the messageValidator to set
	 */
	public void setMessageValidator(BetGamesMessageValidator messageValidator) {
		this.messageValidator = messageValidator;
	}

	/**
	 * @param commands
	 *            the commands to set
	 */
	public void setCommands(Map<String, BetGamesMessageCommand> commands) {
		this.commands = commands;
	}
}