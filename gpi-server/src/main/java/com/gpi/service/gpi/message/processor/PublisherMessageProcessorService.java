package com.gpi.service.gpi.message.processor;

import com.gpi.api.Message;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.NoFreeSpinLeftException;

public interface PublisherMessageProcessorService {

    Message processMessage(Message messageFromGame, Message publisherMessage, GameRequest gameRequest, int ttl) throws GamePlatformIntegrationException, NoFreeSpinLeftException;

}
