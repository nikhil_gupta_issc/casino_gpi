package com.gpi.service.gpi.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.InvalidCredentialsException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.service.gpi.WazdanService;
import com.gpi.service.gpi.validation.WazdanMessageValidator;
import com.gpi.service.wazdan.WazdanMessageCommand;
import com.wazdan.api.WazdanMethodType;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanRequestHeader;
import com.wazdan.domain.WazdanResponse;

public class WazdanServiceImpl implements WazdanService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(WazdanServiceImpl.class);
	
	private WazdanMessageValidator messageValidator;
	
	private Map<String, WazdanMessageCommand> commands;
	
	@Override
	public WazdanResponse processRequest(WazdanRequestHeader requestHeader, WazdanRequest request, WazdanMethodType method) throws GameRequestNotExistentException, InvalidCredentialsException, TransactionNotSuccessException  {
		if (messageValidator.validateRequestMessage(requestHeader)){
			return commands.get(method.getMethodName()).execute(request);
		}
		
		throw new InvalidCredentialsException("Invalid Credentials");
	}

	/**
	 * @param commands the commands to set
	 */
	public void setCommands(Map<String, WazdanMessageCommand> commands) {
		this.commands = commands;
	}

	public void setMessageValidator(WazdanMessageValidator messageValidator) {
		this.messageValidator = messageValidator;
	}	

}
