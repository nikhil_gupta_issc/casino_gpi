package com.gpi.service.gpi;

import java.util.Map;

import com.gameart.api.domain.GameArtBalanceResponse;
import com.gameart.api.domain.GameArtResponse;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;

/**
 * Service for the Game Art integration
 * 
 * @author Alexandre
 *
 */
public interface GameArtService {

	/**
	 * Process a request received by Game Art
	 * @param params query string params
	 * @param original query string received in request
	 * @return response after the service processing
	 */
	GameArtResponse processRequest(Map<String, String> params, String queryString);
	
	/**
	 * @param idGame id of the game to get url
	 * @param remoteId remote id
	 * @param token token
	 * @param currency used currency
	 * @param deomMode if the game mode is demo
	 * @param homeURL url to be used in home parameter
	 * @return url to open a game
	 */
	String getGameURL(int idGame, String remoteId, String token, String currency, boolean demodMode, String homeURL);
	
}
