package com.gpi.service.gpi;

import com.gpi.api.Message;
import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.exceptions.GameUrlNotDefinedException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

/**
 * Created by Nacho on 2/18/14.
 */
public interface GamePlatformIntegrationService {

    /**
     * This method is responsible to manage every xml received from games. Is responsible to send the message to the publisher,
     * receive the response and return this response to the game. If an error occurs the xml must contain a the Message object who represents
     * this error.
     *
     * @param xml String representation of a Message object
     * @return Always returns an well formed xml representing a Message object.
     */
    Message processMessage(Message messageFromGame);

    /**
     * When a game is called this method stores the needed information for future request.
     *
     * @param publisherToken token generated by the publisher
     * @param pn             The publisher name
     * @param game           The game being called
     * @param type
     * @param lobby         Boolean indicating if the request is for a lobby or not
     * @param device        The kind of device who starts the game (desktop, mobile)
     * @param ttl			the time to live of the game request
     * @return The generated token that must be sent to the game provider.
     */
    LoadGameDto loadGame(String publisherToken, String pn, Game game, String type, boolean lobby, String device, int ttl) throws NonExistentPublisherException, NonExistentGameException, GameUrlNotDefinedException;

    void setProviderSpecificParams(Map parameterMap, LoadGameDto loadGameDto, UriComponentsBuilder builder) throws NonExistentPublisherException;
}
