package com.gpi.service.gpi.impl;

import com.gpi.api.AlreadyProcessed;
import com.gpi.api.Balance;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.api.Params;
import com.gpi.api.Result;
import com.gpi.api.Returnset;
import com.gpi.api.Token;
import com.gpi.api.TransactionId;
import com.gpi.communication.CommunicationHandlerSelector;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.GameRequest;
import com.gpi.domain.audit.Round;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.PlayingType;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.LoadGameDto;
import com.gpi.exceptions.*;
import com.gpi.service.gamerequest.GameRequestService;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.gpi.message.processor.GameMessageProcessorService;
import com.gpi.service.gpi.message.processor.PublisherMessageProcessorService;
import com.gpi.service.publisher.PublisherService;
import com.gpi.service.round.RoundService;
import com.gpi.service.token.TokenService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.MessageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

public class GamePlatformIntegrationServiceImpl implements GamePlatformIntegrationService {

	private static final Logger logger = LoggerFactory.getLogger(GamePlatformIntegrationServiceImpl.class);
	PublisherService publisherService;
	private GameRequestService gameRequestService;
	private RoundService roundService;
	private CommunicationHandlerSelector communicationHandlerSelector;
	private TransactionService transactionService;
	private GameMessageProcessorService gameMessageProcessorService;
	private PublisherMessageProcessorService publisherMessageProcessorService;

	private GamePlatformIntegrationUtils gamePlatformIntegrationUtils;

	@Override
	public LoadGameDto loadGame(String publisherToken, String pn, Game game, String type, boolean lobby, String device, int ttl) throws NonExistentPublisherException, NonExistentGameException, GameUrlNotDefinedException {
		logger.debug("Loading game with token={}, pn={}, game={} and type={}", publisherToken, pn, game.getName(), type);
		Publisher publisher = publisherService.findByName(pn);
		if (game.getUrl() == null) {
			throw new GameUrlNotDefinedException("Game \"" + game.getName() + "\" must have an url.");
		}
		String token = TokenService.generateToken();
		logger.debug("New token generated={}", token);
		gameRequestService.registerGameRequest(token, publisherToken, publisher, game, type, device, ttl);

		return new LoadGameDto(token, game.getUrl());
	}

	@Override
	public Message processMessage(Message messageFromGame) {
		logger.debug("processing messageFromGame received from game.");
		String xmlResponse;
		GameRequest gameRequest = null;
		Message publisherMessage;
		try {
			logger.debug("Creating Message object from string received from the game.");

			gameRequest = gameRequestService.getGameRequest(messageFromGame);

			String initialMessageFromGameToken = messageFromGame.getMethod().getParams().getToken().getValue();

			Message messageToCommunicate = gameMessageProcessorService.processMessage(messageFromGame, gameRequest);

			if ((isZeroAmountBetWinTransaction(messageFromGame)) && !publisherSupportZeroAmountTransaction(messageFromGame.getMethod().getName(), gameRequest.getPublisher())) {
				logger.info("Publisher " + gameRequest.getPublisher().getName() + " does not support zero bet/win. Calling get balance instead");
				String newToken = messageToCommunicate.getMethod().getParams().getToken().getValue();
				messageToCommunicate = gamePlatformIntegrationUtils.constructGPIMessage(gameRequest.getToken(), null, null, null, MethodType.GET_BALANCE);
				
				MessageHelper.changeToken(messageToCommunicate, newToken);
				MessageHelper.changeCredentials(messageToCommunicate, gameRequest.getPublisher().getApiUsername(), gameRequest.getPublisher().getPassword());
				
			}

			publisherMessage = communicationHandlerSelector.communicate(gameRequest.getPublisher(), messageToCommunicate);

			int ttl = "betgames".equals(gameRequest.getGame().getGameProvider().getName()) ? CacheConstants.BETGAMES_TOKEN_TTL : CacheConstants.GPI_GAME_REQUEST_TTL;
			logger.debug("TTL to use is " + ttl);

			if ((isZeroAmountBetWinTransaction(messageFromGame)) && !publisherSupportZeroAmountTransaction(messageFromGame.getMethod().getName(), gameRequest.getPublisher())) {
				logger.info("Creating mock transaction message to the message " + publisherMessage.toString());

				// the game message token was changed in gameMessageProcessorService.processMessage
				// need to set the initial value again as in db it will have the old token
				messageFromGame.getMethod().getParams().setToken(new Token(initialMessageFromGameToken));

				publisherMessage = createPublisherMockTransactionMessage(messageFromGame, publisherMessage);
				logger.info("Mock transaction message is " + publisherMessage.toString());
			}

			publisherMessage = publisherMessageProcessorService.processMessage(messageFromGame, publisherMessage, gameRequest, ttl);

		} catch (CommunicationException e) {
			logger.error("Something went wrong while communicating message", e);
			
			logger.info("reverting current transaction");
			revertCurrentTransaction(messageFromGame, gameRequest);
			publisherMessage = MessageHelper.createErrorResponse(new GamePlatformIntegrationException(e), messageFromGame.getMethod().getName().value());
		} catch (TransactionNotSuccessException e) {
			logger.info("reverting current transaction");
			revertCurrentTransaction(messageFromGame, gameRequest);
			logger.info("Transaction not Succed: "+e);
			publisherMessage = MessageHelper.createErrorResponse(new GamePlatformIntegrationException(e.getErrorMessage(), e.getErrorCode()), messageFromGame.getMethod().getName().value());
		} catch (GamePlatformIntegrationException e) {
			logger.error("Something went wrong while trying get the GameRequest", e);
			publisherMessage = MessageHelper.createErrorResponse(e, messageFromGame.getMethod().getName().value());
		} catch (Exception e) {
			logger.error("Something went wrong while trying to process messageFromGame.", e);
			if (messageFromGame != null) {
				// messageFromGame fully marshalled
				publisherMessage = MessageHelper.createErrorResponse(new GamePlatformIntegrationException(e), messageFromGame.getMethod().getName().value());
			} else {
				// messageFromGame failed to be processed. not possible to get method name in this case
				publisherMessage = MessageHelper.createErrorResponse(new GamePlatformIntegrationException(e), null);
			}
		}
		return publisherMessage;
	}

	private boolean isZeroAmountBetWinTransaction(Message message) {
		return (MethodType.BET.equals(message.getMethod().getName()) || MethodType.WIN.equals(message.getMethod().getName())) 
				&& message.getMethod().getParams().getAmount().getValue() == 0;
	}

	private boolean publisherSupportZeroAmountTransaction(MethodType methodType, Publisher publisher) {
		if (publisher != null) {
			if  (MethodType.BET.equals(methodType) && publisher.getSupportZeroBet() != null && publisher.getSupportZeroBet()) {
				return true;
			} else if (MethodType.WIN.equals(methodType) && publisher.getSupportZeroWin() != null && publisher.getSupportZeroWin()) {
				return true;
			}
		}
		return false;
	}


	@Override
	public void setProviderSpecificParams(Map parameterMap, LoadGameDto loadGameDto, UriComponentsBuilder builder) throws NonExistentPublisherException {
		builder.queryParam("token", loadGameDto.getToken());
		builder.queryParam("pn", ((String[]) parameterMap.get("pn"))[0]);
		if (parameterMap.get("serviceUrl") != null) {
			builder.queryParam("serviceUrl", parameterMap.get("serviceUrl"));
			builder.queryParam("serviceMessage", parameterMap.get("serviceMessage"));
		}
		String[] lang = (String[]) parameterMap.get("lang");
		builder.queryParam("lang", (lang[0]).toLowerCase());
		String[] type = (String[]) parameterMap.get("type");
		builder.queryParam("type", (type == null) ? PlayingType.CHARGED.getType() : type[0]);

	}

	private void revertCurrentTransaction(Message gameMessage, GameRequest gameRequest) {
		if (gameMessage.getMethod().getName().equals(MethodType.BET)) {
			logger.debug("Reverting a bet transaction.");
			Round round = roundService.getRound(gameRequest.getGame(), gameRequest.getGame().getGameProvider(), gameMessage.getMethod().getParams().getRoundId().getValue());
			try {
				transactionService.registerRefundTransaction(String.valueOf(gameMessage.getMethod().getParams().getTransactionId().getValue()), round, getAmountFromMessage(gameMessage.getMethod().getParams()), gameMessage.getMethod().getParams().getTransactionId().getValue());
			} catch (NoExistentTransactionException e) {
				logger.warn("Trying to refund a transaction that doesn't exists");
			} catch (TransactionAlreadyProcessedException e) {
				logger.warn("Trying to refund a transaction already processed");
			}
		} else if (gameMessage.getMethod().getName().equals(MethodType.WIN)) {
			Round round = roundService.getRound(gameRequest.getGame(), gameRequest.getGame().getGameProvider(), gameMessage.getMethod().getParams().getRoundId().getValue());
			transactionService.registerWinCorrection(gameMessage.getMethod().getParams().getTransactionId().getValue(), round, getAmountFromMessage(gameMessage.getMethod().getParams()));
		}
	}

	private Long getAmountFromMessage(Params params) {
		if (params.getFreeSpin() != null) {
			return 0L;
		}
		return params.getAmount().getValue();
	}

	/**
	 * @param messageFromGame message received from game
	 * @param gameRequest the gameRequest
	 * @param publisherMessage message received from publisher
	 * @return a mock transaction message. it should be used when a publisher doesnt support 0 bet/win calls
	 * @throws GameRequestNotExistentException
	 */
	private Message createPublisherMockTransactionMessage(Message messageFromGame, Message publisherMessage) throws GameRequestNotExistentException {
		Message mockTransactionMessage;

		if (publisherMessage.getResult().getSuccess() == 1) {
			String publisherToken = publisherMessage.getResult().getReturnset().getToken().getValue();

			mockTransactionMessage = MessageHelper.createResponse(messageFromGame.getMethod().getName(), publisherToken);
			mockTransactionMessage.getResult().setSuccess(publisherMessage.getResult().getSuccess());

			long amount = publisherMessage.getResult().getReturnset().getBalance().getValue();
			mockTransactionMessage.getResult().getReturnset().setBalance(new Balance(amount));

			String messageFromGameTransactionId = messageFromGame.getMethod().getParams().getTransactionId().getValue();
			MethodType methodType = messageFromGame.getMethod().getName();
			String extTransactionId = methodType.toString() + "-0-" + messageFromGameTransactionId;
			mockTransactionMessage.getResult().getReturnset().setTransactionId(new TransactionId(extTransactionId));
			mockTransactionMessage.getResult().getReturnset().setAlreadyProcessed(new AlreadyProcessed(false));
		} else {
			int publisherMessageErrorCode = publisherMessage.getResult().getReturnset().getErrorCode().getValue();
			String publisherMessageErrorMessage = publisherMessage.getResult().getReturnset().getError().getValue();
			mockTransactionMessage = MessageHelper.createErrorResponse(new GamePlatformIntegrationException(publisherMessageErrorMessage, publisherMessageErrorCode), publisherMessage.getMethod().getName().value());
		}

		return mockTransactionMessage;
	}


	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	public void setPublisherService(PublisherService publisherService) {
		this.publisherService = publisherService;
	}

	public void setRoundService(RoundService roundService) {
		this.roundService = roundService;
	}

	public void setGameRequestService(GameRequestService gameRequestService) {
		this.gameRequestService = gameRequestService;
	}

	public void setCommunicationHandlerSelector(CommunicationHandlerSelector communicationHandlerSelector) {
		this.communicationHandlerSelector = communicationHandlerSelector;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public void setGameMessageProcessorService(GameMessageProcessorService gameMessageProcessorService) {
		this.gameMessageProcessorService = gameMessageProcessorService;
	}

	public void setPublisherMessageProcessorService(PublisherMessageProcessorService publisherMessageProcessorService) {
		this.publisherMessageProcessorService = publisherMessageProcessorService;
	}
}
