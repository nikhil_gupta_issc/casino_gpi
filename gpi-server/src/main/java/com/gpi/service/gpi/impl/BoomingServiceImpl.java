package com.gpi.service.gpi.impl;

import java.net.URL;
import java.security.SignatureException;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.booming.api.BoomingMethodType;
import com.booming.domain.BoomingPlayerInfoRequest;
import com.booming.domain.BoomingPlayerInfoResponse;
import com.booming.domain.AbstractBoomingRequest;
import com.booming.domain.BoomingRequestHeader;
import com.booming.domain.AbstractBoomingResponse;
import com.booming.domain.BoomingStartSessionRequest;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.game.Game;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.service.booming.BoomingMessageCommand;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.gpi.BoomingService;
import com.gpi.service.gpi.validation.BoomingMessageValidator;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.cache.CacheManager;

import net.sf.json.JSONObject;

public class BoomingServiceImpl implements BoomingService {

	private static final Logger logger = LoggerFactory.getLogger(BoomingServiceImpl.class);
	
	private static final String FREE_PLAYER = "asdf";
	
	private static final String FREE_PLAYER_BALANCE = "100.00";
	
	private static final String FREE_PLAYER_CURRENCY = "EUR";
	
	private static final String DEFAULT_DEVICE = "DEFAULT";
	
	private Map<String, BoomingMessageCommand> commands;

	private HttpConnectionManager connectionManager;
	
	private GameDomainService gameDomainService;
	
	private CacheManager cacheManager;
	
	private BoomingMessageValidator messageValidator;
	
	private Map<String, String> deviceMappers;
	
	@Override
	public AbstractBoomingResponse processRequest(BoomingRequestHeader header, AbstractBoomingRequest request, BoomingMethodType method) throws GameRequestNotExistentException, TransactionNotSuccessException, SignatureException  {
		if (messageValidator.validateRequestMessage(header, request)) {
			return commands.get(method.getMethodName()).execute(header, request);
		}
		throw new SignatureException("Bad signature");
	}

	@Override
	public String getGameURL(String token, String pn, String gameName, String lang, String type, String device) throws GameRequestNotExistentException, NonExistentGameException {

		try {
			
			boolean demo = "FREE".equalsIgnoreCase(type);
			String username = FREE_PLAYER;
			String balance = FREE_PLAYER_BALANCE;
			String currency = FREE_PLAYER_CURRENCY;
			
			if (!demo) {
				BoomingPlayerInfoResponse playerInfo = (BoomingPlayerInfoResponse)commands.get(BoomingMethodType.PLAYER_INFO.getMethodName()).execute(new BoomingRequestHeader(), new BoomingPlayerInfoRequest(token));
	
				username = playerInfo.getUsername();
				balance = playerInfo.getBalance();
				currency = playerInfo.getCurrency();
				token = playerInfo.getToken();
			}
	
			Game game = gameDomainService.getGameByName(gameName);
			
			BoomingStartSessionRequest request = new BoomingStartSessionRequest()
					.setGame_id(game.getName().replace("BM-", ""))
					.setBalance(balance)
					.setLocale(lang.toLowerCase())
					.setCurrency(currency.toUpperCase())
					.setPlayer_id(pn + "-" + username)
					.setRollback_callback(ApplicationProperties.getProperty("booming.api.rollback_callback"))
					.setCallback(ApplicationProperties.getProperty("booming.api.callback"))
					.setDemo(demo)
					.setVariant(deviceMappers.containsKey(device) ? deviceMappers.get(device) : deviceMappers.get(DEFAULT_DEVICE))
					.setPlayer_origin(pn);
			
			JSONObject response = this.executeSignedHttpRequest(pn, game.getUrl(), request.toJson());
	
			String sessionId = response.getString("session_id");
			cacheManager.store(CacheConstants.BOOMING_TOKEN_KEY + "-" + sessionId, token, CacheConstants.BOOMING_TOKEN_TTL);
			return response.getString("play_url");
	
		}catch(Exception e) {
			logger.error(e.getMessage(), e);
			throw new GameRequestNotExistentException("Internal Error");
		}
	}

	private JSONObject executeSignedHttpRequest(String pn, String url, String requestBody) throws Exception {
		
		String nonce = String.valueOf(new java.util.Date().getTime());
		PostMethod postRequest = new PostMethod(url);
		try {
			HttpClient httpClient = new HttpClient(this.connectionManager);
			
			postRequest.addRequestHeader("X-Bg-Api-Key", messageValidator.getApiKey(pn));
			postRequest.addRequestHeader("X-Bg-Nonce", nonce);
			postRequest.addRequestHeader("X-Bg-Signature", messageValidator.calculateSignature(pn, new URL(url).getPath(), nonce, requestBody));
			postRequest.setRequestEntity(new StringRequestEntity(requestBody,"application/json","UTF-8"));
			
			logger.debug("execute http request: uri={}, requestBody={}", postRequest.getURI(), requestBody);
			httpClient.executeMethod(postRequest);
			
			if (postRequest != null && postRequest.getStatusCode() == 200) {
				String responseBody = postRequest.getResponseBodyAsString();
				logger.debug("execute http response: responseBody={}", responseBody);
				JSONObject response = JSONObject.fromObject(responseBody);
				return response;
			}else {
				throw new Exception("StartSession Request StatusCode: " + postRequest.getStatusCode() + ", StatusText=" + postRequest.getStatusText());
			}
		} catch (Exception e) {
			throw e;
		} 
		finally
		{
			try {postRequest.releaseConnection(); }catch(Exception e) {}
		}
	}

	/**
	 * @param commands the commands to set
	 */
	public void setCommands(Map<String, BoomingMessageCommand> commands) {
		this.commands = commands;
	}

	/**
	 * @param connectionManager the connectionManager to set
	 */
	public void setConnectionManager(HttpConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	/**
	 * @param gameDomainService the gameDomainService to set
	 */
	public void setGameDomainService(GameDomainService gameDomainService) {
		this.gameDomainService = gameDomainService;
	}

	/**
	 * @param messageValidator the messageValidator to set
	 */
	public void setMessageValidator(BoomingMessageValidator messageValidator) {
		this.messageValidator = messageValidator;
	}

	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	/**
	 * @param deviceMappers the deviceMappers to set
	 */
	public void setDeviceMappers(Map<String, String> deviceMappers) {
		this.deviceMappers = deviceMappers;
	}
	
	

}
