package com.gpi.service.gpi.impl;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gameart.api.GameArtResponseCodes;
import com.gameart.api.GameArtResponseMessages;
import com.gameart.api.domain.GameArtErrorResponse;
import com.gameart.api.domain.GameArtGetGameResponse;
import com.gameart.api.domain.GameArtRemoteData;
import com.gameart.api.domain.GameArtResponse;
import com.gameart.api.validation.GameArtRequestValidator;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gameart.GameArtMessageCommand;
import com.gpi.service.gpi.GameArtService;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.gpi.OnexTwoService;
import com.gpi.service.onextwo.OnexTwoHandler;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.MessageHelper;
import com.gpi.utils.PlayerIdentifierBuilder;
import com.onextwo.api.OnexTwoMethod;
import com.onextwo.api.domain.OnexTwoRequest;
import com.onextwo.api.domain.OnexTwoResponse;
import com.onextwo.api.validation.OnexTwoRequestValidator;

/**
 * Default implementation for the Game Art integration service
 * 
 * @author Alexandre
 *
 */

public class OnexTwoServiceImpl implements OnexTwoService {
	private OnexTwoRequestValidator requestValidator;
	
	private static final Logger logger = LoggerFactory.getLogger(OnexTwoServiceImpl.class);
	
	private Map<String, OnexTwoHandler> handlers;

	@Override
	public OnexTwoResponse processRequest(OnexTwoRequest request, OnexTwoMethod method) {
		
		if (!requestValidator.validateRequest(request)) {
			OnexTwoResponse response = new OnexTwoResponse();
			response.setErrorCode(-7);
			response.setErrorDesc("security key mismatch");
			return response;
		}
		
		try {
			OnexTwoResponse response = handlers.get(method.toString()).handle(request, method);
			
			logger.info("Response " + response.toString());
			return response;
		} catch (GameRequestNotExistentException e) {
			OnexTwoResponse response = new OnexTwoResponse();
			response.setErrorCode(403);
			response.setErrorDesc("invalid game session id");
			
			logger.info("Response " + response.toString());
			return response;
		}
		
	}

	/**
	 * @param requestValidator the requestValidator to set
	 */
	public void setRequestValidator(OnexTwoRequestValidator requestValidator) {
		this.requestValidator = requestValidator;
	}

	/**
	 * @param handlers the handlers to set
	 */
	public void setHandlers(Map<String, OnexTwoHandler> handlers) {
		this.handlers = handlers;
	}
	
	
}
