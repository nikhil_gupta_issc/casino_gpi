package com.gpi.service.gpi.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.HeliogamingService;
import com.gpi.service.heliogaming.HeliogamingHandler;
import com.heliogaming.api.HeliogamingCoreRequest;
import com.heliogaming.api.HeliogamingCoreResponse;

/**
 * Implementation for the Spinmatic integration service
 * 
 * @author Alexandre
 *
 */
public class HeliogamingServiceImpl implements HeliogamingService {
	
	private static final Logger logger = LoggerFactory.getLogger(HeliogamingServiceImpl.class);
	
	private Map<String, HeliogamingHandler> handlers;
	
	@Override
	public HeliogamingCoreResponse processRequest(HeliogamingCoreRequest request) {
		try {
			HeliogamingCoreResponse response = this.handlers.get(request.getMethodType()).handle(request);
			return response;
		} catch (GameRequestNotExistentException e) {
			logger.error(e.getMessage(), e);
			
			HeliogamingCoreResponse response = new HeliogamingCoreResponse();
			response.setErrorMsg(e.getErrorMessage());
			response.setSuccess(false);
			response.setErrorCode(1); // TODO
			
			return response;
		}
	}

	
	public void setHandlers(Map<String, HeliogamingHandler> handlers) {
		this.handlers = handlers;
	}
}
