package com.gpi.service.gpi.message.processor;

import com.gpi.api.*;
import com.gpi.domain.GameRequest;
import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.game.PlayingType;
import com.gpi.exceptions.*;
import com.gpi.service.freespin.FreeSpinService;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.game.GameProviderService;
import com.gpi.service.round.RoundService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.MessageHelper;
import com.gpi.utils.cache.CacheManager;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

public class GameMessageProcessorServiceImpl implements GameMessageProcessorService {

    private static final Logger logger = LoggerFactory.getLogger(GameMessageProcessorServiceImpl.class);
    private GameDomainService gameDomainService;
    private RoundService roundService;

    private TransactionService transactionService;
    private GameProviderService gameProviderService;
    private FreeSpinService freeSpinService;
    
    private CacheManager gamePlatformIntegrationCacheManager;

    @Override
    @Transactional
    public Message processMessage(Message messageFromGame, GameRequest gameRequest) throws GameProviderNotExistentException, NonExistentGameException, InvalidFreeSpinValue, NoFreeSpinLeftException, GamePlatformIntegrationException {
        logger.debug("Processing the values received from game.");

        MessageHelper.changeToken(messageFromGame, gameRequest.getPublisherToken());
        
        addProviderPrefix(messageFromGame, gameRequest);
        validateMessage(messageFromGame, gameRequest);

        Message newMessage = MessageHelper.copyMessage(messageFromGame);
        setMessageCredentials(gameRequest, newMessage);
        setMessageDevice(gameRequest, newMessage);

        //If is a place_bet or award_winning  or refund_bet or clearBets method we should update the round
        if (shouldUpdateRound(newMessage, gameRequest.getPlayingType())) {
            logger.info("The message has important info to update.");
            Round round;
            try {
                round = getRound(newMessage, gameRequest, messageFromGame);
                newMessage = updateRoundValues(round, newMessage);
            } catch (TransactionAlreadyProcessedException e) {
                throw e;
            } catch (NoExistentTransactionException e) {
                throw e;
            } catch (Exception e) {
                logger.error("Error", e);
                throw new GamePlatformIntegrationException(e);
            }

        }
        return newMessage;
    }

    private void addProviderPrefix(Message messageFromGame, GameRequest gameRequest) {
    	if (messageFromGame.getMethod().getParams() != null && messageFromGame.getMethod().getParams().getGameReference() != null) {
    		String providerPrefix = gameRequest.getGame().getGameProvider().getPrefix();

    		if (providerPrefix != null) {
    			logger.info("Adding prefix " + providerPrefix + " to game reference");
    			String gameReference = messageFromGame.getMethod().getParams().getGameReference().getValue();
    			messageFromGame.getMethod().getParams().getGameReference().setValue(providerPrefix + "-" + gameReference);
    		}

    		logger.info("New game reference is " + messageFromGame.getMethod().getParams().getGameReference().getValue());
    	}

    }

    private void setMessageDevice(GameRequest gameRequest, Message newMessage) {
        if (gameRequest.getDevice() != null) {
            Device device = new Device();
            device.setValue(gameRequest.getDevice());
            newMessage.getMethod().getParams().setDevice(device);
        }
    }

    private void setMessageCredentials(GameRequest gameRequest, Message newMessage) {
        Credentials credentials = new Credentials();
        credentials.setLogin(gameRequest.getPublisher().getApiUsername());
        credentials.setPassword(gameRequest.getPublisher().getPassword());
        newMessage.getMethod().setCredentials(credentials);
    }

    private void validateMessage(Message message, GameRequest gameRequest) throws GamePlatformIntegrationException, NonExistentGameException, GameProviderNotExistentException, NoFreeSpinLeftException, InvalidFreeSpinValue {
        logger.debug("Validating the message received from game provider");

        gameProviderService.checkCredentials(gameRequest.getGame().getGameProvider(), message.getMethod().getCredentials().getLogin(),
                message.getMethod().getCredentials().getPassword());

        if (message.getMethod().getName().equals(MethodType.BET)) {
            Game gameReferenced = gameDomainService.getGameByName(message.getMethod().getParams().getGameReference().getValue());
            if (!gameReferenced.getGameProvider().equals(gameRequest.getGame().getGameProvider())) {
                throw new GamePlatformIntegrationException("The game reference was not expected.");
            } else if (gameRequest.getPlayer() == null) {
                throw new PlayerNotConfiguredException("Unknown player assigned. Probably method 'getPlayerInfo' wasn't called.");
            }

            if (message.getMethod().getParams().getFreeSpin() != null) {
            	List<FreeSpin> freeSpin = freeSpinService.findFreeSpinByPlayerAndGame(gameRequest.getPlayer(), gameRequest.getGame());
                if (freeSpin.size() <= 0) {
                    throw new NoFreeSpinLeftException("No more free spin left for player=" + gameRequest.getPlayer().getName());
                } 
            }
        }
    }

    private Message updateRoundValues(Round round, Message message) throws TransactionAlreadyProcessedException, NoExistentTransactionException {
        logger.debug("Updating Round values");
        MethodType method = message.getMethod().getName();
        Transaction transaction = getAlreadyProcessedTransaction(message, round);
        logger.debug("TransactionProviderMapping received={}", transaction);
        Params params = message.getMethod().getParams();

        if (transaction == null) {
            Long amount = getAmountFromMessage(params, method);
            
            params.setAmount( new Amount(amount));
            
            List<FreeSpin> freeSpins = freeSpinService.findFreeSpinByPlayerAndGame(round.getPlayer(), round.getGame());
            
            if (freeSpins != null && freeSpins.size() > 0 ) {
	            FreeSpin freeSpin = freeSpins.get(0);
	            
	            if (freeSpin != null && freeSpin.getExternalRef() != null && params.getFreeSpin() != null) {
	            	params.setFreeSpinExternalRef(new FreeSpinExternalRef(freeSpin.getExternalRef()));
	            	gamePlatformIntegrationCacheManager.store("free-spin-last-external", freeSpin.getExternalRef(), 1000);
	            }
            } else if (method.equals(MethodType.WIN) && params.getFreeSpin() != null){
            	params.setFreeSpinExternalRef(new FreeSpinExternalRef((String)gamePlatformIntegrationCacheManager.get("free-spin-last-external")));
            	gamePlatformIntegrationCacheManager.delete("free-spin-last-external");
            }
            
            String gameTransactionId = params.getTransactionId().getValue();

            if (method.equals(MethodType.BET)) {
                logger.debug("Updating betAmount in {} cents", amount);
                Transaction t = transactionService.registerBet(gameTransactionId, round, amount);
                message.getMethod().getParams().setTransactionId(new TransactionId(String.valueOf(t.getId())));
            } else if (method.equals(MethodType.WIN)) {
                logger.debug("Updating winAmount in {} cents", amount);
                Transaction t = transactionService.registerWin(gameTransactionId, round, amount);
                message.getMethod().getParams().setTransactionId(new TransactionId(String.valueOf(t.getId())));
            } else if (method.equals(MethodType.REFUND_TRANSACTION)) {
                logger.debug("processing a refundBet for transactionId={}", gameTransactionId);
                Transaction t = transactionService.registerRefundTransaction(gameTransactionId, round, amount, params.getRefundedTransactionId().getValue());
                message.getMethod().getParams().setRefundedTransactionId(new RefundedTransactionId(String.valueOf(transactionService.getTransaction(params.getRefundedTransactionId().getValue(), round).getId())));
                message.getMethod().getParams().setTransactionId(new TransactionId(String.valueOf(t.getId())));
            }
            MessageHelper.changeRoundId(message, round.getId());
            roundService.updateRound(round);
        } else {
            logger.warn("Transaction with id={} for round={} is already processed.", params.getTransactionId().getValue(), params.getRoundId().getValue());
            throw new TransactionAlreadyProcessedException("Operation already processed.", transaction);
        }
        return message;
    }

    private Long getAmountFromMessage(Params params, MethodType method) {
        if (params.getFreeSpin() != null && method.equals(MethodType.BET)) {
            return 0L;
        }
        return params.getAmount().getValue();
    }


    private Transaction getAlreadyProcessedTransaction(Message message, Round round) {
        String providerTransactionId;
        logger.debug("Looking if transaction already exists.");
        Params params = message.getMethod().getParams();
        if (params.getTransactionId() != null) {
            providerTransactionId = params.getTransactionId().getValue();
            logger.debug("looking transaction for providerTransactionId={}", providerTransactionId);
            Transaction transaction = transactionService.getTransaction(providerTransactionId, round);
            logger.debug("transaction found={}", transaction);
            return transaction;
        } else {
            return null;
        }

    }


    private Round getRound(Message message, GameRequest gameRequest, Message messageFromGame) throws
            NonExistentGameException, GamePlatformIntegrationException {
        GameProvider gameProvider = gameRequest.getGame().getGameProvider();

        Game game = gameDomainService.getGameByName(message.getMethod().getParams().getGameReference().getValue());
        Round round = roundService.getRound(game, gameProvider, message.getMethod().getParams().getRoundId().getValue());
        if (message.getMethod().getName().equals(MethodType.BET)) {
            if (round == null) {
                round = roundService.createRound(gameRequest.getPlayer(), game, message.getMethod().getParams().getRoundId().getValue());
            } else {
                if (message.getMethod().getParams().getTransactionId() != null && !round.getGameRoundId().equals
                        (message.getMethod().getParams().getRoundId().getValue())) {
                    round = roundService.createRound(gameRequest.getPlayer(), game, message.getMethod().getParams().getRoundId().getValue());
                }
            }
        } else if (round == null && MethodType.WIN.equals(message.getMethod().getName())) {
        	//add this if because RCT don't send the winnings until the session is closed
        	round = roundService.findLastRound(gameRequest.getPlayer(), game);
        	if (round != null) {
        		messageFromGame.getMethod().getParams().getRoundId().setValue(round.getGameRoundId());	
        	}
        	
        } 
        if (round == null) {
            throw new NoExistentTransactionException(String.format("Transaction not found for game=%s", message
                    .getMethod().getParams().getGameReference().getValue()));
        }
        return round;
    }

    private boolean shouldUpdateRound(Message message, PlayingType playingType) {

        boolean response = false;
        if (playingType.equals(PlayingType.CHARGED)) {
            response = message.getMethod().getName().equals(MethodType.BET)
                    || message.getMethod().getName().equals(MethodType.WIN)
                    || message.getMethod().getName().equals(MethodType.REFUND_TRANSACTION);

        }
        logger.debug("Message method = [{}], should update the round values=[{}]", message.getMethod().getName().name(),
                response);
        return response;
    }

    public void setGameDomainService(GameDomainService gameDomainService) {
        this.gameDomainService = gameDomainService;
    }


    public void setRoundService(RoundService roundService) {
        this.roundService = roundService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setGameProviderService(GameProviderService gameProviderService) {
        this.gameProviderService = gameProviderService;
    }

    public void setFreeSpinService(FreeSpinService freeSpinService) {
        this.freeSpinService = freeSpinService;
    }

	/**
	 * @param gamePlatformIntegrationCacheManager the gamePlatformIntegrationCacheManager to set
	 */
	public void setGamePlatformIntegrationCacheManager(CacheManager gamePlatformIntegrationCacheManager) {
		this.gamePlatformIntegrationCacheManager = gamePlatformIntegrationCacheManager;
	}
}
