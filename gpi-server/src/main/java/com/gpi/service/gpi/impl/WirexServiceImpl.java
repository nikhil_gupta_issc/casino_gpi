package com.gpi.service.gpi.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.WirexService;
import com.gpi.service.wirex.WirexMessageCommand;
import com.gpi.utils.ApplicationProperties;
import it.wirex.api.WirexErrorCodes;
import it.wirex.api.WirexMethod;
import it.wirex.api.domain.WirexError;
import it.wirex.api.domain.WirexResponse;

public class WirexServiceImpl implements WirexService {
	
	protected static final Logger logger = LoggerFactory.getLogger(WirexServiceImpl.class);
	
	private static final String SERVER_CONFIG_KEY = "wirex.api.url";
	private static final String OPERATOR_CONFIG_KEY = "wirex.api.operator";
	
	private Map<String, WirexMessageCommand> commands;


	@Override
	public WirexResponse processRequest(String sessionId, Map<String, String> params, WirexMethod method) {
		logger.info("Processing request " + method.toString());
		StringBuilder sb = new StringBuilder();
		for (String k : params.keySet()) {
			sb.append(k).append("=").append(params.get(k)).append("&");
		}
		
		logger.info("Params received: " + sb.toString().replaceAll("&$", ""));		
		
		WirexResponse wirexResponse;
		try {
			wirexResponse = this.commands.get(method.getName()).execute(sessionId, params, method);
		} catch (GameRequestNotExistentException e) {
			wirexResponse = new WirexResponse();
			WirexError error = new WirexError();
			error.setTypeCode(WirexErrorCodes.INTERNAL_SERVER_ERROR);
			wirexResponse.setError(error);
			wirexResponse.setStatusCode(-1);
		}
		
		return wirexResponse;
	}
	

	@Override
	public String getGameURL(String token, String idGame, String currency, String language) {
		StringBuilder sb = new StringBuilder();

		String server = ApplicationProperties.getProperty(SERVER_CONFIG_KEY);
		String operatorId = ApplicationProperties.getProperty(OPERATOR_CONFIG_KEY);

		sb.append(server).append("/smartcasino/ext/PlatformRsLoadGame?")
		.append("operatorId=").append(operatorId)
		.append("&currency=").append(currency)
		.append("&client=").append("desktop")
		.append("&game=").append(idGame)
		.append("&token=").append(token);

		if (language != null) {
			sb.append("&lang=").append(language);
		} else {
			sb.append("&lang=").append("en");
		}

		return sb.toString();
	}
	

	/*@Override
	public String getFreeGameURL(String idGame, String language, String currency) {
		StringBuilder sb = new StringBuilder();
		
		String demoServer = ApplicationProperties.getProperty(DEMO_SERVER_CONFIG_KEY);
		
		try {
			sb.append(demoServer).append("/gs2c/openGame.do?")
			.append("gameSymbol=").append(URLEncoder.encode(idGame, "UTF-8"));
			
			if (language != null) {
				sb.append("&lang=").append(URLEncoder.encode(language, "UTF-8"));
			}
			
			sb.append("&cur=").append(URLEncoder.encode(currency, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error(e.toString());
		}
		
		return sb.toString();
	}*/

	
	public void setCommands(Map<String, WirexMessageCommand> commands) {
		this.commands = commands;
	}
}
