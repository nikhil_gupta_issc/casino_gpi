package com.gpi.service.gpi.impl;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gameart.api.GameArtResponseCodes;
import com.gameart.api.GameArtResponseMessages;
import com.gameart.api.domain.GameArtErrorResponse;
import com.gameart.api.domain.GameArtGetGameResponse;
import com.gameart.api.domain.GameArtRemoteData;
import com.gameart.api.domain.GameArtResponse;
import com.gameart.api.validation.GameArtRequestValidator;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gameart.GameArtMessageCommand;
import com.gpi.service.gpi.GameArtService;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.MessageHelper;
import com.gpi.utils.PlayerIdentifierBuilder;

/**
 * Default implementation for the Game Art integration service
 * 
 * @author Alexandre
 *
 */

public class GameArtServiceImpl implements GameArtService {
	private GameArtRequestValidator requestValidator;
	
	private static final Logger logger = LoggerFactory.getLogger(GameArtServiceImpl.class);
	
	private Map<String, GameArtMessageCommand> commands;

	private HttpConnectionManager connectionManager;
	private long timeout;
	private int connectionTimeout;
	
	@Override
	public GameArtResponse processRequest(Map<String, String> params, String queryString) {
		
		String currency = getRemoteDataCurrency(params);
		String saltKey = ApplicationProperties.getProperty("gameart.api." + currency + ".key");
		
		if (!requestValidator.validate(queryString, params.get("key"), saltKey)) {
			GameArtErrorResponse response = new GameArtErrorResponse();
			response.setMsg(GameArtResponseMessages.INVALID_KEY);
			response.setStatus(GameArtResponseCodes.INTERNAL_ERROR_STATUS_CODE);
			return response;
		}
		
		try {
			return commands.get(params.get("action")).execute(params, queryString);
		} catch (GameRequestNotExistentException e) {
			GameArtErrorResponse response = new GameArtErrorResponse();
			response.setMsg(GameArtResponseMessages.GAME_REQUEST_NOT_EXIST);
			response.setStatus(GameArtResponseCodes.INTERNAL_ERROR_STATUS_CODE);
			
			return response;
		}
		
	}

	@Override
	public String getGameURL(int idGame, String remoteId, String token, String currency, boolean demoMode, String homeURL) {
		HttpClient client = new HttpClient(this.connectionManager);

		HttpClientParams params = client.getParams();
		params.setConnectionManagerTimeout(timeout);
		params.setSoTimeout(connectionTimeout);

		client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, true));

		GetMethod method = null;
		
		try {
			String lowerCaseCurrency = currency.toLowerCase();
			
			String apiURL = ApplicationProperties.getProperty("gameart.api.url");
			String apiUser = ApplicationProperties.getProperty("gameart.api." + lowerCaseCurrency + ".user");
			String apiPassword = ApplicationProperties.getProperty("gameart.api." + lowerCaseCurrency + ".password");
			
			StringBuilder sb = new StringBuilder();
			sb.append(apiURL).append("/!v8/api/admin.js?action=");
			sb.append(demoMode ?  "get_demo" :"get_game");
			
			sb.append("&args").append(URLEncoder.encode("[1][usr]","UTF-8")).append("=").append(apiUser);
			sb.append("&args").append(URLEncoder.encode("[1][passw]", "UTF-8")).append("=").append(apiPassword);
			sb.append("&args").append(URLEncoder.encode("[0][remote_id]", "UTF-8")).append("=").append(remoteId);
			sb.append("&args").append(URLEncoder.encode("[0][game_id]", "UTF-8")).append("=").append(idGame);
			sb.append("&args").append(URLEncoder.encode("[0][remote_data]", "UTF-8")).append("=")
			.append(URLEncoder.encode("{\"current_token\":\"", "UTF-8")).append(token)
			.append(URLEncoder.encode("\",\"currency\":\"", "UTF-8")).append(lowerCaseCurrency)
			.append(URLEncoder.encode("\"}", "UTF-8"));
			
			logger.debug("Request to send to GA is " + sb.toString());
			
			method = new GetMethod(sb.toString());
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				logger.error("Response to getGame not OK status code. Returned code is " + statusCode);
			}

			byte[] responseBody = method.getResponseBody();

			String response = new String(responseBody);
			
			logger.info("Response from gameart: " + response);

			GameArtGetGameResponse resp = serializeObjectFromJSON(response);
			
			if (resp != null) {
				String gameURL = resp.getResponse().getGameUrl();

				if (homeURL != null) {
					gameURL = gameURL + "&home=" + URLEncoder.encode(homeURL, "UTF-8");
				}
				
				return gameURL;
			}
		} catch (HttpException e) {
			logger.error("Fatal protocol violation", e);
			System.err.println("Fatal protocol violation: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Fatal transport error:", e);
		} finally {
			// Release the connection.
			if (method != null)
				method.releaseConnection();
		}
		return null;
	}

	/**
	 * @param message json message
	 * @return serialized object with the get_game response
	 */
	private GameArtGetGameResponse serializeObjectFromJSON(String message) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
			GameArtGetGameResponse gameResponse = objectMapper.readValue(message, GameArtGetGameResponse.class);
			return gameResponse;
		} catch (IOException e) {
			logger.error("IOException", e);
		}

		return null;
	}
	
	/**
	 * @param params params sent by Game Art in request
	 * @return currency extracted from remote_data request field
	 */
	private String getRemoteDataCurrency(Map<String, String> params) {
		if (params.containsKey("remote_data")) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				GameArtRemoteData remoteData = mapper.readValue(params.get("remote_data"), GameArtRemoteData.class);
				String currency = remoteData.getCurrency();
				return currency;
			} catch (IOException e) {
				logger.error("Error deserializing remoteData",e);
			}
		}
		
		return null;
	}

	/**
	 * @param timeout the connection manager timeout to set
	 */
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	/**
	 * @param connectionTimeout the socket timeout to set
	 */
	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	/**
	 * @param connectionManager the connectionManager to set
	 */
	public void setConnectionManager(HttpConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	/**
	 * @param requestValidator the requestValidator to set
	 */
	public void setRequestValidator(GameArtRequestValidator requestValidator) {
		this.requestValidator = requestValidator;
	}

	/**
	 * @param commands the commands to set
	 */
	public void setCommands(Map<String, GameArtMessageCommand> commands) {
		this.commands = commands;
	}
	
	
}
