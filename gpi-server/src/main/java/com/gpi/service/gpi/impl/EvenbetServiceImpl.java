package com.gpi.service.gpi.impl;

import java.security.SignatureException;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.evenbet.api.EvenbetErrorCodes;
import com.evenbet.api.EvenbetErrorMessages;
import com.evenbet.api.EvenbetMethodType;
import com.evenbet.domain.EvenbetErrorResponse;
import com.evenbet.domain.EvenbetRequest;
import com.evenbet.domain.EvenbetRequestHeader;
import com.evenbet.domain.EvenbetResponse;
import com.evenbet.domain.EvenbetStartSessionRequest;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.service.evenbet.EvenbetMessageCommand;
import com.gpi.service.gpi.EvenbetService;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.gpi.validation.EvenbetMessageValidator;
import com.gpi.service.player.PlayerService;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.cache.CacheManager;

import net.sf.json.JSONObject;

public class EvenbetServiceImpl implements EvenbetService {

	private static final Logger logger = LoggerFactory.getLogger(EvenbetServiceImpl.class);
	
	private Map<String, EvenbetMessageCommand> commands;

	private HttpConnectionManager connectionManager;
	
	private CacheManager cacheManager;
	
	private EvenbetMessageValidator messageValidator;
	
	private GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	
	private GamePlatformIntegrationService gamePlatformIntegrationService;
	
	private PublisherService publisherService;
	
	private PlayerService playerService;
	
	private static final String PROPERTY_CLIENT_ID = "evenbet.api.clientID";
	private static final String PROPERTY_API_URL = "evenbet.api.url";
	private static final String PROPERTY_LOBBY = "evenbet.lobby";
	private static final String PROPERTY_DEMO_LOBBY = "evenbet.demo.lobby";
	
	@Override
	public EvenbetResponse processRequest(EvenbetRequestHeader header, EvenbetRequest request, EvenbetMethodType method) throws GameRequestNotExistentException, TransactionNotSuccessException, SignatureException  {
		if (messageValidator.validateRequestMessage(header, request)) {
			return commands.get(method.getMethodName()).execute(header, request);
		}
		return new EvenbetErrorResponse(EvenbetErrorCodes.INVALID_SIGNATURE, EvenbetErrorMessages.INVALID_SIGNATURE_MESSAGE);
	}

	@Override
	public String getGameURL(String token, String pn, String gameName, String lang, String type, String lobbyUrl) throws GameRequestNotExistentException, NonExistentGameException {

		try {
			
			boolean demo = "FREE".equalsIgnoreCase(type);
			
			if (!demo) {
				Message gpiFormattedMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_PLAYER_INFO);
				Message responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);
				
				String username = responseMessage.getResult().getReturnset().getLoginName().getValue();
				String currency = responseMessage.getResult().getReturnset().getCurrency().getValue();
				token = responseMessage.getResult().getReturnset().getToken().getValue();
				Publisher publisher = publisherService.findByName(pn);
				Player player = playerService.findUserByUserNameAndPublisher(username, publisher.getId());
				String userId = String.valueOf(player.getId());
				
				String clientId = ApplicationProperties.getProperty(PROPERTY_CLIENT_ID);
				
				EvenbetStartSessionRequest request = new EvenbetStartSessionRequest();
				request.setUserId(userId);
				request.setNick(username);
				request.setAuthType("external");
				request.setLang(lang.toLowerCase());
				request.setLaunchOptions("{\"restoreSessionUrl\":\"" + lobbyUrl + "\"}");
				request.setCurrency(currency);
				
				String apiUrl = ApplicationProperties.getProperty(PROPERTY_API_URL);
				
				String lobby = ApplicationProperties.getProperty(PROPERTY_LOBBY);
				
				String url = lobby
						.replace("{API_URL}", apiUrl)
						.replace(":userId", userId)
						.replace("{CLIENT_ID}", clientId);
				
				JSONObject response = this.executeSignedHttpRequest(url, request);
				
				cacheManager.store(CacheConstants.EVENBET_TOKEN_KEY + "-" + userId, token, CacheConstants.EVENBET_TOKEN_TTL);
				return response.getJSONObject("data").getJSONObject("attributes").getString("redirect-url");
			}else {
				String demoLobby = ApplicationProperties.getProperty(PROPERTY_DEMO_LOBBY);
				return demoLobby;
			}
			
		}catch(Exception e) {
			logger.error(e.getMessage(), e);
			throw new GameRequestNotExistentException("Internal Error");
		}
	}

	private JSONObject executeSignedHttpRequest(String url, EvenbetStartSessionRequest request) throws Exception {
		
		PostMethod postRequest = new PostMethod(url);
		try {
			HttpClient httpClient = new HttpClient(this.connectionManager);
			
			String requestBody = request.toJson();
			
			postRequest.addRequestHeader("sign", messageValidator.calculateSignature(request.buildSignRequest()));
			postRequest.setRequestEntity(new StringRequestEntity(requestBody,"application/vnd.api+json","UTF-8"));
			
			logger.debug("execute http request: uri={}, requestBody={}", postRequest.getURI(), requestBody);
			httpClient.executeMethod(postRequest);
			
			if (postRequest != null && postRequest.getStatusCode() == 200) {
				String responseBody = postRequest.getResponseBodyAsString();
				logger.debug("execute http response: responseBody={}", responseBody);
				JSONObject response = JSONObject.fromObject(responseBody);
				return response;
			}else {
				throw new Exception("StartSession Request StatusCode: " + postRequest.getStatusCode() + ", StatusText=" + postRequest.getStatusText());
			}
		} catch (Exception e) {
			throw e;
		} 
		finally
		{
			try {postRequest.releaseConnection(); }catch(Exception e) {}
		}
	}

	/**
	 * @param commands the commands to set
	 */
	public void setCommands(Map<String, EvenbetMessageCommand> commands) {
		this.commands = commands;
	}

	/**
	 * @param connectionManager the connectionManager to set
	 */
	public void setConnectionManager(HttpConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	/**
	 * @param messageValidator the messageValidator to set
	 */
	public void setMessageValidator(EvenbetMessageValidator messageValidator) {
		this.messageValidator = messageValidator;
	}

	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	/**
	 * @param gamePlatformIntegrationUtils the gamePlatformIntegrationUtils to set
	 */
	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	/**
	 * @param gamePlatformIntegrationService the gamePlatformIntegrationService to set
	 */
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	/**
	 * @param publisherService the publisherService to set
	 */
	public void setPublisherService(PublisherService publisherService) {
		this.publisherService = publisherService;
	}

	/**
	 * @param playerService the playerService to set
	 */
	public void setPlayerService(PlayerService playerService) {
		this.playerService = playerService;
	}
	

}
