package com.gpi.service.gpi.validation;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.pragmaticplay.PragmaticPlayMessageCommand;
import com.gpi.utils.AlphabeticalOrderQueryStringBuilder;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.MessageHelper;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.PragmaticPlayMethod;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

/**
 * Validator for the requests received in PragmaticPlay
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayRequestValidator {
	
	private static final Logger logger = LoggerFactory.getLogger(PragmaticPlayRequestValidator.class);
	
	private AlphabeticalOrderQueryStringBuilder pragmaticPlayQueryStringBuilder;
	protected PlayerInfoDao playerInfoDaoCache;
	
	/**
	 * @param method method of the call
	 * @param params key value map of parameters received in request
	 * @param pragmaticPlayResponse response to be returned
	 * @return if the request is valid
	 */
	public boolean validateRequest(PragmaticPlayMethod method, Map<String, String> params,
			PragmaticPlayResponse pragmaticPlayResponse) {
		if (!this.validateRequestHash(params)) {
			pragmaticPlayResponse.setError(PragmaticPlayErrorCodes.INVALIDHASH_ERROR);
			
			logger.error("Invalid hash");
			
			return false;
		} else if (!PragmaticPlayMethod.AUTHENTICATE.equals(method) && !this.isPlayerLoggedIn(params.get("userId"))) {
			pragmaticPlayResponse.setError(PragmaticPlayErrorCodes.PLAYER_NOT_FOUND_ERROR);
			logger.error("Player not found");
			return false;
		}
		
		return true;
	}

	/**
	 * @param params received parameters
	 * @return if the received hash is valid
	 */
	private boolean validateRequestHash(Map<String, String> params) {
		if (params.containsKey("hash")) {
			String hash = params.get("hash");
			
			this.removeHashNullAndEmptyParams(params);
			this.pragmaticPlayQueryStringBuilder.setParams(params);
			
			String queryString = pragmaticPlayQueryStringBuilder.build();
			String queryStringWithSecret = queryString+ApplicationProperties.getProperty("pragmaticplay.api.secret");
			
			String hashedQueryStringWithSecret = this.getMD5Hash(queryStringWithSecret);
			
			return hash.equalsIgnoreCase(hashedQueryStringWithSecret);
		}
		
		return false;
	}
	
	private boolean isPlayerLoggedIn(String username) {
		return this.playerInfoDaoCache.findPlayerInfo(PragmaticPlayMessageCommand.getPlayerInfoKey(username)) != null;
	}
	
	/**
	 * Return md5 hash of a message
	 * @param message message to get md5 hash of
	 * @return md5 hash of a message
	 */
	private String getMD5Hash(String message) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(message.getBytes());
			byte[] digest = md.digest();
			String signatureHash = DatatypeConverter.printHexBinary(digest).toLowerCase();

			return signatureHash;
		} catch (NoSuchAlgorithmException e) {
			logger.error("No such algorithm", e);
		}

		return null;
	}
	
	private void removeHashNullAndEmptyParams(Map<String, String> params) {
		// hashed query string wont contain hash
		params.remove("hash");
		
		for (String k : params.keySet()) {
			if (params.get(k) == null || params.get(k).isEmpty()) {
				params.remove(k);
			}
		}
	}
	
	public void setPlayerInfoDaoCache(PlayerInfoDao playerInfoDaoCache) {
		this.playerInfoDaoCache = playerInfoDaoCache;
	}

	public void setPragmaticPlayQueryStringBuilder(AlphabeticalOrderQueryStringBuilder pragmaticPlayQueryStringBuilder) {
		this.pragmaticPlayQueryStringBuilder = pragmaticPlayQueryStringBuilder;
	}
}
