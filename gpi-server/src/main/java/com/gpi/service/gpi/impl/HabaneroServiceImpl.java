package com.gpi.service.gpi.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.service.gpi.HabaneroService;
import com.gpi.service.gpi.validation.HabaneroMessageValidator;
import com.gpi.service.habanero.HabaneroMessageCommand;
import com.habanero.api.HabaneroMethodType;
import com.habanero.domain.HabaneroFundTransferResponse;
import com.habanero.domain.HabaneroRequest;
import com.habanero.domain.HabaneroResponse;
import com.habanero.domain.HabaneroStatus;
import com.habanero.domain.HabaneroTransactionResponse;

public class HabaneroServiceImpl implements HabaneroService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(BetGamesServiceImpl.class);
	
	private HabaneroMessageValidator messageValidator;
	
	private Map<String, HabaneroMessageCommand> commands;
	
	@Override
	public HabaneroResponse processRequest(HabaneroRequest request, HabaneroMethodType method) throws GameRequestNotExistentException, TransactionNotSuccessException  {
		if (messageValidator.validateRequestMessage(request.getAuth())){
			return commands.get(method.getMethodName()).execute(request);
		}
		
		return new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus(false, true)));
	}

	/**
	 * @param commands the commands to set
	 */
	public void setCommands(Map<String, HabaneroMessageCommand> commands) {
		this.commands = commands;
	}

	public void setMessageValidator(HabaneroMessageValidator messageValidator) {
		this.messageValidator = messageValidator;
	}	

}
