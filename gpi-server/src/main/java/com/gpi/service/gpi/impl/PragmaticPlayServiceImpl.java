package com.gpi.service.gpi.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.PragmaticPlayService;
import com.gpi.service.gpi.validation.PragmaticPlayRequestValidator;
import com.gpi.service.pragmaticplay.PragmaticPlayMessageCommand;
import com.gpi.utils.ApplicationProperties;
import com.mysql.jdbc.log.Log;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.PragmaticPlayMethod;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

public class PragmaticPlayServiceImpl implements PragmaticPlayService {
	
	protected static final Logger logger = LoggerFactory.getLogger(PragmaticPlayServiceImpl.class);
	
	private static final String SERVER_CONFIG_KEY = "pragmaticplay.api.url";
	private static final String DEMO_SERVER_CONFIG_KEY = "pragmaticplay.api.demourl";
	private static final String PUBLISHER_PREFIX = "wac_";
	
	private Map<String, PragmaticPlayMessageCommand> commands;
	private PragmaticPlayRequestValidator requestValidator;


	@Override
	public PragmaticPlayResponse processRequest(Map<String, String> params, PragmaticPlayMethod method) {
		logger.info("Processing request " + method.toString());
		StringBuilder sb = new StringBuilder();
		for (String k : params.keySet()) {
			sb.append(k).append("=").append(params.get(k)).append("&");
		}
		
		logger.info("Params received: " + sb.toString().replaceAll("&$", ""));		
		
		PragmaticPlayResponse pragmaticPlayResponse = new PragmaticPlayResponse();
		
		if (this.requestValidator.validateRequest(method, params, pragmaticPlayResponse)) {
			try {
				pragmaticPlayResponse = this.commands.get(method.getName()).execute(params);
			} catch (GameRequestNotExistentException e) {
				logger.error(e.getErrorMessage());
				pragmaticPlayResponse.setError(PragmaticPlayErrorCodes.GAME_NOT_FOUND_ERROR);
				
				logger.error("Game not found");
			}
		} else {
			pragmaticPlayResponse.setError(PragmaticPlayErrorCodes.PLAYER_NOT_FOUND_ERROR);
		}
		
		return pragmaticPlayResponse;
	}
	

	@Override
	public String getGameURL(String token, String idGame, String language, String publisher, String lobbyURL) {
		StringBuilder sb = new StringBuilder();
		
		String server = ApplicationProperties.getProperty(SERVER_CONFIG_KEY);
		String loginKey = PUBLISHER_PREFIX + publisher;
		
		try {
			sb.append(server).append("/gs2c/playGame.do?")
			.append("key=")
			.append(URLEncoder.encode("token=", "UTF-8")).append(URLEncoder.encode(token, "UTF-8"))
			.append(URLEncoder.encode("&symbol=", "UTF-8")).append(URLEncoder.encode(idGame, "UTF-8"))
			.append(URLEncoder.encode("&technology=H5", "UTF-8"));
			
			if (language != null) {
				sb.append(URLEncoder.encode("&language=", "UTF-8")).append(language);
			}
			
			if (lobbyURL != null) {
				sb.append(URLEncoder.encode("&lobbyURL=", "UTF-8")).append(URLEncoder.encode(lobbyURL, "UTF-8"));
			}
			
			sb.append("&stylename=").append(loginKey);
		} catch (UnsupportedEncodingException e) {
			logger.error(e.toString());
		}

		
		return sb.toString();
	}
	

	@Override
	public String getFreeGameURL(String idGame, String language, String currency, String lobbyURL) {
		StringBuilder sb = new StringBuilder();
		
		String demoServer = ApplicationProperties.getProperty(DEMO_SERVER_CONFIG_KEY);
		
		try {
			sb.append(demoServer).append("/gs2c/openGame.do?")
			.append("gameSymbol=").append(URLEncoder.encode(idGame, "UTF-8"));
			
			if (language != null) {
				sb.append("&lang=").append(URLEncoder.encode(language, "UTF-8"));
			}
			
			if (lobbyURL != null) {
				sb.append("&lobbyURL=").append(URLEncoder.encode(lobbyURL, "UTF-8"));
			}
			
			sb.append("&cur=").append(URLEncoder.encode(currency, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error(e.toString());
		}
		
		return sb.toString();
	}

	
	public void setRequestValidator(PragmaticPlayRequestValidator requestValidator) {
		this.requestValidator = requestValidator;
	}


	public void setCommands(Map<String, PragmaticPlayMessageCommand> commands) {
		this.commands = commands;
	}
}
