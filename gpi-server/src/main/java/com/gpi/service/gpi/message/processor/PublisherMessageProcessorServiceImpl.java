package com.gpi.service.gpi.message.processor;

import com.gpi.api.FreeSpins;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.api.Returnset;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.domain.GameRequest;
import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.currency.Currency;
import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.player.Player;
import com.gpi.exceptions.GamePlatformIntegrationException;
import com.gpi.exceptions.NoFreeSpinLeftException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.service.freespin.FreeSpinService;
import com.gpi.service.freespin.FreeSpinTransactionService;
import com.gpi.service.gamerequest.GameRequestService;
import com.gpi.service.player.PlayerService;
import com.gpi.service.round.RoundService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.MessageHelper;

import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

public class PublisherMessageProcessorServiceImpl implements PublisherMessageProcessorService {

    private static final Logger logger = LoggerFactory.getLogger(PublisherMessageProcessorServiceImpl.class);
    private GameRequestService gameRequestService;
    private GameRequestDao gameRequestDao;
    private RoundService roundService;
    private PlayerService playerService;
    private FreeSpinTransactionService freeSpinTransactionService;
    private TransactionService transactionService;
    private FreeSpinService freeSpinService;


    @Override
    @Transactional
    public Message processMessage(Message messageFromGame, Message publisherMessage, GameRequest gameRequest, int ttl) throws GamePlatformIntegrationException, NoFreeSpinLeftException {
        logger.info("Evaluating the response received from publisher.");
        Returnset returnset = publisherMessage.getResult().getReturnset();
        if (publisherMessage.getResult().getSuccess() == 0) {
            throw new TransactionNotSuccessException(returnset.getError().getValue(), returnset.getErrorCode().getValue());
        }
        savePlayerToRequest(publisherMessage, gameRequest);
        confirmTransactionValues(messageFromGame, publisherMessage, gameRequest);

        removeOldGameRequest(gameRequest);
        updateGameRequest(gameRequest, returnset.getToken().getValue(), ttl);
        returnset.getToken().setValue(gameRequest.getToken());

        return publisherMessage;
    }

    private void savePlayerToRequest(Message messageResponse, GameRequest gameRequest) {
        if (messageResponse.getResult().getName().equals(MethodType.GET_PLAYER_INFO)) {
            Player player;
            player = playerService.findUserByUserNameAndPublisher(messageResponse.getResult().getReturnset()
                    .getLoginName().getValue(), gameRequest.getPublisher().getId());
            if (player == null) {
                logger.info("Player with name={} not found ", messageResponse.getResult().getReturnset()
                        .getLoginName().getValue());
                player = playerService.createNewUser(messageResponse.getResult().getReturnset()
                        .getLoginName().getValue(), Currency.valueOf(messageResponse.getResult().getReturnset()
                        .getCurrency().getValue()), gameRequest.getPublisher());
            } else {
            	Currency newCurrency = Currency.valueOf(messageResponse.getResult().getReturnset()
                        .getCurrency().getValue());
            	if (!player.getCurrency().equals(newCurrency)) {
            		player.setCurrency(newCurrency);
            		playerService.updatePlayer(player);
            	}
            }
            logger.debug("Adding player {} to gameRequest.", player);
            gameRequest.setPlayer(player);
        }
    }

    private void confirmTransactionValues(Message gameMessage, Message messageResponse, GameRequest gameRequest) throws GamePlatformIntegrationException, NoFreeSpinLeftException {
        Transaction transaction = null;
        if (gameMessage.getMethod().getName().equals(MethodType.BET)
                || gameMessage.getMethod().getName().equals(MethodType.WIN)
                || gameMessage.getMethod().getName().equals(MethodType.REFUND_TRANSACTION)) {
            logger.debug("Confirm a bet transaction.");
            Round round = roundService.getRound(gameRequest.getGame(), gameRequest.getGame().getGameProvider(), gameMessage.getMethod().getParams().getRoundId().getValue());
            if (round == null) {
                throw new GamePlatformIntegrationException("Round doesn't exists", MessageHelper.PREVIOUS_ROUND_NOT_EXISTS_ERROR_CODE);
            }
            transaction = transactionService.getTransaction(gameMessage.getMethod().getParams().getTransactionId().getValue(), round);
            transaction.setExtTransactionId(messageResponse.getResult().getReturnset().getTransactionId().getValue());
            transaction.setSuccess(true);
            transactionService.updateTransaction(transaction);

            messageResponse.getResult().getReturnset().getTransactionId().setValue(String.valueOf(transaction.getId()));
        }
        insertFreeSpinValues(gameMessage, messageResponse, gameRequest, transaction);
    }

    private void removeOldGameRequest(GameRequest gameRequest) {
        logger.debug("Remove GameRequest with token={}", gameRequest.getToken());
        gameRequestService.removeGameRequest(gameRequest);
    }

    private void insertFreeSpinValues(Message gameMessage, Message messageResponse, GameRequest gameRequest, Transaction transaction) throws NoFreeSpinLeftException {
        FreeSpin freeSpin = null;
        if (gameMessage.getMethod().getParams().getFreeSpin() != null && gameMessage.getMethod().getName().equals(MethodType.BET)) {
            freeSpin = freeSpinTransactionService.processFreeSpinMessage(gameRequest.getGame(), gameRequest.getPlayer(), transaction,
                    gameMessage.getMethod().getParams().getAmount().getValue());
            
            if (freeSpin != null) {
                FreeSpins freeSpins = new FreeSpins();
                freeSpins.setAmount(freeSpin.getAmountLeft());
                freeSpins.setCents(freeSpin.getCents());
                messageResponse.getResult().getReturnset().setFreeSpins(freeSpins);
            }
            
            
        } else if (gameMessage.getMethod().getName().equals(MethodType.GET_PLAYER_INFO)) {
        	logger.info("player: "+ gameRequest.getPlayer());
        	logger.info("game: "+ gameRequest.getGame());
            List<FreeSpin> freeSpinsFound = freeSpinService.findFreeSpinByPlayerAndGame(gameRequest.getPlayer(), gameRequest.getGame());
            logger.info(freeSpinsFound.toString());
            if (freeSpinsFound != null && freeSpinsFound.size() >0) {
            	int amount = 0;
            	int cents = 0;
            	for (FreeSpin f :freeSpinsFound) {
            		amount = amount + f.getAmountLeft();
            		cents = f.getCents();
            	}
                FreeSpins freeSpins = new FreeSpins();
                freeSpins.setAmount(amount);
                freeSpins.setCents(cents);
                messageResponse.getResult().getReturnset().setFreeSpins(freeSpins);
            }
        }
        

    }

    private void updateGameRequest(GameRequest gameRequest, String newPublisherToken, int ttl) {
        gameRequest.setPublisherToken(newPublisherToken);
        gameRequestDao.updateGameRequest(gameRequest, ttl);
    }

    public void setGameRequestDao(GameRequestDao gameRequestDao) {
        this.gameRequestDao = gameRequestDao;
    }

    public void setRoundService(RoundService roundService) {
        this.roundService = roundService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public void setFreeSpinTransactionService(FreeSpinTransactionService freeSpinTransactionService) {
        this.freeSpinTransactionService = freeSpinTransactionService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setGameRequestService(GameRequestService gameRequestService) {
        this.gameRequestService = gameRequestService;
    }

    public void setFreeSpinService(FreeSpinService freeSpinService) {
        this.freeSpinService = freeSpinService;
    }
}
