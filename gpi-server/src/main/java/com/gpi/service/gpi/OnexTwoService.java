package com.gpi.service.gpi;

import com.onextwo.api.OnexTwoMethod;
import com.onextwo.api.domain.OnexTwoRequest;
import com.onextwo.api.domain.OnexTwoResponse;
import com.spinmatic.api.domain.SpinmaticOpenGameData;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponse;

/**
 * Service for the 1x2 integration
 * 
 * @author Alexandre
 *
 */
public interface OnexTwoService {

	/**
	 * @param request request received from spinmatic to be processed
	 * @param method method of the request
	 * @return response to return to spinmatic
	 */
	OnexTwoResponse processRequest(OnexTwoRequest request, OnexTwoMethod method);
	
}
