package com.gpi.service.gpi.impl;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.SpinmaticService;
import com.gpi.service.spinmatic.SpinmaticHandler;
import com.gpi.utils.ApplicationProperties;
import com.spinmatic.api.SpinmaticStatus;
import com.spinmatic.api.domain.SpinmaticCheckPlayerResponse;
import com.spinmatic.api.domain.SpinmaticErrorResponse;
import com.spinmatic.api.domain.SpinmaticLoginRequest;
import com.spinmatic.api.domain.SpinmaticOpenGameData;
import com.spinmatic.api.domain.SpinmaticOpenGameRequest;
import com.spinmatic.api.domain.SpinmaticOpenGameResponse;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponse;

/**
 * Implementation for the Spinmatic integration service
 * 
 * @author Alexandre
 *
 */
public class SpinmaticServiceImpl implements SpinmaticService {
	private static final Logger logger = LoggerFactory.getLogger(SpinmaticServiceImpl.class);
	
	private HttpConnectionManager connectionManager;
	private long timeout;
	private int connectionTimeout;
	
	private Map<String, SpinmaticHandler> handlers;
	
	@Override
	public SpinmaticResponse processRequest(SpinmaticRequest request) {
		try {
			SpinmaticResponse response = this.handlers.get(request.getAction()).handle(request);
			return response;
		} catch (GameRequestNotExistentException e) {
			SpinmaticErrorResponse response = new SpinmaticErrorResponse();
			response.setMessage(e.getErrorMessage());
			response.setResponse(false);
			response.setStatus(SpinmaticStatus.ERROR_MESSAGE);
			
			return response;
		}
	}

	@Override
	public SpinmaticOpenGameData getOpenGameData(int idGame, String username, String currency, long idPlayer) {
		HttpClient client = new HttpClient(this.connectionManager);

		HttpClientParams params = client.getParams();
		params.setConnectionManagerTimeout(timeout);
		params.setSoTimeout(connectionTimeout);

		client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, true));

		PostMethod method = null;
		
		try {
			boolean playerLoggedIn = this.loginPlayer(username, idPlayer, currency);
			
			if (playerLoggedIn) {
				
				SpinmaticOpenGameRequest openGameRequest = new SpinmaticOpenGameRequest();

				String apiURL = ApplicationProperties.getProperty("spinmatic.api.url");
				Integer siteID = Integer.valueOf(ApplicationProperties.getProperty("spinmatic.api.siteid"));
				String privateKey = ApplicationProperties.getProperty("spinmatic.api.privatekey");
				String publicKey = ApplicationProperties.getProperty("spinmatic.api.publickey");
				String accessKey = ApplicationProperties.getProperty("spinmatic.api.accesskey");
				Integer ownerID = Integer.valueOf(ApplicationProperties.getProperty("spinmatic.api.ownerid"));
				Long externalId = idPlayer;
				
				openGameRequest.setSiteId(siteID);
				openGameRequest.setPrivateKey(privateKey);
				openGameRequest.setPublicKey(publicKey);
				openGameRequest.setAccessKey(accessKey);
				openGameRequest.setOwnerId(ownerID);
				openGameRequest.setUsername(username);
				openGameRequest.setExternalId(externalId);
				openGameRequest.setGameId(idGame);
				openGameRequest.setCurrency(currency);
				
				ObjectMapper objectMapper = new ObjectMapper();
				
				String openGameJson = objectMapper.writeValueAsString(openGameRequest);
				
				logger.debug("Open game request is " + openGameRequest.toString());

				method = new PostMethod(apiURL);
				method.setRequestEntity(new StringRequestEntity(openGameJson,"application/json","UTF-8"));
				
				int statusCode = client.executeMethod(method);

				if (statusCode != HttpStatus.SC_OK) {
					logger.error("Response to getGame not OK status code. Returned code is " + statusCode);
					return null;
				}

				byte[] responseBody = method.getResponseBody();

				String openGameMessage = new String(responseBody);
				
				logger.debug("Open game response message is " + openGameMessage);
				
				SpinmaticOpenGameResponse response = (SpinmaticOpenGameResponse) this.serializeObjectFromJSON(openGameMessage, SpinmaticOpenGameResponse.class);
				
				String gameURL = response.getResponse().getLobbyUrl();
				String connectionID = response.getResponse().getCon_id();
				
				SpinmaticOpenGameData openGameData = new SpinmaticOpenGameData();
				openGameData.setLobbyURL(gameURL);
				openGameData.setSessionID(connectionID);
				
				return openGameData;
			}
		} catch (HttpException e) {
			logger.error("Fatal protocol violation", e);
			System.err.println("Fatal protocol violation: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Fatal transport error:", e);
		} finally {
			// Release the connection.
			if (method != null)
				method.releaseConnection();
		}
		return null;
	}
	
	/**
	 * @param username username of the player
	 * @param idPlayer id of the player in gpi side
	 * @param currency of the player
	 * @return if the player was correctly logged in
	 */
	private boolean loginPlayer(String username, long idPlayer, String currency) {
		String apiURL = ApplicationProperties.getProperty("spinmatic.api.url");
		Integer siteID = Integer.valueOf(ApplicationProperties.getProperty("spinmatic.api.siteid"));
		String privateKey = ApplicationProperties.getProperty("spinmatic.api.privatekey");
		String publicKey = ApplicationProperties.getProperty("spinmatic.api.publickey");
		String accessKey = ApplicationProperties.getProperty("spinmatic.api.accesskey");
		Integer ownerID = Integer.valueOf(ApplicationProperties.getProperty("spinmatic.api.ownerid"));
		String password = ApplicationProperties.getProperty("spinmatic.api.password");
		Long externalId = idPlayer;
		
		SpinmaticLoginRequest loginRequest = new SpinmaticLoginRequest();
		
		loginRequest.setPrivateKey(privateKey);
		loginRequest.setPublicKey(publicKey);
		loginRequest.setAccessKey(accessKey);
		loginRequest.setOwnerId(ownerID);
		loginRequest.setUsername(username);
		loginRequest.setPassword(password);
		loginRequest.setExternalId(externalId);
		loginRequest.setSiteId(siteID);
		loginRequest.setCurrency(currency);
		
		HttpClient client = new HttpClient(this.connectionManager);

		HttpClientParams params = client.getParams();
		params.setConnectionManagerTimeout(timeout);
		params.setSoTimeout(connectionTimeout);

		client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, true));

		PostMethod method = null;
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		try {
			String loginJson = objectMapper.writeValueAsString(loginRequest);
			
			method = new PostMethod(apiURL);
			method.setRequestEntity(new StringRequestEntity(loginJson,"application/json","UTF-8"));
			
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				logger.error("Response to login not OK status code. Returned code is " + statusCode);
				return false;
			}

			byte[] responseBody = method.getResponseBody();
			
			String loginPlayerMessage = new String(responseBody);
			
			logger.debug("Login player response message is " + loginPlayerMessage);
			
			SpinmaticCheckPlayerResponse response = (SpinmaticCheckPlayerResponse) this.serializeObjectFromJSON(loginPlayerMessage, SpinmaticCheckPlayerResponse.class);
			
			boolean success = SpinmaticStatus.SUCCESS_MESSAGE.equalsIgnoreCase(response.getStatus());
			return success;
		} catch (JsonGenerationException e) {
			logger.error("JsonGenerationException", e);
		} catch (JsonMappingException e) {
			logger.error("JsonMappingException", e);
		} catch (IOException e) {
			logger.error("IOException", e);
		}
		
		return false;
		
	}
	
	/**
	 * @param message json message
	 * @return serialized object with the response
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Object serializeObjectFromJSON(String message, Class type) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
			Object spinmaticResponse = objectMapper.readValue(message, type);
			return spinmaticResponse;
		} catch (IOException e) {
			logger.error("IOException", e);
		}

		return null;
	}

	
	/**
	 * @param timeout the connection manager timeout to set
	 */
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	/**
	 * @param connectionTimeout the socket timeout to set
	 */
	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	/**
	 * @param connectionManager the connectionManager to set
	 */
	public void setConnectionManager(HttpConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	public void setHandlers(Map<String, SpinmaticHandler> handlers) {
		this.handlers = handlers;
	}
}
