package com.gpi.service.gpi.validation;

import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.evenbet.domain.EvenbetRequest;
import com.evenbet.domain.EvenbetRequestHeader;
import com.google.common.hash.Hashing;
import com.gpi.utils.ApplicationProperties;

/**
 * Message Validator Utils for Evenbet
 * @author f.fernandez
 *
 */
public class EvenbetMessageValidator {

	private static final Logger logger = LoggerFactory.getLogger(EvenbetMessageValidator.class);

	private static final String PROPERTY_API_SECRET = "evenbet.api.secret";
	
	private static final String PROPERTY_WALLET_API_SECRET = "evenbet.api.wallet.secret";
	
	public boolean validateRequestMessage(EvenbetRequestHeader header, EvenbetRequest request) {
		String signedMessage = caluclateWalletSignature(request.getRawJson());
		return signedMessage != null && signedMessage.equals(header.getSign());
	}
	
	public String calculateSignature(String body){
		String secret = ApplicationProperties.getProperty(PROPERTY_API_SECRET);
		return caluculateHashSHA256(body, secret);
	}
	
	private String caluclateWalletSignature(String body) {
		String secret = ApplicationProperties.getProperty(PROPERTY_WALLET_API_SECRET);
		return caluculateHashSHA256(body, secret);		
	}
	
	private String caluculateHashSHA256(String body, String secret) {
		String signature = null;
		try {
			return Hashing.sha256().hashString(body+secret, StandardCharsets.UTF_8).toString();
		}catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
		return signature;
	}
	
}