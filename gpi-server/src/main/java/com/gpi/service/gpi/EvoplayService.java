package com.gpi.service.gpi;

import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.evoplay.api.domain.EvoplayResponse;

/**
 * Service for the Evoplay integration
 * 
 * @author Alexandre
 *
 */
public interface EvoplayService {

	/**
	 * @param request request received from evoplay to be processed
	 * @return response to return to evoplay
	 */
	EvoplayResponse processRequest(EvoplayCallbackRequest request);
	
	/**
	 * @param idGame id of the game to get url
	 * @param token token
	 * @param currency used currency
	 * @param lang language of the game
	 * @param homeURL url to be used in home parameter
	 * @return url to open a game
	 */
	String getGameURL(int idGame, String token, String currency, String lang, String homeURL);
	
}
