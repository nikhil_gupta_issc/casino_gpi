package com.gpi.service.gpi.validation;

import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.booming.domain.AbstractBoomingRequest;
import com.booming.domain.BoomingRequest;
import com.booming.domain.BoomingRequestHeader;
import com.google.common.hash.Hashing;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.MACUtils;
import com.gpi.utils.cache.CacheManager;

/**
 * Message Validator Utils for Booming
 * @author f.fernandez
 *
 */
public class BoomingMessageValidator {

	private static final Logger logger = LoggerFactory.getLogger(BoomingMessageValidator.class);

	private static final String PROPERTY_KEY = "booming.api.key.";
	private static final String PROPERTY_SECRET = "booming.api.secret.";
	
	private GameRequestDao gameRequestDao;
	
	private CacheManager cacheManager;
	
	public String getApiKey(String pn) {
		logger.debug("Booming: Using api key for application property '" + PROPERTY_KEY+pn + "'");
		return ApplicationProperties.getProperty(PROPERTY_KEY+pn);
	}
	
	public boolean validateRequestMessage(BoomingRequestHeader header, AbstractBoomingRequest request) throws GameRequestNotExistentException {
			BoomingRequest boomingRequest = (BoomingRequest)request;
			String token = 	(String)cacheManager.getAndTouch(CacheConstants.BOOMING_TOKEN_KEY + "-" + boomingRequest.getSessionId(), CacheConstants.BOOMING_TOKEN_TTL);
			GameRequest gameRequest = gameRequestDao.getGameRequest(token);			
			
			String pn = gameRequest.getPublisher().getName();
			String currentSignature = this.calculateSignature(pn, header.getPath(), header.getNonce(), request.getRawJson());
			return currentSignature != null && currentSignature.equals(header.getSignature());
	}
	
	public String calculateSignature(String pn, String path, String nonce, String body){
		String signature = null;
		try {
			logger.debug("Booming: Calculating signature for application property '" + PROPERTY_SECRET+pn + "'");
			return MACUtils.calculateHMAC(path + nonce + Hashing.sha256().hashString(body, StandardCharsets.UTF_8).toString(), ApplicationProperties.getProperty(PROPERTY_SECRET+pn), MACUtils.HMAC_SHA512);
		}catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
		return signature;
	}

	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}

	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	
	
}