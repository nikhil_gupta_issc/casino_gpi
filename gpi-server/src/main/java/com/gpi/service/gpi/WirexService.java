package com.gpi.service.gpi;

import java.util.Map;

import it.wirex.api.WirexMethod;
import it.wirex.api.domain.WirexResponse;

/**
 * Service for the Wirex integration
 * 
 * @author Alexandre
 *
 */
public interface WirexService {
	/**
	 * Process a request received by Wirex
	 * @param sessionId id of the session. null if not present on call
	 * @param params map of key, value parameters
	 * @param method method called
	 * @return response ready to be returned to Wirex
	 */
	WirexResponse processRequest(String sessionId, Map<String, String> params, WirexMethod method);
	
	/**
	 * @param token token of the player
	 * @param idGame name of the game in Wirex system
	 * @param currency currency of the game
	 * @param language language of the game
	 * @return url to open a game in charged mode
	 */
	String getGameURL(String token, String idGame, String currency, String language);
	
	/**
	 * @param idGame name of the game in Wirex system
	 * @param language language of the game
	 * @return currency currency of the game
	 */
	//String getFreeGameURL(String idGame, String language, String currency);
}
