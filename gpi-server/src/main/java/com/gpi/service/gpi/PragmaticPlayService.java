package com.gpi.service.gpi;

import java.util.Map;

import com.pragmaticplay.api.PragmaticPlayMethod;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

/**
 * Service for the Pragmatic Play integration
 * 
 * @author Alexandre
 *
 */
public interface PragmaticPlayService {
	/**
	 * Process a request received by Pragmatic Play
	 * @param params map of key, value parameters
	 * @param method method called
	 * @return response ready to be returned to Pragmatic Play
	 */
	PragmaticPlayResponse processRequest(Map<String, String> params, PragmaticPlayMethod method);
	
	/**
	 * @param token token of the player
	 * @param idGame name of the game in Pragmatic Play system
	 * @param language language of the game
	 * @param publisher publisher name doing the request
	 * @param lobbyURL url to open the publisher lobby
	 * @return url to open a game in charged mode
	 */
	String getGameURL(String token, String idGame, String language, String publisher, String lobbyURL);
	
	/**
	 * @param idGame name of the game in Pragmatic Play system
	 * @param language language of the game
	 * @param lobbyURL url to open the publisher lobby
	 * @return currency currency of the game
	 */
	String getFreeGameURL(String idGame, String language, String currency, String lobbyURL);
}
