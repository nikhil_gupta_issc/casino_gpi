package com.gpi.service.gpi;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.habanero.api.HabaneroMethodType;
import com.habanero.domain.HabaneroRequest;
import com.habanero.domain.HabaneroResponse;

/**
 * Service for the Habanero integration
 * 
 * @author f.fernandez
 *
 */
public interface HabaneroService {

	HabaneroResponse processRequest(HabaneroRequest request, HabaneroMethodType method) throws GameRequestNotExistentException, TransactionNotSuccessException ;
	
}
