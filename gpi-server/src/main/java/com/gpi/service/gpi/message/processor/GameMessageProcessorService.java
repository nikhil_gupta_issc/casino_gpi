package com.gpi.service.gpi.message.processor;

import com.gpi.api.Message;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.*;

public interface GameMessageProcessorService {

    Message processMessage(Message messageFromGame, GameRequest gameRequest) throws GameProviderNotExistentException, NonExistentGameException, InvalidFreeSpinValue, NoFreeSpinLeftException, GamePlatformIntegrationException;
}
