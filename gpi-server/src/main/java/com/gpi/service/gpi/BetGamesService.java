package com.gpi.service.gpi;

import com.bet.games.api.Root;
import com.gpi.api.Message;
import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.GameUrlNotDefinedException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

/**
 * Service to process messages received from BetGames
 * 
 * @author Alexandre
 *
 */
public interface BetGamesService {

	/**
	 * Processes a message received from a game on BetGames
	 * @param messageFromGame message received
	 * @return response to return to the call message
	 * @throws GameRequestNotExistentException
	 */
    Root processMessage(Root messageFromGame) throws GameRequestNotExistentException;
}
