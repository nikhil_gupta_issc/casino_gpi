package com.gpi.service.gpi.impl;

import java.io.IOException;
import java.net.URLEncoder;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.evoplay.api.domain.EvoplayCallbackRequest;
import com.evoplay.api.domain.EvoplayErrorData;
import com.evoplay.api.domain.EvoplayErrorResponse;
import com.evoplay.api.domain.EvoplayGetGameURLResponse;
import com.evoplay.api.domain.EvoplayResponse;
import com.evoplay.api.validation.EvoplayRequestValidator;
import com.evoplay.api.validation.signature.EvoplayGetGameURLSignatureBuilder;
import com.google.common.net.HttpHeaders;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.evoplay.EvoplayCallbackProcessor;
import com.gpi.service.gpi.EvoplayService;
import com.gpi.utils.ApplicationProperties;

/**
 * Default implementation for the Evoplay integration service
 * 
 * @author Alexandre
 *
 */

public class EvoplayServiceImpl implements EvoplayService {
	private EvoplayRequestValidator requestValidator;
	
	private static final Logger logger = LoggerFactory.getLogger(EvoplayServiceImpl.class);
	
	private HttpConnectionManager connectionManager;
	private long timeout;
	private int connectionTimeout;
	
	private EvoplayCallbackProcessor callbackProcessor;
	
	@Override
	public EvoplayResponse processRequest(EvoplayCallbackRequest request) {
		if (!requestValidator.validate(request)) {
			EvoplayErrorResponse response = new EvoplayErrorResponse();
			response.setStatus("error");
			EvoplayErrorData errorData = new EvoplayErrorData();
			errorData.setMessage("invalid signature");
			errorData.setNoRefund(1);
			errorData.setScope("internal");
			
			response.setError(errorData);
			
			return response;
		}
		
		try {
			return this.callbackProcessor.processRequest(request);
		} catch (GameRequestNotExistentException e) {
			EvoplayErrorResponse response = new EvoplayErrorResponse();
			response.setStatus("error");
			EvoplayErrorData errorData = new EvoplayErrorData();
			errorData.setMessage("game request does not exist");
			errorData.setNoRefund(1);
			
			response.setError(errorData);
			
			return response;
		}
	}

	@Override
	public String getGameURL(int idGame, String token, String currency, String lang, String homeURL) {
		HttpClient client = new HttpClient(this.connectionManager);

		HttpClientParams params = client.getParams();
		params.setConnectionManagerTimeout(timeout);
		params.setSoTimeout(connectionTimeout);

		client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, true));

		GetMethod method = null;
		
		try {
			String apiURL = ApplicationProperties.getProperty("evoplay.api.url");
			Integer projectNumber = Integer.valueOf(ApplicationProperties.getProperty("evoplay.project.number"));
			Integer apiVersion = Integer.valueOf((ApplicationProperties.getProperty("evoplay.api.version")));
			Integer callbackVersion = Integer.valueOf(ApplicationProperties.getProperty("evoplay.api.callbackversion"));

			EvoplayGetGameURLSignatureBuilder signatureBuilder = new EvoplayGetGameURLSignatureBuilder();
			signatureBuilder.setProjectNumber(projectNumber);
			signatureBuilder.setVersion(apiVersion);
			signatureBuilder.setGame(idGame);
			signatureBuilder.setToken(token);
			
			if (homeURL != null && !homeURL.isEmpty()) {
				signatureBuilder.setExitURL(homeURL);
			}
			signatureBuilder.setLanguage(lang);
			signatureBuilder.setHttps(1);
			signatureBuilder.setDenomination("default");
			signatureBuilder.setReturnURLInfo(1);
			signatureBuilder.setCurrency(currency);
			signatureBuilder.setCallbackVersion(callbackVersion);
			
			String signature = signatureBuilder.buildSignature();
			
			signature = EvoplayRequestValidator.getHashedSignature(signature);
			
			StringBuilder sb = new StringBuilder();
			sb.append(apiURL).append("/Game/getURL?");
			sb.append("project=").append(projectNumber);
			sb.append("&version=").append(apiVersion);
			sb.append("&signature=").append(signature);
			sb.append("&token=").append(token);
			sb.append("&game=").append(idGame);
			
			if (homeURL != null && !homeURL.isEmpty()) {
				sb.append("&").append(URLEncoder.encode("settings[exit_url]", "UTF-8")).append("=").append(URLEncoder.encode(homeURL, "UTF-8"));
			}
			
			sb.append("&").append(URLEncoder.encode("settings[language]", "UTF-8")).append("=").append(lang);
			sb.append("&").append(URLEncoder.encode("settings[https]", "UTF-8")).append("=1");
			sb.append("&denomination=default");
			sb.append("&currency=").append(currency);
			sb.append("&return_url_info=1");
			sb.append("&callback_version=").append(callbackVersion);
			
			logger.debug("Request to send to evoplay is " + sb.toString());
			
			method = new GetMethod(sb.toString());
			
			method.setRequestHeader(HttpHeaders.USER_AGENT, "");
			method.setRequestHeader("http_user_agent", "");
			
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				logger.error("Response to getGame not OK status code. Returned code is " + statusCode);
			}

			byte[] responseBody = method.getResponseBody();

			String gameURLMessage = new String(responseBody);
			
			logger.debug("Response message is " + gameURLMessage);
			
			EvoplayGetGameURLResponse response = this.serializeObjectFromJSON(gameURLMessage);
			
			String gameURL = response.getData().getLink();
			
			return gameURL;
		} catch (HttpException e) {
			logger.error("Fatal protocol violation", e);
			System.err.println("Fatal protocol violation: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Fatal transport error:", e);
		} finally {
			// Release the connection.
			if (method != null)
				method.releaseConnection();
		}
		return null;
	}
	
	/**
	 * @param message json message
	 * @return serialized object with the response
	 */
	private EvoplayGetGameURLResponse serializeObjectFromJSON(String message) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
			EvoplayGetGameURLResponse gameResponse = objectMapper.readValue(message, EvoplayGetGameURLResponse.class);
			return gameResponse;
		} catch (IOException e) {
			logger.error("IOException", e);
		}

		return null;
	}

	/**
	 * @param timeout the connection manager timeout to set
	 */
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	/**
	 * @param connectionTimeout the socket timeout to set
	 */
	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	/**
	 * @param connectionManager the connectionManager to set
	 */
	public void setConnectionManager(HttpConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	/**
	 * @param requestValidator the requestValidator to set
	 */
	public void setRequestValidator(EvoplayRequestValidator requestValidator) {
		this.requestValidator = requestValidator;
	}

	public void setCallbackProcessor(EvoplayCallbackProcessor callbackProcessor) {
		this.callbackProcessor = callbackProcessor;
	}
}
