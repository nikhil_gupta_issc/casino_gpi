package com.gpi.service.gpi;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.InvalidCredentialsException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.wazdan.api.WazdanMethodType;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanRequestHeader;
import com.wazdan.domain.WazdanResponse;

/**
 * Service for the Wazdan integration
 * 
 * @author f.fernandez
 *
 */
public interface WazdanService {

	WazdanResponse processRequest(WazdanRequestHeader requestHeader, WazdanRequest wazdanRequest, WazdanMethodType method) throws GameRequestNotExistentException, TransactionNotSuccessException, InvalidCredentialsException;
	
}
