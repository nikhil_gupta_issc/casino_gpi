package com.gpi.service.gpi;

import java.security.SignatureException;

import com.booming.api.BoomingMethodType;
import com.booming.domain.AbstractBoomingRequest;
import com.booming.domain.BoomingRequestHeader;
import com.booming.domain.AbstractBoomingResponse;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.TransactionNotSuccessException;

/**
 * Service for the Booming integration
 * 
 * @author f.fernandez
 *
 */
public interface BoomingService {

	AbstractBoomingResponse processRequest(BoomingRequestHeader header, AbstractBoomingRequest request, BoomingMethodType method) throws GameRequestNotExistentException, TransactionNotSuccessException, SignatureException ;
	
	String getGameURL(String token, String pn, String gameName, String lang, String type, String device)  throws GameRequestNotExistentException, NonExistentGameException;
	
}
