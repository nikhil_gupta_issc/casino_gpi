package com.gpi.service.gpi;

import com.spinmatic.api.domain.SpinmaticOpenGameData;
import com.spinmatic.api.domain.SpinmaticRequest;
import com.spinmatic.api.domain.SpinmaticResponse;

/**
 * Service for the Spinmatic integration
 * 
 * @author Alexandre
 *
 */
public interface SpinmaticService {

	/**
	 * @param request request received from spinmatic to be processed
	 * @return response to return to spinmatic
	 */
	SpinmaticResponse processRequest(SpinmaticRequest request);
	
	/**
	 * @param idGame id of the game to get url
	 * @param username player username
	 * @param currency player currency
	 * @param idPlayer player id in gpi
	 * @return url to open a game
	 */
	SpinmaticOpenGameData getOpenGameData(int idGame, String username, String currency, long idPlayer);
	
}
