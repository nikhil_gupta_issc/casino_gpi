package com.gpi.service.gpi;

import java.security.SignatureException;

import com.evenbet.api.EvenbetMethodType;
import com.evenbet.domain.EvenbetRequest;
import com.evenbet.domain.EvenbetRequestHeader;
import com.evenbet.domain.EvenbetResponse;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.TransactionNotSuccessException;

/**
 * Service for the Evenbet integration
 * 
 * @author f.fernandez
 *
 */
public interface EvenbetService {

	EvenbetResponse processRequest(EvenbetRequestHeader header, EvenbetRequest request, EvenbetMethodType method) throws GameRequestNotExistentException, TransactionNotSuccessException, SignatureException ;
	
	String getGameURL(String token, String pn, String gameName, String lang, String type, String lobbyUrl)  throws GameRequestNotExistentException, NonExistentGameException;
	
}
