package com.gpi.service.gpi.validation;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.utils.ApplicationProperties;
import com.wazdan.domain.WazdanRequestHeader;

/**
 * Message Validator Utils for Wazdan
 * @author f.fernandez
 *
 */
public class WazdanMessageValidator {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(WazdanMessageValidator.class);

	private static final String API_AUTH_TYPE = "wazdan.api.auth.type";
	private static final String API_AUTH_CREDENTIALS = "wazdan.api.auth.credentials";
		
	public boolean validateRequestMessage(WazdanRequestHeader header) {
		String type = ApplicationProperties.getProperty(API_AUTH_TYPE);
		String credentials = ApplicationProperties.getProperty(API_AUTH_CREDENTIALS);
		
		if ("Basic".equals(type)) {
			if (header.getAuthorization() != null) {
				String[] authentication = header.getAuthorization().split(" ");
				if (type.equals(authentication[0])) {
					return credentials != null && credentials.equals(authentication[1]);
				}
			}
			return false;
		}
		
		throw new NotImplementedException("Authentication type " + type + " not implemented");
	}
	
	
}