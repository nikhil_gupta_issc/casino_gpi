package com.gpi.service.gpi.validation;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bet.games.api.BetGamesErrorCodes;
import com.bet.games.api.BetGamesErrorMessages;
import com.bet.games.api.BetGamesMethodType;
import com.bet.games.api.Root;
import com.bet.games.api.validation.BetGamesSignatureBuilder;
import com.gpi.api.Message;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.token.TokenService;
import com.gpi.utils.MessageHelper;

/**
 * Validator for messages received from Bet Games
 * 
 * @author Alexandre
 *
 */
public class BetGamesMessageValidator {

	private static final Logger logger = LoggerFactory.getLogger(BetGamesMessageValidator.class);

	private GameRequestDao gameRequestDao;

	/**
	 * Returns the hash of the signature of a message
	 * @param message message to get signature
	 * @param type type of the call. One of {@link BetGamesMethodType}
	 * @return hash of the signature of a message
	 */
	public String getSignatureHash(Root message, BetGamesMethodType type) {

		BetGamesSignatureBuilder signatureBuilder = new BetGamesSignatureBuilder(message, type);
		String signature = signatureBuilder.build();
		logger.info("Signature String: {}", signature);
		String signatureHash = getMD5Hash(signature);

		return signatureHash;
	}

	/**
	 * Check if the message that was received is valid
	 * @param messageFromGame message from game to check validity
	 * @param returnMessage message to return
	 * @return true if the message is valid; false otherwise
	 */
	public boolean validateRequestMessage(Root messageFromGame, Root returnMessage) {
		if (!validateSignature(messageFromGame, BetGamesMethodType.REQUEST)) {
			returnMessage.setErrorCode(BigInteger.valueOf(BetGamesErrorCodes.INVALIDSIGNATURE_ERROR));
			returnMessage.setErrorText(BetGamesErrorMessages.INVALIDSIGNATURE_MESSAGE);

			return false;
		}
		else if (!validateTime(messageFromGame)) {
			returnMessage.setErrorCode(BigInteger.valueOf(BetGamesErrorCodes.EXPIREDMESSAGE_ERROR));
			returnMessage.setErrorText(BetGamesErrorMessages.EXPIREDMESSAGE_MESSAGE);

			return false;
		}
		else if (!validateToken(messageFromGame)) {
			returnMessage.setErrorCode(BigInteger.valueOf(BetGamesErrorCodes.INVALIDTOKEN_ERROR));
			returnMessage.setErrorText(BetGamesErrorMessages.INVALIDTOKEN_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Check if the token of the message is valid 
	 * @param message message to check token validity
	 * @return true if the token of the message is valid; false otherwise
	 */
	public boolean validateToken(Root message) {
		String token = message.getToken();

		// not care of token in these methods
		if (message.getMethod().equals("ping") || message.getMethod().equals("transaction_bet_payout"))
			return true;

		try {
			if (TokenService.isValidToken(token)) {
				// just to throw an exception if the game request doesnt exist
				gameRequestDao.getGameRequest(token);
				return true;
			}
		} catch (GameRequestNotExistentException ex) {
			return false;
		}

		return false;
	}

	/**
	 * Check if the time of the message didnt expire
	 * @param message message to check time validity
	 * @return true if the time of the message didnt expire; false otherwise
	 */
	private boolean validateTime(Root message) {
		long currentTime = new Date().getTime() / 1000;
		long messageTime = message.getTime();

		return (currentTime-messageTime <= CacheConstants.BETGAMES_TIME_EXPIRATION);
	}

	/**
	 * Return md5 hash of a message
	 * @param message message to get md5 hash of
	 * @return md5 hash of a message
	 */
	private String getMD5Hash(String message) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(message.getBytes());
			byte[] digest = md.digest();
			String signatureHash = DatatypeConverter.printHexBinary(digest).toLowerCase();

			return signatureHash;
		} catch (NoSuchAlgorithmException e) {
			logger.error("No such algorithm", e);
		}

		return null;
	}

	/**
	 * Check if the signature of a message is valid
	 * @param request message to check signature validity
	 * @param type type of the call. One of {@link BetGamesMethodType}
	 * @return true if the signature of the message is valid; false otherwise
	 */
	private boolean validateSignature(Root request, BetGamesMethodType type) {
		String signatureHash = getSignatureHash(request, type);

		return signatureHash.equals(request.getSignature());
	}

	/**
	 * Check if the response message returned from GPI contains any error. If it contains errors, it adds the errors to the message to be returned by Bet Games Service
	 * @param gpiResponseMessage message returned from GPI
	 * @param returnMessage message to return by Bet Games Service
	 * @param method method called
	 * @return if the response message returned from GPI does not contains errors
	 */
	public boolean validateResponseMessage(Message gpiResponseMessage, Root returnMessage, String method) {

		if ("transaction_bet_payin".equals(method) || "transaction_bet_payout".equals(method)) {

			if ("transaction_bet_payout".equals(method) && gpiResponseMessage.getResult().getSuccess() == 0 && gpiResponseMessage.getResult().getReturnset().getErrorCode() != null) {
				if (Integer.valueOf(MessageHelper.PREVIOUS_ROUND_NOT_EXISTS_ERROR_CODE).equals(gpiResponseMessage.getResult().getReturnset().getErrorCode().getValue()) || Integer.valueOf(MessageHelper.TRANSACTION_NOT_FOUND_ERROR_CODE).equals(gpiResponseMessage.getResult().getReturnset().getErrorCode().getValue())) {

					// no previous bet transaction associated with the same round
					returnMessage.setErrorCode(BigInteger.valueOf(BetGamesErrorCodes.NOPAYIN_ERROR));
					returnMessage.setErrorText(BetGamesErrorMessages.NOPAYIN_MESSAGE);
					return false;
				}
			}
			else if (gpiResponseMessage.getResult().getSuccess() == 0 && gpiResponseMessage.getResult().getReturnset().getAlreadyProcessed() != null && gpiResponseMessage.getResult().getReturnset().getAlreadyProcessed().isValue()) {
				// already processed. no need to add errors to the message
				return false;
			}
		}

		if (gpiResponseMessage.getResult().getSuccess() == 0) {
			returnMessage.setErrorCode(BigInteger.valueOf(gpiResponseMessage.getResult().getReturnset().getErrorCode().getValue()));
			returnMessage.setErrorText(gpiResponseMessage.getResult().getReturnset().getError().getValue());
			return false;
		}

		return true;
	}

	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}
	
}
