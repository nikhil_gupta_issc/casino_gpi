package com.gpi.service.gpi.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.utils.ApplicationProperties;
import com.habanero.domain.HabaneroAuth;

/**
 * Message Validator Utils for Habanero
 * @author f.fernandez
 *
 */
public class HabaneroMessageValidator {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(HabaneroMessageValidator.class);

	private static final String PROPERTY_PASS_KEY = "habanero.pass.key";
		
	public boolean validateRequestMessage(HabaneroAuth auth) {
		String passkey = ApplicationProperties.getProperty(PROPERTY_PASS_KEY);
		if (auth != null && auth.getPasskey() != null && passkey != null){
			return passkey.equals(auth.getPasskey());
		}
		return false;
	}
	
	
}