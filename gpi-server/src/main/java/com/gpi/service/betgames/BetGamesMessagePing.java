package com.gpi.service.betgames;

import java.math.BigInteger;

import com.bet.games.api.Root;
import com.gpi.exceptions.GameRequestNotExistentException;

public class BetGamesMessagePing extends BetGamesMessageCommand {

	@Override
	public void execute(Root messageFromGame, Root returnMessage, String method)
			throws GameRequestNotExistentException {
		returnMessage.setSuccess(BigInteger.valueOf(1));
		returnMessage.setToken("-");

	}

}
