package com.gpi.service.betgames;

import java.math.BigInteger;

import com.bet.games.api.Root;
import com.gpi.constants.CacheConstants;
import com.gpi.exceptions.GameRequestNotExistentException;

public class BetGamesMessageRefreshToken extends BetGamesMessageCommand {

	@Override
	public void execute(Root messageFromGame, Root returnMessage, String method)
			throws GameRequestNotExistentException {
		gameRequestDao.refreshGameRequest(messageFromGame.getToken(), CacheConstants.BETGAMES_TOKEN_TTL);
		this.updateTokenUserMappingTTL(messageFromGame.getToken());

		returnMessage.setSuccess(BigInteger.valueOf(1));

	}

}
