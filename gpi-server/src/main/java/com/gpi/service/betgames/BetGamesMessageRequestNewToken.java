package com.gpi.service.betgames;

import java.math.BigInteger;

import com.bet.games.api.Params;
import com.bet.games.api.Root;
import com.gpi.constants.CacheConstants;
import com.gpi.exceptions.GameRequestNotExistentException;

public class BetGamesMessageRequestNewToken extends BetGamesMessageCommand {

	@Override
	public void execute(Root messageFromGame, Root returnMessage, String method)
			throws GameRequestNotExistentException {
		
		// just update the ttl
		gameRequestDao.refreshGameRequest(messageFromGame.getToken(), CacheConstants.BETGAMES_TOKEN_TTL);
		this.updateTokenUserMappingTTL(messageFromGame.getToken());

		Params params = new Params();
		params.setNewToken(messageFromGame.getToken());

		returnMessage.setSuccess(BigInteger.valueOf(1));
		returnMessage.setParams(params);
	}

}
