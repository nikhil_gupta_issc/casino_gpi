package com.gpi.service.betgames;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bet.games.api.BetGamesErrorCodes;
import com.bet.games.api.BetGamesErrorMessages;
import com.bet.games.api.Params;
import com.bet.games.api.Root;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.GameRequest;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.transaction.TransactionType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.transaction.TransactionService;

public class BetGamesMessageMoneyTransaction extends BetGamesMessageCommand {

	private TransactionService transactionService;
	
    private static final Logger logger = LoggerFactory.getLogger(BetGamesMessageMoneyTransaction.class);
	
	@Override
	public void execute(Root messageFromGame, Root returnMessage, String method) throws GameRequestNotExistentException {
		MethodType gpiMethod;
		String token;
		if (method.equals("transaction_bet_payin")) {
			gpiMethod = MethodType.BET;
			token = messageFromGame.getToken();
		} else {
			gpiMethod = MethodType.WIN;
			token = (String)cacheManager.get(CacheConstants.BETGAMES_KEY+messageFromGame.getParams().getPlayerId());
			if (token != null) {
			
			logger.info("Getting token from playerid " + messageFromGame.getParams().getPlayerId() + ". Value is " + token);
			
			GameRequest gameRequest = gameRequestDao.getGameRequest(token);
			List<Transaction> roundTransactions = transactionService.getTransactionsByGameRoundId(0, 10, messageFromGame.getParams().getBetId(), gameRequest.getGame());
			
			boolean hasPreviousBet = false;
				if (roundTransactions!=null) {
					// search for previous WIN and BET transactions for same round id
					for (Transaction t : roundTransactions) {
						if (TransactionType.WIN.equals(t.getType())) {
							// there is already a payout for the current round id
							
							Message gpiFormattedMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
							Message responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);
							Long balance = responseMessage.getResult().getReturnset().getBalance().getValue();
							
							Params params = new Params();
							params.setBalanceAfter(balance);
							params.setAlreadyProcessed(BigInteger.valueOf(1));
							returnMessage.setParams(params);
							returnMessage.setSuccess(BigInteger.valueOf(1));
							gameRequestDao.refreshGameRequest(token, CacheConstants.BETGAMES_TOKEN_TTL);
							this.updateTokenUserMappingTTL(messageFromGame.getToken());
							return;
						}
						else if (TransactionType.BET.equals(t.getType())) {
							hasPreviousBet = true;
						}
					}
				}
				
				if (!hasPreviousBet) {
					Params params = new Params();
					returnMessage.setParams(params);
					returnMessage.setSuccess(BigInteger.valueOf(0));
					returnMessage.setErrorCode(BigInteger.valueOf(BetGamesErrorCodes.NOPAYIN_ERROR));
					returnMessage.setErrorText(BetGamesErrorMessages.NOPAYIN_MESSAGE);
					return;
				}
			} else {
				Params params = new Params();
				returnMessage.setParams(params);
				returnMessage.setSuccess(BigInteger.valueOf(0));
				returnMessage.setErrorCode(BigInteger.valueOf(BetGamesErrorCodes.NOPAYIN_ERROR));
				returnMessage.setErrorText(BetGamesErrorMessages.NOPAYIN_MESSAGE);
				return;
			}
		}
		
		Message gpiFormattedMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, messageFromGame.getParams().getAmount(),
				String.valueOf(messageFromGame.getParams().getTransactionId()), messageFromGame.getParams().getBetId(),
				gpiMethod);
		Message responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);

		long balance;
		boolean alreadyProcessed = false;

		if (responseMessage.getResult().getReturnset().getAlreadyProcessed() != null) {
			alreadyProcessed = responseMessage.getResult().getReturnset().getAlreadyProcessed().isValue();
		}

		// although an already processed message has success with value
		// 0 it should return success to the betgames
		if (messageValidator.validateResponseMessage(responseMessage, returnMessage, method)
				|| alreadyProcessed) {

			if (!alreadyProcessed) {
				balance = responseMessage.getResult().getReturnset().getBalance().getValue();
			}
			else {
				gpiFormattedMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null,
						MethodType.GET_BALANCE);
				responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);
				balance = responseMessage.getResult().getReturnset().getBalance().getValue();
			}

			Params params = new Params();
			params.setBalanceAfter(balance);
			params.setAlreadyProcessed(alreadyProcessed ? BigInteger.valueOf(1) : BigInteger.valueOf(0));

			returnMessage.setParams(params);
			returnMessage.setSuccess(BigInteger.valueOf(1));
			gameRequestDao.refreshGameRequest(token, CacheConstants.BETGAMES_TOKEN_TTL);
			this.updateTokenUserMappingTTL(messageFromGame.getToken());
		}

	}
	
	/**
	 * @param transactionService the transactionService to set
	 */
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

}
