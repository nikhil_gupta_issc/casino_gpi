package com.gpi.service.betgames;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bet.games.api.Root;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.gpi.validation.BetGamesMessageValidator;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.cache.CacheManager;

public abstract class BetGamesMessageCommand {
	
    private static final Logger logger = LoggerFactory.getLogger(BetGamesMessageMoneyTransaction.class);
	
	protected GamePlatformIntegrationService gamePlatformIntegrationService;
	
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;

	protected GameRequestDao gameRequestDao;

	protected BetGamesMessageValidator messageValidator;
	
	protected CacheManager cacheManager;

	public abstract void execute (Root messageFromGame, Root returnMessage, String method) throws GameRequestNotExistentException;
	
	/**
	 * Update current user mapping token cache TTL
	 * @param token token to update TTL
	 */
	protected void updateTokenUserMappingTTL(String token) {
		logger.debug("UPDATE TOKEN MAPPING TTL");
		logger.debug(""+cacheManager);
		logger.debug(token);
	
		String idPlayer = (String)cacheManager.getAndTouch(CacheConstants.BETGAMES_TOKEN_KEY + token, CacheConstants.BETGAMES_TOKEN_TTL);
		
		if (idPlayer != null)
			cacheManager.getAndTouch(CacheConstants.BETGAMES_KEY + idPlayer, CacheConstants.BETGAMES_TOKEN_TTL);
	}
	
	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		logger.debug("setCacheManager: cache>Manager: "+cacheManager);
		this.cacheManager = cacheManager;
	}
	
	/**
	 * @param gamePlatformIntegrationUtils the gamePlatformIntegrationUtils to set
	 */
	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}

	/**
	 * @param gamePlatformIntegrationService the gamePlatformIntegrationService to set
	 */
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	/**
	 * @param messageValidator the messageValidator to set
	 */
	public void setMessageValidator(BetGamesMessageValidator messageValidator) {
		this.messageValidator = messageValidator;
	}

}
