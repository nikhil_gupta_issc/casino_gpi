package com.gpi.service.betgames;

import java.math.BigInteger;

import com.bet.games.api.Params;
import com.bet.games.api.Root;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.PlayerIdentifierBuilder;

public class BetGamesMessageGetAccountDetails extends BetGamesMessageCommand {

	@Override
	public void execute(Root messageFromGame, Root returnMessage, String method)
			throws GameRequestNotExistentException {
		GameRequest gameRequest = gameRequestDao.getGameRequest(messageFromGame.getToken());
		
		Message gpiFormattedMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(messageFromGame.getToken(), null, null, null,
				MethodType.GET_PLAYER_INFO);
		Message responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);

		if (messageValidator.validateResponseMessage(responseMessage, returnMessage, "get_account_details")) {
			Params params = new Params();
			
			String username = responseMessage.getResult().getReturnset().getLoginName().getValue();
			
			params.setUsername(username);
			params.setCurrency(responseMessage.getResult().getReturnset().getCurrency().getValue());
			
			PlayerIdentifierBuilder playerIdentifierBuilder = new PlayerIdentifierBuilder();
			playerIdentifierBuilder.setPublisherName(gameRequest.getPublisher().getName());
			playerIdentifierBuilder.setUsername(username);
			params.setUserId(playerIdentifierBuilder.build());

			params.setInfo("-");

			returnMessage.setSuccess(BigInteger.valueOf(1));
			returnMessage.setParams(params);
			gameRequestDao.refreshGameRequest(messageFromGame.getToken(), CacheConstants.BETGAMES_TOKEN_TTL);
			
			// do a two way mapping to later use when getting the user from the token
			cacheManager.store(CacheConstants.BETGAMES_TOKEN_KEY + messageFromGame.getToken(), params.getUserId(), CacheConstants.BETGAMES_TOKEN_TTL);
			cacheManager.store(CacheConstants.BETGAMES_KEY+params.getUserId(), messageFromGame.getToken(), CacheConstants.BETGAMES_TOKEN_TTL);
		}
	}

}
