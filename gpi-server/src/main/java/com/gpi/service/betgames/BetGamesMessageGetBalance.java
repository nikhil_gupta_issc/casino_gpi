package com.gpi.service.betgames;

import java.math.BigInteger;

import com.bet.games.api.Params;
import com.bet.games.api.Root;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.exceptions.GameRequestNotExistentException;

public class BetGamesMessageGetBalance extends BetGamesMessageCommand {

	@Override
	public void execute(Root messageFromGame, Root returnMessage, String method)
			throws GameRequestNotExistentException {
		Message gpiFormattedMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(messageFromGame.getToken(), null, null, null,
				MethodType.GET_BALANCE);
		Message responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);

		long balance = responseMessage.getResult().getReturnset().getBalance().getValue();

		if (messageValidator.validateResponseMessage(responseMessage, returnMessage, "get_balance")) {
			Params params = new Params();
			params.setBalance(balance);

			returnMessage.setSuccess(BigInteger.valueOf(1));
			returnMessage.setParams(params);
			gameRequestDao.refreshGameRequest(messageFromGame.getToken(), CacheConstants.BETGAMES_TOKEN_TTL);
			this.updateTokenUserMappingTTL(messageFromGame.getToken());
		}

	}

}
