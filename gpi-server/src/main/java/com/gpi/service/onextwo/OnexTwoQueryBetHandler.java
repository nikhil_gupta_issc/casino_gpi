package com.gpi.service.onextwo;

import java.util.List;

import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.Game;
import com.gpi.domain.transaction.TransactionType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.onextwo.api.OnexTwoCodes;
import com.onextwo.api.OnexTwoMethod;
import com.onextwo.api.domain.OnexTwoQueryBetResponse;
import com.onextwo.api.domain.OnexTwoRequest;
import com.onextwo.api.domain.OnexTwoResponse;
import com.onextwo.api.domain.OnexTwoTransactionRequest;

/**
 * Handler for QueryBet of 1x2
 * 
 * @author Alexandre
 *
 */
public class OnexTwoQueryBetHandler extends OnexTwoHandler {
	
	@Override
	public OnexTwoResponse handle(OnexTwoRequest request, OnexTwoMethod method) throws GameRequestNotExistentException {
		Long roundId = Long.valueOf(((OnexTwoTransactionRequest)request).getF1x2ID());
		
		
		String playerInfoKey = this.getPlayerInfoKeyWithSessionId(request);
		PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
		String token = playerInfo.getToken();
		
		GameRequest gameRequest = gameRequestDao.getGameRequest(token);
		
		if (this.checkIfBetTransactionExists(roundId, gameRequest.getGame())) {
			OnexTwoQueryBetResponse response = new OnexTwoQueryBetResponse();
			response.setBetId(String.valueOf(roundId));
			return response;
		} else {
			OnexTwoResponse response = new OnexTwoResponse();
			response.setErrorCode(OnexTwoCodes.NO_BET_RECORD);
			response.setErrorDesc("no record of this bet");
			
			return response;
		}
		
	}
	
	private boolean checkIfBetTransactionExists(Long gameRoundId, Game game) {
		List<Transaction> transactions = this.transactionService.getTransactionsByGameRoundId(0, Integer.MAX_VALUE, gameRoundId, game);
		
		for (Transaction transaction : transactions) {
			if (TransactionType.BET.equals(transaction.getType())) {
				return true;
			}
		}
		
		return false;
	}
}
