package com.gpi.service.onextwo;

import java.util.List;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.Game;
import com.gpi.domain.transaction.TransactionType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.onextwo.api.OnexTwoMethod;
import com.onextwo.api.domain.OnexTwoRequest;
import com.onextwo.api.domain.OnexTwoResponse;

/**
 * Abstract handler to 1x2 integration
 * 
 * @author Alexandre
 *
 */
public abstract class OnexTwoHandler {
	protected GamePlatformIntegrationService gamePlatformIntegrationService;
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	protected PlayerInfoDao playerInfoDao;
	protected TransactionService transactionService;
	
	protected GameRequestDao gameRequestDao;
	
	protected static final int BALANCE_COEFFICIENT = 100;
	
	/**
	 * Handles a request from spinmatic
	 * @param request request sent by spinmatic
	 * @return response ready to be returned to spinmatic
	 * @throws GameRequestNotExistentException
	 */
	public abstract OnexTwoResponse handle(OnexTwoRequest request, OnexTwoMethod method) throws GameRequestNotExistentException;
	
	
	/**
	 * @param request request form gpi
	 * @return player info cache key constructed using the sessionid from spinmatic
	 */
	protected String getPlayerInfoKeyWithSessionId(OnexTwoRequest request)  {
		String sessionId = request.getSessionId();
		String playerInfoKey = getPlayerInfoKey(sessionId);
		
		return playerInfoKey;
	}
	
	/**
	 * @param key identifier of the player
	 * @return player info identifier in the 1x2 playerinfo cache
	 */
	public static String getPlayerInfoKey(String key) {
		return CacheConstants.ONEXTWO_PLAYERINFO_KEY + key;
	}
	
	protected void setPlayerInfoTokenAndStore(String key, String token, PlayerInfo playerInfo) {
		if (!token.trim().isEmpty()) {
			playerInfo.setToken(token);
			this.playerInfoDao.savePlayerInfo(key, playerInfo);
		}
	}
	
	protected String getBetTransactionId(Long gameRoundId, Game game) {
		String transactionId = null;
		List<Transaction> transactions = this.transactionService.getTransactionsByGameRoundId(0, Integer.MAX_VALUE, gameRoundId, game);
		
		for (Transaction t : transactions) {
			if (TransactionType.BET.equals(t.getType())) {
				transactionId = t.getGameTransactionId();
				return transactionId;
			}
		}
		
		return null;
	}
	
	protected boolean roundHasBet(Long roundId, Game game) {
		return this.getBetTransactionId(roundId, game) != null;
	}

	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	public void setPlayerInfoDao(PlayerInfoDao playerInfoDao) {
		this.playerInfoDao = playerInfoDao;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}


	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	} 
}
