package com.gpi.service.onextwo;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.transaction.TransactionType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.transaction.TransactionService;
import com.onextwo.api.OnexTwoCodes;
import com.onextwo.api.OnexTwoMethod;
import com.onextwo.api.domain.OnexTwoRequest;
import com.onextwo.api.domain.OnexTwoResponse;
import com.onextwo.api.domain.OnexTwoTransactionRequest;
import com.onextwo.api.domain.OnexTwoTransactionResponse;

/**
 * Handler for PlaceBet and SettleBet of 1x2
 * 
 * @author Alexandre
 *
 */
public class OnexTwoPlaceSettleBetHandler extends OnexTwoHandler {
	
	@Override
	public OnexTwoResponse handle(OnexTwoRequest request, OnexTwoMethod method) throws GameRequestNotExistentException {
		String playerInfoKey = this.getPlayerInfoKeyWithSessionId(request);
		PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
		String token = playerInfo.getToken();
		
		GameRequest gameRequest = gameRequestDao.getGameRequest(token);
		
		Double amount = Double.valueOf(((OnexTwoTransactionRequest)request).getAmount());
		Long roundId = Long.valueOf(((OnexTwoTransactionRequest)request).getF1x2ID());
		String transactionId;
		
		if (OnexTwoMethod.PLACE_VIRTUAL_BET.equals(amount) && BigDecimal.valueOf(amount).compareTo(BigDecimal.ZERO) == 0) {
			// freespin. generate uuid
			transactionId = "BET-0-" + roundId + "-" + UUID.randomUUID().toString();
		} else if (OnexTwoMethod.PLACE_VIRTUAL_BET.equals(method)) {
			transactionId = "BET-" + roundId;
		} else {
			transactionId = "WIN-" + roundId;
		}
		
		long gpiAmount = this.gamePlatformIntegrationUtils.getPreciseAmountFromProvider(BigDecimal.valueOf(amount), BALANCE_COEFFICIENT);

		MethodType methodType;
		
		if (OnexTwoMethod.PLACE_VIRTUAL_BET.equals(method)) {
			methodType = MethodType.BET;
		} else {
			
			if (!this.roundHasBet(roundId, gameRequest.getGame())) {
				OnexTwoResponse response = new OnexTwoResponse();
				response.setErrorCode(OnexTwoCodes.BET_NOT_FOUND);
				return response;
			}
			
			methodType = MethodType.WIN;
		}
		
		Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, transactionId, roundId, methodType);
		Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
		
		if (this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
			if (this.gamePlatformIntegrationUtils.messageIsAlreadyProcessed(gpiMessage)) {
				message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
				gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
			}
			
			long gpiBalance = gpiMessage.getResult().getReturnset().getBalance().getValue();
			Double providerBalance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT).doubleValue();
			String gpiToken = gpiMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(playerInfoKey, gpiToken, playerInfo);
			
			OnexTwoTransactionResponse response = new OnexTwoTransactionResponse();
			response.setBetId(((OnexTwoTransactionRequest)request).getF1x2ID());
			response.setBalance(String.valueOf(providerBalance));

			return response;
		} else {
			OnexTwoResponse response = new OnexTwoResponse();
			response.setErrorCode(OnexTwoCodes.GENERIC_ERROR);
			response.setErrorDesc(gpiMessage.getResult().getReturnset().getError().getValue());
			return response;
		}
	}
}
