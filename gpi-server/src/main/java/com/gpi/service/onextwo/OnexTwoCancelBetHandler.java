package com.gpi.service.onextwo;

import java.math.BigDecimal;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.MessageHelper;
import com.onextwo.api.OnexTwoCodes;
import com.onextwo.api.OnexTwoMethod;
import com.onextwo.api.domain.OnexTwoCancelBetResponse;
import com.onextwo.api.domain.OnexTwoRequest;
import com.onextwo.api.domain.OnexTwoResponse;
import com.onextwo.api.domain.OnexTwoTransactionRequest;

/**
 * Handler for CancelBet of 1x2
 * 
 * @author Alexandre
 *
 */
public class OnexTwoCancelBetHandler extends OnexTwoHandler {
	
	@Override
	public OnexTwoResponse handle(OnexTwoRequest request, OnexTwoMethod method) throws GameRequestNotExistentException {
		String playerInfoKey = this.getPlayerInfoKeyWithSessionId(request);
		PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
		String token = playerInfo.getToken();
		
		Double amount = Double.valueOf(((OnexTwoTransactionRequest)request).getAmount());
		Long roundId = Long.valueOf(((OnexTwoTransactionRequest)request).getF1x2ID());
		String transactionId = "REF-" + roundId;
		
		long gpiAmount = this.gamePlatformIntegrationUtils.getPreciseAmountFromProvider(BigDecimal.valueOf(amount), BALANCE_COEFFICIENT);

		Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, gpiAmount, transactionId, roundId, MethodType.REFUND_TRANSACTION);
		
		GameRequest gameRequest = gameRequestDao.getGameRequest(token);
		
		String gameTransactionId = this.getBetTransactionId(roundId , gameRequest.getGame());
		
		if (gameTransactionId == null) {
			OnexTwoResponse response = new OnexTwoResponse();
			response.setErrorCode(OnexTwoCodes.BET_NOT_FOUND);
			response.setErrorDesc("Transaction not found");
		}
		
		MessageHelper.addRefundedTransactionIdToMethod(message, gameTransactionId);
		
		Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
		
		if (this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
			if (this.gamePlatformIntegrationUtils.messageIsAlreadyProcessed(gpiMessage)) {
				message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
				gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
			}
			
			long gpiBalance = gpiMessage.getResult().getReturnset().getBalance().getValue();
			Double providerBalance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT).doubleValue();
			String gpiToken = gpiMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(playerInfoKey, gpiToken, playerInfo);
			
			OnexTwoCancelBetResponse response = new OnexTwoCancelBetResponse();
			response.setBetId(((OnexTwoTransactionRequest)request).getF1x2ID());
			response.setBalance(String.valueOf(providerBalance));
			response.setF1x2ID(((OnexTwoTransactionRequest)request).getF1x2ID());
			
			return response;
		} else {
			OnexTwoResponse response = new OnexTwoResponse();
			response.setErrorCode(OnexTwoCodes.GENERIC_ERROR);
			response.setErrorDesc(gpiMessage.getResult().getReturnset().getError().getValue());
			return response;
		}
	}
}
