package com.gpi.service.onextwo;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.onextwo.api.OnexTwoCodes;
import com.onextwo.api.OnexTwoMethod;
import com.onextwo.api.domain.OnexTwoGetAccountDetailsResponse;
import com.onextwo.api.domain.OnexTwoRequest;
import com.onextwo.api.domain.OnexTwoResponse;

/**
 * Handler for GetAccountDetails of 1x2
 * 
 * @author Alexandre
 *
 */
public class OnexTwoGetAccountDetailsHandler extends OnexTwoHandler {

	@Override
	public OnexTwoResponse handle(OnexTwoRequest request, OnexTwoMethod method) throws GameRequestNotExistentException {
		String playerInfoKey = this.getPlayerInfoKeyWithSessionId(request);
		PlayerInfo playerInfo = this.playerInfoDao.findPlayerInfo(playerInfoKey);
		String token = playerInfo.getToken();
		
		Message message = this.gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_BALANCE);
		Message gpiMessage = this.gamePlatformIntegrationService.processMessage(message);
		
		if (this.gamePlatformIntegrationUtils.messageIsSuccess(gpiMessage)) {
			long gpiBalance = gpiMessage.getResult().getReturnset().getBalance().getValue();
			Double balance = this.gamePlatformIntegrationUtils.getPreciseAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT).doubleValue();
			String currency = playerInfo.getCurrency();
			String gpiToken = gpiMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(playerInfoKey, gpiToken, playerInfo);
			
			OnexTwoGetAccountDetailsResponse response = new OnexTwoGetAccountDetailsResponse();
			response.setAccountId(playerInfo.getUsername());
			response.setBalance(String.valueOf(balance));
			response.setIsoCode(currency);

			return response;
		} else {
			OnexTwoResponse response = new OnexTwoResponse();
			response.setErrorCode(OnexTwoCodes.GENERIC_ERROR);
			response.setErrorDesc(gpiMessage.getResult().getReturnset().getError().getValue());
			return response;
		}
	}

}
