package com.gpi.service.provider;

import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.exceptions.GameUrlNotDefinedException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

/**
 * Created by igabba on 15/01/15.
 */
public class ProviderService{

    private Map<String, GamePlatformIntegrationService> serviceMap;

    public LoadGameDto loadGame(String publisherToken, String pn, Game game, String type, boolean lobby, String device, int ttl) throws NonExistentPublisherException, NonExistentGameException, GameUrlNotDefinedException {
        return serviceMap.get(game.getGameProvider().getApiName()).loadGame(publisherToken, pn, game,type,lobby, device, ttl);
    }

    public void setServiceMap(Map<String, GamePlatformIntegrationService> serviceMap) {
        this.serviceMap = serviceMap;
    }

    public void setProviderSpecificParams(Game game, Map parameterMap, LoadGameDto loadGameDto, UriComponentsBuilder builder) throws NonExistentGameException, NonExistentPublisherException {
        serviceMap.get(game.getGameProvider().getApiName()).setProviderSpecificParams(parameterMap, loadGameDto, builder);
    }


}
