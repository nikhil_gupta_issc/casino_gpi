package com.gpi.service.booming;

import com.booming.domain.BoomingRequest;
import com.booming.domain.BoomingResponse;
import com.booming.domain.AbstractBoomingRequest;
import com.booming.domain.BoomingRequestHeader;
import com.booming.domain.AbstractBoomingResponse;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.utils.ApplicationProperties;

/**
 * BoomingMessageBetWin
 * 
 * @author f.fernandez
 *
 */
public class BoomingMessageBetWin extends BoomingMessageCommand{

	private static final int NOT_ENOUGH_CREDIT_ERROR_CODE = 200;
	
	private static final String NOT_ENOUGHT_CREDIT_CALLBACK = "/booming/notenoughcredit.do";
	
	@Override
	public AbstractBoomingResponse execute(BoomingRequestHeader header, AbstractBoomingRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {

		BoomingRequest betwinRequest = (BoomingRequest)request;
		
		String token = 	(String)cacheManager.getAndTouch(CacheConstants.BOOMING_TOKEN_KEY + "-" + betwinRequest.getSessionId(), CacheConstants.BOOMING_TOKEN_TTL);
		String transactionId = String.format("%1$s-%2$s", betwinRequest.getSessionId(), betwinRequest.getRound());
		Long roundId = Long.parseLong(header.getNonce());
		
		Long bet = super.balanceStringToLong(betwinRequest.getBet());
		
		//BET
		Message betMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, bet, MethodType.BET.name() + "-" + transactionId, roundId, MethodType.BET);
		Message betReturnMessage = gamePlatformIntegrationService.processMessage(betMessageFromGame);
		
		Long balance = 0L;
		String _return = null;
		if (betReturnMessage.getResult().getSuccess() == 1L) {
			token = betReturnMessage.getResult().getReturnset().getToken().getValue();
			balance = betReturnMessage.getResult().getReturnset().getBalance().getValue();
			
			refreshToken(betwinRequest.getSessionId(), token);
			
			Long win = super.balanceStringToLong(betwinRequest.getWin());
			
			//WIN
			Message winMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, win, MethodType.WIN.name() + "-" + transactionId, roundId, MethodType.WIN);
			Message winReturnMessage = gamePlatformIntegrationService.processMessage(winMessageFromGame);
			
			if (winReturnMessage.getResult().getSuccess() == 1L) {
				token = winReturnMessage.getResult().getReturnset().getToken().getValue();
				balance = winReturnMessage.getResult().getReturnset().getBalance().getValue();
				
				refreshToken(betwinRequest.getSessionId(), token);
			}else {
				//REFUND
				Message refundReturnMessage = this.refund(token, bet, roundId, MethodType.BET.name() + "-" + transactionId);
				
				if (refundReturnMessage.getResult().getSuccess() == 1L) 
					refreshToken(betwinRequest.getSessionId(), refundReturnMessage.getResult().getReturnset().getToken().getValue());
				
				throw new TransactionNotSuccessException(winReturnMessage.getResult().getReturnset().getError().getValue(), winReturnMessage.getResult().getReturnset().getErrorCode().getValue());
			}
		}else {
			if (betReturnMessage.getResult().getReturnset().getErrorCode().getValue() == NOT_ENOUGH_CREDIT_ERROR_CODE) {
				_return = ApplicationProperties.getProperty("application.host.url") + NOT_ENOUGHT_CREDIT_CALLBACK;
			}else {
				throw new TransactionNotSuccessException(betReturnMessage.getResult().getReturnset().getError().getValue(), betReturnMessage.getResult().getReturnset().getErrorCode().getValue());
			}
		}
		
		return new BoomingResponse(balanceLongToString(balance), _return);
	}	
}
