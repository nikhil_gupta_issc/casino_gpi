package com.gpi.service.booming;

import com.booming.domain.BoomingPlayerInfoRequest;
import com.booming.domain.BoomingPlayerInfoResponse;
import com.booming.domain.AbstractBoomingRequest;
import com.booming.domain.BoomingRequestHeader;
import com.booming.domain.AbstractBoomingResponse;
import com.gpi.api.Message;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;

/**
 * BoomingMessagePlayerInfo
 * 
 * @author f.fernandez
 *
 */
public class BoomingMessagePlayerInfo extends BoomingMessageCommand{

	@Override
	public AbstractBoomingResponse execute(BoomingRequestHeader header, AbstractBoomingRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		BoomingPlayerInfoRequest playerInfoRequest = (BoomingPlayerInfoRequest)request;
		
		Message responseMessage = super.playerInfo(playerInfoRequest.getToken());
		
		return new BoomingPlayerInfoResponse(
				responseMessage.getResult().getReturnset().getLoginName().getValue(), 
				balanceLongToString(responseMessage.getResult().getReturnset().getBalance().getValue()),
				responseMessage.getResult().getReturnset().getCurrency().getValue(),
				responseMessage.getResult().getReturnset().getToken().getValue());
	}

	
	
	
}
