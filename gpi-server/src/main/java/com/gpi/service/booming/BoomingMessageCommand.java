package com.gpi.service.booming;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.booming.domain.AbstractBoomingRequest;
import com.booming.domain.BoomingRequestHeader;
import com.booming.domain.AbstractBoomingResponse;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.MessageHelper;
import com.gpi.utils.cache.CacheManager;

/**
 * Base message handler for Booming integration
 * 
 * @author f.fernandez
 *
 */
public abstract class BoomingMessageCommand {
	
	protected GamePlatformIntegrationService gamePlatformIntegrationService;

	protected GameRequestDao gameRequestDao;
	
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	
	protected CacheManager cacheManager;
	
	protected TransactionService transactionService;
	
	protected static final int BALANCE_COEFFICIENT = 100;
	
	/**
	 * Handles the requested message
	 * @param params map of key value parameter received
	 * @return response to be returned
	 * @throws GameRequestNotExistentException thrown when the game request does not exist
	 */
	public abstract AbstractBoomingResponse execute(BoomingRequestHeader header, AbstractBoomingRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException;
	
	protected void refreshToken(String sessionId, String token) {
		cacheManager.store(CacheConstants.BOOMING_TOKEN_KEY + "-" + sessionId, token, CacheConstants.BOOMING_TOKEN_TTL);
	}
	
	protected String balanceLongToString(Long balance) {
		return new BigDecimal(balance).divide(new BigDecimal(BALANCE_COEFFICIENT), 2, RoundingMode.UNNECESSARY).toString();
	}
	
	protected Long balanceStringToLong(String balance) {
		return new BigDecimal(balance).multiply(new BigDecimal(BALANCE_COEFFICIENT)).longValue();
	}
	
	protected Message playerInfo(String token) throws GameRequestNotExistentException, TransactionNotSuccessException {
		Message gpiFormattedMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_PLAYER_INFO);
		Message responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);
		
		if (responseMessage.getResult().getSuccess() == 1) {
			return responseMessage;
		}else {
			throw new TransactionNotSuccessException(responseMessage.getResult().getReturnset().getError().getValue(), responseMessage.getResult().getReturnset().getErrorCode() .getValue());
		}
	}
	
	protected Message refund(String token, Long bet, Long roundId, String transactionId) throws TransactionNotSuccessException, GameRequestNotExistentException {
		Message refundMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, bet, "REF-" + transactionId, roundId, MethodType.REFUND_TRANSACTION);
		MessageHelper.addRefundedTransactionIdToMethod(refundMessageFromGame, transactionId);
		Message refundReturnMessage = gamePlatformIntegrationService.processMessage(refundMessageFromGame);
		
		if (refundReturnMessage.getResult().getSuccess() == 1L) {
			return refundReturnMessage;
		}else {
			throw new TransactionNotSuccessException(refundReturnMessage.getResult().getReturnset().getError().getValue(), refundReturnMessage.getResult().getReturnset().getErrorCode().getValue());
		}
	}
	
	/**
	 * @param gamePlatformIntegrationUtils the gamePlatformIntegrationUtils to set
	 */
	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	/**
	 * @param gamePlatformIntegrationService the gamePlatformIntegrationService to set
	 */
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}


	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}


	/**
	 * @param transactionService the transactionService to set
	 */
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}
	
	
}
