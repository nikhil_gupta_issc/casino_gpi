package com.gpi.service.booming;

import com.booming.domain.AbstractBoomingRequest;
import com.booming.domain.AbstractBoomingResponse;
import com.booming.domain.BoomingRequest;
import com.booming.domain.BoomingRequestHeader;
import com.booming.domain.BoomingResponse;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.GameRequest;
import com.gpi.domain.audit.Transaction;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;

/**
 * BoomingMessageRollback
 * 
 * @author f.fernandez
 *
 */
public class BoomingMessageRollback extends BoomingMessageCommand{

	@Override
	public AbstractBoomingResponse execute(BoomingRequestHeader header, AbstractBoomingRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {

		BoomingRequest rollbackRequest = (BoomingRequest)request;
		
		String token = 	(String)cacheManager.getAndTouch(CacheConstants.BOOMING_TOKEN_KEY + "-" + rollbackRequest.getSessionId(), CacheConstants.BOOMING_TOKEN_TTL);
		String transactionId = String.format("%1$s-%2$s", rollbackRequest.getSessionId(), rollbackRequest.getRound());
		Long balance = 0L;
		Long bet = balanceStringToLong(rollbackRequest.getBet());
		
		Long roundId = refundRound(token, transactionId, rollbackRequest.getWin());
		
		if (roundId != null) {
			// Refund
			Message refundReturnMessage = this.refund(token, bet, roundId, MethodType.BET + "-" + transactionId);
			token = refundReturnMessage.getResult().getReturnset().getToken().getValue();
			balance = refundReturnMessage.getResult().getReturnset().getBalance().getValue();
			refreshToken(rollbackRequest.getSessionId(), token);
		}
		
		// Balance
		Message playerInfoReturnMessage = super.playerInfo(token);
		token = playerInfoReturnMessage.getResult().getReturnset().getToken().getValue();
		balance = playerInfoReturnMessage.getResult().getReturnset().getBalance().getValue();
		refreshToken(rollbackRequest.getSessionId(), token);

		return new BoomingResponse(this.balanceLongToString(balance));
	}
	
	/**
	 * Check if should refund bet: if there is a bet without a refund and without a win if apply
	 * 
	 * @param header
	 * @param request
	 * 
	 * @return RoundId to refund if apply
	 * @throws GameRequestNotExistentException 
	 */
	private Long refundRound(String token, String transactionId, String win) throws GameRequestNotExistentException {
		
		Long roundId = null;
		
		GameRequest gameRequest = gameRequestDao.getGameRequest(token);
		
		Transaction betTransaction = transactionService.getTransaction(MethodType.BET.name() + "-" + transactionId, gameRequest.getGame().getName());
		
		if (betTransaction != null && betTransaction.isSuccess()) {
			Transaction winTransaction = transactionService.getTransaction(MethodType.WIN.name() + "-" + transactionId, gameRequest.getGame().getName());
			
			if ((winTransaction != null && !winTransaction.isSuccess()) || (winTransaction == null)) {
				Transaction refundTransaction = transactionService.getTransaction("REF-" + MethodType.BET.name() + "-" + transactionId, betTransaction.getRound());
				
				if (refundTransaction == null || !refundTransaction.isSuccess()) {
					roundId = betTransaction.getRound().getGameRoundId();
				}
			}
		}
		
		return roundId;
	}
	
}
