package com.gpi.service.evenbet;

import com.evenbet.domain.EvenbetGetBalanceRequest;
import com.evenbet.domain.EvenbetGetBalanceResponse;
import com.evenbet.domain.EvenbetRequest;
import com.evenbet.domain.EvenbetRequestHeader;
import com.evenbet.domain.EvenbetResponse;
import com.gpi.api.Message;
import com.gpi.constants.CacheConstants;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;

/**
 * EvenbetMessageGetBalance
 * 
 * @author f.fernandez
 *
 */
public class EvenbetMessageGetBalance extends EvenbetMessageCommand{

	@Override
	public EvenbetResponse execute(EvenbetRequestHeader header, EvenbetRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		EvenbetGetBalanceRequest getBalanceRequest = (EvenbetGetBalanceRequest)request;
		
		String token = 	(String)cacheManager.getAndTouch(CacheConstants.EVENBET_TOKEN_KEY + "-" + getBalanceRequest.getUserId(), CacheConstants.EVENBET_TOKEN_TTL);
		
		Message responseMessage = super.playerInfo(token);
		this.refreshToken(getBalanceRequest.getUserId(), responseMessage.getResult().getReturnset().getToken().getValue());
		
		return new EvenbetGetBalanceResponse(responseMessage.getResult().getReturnset().getBalance().getValue());
	}
	
}
