package com.gpi.service.evenbet;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.evenbet.api.EvenbetErrorCodes;
import com.evenbet.api.EvenbetErrorMessages;
import com.evenbet.domain.EvenbetErrorResponse;
import com.evenbet.domain.EvenbetGetBalanceResponse;
import com.evenbet.domain.EvenbetGetCashRequest;
import com.evenbet.domain.EvenbetRequest;
import com.evenbet.domain.EvenbetRequestHeader;
import com.evenbet.domain.EvenbetResponse;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.GameRequest;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.transaction.TransactionFilter;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;

/**
 * EvenbetMessageGetBalance
 * 
 * @author f.fernandez
 *
 */
public class EvenbetMessageGetCash extends EvenbetMessageCommand{


	private static final Logger logger = LoggerFactory.getLogger(EvenbetMessageGetCash.class);

	
	private static final int NOT_ENOUGH_CREDIT_GPI_ERROR_CODE = 200;

	@Override
	public EvenbetResponse execute(EvenbetRequestHeader header, EvenbetRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		EvenbetGetCashRequest getCashRequest = (EvenbetGetCashRequest)request;
		
		String token = 	(String)cacheManager.getAndTouch(CacheConstants.EVENBET_TOKEN_KEY + "-" + getCashRequest.getUserId(), CacheConstants.EVENBET_TOKEN_TTL);
		
		String transactionId = getCashRequest.getTransactionId();
		Long bet = getCashRequest.getAmount();
		
		Long roundId = new Date().getTime();
		
		GameRequest gameRequest = this.gameRequestDao.getGameRequest(token);
		
		TransactionFilter filter = new TransactionFilter();
		filter.setPlayerPublisherCode(gameRequest.getPublisher().getName());
		filter.setProviderTransactionId(MethodType.BET.name() + "-" + getCashRequest.getTransactionId());
		List<Transaction> transactions = transactionService.getTransactions(filter);
		
		if (transactions == null || transactions.size() == 0 || !transactions.get(0).isSuccess()) {
		
			//BET
			Message betMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, bet, MethodType.BET.name() + "-" + transactionId, roundId, MethodType.BET);
			Message betReturnMessage = gamePlatformIntegrationService.processMessage(betMessageFromGame);
			
			Long balance = 0L;
			
			if (betReturnMessage.getResult().getSuccess() == 1L) {
				token = betReturnMessage.getResult().getReturnset().getToken().getValue();
				balance = betReturnMessage.getResult().getReturnset().getBalance().getValue();
				
				token = mockCreditCero(getCashRequest,token, roundId);
				refreshToken(getCashRequest.getUserId(), token);
				
				return new EvenbetGetBalanceResponse(balance);
			}else {
				if (betReturnMessage.getResult().getReturnset().getErrorCode().getValue() == NOT_ENOUGH_CREDIT_GPI_ERROR_CODE) {
					return new EvenbetErrorResponse(EvenbetErrorCodes.INSUFFICIENT_FUNDS, EvenbetErrorMessages.INSUFFICIENT_FUNDS_MESSAGE);
				} else if (betReturnMessage.getResult().getReturnset().getAlreadyProcessed().isValue()) {
					return new EvenbetErrorResponse(EvenbetErrorCodes.ALREADY_PROCCESED, EvenbetErrorMessages.ALREADY_PROCCESED_MESSAGE);
				}else {
					throw new TransactionNotSuccessException(betReturnMessage.getResult().getReturnset().getError().getValue(), betReturnMessage.getResult().getReturnset().getErrorCode().getValue());
				}
			}
		} else {
			return new EvenbetErrorResponse(EvenbetErrorCodes.ALREADY_PROCCESED, EvenbetErrorMessages.ALREADY_PROCCESED_MESSAGE);
		}
	}
	
	private String mockCreditCero(EvenbetGetCashRequest getCashRequest, String token, Long roundId) throws GameRequestNotExistentException, TransactionNotSuccessException {

		logger.info(String.format("Evenbet: mocking a cero credit to make the pair bet/win: %1$s", getCashRequest.toJson()));
		
		Long win = 0L;
		
		//WIN
		Message winMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, win, MethodType.WIN.name() + "-" + getCashRequest.getTransactionId(), roundId, MethodType.WIN);
		Message winReturnMessage = gamePlatformIntegrationService.processMessage(winMessageFromGame);
		
		if (winReturnMessage.getResult().getSuccess() == 1L) {
			return winReturnMessage.getResult().getReturnset().getToken().getValue();
		}else {
			throw new TransactionNotSuccessException("Evenbet: Can't mock 0 credit for transactionId=" + getCashRequest.getTransactionId(), EvenbetErrorCodes.INTERNAL_ERROR_MOCK_CREDIT_CERO_FAIL.intValue());
		}
	}
	
}
