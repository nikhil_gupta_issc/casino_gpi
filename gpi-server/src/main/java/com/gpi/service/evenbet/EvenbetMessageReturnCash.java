package com.gpi.service.evenbet;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.evenbet.api.EvenbetErrorCodes;
import com.evenbet.api.EvenbetErrorMessages;
import com.evenbet.domain.EvenbetErrorResponse;
import com.evenbet.domain.EvenbetGetBalanceResponse;
import com.evenbet.domain.EvenbetRequest;
import com.evenbet.domain.EvenbetRequestHeader;
import com.evenbet.domain.EvenbetResponse;
import com.evenbet.domain.EvenbetReturnCashRequest;
import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.domain.GameRequest;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.transaction.TransactionFilter;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;

/**
 * EvenbetMessageGetBalance
 * 
 * @author f.fernandez
 *
 */
public class EvenbetMessageReturnCash extends EvenbetMessageCommand{


	private static final Logger logger = LoggerFactory.getLogger(EvenbetMessageReturnCash.class);
	
	@Override
	public EvenbetResponse execute(EvenbetRequestHeader header, EvenbetRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		EvenbetReturnCashRequest returnCashRequest = (EvenbetReturnCashRequest)request;
		
		String token = 	(String)cacheManager.getAndTouch(CacheConstants.EVENBET_TOKEN_KEY + "-" + returnCashRequest.getUserId(), CacheConstants.EVENBET_TOKEN_TTL);
		
		Long roundId = new Date().getTime();
		
		GameRequest gameRequest = this.gameRequestDao.getGameRequest(token);
		
		TransactionFilter filter = new TransactionFilter();
		filter.setPlayerPublisherCode(gameRequest.getPublisher().getName());
		filter.setProviderTransactionId(MethodType.WIN.name() + "-" + returnCashRequest.getTransactionId());
		List<Transaction> transactions = transactionService.getTransactions(filter);
		
		if (transactions == null || transactions.size() == 0 || !transactions.get(0).isSuccess()) {
			token = mockDebitCero(returnCashRequest, token, roundId);
			refreshToken(returnCashRequest.getUserId(), token);
		
			//WIN
			Message winMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, returnCashRequest.getAmount(), MethodType.WIN.name() + "-" + returnCashRequest.getTransactionId(), roundId, MethodType.WIN);
			Message winReturnMessage = gamePlatformIntegrationService.processMessage(winMessageFromGame);
			
			if (winReturnMessage.getResult().getSuccess() == 1L) {
				refreshToken(returnCashRequest.getUserId(), winReturnMessage.getResult().getReturnset().getToken().getValue());
	
				return new EvenbetGetBalanceResponse(winReturnMessage.getResult().getReturnset().getBalance().getValue());	
			}else {
				if (winReturnMessage.getResult().getReturnset().getAlreadyProcessed().isValue()) {
					return new EvenbetErrorResponse(EvenbetErrorCodes.ALREADY_PROCCESED, EvenbetErrorMessages.ALREADY_PROCCESED_MESSAGE);
				} else {
					throw new TransactionNotSuccessException(winReturnMessage.getResult().getReturnset().getError().getValue(), winReturnMessage.getResult().getReturnset().getErrorCode().getValue());	
				}
			}
		}else {
			return new EvenbetErrorResponse(EvenbetErrorCodes.ALREADY_PROCCESED, EvenbetErrorMessages.ALREADY_PROCCESED_MESSAGE);
		}	
	}
	
	private String mockDebitCero(EvenbetReturnCashRequest returnCashRequest, String token, Long roundId) throws GameRequestNotExistentException, TransactionNotSuccessException {

		logger.info(String.format("Evenbet: mocking a cero debit to make the pair bet/win: %1$s", returnCashRequest.toJson()));
		
		Long bet = 0L;
		
		//BET
		Message betMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, bet, MethodType.BET.name() + "-" + returnCashRequest.getTransactionId(), roundId, MethodType.BET);
		Message betReturnMessage = gamePlatformIntegrationService.processMessage(betMessageFromGame);
		
		if (betReturnMessage.getResult().getSuccess() == 1L) {
			return betReturnMessage.getResult().getReturnset().getToken().getValue();
		}else {
			throw new TransactionNotSuccessException("Evenbet: Can't mock 0 debit for transactionId=" + returnCashRequest.getTransactionId(), EvenbetErrorCodes.INTERNAL_ERROR_MOCK_CREDIT_CERO_FAIL.intValue());
		}
	}
	
}
