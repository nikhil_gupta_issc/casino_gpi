package com.gpi.service.habanero;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.GameRequest;
import com.gpi.domain.audit.RefundedTransaction;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.Game;
import com.gpi.domain.transaction.TransactionType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.MessageHelper;
import com.habanero.domain.HabaneroDialogMessageResponse;
import com.habanero.domain.HabaneroFundInfo;
import com.habanero.domain.HabaneroFundTransferResponse;
import com.habanero.domain.HabaneroRefund;
import com.habanero.domain.HabaneroRequest;
import com.habanero.domain.HabaneroResponse;
import com.habanero.domain.HabaneroStatus;
import com.habanero.domain.HabaneroTransactionRequest;
import com.habanero.domain.HabaneroTransactionResponse;

/**
 * HabaneroMessageDebitCredit
 * 
 * @author f.fernandez
 *
 */
public class HabaneroMessageTransaction extends HabaneroMessageCommand{
	
	private static final Logger logger = LoggerFactory.getLogger(HabaneroMessageTransaction.class);
	
	private static final int NOT_ENOUGH_CREDIT_ERROR_CODE = 200;
	
	private static final int REFUNDSTATUS_REFUNDED = 1;
	
	private static final int REFUNDSTATUS_NOT_REQUIRED = 2;
	
	private static final String ACTIVE_PROFILE = ApplicationProperties.getProperty("active.profile");
	
	private static final int GAMESTATEMODE_NEW = 1;
	
	private static final int GAMESTATEMODE_LAST = 2;
	
	private static final int GAMESTATEMODE_EXPIRED = 3;
	
	private static final int GAMESTATEMODE_CONTINUATION = 0;
	
	private static final boolean MOCK_DEBIT_CREDIT = true;
	
	@Override
	public HabaneroResponse execute(HabaneroRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		HabaneroTransactionRequest txRequest = (HabaneroTransactionRequest)request;
		HabaneroTransactionResponse response = null;
		testTrigger(txRequest, "session-expired", "session-expired");
		if (!"prod".equalsIgnoreCase(ACTIVE_PROFILE) && "auth".equals(request.getTt())) {
			response = new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus(false, true)), new HabaneroDialogMessageResponse(0, "wac error test dialog")); 
		}else if (this.isDebitAndCredit(txRequest)) {
			response = this.processDebitAndCredit(txRequest);
		}else if(this.isDebitOnly(txRequest)) {
			response = this.processDebit(txRequest, txRequest.getFundtransferrequest().getFunds().getFundinfo().get(0), MOCK_DEBIT_CREDIT);
		}else if (this.isCreditOnly(txRequest)) {
			response = this.processCredit(txRequest, txRequest.getFundtransferrequest().getFunds().getFundinfo().get(0), MOCK_DEBIT_CREDIT);
		}else if(this.isRefund(txRequest)) {
			response =  this.processRefund(txRequest);
		}else if (this.isRecredit(txRequest)) {
			response =  this.processRecredit(txRequest);
		}else {
			throw new IllegalStateException(String.format("Habanero: Illegal Transaction state. request=%1$s", request.toJson()));
		}
		return response;
	}
	
	private boolean isDebitOnly(HabaneroTransactionRequest txRequest) {
		return txRequest.getFundtransferrequest().getIsrecredit() != true 
				&& txRequest.getFundtransferrequest().getIsrefund() != true 
				&& txRequest.getFundtransferrequest().getIsretry() != true
				&& txRequest.getFundtransferrequest().getFunds().getDebitandcredit() != true
				&& this.balanceStringToLong(txRequest.getFundtransferrequest().getFunds().getFundinfo().get(0).getAmount()) < 0;
	}

	private boolean isCreditOnly(HabaneroTransactionRequest txRequest) {
		return txRequest.getFundtransferrequest().getIsrecredit() != true 
				&& txRequest.getFundtransferrequest().getIsrefund() != true 
				&& txRequest.getFundtransferrequest().getIsretry() != true
				&& txRequest.getFundtransferrequest().getFunds().getDebitandcredit() != true
				&& this.balanceStringToLong(txRequest.getFundtransferrequest().getFunds().getFundinfo().get(0).getAmount()) >= 0;
	}
	
	private boolean isDebitAndCredit(HabaneroTransactionRequest txRequest) {
		return txRequest.getFundtransferrequest().getIsrecredit() != true
				&& txRequest.getFundtransferrequest().getIsrefund() != true
				&& txRequest.getFundtransferrequest().getIsretry() != true
				&& txRequest.getFundtransferrequest().getFunds().getDebitandcredit() == true;
	}
	
	private boolean isRefund(HabaneroTransactionRequest txRequest) {
		return txRequest.getFundtransferrequest().getIsrefund() == true 
				&& txRequest.getFundtransferrequest().getIsretry() == true;
	}
	
	
	private boolean isRecredit(HabaneroTransactionRequest txRequest) {
		return txRequest.getFundtransferrequest().getIsrecredit() == true
				&& txRequest.getFundtransferrequest().getIsretry() == true;
	}
	
	private HabaneroTransactionResponse processDebitAndCredit(HabaneroTransactionRequest txRequest) throws TransactionNotSuccessException, GameRequestNotExistentException {

		HabaneroTransactionResponse response = new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus()));
		
		for (HabaneroFundInfo fundInfo : txRequest.getFundtransferrequest().getFunds().getFundinfo()) {				
			
			Long amount = super.balanceStringToLong(fundInfo.getAmount());
			if (amount < 0) {
				//BET/DEBIT
				HabaneroTransactionResponse debitResponse = this.processDebit(txRequest, fundInfo, !MOCK_DEBIT_CREDIT);
				if (debitResponse.getFundtransferresponse().getStatus().getSuccess() == true) {
					response.getFundtransferresponse().getStatus().setSuccessdebit(true);
					response.getFundtransferresponse().setBalance(debitResponse.getFundtransferresponse().getBalance());
					response.getFundtransferresponse().setCurrencycode(debitResponse.getFundtransferresponse().getCurrencycode());
				}else {
					response.getFundtransferresponse().getStatus().setSuccessdebit(false);
					
					if (debitResponse.getFundtransferresponse().getStatus().getNofunds() == true) {
						response.getFundtransferresponse().getStatus().setNofunds(true);
						response.getFundtransferresponse().setBalance(debitResponse.getFundtransferresponse().getBalance());
						response.getFundtransferresponse().setCurrencycode(debitResponse.getFundtransferresponse().getCurrencycode());
					}
				}
			}else {
				if (response.getFundtransferresponse().getStatus().getSuccessdebit() == true) {
					//WIN/CREDIT
					HabaneroTransactionResponse creditResponse = this.processCredit(txRequest, fundInfo, !MOCK_DEBIT_CREDIT);
					if (creditResponse.getFundtransferresponse().getStatus().getSuccess() == true) {
						response.getFundtransferresponse().getStatus().setSuccesscredit(true);
						response.getFundtransferresponse().setBalance(creditResponse.getFundtransferresponse().getBalance());
						response.getFundtransferresponse().setCurrencycode(creditResponse.getFundtransferresponse().getCurrencycode());
					}else {
						response.getFundtransferresponse().getStatus().setSuccesscredit(false);
					}
				}else {
					response.getFundtransferresponse().getStatus().setSuccesscredit(false);
				}
			}

		}
		
		//if one is false then all are false, then habanero will check status using com.gpi.service.habanero.HabaneroMessageQuery
		response.getFundtransferresponse().getStatus().setSuccess(response.getFundtransferresponse().getStatus().getSuccessdebit() == true &&  response.getFundtransferresponse().getStatus().getSuccesscredit() == true);
		response.getFundtransferresponse().getStatus().setSuccessdebit(response.getFundtransferresponse().getStatus().getSuccess());
		response.getFundtransferresponse().getStatus().setSuccesscredit(response.getFundtransferresponse().getStatus().getSuccess());
		
		return response;
	}
	
	private HabaneroTransactionResponse processRefund(HabaneroTransactionRequest txRequest) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		HabaneroStatus status = new HabaneroStatus();
		
		GameRequest gameRequest = gameRequestDao.getGameRequest(txRequest.getFundtransferrequest().getToken());
		
		HabaneroRefund refundInfo = txRequest.getFundtransferrequest().getFunds().getRefund();
		
		Transaction betTransaction = transactionService.getTransaction(refundInfo.getOriginaltransferid(), gameRequest.getGame().getName());
		if (betTransaction != null && betTransaction.isSuccess()) {
			RefundedTransaction refundTransaction = refundedTransactionService.getRefundedTransaction(refundInfo.getOriginaltransferid(), betTransaction);
			if (refundTransaction == null) {
				Long amount = super.balanceStringToLong(refundInfo.getAmount());
				amount = amount < 0 ? -1*amount : amount;
				Long roundId = this.getGameRoundId(txRequest);
				
				Message refundReturnMessage = this.placeRefund(txRequest.getFundtransferrequest().getToken(), amount, roundId, refundInfo.getOriginaltransferid(), refundInfo.getTransferid());

				if (refundReturnMessage.getResult().getSuccess() == 1L) {
					status.setSuccess(true);
					status.setRefundStatus(REFUNDSTATUS_REFUNDED);
				}else {
					status.setSuccess(false);
				}
			}else {
				if (refundTransaction.getTransaction().isSuccess()) {
					status.setSuccess(true);
					status.setRefundStatus(REFUNDSTATUS_REFUNDED);
				}else {
					status.setSuccess(false);
				}
			}
			
		}else {
			status.setSuccess(true);
			status.setRefundStatus(REFUNDSTATUS_NOT_REQUIRED);
		}
		
		// Balance
		Message playerInfoReturnMessage = super.playerInfo(txRequest.getFundtransferrequest().getToken());
		Long balance = playerInfoReturnMessage.getResult().getReturnset().getBalance().getValue();
		String currency = playerInfoReturnMessage.getResult().getReturnset().getCurrency().getValue().toUpperCase();
		
		HabaneroFundTransferResponse transferResponse = new HabaneroFundTransferResponse(status);
		transferResponse.setBalance(super.balanceLongToString(balance));
		transferResponse.setCurrencycode(currency);
		return new HabaneroTransactionResponse(transferResponse);
	}
	
	/**
	 * recredits are performed when D&C transaction fails and query return a success bet and a fail credit
	 * Habanero Api: "	If the outcome of a credit is unknown to Habanero, we will reissue the credit request. If the original credit was
						not successful you must redo/commit the credit. If the original credit was successful return the same data as if the
						original credit was done"

	 * @param txRequest
	 * @return
	 * @throws GameRequestNotExistentException
	 * @throws TransactionNotSuccessException
	 */
	private HabaneroTransactionResponse processRecredit(HabaneroTransactionRequest txRequest) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		HabaneroTransactionResponse response = new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus()));
		
		HabaneroFundInfo winFundInfo = txRequest.getFundtransferrequest().getFunds().getFundinfo().get(0);
		
		GameRequest gameRequest = gameRequestDao.getGameRequest(txRequest.getFundtransferrequest().getToken());
		
		Transaction winTransaction = transactionService.getTransaction(winFundInfo.getTransferid(), gameRequest.getGame().getName());
		
		
		if (winTransaction == null) {
			boolean mockDebit = (betWinDiff(txRequest, gameRequest.getGame()) == 0);
			response = this.processCredit(txRequest, winFundInfo, mockDebit);
		}else if (winTransaction.isSuccess()) {
			// Balance
			Message playerInfoReturnMessage = super.playerInfo(txRequest.getFundtransferrequest().getToken());
			
			response.getFundtransferresponse().getStatus().setSuccess(true);
			response.getFundtransferresponse().setBalance(this.balanceLongToString(playerInfoReturnMessage.getResult().getReturnset().getBalance().getValue()));
			response.getFundtransferresponse().setCurrencycode(playerInfoReturnMessage.getResult().getReturnset().getCurrency().getValue().toUpperCase());
		}else {
			response.getFundtransferresponse().getStatus().setSuccess(false);
		}
		
		return response;
	}
	
	private Message placeRefund(String token, Long bet, Long roundId, String originalTransactionId, String transactionId) throws GameRequestNotExistentException {
		Message refundMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, bet, transactionId, roundId, MethodType.REFUND_TRANSACTION);
		MessageHelper.addRefundedTransactionIdToMethod(refundMessageFromGame, originalTransactionId);
		Message refundReturnMessage = gamePlatformIntegrationService.processMessage(refundMessageFromGame);
		return refundReturnMessage;
	}
	
	private Long getGameRoundId(HabaneroTransactionRequest txRequest) {
		return Long.parseLong(txRequest.getFundtransferrequest().getFriendlygameinstanceid());
	}
	
	//BET/DEBIT
	private HabaneroTransactionResponse processDebit(HabaneroTransactionRequest txRequest, HabaneroFundInfo fundInfo, boolean mock) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		HabaneroTransactionResponse response = new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus()));
		
		if (fundInfo.getGamestatemode() == GAMESTATEMODE_NEW || fundInfo.getGamestatemode() == GAMESTATEMODE_CONTINUATION) {
					
			GameRequest gameRequest = gameRequestDao.getGameRequest(txRequest.getFundtransferrequest().getToken());
			
			String token = txRequest.getFundtransferrequest().getToken();
			Long roundId = this.getGameRoundId(txRequest);
			
			Long amount = this.balanceStringToLong(fundInfo.getAmount());
			Long betAmount = amount < 0 ? -1L*this.balanceStringToLong(fundInfo.getAmount()) : amount;
			
			testTrigger(txRequest, "single-debiterror-before", "dc-debiterror-before");
			
			Message betMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, betAmount, fundInfo.getTransferid(), roundId, MethodType.BET);
			Message betReturnMessage = gamePlatformIntegrationService.processMessage(betMessageFromGame);
			
			testTrigger(txRequest, "single-debiterror-after", "dc-debiterror-after");
			
			if (betReturnMessage.getResult().getSuccess() == 1L) {
				response.getFundtransferresponse().getStatus().setSuccess(true);
				response.getFundtransferresponse().setBalance(super.balanceLongToString(betReturnMessage.getResult().getReturnset().getBalance().getValue()));
				response.getFundtransferresponse().setCurrencycode(gameRequest.getPlayer().getCurrency().name().toUpperCase());
				
				if (mock)
					mockCreditCero(txRequest, fundInfo);
			}else {
				response.getFundtransferresponse().getStatus().setSuccess(false);
				if (betReturnMessage.getResult().getReturnset().getErrorCode().getValue() == NOT_ENOUGH_CREDIT_ERROR_CODE) {
					response.getFundtransferresponse().getStatus().setNofunds(true);
					
					// Balance
					Message playerInfoReturnMessage = super.playerInfo(txRequest.getFundtransferrequest().getToken());
					
					response.getFundtransferresponse().setBalance(this.balanceLongToString(playerInfoReturnMessage.getResult().getReturnset().getBalance().getValue()));
					response.getFundtransferresponse().setCurrencycode(gameRequest.getPlayer().getCurrency().name().toUpperCase());
				}
			}
		}else {
			throw new IllegalStateException(String.format("Habanero: debit with gamestate=%1$s is not possible", String.valueOf(fundInfo.getGamestatemode())));
		}
		
		return response;
	}
	
	//WIN/CREDIT
	private HabaneroTransactionResponse processCredit(HabaneroTransactionRequest txRequest, HabaneroFundInfo fundInfo, boolean mock) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		HabaneroTransactionResponse response = new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus()));
		
		if (fundInfo.getGamestatemode() == GAMESTATEMODE_CONTINUATION || fundInfo.getGamestatemode() == GAMESTATEMODE_LAST) {
			
			GameRequest gameRequest = gameRequestDao.getGameRequest(txRequest.getFundtransferrequest().getToken());
		
			if (mock)
				mockDebitCero(txRequest, fundInfo);
			
			String token = txRequest.getFundtransferrequest().getToken();
			Long roundId = this.getGameRoundId(txRequest);
			
			Long winAmount = this.balanceStringToLong(fundInfo.getAmount());
			
			testTrigger(txRequest, "single-crediterror-before", "dc-crediterror-before");
			
			Message winMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, winAmount, fundInfo.getTransferid(), roundId, MethodType.WIN);
			Message winReturnMessage = gamePlatformIntegrationService.processMessage(winMessageFromGame);
			
			testTrigger(txRequest, "single-crediterror-after", "dc-crediterror-after");
			
			if (fundInfo.getJpwin() == true) {
				logger.info(String.format("Habanero: Jackpot Won: %1$s, jpcont: %2$s, amount=%3$s", fundInfo.getJpid(), fundInfo.getJpcont(), winAmount));
			}
			
			if (fundInfo.getIsbonus() == true && txRequest.getBonusdetails() != null) {
				logger.info(String.format("Habanero: Bonus: couponid=%1$s, couponcode: %2$s, coupontypeid: %3$s, amount=%4$s", txRequest.getBonusdetails().getCouponid(),  txRequest.getBonusdetails().getCouponcode(), txRequest.getBonusdetails().getCoupontypeid(), winAmount));
			}
			
			if (winReturnMessage.getResult().getSuccess() == 1L) {
				response.getFundtransferresponse().getStatus().setSuccess(true);
				response.getFundtransferresponse().setBalance(super.balanceLongToString(winReturnMessage.getResult().getReturnset().getBalance().getValue()));
				response.getFundtransferresponse().setCurrencycode(gameRequest.getPlayer().getCurrency().name().toUpperCase());
			}else {
				response.getFundtransferresponse().getStatus().setSuccess(false);
			}
		}else if (fundInfo.getGamestatemode() == GAMESTATEMODE_EXPIRED) {
			logger.debug("Habanero: game expired!");
			response = HabaneroTransactionResponse.createSuccessExpired();
		}else {
			throw new IllegalStateException(String.format("Habanero: credit with gamestate=%1$s is not possible", String.valueOf(fundInfo.getGamestatemode())));
		}
		
		return response;
	}

	private void testTrigger(HabaneroTransactionRequest txRequest, String single, String dual) throws TransactionNotSuccessException, GameRequestNotExistentException {
		if (!"prod".equalsIgnoreCase(ACTIVE_PROFILE)){
			if (single.equals(txRequest.getTt()) || dual.equals(txRequest.getTt())){
				if (txRequest.getTt().equals("session-expired")) {
					throw new GameRequestNotExistentException("GameRequest does not exist with token=" + txRequest.getFundtransferrequest().getToken());
				}
				throw new TransactionNotSuccessException("Habanero: Test Trigger Error " + txRequest.getTt(), 0);
			}
		}
	}
	
	private void mockDebitCero(HabaneroTransactionRequest txRequest, HabaneroFundInfo creditFundInfo) throws GameRequestNotExistentException, TransactionNotSuccessException {
		HabaneroFundInfo mockDebitFundInfo = new HabaneroFundInfo();
		mockDebitFundInfo.setAmount("0");
		mockDebitFundInfo.setCurrencycode(creditFundInfo.getCurrencycode());
		mockDebitFundInfo.setDtevent(creditFundInfo.getDtevent());
		mockDebitFundInfo.setGamestatemode(GAMESTATEMODE_CONTINUATION);
		mockDebitFundInfo.setInitialdebittransferid(creditFundInfo.getInitialdebittransferid());
		mockDebitFundInfo.setIsbonus(false);
		mockDebitFundInfo.setJpcont("0");
		mockDebitFundInfo.setJpid(creditFundInfo.getJpid());
		mockDebitFundInfo.setJpwin(false);
		mockDebitFundInfo.setTransferid(MethodType.BET + "-0-" + creditFundInfo.getTransferid());
		
		logger.info(String.format("Habanero: mocking a cero debit to make the pair fundInfo: %1$s", mockDebitFundInfo.toJson()));
		
		HabaneroTransactionResponse response = this.processDebit(txRequest, mockDebitFundInfo, !MOCK_DEBIT_CREDIT);
		if (response.getFundtransferresponse().getStatus().getSuccess() != true) {
			throw new TransactionNotSuccessException("Habanero: Can't mock 0 debit for transferid=" + creditFundInfo.getTransferid(), 101);
		}
	}
	
	private void mockCreditCero(HabaneroTransactionRequest txRequest, HabaneroFundInfo debitFundInfo) throws GameRequestNotExistentException, TransactionNotSuccessException {
		HabaneroFundInfo mockCreditFundInfo = new HabaneroFundInfo();
		mockCreditFundInfo.setAmount("0");
		mockCreditFundInfo.setCurrencycode(debitFundInfo.getCurrencycode());
		mockCreditFundInfo.setDtevent(debitFundInfo.getDtevent());
		mockCreditFundInfo.setGamestatemode(GAMESTATEMODE_CONTINUATION);
		mockCreditFundInfo.setInitialdebittransferid(debitFundInfo.getInitialdebittransferid());
		mockCreditFundInfo.setIsbonus(false);
		mockCreditFundInfo.setJpcont("0");
		mockCreditFundInfo.setJpid(debitFundInfo.getJpid());
		mockCreditFundInfo.setJpwin(false);
		mockCreditFundInfo.setTransferid(MethodType.WIN + "-0-" + debitFundInfo.getTransferid());
		
		logger.info(String.format("Habanero: mocking a cero credit to make the pair fundInfo: %1$s", mockCreditFundInfo.toJson()));
		
		HabaneroTransactionResponse response = this.processCredit(txRequest, mockCreditFundInfo, !MOCK_DEBIT_CREDIT);
		if (response.getFundtransferresponse().getStatus().getSuccess() != true) {
			throw new TransactionNotSuccessException("Habanero: Can't mock 0 credit for transferid=" + debitFundInfo.getTransferid(), 101);
		}
	}
	
	private int betWinDiff(HabaneroTransactionRequest txRequest, Game game) {
		Long gameRoundId = this.getGameRoundId(txRequest);
		
		List<Transaction> transactions = transactionService.getTransactionsByGameRoundId(0, -1 , gameRoundId, game);
		
		int bets = 0;
		int wins = 0;
		
		for (Transaction transaction : transactions) {
			if (transaction.getType() == TransactionType.BET) {
				bets++;
			}else if (transaction.getType() == TransactionType.BET_CORRECTION) {
				bets--;
			}else if (transaction.getType() == TransactionType.WIN) {
				wins++;
			}else if (transaction.getType() == TransactionType.WIN_CORRECTION) {
				wins--;
			}
		}
		
		return bets - wins;
	}
	
}
