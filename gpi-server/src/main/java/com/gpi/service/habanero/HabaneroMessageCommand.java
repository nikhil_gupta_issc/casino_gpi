package com.gpi.service.habanero;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.refund.transaction.RefundedTransactionService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.cache.CacheManager;
import com.habanero.domain.HabaneroRequest;
import com.habanero.domain.HabaneroResponse;

/**
 * Base message handler for Habanero integration
 * 
 * @author f.fernandez
 *
 */
public abstract class HabaneroMessageCommand {
	
	private static final Logger logger = LoggerFactory.getLogger(HabaneroMessageCommand.class);
	
	protected static final int ACCOUNT_PUBLISHER_CODE_INDEX = 0;
	
	protected static final int ACCOUNT_LOGIN_NAME_INDEX = 1;
	
	protected static final String ACCOUNT_INFO_SEPARATOR = "-";
	
	protected GamePlatformIntegrationService gamePlatformIntegrationService;

	protected GameRequestDao gameRequestDao;
	
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	
	protected CacheManager cacheManager;
	
	protected TransactionService transactionService;
	
	protected RefundedTransactionService refundedTransactionService;
	
	protected static final int BALANCE_COEFFICIENT = 100;
	
	/**
	 * Handles the requested message
	 * @param params map of key value parameter received
	 * @return response to be returned
	 * @throws GameRequestNotExistentException thrown when the game request does not exist
	 */
	public abstract HabaneroResponse execute(HabaneroRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException;
	
	protected void refreshToken(String sessionId, String token) {
		cacheManager.store(CacheConstants.HABANERO_TOKEN_KEY + "-" + sessionId, token, CacheConstants.HABANERO_TOKEN_TTL);
	}
	
	protected String balanceLongToString(Long balance) {
		return new BigDecimal(balance).divide(new BigDecimal(BALANCE_COEFFICIENT), 2, RoundingMode.UNNECESSARY).toString();
	}
	
	protected Long balanceStringToLong(String balance) {
		return new BigDecimal(balance).multiply(new BigDecimal(BALANCE_COEFFICIENT)).longValue();
	}
	
	protected Message playerInfo(String token) throws GameRequestNotExistentException, TransactionNotSuccessException {
		Message gpiFormattedMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_PLAYER_INFO);
		Message responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);
		
		if (responseMessage.getResult().getSuccess() == 1) {
			return responseMessage;
		}else {
			logger.error("error: " + responseMessage.getResult().getReturnset().getError().getValue() + ", message: " + responseMessage.getResult().getReturnset().getErrorCode() .getValue());
			throw new TransactionNotSuccessException(responseMessage.getResult().getReturnset().getError().getValue(), responseMessage.getResult().getReturnset().getErrorCode() .getValue());
		}
	}
	
	/**
	 * @param gamePlatformIntegrationUtils the gamePlatformIntegrationUtils to set
	 */
	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	/**
	 * @param gamePlatformIntegrationService the gamePlatformIntegrationService to set
	 */
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}


	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}


	/**
	 * @param transactionService the transactionService to set
	 */
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public void setRefundedTransactionService(RefundedTransactionService refundedTransactionService) {
		this.refundedTransactionService = refundedTransactionService;
	}
	
}
