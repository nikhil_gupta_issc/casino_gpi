package com.gpi.service.habanero;

import com.gpi.domain.audit.Transaction;
import com.gpi.domain.transaction.TransactionFilterBuilder;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.habanero.domain.HabaneroFundTransferResponse;
import com.habanero.domain.HabaneroQueryRequest;
import com.habanero.domain.HabaneroRequest;
import com.habanero.domain.HabaneroResponse;
import com.habanero.domain.HabaneroStatus;
import com.habanero.domain.HabaneroTransactionResponse;

/**
 * HabaneroMessageQuery
 * 
 * @author f.fernandez
 *
 */
public class HabaneroMessageQuery extends HabaneroMessageCommand{
	
	@Override
	public HabaneroResponse execute(HabaneroRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
		
		HabaneroQueryRequest queryRequest = (HabaneroQueryRequest)request;
		
		HabaneroTransactionResponse response = new HabaneroTransactionResponse(new HabaneroFundTransferResponse(new HabaneroStatus()));
		
		String[] account = queryRequest.getQueryrequest().getAccountid().split(ACCOUNT_INFO_SEPARATOR);
		
		Transaction transaction = transactionService.getTransactions(TransactionFilterBuilder.create()
																					.setProviderTransactionId(queryRequest.getQueryrequest().getTransferid())
																					.setPlayerPublisherCode(account[ACCOUNT_PUBLISHER_CODE_INDEX])
																					.setPlayerName(account[ACCOUNT_LOGIN_NAME_INDEX])
																					.build()).stream().findFirst().orElse(null);

		response.getFundtransferresponse().getStatus().setSuccess(transaction != null && transaction.isSuccess());
		
		
		return response;
	}

}
