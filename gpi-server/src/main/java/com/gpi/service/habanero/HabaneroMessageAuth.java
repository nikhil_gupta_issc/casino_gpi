package com.gpi.service.habanero;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.habanero.domain.HabaneroAuthRequest;
import com.habanero.domain.HabaneroAuthResponse;
import com.habanero.domain.HabaneroPlayerDetailResponse;
import com.habanero.domain.HabaneroRequest;
import com.habanero.domain.HabaneroResponse;
import com.habanero.domain.HabaneroStatus;

/**
 * HabaneroMessageAuth
 * 
 * @author f.fernandez
 *
 */
public class HabaneroMessageAuth extends HabaneroMessageCommand{
	
	@Override
	public HabaneroResponse execute(HabaneroRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
	
		HabaneroAuthRequest authRequest = (HabaneroAuthRequest)request;
		
		Message playerInfo = super.playerInfo(authRequest.getPlayerdetailrequest().getToken());
		
		GameRequest gameRequest = super.gameRequestDao.getGameRequest(authRequest.getPlayerdetailrequest().getToken());
		
		return new HabaneroAuthResponse(new HabaneroPlayerDetailResponse(
				gameRequest.getPublisher().getName() + ACCOUNT_INFO_SEPARATOR + playerInfo.getResult().getReturnset().getLoginName().getValue(),
				playerInfo.getResult().getReturnset().getLoginName().getValue(),
				super.balanceLongToString(playerInfo.getResult().getReturnset().getBalance().getValue()),
				playerInfo.getResult().getReturnset().getCurrency().getValue().toUpperCase(),
				gameRequest.getPublisher().getName(),
				HabaneroStatus.createSuccess()
			));
	}
}
