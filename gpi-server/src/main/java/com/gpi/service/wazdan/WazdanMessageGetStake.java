package com.gpi.service.wazdan;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.wazdan.domain.WazdanGetStakeRequest;
import com.wazdan.domain.WazdanGetStakeResponse;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanResponse;

/**
 * WazdanMessageGetStake
 * 
 * @author f.fernandez
 *
 */
public class WazdanMessageGetStake extends WazdanMessageCommand{
	
	@Override
	public WazdanResponse execute(WazdanRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
	
		WazdanGetStakeRequest getStakeRequest = (WazdanGetStakeRequest)request;
		
		String token = this.getToken(getStakeRequest.getToken());
		
		Long bet = super.balanceStringToLong(getStakeRequest.getStake());
		
		String roundId = String.valueOf(getStakeRequest.getSessionId()) + getStakeRequest.getGameNo();
		
		//BET
		Message betMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, bet, MethodType.BET.name() + "-" + getStakeRequest.getTransactionId(), Long.valueOf(roundId), MethodType.BET);
		Message betReturnMessage = gamePlatformIntegrationService.processMessage(betMessageFromGame);
		
		if (betReturnMessage.getResult().getSuccess() == 1L) {
			token = betReturnMessage.getResult().getReturnset().getToken().getValue();
			Long balance = betReturnMessage.getResult().getReturnset().getBalance().getValue();
			
			refreshToken(getStakeRequest.getToken(), token);
			
			WazdanGetStakeResponse response = new WazdanGetStakeResponse();
			response.setStatus(0);
			response.setCurrentFunds(this.balanceLongToDouble(balance));
			
			return response;
		}
		
		throw new TransactionNotSuccessException(betReturnMessage.getResult().getReturnset().getError().getValue(), betReturnMessage.getResult().getReturnset().getErrorCode().getValue());
	}
	
}
