package com.gpi.service.wazdan;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.utils.MessageHelper;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanResponse;
import com.wazdan.domain.WazdanRollbackStakeRequest;
import com.wazdan.domain.WazdanRollbackStakeResponse;

/**
 * WazdanMessageRollbackStake
 * 
 * @author f.fernandez
 *
 */
public class WazdanMessageRollbackStake extends WazdanMessageCommand{
	
	@Override
	public WazdanResponse execute(WazdanRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
	
		WazdanRollbackStakeRequest rollbackStakeRequest = (WazdanRollbackStakeRequest)request;
		
		String token = this.getToken(rollbackStakeRequest.getToken());
		
		String transactionId = String.valueOf(rollbackStakeRequest.getOriginalTransactionId());
		Long balance = 0L;
		Long bet = balanceStringToLong(rollbackStakeRequest.getStake());
		String roundId = String.valueOf(rollbackStakeRequest.getSessionId()) + rollbackStakeRequest.getGameNo();
		
		
		// Refund
		Message refundReturnMessage = this.refund(token, bet, Long.valueOf(roundId), MethodType.BET + "-" + transactionId);
		if (refundReturnMessage.getResult().getSuccess() == 1L) {
			token = refundReturnMessage.getResult().getReturnset().getToken().getValue();
			balance = refundReturnMessage.getResult().getReturnset().getBalance().getValue();
			
			// Balance
			Message playerInfoReturnMessage = super.playerInfo(token);
			token = playerInfoReturnMessage.getResult().getReturnset().getToken().getValue();
			balance = playerInfoReturnMessage.getResult().getReturnset().getBalance().getValue();
			refreshToken(rollbackStakeRequest.getToken(), token);

			
			WazdanRollbackStakeResponse response = new WazdanRollbackStakeResponse();
			response.setStatus(0);
			response.setCurrentFunds(this.balanceLongToDouble(balance));
			
			return response;
		}
		
		throw new TransactionNotSuccessException(refundReturnMessage.getResult().getReturnset().getError().getValue(), refundReturnMessage.getResult().getReturnset().getErrorCode().getValue());	
	}
	
	private Message refund(String token, Long bet, Long roundId, String transactionId) throws TransactionNotSuccessException, GameRequestNotExistentException {
		Message refundMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, bet, "REF-" + transactionId, roundId, MethodType.REFUND_TRANSACTION);
		MessageHelper.addRefundedTransactionIdToMethod(refundMessageFromGame, transactionId);
		Message refundReturnMessage = gamePlatformIntegrationService.processMessage(refundMessageFromGame);
		
		if (refundReturnMessage.getResult().getSuccess() == 1L) {
			return refundReturnMessage;
		}else {
			throw new TransactionNotSuccessException(refundReturnMessage.getResult().getReturnset().getError().getValue(), refundReturnMessage.getResult().getReturnset().getErrorCode().getValue());
		}
	}
	
}
