package com.gpi.service.wazdan;

import com.gpi.api.Message;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.wazdan.domain.WazdanGetFundsRequest;
import com.wazdan.domain.WazdanGetFundsResponse;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanResponse;

/**
 * WazdanMessageGetFunds
 * 
 * @author f.fernandez
 *
 */
public class WazdanMessageGetFunds extends WazdanMessageCommand{
	
	@Override
	public WazdanResponse execute(WazdanRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
	
		WazdanGetFundsRequest getFundsRequest = (WazdanGetFundsRequest)request;
		
		String token = this.getToken(getFundsRequest.getToken());
		
		// Balance
		Message playerInfoReturnMessage = super.playerInfo(token);
		token = playerInfoReturnMessage.getResult().getReturnset().getToken().getValue();
		Long balance = playerInfoReturnMessage.getResult().getReturnset().getBalance().getValue();
		refreshToken(getFundsRequest.getToken(), token);
		
		WazdanGetFundsResponse response = new WazdanGetFundsResponse();
		response.setStatus(0);
		response.setCurrentFunds(this.balanceLongToDouble(balance));
		
		return response;
	}
	
}
