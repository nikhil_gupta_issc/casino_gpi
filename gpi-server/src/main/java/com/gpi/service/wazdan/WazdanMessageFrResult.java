package com.gpi.service.wazdan;

import org.apache.commons.lang.NotImplementedException;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanResponse;

/**
 * WazdanMessageFrResult
 * 
 * @author f.fernandez
 *
 */
public class WazdanMessageFrResult extends WazdanMessageCommand{
	
	@Override
	public WazdanResponse execute(WazdanRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
		throw new NotImplementedException("FrResult not implemented");
	}
	
}
