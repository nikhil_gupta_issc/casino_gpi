package com.gpi.service.wazdan;

import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.wazdan.domain.WazdanGameCloseResponse;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanResponse;

/**
 * WazdanMessageGameClose
 * 
 * @author f.fernandez
 *
 */
public class WazdanMessageGameClose extends WazdanMessageCommand{
	
	@Override
	public WazdanResponse execute(WazdanRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
	
		WazdanGameCloseResponse response = new WazdanGameCloseResponse();
		response.setStatus(0);
		
		return response;
	}
	
}
