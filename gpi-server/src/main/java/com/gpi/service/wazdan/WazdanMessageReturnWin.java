package com.gpi.service.wazdan;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanResponse;
import com.wazdan.domain.WazdanReturnWinRequest;
import com.wazdan.domain.WazdanReturnWinResponse;

/**
 * WazdanMessageReturnWin
 * 
 * @author f.fernandez
 *
 */
public class WazdanMessageReturnWin extends WazdanMessageCommand{
	
	@Override
	public WazdanResponse execute(WazdanRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
	
		WazdanReturnWinRequest returnWinRequest = (WazdanReturnWinRequest)request;
		
		String token = this.getToken(returnWinRequest.getToken());
		
		Long balance = 0L;
		Long win = super.balanceStringToLong(returnWinRequest.getWin());
		
		String roundId = String.valueOf(returnWinRequest.getSessionId()) + returnWinRequest.getGameNo();
		
		//WIN
		Message winMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, win, MethodType.WIN.name() + "-" + returnWinRequest.getTransactionId(), Long.valueOf(roundId), MethodType.WIN);
		Message winReturnMessage = gamePlatformIntegrationService.processMessage(winMessageFromGame);
		
		if (winReturnMessage.getResult().getSuccess() == 1L) {
			token = winReturnMessage.getResult().getReturnset().getToken().getValue();
			balance = winReturnMessage.getResult().getReturnset().getBalance().getValue();
			
			refreshToken(returnWinRequest.getToken(), token);
			
			WazdanReturnWinResponse response = new WazdanReturnWinResponse();
			response.setStatus(0);
			response.setCurrentFunds(this.balanceLongToDouble(balance));
			
			return response;
		}
		
		//TODO: FFS what happen if win fails? how is it done the refund of the bet?
		
		throw new TransactionNotSuccessException(winReturnMessage.getResult().getReturnset().getError().getValue(), winReturnMessage.getResult().getReturnset().getErrorCode().getValue());
	}
	
}
