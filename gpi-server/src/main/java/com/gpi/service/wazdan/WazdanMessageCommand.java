package com.gpi.service.wazdan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.refund.transaction.RefundedTransactionService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.cache.CacheManager;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanResponse;

/**
 * Base message handler for Wazdan integration
 * 
 * @author f.fernandez
 *
 */
public abstract class WazdanMessageCommand {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(WazdanMessageCommand.class);
	
	protected GamePlatformIntegrationService gamePlatformIntegrationService;

	protected GameRequestDao gameRequestDao;
	
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	
	protected CacheManager cacheManager;
	
	protected TransactionService transactionService;
	
	protected RefundedTransactionService refundedTransactionService;
	
	protected static final double BALANCE_COEFFICIENT = 100D;
	
	/**
	 * Handles the requested message
	 * @param params map of key value parameter received
	 * @return response to be returned
	 * @throws GameRequestNotExistentException thrown when the game request does not exist
	 */
	public abstract WazdanResponse execute(WazdanRequest request) throws GameRequestNotExistentException,TransactionNotSuccessException;
	
	protected Double balanceLongToDouble(Long balance) {
		return balance/BALANCE_COEFFICIENT;
	}
	
	protected Long balanceStringToLong(Double balance) {
		return (long)(balance*BALANCE_COEFFICIENT);
	}
	
	
	protected String getToken(String originalToken) {
		String token = (String)cacheManager.getAndTouch(CacheConstants.WAZDAN_TOKEN_KEY + "-" + originalToken, CacheConstants.WAZDAN_TOKEN_TTL);
		return token != null ? token : originalToken;
	}
	
	protected void refreshToken(String originalToken, String newToken) {
		cacheManager.store(CacheConstants.WAZDAN_TOKEN_KEY + "-" + originalToken, newToken, CacheConstants.WAZDAN_TOKEN_TTL);
	}
	
	protected Message playerInfo(String token) throws GameRequestNotExistentException, TransactionNotSuccessException {
		Message gpiFormattedMessageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_PLAYER_INFO);
		Message responseMessage = gamePlatformIntegrationService.processMessage(gpiFormattedMessageFromGame);
		
		if (responseMessage.getResult().getSuccess() == 1) {
			return responseMessage;
		}else {
			throw new TransactionNotSuccessException(responseMessage.getResult().getReturnset().getError().getValue(), responseMessage.getResult().getReturnset().getErrorCode() .getValue());
		}
	}
	
	/**
	 * @param gamePlatformIntegrationUtils the gamePlatformIntegrationUtils to set
	 */
	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	/**
	 * @param gamePlatformIntegrationService the gamePlatformIntegrationService to set
	 */
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}


	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	/**
	 * @param transactionService the transactionService to set
	 */
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	/**
	 * @param refundedTransactionService the refundedTransactionService to set
	 */
	public void setRefundedTransactionService(RefundedTransactionService refundedTransactionService) {
		this.refundedTransactionService = refundedTransactionService;
	}
	
}
