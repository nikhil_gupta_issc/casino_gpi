package com.gpi.service.wazdan;

import com.gpi.api.Message;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.TransactionNotSuccessException;
import com.wazdan.domain.WazdanAuthenticateRequest;
import com.wazdan.domain.WazdanAuthenticateResponse;
import com.wazdan.domain.WazdanRequest;
import com.wazdan.domain.WazdanResponse;

/**
 * WazdanMessageAuthenticate
 * 
 * @author f.fernandez
 *
 */
public class WazdanMessageAuthenticate extends WazdanMessageCommand{
	
	@Override
	public WazdanResponse execute(WazdanRequest request) throws GameRequestNotExistentException, TransactionNotSuccessException {
	
		WazdanAuthenticateRequest authenticateRequest = (WazdanAuthenticateRequest)request;
		
		GameRequest gameRequest = super.gameRequestDao.getGameRequest(authenticateRequest.getToken());
		
		Message playerInfo = super.playerInfo(this.getToken(authenticateRequest.getToken()));
		this.refreshToken(authenticateRequest.getToken(), playerInfo.getResult().getReturnset().getToken().getValue());
		String pn = gameRequest.getPublisher().getName();
		String username = playerInfo.getResult().getReturnset().getLoginName().getValue();
		String currency = playerInfo.getResult().getReturnset().getCurrency().getValue();
		
		WazdanAuthenticateResponse response = new WazdanAuthenticateResponse();
		response.setStatus(0);
		response.setRemoteUserId(pn + "-" + username);
		response.setCurrency(currency);
		
		return response;
	}
	
}
