package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.utils.DeviceUtils;

public class WazdanOpenGameUrl implements OpenGameUrl {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(WazdanOpenGameUrl.class);

	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException {

		String[] type = (String[]) request.getParameterMap().get("type");
		if (type != null && type.length > 0 && StringUtils.equalsIgnoreCase(type[0], "FREE")) {
			builder.queryParam("mode", "demo");
		} else {
			builder.queryParam("mode", "real");
			builder.queryParam("license", "curacao");
		}

    	String device = DeviceUtils.resolveDevice(request);
    	if ("tablet".equals(device)) {
    		device = "mobile";
    	}
    	
    	builder.queryParam("platform", device);
    	
    	String homeURL = gameRequest.getHome();
    	builder.queryParam("lobbyUrl", homeURL);
    	
		return null;
	}

}
