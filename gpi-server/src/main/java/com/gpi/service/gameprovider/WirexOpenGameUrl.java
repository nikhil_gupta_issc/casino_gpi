package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.PlayerInfo;
import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.gpi.WirexService;
import com.gpi.utils.GamePlatformIntegrationUtils;

public class WirexOpenGameUrl implements OpenGameUrl {
	
    private WirexService wirexService;
    
	private GamePlatformIntegrationUtils gpiUtils;
	
	private static final Logger logger = LoggerFactory.getLogger(WirexOpenGameUrl.class);

	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException, NonExistentGameException {
		
		String gameName = game.getName();
    	ModelAndView mav;
    	Pattern p = Pattern.compile("WIREX-(.+)");
    	Matcher m = p.matcher(gameName);
    	if (m.matches()) {
    		String gameId = m.group(1);
    		PlayerInfo playerInfo = gpiUtils.loadPlayerInfo(gameRequest.getPn(), loadGameDto.getToken());
    		
        	url = wirexService.getGameURL(playerInfo.getToken(), gameId, playerInfo.getCurrency(), gameRequest.getLang());
    		logger.info("URL: "+url);
    		
        	if (url != null) {
        		mav = new ModelAndView("redirect:" + url);
        	} else {
        		mav = new ModelAndView("error");
                mav.addObject("error", "Invalid response");
        	}
        	return mav;
    	} else {
    		throw new NonExistentGameException("Game id " + gameName + " is malformed");
    	}        

	}
	
	public void setWirexService(WirexService wirexService) {
		this.wirexService = wirexService;
	}

	public void setGpiUtils(GamePlatformIntegrationUtils gpiUtils) {
		this.gpiUtils = gpiUtils;
	}

}
