package com.gpi.service.gameprovider;

import java.util.Map;

public class OpenGameUrlService {
	
    private Map<String, OpenGameUrl> providerUrlMap;
    
    public OpenGameUrl getImplementation (String name) {
    	return providerUrlMap.get(name);
    }

	public Map<String, OpenGameUrl> getProviderUrlMap() {
		return providerUrlMap;
	}

	public void setProviderUrlMap(Map<String, OpenGameUrl> providerUrlMap) {
		this.providerUrlMap = providerUrlMap;
	}

}
