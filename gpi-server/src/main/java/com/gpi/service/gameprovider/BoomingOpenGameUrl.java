package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.gpi.BoomingService;
import com.gpi.utils.DeviceUtils;

public class BoomingOpenGameUrl implements OpenGameUrl {

	private BoomingService boomingService;
	
	private static final Logger logger = LoggerFactory.getLogger(BoomingOpenGameUrl.class);

	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException, NonExistentGameException {
		ModelAndView mav = new ModelAndView("redirect:" + boomingService.getGameURL(
    			loadGameDto.getToken(), gameRequest.getPn(), 
    			gameRequest.getGame(), StringUtils.upperCase(gameRequest.getLang()), 
    			gameRequest.getType(), DeviceUtils.resolveDevice(request)));
    	return mav;

	}
	
	public BoomingService getBoomingService() {
		return boomingService;
	}

	public void setBoomingService(BoomingService boomingService) {
		this.boomingService = boomingService;
	}

}
