package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.utils.GamePlatformIntegrationUtils;

public class TheGamesCompanyOpenGameUrl implements OpenGameUrl {

	private GamePlatformIntegrationUtils gpiUtils;
	
	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException {
		
		String[] type = (String[]) request.getParameterMap().get("type");
		if (!StringUtils.equalsIgnoreCase(type[0], "FREE") || (type == null)) {
			PlayerInfo playerInfo = gpiUtils.loadPlayerInfo(gameRequest.getPn(), loadGameDto.getToken(), null);

			builder.queryParam("ccy", StringUtils.upperCase(playerInfo.getCurrency()));
		} else {
			builder.queryParam("ccy", "USD");
		}
		
		return null;
	}
	
	public void setGpiUtils(GamePlatformIntegrationUtils gpiUtils) {
		this.gpiUtils = gpiUtils;
	}
}
