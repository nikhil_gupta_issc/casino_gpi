package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.utils.ApplicationProperties;

public class HabaneroOpenGameUrl implements OpenGameUrl {
	
	 private static final Logger logger = LoggerFactory.getLogger(HabaneroOpenGameUrl.class);

	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException {
		String[] type = (String[]) request.getParameterMap().get("type");
		builder.queryParam("brandid", ApplicationProperties.getProperty("habanero.brandid"));
		builder.queryParam("mode", "FREE".equalsIgnoreCase(type[0]) ? "fun" : "real");
    	builder.queryParam("locale", gameRequest.getLang().toLowerCase());
    	builder.queryParam("lobbyUrl", gameRequest.getHome());
    	builder.queryParam("ifrm", request.getParameterMap().get("ifrm"));
    	builder.queryParam("segmentkey", gameRequest.getPn());
    	return null;

	}

}
