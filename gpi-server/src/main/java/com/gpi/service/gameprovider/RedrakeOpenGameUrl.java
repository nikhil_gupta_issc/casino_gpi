package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.utils.ApplicationProperties;

public class RedrakeOpenGameUrl implements OpenGameUrl {

	private static final Logger logger = LoggerFactory.getLogger(RedrakeOpenGameUrl.class);

	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException {
		builder.queryParam("sessionid", loadGameDto.getToken());
		String[] type = (String[]) request.getParameterMap().get("type");
		if (type != null && type.length > 0) {
			builder.queryParam("mode", StringUtils.equalsIgnoreCase(type[0], "FREE") ? "demo" : "real");
		} else {
			builder.queryParam("mode", "real");
		}

		String toSign = this.getPreparedParamList(Arrays.asList(builder.build().getQuery().split("&")));
		builder.queryParam("sig",
				this.bytesToHex(this.sha256(toSign, ApplicationProperties.getProperty("redrake.secret"))));

		return null;
	}

	/**
	 * Return the MAC of a String using SHA256
	 * 
	 * @param params
	 *            String to be ciphered
	 * @return The byte buffer of the result
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws Exception
	 */
	private byte[] sha256(String params, String key) throws NoSuchAlgorithmException, InvalidKeyException {
		Mac mac = Mac.getInstance("HmacSHA256");
		SecretKeySpec secret = new SecretKeySpec(key.getBytes(), "HmacSHA256");
		mac.init(secret);

		byte[] digest = mac.doFinal(params.getBytes());

		return digest;
	}

	private String getPreparedParamList(List<String> urlParams) {

		Collections.sort(urlParams, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});

		StringBuffer sb = new StringBuffer();

		for (String s : urlParams) {
			sb.append("|" + s);
		}

		return sb.toString();
	}

	/**
	 * Convert a byte buffer to hex string
	 * 
	 * @param bytes
	 *            Byte buffer
	 * @return hex string
	 */
	private String bytesToHex(byte[] bytes) {
		StringBuffer result = new StringBuffer();
		for (byte byt : bytes)
			result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
		return result.toString();
	}

}
