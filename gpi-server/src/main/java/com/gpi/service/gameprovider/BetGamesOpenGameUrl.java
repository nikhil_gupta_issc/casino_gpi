package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.game.Game;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.DeviceUtils;

public class BetGamesOpenGameUrl implements OpenGameUrl {

	private PublisherService publisherService;

	private static final Logger logger = LoggerFactory.getLogger(BetGamesOpenGameUrl.class);
	
	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException {
		ModelAndView betGamesMav = new ModelAndView("betgames");
    	
    	Publisher publisher = publisherService.findByName(gameRequest.getPn());
    	
    	String partnerCode = "wearecasino_" + publisher.getPartnerCode();
    	
    	String server = ApplicationProperties.getProperty("betgames.api.url");
    	
    	logger.info("Read betgames server from config. Server: " + server);
    	
    	String gameName = game.getName();
    	Pattern p = Pattern.compile("BGAMES-(\\d+)");
    	Matcher m = p.matcher(gameName);
    	
    	betGamesMav.addObject("lang", gameRequest.getLang());
    	betGamesMav.addObject("token", loadGameDto.getToken());
    	betGamesMav.addObject("partnerCode", partnerCode);
    	betGamesMav.addObject("server", server);
    	
    	String gameCode = null;
    	
    	if (m.matches()) {
    		gameCode = m.group(1);
    	}
    	betGamesMav.addObject("currentGame", gameCode);
    	
    	String homeUrl = gameRequest.getHome();
    	betGamesMav.addObject("homeUrl", homeUrl);
    	
    	boolean isMobile = !"desktop".equals(DeviceUtils.resolveDevice(request));
    	betGamesMav.addObject("isMobile", isMobile ? 1 : 0);

    	return betGamesMav;

	}
	
	public PublisherService getPublisherService() {
		return publisherService;
	}

	public void setPublisherService(PublisherService publisherService) {
		this.publisherService = publisherService;
	}

}
