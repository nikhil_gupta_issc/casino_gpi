package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.gpi.SpinmaticService;
import com.gpi.service.player.PlayerService;
import com.gpi.service.publisher.PublisherService;
import com.gpi.service.spinmatic.SpinmaticHandler;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.spinmatic.api.domain.SpinmaticOpenGameData;

/**
 * Open game url handler for Spinmatic
 * 
 * @author Alexandre
 *
 */
public class SpinmaticOpenGameUrl implements OpenGameUrl {

    private SpinmaticService spinmaticService;
    
	private GamePlatformIntegrationUtils gpiUtils;
	
	private PlayerService playerService;
	
	private PublisherService publisherService;
	
	private PlayerInfoDao playerInfoDao;
    
    private static final Logger logger = LoggerFactory.getLogger(SpinmaticOpenGameUrl.class);
    
	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException, NonExistentGameException {
		String gameName = game.getName();
    	Pattern p = Pattern.compile("SPINMATIC-(\\d+)");
    	Matcher m = p.matcher(gameName);
    	if (m.matches()) {
    		int gameId = Integer.valueOf(m.group(1));
    		
    		String[] type = (String[]) request.getParameterMap().get("type");
    		
    		String homeURL = gameRequest.getHome();

    		if ("FREE".equalsIgnoreCase(type[0])) {
    			String freeGameURL = ApplicationProperties.getProperty("spinmatic.api.freeurl");
    			url = freeGameURL + gameId;
    		} else {
    			PlayerInfo playerInfo = gpiUtils.loadPlayerInfo(gameRequest.getPn(), loadGameDto.getToken());
        		playerInfo.setGameName(gameName);
        		
        		int idPublisher = this.getPublisherId(gameRequest.getPn());
        		long playerId = this.getPlayerId(playerInfo.getUsernameInPublisher(), idPublisher);
        		
        		SpinmaticOpenGameData openGameData = spinmaticService.getOpenGameData(gameId, playerInfo.getUsername(), playerInfo.getCurrency(), playerId);
        		url = openGameData.getLobbyURL();
        		String spinmaticToken = openGameData.getSessionID();
        		
        		this.playerInfoDao.savePlayerInfo(SpinmaticHandler.getPlayerInfoKey(spinmaticToken), playerInfo);
    		}
    		
    		logger.info("URL: "+url);
    		ModelAndView mav;
        	if (url != null) {
        		url = this.addLobbyURL(url, homeURL);
        		mav = new ModelAndView("redirect:" + url);
        	} else {
        		mav = new ModelAndView("error");
                mav.addObject("error", "Invalid response");
        	}
        	return mav;
    	} else {
    		throw new NonExistentGameException("Game id " + gameName + " is malformed");
    	}

	}
	
	/**
	 * @param playerName username of the player
	 * @param publisherId id of the publisher
	 * @return id of the player in gpi
	 */
	private long getPlayerId(String playerName, int publisherId) {
		Player player = this.playerService.findUserByUserNameAndPublisher(playerName, publisherId);
		return player.getId();
	}
	
	/**
	 * @param publisherName name of the publisher
	 * @return id of the publisher
	 * @throws NonExistentPublisherException
	 */
	private int getPublisherId(String publisherName) throws NonExistentPublisherException {
		Publisher publisher = this.publisherService.findByName(publisherName);
		return publisher.getId();
	}
	
	/**
	 * @param baseURL base url returned by spinmatic service
	 * @param homeURL home url of the publisher
	 * @return base url with the extra lobby url parameter
	 */
	private String addLobbyURL(String baseURL, String homeURL) {
		return baseURL + "&lobbyUrl=" + homeURL;
	}
	
	public void setPlayerInfoDao(PlayerInfoDao playerInfoDao) {
		this.playerInfoDao = playerInfoDao;
	}
	
	public void setPlayerService(PlayerService playerService) {
		this.playerService = playerService;
	}
	
	public void setPublisherService(PublisherService publisherService) {
		this.publisherService = publisherService;
	}

	public void setSpinmaticService(SpinmaticService spinmaticService) {
		this.spinmaticService = spinmaticService;
	}

	public void setGpiUtils(GamePlatformIntegrationUtils gpiUtils) {
		this.gpiUtils = gpiUtils;
	}

}
