package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.PlayerInfo;
import com.gpi.domain.game.Game;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.DeviceUtils;
import com.gpi.utils.GamePlatformIntegrationUtils;

public class FaziOpenGameUrl implements OpenGameUrl {

	private GamePlatformIntegrationUtils gpiUtils;
    
	private static final Logger logger = LoggerFactory.getLogger(FaziOpenGameUrl.class);
	
	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException {
		Map<String, String> faziLanguages = new HashMap<>();
      	faziLanguages.put("en", "eng");
      	faziLanguages.put("fr", "fra");
      	faziLanguages.put("es", "spa");
      	faziLanguages.put("sr", "srb");
      	faziLanguages.put("pt", "por");
      	faziLanguages.put("nl", "nld");
    	
    	String language = StringUtils.lowerCase(gameRequest.getLang());
    	if (language == null) {
    		language = "en";
    	}
    	builder.queryParam("languageCode", faziLanguages.get(language));
    	
    	String device = DeviceUtils.resolveDevice(request);
    	if ("tablet".equals(device)) {
    		device = "mobile";
    	}
    	builder.queryParam("platform", device);
    	
		String[] type = (String[]) request.getParameterMap().get("type");
		
		int moneyType;
		String currency;
		
		if ("FREE".equals(type[0])) {
			moneyType = 0;
			currency = (String) request.getParameterMap().get("currency");
			
			if (currency == null) {
				currency = "usd";
			}
		} else {
			moneyType = 1;
			PlayerInfo playerInfo = gpiUtils.loadPlayerInfo(gameRequest.getPn(), loadGameDto.getToken(), language);
			currency = playerInfo.getCurrency();
		}
		
		builder.queryParam("currency", StringUtils.lowerCase(currency));
		
		builder.queryParam("moneyType", moneyType);
    	String homeURL = gameRequest.getHome();
    	
    	builder.queryParam("lobbyUrl", homeURL);
    	builder.queryParam("integrationType", 12);
    	
    	if ("prod".equalsIgnoreCase(ApplicationProperties.getProperty("active.profile"))) {
        	String roomID = gameRequest.getPn();
        	builder.queryParam("roomID", roomID);
    	}
    	
    	return null;

	}

	public void setGpiUtils(GamePlatformIntegrationUtils gpiUtils) {
		this.gpiUtils = gpiUtils;
	}
	
	
}
