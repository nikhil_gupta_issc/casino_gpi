package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.game.Game;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.evoplay.EvoplayCallbackProcessor;
import com.gpi.service.gpi.EvoplayService;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.GamePlatformIntegrationUtils;

public class EvoplayOpenGameUrl implements OpenGameUrl {

    private EvoplayService evoplayService;
    
	private GamePlatformIntegrationUtils gpiUtils;
	
	private PlayerInfoDao playerInfoDaoCache;
    
    private static final Logger logger = LoggerFactory.getLogger(EvoplayOpenGameUrl.class);
    
	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException, NonExistentGameException {
		String gameName = game.getName();
    	
    	Pattern p = Pattern.compile("EVOPLAY-(\\d+)");
    	Matcher m = p.matcher(gameName);
    	if (m.matches()) {
    		Integer gameId = Integer.valueOf(m.group(1));
    		String[] type = (String[]) request.getParameterMap().get("type");
    		
        	String homeURL = gameRequest.getHome();
        	
        	String language = gameRequest.getLang();
        	if (language == null) {
        		language = "en";
        	}
        	
    		if ("FREE".equalsIgnoreCase(type[0])) {
        		url = evoplayService.getGameURL(gameId, "demo", "USD", language, homeURL);
    		} else {
    			PlayerInfo playerInfo = gpiUtils.loadPlayerInfo(gameRequest.getPn(), loadGameDto.getToken());
        		playerInfoDaoCache.savePlayerInfo(EvoplayCallbackProcessor.getPlayerInfoKey(loadGameDto.getToken()), playerInfo);
        		
        		url = evoplayService.getGameURL(gameId, playerInfo.getToken(), playerInfo.getCurrency(), language, homeURL);
    		}
    		logger.info("URL: "+url);
    		
    		ModelAndView mav;
        	if (url != null) {
        		mav = new ModelAndView("redirect:" + url);
        	} else {
        		mav = new ModelAndView("error");
                mav.addObject("error", "Invalid response");
        	}
        	return mav;
    	} else {
    		throw new NonExistentGameException("Game id " + gameName + " is malformed");
    	}            

	}
	
	
	public void setEvoplayService(EvoplayService evoplayService) {
		this.evoplayService = evoplayService;
	}


	public void setGpiUtils(GamePlatformIntegrationUtils gpiUtils) {
		this.gpiUtils = gpiUtils;
	}

	public void setPlayerInfoDaoCache(PlayerInfoDao playerInfoDaoCache) {
		this.playerInfoDaoCache = playerInfoDaoCache;
	}

}
