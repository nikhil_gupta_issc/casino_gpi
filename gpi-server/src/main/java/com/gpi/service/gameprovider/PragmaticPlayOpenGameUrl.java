package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.PlayerInfo;
import com.gpi.domain.game.Game;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.gpi.PragmaticPlayService;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.GamePlatformIntegrationUtils;

public class PragmaticPlayOpenGameUrl implements OpenGameUrl {

    private PragmaticPlayService pragmaticPlayService;
    
	private GamePlatformIntegrationUtils gpiUtils;
    
	private static final Logger logger = LoggerFactory.getLogger(PragmaticPlayOpenGameUrl.class);
    
	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException, NonExistentGameException {

		String gameName = game.getName();
    	
    	Pattern p = Pattern.compile("PRPLAY-(.+)");
    	Matcher m = p.matcher(gameName);
    	ModelAndView mav;
    	if (m.matches()) {
    		String gameId = m.group(1);
    		String[] type = (String[]) request.getParameterMap().get("type");
    		
    		String homeURL = gameRequest.getHome();
    		
    		if ("FREE".equalsIgnoreCase(type[0])) {
        		url = pragmaticPlayService.getFreeGameURL(gameId, gameRequest.getLang(), "USD", homeURL);
    		} else {
    			PlayerInfo playerInfo = gpiUtils.loadPlayerInfo(gameRequest.getPn(), loadGameDto.getToken());
        		url = pragmaticPlayService.getGameURL(playerInfo.getToken(), gameId, playerInfo.getLanguage(), gameRequest.getPn(), homeURL);
    		}
    		logger.info("URL: "+url);
    		
        	if (url != null) {
        		mav = new ModelAndView("redirect:" + url);
        	} else {
        		mav = new ModelAndView("error");
                mav.addObject("error", "Invalid response");
        	}
        	return mav;
    	} else {
    		throw new NonExistentGameException("Game id " + gameName + " is malformed");
    	}
	}
	
	public void setPragmaticPlayService(PragmaticPlayService pragmaticPlayService) {
		this.pragmaticPlayService = pragmaticPlayService;
	}

	public void setGpiUtils(GamePlatformIntegrationUtils gpiUtils) {
		this.gpiUtils = gpiUtils;
	}

}
