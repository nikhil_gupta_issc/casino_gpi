package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.game.Game;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.OrtizUtils;

public class OrtizOpenGameUrl implements OpenGameUrl {

	private static final Logger logger = LoggerFactory.getLogger(OrtizOpenGameUrl.class);

	private GamePlatformIntegrationUtils gpiUtils;

	private PlayerInfoDao playerInfoDaoCache;

	private PublisherService publisherService;

	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException {
		String[] type = (String[]) request.getParameterMap().get("type");
		if (type != null && type.length > 0) {
			builder.queryParam("p", StringUtils.equalsIgnoreCase(type[0], "FREE") ? "D" : "P");
		} else {
			builder.queryParam("p", "P");
		}

		String language = StringUtils.upperCase(gameRequest.getLang());

		builder.queryParam("ul", language);

		if (!StringUtils.equalsIgnoreCase(type[0], "FREE") || (type == null)) {
			PlayerInfo playerInfo = gpiUtils.loadPlayerInfo(gameRequest.getPn(), loadGameDto.getToken(), language);
			String playerInfoKey = OrtizUtils.getPlayerInfoKey(loadGameDto.getToken());
			
			playerInfoDaoCache.savePlayerInfo(playerInfoKey, playerInfo);
			logger.info("Game open. Token is " + playerInfo.getToken());

			builder.queryParam("uc", StringUtils.upperCase(playerInfo.getCurrency()));
			builder.queryParam("t", playerInfo.getToken());
		} else {
			builder.queryParam("uc", "USD");
			builder.queryParam("t", loadGameDto.getToken());
		}

		Publisher publisher = publisherService.findByName(gameRequest.getPn());
		builder.queryParam("cid", publisher.getOrtizCasinoNumber());
		return null;

	}
	
	public void setGpiUtils(GamePlatformIntegrationUtils gpiUtils) {
		this.gpiUtils = gpiUtils;
	}

	public void setPlayerInfoDaoCache(PlayerInfoDao playerInfoDaoCache) {
		this.playerInfoDaoCache = playerInfoDaoCache;
	}

	public void setPublisherService(PublisherService publisherService) {
		this.publisherService = publisherService;
	}

}
