package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.heliogaming.HeliogamingHandler;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.cache.CacheManager;

public class HeliogamingOpenGameUrl implements OpenGameUrl {

	private GamePlatformIntegrationUtils gpiUtils;
	private PlayerInfoDao playerInfoDao;
	private CacheManager cacheManager;
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(HeliogamingOpenGameUrl.class);
	
	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException, NonExistentGameException {
		
		ModelAndView heliogamingMav = new ModelAndView("heliogaming");
    	String gameName = game.getName();
    	Pattern p = Pattern.compile("HELIOGAMING-(.*)");
    	Matcher m = p.matcher(gameName);
    	
    	String gameCode = null;
    	
    	if (m.matches()) {
    		gameCode = m.group(1);
        	heliogamingMav.addObject("gameGroupCode", gameCode);
        	
        	String apiKey = ApplicationProperties.getProperty("heliogaming.api.key");
        	heliogamingMav.addObject("apiKey", apiKey);
        	
        	PlayerInfo playerInfo = gpiUtils.loadPlayerInfo(gameRequest.getPn(), loadGameDto.getToken());
        	playerInfoDao.savePlayerInfo(HeliogamingHandler.getPlayerInfoKey(playerInfo.getUsername()), playerInfo);
        	cacheManager.store(HeliogamingHandler.getSessionKey(loadGameDto.getToken()), playerInfo.getUsername(), CacheConstants.HELIOGAMING_TOKEN_TTL_KEY);
        	
        	String sessionKey = loadGameDto.getToken();
        	heliogamingMav.addObject("sessionKey", sessionKey);
        	
        	heliogamingMav.addObject("languageCode", StringUtils.upperCase(gameRequest.getLang()));
        	heliogamingMav.addObject("currencyCode", playerInfo.getCurrency());
        	
        	String gameLaunchURL = ApplicationProperties.getProperty("heliogaming.api.gamelaunch");
        	heliogamingMav.addObject("gameLaunchUrl", gameLaunchURL);
    		
    	} else {
    		throw new NonExistentGameException("Game id " + gameName + " is malformed");
    	}
    	
    	return heliogamingMav;

	}
	
	public void setGpiUtils(GamePlatformIntegrationUtils gpiUtils) {
		this.gpiUtils = gpiUtils;
	}

	public void setPlayerInfoDao(PlayerInfoDao playerInfoDao) {
		this.playerInfoDao = playerInfoDao;
	}

	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
}
