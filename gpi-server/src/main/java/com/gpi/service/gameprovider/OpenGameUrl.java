package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.exceptions.NonExistentPublisherException;

public interface OpenGameUrl {

	ModelAndView processURL(UriComponentsBuilder builder,  HttpServletRequest request,  LoadGameDto loadGameDto, LoadGameRequest gameRequest, Game game, String url)  throws InvalidKeyException, NoSuchAlgorithmException, GameRequestNotExistentException, NonExistentPublisherException, NonExistentGameException ;
}
