package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.service.onextwo.OnexTwoHandler;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.DeviceUtils;
import com.gpi.utils.GamePlatformIntegrationUtils;

public class OnexTwoOpenGameUrl implements OpenGameUrl {
	
	private GamePlatformIntegrationUtils gpiUtils;

	private PlayerInfoDao playerInfoDao;

	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException {
		
		String[] type = (String[]) request.getParameterMap().get("type");
		boolean free = "FREE".equalsIgnoreCase(type[0]);
		
		if (!free) {
			PlayerInfo playerInfo = gpiUtils.loadPlayerInfo(gameRequest.getPn(), loadGameDto.getToken());
			this.playerInfoDao.savePlayerInfo(OnexTwoHandler.getPlayerInfoKey(loadGameDto.getToken()), playerInfo);	
			builder.queryParam("sessionID", playerInfo.getToken());
		}else {
		    byte[] array = new byte[7];
		    new Random().nextBytes(array);
			builder.queryParam("sessionID", RandomStringUtils.random(10, true, false));
		}
		
		String siteID = ApplicationProperties.getProperty("1x2.api.siteid");
		builder.queryParam("siteID", siteID);
		
		String device = DeviceUtils.resolveDevice(request);
		builder.queryParam("platform", "desktop".equals(device) ? "desktop" : "mobile");
		
		String home = gameRequest.getHome();
		if (home != null) {
			builder.queryParam("lobbyUrl", gameRequest.getHome());
		}

		
		builder.queryParam("playMode", free ? "fun" : "real");
		
		return null;
	}

	public void setGpiUtils(GamePlatformIntegrationUtils gpiUtils) {
		this.gpiUtils = gpiUtils;
	}

	public void setPlayerInfoDao(PlayerInfoDao playerInfoDao) {
		this.playerInfoDao = playerInfoDao;
	}

}
