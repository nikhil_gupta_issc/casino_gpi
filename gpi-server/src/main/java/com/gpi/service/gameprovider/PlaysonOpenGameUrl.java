package com.gpi.service.gameprovider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.gpi.domain.game.Game;
import com.gpi.dto.LoadGameDto;
import com.gpi.dto.LoadGameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.exceptions.NonExistentPublisherException;

public class PlaysonOpenGameUrl implements OpenGameUrl {
	
    private static final Logger logger = LoggerFactory.getLogger(PlaysonOpenGameUrl.class);
    
	@Override
	public ModelAndView processURL(UriComponentsBuilder builder, HttpServletRequest request, LoadGameDto loadGameDto,
			LoadGameRequest gameRequest, Game game, String url) throws InvalidKeyException, NoSuchAlgorithmException,
			GameRequestNotExistentException, NonExistentPublisherException {
    	String homeURL = gameRequest.getHome();
    	
    	builder.queryParam("exit_url", homeURL);
    	builder.queryParam("platform", "desktop".equals(gameRequest.getDevice()) ? "desk" : "mob");
    	
    	return null;

	}
}
