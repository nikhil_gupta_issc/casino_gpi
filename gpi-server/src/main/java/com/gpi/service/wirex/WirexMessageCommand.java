package com.gpi.service.wirex;

import java.util.Map;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.gpi.GamePlatformIntegrationService;
import com.gpi.service.gpi.impl.PragmaticPlayServiceImpl;
import com.gpi.utils.GamePlatformIntegrationUtils;
import com.gpi.utils.MessageHelper;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

import it.wirex.api.WirexMethod;
import it.wirex.api.domain.WirexResponse;

/**
 * Base message handler for Wirex integration
 * 
 * @author Alexandre
 *
 */
public abstract class WirexMessageCommand {
	
	protected GamePlatformIntegrationService gamePlatformIntegrationService;

	protected GameRequestDao gameRequestDao;
	
	protected GamePlatformIntegrationUtils gamePlatformIntegrationUtils;
	
	protected PlayerInfoDao playerInfoDaoCache;
	
	protected static final int BALANCE_COEFFICIENT = 100;
	
	/**
	 * Handles the requested message
	 * @param sessionId id of the player session
	 * @param params map of key value parameter received
	 * @param method method to call
	 * @return response to be returned
	 * @throws GameRequestNotExistentException thrown when the game request does not exist
	 */
	public abstract WirexResponse execute(String sessionId, Map<String, String> params, WirexMethod method) throws GameRequestNotExistentException;
	
	/**
	 * @param key identifier of the player
	 * @return player info identifier in the wirex playerinfo cache
	 */
	public static String getPlayerInfoKey(String key) {
		return CacheConstants.WIREX_PLAYERINFO_KEY + key;
	}
	
	protected void setPlayerInfoTokenAndStore(String token, PlayerInfo playerInfo) {
		playerInfo.setToken(token);
		this.playerInfoDaoCache.savePlayerInfo(getPlayerInfoKey(playerInfo.getUsername()), playerInfo);
	}
	
	/**
	 * @param playerInfoDaoCache the playerInfoDaoCache to set
	 */
	public void setPlayerInfoDaoCache(PlayerInfoDao playerInfoDaoCache) {
		this.playerInfoDaoCache = playerInfoDaoCache;
	}
	
	/**
	 * @param gamePlatformIntegrationUtils the gamePlatformIntegrationUtils to set
	 */
	public void setGamePlatformIntegrationUtils(GamePlatformIntegrationUtils gamePlatformIntegrationUtils) {
		this.gamePlatformIntegrationUtils = gamePlatformIntegrationUtils;
	}

	/**
	 * @param gamePlatformIntegrationService the gamePlatformIntegrationService to set
	 */
	public void setGamePlatformIntegrationService(GamePlatformIntegrationService gamePlatformIntegrationService) {
		this.gamePlatformIntegrationService = gamePlatformIntegrationService;
	}

	/**
	 * @param gameRequestDao the gameRequestDao to set
	 */
	public void setGameRequestDao(GameRequestDao gameRequestDao) {
		this.gameRequestDao = gameRequestDao;
	}
}
