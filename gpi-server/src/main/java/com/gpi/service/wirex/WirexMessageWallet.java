package com.gpi.service.wirex;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayBalanceResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

import it.wirex.api.WirexErrorCodes;
import it.wirex.api.WirexMethod;
import it.wirex.api.domain.WirexError;
import it.wirex.api.domain.WirexResponse;
import it.wirex.api.domain.WirexWallet;
import it.wirex.api.domain.WirexWalletResponse;

/**
 * Get session wallet message handler for Wirex integration
 * 
 * @author Alexandre
 *
 */
public class WirexMessageWallet extends WirexMessageCommand {
	
	protected static final Logger logger = LoggerFactory.getLogger(WirexMessageWallet.class);

	@Override
	public WirexResponse execute(String sessionId, Map<String, String> params, WirexMethod method) throws GameRequestNotExistentException {
		PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(getPlayerInfoKey(sessionId));

		Message messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfo.getToken(), null, null, null, MethodType.GET_BALANCE);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);

		String currency;
		double balance;
		
		WirexResponse response;

		if (returnMessage.getResult().getSuccess() == 1) {
			currency = playerInfo.getCurrency();
			balance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			
			String gpiToken = returnMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(gpiToken, playerInfo);
			
			response = new WirexWalletResponse();
			WirexWallet wallet = new WirexWallet();
			
			wallet.setBalance(balance);
			wallet.setCurrency(currency);
			
			((WirexWalletResponse)response).setResult(wallet);
			logger.info("Wallet response: " + ((WirexWalletResponse)response).toString());
			
		} else {
			response = new WirexResponse();
			response.setStatusCode(-1);
			
			WirexError error = new WirexError();
			
			error.setTypeCode(WirexErrorCodes.INVALID_TOKEN);
			
			response.setError(error);
		}
		
		return response;
	}

}
