package com.gpi.service.wirex;

import java.math.BigInteger;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.domain.audit.Transaction;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.MessageHelper;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayBetResponse;
import com.pragmaticplay.api.domain.PragmaticPlayRefundResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

import it.wirex.api.WirexErrorCodes;
import it.wirex.api.WirexMethod;
import it.wirex.api.domain.WirexCancelResponse;
import it.wirex.api.domain.WirexError;
import it.wirex.api.domain.WirexResponse;
import it.wirex.api.domain.WirexTransaction;
import it.wirex.api.domain.WirexWallet;

/**
 * Cancel and rollback message handler for Wirex integration
 * 
 * @author Alexandre
 *
 */
public class WirexMessageCancelRollback extends WirexMessageCommand {
	
	protected static final Logger logger = LoggerFactory.getLogger(WirexMessageCancelRollback.class);
	private TransactionService transactionService;

	@Override
	public WirexResponse execute(String sessionId, Map<String, String> params, WirexMethod method) throws GameRequestNotExistentException {
		String transactionId = params.get("transactionUid");
		String refundedTransactionId;
		
		if (WirexMethod.ROLLBACK.equals(method)) {
			refundedTransactionId = params.get("transactionUidToRollback");
		} else {
			refundedTransactionId = params.get("transactionUidToCancel");
		}
		
		PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(getPlayerInfoKey(sessionId));
		String playerInfoToken = playerInfo.getToken();
		
		Transaction transactionToRefund = getTransactionToRefund(refundedTransactionId, playerInfoToken);
		WirexResponse response;
		
		if (transactionToRefund != null) {
			long transactionToRefundAmount = transactionToRefund.getAmount();
			long transactionToRefundGameRoundId = transactionToRefund.getRound().getGameRoundId();
			
			Message messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, transactionToRefundAmount, transactionId, transactionToRefundGameRoundId, MethodType.REFUND_TRANSACTION);
			MessageHelper.addRefundedTransactionIdToMethod(messageFromGame, refundedTransactionId);
			Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);

			if ((returnMessage.getResult().getSuccess() == 1) || returnMessage.getResult().getSuccess() == 0 && returnMessage.getResult().getReturnset().getTransactionId() != null) {
				// success or transaction already processed
				String gpiToken = returnMessage.getResult().getReturnset().getToken().getValue();
				setPlayerInfoTokenAndStore(gpiToken, playerInfo);
			
				response = new WirexCancelResponse();
				
				long gpiBalance = returnMessage.getResult().getReturnset().getBalance().getValue();
				
				double balance = gamePlatformIntegrationUtils.getAmountFromGPI(gpiBalance, BALANCE_COEFFICIENT);
				
				WirexWallet wallet = new WirexWallet();
				wallet.setBalance(balance);
				wallet.setCurrency(playerInfo.getCurrency());
				
				WirexTransaction transaction = new WirexTransaction();
				transaction.setWallet(wallet);
				
				((WirexCancelResponse) response).setResult(transaction);
				logger.info("Refund response: " + ((WirexCancelResponse)response).toString());
			} else {
				response = new WirexResponse();
				response.setStatusCode(-1);
				WirexError error = new WirexError();
				error.setTypeCode(WirexErrorCodes.INTERNAL_SERVER_ERROR);
				logger.error(returnMessage.getResult().toString());
			}
			
		} else {
			response = new WirexResponse();
			response.setStatusCode(-1);
			WirexError error = new WirexError();
			error.setTypeCode(WirexErrorCodes.INVALID_SESSION);
			logger.error(error.toString());
		}
		
		return response;
	}
	
	/**
	 * @param transactionToRefundId id of the original transaction to be refunded
	 * @param token token of the player
	 * @return original transaction to be refunded
	 */
	private Transaction getTransactionToRefund(String transactionToRefundId, String token) {
		GameRequest gameRequest;
		try {
			gameRequest = gameRequestDao.getGameRequest(token);
			
			String gameName = gameRequest.getGame().getName();
			Transaction transactionToRefund = transactionService.getTransaction(transactionToRefundId, gameName);
			
			return transactionToRefund;
		} catch (GameRequestNotExistentException e) {
			logger.error(e.getMessage(), e);
		}
		
		return null;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

}
