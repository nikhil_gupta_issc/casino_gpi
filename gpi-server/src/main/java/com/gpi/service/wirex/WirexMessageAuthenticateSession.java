package com.gpi.service.wirex;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.GameRequest;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.PlayerIdentifierBuilder;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayAuthenticateResponse;
import com.pragmaticplay.api.domain.PragmaticPlayBalanceResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;

import it.wirex.api.domain.WirexResponse;
import it.wirex.api.domain.WirexSession;
import it.wirex.api.WirexErrorCodes;
import it.wirex.api.WirexMethod;
import it.wirex.api.domain.WirexAuthenticationSessionResponse;
import it.wirex.api.domain.WirexError;

/**
 * Authentication and session message handler for Wirex integration
 * 
 * @author Alexandre
 *
 */
public class WirexMessageAuthenticateSession extends WirexMessageCommand {

	protected static final Logger logger = LoggerFactory.getLogger(WirexMessageAuthenticateSession.class);
	
	@Override
	public WirexResponse execute(String sessionId, Map<String, String> params, WirexMethod method) throws GameRequestNotExistentException {
		
		String token;
		PlayerInfo playerInfo = null;
		String userId = null;
		
		if (WirexMethod.AUTHENTICATE.equals(method)) {
			token = params.get("token");
		} else {
			playerInfo = playerInfoDaoCache.findPlayerInfo(sessionId);
			token = playerInfo.getToken();
		}
		
		Message messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(token, null, null, null, MethodType.GET_PLAYER_INFO);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);

		WirexResponse response;
		
		if (returnMessage.getResult().getSuccess() == 1) {
			String currency = returnMessage.getResult().getReturnset().getCurrency().getValue();
			String username = returnMessage.getResult().getReturnset().getLoginName().getValue();
			
			if (WirexMethod.AUTHENTICATE.equals(method)) {
				GameRequest gameRequest = gameRequestDao.getGameRequest(token);
				
				playerInfo = new PlayerInfo();
				
				PlayerIdentifierBuilder playerIdentifierBuilder = new PlayerIdentifierBuilder();
				playerIdentifierBuilder.setPublisherName(gameRequest.getPublisher().getName());
				playerIdentifierBuilder.setUsername(username);
				userId = playerIdentifierBuilder.build();
				
				playerInfo.setUsername(userId);
				playerInfo.setCurrency(currency);
			} else {
				userId = playerInfo.getUsername();
			}
			
			String responseToken = returnMessage.getResult().getReturnset().getToken().getValue();
			
			playerInfo.setToken(responseToken);
			setPlayerInfoTokenAndStore(responseToken, playerInfo);
			
			response = new WirexAuthenticationSessionResponse();
			
			WirexSession wirexSession = new WirexSession();
			
			wirexSession.setSessionId(userId);
			wirexSession.setUserId(Math.abs(userId.hashCode()));
			wirexSession.setCurrency(currency);
			wirexSession.setUsername(username);
			wirexSession.setCountry("IT");
			((WirexAuthenticationSessionResponse) response).setResult(wirexSession);
			
			logger.info(method.toString() + " response: " + ((WirexAuthenticationSessionResponse)response).toString());
		} else {
			response = new WirexResponse();
			response.setStatusCode(-1);
			
			WirexError error = new WirexError();
			
			error.setTypeCode(WirexErrorCodes.INVALID_TOKEN);
			
			response.setError(error);
		}
		
		return response;
	}

}
