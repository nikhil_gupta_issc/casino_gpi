package com.gpi.service.wirex;

import java.math.BigInteger;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.domain.PlayerInfo;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.GPIErrorCodes;
import com.pragmaticplay.api.PragmaticPlayErrorCodes;
import com.pragmaticplay.api.domain.PragmaticPlayBetResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResponse;
import com.pragmaticplay.api.domain.PragmaticPlayResultResponse;

import it.wirex.api.WirexErrorCodes;
import it.wirex.api.WirexMethod;
import it.wirex.api.domain.WirexDepositWithdrawResponse;
import it.wirex.api.domain.WirexError;
import it.wirex.api.domain.WirexResponse;
import it.wirex.api.domain.WirexTransfer;
import it.wirex.api.domain.WirexWallet;

/**
 * Withdraw message handler for Wirex integration
 * 
 * @author Alexandre
 *
 */
public class WirexMessageDepositWithdraw extends WirexMessageCommand {
	
	protected static final Logger logger = LoggerFactory.getLogger(WirexMessageDepositWithdraw.class);

	@Override
	public WirexResponse execute(String sessionId, Map<String, String> params, WirexMethod method) throws GameRequestNotExistentException {
		String transactionId = params.get("transactionUid");
		Long roundId = Long.valueOf(params.get("gameRoundId"));
		
		if (roundId == null) {
			roundId = Long.valueOf(params.get("gameRoundId").hashCode());
		}
		
		double amount = Double.valueOf(params.get("amount"));
		
		PlayerInfo playerInfo = playerInfoDaoCache.findPlayerInfo(getPlayerInfoKey(sessionId));
		String playerInfoToken = playerInfo.getToken();
		
		long gpiAmount = gamePlatformIntegrationUtils.getAmountFromProvider(amount, BALANCE_COEFFICIENT);
		
		MethodType methodType;
		
		if (WirexMethod.DEPOSIT.equals(method)) {
			methodType = MethodType.WIN;
		} else {
			methodType = MethodType.BET;
		}
		
		Message messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(playerInfoToken, gpiAmount, transactionId, roundId, methodType);
		Message returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
		
		WirexResponse response;
		
		if ((returnMessage.getResult().getSuccess() == 1) || returnMessage.getResult().getSuccess() == 0 && returnMessage.getResult().getReturnset().getTransactionId() != null) {
			// success or transaction already processed
			
			String gpiToken = returnMessage.getResult().getReturnset().getToken().getValue();
			setPlayerInfoTokenAndStore(gpiToken, playerInfo);
			
			double balance;
			if (returnMessage.getResult().getReturnset().getBalance() != null) {
				balance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			} else {
				// there is no balance in the previous response. do a new call to get the updated balance
				messageFromGame = gamePlatformIntegrationUtils.constructGPIMessage(gpiToken, null, null, null, MethodType.GET_BALANCE);
				returnMessage = gamePlatformIntegrationService.processMessage(messageFromGame);
				balance = gamePlatformIntegrationUtils.getAmountFromGPI(returnMessage.getResult().getReturnset().getBalance().getValue(), BALANCE_COEFFICIENT);
			}
			
			response = new WirexDepositWithdrawResponse();
			
			WirexWallet wallet = new WirexWallet();
			wallet.setBalance(balance);
			wallet.setCurrency(playerInfo.getCurrency());
			
			WirexTransfer transfer = new WirexTransfer();
			transfer.setAmount(amount);
			transfer.setWallet(wallet);
			transfer.setCurrency(playerInfo.getCurrency());
			
			((WirexDepositWithdrawResponse) response).setResult(transfer);
			
			logger.info(method + " response: " + ((WirexDepositWithdrawResponse)response).toString());

		} else {
			response = new WirexResponse();
			response.setStatusCode(-1);
			WirexError error = new WirexError();
			if (GPIErrorCodes.NOT_ENOUGH_CREDITS == returnMessage.getResult().getReturnset().getErrorCode().getValue()) {
				error.setTypeCode(WirexErrorCodes.AMOUNT_NOT_AVAILABLE);
			} else {
				error.setTypeCode(WirexErrorCodes.INTERNAL_SERVER_ERROR);
			}
			logger.error(returnMessage.getResult().toString());
		}
		
		return response;
	}

}
