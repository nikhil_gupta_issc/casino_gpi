package com.gpi.dao.rct;

import com.gpi.domain.rct.RCTSession;

/**
 * Created by igabba on 20/01/15.
 */
public interface RCTDao {


    RCTSession findRCTSession(String sessionId);

    void saveRCTSession(RCTSession rctSession);
}
