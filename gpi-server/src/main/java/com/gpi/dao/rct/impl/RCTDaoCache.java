package com.gpi.dao.rct.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.rct.RCTDao;
import com.gpi.domain.rct.RCTSession;
import com.gpi.utils.cache.CacheManager;

/**
 * Created by igabba on 20/01/15.
 */
public class RCTDaoCache implements RCTDao{

    private CacheManager cacheManager;

    @Override
    public RCTSession findRCTSession(String sessionId) {
        return (RCTSession) cacheManager.get(getKey(sessionId));
    }

    @Override
    public void saveRCTSession(RCTSession rctSession) {
        cacheManager.store(getKey(rctSession.getToken()), rctSession, CacheConstants.GPI_LOAD_GAME_TTL);
    }

    private String getKey(String token) {
        return CacheConstants.GPI_LOAD_GAME_KEY + token;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
}
