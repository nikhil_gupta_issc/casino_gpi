package com.gpi.dao.playerinfo.impl;

import com.gpi.domain.PlayerInfo;
import com.gpi.constants.CacheConstants;
import com.gpi.dao.playerinfo.PlayerInfoDao;
import com.gpi.utils.cache.CacheManager;

public class PlayerInfoDaoCache implements PlayerInfoDao {
	
	private CacheManager cacheManager;

	@Override
	public PlayerInfo findPlayerInfo(String key) {
		return (PlayerInfo)this.cacheManager.getAndTouch(this.getKey(key), CacheConstants.PLAYER_INFO_CACHE_TTL);
	}

	@Override
	public void savePlayerInfo(String key, PlayerInfo playerInfo) {
		this.cacheManager.store(this.getKey(key), playerInfo, CacheConstants.PLAYER_INFO_CACHE_TTL);
	}
	
    private String getKey(String key) {
        return CacheConstants.PLAYER_INFO_CACHE_KEY + key;
    }
	
    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

}
