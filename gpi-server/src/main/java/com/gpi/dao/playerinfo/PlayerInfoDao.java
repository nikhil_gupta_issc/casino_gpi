package com.gpi.dao.playerinfo;

import com.gpi.domain.PlayerInfo;

/**
 * DAO for the PlayerInfo
 * 
 * @author Alexandre
 *
 */
public interface PlayerInfoDao {
	/**
	 * @param key key that identifies the player info
	 * @return previous cached PlayerInfo
	 */
	PlayerInfo findPlayerInfo(String key);
	
	/**
	 * Save PlayerInfo into cache using key
	 * @param key key to use to identify the saved object
	 * @param playerInfo the playerInfo to save
	 */
	void savePlayerInfo(String key, PlayerInfo playerInfo);
}
