package com.gpi.dao.gamerequest;

import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;

/**
 * Created by Nacho on 2/18/14.
 */
public interface GameRequestDao {

    void saveGameRequest(GameRequest gameRequest, int ttl);

    void updateGameRequest(GameRequest gameRequest, int ttl);

    GameRequest getGameRequest(String token) throws GameRequestNotExistentException;

    void removeGameRequest(GameRequest gameRequest);
    
    /**
     * Copy a game request from a token key to another one, deleting the old one
     * @param token original token of the game request
     * @param newToken new token of the game request
     * @param ttl the time to live of the game request
     * @throws GameRequestNotExistentException
     */
    void copyGameRequest(String token, String newToken, int ttl) throws GameRequestNotExistentException;
    
    /**
     * Refresh a game request TTL
     * @param token token of the game request
     * @param ttl the time to live of the game request
     * @return the game request with the specified token
     * @throws GameRequestNotExistentException
     */
    GameRequest refreshGameRequest(String token, int ttl) throws GameRequestNotExistentException;
}
