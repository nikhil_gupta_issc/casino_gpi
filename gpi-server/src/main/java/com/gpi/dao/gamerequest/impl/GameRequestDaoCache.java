package com.gpi.dao.gamerequest.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.gamerequest.GameRequestDao;
import com.gpi.domain.GameRequest;
import com.gpi.exceptions.GameRequestNotExistentException;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Ignacio on 2/19/14.
 */
public class GameRequestDaoCache implements GameRequestDao {

    private static final Logger logger = LoggerFactory.getLogger(GameRequestDaoCache.class);
    private CacheManager cacheManager;

    @Override
    public void saveGameRequest(GameRequest gameRequest, int ttl) {
        logger.debug("Saving gameRequest={}", gameRequest);
        cacheManager.store(getGameRequestKey(gameRequest.getToken()), gameRequest, ttl);
        logger.debug("Saving gameRequest={} successfully", gameRequest);
    }

    @Override
    public void updateGameRequest(GameRequest gameRequest, int ttl) {
        logger.debug("Updating gameRequest={}", gameRequest);
        boolean store = cacheManager.store(getGameRequestKey(gameRequest.getToken()), gameRequest, ttl);
        if (!store) {
            logger.error("GameRequest wasn't stored");
        }
    }

    @Override
    public GameRequest getGameRequest(String token) throws GameRequestNotExistentException {
        logger.debug("Getting GameRequest from cache for token={}", token);
        GameRequest gameRequest = (GameRequest) cacheManager.get(getGameRequestKey(token));
        if (gameRequest == null) {
            logger.debug("GameRequest with token=[{}] doesn't exists on cache.");
            throw new GameRequestNotExistentException("GameRequest does not exist with token=" + token);
        }

        return gameRequest;
    }

    @Override
    public void removeGameRequest(GameRequest gameRequest) {
        logger.debug("Removing GameRequest with token={} from cache.", gameRequest.getToken());
        boolean delete = cacheManager.delete(getGameRequestKey(gameRequest.getToken()));
        if (delete) {
            logger.debug("gameRequest removed successfuly");
        } else {
            logger.warn("Something went wrong while removing request with token={}", gameRequest.getToken());
        }
    }
    
	@Override
	public GameRequest refreshGameRequest(String token, int ttl) throws GameRequestNotExistentException {
		 logger.debug("Refreshing GameRequest from cache for token={}", token);
	        GameRequest cachedGameRequest = (GameRequest) cacheManager.getAndTouch(getGameRequestKey(token), ttl);
	        if (cachedGameRequest == null) {
	            logger.debug("GameRequest with token=[{}] doesn't exists on cache.");
	            throw new GameRequestNotExistentException("GameRequest does not exist with token=" + token);
	        }

	        return cachedGameRequest;
	}
	

	@Override
	public void copyGameRequest(String token, String newToken, int ttl) throws GameRequestNotExistentException {
		 logger.debug("Copying GameRequest to token={}", newToken);
	        GameRequest cachedGameRequest = (GameRequest) cacheManager.get(getGameRequestKey(token));
	        
	        if (cachedGameRequest == null) {
	            logger.debug("GameRequest with token=[{}] doesn't exists on cache.");
	            throw new GameRequestNotExistentException("GameRequest does not exist with token=" + token);
	        }
	        
	        cachedGameRequest.setToken(newToken);
	        
	        cacheManager.store(getGameRequestKey(newToken), cachedGameRequest, ttl);
	        cacheManager.delete(getGameRequestKey(token));
	}

    String getGameRequestKey(String token) {
        return CacheConstants.GPI_GAME_REQUEST_KEY + token;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

}
