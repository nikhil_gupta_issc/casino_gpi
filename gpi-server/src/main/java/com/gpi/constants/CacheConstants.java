package com.gpi.constants;

/**
 * Created by Ignacio on 2/19/14.
 */
public interface CacheConstants {


    public static final String GPI_GAME_REQUEST_KEY = "GPI_GPI-P-";
    public static final int GPI_GAME_REQUEST_TTL = 60 * 60 * 24;

    public static final String GPI_LOAD_GAME_KEY = "GPI_LG-";
    public static final int GPI_LOAD_GAME_TTL =  60 * 60 * 24;
    
    public static final String PLAYER_INFO_CACHE_KEY = "GPI_PLAYERINFO-";
    public static final int PLAYER_INFO_CACHE_TTL =  60 * 60 * 24;
    
    public static final String BETGAMES_KEY = "betgames-";
    public static final String BETGAMES_TOKEN_KEY = "betgames-T-";
    public static final int BETGAMES_TIME_EXPIRATION = 60 * 60 * 24; // seconds
  	public static final int BETGAMES_TOKEN_TTL = 60 * 60 * 24; // seconds

	
  	public static final String BOOMING_KEY = "booming-";
  	public static final String BOOMING_TOKEN_KEY = "booming-S-";
  	public static final String BOOMING_PLAYERINFO_KEY = "booming-P-";
  	public static final int BOOMING_TOKEN_TTL =  60 * 60 * 24; // seconds
  
  	public static final String HABANERO_TOKEN_KEY = "habanero-";
  	public static final int HABANERO_TOKEN_TTL =  60 * 60 * 24; // seconds
  
  	public static final String EVOPLAY_PLAYERINFO_KEY = "evoplay-P-";
  	public static final String PRAGMATICPLAY_PLAYERINFO_KEY = "pragmaticplay-P-";
  	public static final String WIREX_PLAYERINFO_KEY = "wirex-P-";
  	public static final String ORTIZ_PLAYERINFO_KEY = "ortiz-P-";
	public static final String SPINMATIC_PLAYERINFO_KEY = "spinmatic-P-";
	public static final String ONEXTWO_PLAYERINFO_KEY = "onextwo-P-";

	
	public static final String HELIOGAMING_PLAYERINFO_KEY = "heliogaming-P-";
	public static final String HELIOGAMING_SESSION_KEY = "heliogaming-S-";
	public static final String HELIOGAMING_COUPON_KEY = "heliogaming-C-";
	public static final int HELIOGAMING_TOKEN_TTL_KEY = 60 * 60 * 24 * 30; // 30 Days
	
    public static final String AMELCO_TOKEN_KEY = "GPI-AMELCO-T-";
    public static final int AMELCO_TTL = 60 * 60;
    public static final String AMELCO_TRANSACTION_KEY = "GPI-AMELCO-TX-";


    public static final String WAZDAN_TOKEN_KEY = "GPI-WAZDAN-T-";
    public static final int WAZDAN_TOKEN_TTL = 60 * 60;
    
  	public static final String EVENBET_TOKEN_KEY = "GPI-EVENBET-T-";
  	public static final int EVENBET_TOKEN_TTL =  60 * 60;

}
