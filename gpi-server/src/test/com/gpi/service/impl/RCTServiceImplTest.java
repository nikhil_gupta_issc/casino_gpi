package com.gpi.service.impl;

import com.gpi.api.Message;
import com.gpi.api.MethodType;
import com.gpi.api.TransactionId;
import com.gpi.communication.CommunicationHandlerSelector;
import com.gpi.dao.rct.RCTDao;
import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.PlayingType;
import com.gpi.domain.publisher.Publisher;
import com.gpi.domain.rct.*;
import com.gpi.domain.rct.utils.RCTMessageHelper;
import com.gpi.domain.transaction.TransactionType;
import com.gpi.dto.RCTLoadGameDto;
import com.gpi.exceptions.*;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.game.GameProviderService;
import com.gpi.service.gpi.impl.RCTServiceImpl;
import com.gpi.service.round.RoundService;
import com.gpi.service.transaction.TransactionService;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.MessageHelper;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Log4jConfigurer;

import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring/applicationContext.xml"
})
@Transactional
public class RCTServiceImplTest {

    @Autowired
    GameDomainService gameDomainService;
    String allwayspayingPass = null;
    Message message = null;
    Message betMessage = null;
    Message messagePayWin = null;
    Long betAmount = 1000l;
    Long winAmount = 4500l;

    static {
        try {
            Log4jConfigurer.initLogging("classpath:log4j-testing.xml");
        } catch (FileNotFoundException ex) {
            System.err.println("Cannot Initialize log4j");
        }
    }

    @Autowired
    private RCTServiceImpl rctService;
    @Autowired
    private GameProviderService gameProviderService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private RCTDao rctDao;
    @Autowired
    private RoundService roundService;
    private CommunicationHandlerSelector communicationHandlerSelector;

    @Before
    public void setUp() throws Exception {
        communicationHandlerSelector = mock(CommunicationHandlerSelector.class);
        rctService.setCommunicationHandlerSelector(communicationHandlerSelector);
        gameProviderService.createGameProvider("rctTest", "username", "password", "rct", providerRequest.isHasExtendedInfo());
        gameDomainService.addGame("testGame", "testUrl", "rctTest");
        allwayspayingPass = ApplicationProperties.getProperty("rct.allwaysplayingPass");

        message = MessageHelper.createResponse(MethodType.GET_PLAYER_INFO, "newToken");
        MessageHelper.addLoginName(message, "tito3");
        MessageHelper.addCurrency(message, "USD");
        MessageHelper.addBalance(message, (long) 10000);

        Long betAmount = 1000l;
        betMessage = MessageHelper.createResponse(MethodType.BET, "newToken");
        MessageHelper.addBalance(betMessage, (long) 10000 - betAmount);
        MessageHelper.setAlreadyProcess(betMessage, false);
        TransactionId transactionIdPayBet = new TransactionId();
        transactionIdPayBet.setValue(new Date().getTime());
        betMessage.getResult().getReturnset().setTransactionId(transactionIdPayBet);

        messagePayWin = MessageHelper.createResponse(MethodType.WIN, "newToken");
        MessageHelper.addLoginName(messagePayWin, "tito3");
        MessageHelper.addCurrency(messagePayWin, "USD");

        MessageHelper.setAlreadyProcess(messagePayWin, false);
        TransactionId transactionIdPayWin = new TransactionId();
        transactionIdPayWin.setValue(new Date().getTime());
        messagePayWin.getResult().getReturnset().setTransactionId(transactionIdPayWin);

    }

    @Test
    public void testLoadGame() throws Exception {
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(message);

        RCTLoadGameDto loadGameDto = (RCTLoadGameDto) rctService.loadGame("publisherToken", "test", "testGame", PlayingType.CHARGED.getType(), false, device);
        assertEquals("tito3", loadGameDto.getLoginName());
        assertNotNull("testUrl", loadGameDto.getGameUrl());
        assertNotNull("newToken", loadGameDto.getToken());
        assertEquals(getKey("password", loadGameDto.getToken()), loadGameDto.getKey());
        System.out.println(loadGameDto.getKey());
    }

    @Test
    public void testconfirmOpenSession() throws Exception {
        String token = "newToken";
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(message);
        RCTLoadGameDto loadGameDto = (RCTLoadGameDto) rctService.loadGame("publisherToken", "test", "testGame", PlayingType.CHARGED.getType(), false, device);
        OpenSession s = rctService.confirmOpenSession(loadGameDto.getToken(), 45l, getKey(allwayspayingPass, loadGameDto.getToken()));
        String stringFromMessage = RCTMessageHelper.createStringFromObject(s);
        System.out.println(stringFromMessage);
        assertEquals("tito3", s.getUser());
        assertEquals(getKey("password", s.getToken()), s.getKey());
        assertEquals("true", s.isProcessed());
        assertEquals("", s.getError());
        assertEquals("10000", s.getBalance());
        assertEquals("USD", s.getCurrency());
    }

    @Test
    public void testconfirmOpenSessionWrongKey() throws Exception {
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(message);
        RCTLoadGameDto loadGameDto = (RCTLoadGameDto) rctService.loadGame("publisherToken", "test", "testGame", PlayingType.CHARGED.getType(), false, device);
        OpenSession s = rctService.confirmOpenSession(loadGameDto.getToken(), 45l, "wrongKey");
        String stringFromMessage = RCTMessageHelper.createStringFromObject(s);
        System.out.println(stringFromMessage);
        assertEquals("1", s.getError());
    }

    @Test
    public void testGetBalance() throws Exception {
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(message);

        RCTLoadGameDto loadGameDto = (RCTLoadGameDto) rctService.loadGame("publisherToken", "test", "testGame", PlayingType.CHARGED.getType(), false, device);
        GetBalance balance = rctService.getBalance(loadGameDto.getToken());
        String stringFromMessage = RCTMessageHelper.createStringFromObject(balance);
        System.out.println(stringFromMessage);
        assertEquals("password", balance.getKey());
        assertEquals("true", balance.getProcessed());
        assertEquals("", balance.getError());
        assertEquals(new Long(10000), balance.getCurrentBalance());
        assertNotNull(balance.getTimestamp());
        assertEquals(1, balance.getTransactionId());

        GetBalance balance2 = rctService.getBalance(loadGameDto.getToken());
        String stringFromMessage2 = RCTMessageHelper.createStringFromObject(balance2);
        System.out.println(stringFromMessage2);
        assertEquals("password", balance2.getKey());
        assertEquals("true", balance2.getProcessed());
        assertEquals("", balance2.getError());
        assertEquals(new Long(10000), balance2.getCurrentBalance());
        assertTrue(balance.getTimestamp() < balance2.getTimestamp());
        assertEquals(2, balance2.getTransactionId());

        GetBalance balance3 = rctService.getBalance(loadGameDto.getToken());
        String stringFromMessage3 = RCTMessageHelper.createStringFromObject(balance3);
        System.out.println(stringFromMessage3);
        assertEquals("password", balance3.getKey());
        assertEquals("true", balance3.getProcessed());
        assertEquals("", balance3.getError());
        assertEquals(new Long(10000), balance3.getCurrentBalance());
        assertTrue(balance2.getTimestamp() < balance3.getTimestamp());
        assertEquals(3, balance3.getTransactionId());
    }

    @Test
    public void testPayBet() throws CommunicationException, NonExistentGameException, MarshalException, GameUrlNotDefinedException, NonExistentPublisherException, NoSuchAlgorithmException {
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(message);

        RCTLoadGameDto loadGameDto = (RCTLoadGameDto) rctService.loadGame("publisherToken", "test", "testGame", PlayingType.CHARGED.getType(), false, device);
        GetBalance balance = rctService.getBalance(loadGameDto.getToken());
        String stringFromMessage = RCTMessageHelper.createStringFromObject(balance);
        System.out.println(stringFromMessage);
        assertEquals("password", balance.getKey());
        assertEquals("true", balance.getProcessed());
        assertEquals("", balance.getError());
        assertEquals(new Long(10000), balance.getCurrentBalance());
        assertNotNull(balance.getTimestamp());
        assertEquals(1, balance.getTransactionId());

        //Here starts payBet method

        TransactionId transactionId = new TransactionId();
        transactionId.setValue(new Date().getTime());
        betMessage.getResult().getReturnset().setTransactionId(transactionId);
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(betMessage);

        String key = getKey(allwayspayingPass, message.getResult().getReturnset().getLoginName().getValue(), betAmount);
        PayBet payBet = rctService.payBet(loadGameDto.getToken(), betAmount, (long) 1, key, message.getResult().getReturnset().getLoginName().getValue());
        String payBetString = RCTMessageHelper.createStringFromObject(payBet);
        System.out.println(payBetString);
        assertEquals(getKey("password",message.getResult().getReturnset().getLoginName().getValue(), betAmount), payBet.getKey());
        assertEquals("true", payBet.getProcessed());
        assertEquals("", payBet.getError());
        assertEquals(new Long(10000 - betAmount), payBet.getNewBalance());
        assertNotNull(payBet.getTimestamp());

        RCTSession rctSession = rctDao.findRCTSession(loadGameDto.getToken());
        Round round = roundService.getRound(rctSession.getGame(), rctSession.getGame().getGameProvider(), rctSession.getRoundId());
        Transaction transaction = transactionService.getTransaction((long) 1, round);
        assertNotNull(transaction);
        assertEquals(payBet.getTransactionId(), transaction.getId().longValue());

    }

    @Test
    public void testPayBetWrongKey() throws CommunicationException, NonExistentGameException, MarshalException, GameUrlNotDefinedException, NonExistentPublisherException, NoSuchAlgorithmException {
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(message);

        RCTLoadGameDto loadGameDto = (RCTLoadGameDto) rctService.loadGame("publisherToken", "test", "testGame", PlayingType.CHARGED.getType(), false, device);
        GetBalance balance = rctService.getBalance(loadGameDto.getToken());
        String stringFromMessage = RCTMessageHelper.createStringFromObject(balance);

        //Here starts payBet method
        Long amount = 1000l;
        betMessage = MessageHelper.createResponse(MethodType.BET, "newToken");
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(betMessage);

        PayBet payBet = rctService.payBet(loadGameDto.getToken(), amount, (long) 1, "wrongKey", message.getResult().getReturnset().getLoginName().getValue());
        String payBetString = RCTMessageHelper.createStringFromObject(payBet);
        System.out.println(payBetString);
        assertEquals("1", payBet.getError());
    }

    @Test
    public void testPayWin() throws CommunicationException, NonExistentPublisherException, NonExistentGameException, GameUrlNotDefinedException, MarshalException, NoSuchAlgorithmException {
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(message);

        RCTLoadGameDto loadGameDto = (RCTLoadGameDto) rctService.loadGame("publisherToken", "test", "testGame", PlayingType.CHARGED.getType(), false, device);
        GetBalance balance = rctService.getBalance(loadGameDto.getToken());
        String stringFromMessage = RCTMessageHelper.createStringFromObject(balance);

        //Here starts payBet method
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(betMessage);

        //First we make a payBet
        String key = getKey(allwayspayingPass, message.getResult().getReturnset().getLoginName().getValue(), betAmount);
        PayBet payBet = rctService.payBet(loadGameDto.getToken(), betAmount, (long) 1, key, message.getResult().getReturnset().getLoginName().getValue());

        MessageHelper.addBalance(messagePayWin, payBet.getNewBalance() + winAmount);
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(messagePayWin);


        //Then we test the payWin
        String key1 = getKey(allwayspayingPass, message.getResult().getReturnset().getLoginName().getValue(), winAmount);
        PayWin payWin = rctService.payWin(loadGameDto.getToken(), winAmount, (long) 2, key1, message.getResult().getReturnset().getLoginName().getValue());
        String payBetString = RCTMessageHelper.createStringFromObject(payWin);
        System.out.println(payBetString);
        assertEquals(getKey("password", message.getResult().getReturnset().getLoginName().getValue(), winAmount), payWin.getKey());
        assertEquals("true", payWin.getProcessed());
        assertEquals("", payWin.getError());
        assertEquals(new Long(payBet.getNewBalance() + winAmount), payWin.getNewBalance());
        assertNotNull(payWin.getTimestamp());

        RCTSession rctSession = rctDao.findRCTSession(loadGameDto.getToken());
        Round round = roundService.getRound(rctSession.getGame(), rctSession.getGame().getGameProvider(), rctSession.getRoundId());
        Transaction transaction = transactionService.getTransaction((long) 2, round);
        assertNotNull(transaction);
        assertEquals(TransactionType.WIN, transaction.getType());
        assertEquals(payWin.getTransactionId(), transaction.getId().longValue());
    }

    @Test
    public void testPayWinWrongKey() throws CommunicationException, NonExistentPublisherException, NonExistentGameException, GameUrlNotDefinedException, MarshalException, NoSuchAlgorithmException {
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(message);

        RCTLoadGameDto loadGameDto = (RCTLoadGameDto) rctService.loadGame("publisherToken", "test", "testGame", PlayingType.CHARGED.getType(), false, device);
        GetBalance balance = rctService.getBalance(loadGameDto.getToken());
        String stringFromMessage = RCTMessageHelper.createStringFromObject(balance);

        //Here starts payBet method
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(betMessage);

        //First we make a payBet
        String key = getKey(allwayspayingPass, message.getResult().getReturnset().getLoginName().getValue(), betAmount);
        PayBet payBet = rctService.payBet(loadGameDto.getToken(), betAmount, (long) 1, key, message.getResult().getReturnset().getLoginName().getValue());

        MessageHelper.addBalance(messagePayWin, payBet.getNewBalance() + winAmount);
        when(communicationHandlerSelector.communicate(any(Publisher.class), any(Message.class))).thenReturn(messagePayWin);


        //Then we test the payWin
        PayWin payWin = rctService.payWin(loadGameDto.getToken(), winAmount, (long) 2, "wrongKey", message.getResult().getReturnset().getLoginName().getValue());
        String payBetString = RCTMessageHelper.createStringFromObject(payWin);
        System.out.println(payBetString);
        assertEquals("1", payWin.getError());
    }

    private String getKey(Object... args) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");

        String key = StringUtils.join(args, '.');
        byte[] digest = messageDigest.digest(key.getBytes());
        return new String(Hex.encodeHex(digest));
    }

}