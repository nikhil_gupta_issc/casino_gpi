package com.gpi.dao.audit.impl;

import com.gpi.dao.audit.AccumulatedStatsDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.currency.Currency;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.domain.publisher.PublisherStatsDaily;
import com.gpi.domain.publisher.PublisherStatsMonthly;
import com.gpi.domain.publisher.PublisherStatsSummary;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nacho on 1/20/14.
 */
public class AccumulatedStatsDaoHibernate extends ExtendedHibernateDaoSupport implements AccumulatedStatsDao {

    private static final Logger logger = LoggerFactory.getLogger(AccumulatedStatsDaoHibernate.class);

    @Override
    public PublisherStatsDaily updatePublisherStatsDaily(PublisherStatsDaily p) {
        logger.debug("updating PublisherStatsDaily={}", p);
        getHibernateSession().merge(p);
        return p;
    }

    @Override
    public PublisherStatsDaily findDailyStat(Player player, Publisher publisher, Game game, Currency currency, Date date) {
        logger.debug("Looking for stat into database...");
        Session session = getHibernateSession();
        PublisherStatsDaily dailyStats = (PublisherStatsDaily) session
                .createQuery("from PublisherStatsDaily where playerId = :playerId and publisherId = :publisherId and gameId = :gameId and "
                        + "currency = :currency and createdOn =:created_on")
                .setLong("playerId", player.getId())
                .setInteger("publisherId", publisher.getId())
                .setLong("gameId", game.getId())
                .setString("currency", currency.getDescription())
                .setDate("created_on", date)
                .uniqueResult();

		/*
		 * Updates are done manually, by evicting the instance we tell
		 * hibernate to avoid saving changes made to the object
		 */
        session.evict(dailyStats);
        return dailyStats;
    }

    @Override
    public PublisherStatsMonthly updatePublisherStatsMonthly(PublisherStatsMonthly p) {
        logger.debug("Updating PublisherStatsMonthly={}", p);
        getHibernateSession().merge(p);
        return p;
    }

    @Override
    public PublisherStatsMonthly findMonthlyStat(Player player, Publisher publisher, Game game, Currency currency, Date date) {
        logger.debug("Looking for stat into database...");
        Session session = getHibernateSession();
        PublisherStatsMonthly monthlyStats = (PublisherStatsMonthly) session
                .createQuery("from PublisherStatsMonthly where playerId = :playerId and publisherId = :publisherId and gameId = :gameId and "
                        + "currency = :currency and createdOn = :created_on")
                .setLong("playerId", player.getId())
                .setInteger("publisherId", publisher.getId())
                .setLong("gameId", game.getId())
                .setString("currency", currency.getDescription())
                .setDate("created_on", date)
                .uniqueResult();
		
		/*
		 * Updates are done manually, by evicting the instance we tell
		 * hibernate to avoid saving changes made to the object
		 */
        session.evict(monthlyStats);
        return monthlyStats;
    }

    @Override
    public List<PublisherStatsSummary> generateDailySummary(Publisher publisher, Date day) {
        Query query = getHibernateSession().createQuery(
                "select playerId, sum(betAmount), sum(winAmount) from PublisherStatsDaily where publisherId = :publisherId "
                        + "and createdOn = :day group by playerId")
                .setInteger("publisherId", publisher.getId())
                .setDate("day", day);

        List<?> rows = query.list();
        List<PublisherStatsSummary> summaryList = new ArrayList<>();

        for (Object object : rows) {
            Object[] row = (Object[]) object;
            PublisherStatsSummary summary = new PublisherStatsSummary();
            summary.setPlayerId((Long) row[0]);
            summary.setBetAmount((Long) row[1]);
            summary.setWinAmount((Long) row[2]);
            summaryList.add(summary);
        }

        return summaryList;
    }

    @Override
    public List<PublisherStatsSummary> generateDailySummary(Publisher publisher, Date dayFrom, Date dayTo) {
        Query query = getHibernateSession().createQuery(
                "select gameId, sum(betAmount), sum(winAmount), createdOn, currency from PublisherStatsDaily where publisherId = :publisherId "
                        + "and createdOn >= :dayFrom and createdOn <= :dayTo group by gameId, createdOn, currency")
                .setInteger("publisherId", publisher.getId())
                .setDate("dayFrom", dayFrom)
                .setDate("dayTo", dayTo);
        logger.debug("Query: {}", query.getQueryString());
        List<?> rows = query.list();
        List<PublisherStatsSummary> summaryList = new ArrayList<>();
        logger.debug("Number of rows: {}", rows.size());
        for (Object object : rows) {
            Object[] row = (Object[]) object;

            PublisherStatsSummary summary = new PublisherStatsSummary();

            summary.setGameId((Long) row[0]);
            summary.setBetAmount((Long) row[1]);
            summary.setWinAmount((Long) row[2]);
            summary.setCreatedOn((Date) row[3]);
            summary.setCurrency((Currency) row[4]);
            summaryList.add(summary);
        }

        return summaryList;
    }

    @Override
    public List<PublisherStatsSummary> generateDailySummary(Publisher publisher, Game game, Date dayFrom, Date dayTo) {
        Query query = getHibernateSession().createQuery(
                "select gameId, sum(betAmount), sum(winAmount) from PublisherStatsDaily where publisherId = :publisherId "
                        + "and gameId = :gameId and createdOn >= :dayFrom and createdOn <= :dayTo group by gameId")
                .setInteger("publisherId", publisher.getId())
                .setLong("gameId", game.getId())
                .setDate("dayFrom", dayFrom)
                .setDate("dayTo", dayTo);

        List<?> rows = query.list();
        List<PublisherStatsSummary> summaryList = new ArrayList<>();

        for (Object object : rows) {
            Object[] row = (Object[]) object;
            PublisherStatsSummary summary = new PublisherStatsSummary();
            summary.setGameId((Long) row[0]);
            summary.setBetAmount((Long) row[1]);
            summary.setWinAmount((Long) row[2]);
            summaryList.add(summary);
        }

        return summaryList;
    }

    @Override
    public PublisherStatsMonthly saveMonthlyStats(PublisherStatsMonthly monthlyStat) {
        logger.debug("Saving monthlyStat into database");
        getHibernateSession().save(monthlyStat);
        return monthlyStat;
    }

    @Override
    public PublisherStatsDaily saveDailyStats(PublisherStatsDaily dailyStat) {
        logger.debug("Saving dailyStat into database");
        getHibernateSession().save(dailyStat);
        return dailyStat;
    }
}
