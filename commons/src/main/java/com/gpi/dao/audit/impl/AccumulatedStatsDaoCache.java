package com.gpi.dao.audit.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.audit.AccumulatedStatsDao;
import com.gpi.domain.currency.Currency;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.domain.publisher.PublisherStatsDaily;
import com.gpi.domain.publisher.PublisherStatsMonthly;
import com.gpi.domain.publisher.PublisherStatsSummary;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Nacho on 1/20/14.
 */
public class AccumulatedStatsDaoCache implements AccumulatedStatsDao {

    private static final Logger logger = LoggerFactory.getLogger(AccumulatedStatsDaoCache.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private AccumulatedStatsDao accumulatedStatsDao;

    private CacheManager cacheManager;

    @Override
    public PublisherStatsDaily updatePublisherStatsDaily(PublisherStatsDaily dailyStats) {
        logger.debug("Saving daily stats in cache. Stats = {}", dailyStats);
        dailyStats = accumulatedStatsDao.updatePublisherStatsDaily(dailyStats);
        cacheManager.store(getDailyStatsKey(dailyStats), dailyStats, CacheConstants.DAILY_STATS_TTL);
        return dailyStats;
    }

    @Override
    public PublisherStatsDaily findDailyStat(Player player, Publisher publisher, Game game, Currency currency, Date date) {
        logger.debug("Looking for daily stats into cache with player={}, publisher={}, game={}, currency={} and date={}", player.getId(), publisher.getName(),
                game.getName(), currency.toString(), date);
        PublisherStatsDaily daily = (PublisherStatsDaily) cacheManager.get(getDailyStatsKey(player.getId(), publisher.getId(), game.getId(), currency, date));
        if (daily == null) {
            logger.debug("Daily stat not found in cache.");
            daily = accumulatedStatsDao.findDailyStat(player, publisher, game, currency, date);
        }
        return daily;
    }

    @Override
    public PublisherStatsMonthly updatePublisherStatsMonthly(PublisherStatsMonthly monthlyStats) {
        monthlyStats = accumulatedStatsDao.updatePublisherStatsMonthly(monthlyStats);
        logger.debug("Saving monthly stats in cache. Stats = {}", monthlyStats);
        cacheManager.store(getMonthlyStatsKey(monthlyStats), monthlyStats, CacheConstants.MONTHLY_STATS_TTL);
        return monthlyStats;
    }

    @Override
    public PublisherStatsMonthly findMonthlyStat(Player player, Publisher publisher, Game game, Currency currency, Date date) {
        logger.debug("Looking for monthly stats into cache with player={}, publisher={}, game={}, currency={} and date={}", player.getId(),
                publisher.getName(), game.getName(), currency.toString(), date);
        PublisherStatsMonthly monthly = (PublisherStatsMonthly) cacheManager.get(getMonthlyStatsKey(player.getId(), publisher.getId(), game.getId(), currency,
                date));
        if (monthly == null) {
            logger.debug("Monthly stat not found in cache.");
            monthly = accumulatedStatsDao.findMonthlyStat(player, publisher, game, currency, date);
        }
        return monthly;
    }

    @Override
    public List<PublisherStatsSummary> generateDailySummary(Publisher publisher, Date day) {
        return accumulatedStatsDao.generateDailySummary(publisher, day);
    }

    private String getDailyStatsKey(PublisherStatsDaily dailyStats) {
        return getDailyStatsKey(dailyStats.getPlayerId(), dailyStats.getPublisherId(), dailyStats.getGameId(), dailyStats.getCurrency(),
                dailyStats.getCreatedOn());
    }

    private String getDailyStatsKey(Long playerId, Integer publisherId, Long gameId, Currency currency, Date date) {
        return String.format("%s-%s-%s-%s-%s-%s", CacheConstants.DAILY_STATS_KEY, playerId, publisherId, gameId, currency.ordinal(), formatDate(date));
    }

    private String getMonthlyStatsKey(PublisherStatsMonthly monthlyStats) {
        return getMonthlyStatsKey(monthlyStats.getPlayerId(), monthlyStats.getPublisherId(), monthlyStats.getGameId(), monthlyStats.getCurrency(),
                monthlyStats.getCreatedOn());
    }

    private String getMonthlyStatsKey(Long playerId, Integer publisherId, Long gameId, Currency currency, Date date) {
        return String.format("%s-%s-%s-%s-%s-%s", CacheConstants.MONTHLY_STATS_KEY, playerId, publisherId, gameId, currency.ordinal(), formatDate(date));
    }

    @Override
    public List<PublisherStatsSummary> generateDailySummary(Publisher publisher, Date dayFrom, Date dayTo) {
        logger.debug("There is no cache implementation for this method. Calling hibernate dao.");
        return accumulatedStatsDao.generateDailySummary(publisher, dayFrom, dayTo);
    }

    @Override
    public List<PublisherStatsSummary> generateDailySummary(Publisher p, Game game, Date from, Date to) {
        logger.debug("There is no cache implementation for this method. Calling hibernate dao.");
        return accumulatedStatsDao.generateDailySummary(p, game, from, to);
    }

    @Override
    public PublisherStatsMonthly saveMonthlyStats(PublisherStatsMonthly monthlyStat) {
        logger.debug("Saving monthlyStat={}", monthlyStat);
        monthlyStat = accumulatedStatsDao.saveMonthlyStats(monthlyStat);
        cacheManager.store(getMonthlyStatsKey(monthlyStat), monthlyStat, CacheConstants.MONTHLY_STATS_TTL);
        return monthlyStat;
    }

    @Override
    public PublisherStatsDaily saveDailyStats(PublisherStatsDaily dailyStat) {
        logger.debug("Saving dailyStat={}", dailyStat);
        dailyStat = accumulatedStatsDao.saveDailyStats(dailyStat);
        cacheManager.store(getDailyStatsKey(dailyStat), dailyStat, CacheConstants.DAILY_STATS_TTL);
        return dailyStat;
    }

    /**
     * Retrieves a date in a common string format so the key generated are
     * always the same. Date.toString() returns a different format if the
     * information contained is different
     */
    private String formatDate(Date date) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
        return df.format(date);
    }

    public void setAccumulatedStatsDao(AccumulatedStatsDao accumulatedStatsDao) {
        this.accumulatedStatsDao = accumulatedStatsDao;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
}
