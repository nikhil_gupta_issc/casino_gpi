package com.gpi.dao.audit;

import com.gpi.domain.currency.Currency;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.domain.publisher.PublisherStatsDaily;
import com.gpi.domain.publisher.PublisherStatsMonthly;
import com.gpi.domain.publisher.PublisherStatsSummary;

import java.util.Date;
import java.util.List;

/**
 * Created by Nacho on 1/20/14.
 */
public interface AccumulatedStatsDao {

    PublisherStatsDaily updatePublisherStatsDaily(PublisherStatsDaily publisherStatsDaily);

    PublisherStatsDaily findDailyStat(Player player, Publisher publisher, Game game, Currency currency, Date date);

    PublisherStatsMonthly updatePublisherStatsMonthly(PublisherStatsMonthly publisherStatsMonthly);

    PublisherStatsMonthly findMonthlyStat(Player player, Publisher publisher, Game game, Currency currency, Date date);

    /**
     * Generates the wager report for a specific {@link Publisher} and day
     */
    List<PublisherStatsSummary> generateDailySummary(Publisher publisher, Date day);

    List<PublisherStatsSummary> generateDailySummary(Publisher publisher, Date dayFrom, Date dayTo);

    List<PublisherStatsSummary> generateDailySummary(Publisher p, Game game, Date from, Date to);

    PublisherStatsMonthly saveMonthlyStats(PublisherStatsMonthly monthlyStat);

    PublisherStatsDaily saveDailyStats(PublisherStatsDaily dailyStat);
}
