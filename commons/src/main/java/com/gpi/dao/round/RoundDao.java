package com.gpi.dao.round;

import com.gpi.domain.audit.Round;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;

/**
 * Created by Nacho on 11/27/2014.
 */
public interface RoundDao {


    public Round getRound(Game game, Long providerRoundId);

    Round saveRound(Round round);

    void updateRound(Round round);

    Round findRoundById(Long roundId);
    
    Round findLastRound(Game game, Player player);
}
