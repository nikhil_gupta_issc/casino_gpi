package com.gpi.dao.round.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.round.RoundDao;
import com.gpi.domain.audit.Round;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Nacho on 11/27/2014.
 */
public class RoundDaoCache implements RoundDao {

    private static final Logger logger = LoggerFactory.getLogger(RoundDaoCache.class);
    private CacheManager cacheManager;
    private RoundDao roundDao;

    @Override
    public Round getRound(Game game, Long providerRoundId) {
        logger.debug("Looking for a Round with game={} and providerRoundId={}", game.getName(), providerRoundId);
        Round round = (Round) cacheManager.get(getKey(game, providerRoundId));
        if (round == null) {
            round = roundDao.getRound(game, providerRoundId);
            if (round != null) {
                cacheManager.store(getKey(game, providerRoundId), round, CacheConstants.ROUND_TTL);
            }
        }
        return round;
    }

    @Override
    public Round saveRound(Round round) {
        round = roundDao.saveRound(round);
        cacheManager.store(getKey(round.getGame(), round.getGameRoundId()), round, CacheConstants.ROUND_TTL);
        return round;
    }

    @Override
    public void updateRound(Round round) {
        roundDao.updateRound(round);
        logger.debug("Updating round into cache.");
        cacheManager.store(getKey(round.getGame(), round.getGameRoundId()), round, CacheConstants.ROUND_TTL);
        logger.debug("Round Updated successfully.");
    }

    @Override
    public Round findRoundById(Long roundId) {
        return roundDao.findRoundById(roundId);
    }
    
    @Override
	public Round findLastRound(Game game, Player player) {
		return roundDao.findLastRound(game, player);
	}

    private String getKey(Game game, Long providerRoundId) {
        return String.format("%s-%s-%s", CacheConstants.ROUND_KEY, game.getId(), providerRoundId);
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public void setRoundDao(RoundDao roundDao) {
        this.roundDao = roundDao;
    }

	
}
