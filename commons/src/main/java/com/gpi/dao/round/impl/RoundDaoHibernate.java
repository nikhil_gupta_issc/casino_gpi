package com.gpi.dao.round.impl;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.dao.round.RoundDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.audit.Round;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;

public class RoundDaoHibernate extends ExtendedHibernateDaoSupport implements RoundDao {


    private static final Logger logger = LoggerFactory.getLogger(RoundDaoHibernate.class);
    @Override
    public Round getRound(Game game, Long providerRoundId) {
        logger.debug("Looking into database for a round with game={} and providerRoundId={}", game.getName(), providerRoundId);
        Long roundId = getRoundIdFromGameRoundId(providerRoundId, game);
        if (roundId!= null) {
        	return findRoundById(roundId);
        } else {
        	return null;
        }
        
    }

    @Override
    public Round saveRound(Round round) {
        logger.debug("Saving Round={} into database", round);
        getHibernateSession().save(round);
        
        getHibernateSession().createSQLQuery("insert into round_game_round (round_id, game_round_id, game) values (:roundId, :gameRoundId, :game)")
        .setLong("roundId", round.getId())
        .setLong("gameRoundId", round.getGameRoundId())
        .setString("game", round.getGame().getName())
        .executeUpdate();
        
        logger.debug("Round saved successfully. Round id={}", round.getId());
        return round;
    }

    @Override
    public void updateRound(Round round) {
        logger.debug("Updating round={} into database", round);
        getHibernateSession().merge(round);
    }

    @Override
    public Round findRoundById(Long roundId) {
        logger.info("finding Round with id = {}", roundId);
        return (Round) getHibernateSession().createQuery("from Round where id=:id").setLong("id",roundId).uniqueResult();
    }

	@Override
	public Round findLastRound(Game game, Player player) {
		return (Round) getHibernateSession().createQuery("from Round where game.id=:gameId and player.id=:playerId order by createdOn DESC")
                .setLong("gameId", game.getId()).setLong("playerId", player.getId()).setMaxResults(1).uniqueResult();
	}
	
	// TODO temporary fix while there is no idex. remove when its created
    private Long getRoundIdFromGameRoundId(long gameRoundId, Game game) {
    	BigInteger roundId = (BigInteger)getHibernateSession().createSQLQuery("select round_id from round_game_round where game_round_id = :gameRoundId and game = :gameName").setLong("gameRoundId", gameRoundId).setString("gameName", game.getName()).setMaxResults(1).uniqueResult();
    	
    	logger.debug("round id from game round " + gameRoundId + " is " + roundId);
    	return roundId != null ? roundId.longValue() : null;
    }
}
