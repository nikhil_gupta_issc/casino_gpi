package com.gpi.dao.utils;

import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


/**
 * All DAO's of the GPI system should extend from this class. Its purpose is to
 * wrap the implementation of the DaoSupport.
 */
public abstract class ExtendedHibernateDaoSupport extends HibernateDaoSupport {
    protected Session getHibernateSession() {
        return getSessionFactory().getCurrentSession();
    }
}
