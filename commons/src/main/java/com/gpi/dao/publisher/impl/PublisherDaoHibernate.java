package com.gpi.dao.publisher.impl;

import com.gpi.dao.publisher.PublisherDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.NonExistentPublisherException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class PublisherDaoHibernate extends ExtendedHibernateDaoSupport implements PublisherDao {

    private static final Logger log = LoggerFactory.getLogger(PublisherDaoHibernate.class);

    @Override
    public Publisher findPublisherByNameAndPassword(String name, String password) {
        log.debug("Looking in the database for a Publisher with name = [{}] and password = [{}]"
                , name, password);
        List<?> publishers = getHibernateSession().createQuery("from Publisher where name=:name and password = :password")
                .setString("name", name).setString("password", password).list();
        if (publishers != null && publishers.size() > 0) {
            return (Publisher) publishers.get(0);
        } else {
            throw new RuntimeException(String.format("Publisher [%s] does not exists or the credentials are wrong.", name));
        }
    }

    @Override
    public Publisher findByName(String name) throws NonExistentPublisherException {
        log.debug("Looking in the database for a Publisher with name = [{}].", name);
        Publisher publisher = (Publisher) getHibernateSession().createQuery("from Publisher where name=:name")
                .setString("name", name).uniqueResult();
        if (publisher == null) {
            throw new NonExistentPublisherException(String.format("Publisher [%s] does not exists.", name));
        } else {
            return publisher;
        }
    }

    @Override
    public Publisher createPublisher(Publisher publisher) {
        log.debug("Adding new Publisher [{}] in the database.", publisher);
        Integer publisherId = (Integer) getHibernateSession().save(publisher);
        publisher.setId(publisherId);
        return publisher;
    }

    @Override
    public void updatePublisher(Publisher publisher) {
        log.debug("Updating Publisher [{}] in the database.", publisher);
        getHibernateSession().update(publisher);
    }

    @Override
    public List<Publisher> getPublisherList() {
        log.info("Getting all publishers from database");
        return getHibernateSession().createCriteria(Publisher.class).list();
    }

    @Override
    public Publisher findById(Long publisherId) {
        log.info("Getting publisher from database with id={}", publisherId);
        return (Publisher) getHibernateSession().createQuery("from Publisher where id=:publisherId")
                .setLong("publisherId", publisherId).uniqueResult();
    }
}
