package com.gpi.dao.publisher;


import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.NonExistentPublisherException;

import java.util.List;

public interface PublisherDao {

    Publisher findPublisherByNameAndPassword(String publisherName, String password);

    Publisher findByName(String userName) throws NonExistentPublisherException;

    Publisher createPublisher(Publisher publisher);

    void updatePublisher(Publisher publisher);

    List<Publisher> getPublisherList();

    Publisher findById(Long publisherId);
}
