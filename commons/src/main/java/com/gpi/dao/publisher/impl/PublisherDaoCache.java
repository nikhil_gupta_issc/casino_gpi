package com.gpi.dao.publisher.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.publisher.PublisherDao;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PublisherDaoCache implements PublisherDao {

    private static final Logger log = LoggerFactory.getLogger(PublisherDaoCache.class);
    private CacheManager cacheManager;
    private PublisherDao publisherDaoHibernate;

    @Override
    public Publisher findPublisherByNameAndPassword(String publisherName, String password) {
        log.debug("Looking in cache for Publisher with name = [{}] and password = [{}].",
                publisherName, password);
        Publisher publisher = (Publisher) cacheManager.get(getKey(publisherName, password));
        if (publisher == null) {
            publisher = publisherDaoHibernate.findPublisherByNameAndPassword(publisherName, password);
            log.debug("Storing in cache Publisher [{}] ", publisher);
            cacheManager.store(getKey(publisherName, password), publisher, CacheConstants.PUBLISHER_BY_NAME_PASS_TTL);
        }
        return publisher;
    }

    @Override
    public Publisher findByName(String name) throws NonExistentPublisherException {
        log.debug("Looking in cache for Publisher with name = [{}].",
                name);
        Publisher publisher = (Publisher) cacheManager.get(CacheConstants.PUBLISHER_BY_NAME_KEY + name);
        if (publisher == null) {
            publisher = publisherDaoHibernate.findByName(name);
            log.debug("Storing in cache Publisher [{}] ", publisher);
            cacheManager.store(CacheConstants.PUBLISHER_BY_NAME_KEY + name, publisher, CacheConstants.PUBLISHER_BY_NAME_TTL);
        }
        return publisher;
    }

    @Override
    public Publisher createPublisher(Publisher publisher) {
        log.debug("Adding publisher={} into cache", publisher);
        publisher = publisherDaoHibernate.createPublisher(publisher);
        cacheManager.store(CacheConstants.PUBLISHER_BY_NAME_KEY + publisher.getName(),
                publisher, CacheConstants.PUBLISHER_BY_NAME_TTL);
        return publisher;
    }

    @Override
    public void updatePublisher(Publisher publisher) {
        publisherDaoHibernate.updatePublisher(publisher);
        cacheManager.store(CacheConstants.PUBLISHER_BY_NAME_KEY + publisher.getName(),
                publisher, CacheConstants.PUBLISHER_BY_NAME_TTL);
    }

    @Override
    public List<Publisher> getPublisherList() {
        return publisherDaoHibernate.getPublisherList();
    }

    @Override
    public Publisher findById(Long publisherId) {
        log.debug("This method wasn't implemented in cache");
        return publisherDaoHibernate.findById(publisherId);
    }

    private String getKey(String publisherName, String password) {
        return String.format("%s%s_%s", CacheConstants.PUBLISHER_BY_NAME_PASS_KEY, publisherName, password);
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public void setPublisherDaoHibernate(PublisherDao publisherDaoHibernate) {
        this.publisherDaoHibernate = publisherDaoHibernate;
    }
}
