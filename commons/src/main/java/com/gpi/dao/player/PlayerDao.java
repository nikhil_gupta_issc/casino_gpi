package com.gpi.dao.player;

import com.gpi.domain.player.Player;
import com.gpi.exceptions.NonExistentPlayerException;
import com.gpi.exceptions.UnknownTokenException;

import java.util.List;

/**
 * GPI player data access object.
 */
public interface PlayerDao {

    /**
     * Saves the player to the DB
     *
     * @return the player with its proper id
     */
    Player savePlayer(Player player);

    /**
     * updates the player on the DB
     */
    public void updatePlayer(Player player);

    /**
     * Gets a player by id
     *
     * @param id
     * @throws com.gpi.exceptions.NonExistentPlayerException
     */
    Player findById(Long id) throws NonExistentPlayerException;

    /**
     * This method should be called only in the session to get the Player
     *
     * @param token
     * @return
     */
    Player findByToken(String token) throws UnknownTokenException;

    /**
     * This method should be used to store de player in the session. It also
     * removes the old session saved by the old token
     *
     * @param player a Player object to store
     * @param token  a new Token
     */
    void savePlayerInSession(Player player, String token);

    void removePlayerInSession(String token);

    /**
     * Gets a player by playerName
     */
    Player findByPlayerNameAndPublisher(String playerName, Integer publisher);

    /**
     * Retrieves all players that belongs to the specified publisher
     *
     * @param publisher
     * @return
     */
    List<Player> findPlayersByPublisher(Integer publisher);

    List<Player> findPlayerLike(String name, Integer publisherId);

    List<Player> findPlayerLike(String name);

	List<Player> findPlayersByPlayerNameAndPublisher(String playerName,
			Integer publisher);

	List<Player> findPlayersByPlayerName(String playerName);
}
