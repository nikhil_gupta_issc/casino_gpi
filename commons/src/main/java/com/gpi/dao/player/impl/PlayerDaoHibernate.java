package com.gpi.dao.player.impl;

import com.gpi.dao.player.PlayerDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.player.Player;
import com.gpi.exceptions.NonExistentPlayerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class PlayerDaoHibernate extends ExtendedHibernateDaoSupport implements PlayerDao {

    private static final Logger log = LoggerFactory.getLogger(PlayerDaoHibernate.class);

    @Override
    public Player savePlayer(Player player) {
        log.debug("Saving player into database");
        Long userId = (Long) getHibernateSession().save(player);
        player.setId(userId);
        return player;
    }

    @Override
    public void updatePlayer(Player user) {
        log.debug("Updating player into database");
        getHibernateSession().update(user);
    }

    @Override
    public Player findById(Long id) throws NonExistentPlayerException {
        log.debug("Looking for player into database with id = [{}]", id);
        List<?> users = getHibernateSession().createQuery("from Player where id= :id").setLong("id", id).list();
        if (users != null && users.size() > 0) {
            return (Player) users.get(0);
        } else {
            throw new NonExistentPlayerException(String.format("Player with id [%s] does not exists in the database.", id));
        }
    }

    @Override
    public Player findByToken(String token) {
        throw new RuntimeException("This method shouldn't be called");
    }

    @Override
    public void savePlayerInSession(Player player, String token) {
        throw new RuntimeException("This method shouldn't be called");
    }

    @Override
    public void removePlayerInSession(String token) {
        throw new RuntimeException("This method shouldn't be called");
    }

    @Override
    public Player findByPlayerNameAndPublisher(String playerName, Integer publisher) {
        log.debug("Looking for player into database with name = [{}] and publisher = [{}]", playerName, publisher);
        List<?> users = getHibernateSession().createQuery("from Player where name= :playerName and publisher = :publisher")
                .setString("playerName", playerName).setInteger("publisher", publisher).list();
        if (users != null && users.size() > 0) {
            return (Player) users.get(0);
        } else {
            log.debug("No player found. Returning null.");
            return null;
        }
    }

    @Override
    public List<Player> findPlayersByPublisher(Integer publisher) {
        log.debug("Retrieving all player for publisher={}", publisher);
        List<?> users = getHibernateSession().createQuery("from Player where publisher = :publisher").setInteger("publisher", publisher).list();
        return (List<Player>) users;
    }

    @Override
    public List<Player> findPlayerLike(String name, Integer publisherId) {
        return getHibernateSession().createQuery("from Player where publisher.id = :publisher and name like :playerName")
                .setParameter("publisher", publisherId).setParameter("playerName", "%" + name + "%").setMaxResults(10).list();
    }

    @Override
    public List<Player> findPlayerLike(String name) {
        return getHibernateSession().createQuery("from Player where name like :playerName")
                .setParameter("playerName", "%" + name + "%").setMaxResults(10).list();
    }
    
    @Override
    public List<Player> findPlayersByPlayerNameAndPublisher(String playerName, Integer publisher) {
    	log.debug("Looking for player into database with name = [{}] and publisher = [{}]", playerName, publisher);
    	List<?> users = getHibernateSession().createQuery("from Player where name= :playerName and publisher = :publisher")
    			.setString("playerName", playerName).setInteger("publisher", publisher).list();
    	if (users != null && users.size() > 0) {
    		return (List<Player>) users;
    	} else {
    		log.debug("No player found. Returning null.");
    		return null;
    	}
    }
    
    @Override
    public List<Player> findPlayersByPlayerName(String playerName) {
    	log.debug("Looking for player into database with name = [{}] ", playerName);
    	List<?> users = getHibernateSession().createQuery("from Player where name= :playerName").setParameter("playerName", playerName).list();
    	if (users != null && users.size() > 0) {
    		return (List<Player>) users;
    	} else {
    		log.debug("No player found. Returning null.");
    		return null;
    	}
    }
}
