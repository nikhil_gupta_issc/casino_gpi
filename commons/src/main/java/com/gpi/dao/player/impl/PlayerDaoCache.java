package com.gpi.dao.player.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.player.PlayerDao;
import com.gpi.domain.player.Player;
import com.gpi.exceptions.NonExistentPlayerException;
import com.gpi.exceptions.UnknownTokenException;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class PlayerDaoCache implements PlayerDao {

    private static final Logger log = LoggerFactory.getLogger(PlayerDaoCache.class);
    private PlayerDao playerDaoHibernate;
    private CacheManager cacheManager;

    @Override
    public Player savePlayer(Player player) {
        log.debug("Saving player in cache.");
        player = playerDaoHibernate.savePlayer(player);
        cacheManager.store(getPlayerKey(player), player, CacheConstants.PLAYER_TTL);
        cacheManager.store(getPlayerKeyById(player.getId()), player, CacheConstants.PLAYER_TTL);
        return player;
    }

    @Override
    public void updatePlayer(Player player) {
        log.debug("Updateing player in cache.");
        playerDaoHibernate.updatePlayer(player);
        cacheManager.store(getPlayerKey(player), player, CacheConstants.PLAYER_TTL);
        cacheManager.store(getPlayerKeyById(player.getId()), player, CacheConstants.PLAYER_TTL);
    }

    @Override
    public Player findById(Long id) throws NonExistentPlayerException {
        log.debug("Looking player by id = [{}] in cache", id);
        Player player = (Player) cacheManager.get(getPlayerKeyById(id));
        if (player == null) {
            player = playerDaoHibernate.findById(id);
            cacheManager.store(getPlayerKeyById(id), player, CacheConstants.PLAYER_TTL);
        }
        return player;
    }

    @Override
    public Player findByPlayerNameAndPublisher(String playerName, Integer publisher) {
        log.debug("Looking player by name = [{}] and publisher [{}] in cache", playerName, publisher);
        Player player = (Player) cacheManager.get(String.format("%s%s-%s", CacheConstants.PLAYER_KEY, playerName, publisher));
        if (player == null) {
            player = playerDaoHibernate.findByPlayerNameAndPublisher(playerName, publisher);
            if (player != null) {
                cacheManager.store(CacheConstants.PLAYER_KEY, player, CacheConstants.PLAYER_TTL);
            }
        }
        return player;
    }

    @Override
    public Player findByToken(String token) throws UnknownTokenException {
        Player player = (Player) cacheManager.get(CacheConstants.PLAYER_BY_TOKEN_KEY + token);
        if (player == null) {
            throw new UnknownTokenException("Token does not exists in cache: " + token);
        }
        return player;
    }

    @Override
    public List<Player> findPlayersByPublisher(Integer publisher) {
        return playerDaoHibernate.findPlayersByPublisher(publisher);
    }

    @Override
    public void savePlayerInSession(Player player, String token) {
        cacheManager.store(CacheConstants.PLAYER_BY_TOKEN_KEY + token, player, CacheConstants.PLAYER_BY_TOKEN_TTL);
    }

    @Override
    public void removePlayerInSession(String token) {
        cacheManager.delete(CacheConstants.PLAYER_BY_TOKEN_KEY + token);
    }

    @Override
    public List<Player> findPlayerLike(String name, Integer publisherId) {
        return playerDaoHibernate.findPlayerLike(name, publisherId);
    }

    @Override
    public List<Player> findPlayerLike(String name) {
        return playerDaoHibernate.findPlayerLike(name);
    }

    private String getPlayerKey(Player player) {
        return String.format("%s%s-%s", CacheConstants.PLAYER_KEY, player.getName(), player.getPublisher().getName());
    }

    private String getPlayerKeyById(Long id) {
        return String.format("%s%s", CacheConstants.PLAYER_KEY_ID, id);
    }

    public void setPlayerDaoHibernate(PlayerDao playerDaoHibernate) {
        this.playerDaoHibernate = playerDaoHibernate;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

	@Override
	public List<Player> findPlayersByPlayerNameAndPublisher(String playerName,
			Integer publisher) {

        log.debug("Looking players by name = [{}] and publisher [{}] in cache", playerName, publisher);
        Player player = (Player) cacheManager.get(String.format("%s%s-%s", CacheConstants.PLAYER_KEY, playerName, publisher));
        List<Player> players= new ArrayList<Player>();
        if (player == null) {
            players = playerDaoHibernate.findPlayersByPlayerNameAndPublisher(playerName, publisher);
        }
        else players.add(player);
        
		return players;
	}

	@Override
	public List<Player> findPlayersByPlayerName(String playerName) {
        log.debug("Looking players by name = [{}]  in cache", playerName);
        Player player = (Player) cacheManager.get(String.format("%s%s", CacheConstants.PLAYER_KEY, playerName));
        List<Player> players= new ArrayList<Player>();
        if (player == null) {
            players = playerDaoHibernate.findPlayersByPlayerName(playerName);
        }
        else players.add(player);
        
		return players;
	}
}
