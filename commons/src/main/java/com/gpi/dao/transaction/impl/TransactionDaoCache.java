package com.gpi.dao.transaction.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.transaction.TransactionDao;
import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.Game;
import com.gpi.domain.transaction.TransactionFilter;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Nacho on 11/27/2014.
 */
public class TransactionDaoCache implements TransactionDao{

    private static final Logger logger = LoggerFactory.getLogger(TransactionDaoCache.class);
    private CacheManager cacheManager;
    private TransactionDao transactionDao;

    @Override
    public Transaction getTransaction(String providerTransaction, Round round) {
        logger.debug("Looking for transaction into cache...");
        Transaction transaction = (Transaction) cacheManager.get(getKey(providerTransaction, round));
        if(transaction == null) {
            transaction = transactionDao.getTransaction(providerTransaction, round);
            if(transaction != null){
                cacheManager.store(getKey(providerTransaction, round), transaction, CacheConstants.TRANSACTION_TTL);
            }
        }
        return transaction;
    }

    @Override
    public Transaction saveTransaction(Transaction transaction) {
        logger.debug("Saving transaction={}", transaction);
        transaction = transactionDao.saveTransaction(transaction);
        cacheManager.store(getKey(transaction.getGameTransactionId(), transaction.getRound()), transaction, CacheConstants.TRANSACTION_TTL);
        logger.debug("Transaction saved successfully with id={}", transaction.getId());
        return transaction;
    }

    @Override
    public void updateTransaction(Transaction transaction) {
        logger.debug("Saving transaction={}", transaction);
        transactionDao.updateTransaction(transaction);
        cacheManager.store(getKey(transaction.getGameTransactionId(), transaction.getRound()), transaction, CacheConstants.TRANSACTION_TTL);
        logger.debug("Transaction saved successfully with id={}", transaction.getId());
    }

    @Override
    public List<Transaction> getTransactionsByRound(Long roundId, Integer startIndex, Integer pageSize, String sortExpression) {
        logger.debug("This method wasn't implemented in cache");
        return transactionDao.getTransactionsByRound(roundId, startIndex, pageSize, sortExpression);
    }
    
    @Override
	public List<Transaction> getTransactionsByGameRound(Long gameRoundId, Integer startIndex, Integer pageSize, Game game) {
    	logger.debug("This method wasn't implemented in cache");
        return transactionDao.getTransactionsByGameRound(gameRoundId, startIndex, pageSize, game);
	}
    
    @Override
    public Long count(Long roundId, Integer startIndex, Integer pageSize, String sortExpression) {
        logger.debug("This method wasn't implemented in cache");
        return transactionDao.count(roundId, startIndex, pageSize, sortExpression);
    }

    @Override
    public Transaction getTransaction(String providerTransaction, String gameName) {
        logger.debug("Looking for transaction into cache...");
        Transaction transaction = (Transaction) cacheManager.get(getKeyExt(providerTransaction, gameName));
        if(transaction == null) {
            transaction = transactionDao.getTransaction(providerTransaction, gameName);
            if(transaction != null){
                cacheManager.store(getKeyExt(providerTransaction, gameName), transaction, CacheConstants.TRANSACTION_EXID_TTL);
            }
        }
        return transaction;
    }
    
	@Override
	public List<Transaction> getTransactions(TransactionFilter filter) {
        logger.debug("This method wasn't implemented in cache");
        return transactionDao.getTransactions(filter);
	}
    
	@Override
	public Long countByGameRound(Long gameRoundId) {
        logger.debug("This method wasn't implemented in cache");
        return transactionDao.countByGameRound(gameRoundId);
	}
	
    private String getKey(String providerTransaction, Round round) {
        return String.format("%s%s-%s", CacheConstants.TRANSACTION_KEY, providerTransaction, round.getId());
    }
    
    private String getKeyExt(String providerTransaction, String gameName) {
        return String.format("%s%s-%s", CacheConstants.TRANSACTION_EXTID_KEY, providerTransaction, gameName);
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public void setTransactionDao(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

}
