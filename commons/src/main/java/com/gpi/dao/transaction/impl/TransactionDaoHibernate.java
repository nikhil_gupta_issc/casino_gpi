package com.gpi.dao.transaction.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.FilterOperator;
import com.gpi.backoffice.query.filter.FirstLevelFieldFilter;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.backoffice.query.filter.ThirdLevelFieldFilter;
import com.gpi.dao.transaction.TransactionDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.Game;
import com.gpi.domain.transaction.TransactionFilter;
import com.gpi.domain.transaction.TransactionType;

/**
 * Created by Nacho on 11/27/2014.
 */
public class TransactionDaoHibernate extends ExtendedHibernateDaoSupport implements TransactionDao {

    private static final Logger logger = LoggerFactory.getLogger(TransactionDaoHibernate.class);

    private QueryObject<Transaction> queryObject;
    
    @Override
    public Transaction getTransaction(String providerTransaction, Round round) {
        logger.debug("Looking for transaction into database...Round: "+round+" transaction: "+providerTransaction);
        Transaction transaction = (Transaction) getHibernateSession().createQuery(" from Transaction where gameTransactionId=:transactionId and round.id=:roundId").
                setString("transactionId", providerTransaction).setLong("roundId", round.getId()).uniqueResult();
        return transaction;
    }

    @Override
    public Transaction saveTransaction(Transaction t) {
        logger.debug("Saving Transaction into database...");
        getHibernateSession().save(t);
        logger.debug("Saved Transaction successfully.");
        return t;
    }

    @Override
    public void updateTransaction(Transaction transaction) {
        logger.debug("Updating transaction");
        getHibernateSession().merge(transaction);
        logger.debug("Updating transaction successfully");
    }

    @Override
    public List<Transaction> getTransactionsByRound(Long roundId, Integer startIndex, Integer pageSize, String sortExpression) {
        logger.debug("Getting all transactions belonging to the round={}", roundId);
        return getHibernateSession().createQuery("from Transaction where round.id=:roundId").setLong("roundId", roundId)
                .setMaxResults(pageSize).setFirstResult(startIndex).list();
    }
    
	@Override
	public List<Transaction> getTransactionsByGameRound(Long gameRoundId, Integer startIndex, Integer pageSize, Game game) {
		logger.debug("Getting all transactions belonging to the gameRound={}", gameRoundId);
		
		// TODO remove that when an index is created
		Long roundId = this.getRoundIdFromGameRoundId(gameRoundId, game);
		List<Transaction> t ;
		if (roundId != null) {
			t = getHibernateSession().createQuery("from Transaction where round_id=:roundId").setLong("roundId", roundId)
		                .setMaxResults(pageSize).setFirstResult(startIndex).list();
		} else {
			t = new ArrayList<Transaction>();
		}
		
		return t;
				
		//Round round = (Round) getHibernateSession().createQuery("from Round where gameRoundId=:roundId").setLong("roundId", gameRoundId).uniqueResult();
        //return getHibernateSession().createQuery("from Transaction where round_id=:roundId").setLong("roundId", round.getId())
        //        .setMaxResults(pageSize).setFirstResult(startIndex).list();
	}
    
    @Override
    public Long count(Long roundId, Integer startIndex, Integer pageSize, String sortExpression) {
        return (Long) getHibernateSession().createQuery("select count(distinct t.id) from Transaction t where t.round.id=:roundId").setLong("roundId", roundId)
                .setMaxResults(1).uniqueResult();
    }
    
    @Override
    public Transaction getTransaction(String providerTransaction, String gameName) {
        logger.debug("Looking for transaction into database...");
        Transaction transaction = (Transaction) getHibernateSession().createQuery(" from Transaction where gameTransactionId=:transactionId and round.game.name=:gameName and (type =:betType or type =:winType)").
                setString("transactionId", providerTransaction).setString("gameName", gameName).setString("betType", TransactionType.BET.name()).setString("winType", TransactionType.WIN.name()).uniqueResult();
        return transaction;
    }
    
    @Override
    public List<Transaction> getTransactions(TransactionFilter filter) {
        logger.debug("Getting all transactions belonging to the filter={}", filter.toString());
        int startIndex = filter.getStartIndex() == null ? 0 : filter.getStartIndex();
        int pageSize = filter.getPageSize() == null ? -1 : filter.getPageSize();
        return queryObject.get(startIndex, pageSize, filter.getSortExpression(), generateFilters(filter));
    }
    
    @Override
    public Long countByGameRound(Long gameRoundId) {
        return (Long) getHibernateSession().createQuery("select count(distinct t.id) from Transaction t where t.round.gameRoundId=:gameRoundId").setLong("gameRoundId", gameRoundId)
                .setMaxResults(1).uniqueResult();
    }
    
    // TODO temporary fix while there is no idex. remove when its created
    private Long getRoundIdFromGameRoundId(long gameRoundId, Game game) {
    	BigInteger roundId = (BigInteger)getHibernateSession().createSQLQuery("select round_id from round_game_round where game_round_id = :gameRoundId and game = :gameName").setLong("gameRoundId", gameRoundId).setString("gameName", game.getName()).setMaxResults(1).uniqueResult();
    	
    	logger.debug("round id from game round " + gameRoundId + " is " + roundId);
    	return roundId != null ? roundId.longValue() : null;
    }
    
	private List<HibernateFilter> generateFilters(TransactionFilter filter) {
		List<HibernateFilter> filters = new ArrayList<HibernateFilter>();
		
        if (filter.getProviderTransactionId() != null) {
        	filters.add(new FirstLevelFieldFilter("gameTransactionId", FilterOperator.EQ, filter.getProviderTransactionId()));
        }
        
        if (filter.getPlayerName() != null) {
        	filters.add(new ThirdLevelFieldFilter("round", "player", "name", FilterOperator.EQ, filter.getPlayerName()));
        }
        
        if (filter.getPlayerPublisherCode() != null){
        	filters.add(new ThirdLevelFieldFilter("round", "publisher", "name", FilterOperator.EQ, filter.getPlayerPublisherCode()));
        }
        
		return filters;
	}
    
	
	public void setQueryObject(QueryObject<Transaction> queryObject) {
		this.queryObject = queryObject;
	}
    
}
