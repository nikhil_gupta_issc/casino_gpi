package com.gpi.dao.transaction;

import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.Game;
import com.gpi.domain.transaction.TransactionFilter;

import java.util.List;

/**
 * Created by Nacho on 11/27/2014.
 */
public interface TransactionDao {
    Transaction getTransaction(String providerTransaction, Round round);

    Transaction saveTransaction(Transaction t);

    List<Transaction> getTransactionsByRound(Long roundId, Integer startIndex, Integer pageSize, String sortExpression);
    
    Long count(Long roundId, Integer startIndex, Integer pageSize, String sortExpression);

    void updateTransaction(Transaction transaction);
    
    Transaction getTransaction(String providerTransaction, String gameName);

	List<Transaction> getTransactionsByGameRound(Long gameRoundId, Integer startIndex, Integer pageSize, Game game);
	
	List<Transaction> getTransactions(TransactionFilter filter);
	
	Long countByGameRound(Long gameRoundId);

}
