package com.gpi.dao.applicationproperty.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gpi.dao.applicationproperty.ApplicationPropertyDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.applicationproperty.ApplicationProperty;

@Transactional
public class ApplicationPropertyDaoHibernate extends ExtendedHibernateDaoSupport implements ApplicationPropertyDao {

    private static final Logger log = LoggerFactory.getLogger(ApplicationPropertyDaoHibernate.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<ApplicationProperty> findAll() {
		return getHibernateSession().createCriteria(ApplicationProperty.class).list();
	}

	@Override
	public Integer saveOrUpdate(ApplicationProperty applicationProperty) {
        log.debug("SaveOrUpdate ApplicationProperty [{}] in the database.", applicationProperty);
        getHibernateSession().saveOrUpdate(applicationProperty);
        return applicationProperty.getId();
		
	}

	@Override
	public void delete(ApplicationProperty applicationProperty) {
		log.debug("delete ApplicationProperty [{}] in the database.", applicationProperty);
		getHibernateSession().delete(applicationProperty);	
	}

	@Override
	public String getProperty(String key) {
		return (String)
				getHibernateSession().createSQLQuery("select prop from ApplicationProperty prop where name=:name")
			    .addEntity(ApplicationProperty.class)
			    .setParameter("name", key)
			    .uniqueResult();
	}
}
