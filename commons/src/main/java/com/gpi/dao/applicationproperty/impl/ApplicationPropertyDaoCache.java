package com.gpi.dao.applicationproperty.impl;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.applicationproperty.ApplicationPropertyDao;
import com.gpi.domain.applicationproperty.ApplicationProperty;
import com.gpi.utils.cache.CacheManager;

public class ApplicationPropertyDaoCache implements ApplicationPropertyDao {

    private static final Logger log = LoggerFactory.getLogger(ApplicationPropertyDaoCache.class);
    private CacheManager cacheManager;
    private ApplicationPropertyDao applicationPropertyDao;
    
	@Override
	public List<ApplicationProperty> findAll() {
    	List<ApplicationProperty> properties = applicationPropertyDao.findAll();
    	refreshCache(properties);
    	return properties;
	}
    
    @SuppressWarnings("unchecked")
	@Override
	public String getProperty(String key) {
    	HashMap<String, String> properties = (HashMap<String, String>)cacheManager.getAndTouch(CacheConstants.APP_PROPERTY_CACHE_KEY, CacheConstants.APP_PROPERTY_CACHE_TTL);
    	if (properties == null) {
    		properties = refreshCache();
    	}		
    	return properties.get(key);
	}
    
	@Override
	public Integer saveOrUpdate(ApplicationProperty applicationProperty) {
		Integer id = applicationPropertyDao.saveOrUpdate(applicationProperty);
		refreshCache();
		return id;
	}
	@Override
	public void delete(ApplicationProperty applicationProperty) {
		applicationPropertyDao.delete(applicationProperty);
		refreshCache();
	}
	
	private HashMap<String, String> refreshCache() {
		List<ApplicationProperty> properties = applicationPropertyDao.findAll();
		return refreshCache(properties);
	}

	private HashMap<String, String> refreshCache(List<ApplicationProperty> properties) {
		HashMap<String, String> propertiesMap = generateMap(properties);
		cacheManager.store(CacheConstants.APP_PROPERTY_CACHE_KEY, propertiesMap, CacheConstants.APP_PROPERTY_CACHE_TTL);
		return propertiesMap;
	}
	
	private HashMap<String, String> generateMap(List<ApplicationProperty> propertyList) {
		HashMap<String, String> properties = new HashMap<String, String>();
		for (ApplicationProperty appProp : propertyList) {
			properties.put(appProp.getName(), appProp.getValue());
		}
		return properties;
	}
	
	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	/**
	 * @param applicationPropertyDao the applicationPropertyDao to set
	 */
	public void setApplicationPropertyDao(ApplicationPropertyDao applicationPropertyDao) {
		this.applicationPropertyDao = applicationPropertyDao;
	}
    
}
