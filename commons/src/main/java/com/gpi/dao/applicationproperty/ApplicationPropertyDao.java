package com.gpi.dao.applicationproperty;


import java.util.List;

import com.gpi.domain.applicationproperty.ApplicationProperty;

public interface ApplicationPropertyDao {

	String getProperty(String key);
	
	List<ApplicationProperty> findAll();
	
	Integer saveOrUpdate(ApplicationProperty applicationProperty);
	
	void delete(ApplicationProperty applicationProperty);
}
