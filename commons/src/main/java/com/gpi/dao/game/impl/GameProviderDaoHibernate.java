package com.gpi.dao.game.impl;

import com.gpi.dao.game.GameProviderDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.game.GameProvider;
import com.gpi.exceptions.GameProviderNotExistentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Nacho on 2/20/14.
 */
public class GameProviderDaoHibernate extends ExtendedHibernateDaoSupport implements GameProviderDao {

    private static final Logger logger = LoggerFactory.getLogger(GameProviderDaoHibernate.class);

    @Override
    public GameProvider saveGameProvider(GameProvider provider) {
        logger.debug("Saving GameProvider={} into database", provider);
        Integer id = (Integer) getHibernateSession().save(provider);
        provider.setId(id);
        return provider;
    }

    @Override
    public GameProvider getGameProvider(String name) throws GameProviderNotExistentException {
        logger.debug("Getting GameProvider with name={} from database", name);
        GameProvider provider = (GameProvider) getHibernateSession().createQuery("from GameProvider where name=:name")
                .setString("name", name).uniqueResult();
        if (provider == null) {
            throw new GameProviderNotExistentException(String.format("GameProvider with name=%s does not exists", name));
        }
        return provider;
    }

    @Override
    public void updateGameProvider(GameProvider gameProvider) {
        logger.debug("Updating GameProvider [{}]", gameProvider);
        getHibernateSession().update(gameProvider);
    }

    @Override
    public List<GameProvider> getGameProviders() {
        logger.debug("Getting all providers from database");
        return getHibernateSession().createQuery("from GameProvider").list();
    }
}
