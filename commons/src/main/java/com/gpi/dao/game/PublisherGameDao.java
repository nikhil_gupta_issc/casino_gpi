package com.gpi.dao.game;


import java.util.List;

import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.game.PublisherGameMapping;
import com.gpi.domain.publisher.Publisher;

/**
 * User: igabbarini
 */
public interface PublisherGameDao {

    PublisherGameMapping getPublisherGameMappingByGame(Publisher publisher, Game game);

    PublisherGameMapping getPublisherGameMappingById(Long id);

    List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher);

    List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher, GameProvider provider);
    
    Long savePublisherGameMapping(PublisherGameMapping publisherGameMapping);
    
    void delete(Long id);
}
