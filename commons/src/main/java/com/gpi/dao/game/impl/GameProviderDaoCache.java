package com.gpi.dao.game.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.game.GameProviderDao;
import com.gpi.domain.game.GameProvider;
import com.gpi.exceptions.GameProviderNotExistentException;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Nacho on 2/20/14.
 */
public class GameProviderDaoCache implements GameProviderDao {

    private static final Logger logger = LoggerFactory.getLogger(GameProviderDaoCache.class);
    private CacheManager cacheManager;
    private GameProviderDao gameProviderDao;

    @Override
    public GameProvider saveGameProvider(GameProvider provider) {
        logger.debug("Saving GameProvider={} into cache", provider);
        provider = gameProviderDao.saveGameProvider(provider);
        cacheManager.store(getCacheKey(provider.getName()), provider, CacheConstants.GAME_PROVIDER_TTL);
        return null;
    }

    @Override
    public GameProvider getGameProvider(String name) throws GameProviderNotExistentException {
        logger.debug("Get GameProvider from cache with name={}", name);
        GameProvider provider = (GameProvider) cacheManager.get(getCacheKey(name));
        if (provider == null) {
            logger.debug("GameProvider doesn't exists on cache. Looking into database.");
            provider = gameProviderDao.getGameProvider(name);
        }
        return provider;
    }

    @Override
    public void updateGameProvider(GameProvider gameProvider) {
        gameProviderDao.updateGameProvider(gameProvider);
        cacheManager.store(getCacheKey(gameProvider.getName()), gameProvider, CacheConstants.ALL_PROVIDERS_TTL);
    }

    @Override
    public List<GameProvider> getGameProviders() {
        List<GameProvider> providers = (List<GameProvider>) cacheManager.get(getAllProvidersKey());
        if (providers == null) {
            logger.debug("Providers not found into cache. Looking into database");
            providers = gameProviderDao.getGameProviders();
        }
        logger.debug("Providers found: {}", providers);
        return providers;
    }

    private String getAllProvidersKey() {

        return CacheConstants.ALL_PROVIDERS_KEY;
    }

    private String getCacheKey(String name) {
        return CacheConstants.GAME_PROVIDER_KEY + name;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public void setGameProviderDao(GameProviderDao gameProviderDao) {
        this.gameProviderDao = gameProviderDao;
    }
}
