package com.gpi.dao.game.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.game.PublisherGameDao;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.game.PublisherGameMapping;
import com.gpi.domain.publisher.Publisher;
import com.gpi.utils.cache.CacheManager;

public class PublisherGameDaoCache implements PublisherGameDao {

    private static final Logger log = LoggerFactory.getLogger(PublisherGameDaoCache.class);
    private PublisherGameDao publisherGameDaoHibernate;
    private CacheManager cacheManager;

    @Override
    public PublisherGameMapping getPublisherGameMappingByGame(Publisher publisher, Game game) {
        log.info("Getting PublisherGameMapping from cache for publisher=[{}] and game=[{}]", publisher, game);
        PublisherGameMapping publisherGameMapping = (PublisherGameMapping)cacheManager.get(getPublisherGameByGameKey(publisher.getId(), game.getId()));
        if (publisherGameMapping == null) {
            publisherGameMapping = publisherGameDaoHibernate.getPublisherGameMappingByGame(publisher, game);
            if (publisherGameMapping != null) {
            	log.info("Storing in cache = [{}]", publisherGameMapping);
                cacheManager.store(getPublisherGameByGameKey(publisher.getId(), game.getId()), publisherGameMapping, CacheConstants.PUBLISHER_GAME_SESSION_TTL);        	
            }
        }
        return publisherGameMapping;
    }

    @Override
    public PublisherGameMapping getPublisherGameMappingById(Long id) {
        return publisherGameDaoHibernate.getPublisherGameMappingById(id);
    }

    @Override
    public List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher) {
        String cacheKey = getPublisherGameByGameKey(publisher.getId());
    	
        List<PublisherGameMapping> list = (ArrayList<PublisherGameMapping>)cacheManager.getAndTouch(cacheKey, CacheConstants.PUBLISHER_GAME_SESSION_TTL);
    	if (list == null){
            list = publisherGameDaoHibernate.getPublisherGameMappings(publisher);
            cacheManager.store(cacheKey, new ArrayList<PublisherGameMapping>(list), CacheConstants.PUBLISHER_GAME_SESSION_TTL);
    	}
    	return list;
    }

    @Override
    public List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher, GameProvider provider) {
        return publisherGameDaoHibernate.getPublisherGameMappings(publisher, provider); 
    }
    
    @Override
    public Long savePublisherGameMapping(PublisherGameMapping publisherGameMapping) {
        Long id = publisherGameDaoHibernate.savePublisherGameMapping(publisherGameMapping);
        
        log.info("Storing in cache = [{}]", publisherGameMapping);
        cacheManager.store(getPublisherGameByGameKey(publisherGameMapping.getPublisher().getId(), publisherGameMapping.getGame().getId()),publisherGameMapping, CacheConstants.PUBLISHER_GAME_SESSION_TTL);
        cacheManager.delete(getPublisherGameByGameKey(publisherGameMapping.getPublisher().getId()));
        
        return id;
    }
    
    @Override
    public void delete(Long id) {
        PublisherGameMapping publisherGameMapping = this.getPublisherGameMappingById(id);
        Integer publisherId = publisherGameMapping.getPublisher().getId();
        Long gameId = publisherGameMapping.getGame().getId();
        
        publisherGameDaoHibernate.delete(id);
         
        log.info("Deleting from cache = [{}]", id);
        cacheManager.delete(getPublisherGameByGameKey(publisherId, gameId));
        cacheManager.delete(getPublisherGameByGameKey(publisherGameMapping.getPublisher().getId()));
    }

    private String getPublisherGameByGameKey(Integer publisherId, Long gameId) {
        return CacheConstants.PUBLISHER_GAME_SESSION_KEY + publisherId + gameId;
    }
    
    private String getPublisherGameByGameKey(Integer publisherId) {
        return CacheConstants.PUBLISHER_GAME_SESSION_KEY + publisherId;
    }
    
    
    public void setPublisherGameDaoHibernate(PublisherGameDao publisherGameDaoHibernate) {
        this.publisherGameDaoHibernate = publisherGameDaoHibernate;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
}
