package com.gpi.dao.game;

import com.gpi.domain.game.Game;
import com.gpi.exceptions.NonExistentGameException;

import java.util.List;

/**
 * User: igabbarini
 */
public interface GameDao {

    Game saveGame(Game game);

    List<Game> getGames();

    /**
     * Adds new Games
     *
     * @param game The Game to add to the database
     */
    Game addGame(Game game);

    Game getGame(String gameName) throws NonExistentGameException;

    /**
     * Retrieves all the games that are hosted by a specific provider
     */
    List<Game> findGamesByProvider(Integer providerId);

    void updateGame(Game game);

    Game getGame(Long gameId) throws NonExistentGameException;

	List<Game> getGamesWithFreeSpins();
}
