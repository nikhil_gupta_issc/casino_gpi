package com.gpi.dao.game;


import com.gpi.domain.game.GameProvider;
import com.gpi.exceptions.GameProviderNotExistentException;

import java.util.List;

/**
 * Created by Nacho on 2/20/14.
 */
public interface GameProviderDao {


    public GameProvider saveGameProvider(GameProvider provider);

    public GameProvider getGameProvider(String name) throws GameProviderNotExistentException;

    void updateGameProvider(GameProvider gameProvider);

    List<GameProvider> getGameProviders();
}
