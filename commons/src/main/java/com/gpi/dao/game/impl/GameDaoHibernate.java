package com.gpi.dao.game.impl;

import com.gpi.dao.game.GameDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.game.Game;
import com.gpi.exceptions.NonExistentGameException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: igabbarini
 */
@Transactional
public class GameDaoHibernate extends ExtendedHibernateDaoSupport implements GameDao {
    private static final Logger log = LoggerFactory.getLogger(GameDaoHibernate.class);

    @Override
    public Game addGame(Game game) {
        log.debug("Adding new Game=[{}] to the database", game);
        Long id = (Long) getHibernateSession().save(game);
        game.setId(id);
        return game;
    }

    @Override
    public Game saveGame(Game game) {
        log.info("Saving game={} into database", game);
        getHibernateSession().saveOrUpdate(game);
        return game;
    }

    @Override
    public void updateGame(Game game1) {
        log.debug("Saving game into database");
        getHibernateSession().update(game1);
        log.debug("Game saved successfully");
    }

    @Override
    public Game getGame(String gameName) throws NonExistentGameException {
        log.debug("Getting game from database with gameName={}", gameName);
        Game game = (Game) getHibernateSession().createQuery("from Game where name=:gameName")
                .setString("gameName", gameName).uniqueResult();
        if (game == null) {
            throw new NonExistentGameException("The game=" + gameName + " doesn't exists.");
        }
        return game;
    }

    @Override
    public Game getGame(Long gameId) throws NonExistentGameException {
        log.debug("Getting game from database with gameId={}", gameId);
        Game game = (Game) getHibernateSession().createQuery("from Game where id=:id")
                .setLong("id", gameId).uniqueResult();
        if (game == null) {
            throw new NonExistentGameException("The game=" + gameId + " doesn't exists.");
        }
        return game;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Game> findGamesByProvider(Integer providerId) {
        return getHibernateSession().createQuery("from Game where gameProvider.id = :providerId")
                .setInteger("providerId", providerId)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Game> getGames() {
        log.debug("Retrieving the list of games from database");
        return getHibernateSession().createCriteria(Game.class).list();
    }

    @SuppressWarnings("unchecked")
	@Override
	public List<Game> getGamesWithFreeSpins() {
		// TODO Auto-generated method stub
		return getHibernateSession().createQuery("from Game where freespins = :freespins")
                .setBoolean("freespins", true)
                .list();
	}
}
