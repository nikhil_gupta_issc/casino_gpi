package com.gpi.dao.game.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gpi.dao.game.PublisherGameDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.game.PublisherGameMapping;
import com.gpi.domain.publisher.Publisher;

/**
 * User: Igabbarini
 */
@Transactional
public class PublisherGameDaoHibernate extends ExtendedHibernateDaoSupport implements PublisherGameDao {
    private static final Logger log = LoggerFactory.getLogger(PublisherGameDaoHibernate.class);

    @Override
    public PublisherGameMapping getPublisherGameMappingByGame(Publisher publisher, Game game) {
        log.info("Getting PublisherGameMapping from database for publisher=[{}] and game=[{}]"
                , publisher, game);
        PublisherGameMapping publisherGameMapping = (PublisherGameMapping) getHibernateSession()
                .createQuery("from PublisherGameMapping where publisher=:publisher and game=:game_id")
                .setInteger("publisher", publisher.getId()).setLong("game_id", game.getId()).uniqueResult();

        return publisherGameMapping;
    }

    @Override
    public PublisherGameMapping getPublisherGameMappingById(Long id) {
        log.info("Getting PublisherGameMapping from database for id=[{}]", id);
        PublisherGameMapping publisherGameMapping = (PublisherGameMapping) getHibernateSession()
                .createQuery("from PublisherGameMapping where id=:id")
                .setLong("id", id).uniqueResult();
        if (publisherGameMapping == null) {
            throw new RuntimeException(String.format("The id=[%s] doesn't exists", id));
        }
        return publisherGameMapping;
    }

    @Override
    public Long savePublisherGameMapping(PublisherGameMapping publisherGameMapping) {
        log.info("Updating PublisherGameMapping=[{}]", publisherGameMapping);
        getHibernateSession().saveOrUpdate(publisherGameMapping);
        return publisherGameMapping.getId();
    }

    @Override
    public List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher) {
        log.info("Getting all GameMappings from database for publisher=[{}]", publisher);
        return getHibernateSession().createQuery("select pgm from PublisherGameMapping pgm "
        		+ "inner join fetch pgm.game g "
        		+ "inner join fetch g.gameProvider gp "
        		+ "inner join fetch pgm.publisher "
        		+ "where publisher=:publisher")
        		.setInteger("publisher", publisher.getId()).list();
    }

    @Override
    public List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher, GameProvider provider) {
        log.info("Getting all GameMappings from database for publisher=[{}], provider=[{}]", publisher, provider);
        return getHibernateSession().createQuery("select pgm from PublisherGameMapping pgm "
        		+ "inner join fetch pgm.game g "
        		+ "inner join fetch g.gameProvider gp "
        		+ "inner join fetch pgm.publisher "
        		+ "where publisher=:publisher and gp.id=:providerId")
        		.setInteger("publisher", publisher.getId())
        		.setInteger("providerId", provider.getId())
        		.list();
    }
    
    
    @Override
    public void delete(Long id) {
        log.info("Delete id=[{}]", id);
        PublisherGameMapping publisherGameMapping = this.getPublisherGameMappingById(id);
        getHibernateSession().delete(publisherGameMapping);
    }
}
