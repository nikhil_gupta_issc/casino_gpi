package com.gpi.dao.game.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.game.GameDao;
import com.gpi.domain.game.Game;
import com.gpi.exceptions.NonExistentGameException;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * User: igabbarini
 */
public class GameDaoCache implements GameDao {

    private static final Logger log = LoggerFactory.getLogger(GameDaoCache.class);
    private GameDao gameDao;
    private CacheManager cacheManager;

    @Override
    public Game addGame(Game game) {
        log.debug("Adding to cache a new Game={}", game);
        game = gameDao.addGame(game);
        log.info("Saving into cache game={}", game);
        cacheManager.store(getGameCacheKey(game.getName()), game, CacheConstants.GAME_TTL);
        cleanGameListCache(game);
        return game;
    }

    @Override
    public Game saveGame(Game game) {
        log.info("Saving game = {}", game);
        game = gameDao.saveGame(game);
        log.info("Saving game into cache...");
        cacheManager.store(getGameCacheKey(game.getId()), game, CacheConstants.GAME_TTL);
        cacheManager.store(getGameCacheKey(game.getName()), game, CacheConstants.GAME_TTL);
        cacheManager.delete(getGameListCacheKey());
        return game;
    }

    @Override
    public Game getGame(String gameName) throws NonExistentGameException {
        log.debug("Getting from cache a game with name={}", gameName);
        Game game = (Game) cacheManager.get(getGameCacheKey(gameName));
        if (game == null) {
            game = gameDao.getGame(gameName);
            cacheManager.store(getGameCacheKey(gameName), game, CacheConstants.GAME_TTL);
        }
        return game;
    }

    @Override
    public void updateGame(Game game) {
        gameDao.updateGame(game);
        log.info("Updating Game={} into cache", game);
        cacheManager.store(getGameCacheKey(game.getId()), game, CacheConstants.GAME_TTL);
        cacheManager.store(getGameCacheKey(game.getName()), game, CacheConstants.GAME_TTL);
        cacheManager.delete(getGameListCacheKey());
        cleanGameListCache(game);
    }

    @Override
    public Game getGame(Long gameId) throws NonExistentGameException {
        log.debug("getting game from cache with id={}", gameId);
        Game game = (Game) cacheManager.get(getGameCacheKey(gameId));
        if (game == null) {
            game = gameDao.getGame(gameId);
        }
        return game;

    }

    @Override
    public List<Game> findGamesByProvider(Integer providerId) {
        @SuppressWarnings("unchecked")
        List<Game> games = (List<Game>) cacheManager.get(getGameListCacheKey(providerId));
        if (games == null) {
            games = gameDao.findGamesByProvider(providerId);

            if (games.size() > 0) {
                cacheManager.store(getGameListCacheKey(providerId), new ArrayList<>(games), CacheConstants.PROVIDER_GAME_LIST_TTL);
            }
        }

        return games;
    }

    private String getGameCacheKey(Long gameId) {
        return CacheConstants.GAME_KEY_BY_ID + gameId;
    }

    private void cleanGameListCache(Game game) {
        log.debug("Cleanining game list cache");
        cacheManager.delete(getGameListCacheKey());
        cacheManager.delete(getGameListCacheKey(game.getGameProvider().getId()));
    }

    private String getGameCacheKey(String gameName) {
        return CacheConstants.GAME_KEY + gameName;
    }

    private String getGameListCacheKey(Integer providerId) {
        return CacheConstants.PROVIDER_GAME_LIST_KEY + providerId;
    }

    @Override
    public List<Game> getGames() {
        log.debug("Getting all games from cache.");
        @SuppressWarnings("unchecked")
        List<Game> games = (List<Game>) cacheManager.get(getGameListCacheKey());

        if (games == null) {
            games = gameDao.getGames();

            if (games.size() > 0) {
                cacheManager.store(getGameListCacheKey(), new ArrayList<>(games), CacheConstants.GAME_LIST_TTL);
            }
        }

        return games;
    }

    private String getGameListCacheKey() {
        return CacheConstants.GAME_LIST_KEY;
    }

    public void setGameDao(GameDao gameDao) {
        this.gameDao = gameDao;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

	@Override
	public List<Game> getGamesWithFreeSpins() {
		log.debug("Getting all games from cache.");
        @SuppressWarnings("unchecked")
        List<Game> games = (List<Game>) cacheManager.get(CacheConstants.GAME_FREESPIN_KEY);

        if (games == null) {
            games = gameDao.getGamesWithFreeSpins();

            if (games.size() > 0) {
                cacheManager.store(CacheConstants.GAME_FREESPIN_KEY, new ArrayList<>(games), CacheConstants.GAME_FREESPIN_TTL);
            }
        }

        return games;
	}
}
