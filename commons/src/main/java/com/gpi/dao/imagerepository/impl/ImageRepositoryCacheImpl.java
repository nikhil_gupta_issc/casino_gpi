package com.gpi.dao.imagerepository.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.imagerepository.ImageRepository;
import com.gpi.dto.GameImage;
import com.gpi.dto.GameImageKey;
import com.gpi.dto.GamesImages;
import com.gpi.utils.cache.CacheManager;

public class ImageRepositoryCacheImpl implements ImageRepository {
	
    private static final Logger log = LoggerFactory.getLogger(ImageRepositoryCacheImpl.class);
	
    private ImageRepository imageRepository;
    
    private CacheManager cacheManager;
    
	public ImageRepositoryCacheImpl() throws IOException {
		super();
	}

	@Override
	public GameImage save(GameImageKey key, CommonsMultipartFile file) throws IOException {
		GameImage gameImage = imageRepository.save(key, file);
		cacheManager.store(getCacheKey(key.getKey()), gameImage, CacheConstants.IMAGES_GAMES_MEDIALINK_TTL);
		cacheManager.delete(getCacheListKey());
		return gameImage;
	}
	
	@Override
	public GameImage get(GameImageKey key) {
		GameImage gameImage = (GameImage)cacheManager.getAndTouch(getCacheKey(key.getKey()), CacheConstants.IMAGES_GAMES_MEDIALINK_TTL);		
		
		if (gameImage == null) {
			gameImage = imageRepository.get(key);
			if (gameImage != null) {
				cacheManager.store(getCacheKey(key.getKey()), gameImage, CacheConstants.IMAGES_GAMES_MEDIALINK_TTL);
				cacheManager.delete(getCacheListKey());
			}
		}
		
		return gameImage;
	}
	
	@Override
	public GamesImages getAll() {
		
		GamesImages gamesImages = (GamesImages)cacheManager.getAndTouch(getCacheListKey(), CacheConstants.IMAGES_GAMES_MEDIALINK_TTL);
		
		if (gamesImages == null) {
			gamesImages = imageRepository.getAll();
			if (gamesImages != null) {
				cacheManager.store(getCacheListKey(), gamesImages, CacheConstants.IMAGES_GAMES_MEDIALINK_TTL);
			}
		}
		
		return gamesImages;
	}
	
	private String getCacheKey(String fileName) {
		return CacheConstants.IMAGES_GAMES_MEDILINK_CACHE_KEY + fileName;
	}

	private String getCacheListKey() {
		return CacheConstants.IMAGES_GAMES_MEDILINK_ALL_CACHE_KEY;
	}

	public void setImageRepository(ImageRepository imageRepository) {
		this.imageRepository = imageRepository;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	
	
}
