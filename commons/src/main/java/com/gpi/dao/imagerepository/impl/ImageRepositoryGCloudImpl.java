package com.gpi.dao.imagerepository.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.google.api.gax.paging.Page;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.gpi.dao.imagerepository.ImageRepository;
import com.gpi.dto.GameImage;
import com.gpi.dto.GameImageKey;
import com.gpi.dto.GamesImages;
import com.gpi.utils.ApplicationProperties;

public class ImageRepositoryGCloudImpl implements ImageRepository {
	
    private static final Logger log = LoggerFactory.getLogger(ImageRepositoryGCloudImpl.class);
	
    private Storage storage = null;
    
    private Bucket bucket = null; 
    
	public ImageRepositoryGCloudImpl() throws IOException {
		super();
		try {
			init();
		}catch(Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public GameImage save(GameImageKey key, CommonsMultipartFile file) throws IOException {
		log.debug("Saving file {} in google cloud", key);
		Blob blob = bucket.create(key.getKey(), file.getInputStream());
		log.debug("Done saving file {} in google cloud", key);
		return new GameImage(key, blob.getMediaLink());
	}
	
	@Override
	public GameImage get(GameImageKey key) {
		log.debug("Looking file {} in google cloud", key);
		
		String url = null;
		
		Blob blob = bucket.get(key.getKey());
		if (blob != null) {
			url = blob.getMediaLink();
		}

		log.debug("Done looking file {} in google cloud", key);
		return new GameImage(key, url);
	}
	
	@Override
	public GamesImages getAll() {
		GamesImages gamesImages = new GamesImages();
		Page<Blob> blobs = bucket.list();
		for (Blob blob : blobs.iterateAll()) {
			gamesImages.add(new GameImageKey(blob.getName()), blob.getMediaLink());
		}
		return gamesImages;
	}
	
	private void init() throws Exception {
		String bucketName = ApplicationProperties.getProperty("google.cloud.storage.games.images.bucket");
		
		log.debug("initializing storage for bucket {} in google cloud", bucketName);
		
		storage = StorageOptions.newBuilder()
		          .setCredentials(ServiceAccountCredentials.fromStream(new ClassPathResource(ApplicationProperties.getProperty("google.cloud.storage.credentials.jsonfile")).getInputStream()))
		          .setProjectId(ApplicationProperties.getProperty("google.cloud.storage.project"))
		          .build()
		          .getService();
		
		bucket = storage.get(bucketName, Storage.BucketGetOption.fields());
		
		if (bucket == null) {
			throw new Exception("Can't initialize bucket");
		}
		
		log.debug("Done initializing storage for bucket {} in google cloud", bucketName);
	}	
}
