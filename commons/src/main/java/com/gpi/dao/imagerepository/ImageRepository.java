package com.gpi.dao.imagerepository;

import java.io.IOException;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.gpi.dto.GameImage;
import com.gpi.dto.GameImageKey;
import com.gpi.dto.GamesImages;

public interface ImageRepository {

	GameImage save(GameImageKey key, CommonsMultipartFile file) throws IOException;

	GameImage get(GameImageKey key);
	
	GamesImages getAll();

}