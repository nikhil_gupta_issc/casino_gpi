package com.gpi.dao.imagerepository.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.gpi.dao.imagerepository.ImageRepository;
import com.gpi.dto.GameImage;
import com.gpi.dto.GameImageKey;
import com.gpi.dto.GamesImages;
import com.gpi.utils.ApplicationProperties;

public class ImageRepositoryFileImpl implements ImageRepository {
	
    private static final Logger log = LoggerFactory.getLogger(ImageRepositoryFileImpl.class);
	
	@Override
	public GameImage save(GameImageKey key, CommonsMultipartFile file) throws IOException {

		String imageRepositoryPath = ApplicationProperties.getProperty("application.image.games.repository.path");
		
		BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
		String imagePath = imageRepositoryPath + "/" + key.getKey();
		log.info("Saving game image to " + imagePath);
		File outputFile = new File(imagePath);
		ImageIO.write(bufferedImage, "jpg", outputFile);
		
		return get(key);
	}
	
	@Override
	public GameImage get(GameImageKey key) {
		
		String repositoryPath = ApplicationProperties.getProperty("application.image.games.repository.path");
		String imagePath =  repositoryPath + "/" + key.getKey();
		File file = new File(imagePath);
		if (file.exists()) {
			return new GameImage(key, ApplicationProperties.getProperty("application.secure.image.games.url") + "/" + key.getKey() + "?" + file.lastModified());
		}
		return null;
	}

	@Override
	public GamesImages getAll() {
		//TODO: replaced by google cloud. No need to implement right now
		throw new NotImplementedException();
	}
	
}
