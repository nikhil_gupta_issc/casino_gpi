package com.gpi.dao.spin;

import com.gpi.domain.audit.FreeSpinTransaction;
import com.gpi.domain.audit.Transaction;

import java.util.List;

/**
 * Created by igabba on 05/01/15.
 */
public interface FreeSpinTransactionDao {

    FreeSpinTransaction saveFreeSpinTransaction(FreeSpinTransaction freeSpinTransaction);

    FreeSpinTransaction getFreeSpinTransaction(Long gameTransactionId, Transaction transaction);

    List<FreeSpinTransaction> getFreeSpinTransactions(Long roundId);

}
