package com.gpi.dao.spin.impl;

import com.gpi.dao.spin.FreeSpinTransactionDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.audit.FreeSpinTransaction;
import com.gpi.domain.audit.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by igabba on 05/01/15.
 */
public class FreeSpinTransactionDaoHibernate extends ExtendedHibernateDaoSupport implements FreeSpinTransactionDao {

    private static final Logger logger = LoggerFactory.getLogger(FreeSpinTransactionDaoHibernate.class);

    @Override
    public FreeSpinTransaction saveFreeSpinTransaction(FreeSpinTransaction freeSpinTransaction) {
        logger.debug("Saving FreeSpinTransaction={}", freeSpinTransaction);
        getHibernateSession().save(freeSpinTransaction);
        return freeSpinTransaction;
    }

    @Override
    public FreeSpinTransaction getFreeSpinTransaction(Long gameTransactionId, Transaction transaction) {
        logger.debug("Retrieving FreeSpinTransaction for gameTransactionId={} and transaction={}", gameTransactionId, transaction.getId());
       return (FreeSpinTransaction) getHibernateSession().createQuery("from FreeSpinTransaction where gameTransactionId=:gameTransactionId and transaction.gameTransactionId=:transaction")
                .setLong("gameTransactionId", gameTransactionId).setLong("transaction", transaction.getId()).uniqueResult();
    }

    @Override
    public List<FreeSpinTransaction> getFreeSpinTransactions(Long roundId) {
        return getHibernateSession().createQuery(" from FreeSpinTransaction where transaction.round.id=:roundId").
                setLong("roundId",roundId).list();
    }

}
