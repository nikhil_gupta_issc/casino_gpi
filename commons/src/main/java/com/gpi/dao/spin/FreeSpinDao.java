package com.gpi.dao.spin;


import java.util.List;

import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;

public interface FreeSpinDao {

    FreeSpin findFreeSpinById(Long id);

    void saveOrUpdateFreeSpin(FreeSpin fs);

    List<FreeSpin> findFreeSpinByPlayerAndGame(Player player, Game game);

	List<FreeSpin> findFreeSpinByPlayer(Player player);
}
