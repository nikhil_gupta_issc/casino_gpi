package com.gpi.dao.spin.impl;

import java.util.List;

import com.gpi.dao.spin.FreeSpinDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;

public class FreeSpinDaoHibernate extends ExtendedHibernateDaoSupport implements FreeSpinDao {

    @Override
    public List<FreeSpin> findFreeSpinByPlayerAndGame(Player player, Game game) {
    	List<FreeSpin> freeSpin = (List<FreeSpin>) getHibernateSession().createQuery("from FreeSpin where player.id = :id and amountLeft > 0 and game.id = :gameId order by id")
                .setParameter("id", player.getId()).setParameter("gameId", game.getId()).list();

        return freeSpin;
    }
    
    @Override
    public List<FreeSpin> findFreeSpinByPlayer(Player player) {
        List<FreeSpin> freeSpins = getHibernateSession().createQuery("from FreeSpin where player.id = :id and amountLeft > 0")
                .setParameter("id", player.getId()).list();
        return freeSpins;
    }

    @Override
    public void saveOrUpdateFreeSpin(FreeSpin fs) {
        getHibernateSession().saveOrUpdate(fs);

    }

    @Override
    public FreeSpin findFreeSpinById(Long id) {
        return (FreeSpin) getHibernateSession().createQuery("from FreeSpin where id = :id").setParameter("id", id).uniqueResult();
    }

}
