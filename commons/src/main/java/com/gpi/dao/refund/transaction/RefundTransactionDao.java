package com.gpi.dao.refund.transaction;

import com.gpi.domain.audit.RefundedTransaction;
import com.gpi.domain.audit.Transaction;

import java.util.List;

/**
 * Created by Nacho on 12/2/2014.
 */
public interface RefundTransactionDao {

    RefundedTransaction saveRefundedTransaction(RefundedTransaction refundedTransaction);

    RefundedTransaction getRefundedTransaction(String gameTransactionId, Transaction transaction);

    List<RefundedTransaction> getRefundedTransactions(Long roundId);
}
