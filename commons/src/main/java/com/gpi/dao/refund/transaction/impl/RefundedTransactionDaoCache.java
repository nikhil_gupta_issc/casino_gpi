package com.gpi.dao.refund.transaction.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.dao.refund.transaction.RefundTransactionDao;
import com.gpi.domain.audit.RefundedTransaction;
import com.gpi.domain.audit.Transaction;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Nacho on 12/2/2014.
 */
public class RefundedTransactionDaoCache implements RefundTransactionDao {

    private static final Logger logger = LoggerFactory.getLogger(RefundedTransactionDaoCache.class);
    private CacheManager cacheManager;
    private RefundTransactionDao refundTransactionDao;

    @Override
    public RefundedTransaction saveRefundedTransaction(RefundedTransaction refundedTransaction) {
        logger.debug("Saving refundedTransaction={}", refundedTransaction);
        refundedTransaction = refundTransactionDao.saveRefundedTransaction(refundedTransaction);
        cacheManager.store(getKey(refundedTransaction.getGameTransactionId(), refundedTransaction.getTransaction()), refundedTransaction, CacheConstants.REFUNDED_TRANSACTION_TTL);
        return null;
    }

    private String getKey(String gameTransactionId, Transaction transaction) {
        return String.format("%s%s_%s", CacheConstants.REFUNDED_TRANSACTION_KEY, gameTransactionId, transaction.getId());
    }

    @Override
    public RefundedTransaction getRefundedTransaction(String gameTransactionId, Transaction transaction) {
        logger.debug("Getting refundedTransaction with gameTransactionId={} and transaction={}", gameTransactionId, transaction.getId());
        RefundedTransaction refundedTransaction = (RefundedTransaction) cacheManager.get(getKey(gameTransactionId, transaction));
        if (refundedTransaction == null) {
            refundedTransaction = refundTransactionDao.getRefundedTransaction(gameTransactionId, transaction);
            if (refundedTransaction != null) {
                cacheManager.store(getKey(gameTransactionId, transaction), refundedTransaction, CacheConstants.REFUNDED_TRANSACTION_TTL);
            }
        }
        return refundedTransaction;
    }

    @Override
    public List<RefundedTransaction> getRefundedTransactions(Long roundId) {
        logger.debug("This method wasn't implemented");
        return refundTransactionDao.getRefundedTransactions(roundId);
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public void setRefundTransactionDao(RefundTransactionDao refundTransactionDao) {
        this.refundTransactionDao = refundTransactionDao;
    }
}
