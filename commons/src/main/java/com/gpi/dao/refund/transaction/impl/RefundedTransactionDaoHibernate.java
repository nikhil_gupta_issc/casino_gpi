package com.gpi.dao.refund.transaction.impl;

import com.gpi.dao.refund.transaction.RefundTransactionDao;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.domain.audit.RefundedTransaction;
import com.gpi.domain.audit.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Nacho on 12/2/2014.
 */
public class RefundedTransactionDaoHibernate extends ExtendedHibernateDaoSupport implements RefundTransactionDao {

    private static final Logger logger = LoggerFactory.getLogger(RefundedTransactionDaoHibernate.class);

    @Override
    public RefundedTransaction saveRefundedTransaction(RefundedTransaction refundedTransaction) {
        logger.debug("Saving refundedTransaction={}", refundedTransaction);
        getHibernateSession().save(refundedTransaction);
        return refundedTransaction;
    }

    @Override
    public RefundedTransaction getRefundedTransaction(String gameTransactionId, Transaction transaction) {
        logger.debug("Getting refundedTransaction with gameTransactionId={} and transaction={}", gameTransactionId, transaction.getId());
        return (RefundedTransaction) getHibernateSession().createQuery(" from RefundedTransaction where gameTransactionId=:gameTransaction and transaction.id=:transactionId").
                setString("gameTransaction", gameTransactionId).setLong("transactionId", transaction.getId()).uniqueResult();
    }

    @Override
    public List<RefundedTransaction> getRefundedTransactions(Long roundId) {
        return getHibernateSession().createQuery(" from RefundedTransaction where round.id=:roundId").
                setLong("roundId",roundId).list();
    }
}
