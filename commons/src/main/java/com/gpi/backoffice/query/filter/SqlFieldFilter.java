package com.gpi.backoffice.query.filter;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import java.util.Collections;
import java.util.Map;

public class SqlFieldFilter extends FieldFilter implements HibernateFilter {
    private String sql;

    public SqlFieldFilter(String sql) {
        this.sql = sql;
    }

    @Override
    public Map<String, String> getAlias() {
        return Collections.emptyMap();
    }

    @Override
    public Criterion generateCriterion(String entityAlias) {
        return Restrictions.sqlRestriction(sql);
    }
}
