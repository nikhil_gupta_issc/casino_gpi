package com.gpi.backoffice.query.impl;

import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.HibernateFilter;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * A special implementation which retrieves the correct number of entities when those entities have collections
 * which are retrieved eagerly
 *
 * @param <T> The class of an entity currently handled by Hibernate
 * @author csanchez
 */
public class EagerQueryObjectHibernate<T> extends QueryObjectHibernate<T> implements QueryObject<T> {

    private String idProperty = "id";

    public EagerQueryObjectHibernate() {
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<T> get(String sortExpression, List<HibernateFilter> filters) {

        Set<String> aliasId = new HashSet<String>();
        for (HibernateFilter filter : filters) {
            aliasId.addAll(filter.getAlias().keySet());
        }

        Criteria idCriteria = getHibernateSession().createCriteria(argumentType, "root_entity");

        applySortExpression(idCriteria, aliasId, sortExpression);
        applyFilters(idCriteria, filters);

        idCriteria.setProjection(Projections.id());

        Collection ids = idCriteria.list();
        List<T> results = Collections.emptyList();

        if (!ids.isEmpty()) {
            Set<String> alias = new HashSet<String>();
            Criteria criteria = getHibernateSession().createCriteria(argumentType, "root_entity");
            criteria.add(Restrictions.in(this.idProperty, ids));
            applySortExpression(criteria, alias, sortExpression);
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            results = criteria.list();
        }

        return results;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<T> get(int startIndex, int pageSize, String sortExpression, List<HibernateFilter> filters) {

        Set<String> aliasId = new HashSet<String>();
        for (HibernateFilter filter : filters) {
            aliasId.addAll(filter.getAlias().keySet());
        }

        Criteria idCriteria = getHibernateSession().createCriteria(argumentType, "root_entity");

        applySortExpression(idCriteria, aliasId, sortExpression);
        applyFilters(idCriteria, filters);

        idCriteria.setFirstResult(startIndex).setMaxResults(pageSize);

        idCriteria.setProjection(Projections.id());

        Collection ids = idCriteria.list();
        List<T> results = Collections.emptyList();

        if (!ids.isEmpty()) {
            Set<String> alias = new HashSet<String>();
            Criteria criteria = getHibernateSession().createCriteria(argumentType, "root_entity");
            criteria.add(Restrictions.in(this.idProperty, ids));
            applySortExpression(criteria, alias, sortExpression);
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            results = criteria.list();
        }

        return results;
    }

    /**
     * @param idProperty the idProperty to set
     */
    public void setIdProperty(String idProperty) {
        this.idProperty = idProperty;
    }
}
