package com.gpi.backoffice.query.filter;

import org.hibernate.criterion.Criterion;

import java.util.Map;

public interface HibernateFilter {

	Map<String, String> getAlias();

	Criterion generateCriterion(String entityAlias);
}
