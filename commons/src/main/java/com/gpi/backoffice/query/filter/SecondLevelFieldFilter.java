package com.gpi.backoffice.query.filter;

import org.hibernate.criterion.Criterion;

import java.util.HashMap;
import java.util.Map;

public class SecondLevelFieldFilter extends FieldFilter implements HibernateFilter {
    private String firstLevelField;
    private String secondLevelField;
    private FilterOperator filter;
    private Object value;

    public SecondLevelFieldFilter(String firstLevelField, String secondLevelField, FilterOperator filter, Object value) {
        this.firstLevelField = firstLevelField;
        this.secondLevelField = secondLevelField;
        this.filter = filter;
        this.value = value;
    }

    @Override
    public Map<String, String> getAlias() {
        Map<String, String> aliases = new HashMap<>();
        aliases.put(firstLevelField, firstLevelField.toLowerCase());
        return aliases;
    }

    @Override
    public Criterion generateCriterion(String entityAlias) {
        String fieldPath = firstLevelField.toLowerCase() + "." + secondLevelField;
        return applyFilter(fieldPath, filter, value);
    }

}
