package com.gpi.backoffice.query;


import com.gpi.backoffice.query.filter.HibernateFilter;

import java.util.List;

public interface QueryObject<T> {


	/**
	 * Retrieves a list of objects from the database
	 *
	 * @return a list of objects
	 */
	List<T> get(String sortExpression, List<HibernateFilter> filters);

	/**
	 * Retrieves a list of objects from the database
	 *
	 * @param startIndex the result set will start from this position
	 * @param pageSize   the number of objects that will be retrieved
	 * @return a list of objects
	 */
	public List<T> get(int startIndex, int pageSize, String sortExpression, List<HibernateFilter> filters);

	/**
	 * Retrieves the total of objects that can be retrieved by the query object
	 */
	public Long getCount(List<HibernateFilter> filters);

}
