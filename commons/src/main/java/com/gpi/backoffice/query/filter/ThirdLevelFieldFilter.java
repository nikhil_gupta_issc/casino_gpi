package com.gpi.backoffice.query.filter;

import org.hibernate.criterion.Criterion;

import java.util.LinkedHashMap;
import java.util.Map;

public class ThirdLevelFieldFilter extends FieldFilter implements HibernateFilter {
    private String firstLevelField;
    private String secondLevelField;
    private String thirdLevelField;
    private FilterOperator filter;
    private Object value;

    public ThirdLevelFieldFilter(String firstLevelField, String secondLevelField, String thirdLevelField, FilterOperator filter, Object value) {
        this.firstLevelField = firstLevelField;
        this.secondLevelField = secondLevelField;
        this.thirdLevelField = thirdLevelField;
        this.filter = filter;
        this.value = value;
    }

    @Override
    public Map<String, String> getAlias() {
        Map<String, String> aliases = new LinkedHashMap<>();
        aliases.put(firstLevelField, firstLevelField.toLowerCase());
        aliases.put(firstLevelField.toLowerCase() + "." + secondLevelField, secondLevelField.toLowerCase());
        return aliases;
    }

    @Override
    public Criterion generateCriterion(String entityAlias) {
        String fieldPath = secondLevelField.toLowerCase() + "." + thirdLevelField;
        return applyFilter(fieldPath, filter, value);
    }

}
