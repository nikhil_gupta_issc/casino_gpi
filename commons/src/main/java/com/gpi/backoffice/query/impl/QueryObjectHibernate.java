package com.gpi.backoffice.query.impl;

import com.gpi.backoffice.query.QueryObject;
import com.gpi.backoffice.query.filter.HibernateFilter;
import com.gpi.dao.utils.ExtendedHibernateDaoSupport;
import com.gpi.exceptions.InvalidFieldException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QueryObjectHibernate<T> extends ExtendedHibernateDaoSupport implements QueryObject<T> {

	private static final String SORT_EXP_REGEX = "([\\w.]+) (ASC|DESC)";
	private static final Pattern SORT_EXP_PATTERN = Pattern.compile(SORT_EXP_REGEX);

	protected Class<T> argumentType;

	public QueryObjectHibernate() {
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	@SuppressWarnings("unchecked")
	public List<T> get(String sortExpression, List<HibernateFilter> filters) {
		Set<String> alias = new HashSet<String>();

		Criteria criteria = getHibernateSession().createCriteria(argumentType, "root_entity");

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		applySortExpression(criteria, alias, sortExpression);
		applyFilters(criteria, filters);

		List<T> results = criteria.list();
		return results;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	@SuppressWarnings("unchecked")
	public List<T> get(int startIndex, int pageSize, String sortExpression, List<HibernateFilter> filters) {
		Set<String> alias = new HashSet<String>();

		Criteria criteria = getHibernateSession().createCriteria(argumentType, "root_entity");

		criteria.setFirstResult(startIndex)
				.setMaxResults(pageSize);

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		applySortExpression(criteria, alias, sortExpression);
		applyFilters(criteria, filters);

		List<T> results = criteria.list();
		return results;
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public Long getCount(List<HibernateFilter> filters) {
		Criteria criteria = getHibernateSession().createCriteria(argumentType);
		applyFilters(criteria, filters);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	/**
	 * Parses the sort expression specified and uses it to create the proper
	 * order settings in a criteria
	 *
	 * @param criteria
	 * @throws InvalidFieldException when one of the fields specified in the expression doesn't belong
	 *                               to the class
	 */
	protected void applySortExpression(Criteria criteria, Set<String> aliasSet, String sortExpression) {
		if (StringUtils.isEmpty(sortExpression)) {
			return;
		}

		Matcher matcher = SORT_EXP_PATTERN.matcher(sortExpression);

		while (matcher.find()) {
			String fieldName = matcher.group(1);
			if (!isValidField(fieldName)) {
				throw new InvalidFieldException(fieldName + " is not a valid field of " + argumentType);
			}

			String[] filedArray = fieldName.split("\\.");
			String path = "";
			for (int i = 1; i < filedArray.length; i++) {
				path = path + filedArray[i - 1];
				if (!aliasSet.contains(path)) {
					criteria.createAlias(path, filedArray[i - 1], JoinType.LEFT_OUTER_JOIN);
					aliasSet.add(filedArray[i - 1]);
				}
				path = path + ".";
			}

			String entity = "";
			if (filedArray.length > 1)
				entity = filedArray[filedArray.length - 2] + ".";

			String field = filedArray[filedArray.length - 1];

			if (matcher.group(2).equals("ASC")) {
				criteria.addOrder(Order.asc(entity + field));
			} else {
				criteria.addOrder(Order.desc(entity + field));
			}
		}
	}

	/**
	 * Verifies that a field belongs to class provided as argument type
	 *
	 * @param fieldName the field to validate
	 * @return true if the field was found, false otherwise
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	protected boolean isValidField(String fieldName) {

		String[] fieldNameArray = fieldName.split("\\.");

		if (fieldNameArray.length > 1) {
			for (String element : fieldNameArray) {
				Class klass;
				try {
					klass = Class.forName("com.igs.bingo.backoffice.domain." + element.toLowerCase() + "." + WordUtils.capitalize(element));
				} catch (ClassNotFoundException e) {
					try {
						klass = Class.forName("com.igs.bingo.common.domain." + element.toLowerCase() + "." + WordUtils.capitalize(element));
					} catch (ClassNotFoundException ex) {
						try {
							klass = Class.forName("com.gpi.domain." + element.toLowerCase() + "." + WordUtils.capitalize(element));
						} catch (ClassNotFoundException ex2) {
							try {
								klass = Class.forName("com.gpi.domain." + element.split("(?=\\p{Upper})")[0] + "." + WordUtils.capitalize(element));
							} catch (ClassNotFoundException ex3) {
								return false;
							}
						}
					}

				}
				QueryObjectHibernate q = new QueryObjectHibernate();
				q.setArgumentType(klass);
				return q.isValidField(fieldName.replace(element + ".", ""));
			}
			return false;
		} else {
			Field[] fields = argumentType.getDeclaredFields();

			for (Field field : fields) {
				if (field.getName().equals(fieldName)) {
					return true;
				}
			}
			return false;
		}
	}

	protected void applyFilters(Criteria criteria, List<HibernateFilter> filters) {
		Map<String, String> aliasMap = new HashMap<String, String>();

		//Gather the required alias
		for (HibernateFilter filter : filters) {
			aliasMap.putAll(filter.getAlias());
		}

		//Alias creation
		for (Entry<String, String> entry : aliasMap.entrySet()) {
			criteria.createAlias(entry.getKey(), entry.getValue(), JoinType.LEFT_OUTER_JOIN);
		}

		//The filters are applied
		for (HibernateFilter filter : filters) {
			criteria.add(filter.generateCriterion(criteria.getAlias()));
		}
	}


	public void setArgumentType(Class<T> argumentType) {
		this.argumentType = argumentType;
	}
}
