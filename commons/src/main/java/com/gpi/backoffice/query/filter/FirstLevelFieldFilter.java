package com.gpi.backoffice.query.filter;

import org.hibernate.criterion.Criterion;

import java.util.Collections;
import java.util.Map;

public class FirstLevelFieldFilter extends FieldFilter implements HibernateFilter {
    private String fieldName;
    private FilterOperator filter;
    private Object value;

    public FirstLevelFieldFilter(String fieldName, FilterOperator filter, Object value) {
        this.fieldName = fieldName;
        this.filter = filter;
        this.value = value;
    }

    @Override
    public Map<String, String> getAlias() {
        return Collections.emptyMap();
    }

    @Override
    public Criterion generateCriterion(String entityAlias) {
        return applyFilter(fieldName, filter, value);
    }
}
