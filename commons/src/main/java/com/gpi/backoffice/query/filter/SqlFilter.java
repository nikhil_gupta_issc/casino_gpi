package com.gpi.backoffice.query.filter;

public interface SqlFilter {

    /**
     * This method should return a where clause.
     *
     * @return
     */
    String getFilter();
}
