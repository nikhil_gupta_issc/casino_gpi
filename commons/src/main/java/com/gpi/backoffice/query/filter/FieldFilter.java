package com.gpi.backoffice.query.filter;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

public class FieldFilter {

    public Criterion applyFilter(String field, FilterOperator filter, Object value) {
        Criterion criterion = null;
        switch (filter) {
            case LIKE:
                criterion = Restrictions.like(field, value.toString(), MatchMode.ANYWHERE);
                break;
            case EQ:
                criterion = Restrictions.eq(field, value);
                break;
            case GT:
                criterion = Restrictions.gt(field, value);
                break;
            case GE:
                criterion = Restrictions.ge(field, value);
                break;
            case LT:
                criterion = Restrictions.lt(field, value);
                break;
            case LE:
                criterion = Restrictions.le(field, value);
                break;
            case NEQ:
                criterion = Restrictions.ne(field, value);
                break;
            case IN:
                criterion = Restrictions.in(field, (Object[]) value);
                break;
            default:
                throw new IllegalStateException("Filter operator not supported: " + filter);
        }

        return criterion;
    }

}
