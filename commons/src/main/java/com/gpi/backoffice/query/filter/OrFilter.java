package com.gpi.backoffice.query.filter;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import java.util.Map;

public class OrFilter implements HibernateFilter {

    private HibernateFilter filter1;

    private HibernateFilter filter2;

    public OrFilter(HibernateFilter filter1, HibernateFilter filter2) {
        this.filter1 = filter1;
        this.filter2 = filter2;
    }

    @Override
    public Map<String, String> getAlias() {
        return filter1.getAlias();
    }

    @Override
    public Criterion generateCriterion(String entityAlias) {
        return Restrictions.or(filter1.generateCriterion(entityAlias), filter2.generateCriterion(entityAlias));
    }
}
