package com.gpi.backoffice.query.filter;

public enum FilterOperator {
    LIKE, EQ, GT, GE, LT, LE, NEQ, IN
}
