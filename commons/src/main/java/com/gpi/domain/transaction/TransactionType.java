package com.gpi.domain.transaction;

/**
 * Represents a TransactionType
 */
public enum TransactionType {

    BET(1, "BET"),
    WIN(2, "WIN"),
    BET_CORRECTION(3, "BET_CORRECTION"),
    WIN_CORRECTION(4, "WIN_CORRECTION");

    private final Integer id;
    private final String description;

    private TransactionType(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
