package com.gpi.domain.transaction;

public class TransactionFilter{
	
	private Integer startIndex;
	
	private Integer pageSize;
	
	private String sortExpression;
	
	private String playerName;
	
	private String playerPublisherCode;
	
	private String providerTransactionId;
	
	public TransactionFilter() {
		super();
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPlayerPublisherCode() {
		return playerPublisherCode;
	}

	public void setPlayerPublisherCode(String playerPublisherCode) {
		this.playerPublisherCode = playerPublisherCode;
	}

	public String getProviderTransactionId() {
		return providerTransactionId;
	}

	public void setProviderTransactionId(String providerTransactionId) {
		this.providerTransactionId = providerTransactionId;
	}

	public Integer getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortExpression() {
		return sortExpression;
	}

	public void setSortExpression(String sortExpression) {
		this.sortExpression = sortExpression;
	}

	@Override
	public String toString() {
		return "TransactionFilter [startIndex=" + startIndex + ", pageSize=" + pageSize + ", sortExpression="
				+ sortExpression + ", playerName=" + playerName + ", playerPublisherCode=" + playerPublisherCode
				+ ", providerTransactionId=" + providerTransactionId + "]";
	}
	
	
	
}