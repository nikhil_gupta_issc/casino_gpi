package com.gpi.domain.transaction;

public class TransactionFilterBuilder{
	
	private TransactionFilter transactionFilter;
	
	private TransactionFilterBuilder() {
		super();
		transactionFilter = new TransactionFilter();
	}
	
	public static TransactionFilterBuilder create() {
		return new TransactionFilterBuilder();
	}
	
	public TransactionFilterBuilder setPlayerName(String playerName) {
		this.transactionFilter.setPlayerName(playerName);
		return this;
	}

	public TransactionFilterBuilder setPlayerPublisherCode(String playerPublisherCode) {
		this.transactionFilter.setPlayerPublisherCode(playerPublisherCode);
		return this;
	}

	public TransactionFilterBuilder setProviderTransactionId(String providerTransactionId) {
		this.transactionFilter.setProviderTransactionId(providerTransactionId);
		return this;
	}

	public TransactionFilterBuilder setStartIndex(Integer startIndex) {
		this.transactionFilter.setStartIndex(startIndex);
		return this;
	}

	public TransactionFilterBuilder setPageSize(Integer pageSize) {
		this.transactionFilter.setPageSize(pageSize);
		return this;
	}

	public TransactionFilterBuilder setSortExpression(String sortExpression) {
		this.transactionFilter.setSortExpression(sortExpression);
		return this;
	}
	
	public TransactionFilter build() {
		return transactionFilter;
	}

	
	
	
}