package com.gpi.domain.publisher;

import com.gpi.domain.currency.Currency;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Index;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Nacho on 1/20/14.
 */
@Entity
@Table(name = "publisher_stats_monthly", uniqueConstraints = @UniqueConstraint(name = "UQ_STATS_MONTH", columnNames = {"publisher_id", "player_id", "game_id",
        "currency", "created_on"}))
public class PublisherStatsMonthly implements Serializable {

    private static final long serialVersionUID = -8217758469446395378L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "created_on")
    @Temporal(TemporalType.DATE)
    private Date createdOn;

    @Index(name = "publisher_id_index")
    @Column(name = "publisher_id")
    private Integer publisherId;

    @Index(name = "game_id_index")
    @Column(name = "game_id")
    private Long gameId;

    @Index(name = "player_id_index")
    @Column(name = "player_id")
    private Long playerId;

    @Column(name = "currency")
    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Column(name = "win_amount")
    private Long winAmount = 0L;

    @Column(name = "bet_amount")
    private Long betAmount = 0L;

    @Column(name = "bingos")
    private Integer bingos = 0;

    @Column(name = "jackpots")
    private Integer jackpots = 0;

    @Column(name = "rounds")
    private Integer rounds = 0;

    private transient Long winAmountDelta = 0L;

    private transient Long betAmountDelta = 0L;

    private transient Integer bingosDelta = 0;

    private transient Integer jackpotsDelta = 0;

    private transient Integer roundsDelta = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Long getWinAmount() {
        return winAmount;
    }

    public void setWinAmount(Long winAmount) {
        this.winAmount += winAmount;
        this.winAmountDelta = winAmount;
    }

    public Long getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(Long betAmount) {
        this.betAmount += betAmount;
        this.betAmountDelta = betAmount;
    }

    public Integer getBingos() {
        return bingos;
    }

    public void setBingos(Integer bingos) {
        this.bingos += bingos;
        this.bingosDelta = bingos;
    }

    public Integer getJackpots() {
        return jackpots;
    }

    public void addJackpots(Integer jackpots) {
        this.jackpots += jackpots;
        this.jackpotsDelta = jackpots;
    }

    public Integer getRounds() {
        return rounds;
    }

    public void setRounds(Integer rounds) {
        this.rounds += rounds;
        this.roundsDelta = rounds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("createdOn", createdOn)
                .append("publisherId", publisherId)
                .append("gameId", gameId)
                .append("currency", currency.getDescription())
                .append("win_amount", winAmount)
                .append("bet_amount", betAmount)
                .append("bingos", bingos)
                .append("jackpots", jackpots)
                .append("rounds", rounds)
                .toString();
    }

    public Long getWinAmountDelta() {
        return winAmountDelta;
    }

    public Long getBetAmountDelta() {
        return betAmountDelta;
    }

    public Integer getBingosDelta() {
        return bingosDelta;
    }

    public Integer getJackpotsDelta() {
        return jackpotsDelta;
    }

    public Integer getRoundsDelta() {
        return roundsDelta;
    }

    /**
     * @return the playerId
     */
    public Long getPlayerId() {
        return playerId;
    }

    /**
     * @param playerId the playerId to set
     */
    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    /**
     * @return the publisherId
     */
    public Integer getPublisherId() {
        return publisherId;
    }

    /**
     * @param publisherId the publisherId to set
     */
    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    /**
     * @return the gameId
     */
    public Long getGameId() {
        return gameId;
    }

    /**
     * @param gameId the gameId to set
     */
    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
        result = prime * result + ((gameId == null) ? 0 : gameId.hashCode());
        result = prime * result + ((playerId == null) ? 0 : playerId.hashCode());
        result = prime * result + ((publisherId == null) ? 0 : publisherId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PublisherStatsMonthly other = (PublisherStatsMonthly) obj;
        if (createdOn == null) {
            if (other.createdOn != null)
                return false;
        } else if (!isSameMonth(createdOn, other.createdOn))
            return false;
        if (gameId == null) {
            if (other.gameId != null)
                return false;
        } else if (!gameId.equals(other.gameId))
            return false;
        if (playerId == null) {
            if (other.playerId != null)
                return false;
        } else if (!playerId.equals(other.playerId))
            return false;
        if (publisherId == null) {
            if (other.publisherId != null)
                return false;
        } else if (!publisherId.equals(other.publisherId))
            return false;
        return true;
    }

    private boolean isSameMonth(Date date, Date other) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(other);
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH));
    }
}


