package com.gpi.domain.publisher;

import com.gpi.domain.currency.Currency;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Index;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Nacho on 1/20/14.
 */
@Entity
@Table(name = "publisher_stats_daily", uniqueConstraints = @UniqueConstraint(name = "UQ_STATS_DAILY", columnNames = {"publisher_id", "player_id", "game_id",
        "currency", "created_on"}))
public class PublisherStatsDaily implements Serializable {

    private static final long serialVersionUID = 4321246337720728747L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "created_on")
    @Temporal(TemporalType.DATE)
    private Date createdOn;

    @Index(name = "publisher_id_index")
    @Column(name = "publisher_id")
    private Integer publisherId;

    @Index(name = "game_id_index")
    @Column(name = "game_id")
    private Long gameId;

    @Index(name = "player_id_index")
    @Column(name = "player_id")
    private Long playerId;

    @Column(name = "currency")
    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Column(name = "win_amount")
    private Long winAmount = 0L;

    @Column(name = "bet_amount")
    private Long betAmount = 0L;

    @Column(name = "bingos")
    private Integer bingos = 0;

    @Column(name = "jackpots")
    private Integer jackpots = 0;

    @Column(name = "rounds")
    private Integer rounds = 0;

    private transient Long winAmountDelta = (long) 0;

    private transient Long betAmountDelta = (long) 0;

    private transient Integer bingosDelta = 0;

    private transient Integer jackpotsDelta = 0;

    private transient Integer roundsDelta = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Long getWinAmount() {
        return winAmount;
    }

    public void addWinAmount(Long winAmount) {
        this.winAmount += winAmount;
        this.winAmountDelta = winAmount;
    }

    public Long getBet_amount() {
        return betAmount;
    }

    public void addBetAmount(Long betAmount) {
        this.betAmount += betAmount;
        this.betAmountDelta = betAmount;
    }

    public Integer getBingos() {
        return bingos;
    }

    public void addBingos(Integer bingos) {
        this.bingos += bingos;
        this.bingosDelta = bingos;
    }

    public Integer getJackpots() {
        return jackpots;
    }

    public void addJackpots(Integer jackpots) {
        this.jackpots += jackpots;
        this.jackpotsDelta = jackpots;
    }

    public Integer getRounds() {
        return rounds;
    }

    public void addRounds(Integer rounds) {
        this.rounds += rounds;
        this.roundsDelta = rounds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("createdOn", createdOn).append("publisherId", publisherId)
                .append("gameId", gameId).append("currency", currency.getDescription()).append("win_amount", winAmount)
                .append("bet_amount", betAmount).append("bingos", bingos).append("jackpots", jackpots).append("rounds", rounds).toString();
    }

    public Long getWinAmountDelta() {
        return winAmountDelta;
    }

    public Long getBetAmountDelta() {
        return betAmountDelta;
    }

    public Integer getBingosDelta() {
        return bingosDelta;
    }

    public Integer getJackpotsDelta() {
        return jackpotsDelta;
    }

    public Integer getRoundsDelta() {
        return roundsDelta;
    }

    /**
     * @return the playerId
     */
    public Long getPlayerId() {
        return playerId;
    }

    /**
     * @param playerId the playerId to set
     */
    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    /**
     * @return the publisherId
     */
    public Integer getPublisherId() {
        return publisherId;
    }

    /**
     * @param publisherId the publisherId to set
     */
    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    /**
     * @return the gameId
     */
    public Long getGameId() {
        return gameId;
    }

    /**
     * @param gameId the gameId to set
     */
    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PublisherStatsDaily that = (PublisherStatsDaily) o;

        if (!createdOn.equals(that.createdOn)) return false;
        if (currency != that.currency) return false;
        if (!gameId.equals(that.gameId)) return false;
        if (!playerId.equals(that.playerId)) return false;
        if (!publisherId.equals(that.publisherId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = createdOn.hashCode();
        result = 31 * result + publisherId.hashCode();
        result = 31 * result + gameId.hashCode();
        result = 31 * result + playerId.hashCode();
        result = 31 * result + currency.hashCode();
        return result;
    }
}
