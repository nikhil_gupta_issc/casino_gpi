package com.gpi.domain.publisher;

import java.io.Serializable;

/**
 * Publisher's specific parameters required for the communication between our
 * games and their platform
 *
 * @author csanchez
 */
public class GameParameters implements Serializable {

    private static final long serialVersionUID = 4327342721675234565L;

    /**
     * The unique's id used by the {@link Publisher} to identify its players
     */
    private String publisherPlayerId;
    private String currency;

    /**
     * @return the publisherPlayerId
     */
    public String getPublisherPlayerId() {
        return publisherPlayerId;
    }

    /**
     * @param publisherPlayerId the publisherPlayerId to set
     */
    public void setPublisherPlayerId(String publisherPlayerId) {
        this.publisherPlayerId = publisherPlayerId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "GameParameters [publisherPlayerId=" + publisherPlayerId + "]";
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
