package com.gpi.domain.publisher;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Represents a game provider. For example virtue fusion.
 */
@Entity
@Table(name = "publisher")
public class Publisher implements Serializable {

    private static final long serialVersionUID = 7164305044709074379L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "api_username", nullable = false, length = 100)
    private String apiUsername;

    @Column(name = "api_name", nullable = false, length = 100)
    private String apiName;

    @Column(name = "password", nullable = false, length = 100)
    private String password;

    @Column(name = "url", nullable = false, length = 100)
    private String url;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;
    
    @Column(name = "owner_id", nullable = false)
    private String ownerId;
    
    @Column(name = "ortiz_casino", nullable = false)
    private String ortizCasinoNumber;
    
    @Column(name = "partner_code", nullable = false)
    private String partnerCode;
    
    @Column(name = "support_zero_bet", nullable = false)
    private Boolean supportZeroBet;
    
    @Column(name = "support_zero_win", nullable = false)
    private Boolean supportZeroWin;


    public Publisher() {
    }

    public Publisher(String name, String username, String api, String password, String url, String ownerId, String ortizCasinoNumber, String partnerCode, Boolean supportZeroBet, Boolean supportZeroWin) {
        this.name = name;
        this.apiUsername = username;
        this.password = password;
        this.url = url;
        this.apiName = api;
        this.createdOn = new Date();
        this.ownerId = ownerId;
        this.ortizCasinoNumber= ortizCasinoNumber;
        this.partnerCode = partnerCode;
        this.supportZeroBet = supportZeroBet;
        this.supportZeroWin = supportZeroWin;
    }

    public Publisher(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * @return the partner code
     */
    public String getPartnerCode() {
		return partnerCode;
	}

    /**
     * @param partnerCode the partner code to set
     */
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

    @Override
	public String toString() {
		return "Publisher [id=" + id + ", name=" + name + ", apiUsername=" + apiUsername + ", apiName=" + apiName
				+ ", password=" + password + ", url=" + url + ", createdOn=" + createdOn + ", ownerId=" + ownerId
				+ ", ortizCasinoNumber=" + ortizCasinoNumber + ", partnerCode=" + partnerCode + ", supportZeroBet="
				+ supportZeroBet + ", supportZeroWin=" + supportZeroWin + "]";
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Publisher publisher = (Publisher) o;

        if (id != null ? !id.equals(publisher.id) : publisher.id != null) return false;
        return name.equals(publisher.name);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        return result;
    }

    public String getApiUsername() {
        return apiUsername;
    }

    public void setApiUsername(String apiUsername) {
        this.apiUsername = apiUsername;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * @return the ortizCasinoNumber
	 */
	public String getOrtizCasinoNumber() {
		return ortizCasinoNumber;
	}

	/**
	 * @param ortizCasinoNumber the ortizCasinoNumber to set
	 */
	public void setOrtizCasinoNumber(String ortizCasinoNumber) {
		this.ortizCasinoNumber = ortizCasinoNumber;
	}

	public Boolean getSupportZeroBet() {
		return supportZeroBet;
	}

	/**
	 * @param supportZeroBet the supportZeroBet to set
	 */
	public void setSupportZeroBet(Boolean supportZeroBet) {
		this.supportZeroBet = supportZeroBet;
	}

	public Boolean getSupportZeroWin() {
		return supportZeroWin;
	}

	/**
	 * @param supportZeroWin the supportZeroWin to set
	 */
	public void setSupportZeroWin(Boolean supportZeroWin) {
		this.supportZeroWin = supportZeroWin;
	}
	
	
	
	
}
