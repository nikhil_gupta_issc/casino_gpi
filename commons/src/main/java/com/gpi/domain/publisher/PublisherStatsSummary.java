package com.gpi.domain.publisher;


import com.gpi.domain.currency.Currency;

import java.util.Date;

public class PublisherStatsSummary {

    private Integer publisherId;

    private Long gameId;

    private Long playerId;

    private Long winAmount;

    private Long betAmount;

    private Date createdOn;

    private Currency currency;

    /**
     * @return the publisherId
     */
    public Integer getPublisherId() {
        return publisherId;
    }

    /**
     * @param publisherId the publisherId to set
     */
    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    /**
     * @return the gameId
     */
    public Long getGameId() {
        return gameId;
    }

    /**
     * @param gameId the gameId to set
     */
    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    /**
     * @return the playerId
     */
    public Long getPlayerId() {
        return playerId;
    }

    /**
     * @param playerId the playerId to set
     */
    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    /**
     * @return the winAmount
     */
    public Long getWinAmount() {
        return winAmount;
    }

    /**
     * @param winAmount the winAmount to set
     */
    public void setWinAmount(Long winAmount) {
        this.winAmount = winAmount;
    }

    /**
     * @return the betAmount
     */
    public Long getBetAmount() {
        return betAmount;
    }

    /**
     * @param betAmount the betAmount to set
     */
    public void setBetAmount(Long betAmount) {
        this.betAmount = betAmount;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
