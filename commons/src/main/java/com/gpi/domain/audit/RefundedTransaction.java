package com.gpi.domain.audit;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Nacho on 12/2/2014.
 */
@Entity
@Table(name = "refunded_transaction")
public class RefundedTransaction implements Serializable {

    private static final long serialVersionUID = 7913074817756067341L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "game_transaction_id", nullable = false)
    private String gameTransactionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transaction_id", referencedColumnName = "id", nullable = false)
    private Transaction transaction;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "round_id", referencedColumnName = "id", nullable = false)
    private Round round;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGameTransactionId() {
        return gameTransactionId;
    }

    public void setGameTransactionId(String gameTransactionId) {
        this.gameTransactionId = gameTransactionId;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }
}
