package com.gpi.domain.audit;

import com.gpi.domain.currency.Currency;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * Represents the data for audit in the database. table: round
 */
@Entity
@Table(name = "round")
public class Round implements Serializable {

    private static final long serialVersionUID = -2245715080461306197L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id", referencedColumnName = "id")
    private Game game;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "publisher_id", referencedColumnName = "id")
    private Publisher publisher;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id", referencedColumnName = "id")
    private Player player;

    @Column(name = "currency", nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Column(name = "win_amount", nullable = false)
    private Long winAmount = 0L;

    @Column(name = "bet_amount", nullable = false)
    private Long betAmount = 0L;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "error", nullable = false)
    private boolean error;

    @Column(name = "game_round_id", nullable = false)
    private Long gameRoundId;

    public Round() {
        super();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getWinAmount() {
        return winAmount;
    }

    public void setWinAmount(Long amount) {
        this.winAmount = amount;
    }

    public Long getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(Long amount) {
        this.betAmount = amount;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Long getGameRoundId() {
        return gameRoundId;
    }

    public void setGameRoundId(Long gameRoundId) {
        this.gameRoundId = gameRoundId;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Round round = (Round) o;

        if (!createdOn.equals(round.createdOn)) return false;
        if (currency != round.currency) return false;
        if (!game.equals(round.game)) return false;
        if (gameRoundId != null ? !gameRoundId.equals(round.gameRoundId) : round.gameRoundId != null) return false;
        if (id != null ? !id.equals(round.id) : round.id != null) return false;
        if (!player.equals(round.player)) return false;
        if (!publisher.equals(round.publisher)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + game.hashCode();
        result = 31 * result + publisher.hashCode();
        result = 31 * result + player.hashCode();
        result = 31 * result + currency.hashCode();
        result = 31 * result + createdOn.hashCode();
        result = 31 * result + (gameRoundId != null ? gameRoundId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Round{" +
                "id=" + id +
                ", game=" + game +
                ", publisherId=" + (publisher != null ? publisher.getName() : null) +
                ", player=" + (player != null ? player.getName() : null) +
                ", currency=" + currency +
                ", winAmount=" + winAmount +
                ", betAmount=" + betAmount +
                ", createdOn=" + createdOn +
                ", error=" + error +
                ", gameRoundId=" + gameRoundId +
                '}';
    }
}
