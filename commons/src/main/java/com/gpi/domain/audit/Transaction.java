package com.gpi.domain.audit;

import com.gpi.domain.transaction.TransactionType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Represents the data for a specific transaction like bet, win or refund in the
 * database. table: transactions
 */
@Entity
@Table(name = "transaction")
public class Transaction implements Serializable {

    private static final long serialVersionUID = -4597400647037493242L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "round_id", referencedColumnName = "id", nullable = false)
    private Round round;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @Column(name = "created_on", nullable = false)
    private Date createdOn;

    @Column(name = "success", nullable = false)
    private boolean success = false;

    @Column(name = "game_transaction_id", nullable = false)
    private String gameTransactionId;

    @Column(name = "ext_transaction_id", nullable = true)
    private String extTransactionId;

    @Column(name = "amount", nullable = false)
    private Long amount;


    public Transaction() {
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }


    public String getGameTransactionId() {
        return gameTransactionId;
    }

    public void setGameTransactionId(String gameTransactionId) {
        this.gameTransactionId = gameTransactionId;
    }

    public String getExtTransactionId() {
        return extTransactionId;
    }

    public void setExtTransactionId(String extTransactionId) {
        this.extTransactionId = extTransactionId;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", round=" + round +
                ", type=" + type +
                ", createdOn=" + createdOn +
                ", success=" + success +
                ", gameTransactionId=" + gameTransactionId +
                ", extTransactionId=" + extTransactionId +
                ", amount=" + amount +
                '}';
    }
}
