package com.gpi.domain.audit;

import com.gpi.domain.free.spin.FreeSpin;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by igabba on 05/01/15.
 */
@Entity
@Table(name = "free_spin_transaction")
public class FreeSpinTransaction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "game_transaction_id", nullable = false)
    private String gameTransactionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transaction_id", referencedColumnName = "id", nullable = false)
    private Transaction transaction;

    @Column(name = "amount", nullable = false)
    private Long amount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "free_spin_id", referencedColumnName = "id", nullable = false)
    private FreeSpin freeSpin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGameTransactionId() {
        return gameTransactionId;
    }

    public void setGameTransactionId(String gameTransactionId) {
        this.gameTransactionId = gameTransactionId;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public FreeSpin getFreeSpin() {
        return freeSpin;
    }

    public void setFreeSpin(FreeSpin freeSpin) {
        this.freeSpin = freeSpin;
    }
}
