package com.gpi.domain.other;

import com.gpi.utils.ApplicationProperties;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This enum holds the languages supported by our games
 *
 * @author csanchez
 */
public enum Language {
    EN, ES, PT, IT;

    private static Logger logger = LoggerFactory.getLogger(Language.class);

    /**
     * Retrieves a Language enum by code, if it is not supported a default one
     * is returned
     */
    public static Language getByCode(String code) {
        return getByCode(code, ApplicationProperties.getProperty("application.default.language"));
    }

    /**
     * Retrieves a Language enum by code, if it is not supported the default one
     * is returned
     */
    public static Language getByCode(String code, String defaultCode) {
        if (StringUtils.isEmpty(defaultCode)) {
            throw new NullArgumentException("defaultCode");
        }

        logger.debug("Retrieving Language by code: " + code);
        Language language = null;

        if (StringUtils.isNotEmpty(code)) {
            String upperCode = code.toUpperCase();
            for (Language current : Language.values()) {
                if (current.name().equals(upperCode)) {
                    language = current;
                    break;
                }
            }
        }

        if (language == null) {
            Language defaultLanguage = Language.valueOf(defaultCode.toUpperCase());
            logger.info("Language not provided nor supported, specified using default one {}", defaultLanguage);
            language = defaultLanguage;
        }
        return language;
    }

    public String getCode() {
        return name().toLowerCase();
    }
}
