package com.gpi.domain.game;

/**
 * User: Ignacio
 */
public enum PlayingType {

    FREE("free"), CHARGED("charged");

    private final String type;

    private PlayingType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
