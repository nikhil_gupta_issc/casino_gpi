package com.gpi.domain.game;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * User: igabbarini
 */
@Entity
@Table(name = "game")
public class Game implements Serializable {


    private static final long serialVersionUID = -6620701233634365085L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name", unique = true, nullable = false, length = 100)
    private String name;
    
    @Column(name = "url", nullable = true)
    private String url;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id", referencedColumnName = "id")
    private GameProvider gameProvider;

    @Column(name = "freespins", nullable = false, columnDefinition = "tinyint default false")
    private boolean freespins = false;

    @Column(name = "description", nullable = true)
    private String description;


    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public GameProvider getGameProvider() {
        return gameProvider;
    }

    public void setGameProvider(GameProvider gameProvider) {
        this.gameProvider = gameProvider;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	/**
	 * @return the freespins
	 */
	public boolean isFreespins() {
		return freespins;
	}

	/**
	 * @param freespins the freespins to set
	 */
	public void setFreespins(boolean freespins) {
		this.freespins = freespins;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        return name.equals(game.name) && gameProvider.equals(game.gameProvider);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + gameProvider.hashCode();
        return result;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Game [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", url=");
		builder.append(url);
		builder.append(", gameProvider=");
		builder.append(gameProvider);
		builder.append(", freespins=");
		builder.append(freespins);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}
    
    
    
}
