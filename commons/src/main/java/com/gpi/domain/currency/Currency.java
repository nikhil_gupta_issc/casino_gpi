package com.gpi.domain.currency;


public enum Currency {
	// PTR is Points currency for betgames and amelco
    USD, BRL, EUR, JPY, CNY, SDG, RON, MKD, MXN, CAD,
    ZAR, AUD, NOK, ILS, ISK, SYP, LYD, UYU, YER, CSD,
    EEK, THB, IDR, LBP, AED, BOB, QAR, BHD, HNL, HRK,
    COP, ALL, DKK, MYR, SEK, RSD, BGN, DOP, KRW, LVL,
    VEF, CZK, TND, KWD, VND, JOD, NZD, PAB, CLP, PEN,
    GBP, DZD, CHF, RUB, UAH, ARS, SAR, EGP, INR, PYG,
    TWD, TRY, BAM, OMR, SGD, MAD, BYR, NIO, HKD, LTL,
    SKK, GTQ, HUF, IQD, CRC, PHP, SVC, PLN, FBC, NGN,
    GHS, UGX, KZT, AZN, IRR, TMT, GEL, KGS, AMD, TJS,
    PRB, MNT, MBT, XAF, XOF, HTG, UZS, BDT, DTC, GMC,
    ETB, CDF, TZS, KES, MZN, AOA, MMK, BYN, ZMW, ZWL,
    MDL, DASH, XMR, GAME, XEM, PKR, KHR, MDASH, MLTC,
    METH, MXMR, MZEC, LTC, ETH, ZEC, MBCH, UBT, SZL, MOON, 
    MGA, PTR, DHS
    ;

    public static Currency getById(Integer currencyId) {
        return Currency.values()[currencyId - 1];
    }

    public static Currency getByCode(String code) {
        return Currency.valueOf(code);
    }

    public Integer getId() {
        return this.ordinal() + 1;
    }

    public String getDescription() {
        return this.name();
    }

}
