package com.gpi.domain.free.spin;

import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "player_free_spin")
public class FreeSpin implements Serializable {

    private static final long serialVersionUID = 102258413674175515L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id", referencedColumnName = "id", nullable = false)
    private Player player;

    @Column(name = "amount_left", nullable = false)
    private int amountLeft;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id", referencedColumnName = "id", nullable = false)
    private Game game;
    
    @Column(name = "cents", nullable = false)
    private int cents;
    
    @Column(name = "external_ref", nullable = true)
    private String externalRef;


    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @param player the player to set
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * @return the amountLeft
     */
    public int getAmountLeft() {
        return amountLeft;
    }

    /**
     * @param amountLeft the amountLeft to set
     */
    public void setAmountLeft(int amountLeft) {
        this.amountLeft = amountLeft;
    }

    public void decreaseAmountLeft() {
        this.amountLeft--;
    }

	/**
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * @param game the game to set
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * @return the cents
	 */
	public int getCents() {
		return cents;
	}

	/**
	 * @param cents the cents to set
	 */
	public void setCents(int cents) {
		this.cents = cents;
	}

	/**
	 * @return the externalRef
	 */
	public String getExternalRef() {
		return externalRef;
	}

	/**
	 * @param externalRef the externalRef to set
	 */
	public void setExternalRef(String externalRef) {
		this.externalRef = externalRef;
	}
}
