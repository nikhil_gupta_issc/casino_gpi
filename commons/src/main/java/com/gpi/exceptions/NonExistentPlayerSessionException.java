package com.gpi.exceptions;

public class NonExistentPlayerSessionException extends Exception {


    private static final long serialVersionUID = -850802435070985886L;

    public NonExistentPlayerSessionException() {
    }

    public NonExistentPlayerSessionException(String message) {
        super(message);
    }

    public NonExistentPlayerSessionException(String message, Throwable cause) {
        super(message, cause);
    }
}
