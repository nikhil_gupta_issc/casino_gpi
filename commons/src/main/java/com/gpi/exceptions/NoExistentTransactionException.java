package com.gpi.exceptions;

/**
 * Created by Nacho on 12/2/2014.
 */
public class NoExistentTransactionException extends GamePlatformIntegrationException {
    private String errorMessage;
    private int errorCode = 5000;

    public NoExistentTransactionException(String s) {
        super(s);
        this.errorMessage = s;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public int getErrorCode() {
        return errorCode;
    }
}
