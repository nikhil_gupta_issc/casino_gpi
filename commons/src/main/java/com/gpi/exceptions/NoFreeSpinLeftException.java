package com.gpi.exceptions;

/**
 * Created by igabba on 05/01/15.
 */
public class NoFreeSpinLeftException extends Exception{

    private static final long serialVersionUID = -4450410860000535656L;

    public NoFreeSpinLeftException(String s) {
        super(s);
    }
}
