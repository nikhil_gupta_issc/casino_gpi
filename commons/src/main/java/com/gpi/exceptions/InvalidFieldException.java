package com.gpi.exceptions;

/**
 * Thrown whenever a field that doesn't belong to a specific class is requested
 */
public class InvalidFieldException extends RuntimeException {

    private static final long serialVersionUID = -6180620718124177864L;

    public InvalidFieldException(String msg) {
        super(msg);
    }
}