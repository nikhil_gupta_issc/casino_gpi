package com.gpi.exceptions;

/**
 * Created by Nacho on 2/21/14.
 */
public class GameRequestNotExistentException extends GamePlatformIntegrationException {

    private String errorMessage = "Token is invalid or old.";
    private int errorCode = 2000;

    public GameRequestNotExistentException(String message) {
        super(message);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
