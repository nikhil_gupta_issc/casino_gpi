package com.gpi.exceptions;

/**
 * Created by Nacho on 2/20/14.
 */
public class GameProviderNotExistentException extends Exception {

    public GameProviderNotExistentException(String message) {
        super(message);
    }
}
