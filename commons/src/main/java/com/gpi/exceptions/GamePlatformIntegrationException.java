package com.gpi.exceptions;


/**
 * Created by Nacho on 2/20/14.
 */
public class GamePlatformIntegrationException extends Exception {


    private String errorMessage;
    private int errorCode;

    public GamePlatformIntegrationException(String message) {
        super(message);
        this.errorMessage = message;
    }

    public GamePlatformIntegrationException(String message, int errorCode) {
        super(message);
        this.errorMessage = message;
        this.errorCode = errorCode;
    }

    public GamePlatformIntegrationException(Exception e) {
        super(e);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GamePlatformIntegrationException [errorMessage=" + errorMessage + ", errorCode=" + errorCode + "]";
	}
    
    

}
