package com.gpi.exceptions;

/**
 * Created by Nacho on 12/12/2014.
 */
public class InvalidCredentialsException extends GamePlatformIntegrationException {

    private static final long serialVersionUID = -8660078081348425024L;


    public InvalidCredentialsException(String s) {
        super(s);
    }
}
