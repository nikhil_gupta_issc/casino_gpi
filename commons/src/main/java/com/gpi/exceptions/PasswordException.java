package com.gpi.exceptions;

public class PasswordException extends Exception {

    public PasswordException(String e) {
        super(e);
    }

    public PasswordException(String s, Exception e) {
        super(s, e);
    }
}
