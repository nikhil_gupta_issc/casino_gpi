package com.gpi.exceptions;

import com.gpi.utils.ApplicationProperties;

/**
 * User: igabbarini
 */
public class SessionExpiredException extends Exception {

    private static final String ERROR_CODE = "gpi.session.expired.exception";

    private static final long serialVersionUID = -5067285971158351883L;

    public SessionExpiredException() {
        super(ApplicationProperties.getProperty(ERROR_CODE));
    }


}
