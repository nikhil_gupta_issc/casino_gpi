package com.gpi.exceptions;

public class MarshalException extends Exception {

    private static final long serialVersionUID = -8398954405028709065L;

    public MarshalException(String message) {
        super(message);
    }
}
