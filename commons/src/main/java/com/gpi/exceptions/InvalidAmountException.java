package com.gpi.exceptions;

/**
 * Created by Nacho on 8/26/2014.
 */
public class InvalidAmountException extends Exception {

    public InvalidAmountException(String message) {
        super(message);
    }
}
