package com.gpi.exceptions;

public class NonExistentPlayerException extends Exception {


    private static final long serialVersionUID = -2094055390051286770L;

    public NonExistentPlayerException(String message) {
        super(message);
    }

    public NonExistentPlayerException(String message, Throwable cause) {
        super(message, cause);
    }
}
