package com.gpi.exceptions;

/**
 * Created by nacho on 30/12/14.
 */
public class NotEnoughPackagesLeftException extends Exception {
    private static final long serialVersionUID = 6848388341938647631L;
}
