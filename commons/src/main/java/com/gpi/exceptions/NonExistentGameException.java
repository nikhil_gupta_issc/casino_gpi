package com.gpi.exceptions;

/**
 * Created by Ignacio on 2/19/14.
 */
public class NonExistentGameException extends Exception {

    public NonExistentGameException(String s) {
        super(s);
    }
}
