package com.gpi.exceptions;

/**
 * Created by Ignacio on 2/19/14.
 */
public class NonExistentPublisherException extends Exception {

    public NonExistentPublisherException() {
    }

    public NonExistentPublisherException(String message) {
        super(message);
    }
}
