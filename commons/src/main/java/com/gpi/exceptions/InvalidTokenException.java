package com.gpi.exceptions;

import com.gpi.utils.ApplicationProperties;

/**
 * User: igabbarini
 */
public class InvalidTokenException extends Exception {
    private static final long serialVersionUID = -5815627714038914161L;
    private static final String ERROR_CODE = "gpi.invalid.token.exception";

    public InvalidTokenException() {
        super(ApplicationProperties.getProperty(ERROR_CODE));
    }

    public InvalidTokenException(String s) {
        super(s);
    }

}
