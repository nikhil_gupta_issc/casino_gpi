package com.gpi.exceptions;

public class UnknownTokenException extends GamePlatformIntegrationException {

    private static final long serialVersionUID = 7945240124520206843L;
    private String errorMessage = "Token is invalid or old.";
    private int errorCode = 4000;

    public UnknownTokenException(String message) {
        super(message);
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public int getErrorCode() {
        return errorCode;
    }
}
