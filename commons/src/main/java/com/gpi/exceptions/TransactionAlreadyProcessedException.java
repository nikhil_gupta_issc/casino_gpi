package com.gpi.exceptions;

import com.gpi.domain.audit.Transaction;

/**
 * Created by Nacho on 12/1/2014.
 */
public class TransactionAlreadyProcessedException extends GamePlatformIntegrationException {
    Transaction t;
    public TransactionAlreadyProcessedException(String s, Transaction transaction) {
        super(s);
        this.t = transaction;
    }


    public Transaction getT() {
        return t;
    }
}
