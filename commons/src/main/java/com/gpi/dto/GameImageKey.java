package com.gpi.dto;

import java.io.Serializable;

public class GameImageKey implements Serializable{

	private static final long serialVersionUID = -8772170667514093551L;

	private static final String IMAGES_CONSTANT_PREFIX = "gpi";
	
	private String key;

	public GameImageKey(String key) {
		super();
		this.key = key;
	}

	public GameImageKey(Integer publisherId, Long gameId) {
		super();
		this.key = getKey(publisherId, gameId);
	}

	public GameImageKey(Long gameId) {
		super();
		this.key = getKey(gameId);
	}

	public GameImageKey() {
		super();
	}
	
	public String getKey() {
		return key;
	}
	
	public GameImageKey generateAlternativeKey() {
		GameImageKey alternativeKey = null;
		
		String[] keySplit = key.split("-");
		if (keySplit.length == 3) {
			alternativeKey = new GameImageKey(keySplit[0] + "-" + keySplit[2]);
		}
		
		return alternativeKey;
	}
	
	private String getKey(Integer publisherId, Long gameId) {
		String key = null;
		if (publisherId != null && gameId != null) {
			key = IMAGES_CONSTANT_PREFIX + "-" + publisherId + "-" + gameId +  ".jpg";
		}else if (gameId != null) {
			key = getKey(gameId);
		}
		return key;
	}
	
	private String getKey(Long gameId) {
		String key = null;
		if (gameId != null) {
			key = IMAGES_CONSTANT_PREFIX + "-" +  gameId +  ".jpg";
		}
		return key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameImageKey other = (GameImageKey) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}
			
}
