package com.gpi.dto;

import java.io.Serializable;

public class GameImage implements Serializable{

	private static final long serialVersionUID = 6453588656033772997L;

	private GameImageKey key;
	
	private String url;
	
    public GameImage() {
		super();
	}
    
	public GameImage(GameImageKey key, String url) {
		super();
		this.key = key;
		this.url = url;
	}

	public GameImageKey getKey() {
		return key;
	}

	public void setKey(GameImageKey key) {
		this.key = key;
	}

	public GameImage(String url) {
		super();
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}	
			
}
