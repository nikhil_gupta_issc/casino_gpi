package com.gpi.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class GamesImages implements Serializable{

	private static final long serialVersionUID = -7724102995927571785L;

	private Map<GameImageKey, GameImage> gameImages = new HashMap<GameImageKey, GameImage>();
	
	public GamesImages() {
		super();
	}

	public void add(GameImageKey key, String url) {
		this.gameImages.put(key, new GameImage(key, url));
	}
	
	public GameImage get(GameImageKey key) {
		GameImage gameImage = null;
		
		if (this.gameImages.containsKey(key)) {
			gameImage = this.gameImages.get(key);
		}else {
			GameImageKey alternativeKey = key.generateAlternativeKey();
			if (alternativeKey != null) {
				gameImage = this.get(alternativeKey);
			}
		}
		
		return gameImage;
	}	
			
}
