package com.gpi.dto;


import com.gpi.domain.currency.Currency;

/**
 * This class contains the values returned from the publishers and other useful properties
 */
public class ApiResponseDto {

    private String token;
    private String loginName;
    private String error;
    private Integer errorCode;
    private Boolean success = Boolean.TRUE;
    private Boolean alreadyProcessed = Boolean.FALSE;
    private Currency currency;
    private String balance;
    private Long extTransactionId;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public void setExtTransactionId(Long extTransactionId) {
        this.extTransactionId = extTransactionId;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
        this.success = error == null;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Boolean isSuccess() {
        return success;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Boolean getAlreadyProcessed() {
        return alreadyProcessed;
    }

    public void setAlreadyProcessed(Boolean alreadyProcessed) {
        this.alreadyProcessed = alreadyProcessed;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ApiResponseDto{").append((token != null) ? "token=" + token : "").append((loginName != null) ? ", loginName=" + loginName : "").append((error != null) ? ", error=" + error : "").append((errorCode != null) ? ", errorCode=" + errorCode : "").append((success != null) ? ", success=" + success : "").append((alreadyProcessed != null) ? ", alreadyProcessed=" + alreadyProcessed : "").append((currency != null) ? ", currency=" + currency : "").append((balance != null) ? ", balance=" + balance : "").append((extTransactionId != null) ? ", extTransactionId=" + extTransactionId : "").append("}").toString();
    }

}
