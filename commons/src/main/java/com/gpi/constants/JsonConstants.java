package com.gpi.constants;

/**
 * User: igabbarini
 */
public interface JsonConstants {

    public static final String AMOUNT = "ba";

    public static final String WIN_AMOUNT = "wa";

    public static final String EXTRA_BALLS = "eb";

    public static final String EXTRA_BALLS_PRICE = "ebp";

    public static final String CURRENCY = "c";

    public static final String TICKETS = "t";

    public static final String BONUS_SHOOT = "bs";

    public static final String BONUS_CREDITS = "bc";

    public static final String TRANSACTION_REFERENCE = "tr";

    /**
     * Balls assigned
     */
    public static final String BALLS = "b";

    public static final String FREE_BALL = "fb";

    public static final String JACKPOT = "j";

    public static final String JACKPOT_POSITION = "jp";

    public static final String JOKER = "joker";

    public static final String FRONT_VALUES = "frontValues";

    public static final String CONSOLATION_PRIZE = "cp";

    public static final Object JACKPOT_WINNINGS = "jpw";

    public static final String BONUS_CROSS_GAME = "bcg";
}
