package com.gpi.constants;

/**
 * Created by nacho on 19/11/14.
 */
public interface CacheConstants {

    //socialPlayer
    public static final String SOCIAL_PLAYER_KEY = "GPI_SP-";
    public static final int SOCIAL_PLAYER_TTL = 60 * 60 * 5;

    public static final String DAILY_STATS_KEY = "GPI_ds";
    public static final int DAILY_STATS_TTL = 60 * 60 * 24;

    public static final String MONTHLY_STATS_KEY = "GPI_ms";
    public static final int MONTHLY_STATS_TTL = 60 * 60 * 24;

    public static final String ROUND_KEY = "GPI_R-";
    public static final int ROUND_TTL = 60 * 60 * 24;
    public static final String TRANSACTION_KEY = "GPI_T-";
    public static final int TRANSACTION_TTL = 60 * 60 * 24;
    public static final String TRANSACTION_EXTID_KEY = "GPI_T_EXT-";
    public static final int TRANSACTION_EXID_TTL = 60 * 60 * 24;

    public static final String REFUNDED_TRANSACTION_KEY = "GPI_RT-";
    public static final int REFUNDED_TRANSACTION_TTL = 60 * 60 * 24;

    public static final String PUBLISHER_GAME_SESSION_KEY = "GPI_PG-";
    public static final int PUBLISHER_GAME_SESSION_TTL = 60 * 15;

    public static final String GAME_LIST_KEY = "GPI_gl-";
    public static final int GAME_LIST_TTL = 60 * 60 * 24 * 7;
    public static final String PROVIDER_GAME_LIST_KEY = "GPI_pgl-";
    public static final int PROVIDER_GAME_LIST_TTL = 60 * 60 * 24 * 7; // 1 week

    public static final String GAME_KEY = "GPI_game-";
    public static final String GAME_KEY_BY_ID = "GPI_gameId-";
    public static final int GAME_TTL = 60 * 60 * 24;

    public static final String GAME_PROVIDER_KEY = "GPI_gp-";
    public static final int GAME_PROVIDER_TTL = 60 * 60 * 24;
    
    public static final String GAME_FREESPIN_KEY = "GPI_fs-";
    public static final int GAME_FREESPIN_TTL = 60 * 60 * 24;

    public static final String ALL_PROVIDERS_KEY = "GPI_AP";
    public static final int ALL_PROVIDERS_TTL = 60 * 60 * 24;

    public static final String PLAYER_KEY = "GPI_P-";
    public static final String PLAYER_KEY_ID = "GPI_P2-";
    public static final int PLAYER_TTL = 60 * 60 * 24;
    public static final String PLAYER_BY_TOKEN_KEY = "GPI_PT-";
    public static final int PLAYER_BY_TOKEN_TTL = 60 * 60;

    public static final String PUBLISHER_BY_NAME_PASS_KEY = "GPI_PU-";
    public static final int PUBLISHER_BY_NAME_PASS_TTL = 60 * 60 * 24;
    public static final String PUBLISHER_BY_NAME_KEY = "GPI_PN-";
    public static final int PUBLISHER_BY_NAME_TTL = 60 * 60 * 24;

    public static final String GAME_PARAMS_KEY = "GPI_gpp-";
    public static final int GAME_PARAMS_TTL = 60 * 10; // 10 minutes
    
    public static final String APP_PROPERTY_CACHE_KEY = "APP-PROPERTIES";
    public static final int APP_PROPERTY_CACHE_TTL = 60 * 60;
    
    public static final String IMAGES_GAMES_MEDILINK_CACHE_KEY = "IMAGES-GAMES-MEDIALINK-";
    public static final String IMAGES_GAMES_MEDILINK_ALL_CACHE_KEY = "IMAGES-GAMES-MEDIALINK-ALL";
    public static final int IMAGES_GAMES_MEDIALINK_TTL = 60 * 60 * 24;
}
