package com.gpi.state;

/**
 * User: igabbarini
 */
public interface State {
    String getName();
}
