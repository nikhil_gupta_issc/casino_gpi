package com.gpi.utils;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.core.Ordered;

public class JamonAspect implements Ordered {

    private static final Logger LOGGER = Logger.getLogger(JamonAspect.class);

    private int logThresholdMilliseconds = 0;

    private boolean enabled = true;

    private int order = Ordered.LOWEST_PRECEDENCE;

    /**
     * Monitors method calls using Jamon.
     *
     * @param pjp
     * @return
     * @throws Throwable
     */
    public Object monitor(ProceedingJoinPoint pjp) throws Throwable {
        if (!isEnabled()) {
            return pjp.proceed();
        }

        String methodSignature = createInvocationTraceName(pjp);
        Monitor monitor = MonitorFactory.start(methodSignature);
        try {
            return pjp.proceed();
        } finally {
            monitor.stop();
            if (monitor.getLastValue() > getLogThresholdMilliseconds()) {
                LOGGER.warn(monitor.getLastValue() + " ms. " + monitor);
            }
        }
    }

    private String createInvocationTraceName(ProceedingJoinPoint pjp) {
        String longSignatureString = pjp.getSignature().toLongString();

        int lastIndexOfThrows = longSignatureString.lastIndexOf("throws");
        if (lastIndexOfThrows > 0) {
            longSignatureString = longSignatureString.substring(0, lastIndexOfThrows);
        }

        String[] split = longSignatureString.split(" ");
        return split[split.length - 1];
    }

    public int getOrder() {
        return order;
    }

    /**
     * Sets priority order for the aspect.
     *
     * @param order
     */
    public void setOrder(int order) {
        this.order = order;
    }

    protected boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    protected int getLogThresholdMilliseconds() {
        return logThresholdMilliseconds;
    }

    public void setLogThresholdMilliseconds(int logThresholdMilliseconds) {
        this.logThresholdMilliseconds = logThresholdMilliseconds;
    }

}
