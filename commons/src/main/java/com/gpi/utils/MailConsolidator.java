package com.gpi.utils;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.TriggeringEventEvaluator;

import java.util.Calendar;
import java.util.Date;

public class MailConsolidator implements TriggeringEventEvaluator {

    private static final int MAX_EMAILS = 100;
    private static final int MAX_EMAIL_WAIT = -10;
    private static volatile Date lastSend = Calendar.getInstance().getTime();
    private static volatile int count = 0;

    @Override
    public boolean isTriggeringEvent(LoggingEvent arg0) {
        Date now = Calendar.getInstance().getTime();
        Date timeOut = DateUtils.addMinutes(now, MAX_EMAIL_WAIT);
        boolean success = false;
        if (count > MAX_EMAILS || lastSend.before(timeOut)) {
            count = 0;
            success = true;
        } else {
            count++;
        }
        lastSend = now;
        return success;

    }

}
