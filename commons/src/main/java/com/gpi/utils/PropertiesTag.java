package com.gpi.utils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

public class PropertiesTag extends TagSupport {

    private static final long serialVersionUID = 7381273071440383814L;

    private String name;
    private String var;

    public int doStartTag() throws JspException {
        pageContext.setAttribute(var, ApplicationProperties.getProperty(name));
        return Tag.SKIP_BODY;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public void setName(String name) {
        this.name = name;
    }

}
