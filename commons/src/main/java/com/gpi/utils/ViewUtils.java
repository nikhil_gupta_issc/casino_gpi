package com.gpi.utils;

import net.sf.json.spring.web.servlet.view.JsonView;
import org.springframework.web.servlet.ModelAndView;

public class ViewUtils {

    public static ModelAndView createJsonView() {
        return convertToJsonView(new ModelAndView());
    }

    public static ModelAndView convertToJsonView(ModelAndView mav) {
        JsonView jsonView = new JsonView();
        jsonView.setContentType("application/json;charset=UTF-8");
        mav.setView(jsonView);
        return mav;
    }

}
