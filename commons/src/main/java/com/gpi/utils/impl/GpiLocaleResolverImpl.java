package com.gpi.utils.impl;

import com.gpi.utils.GpiLocaleResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.i18n.AbstractLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public class GpiLocaleResolverImpl extends AbstractLocaleResolver implements GpiLocaleResolver {

    private static final Locale DEFAULT_LOCALE = new Locale("pt", "BR");
    private final Logger logger = LoggerFactory.getLogger(GpiLocaleResolverImpl.class);

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        //try to get it from browser
        Locale locale = request.getLocale();
        logger.debug("No locale in cookie. Locale from browser is {}", locale);
        if (locale == null) {
            //as last resource, default
            locale = DEFAULT_LOCALE;
            logger.debug("No locale in browser. Default locale is {}", locale);
        }
        return locale;
    }


    @Override
    public void setLocale(HttpServletRequest request,
                          HttpServletResponse response, Locale locale) {
    }

}
