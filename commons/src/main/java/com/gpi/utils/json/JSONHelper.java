
package com.gpi.utils.json;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author igabba
 */
public class JSONHelper {
    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
        mapper.configure(SerializationConfig.Feature.AUTO_DETECT_GETTERS, true);
        mapper.configure(SerializationConfig.Feature.AUTO_DETECT_IS_GETTERS, true);
        mapper.configure(SerializationConfig.Feature.AUTO_DETECT_FIELDS, true);
        mapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
    }

    public static String toJsonString(Object model) {
        try {
            return mapper.writeValueAsString(model);
        } catch (IOException ex) {
            Logger.getLogger(JSONHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    @SuppressWarnings("rawtypes")
    public static Map<?, ?> fromJsonString(String model) {
        try {
            return mapper.readValue(model, HashMap.class);
        } catch (IOException ex) {
            Logger.getLogger(JSONHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new HashMap();
    }

}

