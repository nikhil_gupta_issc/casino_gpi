package com.gpi.utils.cache.impl;

import com.gpi.utils.cache.CacheManager;
import net.spy.memcached.CASValue;
import net.spy.memcached.MemcachedClientIF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class MemcachedCacheManager implements CacheManager {

    private static final int STORE_ATTEMPTS = 3;

    private static final long STORE_RETRY_SLEEP = 100;

    /**
     * Timeout value to be used when getting objects.
     */
    private static final int ASYNC_GET_TIMEOUT = 4000;

    private final Logger logger = LoggerFactory.getLogger(MemcachedCacheManager.class);

    private MemcachedClientIF client = null;

    @Override
    public boolean store(String key, Serializable obj, int timeout) {
        boolean success = false;
        for (int i = 0; i < STORE_ATTEMPTS; i++) {
            logger.debug("Storing in cache. Key: {}. Value: {} Timeout: {}", key, obj, timeout);
            try {
                client.set(key, timeout, obj);
                success = true;
                break;
            } catch (Exception e) {
                logger.debug("Error storing object in cache. Key: {}", key, e);
                try {
                    Thread.sleep(STORE_RETRY_SLEEP);
                } catch (InterruptedException e1) {
                    logger.error("Error storing object in cache. InterruptedException. Key: {}", key, e);
                }
            }
        }
        if (!success) {
            logger.error("Error storing object in cache. Key: {}", key);
        }
        return success;
    }

    @Override
    public Serializable get(String key) {
        Serializable obj = null;
        Future<Object> f = null;

        try {
            logger.debug("Getting object from cache. Key: {}", key);
            f = client.asyncGet(key);
            obj = (Serializable) f.get(ASYNC_GET_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            logger.error("Error getting object from cache. Key : {}", key, e);
            if (f != null) {
                // destroy future
                f.cancel(false);
            }
        }
        return obj;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Serializable> get(String... keys) {
        @SuppressWarnings("rawtypes")
        Map values = null;
        Future<Map<String, Object>> f = null;
        try {
            logger.debug("Getting objects from cache. Keys: {}", Arrays.toString(keys));
            f = client.asyncGetBulk(keys);
            values = f.get(ASYNC_GET_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            logger.error("Error getting objects from cache. Keys: {} ", Arrays.toString(keys));
            if (f != null) {
                // destroy future
                f.cancel(false);
            }
        }
        return values;
    }

    @Override
    public boolean delete(String key) {
        try {
            logger.debug("Deleting object from cache. Key: {}", key);
            client.delete(key);
        } catch (Exception e) {
            logger.error("Error deleting object from cache. Key: {}", key, e);
            return false;
        }
        return true;
    }

    @Override
    public boolean initCounter(String key, long initialValue, int timeout) {
        // We need to transform to string because of memcached
        final String valueAsString = Long.toString(initialValue);
        return this.store(key, valueAsString, timeout);
    }

    @Override
    public long inc(String key, int amount, int timeout) {
        long value;
        try {
            logger.debug("Incrementing counter from cached. Key: {}", key);
            value = client.incr(key, amount, amount, timeout);
        } catch (Exception e) {
            logger.error("Error incrementing counter from cached. Key: {}", key);
            return -1;
        }
        return value;
    }

    @Override
    public long dec(String key, int amount, int timeout) {
        long value;
        try {
            logger.debug("Decrementing counter from cached. Key: {}", key);
            value = client.decr(key, amount, amount, timeout);
        } catch (Exception e) {
            logger.error("Error decrementing counter from cached. Key: {}", key);
            return -1;
        }
        return value;
    }

    public void setClient(MemcachedClientIF client) {
        this.client = client;
    }

    @Override
    public void shutdown() {
        this.client.shutdown();
    }

    @Override
    public Serializable getAndTouch(String key, int ttl) {
        Serializable obj = null;
        Future<CASValue<Object>> f = null;

        try {
            logger.debug("Getting object from cache. Key: {}. And modify the ttl to: {}", key, ttl);
            f = client.asyncGetAndTouch(key, ttl);
            CASValue<Object> cas = f.get(ASYNC_GET_TIMEOUT, TimeUnit.MILLISECONDS);
            if (cas != null) {
                obj = (Serializable) cas.getValue();
            }
        } catch (Exception e) {
            logger.error("Error getting object from cache. Key :{}", key, e);
            if (f != null) {
                // destroy future
                f.cancel(false);
            }
        }
        return obj;
    }
}
