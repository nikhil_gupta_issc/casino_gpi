package com.gpi.utils.cache.impl;


import com.gpi.utils.cache.CacheManager;

import java.util.List;


public class RoundRobinCacheManager extends AbstractMultipleCacheManager {

    protected CacheManager[] caches = null;

    protected int cachePoint = 0;

    protected int numCaches;

    @Override
    protected CacheManager getCacheManager() {
        cachePoint++;
        if (cachePoint < 0) {
            cachePoint = 0;
        }
        //to avoid sync problems
        return caches[cachePoint % numCaches];
    }

    public void setCacheList(List<CacheManager> cacheListList) {
        this.caches = new CacheManager[cacheListList.size()];
        this.numCaches = caches.length;
        cacheListList.toArray(caches);
    }

    @Override
    public void shutdown() {
        for (CacheManager cache : caches) {
            cache.shutdown();
        }
    }
}
