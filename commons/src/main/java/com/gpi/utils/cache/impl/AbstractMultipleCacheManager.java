package com.gpi.utils.cache.impl;


import com.gpi.utils.cache.CacheManager;

import java.io.Serializable;
import java.util.Map;


public abstract class AbstractMultipleCacheManager implements CacheManager {

    /**
     * Gets the next cache to handle a request
     */
    abstract protected CacheManager getCacheManager();

    @Override
    public boolean delete(String key) {
        return getCacheManager().delete(key);
    }

    @Override
    public Serializable get(String key) {
        return getCacheManager().get(key);
    }

    @Override
    public Map<String, Serializable> get(String... keys) {
        return getCacheManager().get(keys);
    }

    @Override
    public long inc(String key, int amount, int timeout) {
        return getCacheManager().inc(key, amount, timeout);
    }


    @Override
    public long dec(String key, int amount, int timeout) {
        return getCacheManager().dec(key, amount, timeout);
    }

    @Override
    public boolean store(String key, Serializable o, int timeout) {
        return getCacheManager().store(key, o, timeout);
    }

    @Override
    public boolean initCounter(String key, long initialValue, int timeout) {
        return getCacheManager().initCounter(key, initialValue, timeout);
    }

    @Override
    public Serializable getAndTouch(String key, int ttl) {
        return getCacheManager().getAndTouch(key, ttl);
    }

}
