package com.gpi.utils.cache;

import java.io.Serializable;
import java.util.Map;

/**
 * Playt cache implementation. Its basically a key value storing structure.
 *
 * @author gwachnitz
 */
public interface CacheManager {

    /**
     * Stores a Serializable in the cache, for timeout in seconds.
     *
     * @param key     that identifies the object
     * @param o       the object to store
     * @param timeout living time measured in seconds of the object in cache. Max = 30 days = 60*60*24*30
     * @return true if store was successful
     */
    public boolean store(String key, Serializable o, int timeout);

    /**
     * Gets a object from the cache identified by the
     * given key, or null if not found
     *
     * @param key that identifies the object
     * @return an Serializable or null if not found
     */
    public Serializable get(String key);

    /**
     * Deletes an object identified by the
     * given key from the cache
     *
     * @param key that identifies the object
     * @return true if delete was successful
     */
    public boolean delete(String key);


    /**
     * Gets all the objects identified by the given keys
     * from the cache. The map returned will have entries
     * only for the objects where found.
     *
     * @param keys that identify the objects
     * @return Map with the key-Values found or empty map
     */
    public Map<String, Serializable> get(String... keys);

    /**
     * Initializes a counter (see decrement or increment).
     * Value must be greater or equal zero
     *
     * @param key          that identifies the counter
     * @param initialValue initial value that takes the counter
     * @param timeout      seconds of the counter in cache
     * @return true if counter is incremented, false otherwise
     * @throws IllegalArgumentException if initial value less than zero
     */
    public boolean initCounter(String key, long initialValue, int timeout) throws IllegalArgumentException;

    /**
     * Increments the 'key' counter 'amount'. This is not synchronized!
     * Creates the counter if needed, with amount as its initial value.
     *
     * @param key     that identifies the object
     * @param amount  amount to increment variable
     * @param timeout seconds of the counter in cache
     * @return the new value of the counter or -1 if it does not exist.
     */
    public long inc(String key, int amount, int timeout);

    /**
     * Decrements the 'key' counter 'amount'. This is not synchronized!
     * Creates the counter if needed, with amount as its initial value.
     *
     * @param key     that identifies the object
     * @param amount  amount to decrement variable
     * @param timeout seconds of the counter in cache
     * @return the new value of the counter or -1 if it does not exist.
     */
    public long dec(String key, int amount, int timeout);


    /**
     * Gets a object from the cache identified by the
     * given key, or null if not found. Also updates de ttl
     *
     * @param key that identifies the object
     * @return an Serializable or null if not found
     * @paran ttl new time to live of this entry
     */
    public Serializable getAndTouch(String key, int ttl);

    public void shutdown();
}
