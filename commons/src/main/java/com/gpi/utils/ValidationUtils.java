package com.gpi.utils;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtils {

    private static final int MIN_PASS_SIZE = 6;

    private static final int MAX_EMAIL_SIZE = 80;
    private static final String MAIL_REGEX = "^[\\w\\.\\-]+@([\\w\\-]+\\.)+[a-zA-Z]{2,4}$";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(MAIL_REGEX);

    private static final int MAX_PHONE_NUMBER_SIZE = 127;

    private static final String PHONE_REGEX = "[\\s]*[0-9]+[\\s\\-0-9/]*";
    private static final Pattern PHONE_NUMBERS_PATTERN = Pattern.compile(PHONE_REGEX);

    /**
     * Validates an e-mail address used as user name
     */
    public static void validateEmail(String mail) throws InvalidEmailException {
        if (StringUtils.isNotBlank(mail)) {
            final Matcher matcher = EMAIL_PATTERN.matcher(mail);
            if (matcher.matches() && (mail.length() < MAX_EMAIL_SIZE)) {
                // address ok, return
                return;
            }
        }
        throw new InvalidEmailException("Player tried to submit an invalid mail" + mail);
    }

    /**
     * validates this string represents a phone numbers
     *
     * @throws InvalidPhoneNumberException
     */
    public static void validatePhone(String phoneNumber) throws InvalidPhoneNumberException {
        if (StringUtils.isNotBlank(phoneNumber)) {
            final Matcher matcher = PHONE_NUMBERS_PATTERN.matcher(phoneNumber);
            if (matcher.matches() && (phoneNumber.length() < MAX_PHONE_NUMBER_SIZE)) {
                // phones ok, return
                return;
            }
        }
        throw new InvalidPhoneNumberException();
    }


    /**
     * Validates a user password
     */
    public static void validatePassword(String password) throws InvalidPasswordException {
        if (StringUtils.isBlank(password) || password.length() < MIN_PASS_SIZE || !StringUtils.isAlphanumeric(password)) {
            throw new InvalidPasswordException("Player tried to submit an invalid password ");
        }
    }

    /**
     * Validates a user username
     */
    public static void validateUserName(String userName) throws InvaliUserNameException {
        if (StringUtils.isBlank(userName) || !StringUtils.isAlphanumeric(userName)) {
            throw new InvaliUserNameException();
        }
    }

    /**
     * Rejects the email addresses that belongs to some unwanted domains
     *
     * @param email
     * @throws InvalidEmailException
     */
    public static void validateEmailDomain(String email) throws InvalidEmailException {

        String domain = email.substring(email.indexOf("@") + 1);
        String[] domains = ApplicationProperties.getProperty("mailing.invalid.domains").split(",");

        if (ArrayUtils.contains(domains, domain)) {
            throw new InvalidEmailException("Player tried to submit an e-mail with invalid domain:" + email);
        }
    }

    /**
     * This method encodes the text in the MIME type format specified for email subjects
     */
    public static String getMimeEncodedSubject(String subject) throws UnsupportedEncodingException {
        return MimeUtility.encodeText(subject, "utf-8", "Q");
    }

    public static boolean isFreePublisher(String publisherName) {
        return publisherName.equalsIgnoreCase("free");
    }

    static public class InvalidDateFormatException extends Exception {

        private static final long serialVersionUID = -3694220771125616737L;

    }

    static public class InvaliUserNameException extends Exception {

        private static final long serialVersionUID = -3694220771125616737L;

    }

    static public class InvalidEmailException extends Exception {

        private static final long serialVersionUID = 6096939016460548541L;

        public InvalidEmailException(String msg) {
            super(msg);
        }
    }

    static public class InvalidPhoneNumberException extends Exception {

        private static final long serialVersionUID = -592025709883261910L;
    }

    static public class InvalidPasswordException extends Exception {

        private static final long serialVersionUID = -7067535920321101630L;

        public InvalidPasswordException(String msg) {
            super(msg);
        }
    }

}
