package com.gpi.utils;

/**
 * Utility class used to measure code execution time (in nano seconds)
 *
 * @author csanchez
 */
public class StopWatch {

    private static final long NANO_TO_MILLS_FACTOR = 1000000L;

    private long startTime = 0;

    private long elapsedTime = 0;

    private boolean isRunning = false;

    public void start() {
        if (!isRunning) {
            startTime = System.nanoTime();
            isRunning = true;
        }
    }

    public void stop() {
        if (isRunning) {
            elapsedTime += System.nanoTime() - startTime;
            isRunning = false;
        }
    }

    public void reset() {
        elapsedTime = 0;
        if (isRunning) {
            startTime = System.nanoTime();
        }
    }

    public long getElapsedTimeNanos() {
        if (isRunning) {
            return System.nanoTime() - startTime;
        }
        return elapsedTime;
    }

    public long getElapsedTimeMillis() {
        return getElapsedTimeNanos() / NANO_TO_MILLS_FACTOR;
    }
}
