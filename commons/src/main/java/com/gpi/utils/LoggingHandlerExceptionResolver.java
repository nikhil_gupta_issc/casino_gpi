package com.gpi.utils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoggingHandlerExceptionResolver extends SimpleMappingExceptionResolver {

    private static final Logger logger = LoggerFactory.getLogger(LoggingHandlerExceptionResolver.class);

    private static final String JSON_CONTENT_TYPE = "application/json";

    private static final String ACCEPT_HEADER = "Accept";

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        logger.error("Unhandled exception on " + request.getRequestURI(), ex);
        ModelAndView mav = null;
        if (StringUtils.containsIgnoreCase(request.getContentType(), JSON_CONTENT_TYPE)
                || StringUtils.containsIgnoreCase(request.getHeader(ACCEPT_HEADER), JSON_CONTENT_TYPE)) {
            mav = ViewUtils.createJsonView();
            mav.addObject("success", false);
            mav.addObject("errorCode", "unexpected_error");
        } else {
            mav = super.resolveException(request, response, handler, ex);
        }
        return mav;
    }
}
