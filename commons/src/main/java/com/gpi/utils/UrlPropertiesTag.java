package com.gpi.utils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.UnsupportedEncodingException;

/**
 * User: igabbarini
 */
public class UrlPropertiesTag extends TagSupport {

    private static final Logger log = LoggerFactory.getLogger(UrlPropertiesTag.class);

    private static final long serialVersionUID = -8991584349982374293L;

    private String name;

    private String secureName;

    private String var;

    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

        String propertyName = name;

        boolean secure = isSecure(request);
        if (secure && secureName != null) {
            propertyName = secureName;
        }

        String url = ApplicationProperties.getProperty(propertyName);
        if (url != null) {
            if (secure) {
                url = StringUtils.replaceOnce(url, "http:", "https:");
            }

            try {
                url = new String(url.getBytes(), "UTF8");
            } catch (UnsupportedEncodingException e) {
                log.error("Error: ", e);
            }
        } else {
            throw new JspException("property " + propertyName + " is not defined");
        }

        pageContext.setAttribute(var, url);
        return SKIP_BODY;
    }

    private boolean isSecure(HttpServletRequest request) {
        return request.isSecure();
    }

    public void setVar(String var) {
        this.var = var;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSecureName(String secureName) {
        this.secureName = secureName;
    }


}
