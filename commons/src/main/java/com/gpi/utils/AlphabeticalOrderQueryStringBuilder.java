package com.gpi.utils;

import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Query String builder in alphabetical order
 * 
 * @author Alexandre
 *
 */
public class AlphabeticalOrderQueryStringBuilder {
	private Map<String, String> params;
	
	/**
	 * @return query string in the form of key1=value1&key2=value2 in alphabetical order
	 */
	public String build() {
		SortedSet<String> sortedKeys = new TreeSet<>(params.keySet());
		StringBuilder sb = new StringBuilder();
		
		for (String k : sortedKeys) {
			if (params.get(k) != null) {
				sb.append(k).append("=").append(params.get(k)).append("&");
			}
		}
		
		return sb.toString().replaceAll("&$", "");
	}
	
	public void setParams(Map<String, String> params) {
		this.params = params;
	}
}
