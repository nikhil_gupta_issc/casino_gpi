package com.gpi.utils;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import com.gpi.service.applicationproperty.ApplicationPropertyService;

/**
 * Lets us access property values from within the application.
 */
public class ApplicationProperties {


    protected static final String RELOADABLES_KEY = "application.properties.reloadables";
    private static final Logger log = LoggerFactory.getLogger(ApplicationProperties.class);
    //Singleton instance
    private static ApplicationProperties INSTANCE;
    //Elapsed time to reload the app reloadable properties. Is set with spring
    private static long refreshTime = 5 * 1000;
    //Elapsed time since last refresh
    protected long lastRefresh = 0;
    //Properties that are reloaded every refreshTime milliseconds
    private Properties applicationProperties;
    //Properties that are static and are loaded only once at app startup
    private Properties staticProperties;

    private ApplicationProperties() {
        reloadProperties();
    }

    public static void addProperty(String key, String value) {
        ApplicationProperties instance = ApplicationProperties.getInstance();
        instance.applicationProperties.put(key, value);
    }

    public static ApplicationProperties getInstance() {
        if (INSTANCE == null) {
            synchronized (ApplicationProperties.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ApplicationProperties();
                }
            }
        }
        long delta = System.currentTimeMillis() - INSTANCE.lastRefresh;
        if (delta > refreshTime) {
            synchronized (INSTANCE) {
                delta = System.currentTimeMillis() - INSTANCE.lastRefresh;
                INSTANCE.lastRefresh = System.currentTimeMillis();
            }
            if (delta > refreshTime) {
                INSTANCE.reloadProperties();
            } else {
                log.debug("Another thread is reloading the properties values");
            }
        }
        return INSTANCE;
    }

    public static String getProperty(String key) {
    	String property = getPropertyDbFirst(key);
    	return property;
    }

    public static Integer getIntProperty(String key) {
    	return Integer.valueOf(getPropertyDbFirst(key));
    }

    public static Double getDoubleProperty(String key) {
    	return Double.valueOf(getPropertyDbFirst(key));
    }

    public static Float getFloatProperty(String key) {
    	return Float.valueOf(getPropertyDbFirst(key));
    }

    public static boolean getBooleanProperty(String key) {
    	return Boolean.valueOf(getPropertyDbFirst(key));
    }

    public static Long getLongProperty(String key) {
    	return Long.valueOf(getPropertyDbFirst(key));
    }
    
    public static void setMillisecondsToRefresh(long refreshTimeMillis) {
        ApplicationProperties.refreshTime = refreshTimeMillis;
    }

    private synchronized void reloadProperties() {
        Properties props = new Properties();
        log.debug("Reloading reloadable properties files.");
        try {
            //Only reload static props once
            if (staticProperties == null || staticProperties.size() == 0) {
                staticProperties = new Properties();
                // these can't be reloaded, so don't waste time on them
                staticProperties.load(this.getClass().getClassLoader()
                        .getResourceAsStream("project.properties"));
                staticProperties.putAll(System.getProperties());
            }
            props.putAll(staticProperties);
            // get reloadable properties
            String reloadablesString = (String) props.get(RELOADABLES_KEY);
            if (reloadablesString != null && !reloadablesString.isEmpty()) {
                String[] reloadables = reloadablesString.split("[, ]+");
                ApplicationContext ac;
                try {
                    ac = ApplicationContextProvider.getInstance().getContext();
                    for (String file : reloadables) {
                        Resource resource = ac.getResource(file);
                        props.load(resource.getInputStream());
                    }
                } catch (RuntimeException e) {
                    log.warn("Error while trying to reload. Retry later", e);
                }
            }
        } catch (Exception e) {
            log.error("Could not load project.properties. ", e);
        }
        this.applicationProperties = props;
        this.lastRefresh = System.currentTimeMillis();
        log.debug("Finished reloading reloadable properties files.");
    }
    
    private static String getPropertyDbFirst(String key) {
    	ApplicationPropertyService applicationPropertyService = null;
    	try {
    		applicationPropertyService = (ApplicationPropertyService)ApplicationContextProvider.getInstance().getBean("applicationPropertyService");
    	}catch (Exception e) {
    		log.warn(e.getMessage(), e);
    	}
    	
    	String property = applicationPropertyService != null ? applicationPropertyService.getProperty(key) : null;
    	if (property == null) {
    		property = String.valueOf(getInstance().applicationProperties.get(key));
    	}
    	return property;
    }

}
