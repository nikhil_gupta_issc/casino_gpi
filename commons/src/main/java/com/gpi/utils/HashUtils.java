package com.gpi.utils;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hashing utils
 * 
 * @author Alexandre
 *
 */
public class HashUtils {
	private static final Logger logger = LoggerFactory.getLogger(HashUtils.class);

	/**
	 * @param message message to hash
	 * @return md5 hashed message
	 */
	public static String getMD5Hash(String message) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(message.getBytes());
			byte[] digest = md.digest();
			String myHash = DatatypeConverter.printHexBinary(digest).toLowerCase();

			return myHash;

		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}

		return null;
	}
	
	public static String getMD5Hash(InputStream is) {
		try {
			return DigestUtils.md5Hex(is);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}
}
