package com.gpi.utils;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Resolves a request locale
 */
public interface GpiLocaleResolver {

    /**
     * Gets the locale for the user. Searches first in our user locale defined in a cookie,
     * then in the browser and at last it defaults to
     * Guarantees a return.
     */
    Locale resolveLocale(HttpServletRequest request);

}
