package com.gpi.utils;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Log4jConfigurer;
import org.springframework.util.ResourceUtils;
import org.springframework.util.SystemPropertyUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.FileNotFoundException;
import java.util.Properties;

public class Log4jConfigListener implements ServletContextListener {

    public static final String RUNTIME_OVERRIDE_PREFIX = "CTX_RO_";

    /**
     * Parameter specifying the location of the log4j config file
     */
    public static final String CONFIG_LOCATION_PARAM = "log4jConfigLocation";

    /**
     * Parameter specifying the refresh interval for checking the log4j config file
     */
    public static final String REFRESH_INTERVAL_PARAM = "log4jRefreshInterval";

    /**
     * Parameter specifying whether to expose the web app root system property
     */
    public static final String EXPOSE_WEB_APP_ROOT_PARAM = "log4jExposeWebAppRoot";

    private Properties properties;

    public Log4jConfigListener() {
        properties = new Properties();
    }

    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();

        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("project.properties"));
        } catch (Exception e) {
            servletContext.log("Error opening project.properties for log4j initialization: " + e.getMessage());
        }

        initLogging(servletContext);
    }

    public void contextDestroyed(ServletContextEvent event) {
        shutdownLogging(event.getServletContext());
    }

    public String getInitParameterWithRuntimeOverride(ServletContext servletContext, String name) {
        String value = System.getenv(RUNTIME_OVERRIDE_PREFIX + name);

        if (StringUtils.isBlank(value)) {
            value = System.getProperty(RUNTIME_OVERRIDE_PREFIX + name);
        } else {
            servletContext.log("Initialization log4j param [" + name + "] retrieved from environment variable");
            return value;
        }

        if (StringUtils.isBlank(value)) {
            value = properties.getProperty(RUNTIME_OVERRIDE_PREFIX + name);
        } else {
            servletContext.log("Initialization log4j param [" + name + "] retrieved from system property");
            return value;
        }

        if (StringUtils.isBlank(value)) {
            value = servletContext.getInitParameter(name);
        } else {
            servletContext.log("Initialization log4j param [" + name + "] retrieved from project.properties file");
        }

        return value;
    }

    /**
     * Initialize log4j, including setting the web app root system property.
     *
     * @param servletContext the current ServletContext
     * @see WebUtils#setWebAppRootSystemProperty
     */
    public void initLogging(ServletContext servletContext) {
        // Expose the web app root system property.
        if (exposeWebAppRoot(servletContext)) {
            WebUtils.setWebAppRootSystemProperty(servletContext);
        }

        // Only perform custom log4j initialization in case of a config file.
        String location = getInitParameterWithRuntimeOverride(servletContext, CONFIG_LOCATION_PARAM);
        if (location != null) {
            // Perform actual log4j initialization; else rely on log4j's default initialization.
            try {
                // Return a URL (e.g. "classpath:" or "file:") as-is;
                // consider a plain file path as relative to the web application root directory.
                if (!ResourceUtils.isUrl(location)) {
                    // Resolve system property placeholders before resolving real path.
                    location = SystemPropertyUtils.resolvePlaceholders(location);
                    location = WebUtils.getRealPath(servletContext, location);
                }

                // Write log message to server log.
                servletContext.log("Initializing log4j from [" + location + "]");

                // Check whether refresh interval was specified.
                String intervalString = getInitParameterWithRuntimeOverride(servletContext, REFRESH_INTERVAL_PARAM);
                if (intervalString != null) {
                    // Initialize with refresh interval, i.e. with log4j's watchdog thread,
                    // checking the file in the background.
                    try {
                        long refreshInterval = Long.parseLong(intervalString);
                        Log4jConfigurer.initLogging(location, refreshInterval);
                    } catch (NumberFormatException ex) {
                        throw new IllegalArgumentException("Invalid 'log4jRefreshInterval' parameter: "
                                + ex.getMessage());
                    }
                } else {
                    // Initialize without refresh check, i.e. without log4j's watchdog thread.
                    Log4jConfigurer.initLogging(location);
                }
            } catch (FileNotFoundException ex) {
                throw new IllegalArgumentException("Invalid 'log4jConfigLocation' parameter: " + ex.getMessage());
            }
        }
    }

    /**
     * Shut down log4j, properly releasing all file locks and resetting the web app root system property.
     *
     * @param servletContext the current ServletContext
     * @see WebUtils#removeWebAppRootSystemProperty
     */
    public void shutdownLogging(ServletContext servletContext) {
        servletContext.log("Shutting down log4j");
        try {
            Log4jConfigurer.shutdownLogging();
        } finally {
            // Remove the web app root system property.
            if (exposeWebAppRoot(servletContext)) {
                WebUtils.removeWebAppRootSystemProperty(servletContext);
            }
        }
    }

    /**
     * Return whether to expose the web app root system property, checking the corresponding ServletContext init
     * parameter.
     *
     * @see #EXPOSE_WEB_APP_ROOT_PARAM
     */
    private boolean exposeWebAppRoot(ServletContext servletContext) {
        String exposeWebAppRootParam = getInitParameterWithRuntimeOverride(servletContext, EXPOSE_WEB_APP_ROOT_PARAM);
        return (exposeWebAppRootParam == null || Boolean.valueOf(exposeWebAppRootParam));
    }

}