package com.gpi.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Reloadable abstract xml parser. Configuration is injected with spring
 *
 * @author gwachnitz
 */
public abstract class AbstractXMLParser {

    private static final Logger log = LoggerFactory.getLogger(AbstractXMLParser.class);
    protected Resource configFilePath;

    protected abstract void parseDocument(Document dom);

    //Was used to get the xml for debugging purposes.
    protected char[] getXML() {
        File configFile;
        char[] target = new char[40000];
        try {
            configFile = configFilePath.getFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        FileReader reader;
        try {
            reader = new FileReader(configFile);

            reader.read(target);
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return target;
    }

    protected Document getDom(URL input) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            return db.parse(input.toURI().toString());
        } catch (ParserConfigurationException | SAXException | URISyntaxException | IOException pce) {
            throw new RuntimeException(pce);
        }
    }

    protected boolean getBoolean(String value, boolean defaultValue) {
        return value != null && !"".equals(value) ? Boolean.valueOf(value)
                : defaultValue;
    }

    protected int getInt(String value) {
        return getInt(value, 0);
    }

    protected int getInt(String value, int defaultValue) {
        return value != null && !"".equals(value) ? Integer.parseInt(value)
                : defaultValue;
    }

    protected Date getDate(String stringDate) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return df.parse(stringDate);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * This method is used by spring to load Definitions
     */
    public void init() {
        /**
         * This method is used by spring to load
         * definitions
         */
        URL configFile;
        try {
            log.info("loading file {}", configFilePath.getFilename());
            configFile = configFilePath.getURL();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Document dom = getDom(configFile);
        parseDocument(dom);

    }


    public void setConfigFilePath(Resource configFilePath) {
        this.configFilePath = configFilePath;
    }

}




