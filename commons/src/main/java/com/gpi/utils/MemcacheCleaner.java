package com.gpi.utils;

import org.apache.commons.net.telnet.TelnetClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import java.io.PrintStream;

/**
 * User: igabbarini
 */
public class MemcacheCleaner implements InitializingBean {
    private static final Logger log = LoggerFactory.getLogger(MemcacheCleaner.class);

    private PrintStream out;

    private boolean cleanCacheOnStartUp = false;

    private void write(String value) {
        try {
            out.println(value);
            out.flush();
            log.info("sending message [{}] to telnet to clear cache", value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (cleanCacheOnStartUp) {
            TelnetClient telnetClient = new TelnetClient();
            telnetClient.connect("localhost", 11211);
            out = new PrintStream(telnetClient.getOutputStream());
            write("flush_all");
            telnetClient.disconnect();
        } else {
            log.debug("Memcache won't be flushed.");
        }
    }

    public void setCleanCacheOnStartUp(boolean cleanCacheOnStartUp) {
        this.cleanCacheOnStartUp = cleanCacheOnStartUp;
    }
}
