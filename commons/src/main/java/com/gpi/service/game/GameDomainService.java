package com.gpi.service.game;


import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.dto.GameImage;
import com.gpi.dto.GameImageKey;
import com.gpi.dto.GamesImages;
import com.gpi.exceptions.GameProviderNotExistentException;
import com.gpi.exceptions.NonExistentGameException;

/**
 * User: Igabbarini
 */
public interface GameDomainService {


    /**
     * Adds new game to the list of available games.
     *
     * @param game
     * @param url
     * @param provider
     */
    void addGame(String game, String url, String provider) throws NonExistentGameException, GameProviderNotExistentException;

    Game createNewGame(String game, String provider) throws NonExistentGameException, GameProviderNotExistentException;

    void saveOrUpdateGame(Game game);

    /**
     * Get a list of all game registered
     *
     * @return
     */
    List<Game> getAvailableGames();
    
    List<Game> getAvailableGamesWithFreeSpins();

    Game getGameByName(String gameName) throws NonExistentGameException;

    void changeGame(String game, String newName, String newUrl, String newProvider) throws NonExistentGameException;

    Game getGameById(Long gameId) throws NonExistentGameException;

    void resetProvider(GameProvider gameProvider);

    void updateGame(Game game);
   
    List<Game> getGamesByProvider(Integer providerId);
    
    /** GAMES IMAGE METHODS **/ 
    
    GamesImages getGamesImageUrls();
    
    GameImage getGameImage(GameImageKey key);
    
    void saveImage(GameImageKey key, CommonsMultipartFile file) throws IOException;
   
}
