package com.gpi.service.game;


import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.game.PlayingType;
import com.gpi.domain.game.PublisherGameMapping;
import com.gpi.domain.publisher.Publisher;

import java.util.List;

/**
 * User: Igabbarini
 */
public interface PublisherGameService {

    public boolean isPlayingTypeAllowed(Publisher publisher, Game game, PlayingType playingType);

    public List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher);

    List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher, GameProvider provider);
    
    public void changePublisherGameModes(Publisher publisher, Game game, boolean free, boolean charged);

    public void changePublisherGameModes(Long pubGameId, boolean free, boolean charged);

    public Long addPublisherGame(PublisherGameMapping publisherGameMapping);

    List<Game> getAvailableGamesForPublisher(Publisher publisherName);
    
    public PublisherGameMapping getById(Long id);
    
    void deleteGameMapping(Long id);
    
    void saveByPublisherId(Integer publisherId);
    
    void saveByGameId(Long gameId);

    void saveByPublisherAndProvider(Integer publisherId, Integer providerId);
    
    void deleteByPublisherAndProvider(Integer publisherId, Integer providerId);
}
