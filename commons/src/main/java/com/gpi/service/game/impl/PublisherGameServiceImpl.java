package com.gpi.service.game.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gpi.dao.game.PublisherGameDao;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.game.PlayingType;
import com.gpi.domain.game.PublisherGameMapping;
import com.gpi.domain.publisher.Publisher;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.game.PublisherGameService;
import com.gpi.service.publisher.PublisherService;
import com.gpi.utils.ApplicationProperties;

/**
 * User: Igabbarini
 */
public class PublisherGameServiceImpl implements PublisherGameService {

    private static final Logger log = LoggerFactory.getLogger(PublisherGameServiceImpl.class);
    private PublisherGameDao publisherGameDao;
    private GameDomainService gameDomainService;
    private PublisherService publisherService;
    
    @Override
    @Transactional
    public boolean isPlayingTypeAllowed(Publisher publisher, Game game, PlayingType playingType) {
        log.debug("Checking if the PlayingType is allowed. PlayingType={}", playingType);

        boolean testEnvironment = ApplicationProperties.getBooleanProperty("test.environment");
        if (testEnvironment) {
            log.debug("Running in test Environment any type is allowed.");
            return true;
        }

        if (playingType.equals(PlayingType.FREE) && publisher.getApiName().equalsIgnoreCase("free")) {
            log.debug("Using free publisher so FREE type is allowed.");
            return true;
        }
        PublisherGameMapping publisherGameMapping = publisherGameDao.getPublisherGameMappingByGame(publisher, game);
        if (publisherGameMapping == null) {
            throw new RuntimeException(String.format("The publisher [%s] wasn't associated to the game=%s", publisher, game));
        }
        
        if (playingType.equals(PlayingType.CHARGED)) {
            log.debug("Playing the game in Charged mode.");
            return publisherGameMapping.isCharged();
        } else if (playingType.equals(PlayingType.FREE)) {
            log.debug("Playing the game in FREE type mode.");
            return publisherGameMapping.isFree();
        }

        return true;
    }

    @Override
    @Transactional
    public void changePublisherGameModes(Publisher publisher, Game game, boolean free, boolean charged) {
        log.info("Changing game modes for publisher={} and game=[{}]. Setting free=[{}], charged=[{}]", publisher, game, free, charged);
        PublisherGameMapping publisherGameMapping = publisherGameDao.getPublisherGameMappingByGame(publisher, game);
        if (publisherGameMapping == null) {
            throw new RuntimeException(String.format("The publisher [%s] wasn't associated to the game=%s", publisher, game));
        }
        
        if ((publisherGameMapping.isCharged() == charged) && (publisherGameMapping.isFree() == free)) {
            log.info("The modes won't be modified because are the same as stored.");
            return;
        }

        publisherGameMapping.setCharged(charged);
        publisherGameMapping.setFree(free);

        publisherGameDao.savePublisherGameMapping(publisherGameMapping);

    }

    @Override
    @Transactional
    public void changePublisherGameModes(Long pubGameId, boolean free, boolean charged) {
        log.info("Changing game modes for pubGameId=[{}]. Setting free=[{}], charged=[{}]", pubGameId, free, charged);
        PublisherGameMapping publisherGameMapping = publisherGameDao.getPublisherGameMappingById(pubGameId);
        if ((publisherGameMapping.isCharged() == charged) && (publisherGameMapping.isFree() == free)) {
            log.info("The modes won't be modified because are the same as stored.");
            return;
        }

        publisherGameMapping.setCharged(charged);
        publisherGameMapping.setFree(free);

        publisherGameDao.savePublisherGameMapping(publisherGameMapping);
    }

    @Override
    public List<Game> getAvailableGamesForPublisher(Publisher publisher) {
        log.debug("Getting all available games for publisher=[{}]", publisher.getName());
        List<PublisherGameMapping> publisherGameMappings = publisherGameDao.getPublisherGameMappings(publisher);
        List<Game> games = new ArrayList<>();
        for (PublisherGameMapping mapping : publisherGameMappings) {
            if (mapping.isFree() || mapping.isCharged()) {
                games.add(mapping.getGame());
            }
        }
        return games;
    }

    @Override
    @Transactional
    public Long addPublisherGame(PublisherGameMapping publisherGameMapping) {
        log.info("Adding publisherGameMapping=[{}]", publisherGameMapping);
        return publisherGameDao.savePublisherGameMapping(publisherGameMapping);
    }

    @Override
    @Transactional
    public void deleteGameMapping(Long id) {
        log.info("deleting publisher game id=[{}]", id);
        publisherGameDao.delete(id);
    }
    
    @Override
    public List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher) {
        log.info("Getting all game mappings for publisher=[]", publisher);
        return publisherGameDao.getPublisherGameMappings(publisher);
    }

    @Override
    public List<PublisherGameMapping> getPublisherGameMappings(Publisher publisher, GameProvider provider) {
        log.info("Getting all game mappings for publisher=[{}], provider=[{}]", publisher, provider);
        return publisherGameDao.getPublisherGameMappings(publisher, provider);
    }
    
    @Override
    @Transactional
    public PublisherGameMapping getById(Long id){
    	return publisherGameDao.getPublisherGameMappingById(id);
    }
    
    @Override
    @Transactional
	public void saveByPublisherId(Integer publisherId) {
		List<Game> availableGames = gameDomainService.getAvailableGames();
		
		List<PublisherGameMapping> publisherGameMappings = this.getPublisherGameMappings(new Publisher(publisherId));
		
		for (Game game : availableGames) {
        	
            if (hasPublisherGameMapped(game, publisherGameMappings)) {
                continue;
            }
        	PublisherGameMapping gameMapping = new PublisherGameMapping();
            gameMapping.setCharged(true);
            gameMapping.setFree(true);
            gameMapping.setPublisher(new Publisher(publisherId));
            gameMapping.setGame(game);
            log.debug("Adding mapping: {}", gameMapping);
            this.addPublisherGame(gameMapping);
        }
	}
    	
	@Override
	@Transactional
	public void saveByGameId(Long gameId) {
		List<Publisher> publisherList = publisherService.getPublisherList();
        for (Publisher publisher : publisherList) {
            Game game = new Game();
            game.setId(gameId);
            
            PublisherGameMapping publisherGameMapping = publisherGameDao.getPublisherGameMappingByGame(publisher, game);
            
            if (publisherGameMapping != null) {
                continue;
            }
        	PublisherGameMapping gameMapping = new PublisherGameMapping();
            gameMapping.setCharged(true);
            gameMapping.setFree(true);
            gameMapping.setPublisher(publisher);
            gameMapping.setGame(game);
            this.addPublisherGame(gameMapping);
        }
	}
    
    @Override
    @Transactional
	public void saveByPublisherAndProvider(Integer publisherId, Integer providerId) {
		
    	List<Game> games = gameDomainService.getGamesByProvider(providerId);
		
		List<PublisherGameMapping> publisherGameMappings = this.getPublisherGameMappings(new Publisher(publisherId), new GameProvider(providerId));
		
		for (Game game : games) {
        	
            if (hasPublisherGameMapped(game, publisherGameMappings)) {
                continue;
            }
        	PublisherGameMapping gameMapping = new PublisherGameMapping();
            gameMapping.setCharged(true);
            gameMapping.setFree(true);
            gameMapping.setPublisher(new Publisher(publisherId));
            gameMapping.setGame(game);
            log.debug("Adding mapping: {}", gameMapping);
            this.addPublisherGame(gameMapping);
        }
	}
	
	@Override
	@Transactional
    public void deleteByPublisherAndProvider(Integer publisherId, Integer providerId) {
		List<PublisherGameMapping> publisherGameMappings = this.getPublisherGameMappings(new Publisher(publisherId), new GameProvider(providerId));
		
		for (PublisherGameMapping gameMapping : publisherGameMappings) {
          	this.deleteGameMapping(gameMapping.getId());
        }
	}
	
    private boolean hasPublisherGameMapped(Game newGame, List<PublisherGameMapping> publisherGameMappings) {
        for (PublisherGameMapping mapping : publisherGameMappings) {
            if (mapping.getGame().getId().equals(newGame.getId())) {
                log.debug("The publisher already contains the game mapped.");
                return true;
            }
        }
        return false;
    }
	
    public void setPublisherGameDao(PublisherGameDao publisherGameDao) {
        this.publisherGameDao = publisherGameDao;
    }

	public void setGameDomainService(GameDomainService gameDomainService) {
		this.gameDomainService = gameDomainService;
	}

	public void setPublisherService(PublisherService publisherService) {
		this.publisherService = publisherService;
	}	
	
}
