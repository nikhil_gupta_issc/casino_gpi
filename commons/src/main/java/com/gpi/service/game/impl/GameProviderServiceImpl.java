package com.gpi.service.game.impl;

import com.gpi.dao.game.GameProviderDao;
import com.gpi.domain.game.GameProvider;
import com.gpi.exceptions.GameProviderNotExistentException;
import com.gpi.exceptions.InvalidCredentialsException;
import com.gpi.service.game.GameDomainService;
import com.gpi.service.game.GameProviderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nacho on 2/20/14.
 */
public class GameProviderServiceImpl implements GameProviderService {

    private static final Logger logger = LoggerFactory.getLogger(GameProviderServiceImpl.class);
    private GameProviderDao gameProviderDao;
    private GameDomainService gameDomainService;

    @Override
    @Transactional
    public void updateProvider(GameProvider gameProvider, String newName, String loginName, String newPassword, String newPrefix, String newApi, boolean hasExtendedInfo) {
        logger.info("Editing provider=[{}] with new name={}, new loginName={}, new password={} and new prefix={}", gameProvider, newName, loginName, newPassword, newPrefix);
        gameProvider.setName(newName);
        gameProvider.setPassword(newPassword);
        gameProvider.setLoginName(loginName);
        
        newPrefix = this.getCleanupPrefix(newPrefix);
        gameProvider.setPrefix(newPrefix);
        
        gameProvider.setApiName(newApi);
        gameProvider.setExtendedInfo(hasExtendedInfo);
        gameProviderDao.updateGameProvider(gameProvider);
        gameDomainService.resetProvider(gameProvider);
    }

    @Override
    public void checkCredentials(GameProvider provider, String login, String password) throws InvalidCredentialsException, GameProviderNotExistentException {
        logger.debug("Verifying credentials... username={} and password={}.", login, password);
        if (!provider.getPassword().equals(password) || !provider.getLoginName().equals(login)) {
            logger.error("expected username={} received={} -- expected password={} received={}",
                    provider.getLoginName(), login, provider.getPassword(), password);
            throw new InvalidCredentialsException("Invalid Credentials");
        }
        logger.info("The credentials are right.");
    }

    @Override
    @Transactional
    public GameProvider createGameProvider(String name, String loginName, String password, String prefix, String apiName, boolean hasExtendedInfo) {
        logger.debug("Creating GameProvider with name={}, loginName={} and password={}", name, loginName, password);
        GameProvider provider = new GameProvider();
        provider.setName(name);
        provider.setLoginName(loginName);
        provider.setPassword(password);
        
        prefix = this.getCleanupPrefix(prefix);
        provider.setPrefix(prefix);
        
        provider.setApiName(apiName);
        provider.setExtendedInfo(hasExtendedInfo);
        return gameProviderDao.saveGameProvider(provider);
    }

    @Override
    @Transactional
    public List<GameProvider> getProviders() {
        logger.debug("Getting all providers");
        return gameProviderDao.getGameProviders();
    }

    @Override
    @Transactional
    public GameProvider getGameProvider(String provider) throws GameProviderNotExistentException {
        logger.debug("getting GameProvider with name={}", provider);
        return gameProviderDao.getGameProvider(provider);
    }

    public void setGameProviderDao(GameProviderDao gameProviderDao) {
        this.gameProviderDao = gameProviderDao;
    }

    public void setGameDomainService(GameDomainService gameDomainService) {
        this.gameDomainService = gameDomainService;
    }
    
    private String getCleanupPrefix(String prefix) {
    	if (prefix == null || "".equals(prefix.trim())) {
    		return null;
    	}
    	
    	return prefix.trim();
    }
}
