package com.gpi.service.game;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.gpi.dao.game.GameDao;
import com.gpi.dao.imagerepository.ImageRepository;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.dto.GameImage;
import com.gpi.dto.GameImageKey;
import com.gpi.dto.GamesImages;
import com.gpi.exceptions.GameProviderNotExistentException;
import com.gpi.exceptions.NonExistentGameException;

/**
 * User: igabbarini
 */
@Transactional
public class GameDomainServiceImpl implements GameDomainService {

    private static final Logger log = LoggerFactory.getLogger(GameDomainServiceImpl.class);
    private GameDao gameDao;
    private PublisherGameService publisherGameService;
    private GameProviderService gameProviderService;
    private ImageRepository imageRepository;

    @Override
    public List<Game> getAvailableGames() {
        log.debug("Getting all available games");
        return gameDao.getGames();
    }

    @Override
    public Game getGameByName(String gameName) throws NonExistentGameException {
        log.debug("Getting game with name={}", gameName);
        return gameDao.getGame(gameName);
    }

    @Override
    public Game createNewGame(String game, String provider) throws NonExistentGameException, GameProviderNotExistentException {
        log.debug("Adding a game={}", game);
        List<Game> games = gameDao.getGames();
        Game newGame = new Game();
        newGame.setName(game);
        if (provider != null) {
            GameProvider gameProvider = gameProviderService.getGameProvider(provider);
            newGame.setGameProvider(gameProvider);
        }
        Game game1;
        if (games.contains(newGame)) {
            log.debug("Game [{}] already exists in the database", game);
            game1 = gameDao.getGame(game);
        } else {
            game1 = gameDao.addGame(newGame);
        }
        return game1;
    }

    @Override
    @Transactional
    public void saveOrUpdateGame(Game game) {
        log.info("Saving game = {}", game);
        boolean insert = game.getId() == null;
        gameDao.saveGame(game);
        if (insert)
        	publisherGameService.saveByGameId(game.getId());
    }

    @Override
    @Transactional
    public void addGame(String game, String url, String provider) throws NonExistentGameException, GameProviderNotExistentException {
        log.debug("Adding a game={}", game);
        List<Game> games = gameDao.getGames();
        Game newGame = new Game();
        newGame.setName(game);
        newGame.setUrl(url);
        if (provider != null) {
            GameProvider gameProvider = gameProviderService.getGameProvider(provider);
            newGame.setGameProvider(gameProvider);
        }
        Game game1;
        if (games.contains(newGame)) {
            log.debug("Game [{}] already exists in the database", game);
            game1 = gameDao.getGame(game);
        } else {
            game1 = gameDao.addGame(newGame);
        }
        publisherGameService.saveByGameId(game1.getId());
    }

    @Override
    @Transactional
    public void changeGame(String game, String newName, String newUrl, String newProvider) throws NonExistentGameException {
        Game game1 = gameDao.getGame(game);
        log.debug("Changing game={}", game1);
        game1.setName(newName);
        game1.setUrl(newUrl);
        GameProvider gameProvider = null;
        try {
            gameProvider = gameProviderService.getGameProvider(newProvider);
        } catch (GameProviderNotExistentException e) {
            log.warn("GameProvider with name={} does not exists. Setting new provider to null", newProvider);
        }
        game1.setGameProvider(gameProvider);
        log.debug("For this Game={}", game1);
        gameDao.updateGame(game1);
    }

    @Override
    public Game getGameById(Long gameId) throws NonExistentGameException {
        log.debug("Looking for game with id={}", gameId);
        return gameDao.getGame(gameId);
    }

    @Override
    public void resetProvider(GameProvider gameProvider) {
        log.debug("Reset the cache for all games in order to get the new provider values.");
        List<Game> gamesByProvider = gameDao.findGamesByProvider(gameProvider.getId());
        for (Game game : gamesByProvider) {
            game.setGameProvider(gameProvider);
            gameDao.updateGame(game);
        }
    }

    @Override
    public void updateGame(Game game) {
        log.debug("Updating game={}", game);
        gameDao.updateGame(game);
    }
    
    @Override
	public List<Game> getAvailableGamesWithFreeSpins() {
		return gameDao.getGamesWithFreeSpins();
	}
    
    @Override
    public List<Game> getGamesByProvider(Integer providerId){
    	return gameDao.findGamesByProvider(providerId);
    }
    
    /** GAMES IMAGE METHODS **/ 
    
    @Override 
    public GamesImages getGamesImageUrls(){
    	return imageRepository.getAll();
    }
    
    @Override
    public GameImage getGameImage(GameImageKey key) {
    	GameImage gameImage = imageRepository.get(key);
    	if (gameImage == null) {
    		key = key.generateAlternativeKey();
    		if (key != null) {
    			this.getGameImage(key);
    		}
    	}
    	return gameImage;
    }
    
    @Override
	public void saveImage(GameImageKey key, CommonsMultipartFile file) throws IOException {
		if (file != null && !file.isEmpty()) {
			imageRepository.save(key, file);
		}
	}
    
    public void setGameDao(GameDao gameDao) {
        this.gameDao = gameDao;
    }

    public void setPublisherGameService(PublisherGameService publisherGameService) {
        this.publisherGameService = publisherGameService;
    }

    public void setGameProviderService(GameProviderService gameProviderService) {
        this.gameProviderService = gameProviderService;
    }

	public void setImageRepository(ImageRepository imageRepository) {
		this.imageRepository = imageRepository;
	}
    
}
