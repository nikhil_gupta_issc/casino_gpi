package com.gpi.service.game;

import com.gpi.domain.game.GameProvider;
import com.gpi.exceptions.GameProviderNotExistentException;
import com.gpi.exceptions.InvalidCredentialsException;

import java.util.List;

/**
 * Created by Nacho on 2/20/14.
 */
public interface GameProviderService {

    public GameProvider createGameProvider(String name, String loginName, String password, String prefix, String apiName, boolean hasExtendedInfo);

    List<GameProvider> getProviders();

    GameProvider getGameProvider(String provider) throws GameProviderNotExistentException;

    void checkCredentials(GameProvider gameProvider, String login, String password) throws InvalidCredentialsException, GameProviderNotExistentException;

    void updateProvider(GameProvider gameProvider, String newName, String loginName, String newPassword, String newPrefix, String newApi, boolean hasExtendedInfo);
}
