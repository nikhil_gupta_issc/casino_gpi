package com.gpi.service.social;

/**
 * Created by Nacho on 6/10/2014.
 */
public interface SocialPlayerService {

    public void saveSocialPlayer(String token, Long playerId);

    public Long getSocialPlayer(String token);
}
