package com.gpi.service.social;

import com.gpi.constants.CacheConstants;
import com.gpi.utils.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Nacho on 6/10/2014.
 */
public class SocialPlayerServiceImpl implements SocialPlayerService {

    private static final Logger log = LoggerFactory.getLogger(SocialPlayerServiceImpl.class);
    private CacheManager cacheManager;

    @Override
    public void saveSocialPlayer(String token, Long playerId) {
        log.debug("Save player={} into cache with token={}", playerId, token);
        cacheManager.store(getKey(token), playerId, CacheConstants.SOCIAL_PLAYER_TTL);
    }

    private String getKey(String token) {
        return CacheConstants.SOCIAL_PLAYER_KEY + token;
    }

    @Override
    public Long getSocialPlayer(String token) {
        log.debug("Get player with token={}", token);
        return (Long) cacheManager.get(getKey(token));
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
}
