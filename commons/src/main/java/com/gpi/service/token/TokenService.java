package com.gpi.service.token;

import com.gpi.exceptions.InvalidTokenException;
import org.apache.commons.lang.StringUtils;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * User: igabbarini
 */
public class TokenService {

    private static final String TOKEN_EXP = "[0-9a-fA-F]{8}(?:-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12}";
    private static final Pattern TOKEN_EXP_PATTERN = Pattern.compile(TOKEN_EXP);

    private static final long serialVersionUID = 6209336301906389291L;

    public static void validateToken(String token) throws InvalidTokenException {
        if (!isValidToken(token)) {
            throw new InvalidTokenException(token);
        }
    }

    public static String generateToken() {
        return UUID.randomUUID().toString();
    }

    public static boolean isValidToken(String token) {
        return StringUtils.isNotEmpty(token) && TOKEN_EXP_PATTERN.matcher(token).matches();
    }

}
