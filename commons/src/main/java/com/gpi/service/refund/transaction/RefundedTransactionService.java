package com.gpi.service.refund.transaction;

import com.gpi.domain.audit.RefundedTransaction;
import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;

import java.util.List;

/**
 * Created by Nacho on 12/2/2014.
 */
public interface RefundedTransactionService {

    RefundedTransaction saveRefundedTransaction(String gameTransactionId, Transaction transaction, Round round);

    RefundedTransaction getRefundedTransaction(String gameTransactionId, Transaction transaction);

    List<RefundedTransaction> getRefundedTransactions(Long roundId);
}
