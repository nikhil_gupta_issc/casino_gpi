package com.gpi.service.refund.transaction.impl;

import com.gpi.dao.refund.transaction.RefundTransactionDao;
import com.gpi.domain.audit.RefundedTransaction;
import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.service.refund.transaction.RefundedTransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nacho on 12/2/2014.
 */
public class RefundedTransactionServiceImpl implements RefundedTransactionService {

    private static final Logger logger = LoggerFactory.getLogger(RefundedTransactionServiceImpl.class);

    private RefundTransactionDao refundTransactionDao;

    @Override
    @Transactional
    public RefundedTransaction saveRefundedTransaction(String gameTransactionId, Transaction transaction, Round round) {
        logger.debug("Creating refundedTransaction with gameTransactionId={} and transaction={}", gameTransactionId, transaction.getId());
        RefundedTransaction refundedTransaction = new RefundedTransaction();
        refundedTransaction.setGameTransactionId(gameTransactionId);
        refundedTransaction.setTransaction(transaction);
        refundedTransaction.setRound(round);
        refundedTransaction = refundTransactionDao.saveRefundedTransaction(refundedTransaction);
        return refundedTransaction;
    }

    @Override
    @Transactional
    public RefundedTransaction getRefundedTransaction(String gameTransactionId, Transaction transaction) {
        logger.debug("Getting refundedTransaction for gameTransactionId={} and transaction={}", gameTransactionId, transaction.getId());
        return refundTransactionDao.getRefundedTransaction(gameTransactionId, transaction);
    }

    @Override
    @Transactional
    public List<RefundedTransaction> getRefundedTransactions(Long roundId) {
        logger.debug("Getting all refunded transactions for round={}", roundId);
        return refundTransactionDao.getRefundedTransactions(roundId);
    }

    public void setRefundTransactionDao(RefundTransactionDao refundTransactionDao) {
        this.refundTransactionDao = refundTransactionDao;
    }
}
