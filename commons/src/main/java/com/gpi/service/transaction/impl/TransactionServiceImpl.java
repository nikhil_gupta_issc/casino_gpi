package com.gpi.service.transaction.impl;

import com.gpi.dao.transaction.TransactionDao;
import com.gpi.domain.audit.RefundedTransaction;
import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.Game;
import com.gpi.domain.transaction.TransactionFilter;
import com.gpi.domain.transaction.TransactionType;
import com.gpi.exceptions.NoExistentTransactionException;
import com.gpi.exceptions.TransactionAlreadyProcessedException;
import com.gpi.service.audit.AccumulatedStatsService;
import com.gpi.service.refund.transaction.RefundedTransactionService;
import com.gpi.service.round.RoundService;
import com.gpi.service.transaction.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Nacho on 11/27/2014.
 */
public class TransactionServiceImpl implements TransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);
    private TransactionDao transactionDao;
    private RoundService roundService;
    private RefundedTransactionService refundedTransactionService;
    private AccumulatedStatsService accumulatedStatsService;

    @Override
    @Transactional
    public Transaction getTransaction(String providerTransaction, Round round) {
        logger.debug("Looking for transaction with providerTransaction={} and round={}", providerTransaction, round.getId());
        return transactionDao.getTransaction(providerTransaction, round);
    }

    @Override
    @Transactional
    public Transaction registerBet(String gameTransactionId, Round round, Long amount) {
        logger.debug("Registering spin transaction.");
        Transaction t = new Transaction();
        t.setRound(round);
        t.setCreatedOn(new Date());
        t.setType(TransactionType.BET);
        t.setAmount(amount);
        t.setGameTransactionId(gameTransactionId);
        t.setExtTransactionId(null);
        round.setBetAmount(round.getBetAmount() + amount);
        roundService.updateRound(round);
        accumulatedStatsService.updateBetAmount(round.getGame(),round.getPlayer(),round.getPublisher(),amount);
        return transactionDao.saveTransaction(t);
    }

    @Override
    @Transactional
    public Transaction registerWin(String gameTransactionId, Round round, Long amount) {
        logger.debug("Registering win transaction.");
        Transaction t = new Transaction();
        t.setRound(round);
        t.setCreatedOn(new Date());
        t.setType(TransactionType.WIN);
        t.setAmount(amount);
        t.setGameTransactionId(gameTransactionId);
        t.setExtTransactionId(null);
        round.setWinAmount(round.getWinAmount() + amount);
        roundService.updateRound(round);
        accumulatedStatsService.updateWinAmount(round.getGame(),round.getPlayer(),round.getPublisher(),amount);
        return transactionDao.saveTransaction(t);
    }

    @Override
    @Transactional
    public Transaction registerRefundTransaction(String gameTransactionId, Round round, Long amount, String refundedTransactionId) throws TransactionAlreadyProcessedException, NoExistentTransactionException {
        logger.debug("Registering type transaction.");
        Transaction refundedTransaction = transactionDao.getTransaction(refundedTransactionId, round);
        if (refundedTransaction == null) {
            throw new NoExistentTransactionException("Transaction with id=" + refundedTransactionId + " no exists.");
        }
        RefundedTransaction refundedTransaction1 = refundedTransactionService.getRefundedTransaction(refundedTransactionId, refundedTransaction);
        if (refundedTransaction1 == null) {
            Transaction t = new Transaction();
            t.setRound(round);
            t.setCreatedOn(new Date());
            Transaction transaction = transactionDao.getTransaction(refundedTransactionId, round);
            if (transaction.getType().equals(TransactionType.BET)) {
                t.setType(TransactionType.BET_CORRECTION);
                round.setBetAmount(round.getBetAmount() - amount);
                accumulatedStatsService.updateBetAmount(round.getGame(), round.getPlayer(), round.getPublisher(), -amount);
            } else if (transaction.getType().equals(TransactionType.WIN)) {
                t.setType(TransactionType.WIN_CORRECTION);
                round.setWinAmount(round.getWinAmount() - amount);
                accumulatedStatsService.updateWinAmount(round.getGame(), round.getPlayer(), round.getPublisher(), -amount);
            }
            t.setAmount(amount);
            t.setGameTransactionId(gameTransactionId);
            t.setExtTransactionId(null);
            t = transactionDao.saveTransaction(t);
            roundService.updateRound(round);
            refundedTransactionService.saveRefundedTransaction(refundedTransactionId, transaction, round);
            return t;
        } else {
            throw new TransactionAlreadyProcessedException("", refundedTransaction);
        }

    }

    @Override
    @Transactional
    public Transaction registerWinCorrection(String gameTransactionId, Round round, Long amount) {
        logger.debug("Registering win correction.");
        Transaction t = new Transaction();
        t.setRound(round);
        t.setCreatedOn(new Date());
        t.setType(TransactionType.WIN_CORRECTION);
        t.setAmount(amount);
        t.setGameTransactionId(gameTransactionId);
        t.setExtTransactionId(null);
        round.setWinAmount(round.getWinAmount() - amount);
        accumulatedStatsService.updateWinAmount(round.getGame(), round.getPlayer(), round.getPublisher(), -amount);
        roundService.updateRound(round);
        return transactionDao.saveTransaction(t);
    }

    @Override
    @Transactional
    public void updateTransaction(Transaction transaction) {
        transactionDao.updateTransaction(transaction);
    }

    @Override
    @Transactional
    public List<Transaction> getTransactionsByRoundId(Integer startIndex, Integer pageSize, String sortExpression, Long roundId) {
        logger.debug("Getting all transactions for round={}", roundId);
        return transactionDao.getTransactionsByRound(roundId, startIndex, pageSize, sortExpression);
    }
    
    @Override
    @Transactional
    public Long count(Long roundId, Integer startIndex, Integer pageSize, String sortExpression) {
        logger.debug("Counting de amount of transactions for round={}", roundId);
        return transactionDao.count(roundId,startIndex,pageSize,sortExpression);
    }

    @Override
    @Transactional
    public Transaction getTransaction(String providerTransaction, String gameName) {
        logger.debug("Looking for transaction with providerTransaction={} and gameName={}", providerTransaction, gameName);
        return transactionDao.getTransaction(providerTransaction, gameName);
    }
    
	@Override
    @Transactional
	public List<Transaction> getTransactionsByGameRoundId(Integer startIndex, Integer pageSize, Long gameRoundId, Game game) {
		 logger.debug("Getting all transactions for gameRound={}", gameRoundId);
	     return transactionDao.getTransactionsByGameRound(gameRoundId, startIndex, pageSize, game);
	}

	@Override
	@Transactional
	public List<Transaction> getTransactions(TransactionFilter filter) {
		 logger.debug("Getting all transactions for filter={}", filter.toString());
	     return transactionDao.getTransactions(filter);
	}
	
    @Override
    @Transactional
    public Long countByGameRound(Long gameRoundId) {
        logger.debug("Counting de amount of transactions for gameRoundId={}", gameRoundId);
        return transactionDao.countByGameRound(gameRoundId);
    }
	
    public void setTransactionDao(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

    public void setRoundService(RoundService roundService) {
        this.roundService = roundService;
    }

    public void setRefundedTransactionService(RefundedTransactionService refundedTransactionService) {
        this.refundedTransactionService = refundedTransactionService;
    }

    public void setAccumulatedStatsService(AccumulatedStatsService accumulatedStatsService) {
        this.accumulatedStatsService = accumulatedStatsService;
    }
}
