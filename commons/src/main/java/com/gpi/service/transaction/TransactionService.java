package com.gpi.service.transaction;

import java.util.List;

import com.gpi.domain.audit.Round;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.game.Game;
import com.gpi.domain.transaction.TransactionFilter;
import com.gpi.exceptions.NoExistentTransactionException;
import com.gpi.exceptions.TransactionAlreadyProcessedException;

/**
 * Created by Nacho on 11/27/2014.
 */
public interface TransactionService {

    Transaction getTransaction(String providerTransaction, Round round);

    Transaction registerBet(String gameTransactionId, Round round, Long amount);

    Transaction registerWin(String gameTransactionId, Round round, Long amount);

    Transaction registerRefundTransaction(String gameTransactionId, Round round, Long amount, String refundedTransactionId) throws TransactionAlreadyProcessedException, NoExistentTransactionException;

    Transaction registerWinCorrection(String gameTransactionId, Round round, Long amount);

    void updateTransaction(Transaction transaction);

    List<Transaction> getTransactionsByRoundId(Integer startIndex, Integer pageSize, String sortExpression, Long roundId);

    List<Transaction> getTransactionsByGameRoundId(Integer startIndex, Integer pageSize, Long gameRoundId, Game game);
    
    public Long count(Long roundId, Integer startIndex, Integer pageSize, String sortExpression);
    
    Transaction getTransaction(String providerTransaction, String gameName);
    
    List<Transaction> getTransactions(TransactionFilter filter);
    
    Long countByGameRound(Long gameRoundId);
}
