package com.gpi.service.publisher;


import com.gpi.domain.publisher.GameParameters;

/**
 * Some {@link com.gpi.domain.publisher.Publisher}s require extra parameters in order to establish a
 * proper communication with their servers. Since we have a two stage loading
 * process: load game (game.do), game session creation (requestGame.do), the
 * extra parameters are hold temporally between those requests in order to save
 * them in the appropriate {@link com.gpi.service.gamesession.Session} for further use
 *
 * @author csanchez
 */
public interface GameParametersService {

    /**
     * Saves the extra parameters required by a publisher
     *
     * @param params        the extra parameters required
     * @param token         the token associated with the player
     * @param publisherName the name of publisher which is requesting the game
     */
    public void save(GameParameters params, String token, String publisherName);

    /**
     * Retrieves the extra parameters for a specific token and publisher
     *
     * @param token         the token associated with the player
     * @param publisherName the name of publisher which is requesting the game
     * @return a valid object
     */
    public GameParameters get(String token, String publisherName);

}
