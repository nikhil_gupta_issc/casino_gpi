package com.gpi.service.publisher.impl;

import com.gpi.constants.CacheConstants;
import com.gpi.domain.publisher.GameParameters;
import com.gpi.service.publisher.GameParametersService;
import com.gpi.utils.cache.CacheManager;

public class GameParametersServiceImpl implements GameParametersService {

    private CacheManager cacheManager;

    @Override
    public void save(GameParameters params, String token, String publisherName) {
        cacheManager.store(getKey(token, publisherName), params, CacheConstants.GAME_PARAMS_TTL);
    }

    @Override
    public GameParameters get(String token, String publisherName) {
        GameParameters params = (GameParameters) cacheManager.get(getKey(token, publisherName));
        return params != null ? params : new GameParameters();
    }

    private String getKey(String token, String publisherName) {
        return CacheConstants.GAME_PARAMS_KEY + publisherName + "-" + token;
    }

    /**
     * @param cacheManager the cacheManager to set
     */
    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

}
