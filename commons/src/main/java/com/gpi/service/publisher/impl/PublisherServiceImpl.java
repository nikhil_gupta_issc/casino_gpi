package com.gpi.service.publisher.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gpi.dao.publisher.PublisherDao;
import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.InvalidUrlException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.exceptions.PasswordException;
import com.gpi.exceptions.UserAuthenticationException;
import com.gpi.service.game.PublisherGameService;
import com.gpi.service.publisher.PublisherService;

public class PublisherServiceImpl implements PublisherService {

    private static final Logger log = LoggerFactory.getLogger(PublisherServiceImpl.class);

    private PublisherDao publisherDao;

    private PublisherGameService publisherGameService;

    @Override
    @Transactional
    public Publisher findByNameAndPassword(String name, String password) throws UserAuthenticationException, NonExistentPublisherException {
        log.debug("Finding publisher with name [{}] and password [{}]", name, password);
        Publisher publisher = publisherDao.findByName(name);
        if (!password.equals(publisher.getPassword())) {
            throw new UserAuthenticationException();
        }
        return publisher;
    }

    @Override
    @Transactional
    public Publisher findByName(String name) throws NonExistentPublisherException {
        log.debug("Finding publisher with name [{}].", name);
        return publisherDao.findByName(name);
    }

    @Override
    public Publisher createPublisher(String name, String api, String username, String password, String passwordConfirmation, String url, String ownerId, String ortizCasinoNumber, String partnerCode, Boolean supportZeroBet, Boolean supportZeroWin) {
        log.debug("Creating publisher with name={}, username={}, api={} and url={}", name, username, api, url);
        Publisher p = new Publisher(name, username, api, password, url, ownerId, ortizCasinoNumber, partnerCode, supportZeroBet, supportZeroWin);
        p = publisherDao.createPublisher(p);
        log.info("Publisher added without errors. Proceeding to register each game to the publisher.");
        publisherGameService.saveByPublisherId(p.getId());
        return p;
    }
    
    @Override
    public void updatePublisher(Publisher publisher) {
        log.debug("Updating publisher [{}]", publisher);
        publisherDao.updatePublisher(publisher);
    }

    @Override
    public void changePassword(String currentPassword, String newPassword, String newPasswordConfirmation, String userName) throws PasswordException, UserAuthenticationException, NonExistentPublisherException {

        Publisher publisher = findByNameAndPassword(userName, currentPassword);

        if (StringUtils.isEmpty(newPassword)) {
            throw new PasswordException("The new Password can not be empty");
        }
        if (!newPassword.equals(newPasswordConfirmation)) {
            throw new PasswordException("Password and confirmation password do not match");
        }

        publisher.setPassword(newPassword);
        try {
            updatePublisher(publisher);
        } catch (Exception e) {
            throw new PasswordException("Failed to update publisher", e);
        }
    }

    @Override
    public void changeURL(String userName, String newURL) throws InvalidUrlException, NonExistentPublisherException {
        Publisher publisher = findByName(userName);

        if (StringUtils.isEmpty(newURL)) {
            throw new InvalidUrlException("The new URL must contain a valid value");
        }
        publisher.setUrl(newURL);

        updatePublisher(publisher);
    }

    @Override
    @Transactional
    public Publisher findById(Long publisherId) {
        log.debug("Getting publisher with id={}", publisherId);
        return publisherDao.findById(publisherId);
    }

    @Override
    public List<Publisher> getPublisherList() {
        log.info("Getting the list of publishers");
        return publisherDao.getPublisherList();
    }

    public void setPublisherDao(PublisherDao publisherDao) {
        this.publisherDao = publisherDao;
    }

	public void setPublisherGameService(PublisherGameService publisherGameService) {
		this.publisherGameService = publisherGameService;
	}
    
}
