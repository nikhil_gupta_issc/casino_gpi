package com.gpi.service.publisher;

import com.gpi.domain.publisher.Publisher;
import com.gpi.exceptions.InvalidUrlException;
import com.gpi.exceptions.NonExistentPublisherException;
import com.gpi.exceptions.PasswordException;
import com.gpi.exceptions.UserAuthenticationException;

import java.util.List;

public interface PublisherService {

    /**
     * This method retrieves a Publisher based on the name and the password
     *
     * @param name     Publisher name
     * @param password Encrypted password
     * @return Publisher
     */
    Publisher findByNameAndPassword(String name, String password) throws UserAuthenticationException, NonExistentPublisherException;

    /**
     * This method retrieves a Publisher based on the name
     *
     * @param name Publisher name
     * @return Publisher
     */
    Publisher findByName(String name) throws NonExistentPublisherException;

    /**
     * This method created a new Publisher with the name and password specified
     *
     * @param name                 The name of the Publisher to create
     * @param password             The password of the Publisher to create
     * @param passwordConfirmation
     * @param partnerCode          The partner code of the Publisher to create
     * @param supportZeroBet		If the partner supports zero amount bet calls
     * @param supportZeroWin	   If the partner supports zero amount win calls
     * @return The Publisher created.
     */
    Publisher createPublisher(String name, String apiName, String apiUsername, String password, String passwordConfirmation, String url, String ownerId, String ortizCasinoNumber, String partnerCode, Boolean supportZeroBet, Boolean supportZeroWin);

    /**
     * This method updates a publisher
     *
     * @param publisher
     */
    void updatePublisher(Publisher publisher);

    public void changePassword(String currentPassword, String newPassword, String newPasswordConfirmation, String userName) throws PasswordException, UserAuthenticationException, NonExistentPublisherException;

    public void changeURL(String userName, String newURL) throws InvalidUrlException, NonExistentPublisherException;

    public List<Publisher> getPublisherList();

    Publisher findById(Long publisherId);
}
