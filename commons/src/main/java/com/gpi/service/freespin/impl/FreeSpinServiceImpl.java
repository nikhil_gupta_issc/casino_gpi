package com.gpi.service.freespin.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gpi.dao.spin.FreeSpinDao;
import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.exceptions.AlreadyAssignException;
import com.gpi.exceptions.NotEnoughPackagesLeftException;
import com.gpi.service.freespin.FreeSpinService;
import com.gpi.service.game.GameDomainService;

/**
 * Created by nacho on 29/12/14.
 */
public class FreeSpinServiceImpl implements FreeSpinService {

    private FreeSpinDao freeSpinDao;
    private GameDomainService gameDomainService;

    @Override
    @Transactional
    public List<FreeSpin> findFreeSpinByPlayerAndGame(Player player, Game game) {
    	List<FreeSpin> freeSpin = null;
        if (player != null) {
            freeSpin = freeSpinDao.findFreeSpinByPlayerAndGame(player, game);
        }
        return freeSpin;
    }
    
    @Override
    @Transactional
	public List<FreeSpin> findFreeSpinByPlayer(Player player) {
    	List<FreeSpin> freeSpin = null;
        if (player != null) {
            freeSpin = freeSpinDao.findFreeSpinByPlayer(player);
        }
        return freeSpin;
	}

    @Override
    @Transactional
    public FreeSpin assignFreeSpin( Player player, Integer amount, Game game, Integer cents, String ref) {

       
        FreeSpin fs = new FreeSpin();
                fs.setPlayer(player);
                fs.setAmountLeft(amount);
                fs.setGame(game);
                fs.setCents(cents);
                fs.setExternalRef(ref);
           
         freeSpinDao.saveOrUpdateFreeSpin(fs);
        
        return fs;
    }

    @Override
    @Transactional
    public FreeSpin decreaseSpinLeft(FreeSpin freeSpin) {
        freeSpin.decreaseAmountLeft();
        freeSpinDao.saveOrUpdateFreeSpin(freeSpin);
        return freeSpin;
    }

    /**
     * @param freeSpinDao the freeSpinDao to set
     */
    public void setFreeSpinDao(FreeSpinDao freeSpinDao) {
        this.freeSpinDao = freeSpinDao;
    }

    public void setGameDomainService(GameDomainService gameDomainService) {
        this.gameDomainService = gameDomainService;
    }

	
}
