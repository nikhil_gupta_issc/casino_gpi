package com.gpi.service.freespin;

import com.gpi.domain.audit.FreeSpinTransaction;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.exceptions.NoFreeSpinLeftException;

import java.util.List;

/**
 * Created by igabba on 05/01/15.
 */
public interface FreeSpinTransactionService {


    List<FreeSpinTransaction> getFreeSpinTransactions(Long roundId);

    FreeSpin processFreeSpinMessage(Game game, Player player, Transaction transaction, Long amount) throws NoFreeSpinLeftException;

}
