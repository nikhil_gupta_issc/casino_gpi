package com.gpi.service.freespin;

import java.util.List;

import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;

/**
 * Created by nacho on 29/12/14.
 */
public interface FreeSpinService {


    /**
     * Finds a active free spin for a given user. Only 1 free spin is permitted
     * per user
     *
     * @param player the player you want to use for the search
     * @param game
     * @return
     */
    public List<FreeSpin> findFreeSpinByPlayerAndGame(Player player, Game game);
    
    
    /**
     * Finds all the freespins for a given user
     * @param player
     * @return
     */
    public List<FreeSpin> findFreeSpinByPlayer(Player player);

    /**
     * Decrease the spin left of a given free spin
     *
     * @param freeSpin
     * @return
     */
    public FreeSpin decreaseSpinLeft(FreeSpin freeSpin);
    
    /**
     * Assing free spins to a given user
     * @param player
     * @param amount
     * @param game
     * @param cents
     * @param ref
     * @return
     */
    public FreeSpin assignFreeSpin( Player player, Integer amount, Game game, Integer cents, String ref);

}
