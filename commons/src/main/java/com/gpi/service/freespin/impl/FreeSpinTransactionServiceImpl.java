package com.gpi.service.freespin.impl;

import com.gpi.dao.spin.FreeSpinTransactionDao;
import com.gpi.domain.audit.FreeSpinTransaction;
import com.gpi.domain.audit.Transaction;
import com.gpi.domain.free.spin.FreeSpin;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.exceptions.NoFreeSpinLeftException;
import com.gpi.service.freespin.FreeSpinService;
import com.gpi.service.freespin.FreeSpinTransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by igabba on 05/01/15.
 */
public class FreeSpinTransactionServiceImpl implements FreeSpinTransactionService {

    private static final Logger logger = LoggerFactory.getLogger(FreeSpinTransactionServiceImpl.class);

    private FreeSpinTransactionDao freeSpinTransactionDao;
    private FreeSpinService freeSpinService;

    @Override
    @Transactional
    public List<FreeSpinTransaction> getFreeSpinTransactions(Long roundId) {
        logger.debug("Retrieving the transaction who were free spin for round={}", roundId);
        return freeSpinTransactionDao.getFreeSpinTransactions(roundId);
    }

    @Override
    @Transactional
    public FreeSpin processFreeSpinMessage(Game game, Player player, Transaction transaction, Long amount) throws NoFreeSpinLeftException {
    	List<FreeSpin> freeSpins = freeSpinService.findFreeSpinByPlayerAndGame(player, game);
        
    	if (freeSpins != null && freeSpins.size() <= 0) {
            throw new NoFreeSpinLeftException("No free spin left for player=" + player.getName() + " on game=" + game.getName());
        }
    	
    	FreeSpin freeSpin = freeSpins.get(0);
        freeSpin = freeSpinService.decreaseSpinLeft(freeSpin);
        FreeSpinTransaction freeSpinTransaction = new FreeSpinTransaction();
        freeSpinTransaction.setGameTransactionId(transaction.getGameTransactionId());
        freeSpinTransaction.setTransaction(transaction);
        freeSpinTransaction.setAmount(amount);
        freeSpinTransaction.setFreeSpin(freeSpin);
        freeSpinTransactionDao.saveFreeSpinTransaction(freeSpinTransaction);
        return freeSpin;
    }

    public void setFreeSpinTransactionDao(FreeSpinTransactionDao freeSpinTransactionDao) {
        this.freeSpinTransactionDao = freeSpinTransactionDao;
    }

    public void setFreeSpinService(FreeSpinService freeSpinService) {
        this.freeSpinService = freeSpinService;
    }
}
