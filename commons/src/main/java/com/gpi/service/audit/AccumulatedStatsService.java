package com.gpi.service.audit;

import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.domain.publisher.PublisherStatsSummary;

import java.util.Date;
import java.util.List;

/**
 * Created by Nacho on 1/20/14.
 */
public interface AccumulatedStatsService {


    public void updateBetAmount(Game game, Player player, Publisher publisher, Long betAmount);

    public void updateWinAmount(Game game, Player player, Publisher publisher, Long winAmount);

    public void updateRounds(Player player, Publisher publisher, Game game);

    List<PublisherStatsSummary> findStatsForPublisher(Publisher p, Date from, Date to);

    List<PublisherStatsSummary> findStatsForPublisher(Publisher p, Game gameByName, Date from, Date to);
}
