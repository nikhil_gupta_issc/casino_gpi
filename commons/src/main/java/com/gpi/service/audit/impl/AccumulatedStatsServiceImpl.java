package com.gpi.service.audit.impl;

import com.gpi.dao.audit.AccumulatedStatsDao;
import com.gpi.domain.game.Game;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.domain.publisher.PublisherStatsDaily;
import com.gpi.domain.publisher.PublisherStatsMonthly;
import com.gpi.domain.publisher.PublisherStatsSummary;
import com.gpi.service.audit.AccumulatedStatsService;
import com.gpi.utils.CalendarUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Nacho on 1/20/14.
 */
@Transactional
public class AccumulatedStatsServiceImpl implements AccumulatedStatsService {

    private static final Logger logger = LoggerFactory.getLogger(AccumulatedStatsServiceImpl.class);

    private AccumulatedStatsDao statsDao;



    @Override
    public void updateBetAmount(Game game, Player player, Publisher publisher, Long betAmount) {
        try {
            logger.debug("Updating daily stat with betAmount={} for publisher={}, game={} and currency={}", betAmount, publisher,
                    game, player.getCurrency());
            PublisherStatsDaily dailyStat = findDailyStat(player, publisher, game);
            dailyStat.addBetAmount(betAmount);
            PublisherStatsMonthly statsMonthly = findMonthlyStat(player, publisher, game);
            statsMonthly.setBetAmount(betAmount);
            statsDao.updatePublisherStatsDaily(dailyStat);
            statsDao.updatePublisherStatsMonthly(statsMonthly);
        } catch (Exception e) {
            logger.error("Error while trying to update bet amount of {} stats.", betAmount, e);
        }
    }

    @Override
    public void updateWinAmount(Game game, Player player, Publisher publisher, Long winAmount) {
        try {
            logger.debug("Updating daily stat with winAmount={} for publisher={}, game={} and currency={}", winAmount, publisher,
                    game, player.getCurrency());
            PublisherStatsDaily dailyStat = findDailyStat(player, publisher, game);
            dailyStat.addWinAmount(winAmount);
            PublisherStatsMonthly statsMonthly = findMonthlyStat(player, publisher, game);
            statsMonthly.setWinAmount(winAmount);
            statsDao.updatePublisherStatsDaily(dailyStat);
            statsDao.updatePublisherStatsMonthly(statsMonthly);
        } catch (Exception e) {
            logger.error("Error while trying to update win amount of {} stats.", winAmount, e);
        }
    }

    @Override
    public List<PublisherStatsSummary> findStatsForPublisher(Publisher p, Date from, Date to) {
        logger.debug("Finding stats for publisher={}, from date = {} to date = {}", p.getName(), from.toString(), to.toString());
        return statsDao.generateDailySummary(p, from, to);
    }

    @Override
    public List<PublisherStatsSummary> findStatsForPublisher(Publisher p, Game game, Date from, Date to) {
        logger.debug("Finding stats for publisher={}, game = {}, from date = {} to date = {}", p.getName(), game.getName(), from.toString(), to.toString());
        return statsDao.generateDailySummary(p, game, from, to);
    }

    @Override
    public void updateRounds(Player player, Publisher publisher, Game game) {
        try {

            PublisherStatsDaily dailyStat = findDailyStat(player, publisher, game);

            dailyStat.addRounds(1);

            PublisherStatsMonthly statsMonthly = findMonthlyStat(player, publisher, game);
            statsMonthly.setRounds(1);
            statsDao.updatePublisherStatsDaily(dailyStat);
            statsDao.updatePublisherStatsMonthly(statsMonthly);
        } catch (Exception e) {
            logger.error("Error while trying to update round stats.", e);
        }
    }

    private PublisherStatsDaily findDailyStat(Player player, Publisher publisher, Game game) {
        Date now = new Date();
        PublisherStatsDaily dailyStat = statsDao.findDailyStat(player, publisher, game, player.getCurrency(), now);

        if (dailyStat == null) {
            logger.debug("Creating a new daily stats for  publisher={}, game={} and currency={}");
            dailyStat = new PublisherStatsDaily();
            dailyStat.setPlayerId(player.getId());
            dailyStat.setPublisherId(publisher.getId());
            dailyStat.setCurrency(player.getCurrency());
            dailyStat.setGameId(game.getId());
            dailyStat.setCreatedOn(now);
            dailyStat = statsDao.saveDailyStats(dailyStat);
        }
        return dailyStat;
    }

    private PublisherStatsMonthly findMonthlyStat(Player player, Publisher publisher, Game game) {
        Date month = CalendarUtils.thisMonth().getTime();
        PublisherStatsMonthly monthlyStat = statsDao.findMonthlyStat(player, publisher, game, player.getCurrency(), month);

        if (monthlyStat == null) {
            logger.debug("Creating a new monthly stats for  publisher={}, game={} and currency={}");
            monthlyStat = new PublisherStatsMonthly();
            monthlyStat.setPlayerId(player.getId());
            monthlyStat.setPublisherId(publisher.getId());
            monthlyStat.setCurrency(player.getCurrency());
            monthlyStat.setGameId(game.getId());
            monthlyStat.setCreatedOn(month);
            monthlyStat = statsDao.saveMonthlyStats(monthlyStat);
        }
        return monthlyStat;
    }

    public void setStatsDao(AccumulatedStatsDao statsDao) {
        this.statsDao = statsDao;
    }
}
