package com.gpi.service.round.impl;

import com.gpi.dao.round.RoundDao;
import com.gpi.domain.audit.Round;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.player.Player;
import com.gpi.service.audit.AccumulatedStatsService;
import com.gpi.service.round.RoundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Nacho on 2/20/14.
 */
public class RoundServiceImpl implements RoundService {
    private static final Logger logger = LoggerFactory.getLogger(RoundServiceImpl.class);
    private RoundDao roundDao;
    private AccumulatedStatsService accumulatedStatsService;


    @Override
    @Transactional
    public Round getRound(Game game, GameProvider gameProvider, Long providerRoundId) {
        logger.debug("Getting Round for game={} and provider={} and transactionID={}", game.getName(),
                gameProvider.getName(), providerRoundId);
        return roundDao.getRound(game, providerRoundId);
    }

    @Override
    @Transactional
    public Round createRound(Player player, Game game, Long providerRoundId) {
        logger.debug("Creating a new Round for player={} and game={}", player.getName(), game.getName());
        Round round = new Round();
        round.setPlayer(player);
        round.setGame(game);
        round.setPublisher(player.getPublisher());
        round.setCreatedOn(new Date());
        round.setCurrency(player.getCurrency());
        round.setGameRoundId(providerRoundId);
        round.setBetAmount(0l);
        round.setWinAmount(0l);
        logger.debug("Creating Round: {}", round);
        round = roundDao.saveRound(round);
        accumulatedStatsService.updateRounds(player, player.getPublisher(), game);
        return round;
    }

    @Override
    @Transactional
    public void updateRound(Round round) {
        logger.debug("Updating Round={}", round);
        roundDao.updateRound(round);
    }

    @Override
    @Transactional
    public Round getRoundById(Long roundId) {
        logger.debug("finding round by id={}", roundId);
        return roundDao.findRoundById(roundId);
    }
    
    @Override
    @Transactional
	public Round findLastRound(Player player, Game game) {
		logger.debug("finding round id by player={} and game={}", player.getId(), game.getId());
		return roundDao.findLastRound(game, player);
	}

    public void setRoundDao(RoundDao roundDao) {
        this.roundDao = roundDao;
    }

    public void setAccumulatedStatsService(AccumulatedStatsService accumulatedStatsService) {
        this.accumulatedStatsService = accumulatedStatsService;
    }

	
}
