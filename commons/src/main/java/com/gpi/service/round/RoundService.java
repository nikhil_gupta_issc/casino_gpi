package com.gpi.service.round;

import com.gpi.domain.audit.Round;
import com.gpi.domain.game.Game;
import com.gpi.domain.game.GameProvider;
import com.gpi.domain.player.Player;

/**
 * Created by Nacho on 2/20/14.
 */
public interface RoundService {


    Round getRound(Game gameByName, GameProvider gameProvider, Long providerRoundId);

    Round createRound(Player player, Game game, Long gameRoundId);

    void updateRound(Round round);

    Round getRoundById(Long roundId);
    
    Round findLastRound(Player player, Game game);
}
