package com.gpi.service.utils.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;


public class AsyncJavaMailSenderImpl extends JavaMailSenderImpl {

    private static final Logger logger = LoggerFactory.getLogger(AsyncJavaMailSenderImpl.class);

    @Async
    @Override
    public void send(MimeMessagePreparator mimeMessagePreparator)
            throws MailException {
        try {
            super.send(mimeMessagePreparator);
            logger.info("Mail send!.");
        } catch (Exception e) {
            logger.error("Could not send mail.", e);
        }

    }
}
