package com.gpi.service.utils;

import java.util.Locale;
import java.util.Map;

/**
 * Service to sent a simple email template(.vm files)
 */
public interface SimpleEmailSender {

    boolean sendEmail(String toAddress, String subjectI18N,
                      String templateFilePropertyKey, Locale locale,
                      Map<String, Object> additionalModelArgs);
}
