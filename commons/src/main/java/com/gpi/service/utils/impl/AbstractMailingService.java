package com.gpi.service.utils.impl;


import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.ValidationUtils;
import com.gpi.utils.ValidationUtils.InvalidEmailException;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.util.Assert;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AbstractMailingService {

    private final Logger logger = LoggerFactory.getLogger(AbstractMailingService.class);
    private AbstractMessageSource messageSource;
    private JavaMailSender mailSender;
    private VelocityEngine velocityEngine;
    private String fromKey;
    private String replyToKey;
    private String templatePathKey;
    private String fromPersonalKey;

    protected Map<String, Object> buildBaseModel() {
        Map<String, Object> model = new HashMap<>();
        model.put("siteName", ApplicationProperties.getProperty("gpi.mailing.site.name"));
        model.put("siteUrl", ApplicationProperties.getProperty("application.host.url"));
        return model;
    }

    protected String resolveMessage(String code, Object[] args, Locale locale) {
        if (code == null || "".equalsIgnoreCase(code)) {
            return "";
        }
        return this.messageSource.getMessage(code, args, code, locale);
    }

    protected void validateEmail(String mail) throws InvalidEmailException {
        ValidationUtils.validateEmail(mail);
        ValidationUtils.validateEmailDomain(mail);
    }

    /**
     * @param recipient may be one or a , separated list
     */
    public void sendTo(final String subject, final String[] recipients,
                       Map<String, Object> model, final String template, final String defaultTemplate) {
        final Map<String, Object> localModel = new HashMap<>();

        if (model != null) {
            localModel.putAll(model);
        }

        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setSubject(subject);

                Assert.notEmpty(recipients);
                Assert.notNull(template);

                messageHelper.setBcc(recipients);

                final String from = getFrom();
                final String fromPersonalName = getFromPersonalName();
                if (StringUtils.isNotBlank(fromPersonalName)) {
                    messageHelper.setFrom(from, fromPersonalName);
                } else {
                    messageHelper.setFrom(from);
                }

                final String replyTo = getReplyTo();
                messageHelper.setReplyTo(replyTo);

                final String templatePath = getTemplatePath();
                String text;
                String templateLocation = templatePath + "/" + template;
                try {
                    text = VelocityEngineUtils
                            .mergeTemplateIntoString(velocityEngine, templateLocation, "UTF-8", localModel);
                } catch (ResourceNotFoundException e) {
                    logger.warn(e.getMessage());
                    templateLocation = templatePath + "/" + defaultTemplate;
                    text = VelocityEngineUtils
                            .mergeTemplateIntoString(velocityEngine, templateLocation, "UTF-8", localModel);
                }
                messageHelper.setText(text, true);
            }
        };
        try {
            logger.info("Sending mail: {}", preparator.toString());
            this.mailSender.send(preparator);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    protected String getFrom() {
        final String from = ApplicationProperties.getProperty(fromKey);
        if (StringUtils.isEmpty(from)) {
            throw new RuntimeException("No from email address configured in properties file.");
        }
        return from;
    }

    protected String getReplyTo() {
        final String replyTo = ApplicationProperties.getProperty(replyToKey);
        if (StringUtils.isEmpty(replyTo)) {
            throw new RuntimeException("No replyTo email address configured in properties file.");
        }
        return replyTo;
    }

    protected String getTemplatePath() {
        final String templatePath = ApplicationProperties.getProperty(templatePathKey);
        return StringUtils.defaultString(templatePath, StringUtils.EMPTY);
    }

    protected String getFromPersonalName() {
        final String personalName = ApplicationProperties.getProperty(fromPersonalKey);
        return StringUtils.defaultString(personalName, StringUtils.EMPTY);
    }

    public void setFromPersonalName(String fromPersonalName) {
        this.fromPersonalKey = fromPersonalName;
    }

    public void setMessageSource(AbstractMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

    public void setFromKey(String fromKey) {
        this.fromKey = fromKey;
    }

    public void setReplyToKey(String replyToKey) {
        this.replyToKey = replyToKey;
    }

    public void setTemplatePathKey(String templatePathKey) {
        this.templatePathKey = templatePathKey;
    }
}
