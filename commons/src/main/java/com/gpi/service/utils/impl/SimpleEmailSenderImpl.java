package com.gpi.service.utils.impl;

import com.gpi.service.utils.SimpleEmailSender;
import com.gpi.utils.ApplicationProperties;
import com.gpi.utils.ValidationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Map;

public class SimpleEmailSenderImpl extends AbstractMailingService implements SimpleEmailSender {

    private static final Logger logger = LoggerFactory.getLogger(SimpleEmailSenderImpl.class);


    @Override
    public boolean sendEmail(String toAddress, String subjectI18N,
                             String templateFilePropertyKey, Locale locale, Map<String, Object> additionalModelArgs) {
        try {
            validateEmail(toAddress);

            String defaultTemplate = templateFilePropertyKey
                    + "_en.vm";

            String template;
            if (locale != null) {
                template = templateFilePropertyKey
                        + "_"
                        + locale.getLanguage() + ".vm";
            } else {
                template = defaultTemplate;
                logger.error("Locale was null while sending mail with subject i18N: ", subjectI18N);
            }

            Map<String, Object> model = buildBaseModel();

            String subject = resolveMessage(subjectI18N, null, locale);

            try {
                subject = ValidationUtils.getMimeEncodedSubject(subject);
            } catch (UnsupportedEncodingException e) {
                logger.error("Error while encoding email subject " + subject, e);
            }

            if (logger.isDebugEnabled()) {
                logger.debug("Email subject is {} to send to {}", subject, toAddress);
            }

            // static URL for images
            String imagesPath = ApplicationProperties.getProperty("application.static.url")
                    + ApplicationProperties.getProperty("application.img.path");
            logger.info("Image base url {}", imagesPath);

            model.putAll(additionalModelArgs);
            model.put("imagesPath", imagesPath);

            logger.debug("Sending email to {} model is {}", toAddress, model);

            sendTo(subject, new String[]{toAddress}, model, template, template);
        } catch (Exception e) {
            logger.error("Invalid email address to sent " + toAddress, e);
            return false;
        }

        return true;
    }

}
