package com.gpi.service.applicationproperty;

public interface ApplicationPropertyService {

	public String getProperty(String name);
	
	public Integer saveOrUpdate(Integer id, String name, String value);
	
	public void delete(Integer id);
}
