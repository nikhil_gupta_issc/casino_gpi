package com.gpi.service.applicationproperty.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gpi.dao.applicationproperty.ApplicationPropertyDao;
import com.gpi.domain.applicationproperty.ApplicationProperty;
import com.gpi.service.applicationproperty.ApplicationPropertyService;


public class ApplicationPropertyServiceImpl implements ApplicationPropertyService {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationPropertyServiceImpl.class);
    private ApplicationPropertyDao applicationPropertyDao;

    @Override
    @Transactional
    public String getProperty(String name) {
    	return applicationPropertyDao.getProperty(name);
    }
    
    @Override
    @Transactional
    public Integer saveOrUpdate(Integer id, String name, String value) {
       ApplicationProperty applicationProperty = new ApplicationProperty();
       applicationProperty.setId(id);
       applicationProperty.setName(name);
       applicationProperty.setValue(value);
       return applicationPropertyDao.saveOrUpdate(applicationProperty);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        ApplicationProperty applicationProperty = new ApplicationProperty();
        applicationProperty.setId(id);
    	applicationPropertyDao.delete(applicationProperty);
    }

	/**
	 * @param applicationPropertyDao the applicationPropertyDao to set
	 */
	public void setApplicationPropertyDao(ApplicationPropertyDao applicationPropertyDao) {
		this.applicationPropertyDao = applicationPropertyDao;
	}

    
}
