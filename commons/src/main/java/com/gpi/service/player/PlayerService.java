package com.gpi.service.player;

import com.gpi.domain.currency.Currency;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.ApiResponseDto;
import com.gpi.exceptions.NonExistentPlayerException;

import java.util.List;

public interface PlayerService {

    /**
     * Create a new user
     *
     * @param userName  the user login name
     * @param currency  see @Currency
     * @param publisher see @Publisher
     * @return the user created with the id assigned
     */
    Player createNewUser(String userName, Currency currency, Publisher publisher);

    /**
     * Find user
     *
     * @param publisher see @Publisher
     * @param user      the user login name
     * @return The Player or null if the user does not exists
     */
    Player findUserByUserNameAndPublisher(String user, Integer publisher);

    /**
     * Find user just by the userName
     *
     * @param id the player login name
     * @return The Player or null if the user does not exists
     * @throws com.gpi.exceptions.NonExistentPlayerException
     */
    Player findUserById(Long id) throws NonExistentPlayerException;

    /**
     * Updates the user
     *
     * @param user
     */
    void updatePlayer(Player user);

    void updateLogin(Player player, ApiResponseDto apiResponseDto);

    List<Player> getAllPlayerByPublisher(Integer publisher);

    List<Player> findPlayerLike(String name, Integer publisherId);

    List<Player> findPlayerLike(String name);

	List<Player> findPlayersByPlayerNameAndPublisher(String playerName,
			Integer publisher);

	List<Player> findPlayersByPlayerName(String playerName);
}
