package com.gpi.service.player.impl;

import com.gpi.dao.player.PlayerDao;
import com.gpi.domain.currency.Currency;
import com.gpi.domain.player.Player;
import com.gpi.domain.publisher.Publisher;
import com.gpi.dto.ApiResponseDto;
import com.gpi.exceptions.NonExistentPlayerException;
import com.gpi.service.player.PlayerService;
import com.gpi.utils.ValidationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PlayerServiceImpl implements PlayerService {

    private static final Logger log = LoggerFactory.getLogger(PlayerServiceImpl.class);
    private PlayerDao playerDao;

    @Override
    public Player findUserByUserNameAndPublisher(String username, Integer publisher) {
        return playerDao.findByPlayerNameAndPublisher(username, publisher);
    }

    public void setPlayerDao(PlayerDao playerDao) {
        this.playerDao = playerDao;
    }

    @Override
    public Player findUserById(Long id) throws NonExistentPlayerException {
        return playerDao.findById(id);
    }

    @Override
    public void updatePlayer(Player user) {
        playerDao.updatePlayer(user);
    }

    @Override
    public Player createNewUser(String userName, Currency currency, Publisher publisher) {
        Player player = new Player(userName, currency, publisher);

        if (ValidationUtils.isFreePublisher(publisher.getName())) {
            return player;
        }
        player = playerDao.savePlayer(player);
        log.debug("Player created with id [{}] and currency [{}]", player.getId(), currency);
        return player;
    }

    @Override
    public void updateLogin(Player player, ApiResponseDto apiResponseDto) {
        log.debug("Updating login. Currency=[{}] and Balance = [{}]", apiResponseDto.getCurrency(), apiResponseDto.getBalance());
        player.setCurrency(apiResponseDto.getCurrency());
        updatePlayer(player);
    }

    @Override
    public List<Player> getAllPlayerByPublisher(Integer publisher) {
        log.debug("Getting all players for publisherId={}", publisher);
        return playerDao.findPlayersByPublisher(publisher);
    }

    @Override
    public List<Player> findPlayerLike(String name, Integer publisherId) {
        log.debug("Getting all players like={} and publisher={}", name, publisherId);
        return playerDao.findPlayerLike(name, publisherId);
    }

    @Override
    public List<Player> findPlayerLike(String name) {
        log.debug("Getting all players like={}", name);
        return playerDao.findPlayerLike(name);
    }
    
    @Override
    public List<Player> findPlayersByPlayerNameAndPublisher(String playerName,
    	Integer publisher) {
   		return playerDao.findPlayersByPlayerNameAndPublisher(playerName, publisher);
    }

	@Override
	public List<Player> findPlayersByPlayerName(String playerName) {
		return playerDao.findPlayersByPlayerName(playerName);
	}
}
