package com.pragmaticplay.api.domain;

/**
 * Base response to PragmaticPlay API
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayResponse {
	protected int error;
	protected String description;
	
	public int getError() {
		return error;
	}
	
	public void setError(int error) {
		this.error = error;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PragmaticPlayResponse [error=" + error + ", description=" + description + "]";
	}
}
