package com.pragmaticplay.api.domain;

/**
 * Bet response to Pragmatic Play API
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayBetResponse extends PragmaticPlayResponse {
	private String transactionId;
	private String currency;
	private double cash;
	private double bonus;
	private double usedPromo;
	
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	
	public double getUsedPromo() {
		return usedPromo;
	}

	public void setUsedPromo(double usedPromo) {
		this.usedPromo = usedPromo;
	}

	@Override
	public String toString() {
		return "PragmaticPlayBetResponse [transactionId=" + transactionId + ", currency=" + currency + ", cash=" + cash
				+ ", bonus=" + bonus + ", usedPromo=" + usedPromo + ", error=" + error + ", description=" + description
				+ "]";
	}
	
}
