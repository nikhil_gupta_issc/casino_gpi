package com.pragmaticplay.api.domain;

/**
 * Result response to Pragmatic Play API
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayEndRoundResponse extends PragmaticPlayResponse {
	private double cash;
	private double bonus;
	
	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

	@Override
	public String toString() {
		return "PragmaticPlayEndRoundResponse [cash=" + cash + ", bonus=" + bonus + "]";
	}
}
