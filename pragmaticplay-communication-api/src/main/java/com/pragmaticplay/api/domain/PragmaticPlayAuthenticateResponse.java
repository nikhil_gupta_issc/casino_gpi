package com.pragmaticplay.api.domain;

public class PragmaticPlayAuthenticateResponse extends PragmaticPlayResponse {
	private String userId;
	private String currency;
	private double cash;
	private double bonus;
	private String token;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "PragmaticPlayAuthenticateResponse [userId=" + userId + ", currency=" + currency + ", cash=" + cash
				+ ", bonus=" + bonus + ", token=" + token + ", error=" + error + ", description=" + description + "]";
	}
}
