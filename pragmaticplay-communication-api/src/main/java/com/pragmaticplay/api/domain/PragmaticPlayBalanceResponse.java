package com.pragmaticplay.api.domain;

/**
 * Balance response to Pragmatic Play API
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayBalanceResponse extends PragmaticPlayResponse {
	private String currency;
	private double cash;
	private double bonus;
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

	@Override
	public String toString() {
		return "PragmaticPlayBalanceResponse [currency=" + currency + ", cash=" + cash + ", bonus=" + bonus + ", error="
				+ error + ", description=" + description + "]";
	}
	
}
