package com.pragmaticplay.api.domain;

/**
 * Refund response to Pragmatic Play API
 * 
 * @author Alexandre
 *
 */
public class PragmaticPlayRefundResponse extends PragmaticPlayResponse {
	private String transactionId;
	
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "PragmaticPlayRefundResponse [transactionId=" + transactionId + ", error=" + error + ", description="
				+ description + "]";
	}

	
}
