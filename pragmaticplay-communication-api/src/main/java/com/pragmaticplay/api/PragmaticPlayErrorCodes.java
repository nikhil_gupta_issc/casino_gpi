package com.pragmaticplay.api;

/**
 * Error codes for the Pragmatic Play API
 * 
 * @author Alexandre
 *
 */
public interface PragmaticPlayErrorCodes {
	public static final int INSUFFICIENT_BALANCE_ERROR = 1;
	public static final int PLAYER_NOT_FOUND_ERROR = 2;
	public static final int EXPIRED_TOKEN_ERROR = 3;
	public static final int INVALIDHASH_ERROR = 5;
	public static final int GAME_NOT_FOUND_ERROR = 6;
	public static final int INTERNAL_ERROR_RECONCILIATION = 100;
	public static final int INTERNAL_ERROR_NO_RECONCILIATION = 120;
}