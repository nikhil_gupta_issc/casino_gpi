package com.pragmaticplay.api;

/**
 * Methods for the Pragmatic Play API
 * 
 * @author Alexandre
 *
 */
public enum PragmaticPlayMethod {

    AUTHENTICATE("authenticate"), BALANCE("balance"), BET("bet"), RESULT("result"), BONUS_WIN("bonusWin"),
    JACKPOT_WIN("jackpotWin"), REFUND("refund"), END_ROUND("endRound");

    private final String name;

    private PragmaticPlayMethod(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
