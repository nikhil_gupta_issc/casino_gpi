package com.ortiz.api.domain;

import java.math.BigDecimal;

public class AccountStatResponse {
	private String accountStatus = "E";
	private String userLanguage;
	private String userCurrency;
	private BigDecimal balanceCredits;
	/**
	 * @return the accountStatus
	 */
	public String getAccountStatus() {
		return accountStatus;
	}
	/**
	 * @param accountStatus the accountStatus to set
	 */
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	/**
	 * @return the userLanguage
	 */
	public String getUserLanguage() {
		return userLanguage;
	}
	/**
	 * @param userLanguage the userLanguage to set
	 */
	public void setUserLanguage(String userLanguage) {
		this.userLanguage = userLanguage;
	}
	/**
	 * @return the userCurrency
	 */
	public String getUserCurrency() {
		return userCurrency;
	}
	/**
	 * @param userCurrency the userCurrency to set
	 */
	public void setUserCurrency(String userCurrency) {
		this.userCurrency = userCurrency;
	}
	/**
	 * @return the balanceCredits
	 */
	public BigDecimal getBalanceCredits() {
		return balanceCredits;
	}
	/**
	 * @param balanceCredits the balanceCredits to set
	 */
	public void setBalanceCredits(BigDecimal balanceCredits) {
		this.balanceCredits = balanceCredits;
	}
	
	@Override
	public String toString() {
		return "AccountStatResponse[accountStatus=" + this.getAccountStatus() + 
				",userLanguage=" + this.getUserLanguage() + 
				",userCurrency=" + this.getUserCurrency() + ",balanceCredits=" + 
				this.getBalanceCredits() + "]";
	}
	
	
}
