package com.ortiz.api.domain;

import java.math.BigDecimal;

public class GameInfoResponse {

	private BigDecimal balanceCredits;

	/**
	 * @return the balanceCredits
	 */
	public BigDecimal getBalanceCredits() {
		return balanceCredits;
	}

	/**
	 * @param balanceCredits the balanceCredits to set
	 */
	public void setBalanceCredits(BigDecimal balanceCredits) {
		this.balanceCredits = balanceCredits;
	}
	
	@Override
	public String toString() {
		return "GameInfoResponse[balanceCredits=" + this.getBalanceCredits() + "]";
	}
	
	
}
