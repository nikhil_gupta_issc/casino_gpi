package com.ortiz.api.domain;

public class GameInfoRequest {

	private String authToken;
	
	private Long messageId;
	
	private Integer gameId;
	
	private Long gameRoundId;
	
	private Object gameRoundData;

	/**
	 * @return the authToken
	 */
	public String getAuthToken() {
		return authToken;
	}

	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * @return the messageId
	 */
	public Long getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return the gameId
	 */
	public Integer getGameId() {
		return gameId;
	}

	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the gameRoundId
	 */
	public Long getGameRoundId() {
		return gameRoundId;
	}

	/**
	 * @param gameRoundId the gameRoundId to set
	 */
	public void setGameRoundId(Long gameRoundId) {
		this.gameRoundId = gameRoundId;
	}

	/**
	 * @return the gameRoundData
	 */
	public Object getGameRoundData() {
		return gameRoundData;
	}

	/**
	 * @param gameRoundData the gameRoundData to set
	 */
	public void setGameRoundData(Object gameRoundData) {
		this.gameRoundData = gameRoundData;
	}
	
	@Override
	public String toString() {
		return "GameInfoRequest[authToken=" + this.getAuthToken() + ",messageId=" + 
				this.getMessageId() + ",gameId=" + this.getGameId() + ",gameRoundId=" + 
				this.getGameRoundId() + ",gameRoundData=" + this.getGameRoundData() + "]";
	}


	
}
