package com.ortiz.api.domain;

import java.math.BigDecimal;

public class BetRequest {

	private String authToken;
	private Long messageId;
	private Integer gameId;
	private Long gameRoundId;
	private Float amount;
	private String reason;
	private Boolean baseGame;
	/**
	 * @return the authToken
	 */
	public String getAuthToken() {
		return authToken;
	}
	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	/**
	 * @return the gameId
	 */
	public Integer getGameId() {
		return gameId;
	}
	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	/**
	 * @return the gameRoundId
	 */
	public Long getGameRoundId() {
		return gameRoundId;
	}
	/**
	 * @param gameRoundId the gameRoundId to set
	 */
	public void setGameRoundId(Long gameRoundId) {
		this.gameRoundId = gameRoundId;
	}
	/**
	 * @return the messageId
	 */
	public Long getMessageId() {
		return messageId;
	}
	/**
	 * @return the amount
	 */
	public Float getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	/**
	 * @return the baseGame
	 */
	public Boolean getBaseGame() {
		return baseGame;
	}
	/**
	 * @param baseGame the baseGame to set
	 */
	public void setBaseGame(Boolean baseGame) {
		this.baseGame = baseGame;
	}

	@Override
	public String toString() {
		return "BetRequest[authToken=" + this.getAuthToken() + 
				",messageId=" + this.getMessageId() + ",gameId=" + this.getGameId() + 
				",amount=" + this.getAmount() + ",reason=" + this.getReason() + 
				",baseGame=" + this.getBaseGame() + "]";
	}
}
