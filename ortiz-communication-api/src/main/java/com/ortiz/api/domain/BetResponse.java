package com.ortiz.api.domain;

import java.math.BigDecimal;

public class BetResponse {
	
	private BigDecimal balanceCredits;

	/**
	 * @return the balanceCredits
	 */
	public BigDecimal getBalanceCredits() {
		return balanceCredits;
	}

	/**
	 * @param balanceCredits the balanceCredits to set
	 */
	public void setBalanceCredits(BigDecimal balanceCredits) {
		this.balanceCredits = balanceCredits;
	}
	
	@Override
	public String toString() {
		return "BetResponse[balanceCredits=" + this.getBalanceCredits() + "]";
	}
}
