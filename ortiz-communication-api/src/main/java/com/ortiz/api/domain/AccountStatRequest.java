package com.ortiz.api.domain;

public class AccountStatRequest {
	private String authToken;
	private Integer messageId;
	
	/**
	 * @return the authToken
	 */
	public String getAuthToken() {
		return authToken;
	}
	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	/**
	 * @return the messageId
	 */
	public Integer getMessageId() {
		return messageId;
	}
	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	@Override
	public String toString() {
		return "AccountStatRequest[authToken=" + this.getAuthToken() + 
				",messageId=" + this.getMessageId() + "]";
	}
	
}
