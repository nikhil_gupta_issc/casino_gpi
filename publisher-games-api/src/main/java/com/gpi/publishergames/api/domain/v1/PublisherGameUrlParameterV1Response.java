package com.gpi.publishergames.api.domain.v1;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.ALWAYS)
public class PublisherGameUrlParameterV1Response {

	protected String name;
	
	protected String value;
	
	protected boolean required;
	
	protected List<String> options;

	public PublisherGameUrlParameterV1Response(String name, String value, boolean required, List<String> options) {
		super();
		this.name = name;
		this.value = value;
		this.required = required;
		this.options = options;
	}
	
	public PublisherGameUrlParameterV1Response(String name, String value, boolean required) {
		super();
		this.name = name;
		this.value = value;
		this.required = required;
	}

	public PublisherGameUrlParameterV1Response() {
		super();
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@JsonProperty("required")
	public boolean getRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	@JsonProperty("options")
	public List<String> getOptions() {
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PublisherGameUrlParameterV1Response [name=");
		builder.append(name);
		builder.append(", value=");
		builder.append(value);
		builder.append(", required=");
		builder.append(required);
		builder.append(", options=");
		builder.append(options);
		builder.append("]");
		return builder.toString();
	}	
	
}
