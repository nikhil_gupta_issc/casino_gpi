package com.gpi.publishergames.api.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.ALWAYS)
public class PublisherGameUrlV1Response {

	protected String host;
	
	protected String path;
	
	protected String type;
	
	protected String url;
	
	protected List<PublisherGameUrlParameterV1Response> parameters;

	public PublisherGameUrlV1Response() {
		super();
		type = "GET";
		parameters = new ArrayList<PublisherGameUrlParameterV1Response>();
	}

	@JsonProperty("host")
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	@JsonProperty("path")
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("parameters")
	public List<PublisherGameUrlParameterV1Response> getParameters() {
		return parameters;
	}

	public void setParameters(List<PublisherGameUrlParameterV1Response> parameters) {
		this.parameters = parameters;
	}
	
	@JsonProperty("url")
	public String getUrl() {
		return getUrl(true);
	}
	
	@JsonProperty("urlFull")
	public String getUrlFull() {
		return getUrl(false);
	}
	
	private String getUrl(boolean onlyRequired) {
		StringBuffer url = new StringBuffer().append(host).append("/").append(path).append("?");
		for (PublisherGameUrlParameterV1Response parameter : parameters) {
			if ((onlyRequired == true && parameter.getRequired()) || (onlyRequired == false)) {
				url.append(parameter.getName()).append("=").append(parameter.getValue()).append("&");
			}
		}
		url.setLength(url.length() - 1);
		return url.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PublisherGameUrlV1Response [host=");
		builder.append(host);
		builder.append(", path=");
		builder.append(path);
		builder.append(", type=");
		builder.append(type);
		builder.append(", url=");
		builder.append(url);
		builder.append(", parameters=");
		builder.append(parameters);
		builder.append("]");
		return builder.toString();
	}
	
}
