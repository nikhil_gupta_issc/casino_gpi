package com.gpi.publishergames.api.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.ALWAYS)
public class PublisherGamesListV1Response {

	protected List<PublisherProviderV1Response> publisherProviders = new ArrayList<PublisherProviderV1Response>();

	@JsonProperty("providers")
	public List<PublisherProviderV1Response> getPublisherProviders() {
		return publisherProviders;
	}

	public void setPublisherProviders(List<PublisherProviderV1Response> publisherProviders) {
		this.publisherProviders = publisherProviders;
	}
	
}
