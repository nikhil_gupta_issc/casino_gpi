package com.gpi.publishergames.api.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.ALWAYS)
public class PublisherProviderV1Response {

	protected String name;
	
	protected List<PublisherGameV1Response> games = new ArrayList<PublisherGameV1Response>();
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("games")
	public List<PublisherGameV1Response> getGames() {
		return games;
	}

	public void setGames(List<PublisherGameV1Response> games) {
		this.games = games;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PublisherProviderV1Response [name=");
		builder.append(name);
		builder.append(", games=");
		builder.append(games);
		builder.append("]");
		return builder.toString();
	}
}
