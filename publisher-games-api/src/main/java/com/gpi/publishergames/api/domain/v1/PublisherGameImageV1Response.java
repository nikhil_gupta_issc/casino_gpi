package com.gpi.publishergames.api.domain.v1;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.ALWAYS)
public class PublisherGameImageV1Response {

	protected String url;
	
	public PublisherGameImageV1Response(String url) {
		super();
		this.url = url;
	}

	public PublisherGameImageV1Response() {
		super();
	}

	@JsonProperty("url")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImageResponse [url=");
		builder.append(url);
		builder.append("]");
		return builder.toString();
	}
	
}
