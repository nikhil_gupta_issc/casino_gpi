package com.gpi.publishergames.api.domain.v1;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.ALWAYS)
public class PublisherGameV1Response {

	protected String name;
	
	protected String description;
	
	protected Boolean free;
	
	protected Boolean charged;
	
	protected String provider;
	
	protected PublisherGameImageV1Response image;

	protected PublisherGameUrlV1Response url;

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("free")
	public Boolean getFree() {
		return free;
	}

	public void setFree(Boolean free) {
		this.free = free;
	}

	@JsonProperty("charged")
	public Boolean getCharged() {
		return charged;
	}

	public void setCharged(Boolean charged) {
		this.charged = charged;
	}

	@JsonProperty("provider")
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	@JsonProperty("image")
	public PublisherGameImageV1Response getImage() {
		return image;
	}

	public void setImage(PublisherGameImageV1Response image) {
		this.image = image;
	}
	
	@JsonProperty("url")
	public PublisherGameUrlV1Response getUrl() {
		return url;
	}

	public void setUrl(PublisherGameUrlV1Response url) {
		this.url = url;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PublisherGameV1Response [name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", free=");
		builder.append(free);
		builder.append(", charged=");
		builder.append(charged);
		builder.append(", provider=");
		builder.append(provider);
		builder.append(", image=");
		builder.append(image);
		builder.append(", url=");
		builder.append(url);
		builder.append("]");
		return builder.toString();
	}



}
