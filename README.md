#Gpi

## Release instructions

In order to perform a release the scm plugin will resolve the process. In the command line we should execute te following 
command:

    mvn release:prepare
    
You will be asked for the release version for each module, the name of the tag and the next version. Normally the suggestions
are right.

This process creates a tag but if you want to make a branch just create a branch from the tag and push this branch.