package com.evenbet.domain;

public class EvenbetErrorResponse extends EvenbetResponse{
	
	public EvenbetErrorResponse() {
		super();
	}
	
	public EvenbetErrorResponse(Long errorCode, String errorDescription) {
		super();
		super.setErrorCode(errorCode);
		super.setErrorDescription(errorDescription);
	}
	
}
