package com.evenbet.domain;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

public class EvenbetRequestHeader {

	private String sign;
	
	public EvenbetRequestHeader(String sign) {
		super();
		this.sign = sign;
	}

	public EvenbetRequestHeader() {
		super();
	}
	
	
	/**
	 * @return the sign
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * @param sign the sign to set
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
	
	
}

