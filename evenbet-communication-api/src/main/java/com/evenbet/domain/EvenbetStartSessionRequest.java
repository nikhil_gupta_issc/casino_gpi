package com.evenbet.domain;

import java.io.IOException;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

public class EvenbetStartSessionRequest {
	
	@JsonProperty("nick")
	private String nick;

	@JsonProperty("authType")
	private String authType;
	
	@JsonProperty("lang")
	private String lang;
	
	@JsonProperty("launchOptions")
	private String launchOptions;

	@JsonProperty("currency")
	private String currency;

	private String userId;
	
	/**
	 * @return the nick
	 */
	public String getNick() {
		return nick;
	}

	/**
	 * @param nich the nich to set
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * @return the authType
	 */
	public String getAuthType() {
		return authType;
	}

	/**
	 * @param authType the authType to set
	 */
	public void setAuthType(String authType) {
		this.authType = authType;
	}

	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * @param lang the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	/**
	 * @return the launchOptions
	 */
	public String getLaunchOptions() {
		return launchOptions;
	}

	/**
	 * @param launchOptions the launchOptions to set
	 */
	public void setLaunchOptions(String launchOptions) {
		this.launchOptions = launchOptions;
	}
	
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String buildSignRequest() {
		return this.implode();
	}
	
	private String implode() {
		return String.join("", new String[]{this.getAuthType(), this.getCurrency(), this.getLang(), this.getLaunchOptions(), this.getNick(), this.getUserId()});
	}
	
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
}