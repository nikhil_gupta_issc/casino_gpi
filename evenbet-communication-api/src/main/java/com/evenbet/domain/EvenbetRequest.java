package com.evenbet.domain;

import java.io.IOException;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.evenbet.api.EvenbetMethodType;

import net.sf.json.JSONObject;


@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=Inclusion.NON_NULL)
public abstract class EvenbetRequest {

	@JsonProperty("method")
	private String method;
	
	@JsonProperty("userId")
	private Long userId;
	
	@JsonProperty("currency")
	private String currency;
	
	private String rawJson;
	
	@JsonCreator
	public static EvenbetRequest create(String jsonBody)//@JsonProperty("method") String method, @JsonProperty("userId") Long userId, @JsonProperty("currency") String currency,@JsonProperty("amount") Long amount, @JsonProperty("transactionId") String transactionId )
	{
	    JSONObject requestJson = JSONObject.fromObject(jsonBody);
	    
		EvenbetRequest request = null;
		if (requestJson.getString("method").equals(EvenbetMethodType.GETBALANCE.getMethodName())) {
			request = new EvenbetGetBalanceRequest();
			((EvenbetGetBalanceRequest)request).setMethod(requestJson.getString("method"));
			((EvenbetGetBalanceRequest)request).setUserId(requestJson.getLong("userId"));
			((EvenbetGetBalanceRequest)request).setCurrency(requestJson.getString("currency"));
			((EvenbetGetBalanceRequest)request).setRawJson(jsonBody);
		}else if (requestJson.getString("method").equals(EvenbetMethodType.GET_CASH.getMethodName())) {
			request = new EvenbetGetCashRequest();
			((EvenbetGetCashRequest)request).setMethod(requestJson.getString("method"));
			((EvenbetGetCashRequest)request).setUserId(requestJson.getLong("userId"));
			((EvenbetGetCashRequest)request).setCurrency(requestJson.getString("currency"));
			((EvenbetGetCashRequest)request).setAmount(requestJson.getLong("amount"));
			((EvenbetGetCashRequest)request).setTransactionId(requestJson.getString("transactionId"));
			((EvenbetGetCashRequest)request).setRawJson(jsonBody);
		}else if (requestJson.getString("method").equals(EvenbetMethodType.RETURN_CASH.getMethodName())) {
			request = new EvenbetReturnCashRequest();
			((EvenbetReturnCashRequest)request).setMethod(requestJson.getString("method"));
			((EvenbetReturnCashRequest)request).setUserId(requestJson.getLong("userId"));
			((EvenbetReturnCashRequest)request).setCurrency(requestJson.getString("currency"));
			((EvenbetReturnCashRequest)request).setAmount(requestJson.getLong("amount"));
			((EvenbetReturnCashRequest)request).setTransactionId(requestJson.getString("transactionId"));
			((EvenbetReturnCashRequest)request).setRawJson(jsonBody);
		}
		return request;
	}
	
	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}


	/**
	 * @param method the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}



	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}



	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * @return the rawJson
	 */
	public String getRawJson() {
		return rawJson;
	}

	/**
	 * @param rawJson the rawJson to set
	 */
	public void setRawJson(String rawJson) {
		this.rawJson = rawJson;
	}

	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
	
	
}

