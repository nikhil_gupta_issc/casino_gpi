package com.evenbet.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class EvenbetGetBalanceResponse extends EvenbetResponse {

	@JsonProperty("balance")
	private Long balance;
	
	public EvenbetGetBalanceResponse(Long balance) {
		super();
		this.balance = balance;
	}

	public EvenbetGetBalanceResponse() {
		super();
	}

	/**
	 * @return the balance
	 */
	public Long getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(Long balance) {
		this.balance = balance;
	}	
}

