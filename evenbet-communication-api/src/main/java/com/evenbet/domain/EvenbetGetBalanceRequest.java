package com.evenbet.domain;

public class EvenbetGetBalanceRequest extends EvenbetRequest {	

	public String buildSignRequest() {
		return this.implode();
	}
	
	private String implode() {
		return String.join("", new String[]{this.getCurrency(), this.getMethod(), String.valueOf(this.getUserId())});
	}
	
}

