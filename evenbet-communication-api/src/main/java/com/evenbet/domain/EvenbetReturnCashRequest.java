package com.evenbet.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class EvenbetReturnCashRequest extends EvenbetRequest {
	
	@JsonProperty("amount")
	private Long amount;
	
	@JsonProperty("transactionId")
	private String transactionId;

	/**
	 * @return the amount
	 */
	public Long getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Long amount) {
		this.amount = amount;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String buildSignRequest() {
		return this.implode();
	}
	
	private String implode() {
		return String.join("", new String[]{String.valueOf(this.getAmount()), this.getCurrency(), this.getMethod(), this.getTransactionId(), String.valueOf(this.getUserId())});
	}
	
}

