package com.evenbet.api;

public interface EvenbetErrorCodes {

	public static final Long GENERIC_ERROR_CODE = 9999L;
	public static final Long INVALID_SIGNATURE = 1L;
	public static final Long PLAYER_NOT_FOUND = 2L;
	public static final Long INSUFFICIENT_FUNDS = 3L;
	public static final Long INVALID_REQUEST_PARAMS = 4L;
	public static final Long ALREADY_PROCCESED = 0L;
	public static final Long SUCCESS = 0L;
	
	public static final Long INTERNAL_ERROR_MOCK_CREDIT_CERO_FAIL = 90L;
}