package com.evenbet.api;

import java.util.HashMap;
import java.util.Map;

public enum EvenbetMethodType {
	GETBALANCE("GetBalance"), GET_CASH("GetCash"), RETURN_CASH("ReturnCash");
	
	private static Map<String, EvenbetMethodType> map = new HashMap<String, EvenbetMethodType>();
	static {
		map.put("GetBalance", EvenbetMethodType.GETBALANCE);
		map.put("GetCash", EvenbetMethodType.GET_CASH);
		map.put("ReturnCash", EvenbetMethodType.RETURN_CASH);
	}
	private EvenbetMethodType(String methodName) {
		this.methodName = methodName;
	}
	
	private String methodName;
	
	public String getMethodName() {
		return methodName;
	}
	
	public static EvenbetMethodType getByMethodName(String methodName) {
		return map.get(methodName);
	}
}
