package com.evenbet.api;

public interface EvenbetErrorMessages {
	public static final String INTERNAL_MESSAGE = "internal error";
	public static final String INVALID_SIGNATURE_MESSAGE = "Invalid signature";
	public static final String PLAYER_NOT_FOUND_MESSAGE = "Player not found";
	public static final String INSUFFICIENT_FUNDS_MESSAGE = "Insufficient funds";
	public static final String INVALID_REQUEST_PARAM_MESSAGE = "Invalid request params";
	public static final String ALREADY_PROCCESED_MESSAGE = "Transaction already processed";
}