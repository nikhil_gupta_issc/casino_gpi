package com.booming.api;

public enum BoomingMethodType {
	BETWIN("betwin"), PLAYER_INFO("playerinfo"), ROLLBACK("rollback");
	
	private BoomingMethodType(String methodName) {
		this.methodName = methodName;
	}
	
	private String methodName;
	
	public String getMethodName() {
		return methodName;
	}
}
