package com.booming.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class BoomingRequest extends AbstractBoomingRequest{

	@JsonProperty("session_id")
	private String sessionId;
	
	@JsonProperty("player_id")
	private String playerId;
	
	@JsonProperty("round")
	private Integer round;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("bet")
	private String bet;
	
	@JsonProperty("win")
	private String win;
	
	@JsonProperty("freespins")
	private BoomingFreespinsRequest freespins;
	
	@JsonProperty("operatorData")
	private String operatorData;
	
	@JsonProperty("customer_id")
	private String customerId;

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the playerId
	 */
	public String getPlayerId() {
		return playerId;
	}

	/**
	 * @param playerId the playerId to set
	 */
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	/**
	 * @return the round
	 */
	public Integer getRound() {
		return round;
	}

	/**
	 * @param round the round to set
	 */
	public void setRound(Integer round) {
		this.round = round;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the bet
	 */
	public String getBet() {
		return bet;
	}

	/**
	 * @param bet the bet to set
	 */
	public void setBet(String bet) {
		this.bet = bet;
	}

	/**
	 * @return the win
	 */
	public String getWin() {
		return win;
	}

	/**
	 * @param win the win to set
	 */
	public void setWin(String win) {
		this.win = win;
	}

	/**
	 * @return the freespins
	 */
	public BoomingFreespinsRequest getFreespins() {
		return freespins;
	}

	/**
	 * @param freespins the freespins to set
	 */
	public void setFreespins(BoomingFreespinsRequest freespins) {
		this.freespins = freespins;
	}

	/**
	 * @return the operatorData
	 */
	public String getOperatorData() {
		return operatorData;
	}

	/**
	 * @param operatorData the operatorData to set
	 */
	public void setOperatorData(String operatorData) {
		this.operatorData = operatorData;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	
}
