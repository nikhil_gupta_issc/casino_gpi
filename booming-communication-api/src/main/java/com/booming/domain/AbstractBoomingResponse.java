package com.booming.domain;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

public class AbstractBoomingResponse {

	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
}
