package com.booming.domain;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

public abstract class AbstractBoomingRequest {
	private String rawJson;
	
	/**
	 * @return the rawJson
	 */
	public String getRawJson() {
		return rawJson;
	}

	/**
	 * @param rawJson the rawJson to set
	 */
	public void setRawJson(String rawJson) {
		this.rawJson = rawJson;
	}

	public static<T> T create(String json, Class<T> klass) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			T request = objectMapper.readValue(json, klass);
			((BoomingRequest)request).setRawJson(json);
			return request;
		} catch (IOException e) {
			return null;
		}
	}
	
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
}

