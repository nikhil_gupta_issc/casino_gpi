package com.booming.domain;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

public class BoomingRequestHeader {

	private String signature;
	
	private String nonce;
	
	private String path;
	
	public BoomingRequestHeader(String signature, String nonce, String path) {
		super();
		this.signature = signature;
		this.nonce = nonce;
		this.path = path;
	}

	public BoomingRequestHeader() {
		super();
	}

	/**
	 * @return the signature
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * @param signature the signature to set
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * @return the nonce
	 */
	public String getNonce() {
		return nonce;
	}

	/**
	 * @param nonce the nonce to set
	 */
	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
	
	
}

