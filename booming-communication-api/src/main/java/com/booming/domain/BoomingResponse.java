package com.booming.domain;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
public class BoomingResponse extends AbstractBoomingResponse{

	public BoomingResponse(String balance, String _return) {
		super();
		this.balance = balance;
		this._return = _return;
	}

	public BoomingResponse() {
		super();
	}

	public BoomingResponse(String balance) {
		this(balance, null);
	}
	
	@JsonProperty("balance")
	private String balance;
	
	@JsonProperty("return")
	private String _return;
	
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}
	
	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @return the _return
	 */
	public String get_return() {
		return _return;
	}

	/**
	 * @param _return the _return to set
	 */
	public void set_return(String _return) {
		this._return = _return;
	}
	
	
}
