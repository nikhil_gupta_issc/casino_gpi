package com.booming.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class BoomingErrorResponse extends AbstractBoomingResponse{
	
	public BoomingErrorResponse(String error, String message) {
		super();
		this.error = error;
		this.message = message;
	}

	public BoomingErrorResponse() {
		super();
	}

	@JsonProperty("error")
	private String error;
	
	@JsonProperty("message")
	private String message;

	/**
	 * @return the code
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param code the code to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
	
}
