package com.booming.domain;

import java.io.IOException;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

public class BoomingStartSessionRequest {

	@JsonProperty("game_id")
	private String game_id;
	
	@JsonProperty("balance")
	private String balance;
	
	@JsonProperty("locale")
	private String locale;
	
	@JsonProperty("currency")
	private String currency;
	
	@JsonProperty("player_id")
	private String player_id;
	
	@JsonProperty("rollback_callback")
	private String rollback_callback;
	
	@JsonProperty("callback")
	private String callback;
	
	@JsonProperty("demo")
	private boolean demo;
	
	@JsonProperty("variant")
	private String variant;
	
	@JsonProperty("player_origin")
	private String player_origin;
	
	public BoomingStartSessionRequest() {
		
	}

	/**
	 * @param game_id the game_id to set
	 */
	public BoomingStartSessionRequest setGame_id(String game_id) {
		this.game_id = game_id;
		return this;
	}

	/**
	 * @param balance the balance to set
	 */
	public BoomingStartSessionRequest setBalance(String balance) {
		this.balance = balance;
		return this;
	}

	/**
	 * @param locale the locale to set
	 */
	public BoomingStartSessionRequest setLocale(String locale) {
		this.locale = locale;
		return this;
	}

	/**
	 * @param currency the currency to set
	 */
	public BoomingStartSessionRequest setCurrency(String currency) {
		this.currency = currency;
		return this;
	}

	/**
	 * @param player_id the player_id to set
	 */
	public BoomingStartSessionRequest setPlayer_id(String player_id) {
		this.player_id = player_id;
		return this;
	}

	/**
	 * @param rollback_Callback the rollback_Callback to set
	 */
	public BoomingStartSessionRequest setRollback_callback(String rollback_callback) {
		this.rollback_callback = rollback_callback;
		return this;
	}

	/**
	 * @param callback the callback to set
	 */
	public BoomingStartSessionRequest setCallback(String callback) {
		this.callback = callback;
		return this;
	}

	/**
	 * @param demo the demo to set
	 */
	public BoomingStartSessionRequest setDemo(boolean demo) {
		this.demo = demo;
		return this;
	}

	/**
	 * @param variant the variant to set
	 */
	public BoomingStartSessionRequest setVariant(String variant) {
		this.variant = variant;
		return this;
	}

	/**
	 * @param player_Origin the player_Origin to set
	 */
	public BoomingStartSessionRequest setPlayer_origin(String player_origin) {
		this.player_origin = player_origin;
		return this;
	}
	
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (IOException e) {
			return null;
		}
	}
}
