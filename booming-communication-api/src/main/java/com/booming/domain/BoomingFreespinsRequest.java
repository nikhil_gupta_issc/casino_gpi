package com.booming.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class BoomingFreespinsRequest {

	@JsonProperty("remaining")
	private Integer remaining;
	
	@JsonProperty("source_round")
	private Integer sourceRound;
	
	@JsonProperty("source_session")
	private String sourceSession;
	
	@JsonProperty("winnings")
	private String winnings;

	/**
	 * @return the remaining
	 */
	public Integer getRemaining() {
		return remaining;
	}

	/**
	 * @param remaining the remaining to set
	 */
	public void setRemaining(Integer remaining) {
		this.remaining = remaining;
	}

	/**
	 * @return the sourceRound
	 */
	public Integer getSourceRound() {
		return sourceRound;
	}

	/**
	 * @param sourceRound the sourceRound to set
	 */
	public void setSourceRound(Integer sourceRound) {
		this.sourceRound = sourceRound;
	}

	/**
	 * @return the sourceSession
	 */
	public String getSourceSession() {
		return sourceSession;
	}

	/**
	 * @param sourceSession the sourceSession to set
	 */
	public void setSourceSession(String sourceSession) {
		this.sourceSession = sourceSession;
	}

	/**
	 * @return the winnings
	 */
	public String getWinnings() {
		return winnings;
	}

	/**
	 * @param winnings the winnings to set
	 */
	public void setWinnings(String winnings) {
		this.winnings = winnings;
	}
		
		

}
