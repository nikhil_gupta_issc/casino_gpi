package com.booming.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class BoomingPlayerInfoResponse extends AbstractBoomingResponse{

	@JsonProperty("username")
	private String username;
	
	@JsonProperty("balance")
	private String balance;
	
	@JsonProperty("currency")
	private String currency;

	private String token;
	
	public BoomingPlayerInfoResponse(String username, String balance, String currency, String token) {
		super();
		this.username = username;
		this.balance = balance;
		this.currency = currency;
		this.token = token;
	}

	public BoomingPlayerInfoResponse() {
		super();
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	
	
	
	

}
