package com.booming.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class BoomingPlayerInfoRequest extends AbstractBoomingRequest{

	@JsonProperty("token")
	private String token;

	
	
	public BoomingPlayerInfoRequest(String token) {
		super();
		this.token = token;
	}

	public BoomingPlayerInfoRequest() {
		super();
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	
}
